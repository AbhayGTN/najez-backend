<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Backenduser extends CI_Controller {

  public function __construct() {
    // Call the Model constructor
    parent::__construct();
    if (!$this->session->userdata('loggedin')) {
      redirect('backlogin', 'refresh');
    }
    $this->load->model('backenduser_model');

    if (!in_array('1', $this->session->userdata('user_roles'))) {
      die('you dont have permission to access this part');
    }
  }

  public function index() {
    $data['head_title'] = 'User';
    $data['active'] = 'backenduser';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/backend-user/user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function add_user() {
    $data['head_title'] = 'Add User';
    $data['active'] = 'backenduser';
    $data['active_sub'] = 'add_user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
   
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/backend-user/add-user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function adduser() {
    $this->form_validation->set_rules('emailid', 'Email', 'trim|required|valid_email|max_length[100]|is_unique[users.emailid]');
    $this->form_validation->set_rules('username', 'Username', 'trim|required|max_length[100]|is_unique[users.username]');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[15]|matches[cpassword]');
    $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required');
    $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|max_length[15]|required|numeric');
    $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
    $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
    $this->form_validation->set_rules('user_role[]', 'user role', 'required');

    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Add User';
      $data['active'] = 'backenduser';
      $data['active_sub'] = 'add_user';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/backend-user/add-user-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $photo = $_FILES['photo']['name'];
      if (!empty($photo)) {
        $this->upload->initialize($this->set_photoupload_options());
        //$this->upload->do_upload();
        if (!$this->upload->do_upload('photo')) {
          $data['error'] = $this->upload->display_errors();
          $data['head_title'] = 'Add User';
          $data['active'] = 'backenduser';
          $data['active_sub'] = 'add_user';
          $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
          $data['userdata'] = $this->session->userdata('loggedin');
          $userdata = $this->session->userdata('loggedin');
          $id = $userdata->id;
          $this->load->view('template/head', $data);
          $this->load->view('template/header', $data);
          $this->load->view('template/sidenav', $data);
          $this->load->view('backend/backend-user/add-user-view', $data);
          $this->load->view('template/footer', $data);
          $this->load->view('template/foot', $data);
          //die();
        } else {
          $filedata = $this->upload->data();
          $path = 'assets/documents/' . $filedata['file_name'];
        }
      } else {
        $path = 'assets/images/user-icon.png';
      }
      $emailid = $this->input->post('emailid');
      $pass = $this->input->post('password');
      $cpass = $this->input->post('cpassword');
      $username = $this->input->post('username');
      $first_name = $this->input->post('first_name');
      $last_name = $this->input->post('last_name');
      $mobile = $this->input->post('mobile');
      $gender = $this->input->post('gender');

      $user_role = implode(',', $this->input->post('user_role'));


      $type = 2;
      $digits = 4;
      $unique_id = "USR" . str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

      $result = $this->backenduser_model->adduser($emailid, $unique_id, $pass, $cpass, $username, $first_name, $last_name, $type, $path, $mobile, $gender, $user_role);
      //print_r($result);die();
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('backenduser/add_user', 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'User successfully added.');
        redirect('backenduser/add_user', 'refresh');
      }
    }
  }

  public function view_users() {
    $data['head_title'] = 'View Backend Users';
    $data['active'] = 'backenduser';
    $data['active_sub'] = 'view_user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['userlist'] = $this->backenduser_model->viewusers();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/backend-user/user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  private function set_photoupload_options() {
    //upload an image options
    $config = array();
    $config['upload_path'] = '././assets/documents/';
    $config['allowed_types'] = 'gif|jpg|png';
    //$config['max_size']      = '0';
    $config['overwrite'] = FALSE;

    return $config;
  }

  public function hide($id) {
    if ($id != '') {
      $result = $this->backenduser_model->hideuser($id);
      echo $result;
    } else {
      echo '0';
    }
  }

  public function activate($id) {
    if ($id != '') {
      $result = $this->backenduser_model->activeuser($id);
      redirect('backenduser/view_users', 'refresh');
    } else {
      echo '0';
    }
  }

  public function viewuser($id) {
    $data['head_title'] = 'View User';
    $data['active'] = 'backenduser';
    $data['active_sub'] = 'view_user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['agentdata'] = $this->backenduser_model->viewuser($id);
    $data['user_roles'] = $this->backenduser_model->get_roles_string($id);

    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/backend-user/single-user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function edit_user($id) {
    $data['head_title'] = 'Edit User';
    $data['active'] = 'backenduser';
    $data['active_sub'] = 'view_user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['agentdata'] = $this->backenduser_model->viewuser($id);
    $data['user_roles'] = $this->backenduser_model->get_roles_string($id);
    $data['all_roles'] = $this->backenduser_model->get_all_roles();

    /*
      foreach ($data['all_roles'] as $key => $value)
      {
      if(in_array($value, $data['user_roles']))
      {
      echo "yes";
      }
      else{
      echo "no";
      }
      }
     */





    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/backend-user/edit-user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function updateuser($id) {
    $first_name = $this->input->post('first_name');
    $last_name = $this->input->post('last_name');
    $mobile = $this->input->post('mobile');
    $gender = $this->input->post('gender');
    $user_role = implode(',', $this->input->post('user_role'));
    $result = $this->backenduser_model->update_user($first_name, $last_name, $mobile, $gender, $id, $user_role);
    $this->session->set_flashdata('flashdata', 'User Updated Successfully.');
    redirect('backenduser/view_users/', 'refresh');
  }

  public function delete_user($id) {
    $this->db->delete("backend_users", ['id' => $id]);
    redirect('backenduser/view_users');
  }

  public function activity_log() {
    $data['head_title'] = 'Edit User';
    $data['active'] = 'backenduser';
    $data['active_sub'] = 'activity_log';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';

   $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
   $this->load->view('template/sidenav', $data);
    $this->load->view('backend/backend-user/activity_log', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

}
