<?php if (!defined('BASEPATH')) exit('.');
class Driver extends CI_Controller {

  function __construct() {
    
    parent::__construct();
    if (!$this->session->userdata('loggedin')) {
      redirect('backlogin', 'refresh');
    }
    if(!in_array('4', $this->session->userdata('user_roles')))
    {
      die('you dont have permission to access this part');
    }
    $this->load->model('driver_model');
    $this->load->model('model');
  }

  public function index() {
    $data['head_title'] = 'Driver';
    $data['active'] = 'driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/driver-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function add_driver() {
    $data['head_title'] = 'Add Driver';
    $data['active'] = 'driver';
    $data['active_sub'] = 'add_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/add-driver-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function   adddriver() {
    $this->form_validation->set_rules('emailid', 'Email', 'trim|required|valid_email|max_length[100]|is_unique[drivers.emailid]');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[15]|matches[cpassword]');
    $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required');
    $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
    $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
    $this->form_validation->set_rules('emailid', 'Email ID', 'trim|required|valid_email');
    $this->form_validation->set_rules('iqama','National ID/Iqama','required|is_unique[drivers.iqama]');
    // Fixed by Nandini
    $this->form_validation->set_rules('iban', 'IBAN Number', 'trim|required|max_length[24]');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Add Driver';
      $data['active'] = 'driver';
      $data['active_sub'] = 'add_driver';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/driver/add-driver-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $photo = $_FILES['photo']['name'];
      if (!empty($photo)) {
        $this->upload->initialize($this->set_photoupload_options());
        //$this->upload->do_upload();
        if (!$this->upload->do_upload('photo')) {
          $data['error'] = $this->upload->display_errors();
          $data['head_title'] = 'Add driver';
          $data['active'] = 'driver';
          $data['active_sub'] = 'add_driver';
          $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
          $data['userdata'] = $this->session->userdata('loggedin');
          $userdata = $this->session->userdata('loggedin');
          $id = $userdata->id;
          $this->load->view('template/head', $data);
          $this->load->view('template/header', $data);
          $this->load->view('template/sidenav', $data);
          $this->load->view('backend/driver/add-driver-view', $data);
          $this->load->view('template/footer', $data);
          $this->load->view('template/foot', $data);
          //die();
        } else {
          $filedata = $this->upload->data();
          $path = 'assets/documents/' . $filedata['file_name'];
        }
      } else {
        $path = 'assets/images/user-icon.png';
      }
      $emailid = $this->input->post('emailid');
      $pass = $this->input->post('password');
      $cpass = $this->input->post('cpassword');
      $username = $this->input->post('fname');
      $mname = $this->input->post('mname');
      $first_name = $this->input->post('fname');
      $last_name = $this->input->post('lname');
      $identy = $this->input->post('identy');
      $city = $this->input->post('city');
      $iban = $this->input->post('iban');
      $iban_bank = $this->input->post('iban_bank');
      $dob = $this->input->post('dob_dd') . '/' . $this->input->post('dob_mm') . '/' . $this->input->post('dob_yy');
      $mobile = $this->input->post('mobile');
      
      $delivertype = $this->input->post('type');
      if ($delivertype == 1 || $delivertype == 2) {
        $candeliver = 1;
      } else {
        $candeliver = 0;
      }
      $type = 2;
      $status = 1;
      $digits = 4;
      $unique_id = "DRV" . str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
      $result = $this->driver_model->adddriver($unique_id, $emailid, $pass, $cpass, $username, $first_name, $mname, $last_name, $type, $status, $path, $mobile, $identy, $city, $dob, $candeliver, $iban, $iban_bank);

      //print_r($result);die();
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('driver/add_driver', 'refresh');
      } else {
        //fixed by Nandini
        $this->session->set_flashdata('flashdata', 'Driver successfully added.Please assign vehicle to driver');
        redirect('vehicles/add_vehicle', 'refresh');
      }
    }
  }

  public function view_drivers() {
    $data['head_title'] = 'View drivers';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['driverlist'] = $this->driver_model->viewdrivers();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/driver-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  private function set_photoupload_options() {
    //upload an image options
    $config = array();
    $config['upload_path'] = '././assets/documents/';
    $config['allowed_types'] = 'gif|jpg|png';
    //$config['max_size']      = '0';
    $config['overwrite'] = FALSE;

    return $config;
  }

  public function hide($id) {
    if ($id != '') {
      $result = $this->driver_model->hidedriver($id);
    $this->load->model('dashboard_model');
    $data = $this->model->s_data("device_token,device","push_notification_driver",['driver_id'=>$id]);
      
  $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => base_url()."api/common/appnotify/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "notify_type=deact_user&device_id={$data[0]['device_token']}&device_type={$data[0]['device']}&notify_user=driver&notify_data=your are deactivated by admin",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
  ),
));
curl_exec($curl);
curl_close($curl);
     echo $result;
    } else {
      echo '0';
    }
  }

  public function activate($id) {
    if ($id != '') {
      $result = $this->driver_model->activatedriver($id);      
      $this->load->model('dashboard_model');
    $data = $this->model->s_data("device_token,device","push_notification_driver",['driver_id'=>$id]);
      
  $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => base_url()."api/common/appnotify/",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "notify_type=activate_user&device_id={$data[0]['device_token']}&device_type={$data[0]['device']}&notify_user=driver&notify_data=your are activated by admin",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
  ),
));
curl_exec($curl);
curl_close($curl);
     redirect('driver/view_drivers', 'refresh');
    } else {
      echo '0';
    }
  }

  public function viewdriver($id) {
    $data['head_title'] = 'View driver';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['agentdata'] = $this->driver_model->viewdriver($id);
    $this->load->model('dashboard_model');
    $data['stats'] = $this->dashboard_model->driver_single_view_stats($id);
    
    
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/single-driver-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function edit_driver($id) {
    $data['head_title'] = 'Edit driver';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['agentdata'] = $this->driver_model->viewdriver($id);
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;

    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/edit-driver-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function updatedriver($id) {
    $first_name = $this->input->post('fname');
    $mname = $this->input->post('mname');
    $last_name = $this->input->post('lname');
    $mobile = $this->input->post('mobile');
   // $gender = $this->input->post('gender');
    $identy = $this->input->post('iqama');
    $city = $this->input->post('city');
    $iban = $this->input->post('iban');
    $iban_bank = $this->input->post('iban_bank');
    $dob = $this->input->post('dob_dd') . '/' . $this->input->post('dob_mm') . '/' . $this->input->post('dob_yy');
    $result = $this->driver_model->update_driver($first_name, $mname, $last_name, $mobile, $identy, $city, $dob, $iban, $iban_bank, $id);
     $this->session->set_flashdata('flashdata', 'Driver successfully Updated.');
      redirect('driver/viewdriver/'.$id);    
  }

  public function vehicle_details() {
    $data['head_title'] = 'Vehicle Details';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');

    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/single-driver-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function update_profileimg($id) {
    $photo = $_FILES['profileimg']['name'];
    if (!empty($photo)) {
      $this->upload->initialize($this->set_photoupload_options());
      //$this->upload->do_upload();
      if (!$this->upload->do_upload('profileimg')) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('driver/viewdriver/' . $id, 'refresh');
        //die();
      } else {
        $filedata = $this->upload->data();
        $path = 'assets/documents/' . $filedata['file_name'];
      }
    } else {
      $path = $this->input->post('old_profileimg');
    }
    $result = $this->driver_model->update_profileimg($path, $id);
    if ($result <= 0) {
      $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
      redirect('driver/viewdriver/' . $id, 'refresh');
    } else {
      $this->session->set_flashdata('flashdata', 'Driver successfully updated.');
      redirect('driver/viewdriver/' . $id, 'refresh');
    }
  }
  
  public function ride_details($id)
  {
    $data['head_title'] = 'View drivers';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['agentdata'] = $this->driver_model->viewdriver($id);
    $this->load->model('dashboard_model');
    $data['userlist'] = $this->dashboard_model->ride_details($id);
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/ride_details_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
    
  }
  public function transaction_details($id)
  {
    $data['head_title'] = 'View drivers';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['agentdata'] = $this->driver_model->viewdriver($id);
    $this->load->model('dashboard_model');
    $data['userlist'] = $this->dashboard_model->wallet_trans_details($id);
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/transaction_details_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
    
  }
  
  public function block_driver($id)
  {
    $this->load->model('dashboard_model');
    $data = $this->model->s_data("device_token,device","push_notification_driver",['driver_id'=>$id]);
    $driver_data = $this->model->s_data("mobile","drivers",['id'=>$id]);
    
    
    $check_ride = $this->dashboard_model->check_driver_ride($id);
          if($check_ride >= 1)
          {
            $this->session->set_flashdata('cannot_block_user_msg', "This user in ride please try after sometime");
            redirect('driver/viewdriver/'.$id);
            die();
          }
    
    
    $this->db->update("drivers", ['webblock'=>'0'], ['id'=>$id]);
    $headers = array(
    'Content-Type: application/x-www-form-urlencoded'
   );
    $fields = ['notify_type'=>'block_user',
        'device_id'=>$data[0]['device_token'],
        'device_type'=>$data[0]['device'],
        'notify_user'=>'driver',
        'notify_data'=>'your account has been blocked'];
  $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => base_url('api/common/appnotify/'),
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "notify_type=block_user&device_id={$data[0]['device_token']}&device_type={$data[0]['device']}&notify_user=driver&notify_data=your account has been blocked",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
  ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);

// sms code 

$curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => base_url('api/common/smsnotify'),
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "mobile={$driver_data[0]['mobile']}&message=you are blocked",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
  ),
));
$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
print_r($response);    
          redirect('driver/viewdriver/'.$id);
  
  }
  
  public function unblock_driver($id)
  {
    $this->load->model('dashboard_model');
    $data = $this->model->s_data("device_token,device","push_notification_driver",['driver_id'=>$id]);
    $this->db->update("drivers", ['webblock'=>'1'], ['id'=>$id]);
   $driver_data = $this->model->s_data("mobile","drivers",['id'=>$id]);
     $curl = curl_init(); 

curl_setopt_array($curl, array(
  CURLOPT_URL => base_url('api/common/appnotify/'),
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "notify_type=unblock_user&device_id={$data[0]['device_token']}&device_type={$data[0]['device']}&notify_user=driver&notify_data=your account unblocked now",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

print_r($response);
         $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => base_url('api/common/smsnotify'),
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "mobile={$driver_data[0]['mobile']}&message=you are unblocked",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
    "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);
curl_close($curl);
    
      redirect('driver/viewdriver/'.$id);
  }
  public function bulk_action($action){
		//echo $action;
		$userid_box = json_decode($_POST['userid_box']);
		if(!empty($userid_box)){
			if($action == 'activate'){
				$columns = array('status' => 1, 'verified' => 1);
			}
			if($action == 'deactivate'){
				$columns = array('status' => 0);
			}
			if($action == 'unblock'){
				$columns = ['webblock' => 1];
			}
			if($action == 'block'){
				$columns = ['webblock' => 0];
			}
			
			foreach($userid_box as $userid){
				
				$this->model->u_data("drivers", $columns, ['id' => $userid]);
			}
			echo $this->db->last_query();die();
		}else{
			echo 0;die();
		}
	}

}
