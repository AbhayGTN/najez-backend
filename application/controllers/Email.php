<?php defined('BASEPATH') or exit('.');

class Email extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('email_template_model');
    if(!in_array('3', $this->session->userdata('user_roles')))
    {
      die('you dont have permission to access this part');
    }
  }

  public function index() {
    $this->view_all();
  }

  public function view_all() {

    $data['head_title'] = 'Email Tamplates';
    $data['active'] = 'email';
    $data['active_sub'] = 'view_email';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');

    $data['tamplate_list'] = $this->email_template_model->get_all_tamplate();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/email_template/all_list_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function view($id) {
    if (empty($id)) {
      $this->view_all();
      die();
    }
  
    $data['email_temp'] = $this->email_template_model->tamplate_all_meta($id);
    $data['head_title'] = 'Settings';
    $data['active'] = 'email';
    $data['active_sub'] = 'view_email';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/email_template/email_single_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function save_data($id)
  {
	  $path = '';
	if(isset($_FILES['attch']['name'])){
		//print_r($_FILES);die();
		$photo = $_FILES['attch']['name'];
		
			if(!empty($photo)){
				$this->upload->initialize($this->set_photoupload_options());
				//$this->upload->do_upload();
				if ( ! $this->upload->do_upload('attch')){
					$this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
					redirect('email/view/'.$id);
				}
				else{
					$filedata = $this->upload->data();
					//print_r($filedata);die();
					$path = 'assets/documents/'.$filedata['file_name'];
					
				}
			}
	}
      $this->email_template_model->save_data($this->input->post(),$path, $id);
      redirect('email/view_all');
  }
 private function set_photoupload_options()
	{   
		//upload an image options
		$config = array();
		$config['upload_path'] = '././assets/documents/';
		$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|txt';
		//$config['max_size']      = '500000';
		$config['overwrite']     = FALSE;

		return $config;
	}
}
