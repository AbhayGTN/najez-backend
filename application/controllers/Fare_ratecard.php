<?php defined('BASEPATH') or exit('.');

class Fare_ratecard extends CI_Controller{
  
  public function __construct() {
    parent::__construct();
    if(!in_array('8', $this->session->userdata('user_roles')))
    {
      die('you dont have permission to access this part');
    }
  }
  
  public function view()
  {
      
    $data['head_title'] = 'Fare Rate Card';
      $data['active'] = 'fare';
      $data['active_sub'] = '';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $userdata = $this->session->userdata('loggedin');
      $id = $userdata->id;
      $this->load->model('fare_ratecard_model','model');
      $data['fare'] = $this->model->table();
      
      
      
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/fare_ratecard/fare_ratecard_view');
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data); 
    
    
  }
  
  
  
}