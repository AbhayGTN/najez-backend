<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend_register extends CI_Controller {
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->model('user_model');
		$this->load->model('driver_model');
		$this->load->model('vehicle_model');
		$this->load->model('vehicledocuments_model');
    }
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function check_emailid_driver(){
		$emailid = $_POST['emailid'];
		$count = $this->user_model->checkemail_driver($emailid);
		echo $count;
	}
	public function check_emailid_user(){
		$emailid = $_POST['emailid'];
		$count = $this->user_model->checkemail_user($emailid);
		echo $count;
	}
	public function partner_reg(){
		//print_r($_POST);die();
		$fname = $_POST['fname'];
		$mname = $_POST['mname'];
		$lname = $_POST['lname'];
		$emailid = $_POST['emailid'];
		$mobileno = $_POST['mobileno'];
		$identy = $_POST['identy'];
		if(isset($_POST['dob_dd'])){
			$date = $_POST['dob_dd']."/".$_POST['dob_mm']."/".$_POST['dob_yy'];
		}else{
			$date = '';
		}

		$city = $_POST['city'];
		if(isset($_POST['code'])){
			$code = $_POST['code'];
		}else{
			$code = '';
		}
		
		
		$driver_type = $_POST['driver_type'];
		if($driver_type == 'Driver'){
			$can_deliver = 0;
		}
		if($driver_type == 'Delivery Man'){
			$can_deliver = 1;
		}
		if(isset($_POST['vehicle_sno'])){
			$vehicle_sno = $_POST['vehicle_sno'];
		}else{
			$vehicle_sno = '';
		}
		if(isset($_POST['vehicle_plate_char'])){
			$vehicle_no_plate = $_POST['vehicle_plate_char'].'-'.$_POST['vehicle_plate_num'];
		}else{
			$vehicle_no_plate ='';
		}
		
		$vehicle_type = $_POST['vehicle_type'];
		$car_model = $_POST['car_model'];
		$vehicle_year = $_POST['vehicle_year'];
		if(isset($_POST['vehicle_color'])){
			$vehicle_color = $_POST['vehicle_color'];
		}else{
			$vehicle_color = '';
		}
		
		/* doc */
		//print_r( $_FILES);die();
		$target_dir = "../../assets/document_uploaded/";
		$path ='';
		
		if(isset($_FILES['photograph'])){
			$photograph = $_FILES['photograph'];
			$photofile ='';
			if($photograph["name"] != ''){
				$this->upload->initialize($this->set_photoupload_options());

					if ( $this->upload->do_upload('photograph')){
						$filedata = $this->upload->data();
						$path = 'assets/document_uploaded/'.$filedata['file_name'];
					}
				$photofile = $path;
			}
		}else{
			$photofile ='';
		}
		
		if(isset($_FILES['residence'])){
			$residence = $_FILES['residence'];
			$residfile ='';
			if($residence["name"] != ''){
				$this->upload->initialize($this->set_photoupload_options());

					if ( $this->upload->do_upload('residence')){
						$filedata = $this->upload->data();
						$path = 'assets/document_uploaded/'.$filedata['file_name'];
					}
				$residfile = $path;
			}
		}else{
			$residfile ='';
		}
		if(isset($_FILES['driver_license'])){
			$driver_license = $_FILES['driver_license'];
			$drivrlicfile ='';
			if($driver_license["name"] != ''){
				$this->upload->initialize($this->set_photoupload_options());

					if ( $this->upload->do_upload('driver_license')){
						$filedata = $this->upload->data();
						$path = 'assets/document_uploaded/'.$filedata['file_name'];
					}
				$drivrlicfile = $path;
			}
		}else{
			$drivrlicfile ='';
		}
		
		if(isset($_FILES['vehicle_reg'])){
			$vehicle_reg = $_FILES['vehicle_reg'];
			$vregfile ='';
			if($vehicle_reg["name"] != ''){
				$this->upload->initialize($this->set_photoupload_options());

					if ( $this->upload->do_upload('vehicle_reg')){
						$filedata = $this->upload->data();
						$path = 'assets/document_uploaded/'.$filedata['file_name'];
					}
				$vregfile = $path;
			}
		}else{
			$vregfile ='';
		}
		
		if(isset($_FILES['vehicle_insur'])){
			$vehicle_insur = $_FILES['vehicle_insur'];
			$vinsurfile ='';
			if($vehicle_insur["name"] != ''){
				$this->upload->initialize($this->set_photoupload_options());

					if ( $this->upload->do_upload('vehicle_insur')){
						$filedata = $this->upload->data();
						$path = 'assets/document_uploaded/'.$filedata['file_name'];
					}
				$vinsurfile = $path;
			}
		}else{
			$vinsurfile ='';
		}
		
		if(isset($_FILES['auth_img'])){
			$auth_img = $_FILES['auth_img'];
			$authfile ='';
			if($auth_img["name"] != ''){
				$this->upload->initialize($this->set_photoupload_options());

					if ( $this->upload->do_upload('auth_img')){
						$filedata = $this->upload->data();
						$path = 'assets/document_uploaded/'.$filedata['file_name'];
					}
				$authfile = $path;
			}
		}else{
			$authfile ='';
		}
		
		if(isset($_FILES['tafweeth_img'])){
			$tafweeth_img = $_FILES['tafweeth_img'];
			$taffile ='';
			if($tafweeth_img["name"] != ''){
				$this->upload->initialize($this->set_photoupload_options());

					if ( $this->upload->do_upload('tafweeth_img')){
						$filedata = $this->upload->data();
						$path = 'assets/document_uploaded/'.$filedata['file_name'];
					}
				$taffile = $path;
			}
		}else{
			$taffile ='';
		}
		
		$iban = $_POST['iban'];
		if(isset($_POST['iban_bank'])){
			$iban_bank = $_POST['iban_bank'];
		}else{
			$iban_bank = '';
		}
		
		if(isset($_POST['driverlic_dd'])){
		    $date_driverlicenseinput = $_POST['driverlic_dd']."/".$_POST['driverlic_mm']."/".$_POST['driverlic_yy'];
		}else{
		    $date_driverlicenseinput = '';
		}
		
		if(isset($_POST['vehiclereg_dd'])){
		    $date_vehiclereginput = $_POST['vehiclereg_dd']."/".$_POST['vehiclereg_mm']."/".$_POST['vehiclereg_yy'];
		}else{
		    $date_vehiclereginput = '';
		}
		
		if(isset($_POST['vehicleins_dd'])){
		    $date_vehicleinsurinput = $_POST['vehicleins_dd']."/".$_POST['vehicleins_mm']."/".$_POST['vehicleins_yy'];
		}else{
		    $date_vehicleinsurinput = '';
		}
		
		if(isset($_POST['tafw_dd'])){
		    $date_tafweethimginput = $_POST['tafw_dd']."/".$_POST['tafw_mm']."/".$_POST['tafw_yy'];
		}else{
		    $date_tafweethimginput = '';
		}
		
		
		
		$driver_id = $this->driver_model->reg_driver($fname, $mname, $lname, $emailid, $mobileno, $identy, $date, $city, $code, $can_deliver, $drivrlicfile);
		//echo $this->db->last_query();
		//echo $driver_id;die();
		//$result = $this->vehicle_model->add_vehicle($driver_id, $vehicle_sno, $vehicle_no_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $photofile, $residfile,$vregfile, $vinsurfile, $authfile,$taffile, $iban,$iban_bank);
		//echo 'test';
		//echo $driver_id.'   '.$vehicle_sno.'   '.$vehicle_no_plate.'   '.$vehicle_type.'   '.$car_model.'   '.$vehicle_year.'   '.$vehicle_color.'   '.$iban.'   '.$iban_bank;
		//die();
		$result = $this->vehicle_model->add_vehicle($driver_id, $vehicle_sno, $vehicle_no_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $iban, $iban_bank);
		//echo $this->db->last_query();
		//print_r($result);die();
		if($can_deliver == 0){
			$datavehicledocument = array(
			   array(
				  'driver_id' => $driver_id,
				  'vehicle_id' => $result,
				  'doc_key' => 'photograph',
				  'doc_val' => $photofile,
				  'doc_expire' => '',
			   ),
			   array(
				  'driver_id' => $driver_id ,
				  'vehicle_id' => $result ,
				  'doc_key' => 'residence-identity',
				  'doc_val' => $residfile,
				  'doc_expire' => '',
			   ),
				array(
				  'driver_id' => $driver_id ,
				  'vehicle_id' => $result,
				  'doc_key' => 'driver-license',
				  'doc_val' => $vregfile,
				  'doc_expire' => $date_driverlicenseinput,
			   ),
				array(
				  'driver_id' => $driver_id ,
				  'vehicle_id' => $result,
				  'doc_key' => 'vehicle-reg',
				  'doc_val' => $vregfile,
				  'doc_expire' => $date_vehiclereginput,
			   ),
				array(
				  'driver_id' => $driver_id ,
				  'vehicle_id' =>$result,
				  'doc_key' => 'vehicle-insur',
				  'doc_val' => $vinsurfile,
				  'doc_expire' =>$date_vehicleinsurinput,
			   ),
				array(
				  'driver_id' => $driver_id ,
				  'vehicle_id' =>$result,
				  'doc_key' => 'auth-img',
				  'doc_val' => $authfile,
				  'doc_expire' => '',
			   ),
				array(
				  'driver_id' => $driver_id ,
				  'vehicle_id' =>$result,
				  'doc_key' => 'tafweeth-img',
				  'doc_val' => $taffile,
				  'doc_expire' => $date_tafweethimginput,
			   )
			);
		}else{
			$datavehicledocument = array(
				array(
				  'driver_id' => $driver_id ,
				  'vehicle_id' =>$result,
				  'doc_key' => 'tafweeth-img',
				  'doc_val' => $taffile,
				  'doc_expire' => $date_tafweethimginput,
			   )
			);
		}
		
		
		$this->vehicledocuments_model->add_vehicledocument( $datavehicledocument);
		
		
		
		
		if($result){
                  if($this->session->userdata['site_lang'] == 'arabic')
                  {
                    $message = '<html><body>';
					$message .= '<h3>Hello,</h3>';
					$message .= '<p>.نشكركم لتزويدنا بمعلوماتكم، سنبقيكم على الاطلاع</p>';
					$message .= '<p>.نشكركم لتزويدنا بمعلوماتكم، يرجى منكم الضغط على الرابط الذي تم إرساله إليكم إلى بريدكم الإلكتروني لنكمل إجراءات التسجيل</p>';
					$code = base64_encode('encode-driver-'.$driver_id."-arabic");
					$link = '<a target="_blank" href="'.base_url('verify/activate/'.$code).'">'.base_url('verify/activate/'.$code).'</a>';
					$message .= '<p>'.$link.'</p>';
					$message .= '</body></html>';
					$this->send($emailid, $message);
                  }
                            else {
$message = '<html><body>';
					$message .= '<h3>Hello,</h3>';
					$message .= '<p>Thankyou for submitting the details. We will keep you posted.</p>';
					$message .= '<p>To verify your emailid, please click on below link</p>';
					$code = base64_encode('encode-driver-'.$driver_id.'-english');
					$link = '<a target="_blank" href="'.base_url('verify/activate/'.$code).'">'.base_url('verify/activate/'.$code).'</a>';
					$message .= '<p>'.$link.'</p>';
					$message .= '</body></html>';
					$this->send($emailid, $message);
                            }
		}
	}
	public function user_reg(){
		
		$fname = $_POST['user_fname'];
		$lname = $_POST['user_lname'];
		$mname = $_POST['user_mname'];
		$email = $_POST['user_email'];
		$pass = rand();
		$mobile = $_POST['user_mobile'];
		$gender = $_POST['user_gender'];
		$user_city = $_POST['user_city'];
	
		$userid = $this->user_model->reg_user($fname, $mname, $lname,$email,$pass,$mobile,$gender,$user_city);
		
		if($this->session->userdata['site_lang'] == 'arabic')
                {
                  $message = '<html><body>';
				$message .= '<h3>Hello,</h3>';
				$message .= '<p>.نشكركم لتزويدنا بمعلوماتكم، سنبقيكم على الاطلاع</p>';
				$message .= '<p>.نشكركم لتزويدنا بمعلوماتكم، يرجى منكم الضغط على الرابط الذي تم إرساله إليكم إلى بريدكم الإلكتروني لنكمل إجراءات التسجيل</p>';
				$code = base64_encode('encode-user-'.$userid.'-arabic');
				$link = '<a target="_blank" href="'.base_url('verify/activate/'.$code).'">'.base_url('verify/activate/'.$code).'</a>';
				$message .= '<p>'.$link.'</p>';
				$message .= '</body></html>';
				$this->send($email, $message);
                  
                  
                }
 else {
   $message = '<html><body>';
			$message .= '<h3>Hello,</h3>';
			$message .= '<p>Thankyou for submitting the details. We will keep you posted.</p>';
			$message .= '<p>To verify your emailid, please click on below link</p>';
			$code = base64_encode('encode-user-'.$userid.'-english');
			$link = '<a target="_blank" href="'.base_url('verify/activate/'.$code).'">'.base_url('verify/activate/'.$code).'</a>';
			$message .= '<p>'.$link.'</p>';
			$message .= '</body></html>';
			$this->send($email, $message);
   
   
 }
		
		
				
			

			

		
			
		
		
	
			
			
	}
        
        public function send($email,$message)
        {
          $this->email->from('noreply@najez-online.com', 'Najez');
          $this->email->to($email); 
			//$this->email->cc('another@another-example.com'); 
			//$this->email->bcc('them@their-example.com'); 
			$this->email->subject('Thankyou');
			$this->email->message($message);
			$this->email->set_mailtype("html");
			$this->email->send();
          
        }
	private function set_photoupload_options()
	{   
		//upload an image options
		$config = array();
		$config['upload_path'] = '././assets/document_uploaded/';
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */