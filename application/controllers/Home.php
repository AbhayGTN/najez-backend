<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		
		$this->load->model('dashboard_model');
		$this->load->model('user_model');
		$this->load->model('model');
		if(empty($this->session->userdata['site_lang'])){
			 $this->session->set_userdata('site_lang', 'english');
		}
		//$language = ($language != "") ? $language : "arabic";
       
    }
	public function index()
	{
		$data['head_title'] = 'Home';
		$data['active'] = 'home';
		$data['active_sub'] = 'home';
		$data['body_class'] = 'hold-transition skin-blue sidebar-mini';
		
		//$data['car_list'] = $this->model->get_car_list();
		$data['brand_list'] = $this->model->get_barnd_list();
		$this->load->view('template/najez-menu', $data);
		$this->load->view('homepage/index', $data);
		if(empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'arabic'){
			//$this->load->view('template/frontend-form-ar', $data);
		}else{
			//$this->load->view('template/frontend-form-en', $data);
		}
		$this->load->view('template/frontend-footer', $data);
	}
	
	public function get_car_type()
	{
		
		$data = $this->model->get_car_type($this->input->post('id'));
		
		$idiom = $this->session->get_userdata('language');
		//$curentLan=$idiom['site_lang'];
		
		if(empty($this->session->userdata['site_lang'])){
		    $curentLan='english';
		}elseif($this->session->userdata['site_lang'] =='english'){
		    $curentLan=$idiom['site_lang'];
		//}elseif($this->session->userdata['site_lang'] =='arabic'){
		   // $curentLan=$idiom['site_lang'];
		}
		
		$this->lang->load('message_lang', $curentLan);
        	$array_val=[];
		foreach($data as $key => $value){
		    $lang_arr['car_type'] = $this->lang->line($value['car_type']);
		    array_push($array_val, $lang_arr);
		}
		
		echo json_encode($array_val);
		
		//$result['type'] = $data;
		//echo json_encode($data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */