<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index($id)
	{
		$this->load->model('page_model');
		$pagedata = $this->page_model->get_page($id);
		$data['pagedata'] = $pagedata;
		$data['active'] = $pagedata[0]['slug'];
		//$data['page_title']= $pagedata[0]['title_en'];
		if(isset($this->session->userdata['site_lang']) && $this->session->userdata['site_lang'] =='arabic'){
		    $data['page_title']=$pagedata[0]['title_ar'];
		}else{
		    $data['page_title']=$pagedata[0]['title_en'];
		}
		$data['body_class']= $pagedata[0]['slug' ].' single-page';
		$this->load->view('template/najez-menu', $data);
		
		$this->load->view('frontend/pages/page-view',$data);
		
	}
}