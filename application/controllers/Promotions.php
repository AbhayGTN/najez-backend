<?php defined('BASEPATH') or exit('.');

class promotions extends CI_Controller{
  
  public function __construct() {
    parent::__construct();
    $this->load->model('promotions_model');
    if(!in_array('7', $this->session->userdata('user_roles')))
    {
      die('you dont have permission to access this part');
    }
  }
  
  public function add()
  {
      $data['head_title'] = 'Add New';
      $data['active'] = 'promotions';
      $data['active_sub'] = 'add';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $userdata = $this->session->userdata('loggedin');
      $id = $userdata->id;
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/promotions/add_view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data); 
  }
  
  public function add_submit()
  {
    $this->form_validation->set_rules('promo_code', 'Promo Code', 'required');
    $this->form_validation->set_rules('discount_percentage', 'Discount Percentage', 'numeric');
    $this->form_validation->set_rules('user_no', 'user limit', 'required|numeric');
    if ($this->form_validation->run() == FALSE)
    {
        $this->add();
    }
    else
    {
      $data = $this->input->post();
      $this->promotions_model->insert_data($data);
      redirect('promotions/view_all');
    }
  }
  
  public function view($id)
  {
      $data['head_title'] = 'View All';
      $data['active'] = 'Promotions';
      $data['active_sub'] = 'add';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $data['promo_data'] = $this->promotions_model->fatch($id);
      
      
      
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/promotions/edit_view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data); 
  }
  
  public function view_all()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'promotions';
    $data['active_sub'] = 'view';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->promotions_model->get_data();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/promotions/view_all', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function hide($id)
  {
    $this->db->update("promotions", ['status'=>'0'], ['id'=>$id]);
    echo "1"; 
  }
  
  public function update()
  {
    $data = $this->input->post();
    print_r($data);
    //$this->db->update("promotions", ['status'=>'0'], ['id'=>$id]);
  }
}