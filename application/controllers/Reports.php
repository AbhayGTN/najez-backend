<?php defined('BASEPATH') or exit('.');

class Reports extends CI_Controller{
  
  public function __construct() {
    parent::__construct();
    $this->load->model('reports_model');
    if(!in_array('9', $this->session->userdata('user_roles')))
    {
      die('you dont have permission to access this part');
    }
  }

  public function total_income()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'total_income';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->total_income();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/total_income_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function total_sales()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'total_sales';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->total_sales();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/total_sales_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function total_promotion_details()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'total_promotion_details';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->total_promotions();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/total_promotions_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function total_driver_income()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'total_driver_income';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->total_driver_income();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/total_driver_income', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function driver_active_trips()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'driver_active_trips';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->active_driver_rides();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/total_driver_active_rides_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function active_driver_past_30_days()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'last_30_days';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->active_driver_last_30_days();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/last_30_days_active_drivers', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function active_drivers_yesterday()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'active_yesterday';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->active_drivers_yesterday();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/active_driver_yesterday', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function earnings_per_driver()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'earnings_per_deriver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->earnings_per_driver();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/earnings_per_deriver', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
    
  }
  
  public function total_daily_sales()
  {
    $date = date('Y-m-d');
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'total_daily_sales';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->total_daily_sales($date);
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/total_daily_sales', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function canceled_rides()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'canceled_rides';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->canceled_rides();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/canceled_rides_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function confirm_rides()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'confirm_rides';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->confirmed_rides();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/confirm_rides_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function inactive_drivers()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'inactive_drivers';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->inactive_drivers();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/inactive_drivers_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function inactive_customers()
  {
    $data['head_title'] = 'View All Cards';
    $data['active'] = 'reports';
    $data['active_sub'] = 'inactive_customers';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->reports_model->inactive_customers();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/reports/inactive_customers_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
   }
}