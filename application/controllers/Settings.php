<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Settings extends CI_Controller {

  function __construct() {
    // Call the Model constructor
    parent::__construct();
    if (!$this->session->userdata('loggedin')) {
      redirect('backlogin', 'refresh');
    }
    if(!in_array('10', $this->session->userdata('user_roles')))
    {
      die('you dont have permission to access this part');
    }
    $this->load->model('setting_model');
    $this->load->model('model');
  }

  public function index() {
    $data['head_title'] = 'Settings';
    $data['active'] = 'settings';
    $data['active_sub'] = 'settings';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $data['settings_data'] = $this->setting_model->get_settings();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/settings/setting-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function email() {

    $data['head_title'] = 'Settings';
    $data['active'] = 'settings';
    $data['active_sub'] = 'settings';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $data['settings_data'] = $this->setting_model->get_settings();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/settings/email_settings_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function email_submit() {
    $data = $this->input->post(['english_text', 'arabic_text']);
    print_r($data);
  }

  public function add_city() {
    $data['head_title'] = 'Add City';
    $data['active'] = 'settings';
    $data['active_sub'] = 'add_city';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $data['city_list'] = $this->setting_model->get_city();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/settings/cities-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function addcity() {
    $this->form_validation->set_rules('city_name_en', 'City Name', 'trim|required|min_length[2]|max_length[30]|is_unique[city_master.city_name_en]');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Add City';
      $data['active'] = 'settings';
      $data['active_sub'] = 'add_city';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $userdata = $this->session->userdata('loggedin');
      $id = $userdata->id;
      $data['city_list'] = $this->setting_model->get_city();
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/settings/cities-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $data = $this->input->post();
      $result = $this->setting_model->add_city($data);
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('settings/add_city', 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'City successfully added.');
        redirect('settings/add_city', 'refresh');
      }
    }
  }

  public function hidecity($id) {
    if ($id != '') {
      $result = $this->setting_model->hidecity($id);
      echo $result;
    } else {
      echo '0';
    }
  }

  public function add_car() {
    $data['head_title'] = 'Add Car';
    $data['active'] = 'settings';
    $data['active_sub'] = 'add_car';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['car_list'] = $this->setting_model->get_car();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/settings/cars-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function edit_car($id) {
    $data['head_title'] = 'Edit Car';
    $data['active'] = 'settings';
    $data['active_sub'] = 'add_car';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['car_list'] = $this->setting_model->get_car();
    $data['car_data'] = $this->setting_model->get_cardata($id);
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/settings/edit-cars-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function addcar() {
    $this->form_validation->set_rules('car_brand', 'Car Brand', 'trim|required|min_length[2]|max_length[30]');
    $this->form_validation->set_rules('car_model', 'Car Model', 'trim|required|min_length[2]|max_length[30]');
    $this->form_validation->set_rules('car_type', 'Car Type', 'trim|required|min_length[2]|max_length[30]');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Add Car';
      $data['active'] = 'settings';
      $data['active_sub'] = 'add_car';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $data['car_list'] = $this->setting_model->get_car();
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/settings/cars-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $data = $this->input->post();
      $result = $this->setting_model->add_car($data);
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('settings/add_car', 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'Car successfully added.');
        redirect('settings/add_car', 'refresh');
      }
    }
  }

  public function updatecar($id) {
    $this->form_validation->set_rules('car_brand', 'Car Brand', 'trim|required|min_length[2]|max_length[30]');
    $this->form_validation->set_rules('car_model', 'Car Model', 'trim|required|min_length[2]|max_length[30]');
    $this->form_validation->set_rules('car_type', 'Car Type', 'trim|required|min_length[2]|max_length[30]');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Edit Car';
      $data['active'] = 'settings';
      $data['active_sub'] = 'add_car';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $data['car_list'] = $this->setting_model->get_car();
      $data['car_data'] = $this->setting_model->get_cardata($id);
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/settings/edit-cars-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $data = $this->input->post();
      $result = $this->setting_model->update_car($data, $id);
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('settings/add_car', 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'Car successfully updated.');
        redirect('settings/add_car', 'refresh');
      }
    }
  }

  public function hidecar($id) {
    if ($id != '') {
      $result = $this->setting_model->hidecar($id);
      echo $result;
    } else {
      echo '0';
    }
  }

  public function add_bank() {
    $data['head_title'] = 'Add Bank';
    $data['active'] = 'settings';
    $data['active_sub'] = 'add_bank';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $data['bank_list'] = $this->setting_model->get_bank();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/settings/banks-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function addbank() {
    $this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|min_length[2]|max_length[40]');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Add Bank';
      $data['active'] = 'settings';
      $data['active_sub'] = 'add_bank';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $userdata = $this->session->userdata('loggedin');
      $id = $userdata->id;
      $data['bank_list'] = $this->setting_model->get_bank();
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/settings/banks-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $data = $this->input->post();
      $result = $this->setting_model->add_bank($data);
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('settings/add_bank', 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'Bank successfully added.');
        redirect('settings/add_bank', 'refresh');
      }
    }
  }

  public function hidebank($id) {
    if ($id != '') {
      $result = $this->setting_model->hidebank($id);
      echo $result;
    } else {
      echo '0';
    }
  }
  
  public function driver_cancle_options()
  {
      $data['head_title'] = 'Add Driver Cancel Option';
      $data['active'] = 'settings';
      $data['active_sub'] = 'add_bank';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $data['city_list'] = $this->setting_model->cancel_options_driver();
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/settings/driver_cancel_options_view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    
  }
  
  public function addoption()
  {
    $input = $this->input->post(['option_en','option_ar']);
    $this->setting_model->insert_driver_option($input);
    redirect('settings/driver_cancle_options');
  }
  
  public function del_driver_option($id)
  {
    if ($id != '') {
      $data = $this->setting_model->d_data("master_cancel_options",['id'=> $id]);
      echo $data;
    } else {
      echo '0';
    }     
  }
  
  public function user_cancle_options()
  {
      $data['head_title'] = 'Add User Cancel Option';
      $data['active'] = 'settings';
      $data['active_sub'] = 'add_bank';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $data['city_list'] = $this->setting_model->cancel_options_user();
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/settings/user_cancel_options_view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
   }
  
  public function addoption_u()
  {
    $input = $this->input->post(['option_en','option_ar']);
    $this->setting_model->insert_user_option($input);
    redirect('settings/user_cancle_options');
  }
  
  public function del_user_option($id)
  {
    
    
       if ($id != '') {
      $data = $this->setting_model->d_data("master_user_cancel_options",['id'=> $id]);
      echo $data;
    } else {
      echo '0';
    }     
  }
  public function airport_zone() {
    $data['head_title'] = 'Airport List';
    $data['active'] = 'settings';
    $data['active_sub'] = 'airport_zone';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $data['airport_list'] = $this->model->s_data("id,name","master_airport_zone");
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/settings/airport-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function addairport() {
	  
    $this->form_validation->set_rules('airport_name', 'Airport Name', 'trim|required|min_length[2]|max_length[100]');
    if ($this->form_validation->run() == FALSE) {
		$data['head_title'] = 'Airport List';
		$data['active'] = 'settings';
		$data['active_sub'] = 'airport_zone';
		$data['body_class'] = 'hold-transition skin-blue sidebar-mini';
		$data['userdata'] = $this->session->userdata('loggedin');
		$userdata = $this->session->userdata('loggedin');
		$id = $userdata->id;
		$data['airport_list'] = $this->model->s_data("id,name","master_airport_zone");
		$this->load->view('template/head', $data);
		$this->load->view('template/header', $data);
		$this->load->view('template/sidenav', $data);
		$this->load->view('backend/settings/airport-view', $data);
		$this->load->view('template/footer', $data);
		$this->load->view('template/foot', $data);
    } else {
      $data['name'] = $this->input->post('airport_name');
	  $url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$data['name'].'&sensor=false';
		$json_data = file_get_contents($url);
		$result2 = json_decode($json_data, TRUE);
		$data['lat'] = $result2['results'][0]['geometry']['location']['lat'];
		$data['lng'] = $result2['results'][0]['geometry']['location']['lng'];
		$result = $this->model->i_data("master_airport_zone",$data);
		$this->session->set_flashdata('flashdata', 'Airport successfully added.');
        redirect('settings/airport_zone', 'refresh');
    }
  }

  public function hideairport($id) {
    if ($id != '') {
      $result = $this->model->d_data("master_airport_zone",["id"=>$id]);
      echo $result;
    } else {
      echo '0';
    }
  }
  
  

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */