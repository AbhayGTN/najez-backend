<?php defined('BASEPATH') or exit('.');

class Tools extends CI_Controller{
  
  public function __construct() {
    parent::__construct();
    
  }
  
  public function index()
  {
    redirect('dashboard');
  }
  
  public function bulk_action() 
  {
     $data['head_title'] = 'Bulk Action';
    $data['active'] = 'tools';
    $data['active_sub'] = 'bulk_action';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/tools/bulk_action', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function block()
  {
      $config['upload_path']          = $_SERVER["DOCUMENT_ROOT"].'/najeztesting/assets/temp_upload/';
      $config['allowed_types']        = 'csv';
      $config['max_size']             = 10000;
      $this->upload->initialize($config);
      $this->load->library('upload', $config);
      if ( ! $this->upload->do_upload('csv_file'))
      {
        $error = array('error' => $this->upload->display_errors());
        $this->session->set_flashdata('flashdata',$error['error']);
        redirect('tools/bulk_action');
     }
      else
      {
          $data =  $this->upload->data();
          $csv = array_map('str_getcsv', file($data['full_path']));
          $this->load->model('model');
          foreach ($csv as $key => $value) {
          //  $this->model->u_data($value[4], ['status'=>$value[6]], ['mobile'=>$value[5]]);
          }
          
        $this->session->set_flashdata('flashdata','Success');
        redirect('tools/bulk_action');
      }
      
      
    }
  
  public function broadcast()
  {
    $data['head_title'] = 'Bulk Action';
    $data['active'] = 'tools';
    $data['active_sub'] = 'broadcast';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/tools/broadcast', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
    
    
    
  }
  
  public function send_email()
  {
    
    
  }
  
  public function send_push()
  {
    
    
  }
  
  
}