<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class User extends CI_Controller {

  function __construct() {
    // Call the Model constructor
    parent::__construct();
    if (!$this->session->userdata('loggedin')) {
      redirect('backlogin', 'refresh');
    }
    if(!in_array('2', $this->session->userdata('user_roles')))
    {
      die('you dont have permission to access this part');
    }
    $this->load->model('user_model');
    $this->load->model('model');
  }

  public function index() {
    $data['head_title'] = 'User';
    $data['active'] = 'user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/user/user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function add_user() {
    $data['head_title'] = 'Add Customer';
    $data['active'] = 'user';
    $data['active_sub'] = 'add_user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/user/add-user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function adduser() {
    $this->form_validation->set_rules('emailid', 'Email', 'trim|required|valid_email|max_length[100]|is_unique[users.emailid]');

    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[15]|matches[cpassword]');
    $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required');
    $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|max_length[15]|required|is_unique[users.mobile]');
    $this->form_validation->set_rules('name', 'Name', 'trim|required');
    //$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Add User';
      $data['active'] = 'user';
      $data['active_sub'] = 'add_user';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/user/add-user-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $photo = $_FILES['photo']['name'];
      if (!empty($photo)) {
        $this->upload->initialize($this->set_photoupload_options());
        //$this->upload->do_upload();
        if (!$this->upload->do_upload('photo')) {
          $data['error'] = $this->upload->display_errors();
          $data['head_title'] = 'Add User';
          $data['active'] = 'user';
          $data['active_sub'] = 'add_user';
          $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
          $data['userdata'] = $this->session->userdata('loggedin');
          $userdata = $this->session->userdata('loggedin');
          $id = $userdata->id;
          $this->load->view('template/head', $data);
          $this->load->view('template/header', $data);
          $this->load->view('template/sidenav', $data);
          $this->load->view('backend/user/add-user-view', $data);
          $this->load->view('template/footer', $data);
          $this->load->view('template/foot', $data);
          //die();
        } else {
          $filedata = $this->upload->data();
          $path = 'assets/documents/' . $filedata['file_name'];
        }
      } else {
        $path = 'assets/images/user-icon.png';
      }
      $emailid = $this->input->post('emailid');
      $pass = $this->input->post('password');
      $cpass = $this->input->post('cpassword');
      $username = $this->input->post('first_name') . rand(111, 999);
      $first_name = $this->input->post('name');
      $last_name = $this->input->post('last_name');
      $mobile = $this->input->post('mobile');
      $gender = $this->input->post('gender');
      $type = 3;
      $status = 1;
      $digits = 4;
      $unique_id = "CUS" . str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
      $result = $this->user_model->adduser($unique_id, $emailid, $pass, $cpass, $username, $first_name, $last_name, $type, $status, $path, $mobile, $gender);
      //print_r($result);die();
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('user/add_user', 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'User successfully added.');
        redirect('user/add_user', 'refresh');
      }
    }
  }

  public function view_users() {
    $data['head_title'] = 'View Users';
    $data['active'] = 'user';
    $data['active_sub'] = 'view_user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['userlist'] = $this->user_model->viewusers();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/user/user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  private function set_photoupload_options() {
    //upload an image options
    $config = array();
    $config['upload_path'] = '././assets/documents/';
    $config['allowed_types'] = 'gif|jpg|png';
    //$config['max_size']      = '0';
    $config['overwrite'] = FALSE;

    return $config;
  }

  public function hide($id) {
    if ($id != '') {
      $result = $this->user_model->hideuser($id);
      echo $result;
    } else {
      echo '0';
    }
  }

  public function activate($id) {
    if ($id != '') {
      $result = $this->user_model->activeuser($id);
      redirect('user/view_users', 'refresh');
    } else {
      echo '0';
    }
  }

  public function viewuser($id) {

    $data['head_title'] = 'View User';
    $data['active'] = 'user';
    $data['active_sub'] = 'view_user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['agentdata'] = $this->user_model->viewuser($id);

    $data['stats'] = $this->user_model->single_view_stats($id);

    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/user/single-user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function edit_user($id) {
    $data['head_title'] = 'Edit User';
    $data['active'] = 'user';
    $data['active_sub'] = 'view_user';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['agentdata'] = $this->user_model->viewuser($id);
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;

    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/user/edit-user-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function updateuser($id) {
    $first_name = $this->input->post('first_name');
    $last_name = $this->input->post('last_name');
    $mobile = $this->input->post('mobile');
    $gender = $this->input->post('gender');
    $result = $this->user_model->update_user($first_name, $last_name, $mobile, $gender, $id);
    //print_r($result);die();
    if ($result <= 0) {
      $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
      redirect('user/edit_user/' . $id, 'refresh');
    } else {
      $this->session->set_flashdata('flashdata', 'User successfully added.');
      redirect('user/view_users/', 'refresh');
    }
  }

  public function block($id) {
    $this->load->model('dashboard_model');
    $data = $this->model->s_data("device_token,device", "push_notification_user", ['userid' => $id]);
    $check_ride = $this->dashboard_model->check_user_ride($id);
    
    
    if ($check_ride >= 1) {
      $this->session->set_flashdata('cannot_block_user_msg', "This user in ride please try after sometime");
      redirect('user/viewuser/' . $id);
      die();
    }
    $this->db->update("users", ['webblock' => '0'], ['id' => $id]);
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => base_url('api/common/appnotify/'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "notify_type=block_user&device_id={$data[0]['device_token']}&device_type={$data[0]['device']}&notify_user=user&notify_data=your account has been blocked",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

// sms code


    redirect('user/viewuser/' . $id);
  }

  public function unblock($id) {
    $this->load->model('dashboard_model');
    $data = $this->model->s_data("device_token,device", "push_notification_user", ['id' => $id]);
    $this->db->update("users", ['webblock' => '1'], ['id' => $id]);
   //$user_data = $this->model->s_data("mobile","users",[''=>'']);
    
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => base_url('api/common/appnotify/'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "notify_type=unblock_user&device_id={$data[0]['device_token']}&device_type={$data[0]['device']}&notify_user=user&notify_data=your account has been blocked",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);


    curl_close($curl);
    
    
   redirect('user/viewuser/' . $id);
  }

  public function update_profileimg($id) {
    $photo = $_FILES['profileimg']['name'];
    if (!empty($photo)) {
      $this->upload->initialize($this->set_photoupload_options());
      //$this->upload->do_upload();
      if (!$this->upload->do_upload('profileimg')) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('user/viewuser/' . $id, 'refresh');
        //die();
      } else {
        $filedata = $this->upload->data();
        $path = 'assets/documents/' . $filedata['file_name'];
      }
    } else {
      $path = $this->input->post('old_profileimg');
    }
    $result = $this->user_model->update_profileimg($path, $id);
    if ($result <= 0) {
      $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
      redirect('user/viewuser/' . $id, 'refresh');
    } else {
      $this->session->set_flashdata('flashdata', 'Driver successfully updated.');
      redirect('user/viewuser/' . $id, 'refresh');
    }
  }

  public function delete_user($id) {
    $this->db->delete("users", ['id' => $id]);
    redirect('user/view_users');
  }

  public function transaction_details($id) {
    $data['head_title'] = 'View drivers';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $this->load->model('dashboard_model');
    $data['userlist'] = $this->dashboard_model->wallet_trans_details_user($id);
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/user/transaction_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  public function bulk_action($action){
		//echo $action;
		$userid_box = json_decode($_POST['userid_box']);
		if(!empty($userid_box)){
			if($action == 'activate'){
				$columns = array('status' => 1, 'verified' => 1);
			}
			if($action == 'deactivate'){
				$columns = array('status' => 0);
			}
			if($action == 'unblock'){
				$columns = ['webblock' => 1];
			}
			if($action == 'block'){
				$columns = ['webblock' => 0];
			}
			
			foreach($userid_box as $userid){

        //Fixed by Nandini
				$this->db->where('id', $userid);
		    $this->db->update('users', $columns); 
				//$this->db->update("users", $columns, ['id' => $userid]);
			}
			echo $this->db->last_query();die();
		}else{
			echo 0;die();
		}
	}

}
