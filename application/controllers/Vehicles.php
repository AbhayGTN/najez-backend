<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Vehicles extends CI_Controller {

  public function __construct() {
    // Call the Model constructor
    parent::__construct();
    if (!$this->session->userdata('loggedin')) {
      redirect('backlogin', 'refresh');
    }
    $this->load->model('user_model');
    $this->load->model('driver_model');
    $this->load->model('vehicle_model');
    $this->load->model('model');
  }

  public function index() {
    
  }

  public function add_vehicle() {

    $data['head_title'] = 'Add Vehicle';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['brand_list'] = $this->model->get_barnd_list();
    $data['driverlist'] = $this->driver_model->viewdrivers();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/vehicles/add-vehicle-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function addvehicle() {
    $this->form_validation->set_rules('vehicle_sno', 'Vehicle Sno', 'trim|required|max_length[50]|is_unique[vehicles.vehicle_sno]');
    $this->form_validation->set_rules('vehicle_plate_char', 'Vehicle Plate', 'trim|required|min_length[3]|max_length[3]');
    $this->form_validation->set_rules('vehicle_plate_num', 'Vehicle Plate', 'trim|required|min_length[4]|max_length[4]');
    $this->form_validation->set_rules('vehicle_type', 'Vehicle Type', 'trim|required');
    $this->form_validation->set_rules('car_model', 'Vehicle Model', 'trim|required');
    $this->form_validation->set_rules('vehicle_year', 'Vehicle Year', 'trim|required');
    $this->form_validation->set_rules('vehicle_color', 'Vehicle Color', 'trim|required');
    $this->form_validation->set_rules('driver', 'Driver', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Add Vehicle';
      $data['active'] = 'driver';
      $data['active_sub'] = 'view_driver';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $data['brand_list'] = $this->model->get_barnd_list();
      $data['driverlist'] = $this->driver_model->viewdrivers();
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/vehicles/add-vehicle-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $vehicle_sno = $this->input->post('vehicle_sno');
      $vehicle_plate_char = $this->input->post('vehicle_plate_char');
      $vehicle_plate_num = $this->input->post('vehicle_plate_num');
      $vehicle_plate = $vehicle_plate_char . '-' . $vehicle_plate_num;
      $vehicle_type = $this->input->post('vehicle_type');
      $car_model = $this->input->post('car_model');
	  $vehicle_type_data = $this->model->s_data("id","front_carlist",["car_model"=>$vehicle_type,"car_type"=>$car_model]);
	  $vehicle_type = $vehicle_type_data[0]['id'];
      $vehicle_year = $this->input->post('vehicle_year');
      $vehicle_color = $this->input->post('vehicle_color');
      $driver = $this->input->post('driver');
      $iban = $this->input->post('iban');
      $iban_bank = $this->input->post('iban_bank');

      $target_dir = "../../assets/document_uploaded/";
      $photofile = '';
      $path = '';
      $photograph = $_FILES['photograph'];
      if ($photograph["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('photograph')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $photofile = $path;
      }
      $residfile = '';
      $residence = $_FILES['residence'];
      if ($residence["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('residence')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $residfile = $path;
      }
      $drivrlicfile = '';
      $driver_license = $_FILES['driver_license'];
      if ($driver_license["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('driver_license')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $drivrlicfile = $path;
      }
      $vregfile = '';
      $vehicle_reg = $_FILES['vehicle_reg'];
      if ($vehicle_reg["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('vehicle_reg')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $vregfile = $path;
      }
      $vinsurfile = '';
      $vehicle_insur = $_FILES['vehicle_insur'];
      if ($vehicle_insur["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('vehicle_insur')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $vinsurfile = $path;
      }
      $authfile = '';
      $auth_img = $_FILES['auth_img'];
      if ($auth_img["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('auth_img')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $authfile = $path;
      }

      $tafweeth_img = $_FILES['tafweeth_img'];
      $taffile = '';
      if ($tafweeth_img["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('tafweeth_img')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $taffile = $path;
      }


      //$result = $this->vehicle_model->add_vehicle($driver, $vehicle_sno, $vehicle_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $photofile, $residfile, $drivrlicfile, $vregfile, $vinsurfile, $authfile, $iban, $iban_bank);
      $result = $this->vehicle_model->add_vehicle($driver, $vehicle_sno, $vehicle_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $iban, $iban_bank);

      /* if(isset($_POST['date_driverlicenseinput'])){
        $date_driverlicenseinput = $_POST['date_driverlicenseinput'];
        }else{
        $date_driverlicenseinput = '';
        }

        if(isset($_POST['date_vehiclereginput'])){
        $date_vehiclereginput = $_POST['date_vehiclereginput'];
        }else{
        $date_vehiclereginput = '';
        }

        if(isset($_POST['date_vehicleinsurinput'])){
        $date_vehicleinsurinput = $_POST['date_vehicleinsurinput'];
        }else{
        $date_vehicleinsurinput = '';
        }

        if(isset($_POST['date_tafweethimginput'])){
        $date_tafweethimginput = $_POST['date_tafweethimginput'];
        }else{
        $date_tafweethimginput = '';
        }

        $datavehicledocument = array(
        array(
        'driver_id' => $driver,
        'vehicle_id' => $result,
        'doc_key' => 'photograph',
        'doc_val' => $photofile,
        'doc_expire' => '',
        ),
        array(
        'driver_id' => $driver ,
        'vehicle_id' => $result ,
        'doc_key' => 'residence-identity',
        'doc_val' => $residfile,
        'doc_expire' => '',
        ),
        array(
        'driver_id' => $driver ,
        'vehicle_id' => $result,
        'doc_key' => 'driver-license',
        'doc_val' => $drivrlicfile,
        'doc_expire' => $date_driverlicenseinput,
        ),
        array(
        'driver_id' => $driver ,
        'vehicle_id' => $result,
        'doc_key' => 'vehicle-reg',
        'doc_val' => $vregfile,
        'doc_expire' => $date_vehiclereginput,
        ),
        array(
        'driver_id' => $driver ,
        'vehicle_id' =>$result,
        'doc_key' => 'vehicle-insur',
        'doc_val' => $vinsurfile,
        'doc_expire' =>$date_vehicleinsurinput,
        ),
        array(
        'driver_id' => $driver ,
        'vehicle_id' =>$result,
        'doc_key' => 'auth-img',
        'doc_val' => $authfile,
        'doc_expire' => '',
        ),
        array(
        'driver_id' => $driver ,
        'vehicle_id' =>$result,
        'doc_key' => 'tafweeth-img',
        'doc_val' => $taffile,
        'doc_expire' => $date_tafweethimginput,
        ),
        );

        $this->vehicledocuments_model->add_vehicledocument( $datavehicledocument); */

      if ($result < 0) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('vehicles/driver_vehicle/' . $driver, 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'Vehicle Successfully Added.');
        redirect('vehicles/driver_vehicle/' . $driver, 'refresh');
      }
    }
  }

  public function view_vehicles() {
    $data['head_title'] = 'View Vehicles';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['vehiclelist'] = $this->vehicle_model->viewvehicles();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/vehicles/vehicle-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function driver_vehicle($id) {
   

    //echo $id;die;
    $data['head_title'] = 'View Vehicle';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    
    $data['vehiclelist'] = $this->vehicle_model->get_drivervehicle($id);
    $data['driverid'] = $id;
    $data['vehiclelist'] = $this->vehicle_model->viewvehicles();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/vehicles/vehicle-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function view_vehicle($id) {
    $data['head_title'] = 'View Vehicle';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['vehicledata'] = $this->vehicle_model->viewvehicle($id);
    $data['vehicle_document'] = $this->vehicle_model->getvehicledoc($id);
    $data['brand_list'] = $this->model->get_barnd_list();
    $data['driverlist'] = $this->driver_model->viewdrivers();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/vehicles/single-vehicle-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function edit_vehicle($id) {
    $data['head_title'] = 'Edit Vehicle';
    $data['active'] = 'driver';
    $data['active_sub'] = 'view_driver';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['brand_list'] = $this->model->get_barnd_list();
    $data['driverlist'] = $this->driver_model->viewdrivers();
    $data['vehicle_document'] = $this->vehicle_model->getvehicledoc($id);
    $data['vehicledata'] = $this->vehicle_model->viewvehicle($id);
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/vehicles/edit-vehicle-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function updatevehicle($id) {
    $this->form_validation->set_rules('vehicle_sno', 'Vehicle Sno', 'trim|required|max_length[50]|is_unique[vehicles.vehicle_sno]');
    $this->form_validation->set_rules('vehicle_plate_char', 'Vehicle Plate', 'trim|required|min_length[3]|max_length[3]');
    $this->form_validation->set_rules('vehicle_plate_num', 'Vehicle Plate', 'trim|required|min_length[4]|max_length[4]');
    $this->form_validation->set_rules('vehicle_type', 'Vehicle Type', 'trim|required');
    $this->form_validation->set_rules('car_model', 'Vehicle Model', 'trim|required');
    $this->form_validation->set_rules('vehicle_year', 'Vehicle Year', 'trim|required');
    $this->form_validation->set_rules('vehicle_color', 'Vehicle Color', 'trim|required');
    $this->form_validation->set_rules('driver', 'Driver', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Edit Vehicle';
      $data['active'] = 'driver';
      $data['active_sub'] = 'view_driver';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $data['brand_list'] = $this->model->get_barnd_list();
      $data['driverlist'] = $this->driver_model->viewdrivers();
      $data['vehicledata'] = $this->vehicle_model->viewvehicle($id);
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/vehicles/edit-vehicle-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {

     // print"<pre>";print_R($this->input->post());die;
      $vehicle_sno = $this->input->post('vehicle_sno');
  
      $vehicle_plate_char = $this->input->post('vehicle_plate_char');
      $vehicle_plate_num = $this->input->post('vehicle_plate_num');
      $vehicle_plate = $vehicle_plate_char . '-' . $vehicle_plate_num;
      $vehicle_type = $this->input->post('vehicle_type');
      $car_model = $this->input->post('car_model');
      $vehicle_year = $this->input->post('vehicle_year');
   
      $vehicle_color = $this->input->post('vehicle_color');
      $driver = $this->input->post('driver');
      $iban = $this->input->post('iban');
      $iban_bank = $this->input->post('iban_bank');

      $target_dir = "../../assets/document_uploaded/";
      $photofile = '';
      $path = '';
      if ($photograph["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('photograph')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $photofile = $path;
      }
      $residfile = '';
      if ($residence["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('residence')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $residfile = $path;
      }
      $drivrlicfile = '';
      if ($driver_license["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('driver_license')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $drivrlicfile = $path;
      }
      $vregfile = '';
      if ($vehicle_reg["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('vehicle_reg')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $vregfile = $path;
      }
      $vinsurfile = '';
      if ($vehicle_insur["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('vehicle_insur')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $vinsurfile = $path;
      }
      $authfile = '';
      if ($auth_img["name"] != '') {
        $this->upload->initialize($this->set_photoupload_options());

        if ($this->upload->do_upload('auth_img')) {
          $filedata = $this->upload->data();
          $path = 'assets/document_uploaded/' . $filedata['file_name'];
        }
        $authfile = $path;
      }

      $result = $this->vehicle_model->update_vehicle($driver, $vehicle_sno, $vehicle_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $photofile, $residfile, $drivrlicfile, $vregfile, $vinsurfile, $authfile, $iban, $iban_bank, $id);
      if ($result < 0) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('vehicles/driver_vehicle/' . $id, 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'Vehicle Successfully Updated.');
        redirect('vehicles/driver_vehicle/' . $id, 'refresh');
      }
    }
  }

  private function set_photoupload_options() {
    //upload an image options
    $config = array();
    $config['upload_path'] = '././assets/documents/';
    $config['allowed_types'] = 'gif|jpg|png';
    //$config['max_size']      = '0';
    $config['overwrite'] = FALSE;

    return $config;
  }

  public function block($id, $vhicle_id) {
    
    $this->load->model('dashboard_model');
    $data = $this->model->s_data("device_token,device", "push_notification_driver", ['driver_id' => $id]);
    $vehicle = $this->model->s_data("id", "vehicles", ['driver_id' => $id]);
    $check_ride = $this->dashboard_model->check_vehicle_ride($vhicle_id);
    
    if ($check_ride >= 1) {
      $this->session->set_flashdata('cannot_block_user_msg', "This vehicle in ride please try after sometime");
      redirect('vehicles/driver_vehicle/' . $id);
      die();
    }
    
    $this->db->update("vehicles", ['active' => '0'], ['driver_id' => $id, 'id' => $vhicle_id]);
    
    $headers = array(
        'Content-Type: application/x-www-form-urlencoded'
    );
    $fields = ['notify_type' => 'block_user',
        'device_id' => $data[0]['device_token'],
        'device_type' => $data[0]['device'],
        'notify_user' => 'driver',
        'notify_data' => 'your account has been blocked'];
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => base_url('api/common/appnotify/'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "notify_type=vehicle_block&device_id={$data[0]['device_token']}&device_type={$data[0]['device']}&notify_user=driver&notify_data=your vehicle has been blocked&vehicle_id={$vehicle[0]['id']}",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    print_r($response);
    redirect('vehicles/driver_vehicle/' . $id);
  }

  public function unblock($id,$vhicle_id) {
    $this->load->model('dashboard_model', 'model');
    $data = $this->model->s_data("device_token,device", "push_notification_driver", ['driver_id' => $id]);
    $vehicle = $this->model->s_data("id", "vehicles", ['driver_id' => $id]);
    $this->db->update("vehicles", ['active' => '1'], ['driver_id' => $id, 'id' => $vhicle_id]);
    $headers = array(
        'Content-Type: application/x-www-form-urlencoded'
    );
    $fields = ['notify_type' => 'block_user',
        'device_id' => $data[0]['device_token'],
        'device_type' => $data[0]['device'],
        'notify_user' => 'driver',
        'notify_data' => 'your account has been blocked'];
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => base_url('api/common/appnotify/'),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "notify_type=vehicle_unblock&device_id={$data[0]['device_token']}&device_type={$data[0]['device']}&notify_user=driver&notify_data=your vehicle unblocked&vehicle_id={$vehicle[0]['id']}",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "postman-token: 883365ea-efb7-cddb-0351-09bb459e15bd"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    print_r($response);
    redirect('vehicles/driver_vehicle/' . $id);
  }

}
