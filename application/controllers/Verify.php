<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Verify extends CI_Controller {

  function __construct() {
    // Call the Model constructor
    parent::__construct();
    $this->load->model('user_model');
    $this->load->model('driver_model');
    $this->load->model('vehicle_model');
    $this->load->model('email_template_model');
  }

public function send($email,$message)
        {
          $this->email->from('noreply@najez-online.com', 'Najez');
          $this->email->to($email);
 	  $this->email->subject('Welcome');
          $this->email->message($message);
	  $this->email->set_mailtype("html");
	  $this->email->send();
          
        }
  
  public function activate($code) {

    $code = base64_decode($code);
    $codearr = explode('-', $code);
    $lang = $codearr[3];
    $userid = $codearr[2];
    $type = $codearr[1];
    $this->session->set_userdata('site_lang',$lang);
    
    
   
    if ($type == 'user') {
      $result = $this->user_model->checkuser_status($userid);
      
      if ($result[0]->status == 0 && $result != null) {
        $query = $this->user_model->setuser_status($userid);
        if ($query > 0) {
        $data = $this->user_model->viewuser($userid);
        
        $email_temp['email_temp'] = $this->email_template_model->tamplate_all_meta("123");
        $emaildata = $this->email_template_model->tamplate_all_meta("123");
        
        $message = $this->load->view('emailtemplate/user_register_email', $email_temp, true);
        $file = $emaildata[7]['meta_value_en'];
        
       // $this->send($data[0]->emailid, $message);
        
        $this->send_maildata($data,$message,$emaildata,$file);
		
        $this->session->set_flashdata('flashdata', 'Thankyou, your account successfully activated. We will keep you posted.');
         redirect('home');
      } else {
         redirect('home');
         }
      } else {
        redirect('home');
      }
    }
    if ($type == 'driver') {
      
      $result = $this->driver_model->checkdriver_status($userid);
      if ($result[0]->status == 0) {
        $query = $this->driver_model->setdriver_status($userid);
        $data = $this->driver_model->viewdriver($userid);
             
        $email_temp['referralcode'] = $data[0]->referralcode;
        $email_temp['email_temp'] = $this->email_template_model->tamplate_all_meta("124");
        $emaildata = $this->email_template_model->tamplate_all_meta("124");
        $message = $this->load->view('emailtemplate/driver_register_email', $email_temp, true);
       /* $this->load->library('email');
		$config = array(
			'mailtype' => 'html',
			'newline' => '\r\n',
			'charset' => 'utf-8' //default charset
		);
		$this->email->initialize($config);
        $this->email->from('noreply@najez-online.com', 'Najez');
          $this->email->to($data[0]->emailid);
 	  $this->email->subject('Welcome');
          $this->email->message($message);
	  $this->email->set_mailtype("html");
	  $this->email->send();*/
	  $file = $emaildata[4]['meta_value_en'];
			$this->send_maildata($data,$message,$emaildata,$file);

        //  print_r($data); die;
        $this->session->set_flashdata('flashdata', 'Thankyou, your account successfully activated. We will keep you posted.');
           
     
         
           redirect('home');
       
      } else {
        redirect('home');
        
      }
    }
  }
  public function send_maildata($data,$message,$emaildata,$filename){
	  $name = ucfirst($data[0]->fname);
		$to = $data[0]->emailid;
		$subject = "Welcome ".$name;
		$file = FCPATH.$filename;
		 $file_size = filesize($file);
		 $handle = fopen($file, "r");
		 $content = fread($handle, $file_size);
		 fclose($handle);
		 $content = chunk_split(base64_encode($content));
		//$message = $message;

		// Always set content-type when sending HTML email
		//$headers = "MIME-Version: 1.0" . "\r\n";
		//$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$uid = md5(uniqid(time()));
		$header = "MIME-Version: 1.0\r\n";
		 $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
		 $header .= "This is a multi-part message in MIME format.\r\n";
		 $header .= "--".$uid."\r\n";
		 $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
		 $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		 //$header .= $message."\r\n\r\n";
		 $header .= "--".$uid."\r\n";
		 $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
		 $header .= "Content-Transfer-Encoding: base64\r\n";
		 $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
		 $header .= $content."\r\n\r\n";
		 $header .= "--".$uid."--";

		// More headers
		$headers .= 'From: Najez<noreply@najez-online.com>' . "\r\n";

		mail($to,$subject,$message,$header);
  }

}
