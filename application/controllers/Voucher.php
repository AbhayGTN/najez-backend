<?php defined('BASEPATH') or  exit('No direct script access allowed');

class Voucher extends CI_Controller {

public function __construct() {
  parent::__construct();
  if($this->session->userdata('loggedin') ==  NULL)
  {
    redirect('backlogin');
  }
  $this->load->model('voucher_model');
  if(!in_array('6', $this->session->userdata('user_roles')))
    {
      die('you dont have permission to access this part');
    }
} 
  
  public function index()
  {
    
  }

 public function add_card() {
    $data['head_title'] = 'Add Card';
    $data['active'] = 'ratecard';
    $data['active_sub'] = 'add_card';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;
    $data['ratecard_list'] = $this->voucher_model->get_ratecardlist();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/voucher/add-card-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function update()
  {
    $data = $this->input->post(['card_code','price','start_date','end_date']);
    $this->voucher_model->update_card($data, $this->input->post('id'));
    redirect('voucher/view_cards');
  }

  public function addcard() {
    $this->form_validation->set_rules('card_code', 'Card Code', 'trim|required|min_length[6]|max_length[20]|is_unique[voucher.card_code]');
    $this->form_validation->set_rules('price', 'price', 'required|numeric');
    $this->form_validation->set_rules('start_date', 'Start Date', 'required');
    $this->form_validation->set_rules('end_date', 'End Date', 'required');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Add Card';
      $data['active'] = 'ratecard';
      $data['active_sub'] = 'add_card';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $userdata = $this->session->userdata('loggedin');
      $id = $userdata->id;
      $data['ratecard_list'] = $this->voucher_model->get_ratecardlist();
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/voucher/add-card-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $data = $this->input->post();
      $result = $this->voucher_model->add_ratecard($data);
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('voucher/add_card', 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'Card successfully added.');
        redirect('voucher/view_cards', 'refresh');
      }
    }
  }

  public function view_cards() {
    $data['head_title'] = 'View Vouchers';
    $data['active'] = 'ratecard';
    $data['active_sub'] = 'view_card';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['cardlist'] = $this->voucher_model->viewcards();
   
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/voucher/voucher_list_view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function viewcard($id) {
    $data['head_title'] = 'View Card';
    $data['active'] = 'ratecard';
    $data['active_sub'] = 'view_card';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['carddata'] = $this->voucher_model->viewcard($id);
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;

    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/voucher/single-card-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function edit_card($id) {
    $data['head_title'] = 'Edit Card';
    $data['active'] = 'ratecard';
    $data['active_sub'] = 'view_card';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    //$data['agentdata'] = $this->driver_model->viewdriver($id);
    $userdata = $this->session->userdata('loggedin');
    $id = $userdata->id;

    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/driver/edit-driver-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function hide($id) {
    if ($id != '') {
      $result = $this->voucher_model->hidecard($id);
      echo $result;
    } else {
      echo '0';
    }
  }

  public function hidelist($id) {
    if ($id != '') {
      $result = $this->voucher_model->hidelist($id);
      echo $result;
    } else {
      echo '0';
    }
  }

  public function pricelist() {
    $data['head_title'] = 'Price list';
    $data['active'] = 'ratecard';
    $data['active_sub'] = 'pricelist';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    $data['ratecard_list'] = $this->voucher_model->get_ratecardlist();
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/voucher/ratecard-pricelist-view', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }

  public function addpricelist() {
    $this->form_validation->set_rules('price', 'price', 'trim|required|max_length[5]');
    $this->form_validation->set_rules('rides', 'Rides', 'trim|required|max_length[5]');
    if ($this->form_validation->run() == FALSE) {
      $data['head_title'] = 'Price list';
      $data['active'] = 'ratecard';
      $data['active_sub'] = 'pricelist';
      $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
      $data['userdata'] = $this->session->userdata('loggedin');
      $data['ratecard_list'] = $this->voucher_model->get_ratecardlist();
      $this->load->view('template/head', $data);
      $this->load->view('template/header', $data);
      $this->load->view('template/sidenav', $data);
      $this->load->view('backend/voucher/ratecard-pricelist-view', $data);
      $this->load->view('template/footer', $data);
      $this->load->view('template/foot', $data);
    } else {
      $data = $this->input->post();
      $result = $this->voucher_model->add_pricelist($data);
      if ($result != 1) {
        $this->session->set_flashdata('flashdata', 'Something went wrong, please try again .');
        redirect('voucher/pricelist', 'refresh');
      } else {
        $this->session->set_flashdata('flashdata', 'Price successfully added.');
        redirect('voucher/pricelist', 'refresh');
      }
    }
  }
  
  public function delete($id,$vehicle_id)
  {
    $this->db->delete("vehicles", ['id'=>$id]);
    redirect('vehicles/driver_vehicle/'.$id);
  }

}
