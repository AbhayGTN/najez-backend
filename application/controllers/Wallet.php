<?php defined('BASEPATH') or exit('.');

class Wallet extends CI_Controller{
  
  public function __construct() {
    parent::__construct();
    
  }
  
  public function driver_transactions()
  {
    $data['head_title'] = 'wallet';
    $data['active'] = 'wallet';
    $data['active_sub'] = 'driver_trans';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/wallet/driver_transactions', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data);
  }
  
  public function customer_transactions()
  {
   $data['head_title'] = 'wallet';
    $data['active'] = 'wallet';
    $data['active_sub'] = 'customer_trans';
    $data['body_class'] = 'hold-transition skin-blue sidebar-mini';
    $data['userdata'] = $this->session->userdata('loggedin');
    
    $this->load->view('template/head', $data);
    $this->load->view('template/header', $data);
    $this->load->view('template/sidenav', $data);
    $this->load->view('backend/wallet/customer_transactions', $data);
    $this->load->view('template/footer', $data);
    $this->load->view('template/foot', $data); 
    
  }
  
  
  
  
}