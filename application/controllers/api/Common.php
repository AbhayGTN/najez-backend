<?php
defined('BASEPATH') or exit('.');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Common extends REST_Controller {

  function __construct() {
    // Call the Model constructor
    parent::__construct();

    $this->load->model('restapi_model');
    $this->load->model('user_model');
    $this->load->model('driver_model');
    $this->load->model('model');
	$this->load->library('notifylib');
	
    //$this->load->model('case_model');
  }

  public function getpage_post() {
    $user_data = array();
    $page = $this->post('page');
    $lang = $this->post('lang');

    /*$check_page = $this->restapi_model->check_page($page, $lang);
    if ($check_page != null) {

      $output_data['data'] = $check_page[0];
      $output_data['message'] = 'Success';
      $output_data['status'] = 1;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Page not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }*/
	if($page=='user-faq'){
		if($lang=='ar'){
			$output_data['data'] = $this->load->view('app-pages/faq-customer-arabic','',true);
			  $output_data['message'] = 'Success';
			  $output_data['status'] = 1;
			  $this->response($output_data);
			  return;
		}else{
			$output_data['data'] = $this->load->view('app-pages/faq-customer-english','',true);
			  $output_data['message'] = 'Success';
			  $output_data['status'] = 1;
			  $this->response($output_data);
			  return;
		}
	}
	elseif($page=='driver-faq'){
		if($lang=='ar'){
			$output_data['data'] = $this->load->view('app-pages/faq-driver-arabic','',true);
			  $output_data['message'] = 'Success';
			  $output_data['status'] = 1;
			  $this->response($output_data);
			  return;
		}else{
			$output_data['data'] = $this->load->view('app-pages/faq-driver-english','',true);
			  $output_data['message'] = 'Success';
			  $output_data['status'] = 1;
			  $this->response($output_data);
			  return;
		}
	}
	elseif($page=='aboutus'){
		if($lang=='ar'){
			$output_data['data'] = $this->load->view('app-pages/aboutus-arabic','',true);
			  $output_data['message'] = 'Success';
			  $output_data['status'] = 1;
			  $this->response($output_data);
			  return;
		}else{
			$output_data['data'] = $this->load->view('app-pages/aboutus-english','',true);
			  $output_data['message'] = 'Success';
			  $output_data['status'] = 1;
			  $this->response($output_data);
			  return;
		}
	}
	elseif($page=='help'){
		if($lang=='ar'){
			$output_data['data'] = $this->load->view('app-pages/help-arabic','',true);
			  $output_data['message'] = 'Success';
			  $output_data['status'] = 1;
			  $this->response($output_data);
			  return;
		}else{
			$output_data['data'] = $this->load->view('app-pages/help-english','',true);
			  $output_data['message'] = 'Success';
			  $output_data['status'] = 1;
			  $this->response($output_data);
			  return;
		}
	}
	 $lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('detailnotvalid','common',$lang);
    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
  }

  public function city_list_get() {
    $data = $this->model->g_data("city_master", "city_name_" . $this->get('lang') . " as city_name,id");
    $this->response(['status' => '1', 'message' => 'found', 'data' => $data]);
  }

  public function bank_list_get() {
    $data = $this->model->g_data("bank_list", "bank_name,id");
    $this->response(['status' => '1', 'message' => 'found', 'data' => $data]);
  }

  public function country_code_list_get() {
    $data = $this->model->s_data("name,phonecode", "master_country_code");
    $this->response(['status' => '1', 'data' => $data]);
  }
public function support_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $support_data = array();
        $support_data['userid'] = $userid;
        $support_data['name'] = $this->post('name');
        $support_data['emailid'] = $this->post('emailid');
        $support_data['comment'] = $this->post('comment');
        //$support_data['option_type'] = $this->post('option_type');
        $support_data['user_type'] = $this->post('user_type');
        $photo = $this->input->post('photo');

        if (!empty($photo)) {
          $data = str_replace('data:image/png;base64,', '', $photo);
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $photo = "assets/documents/" . $filename;
          } else {
            $photo = "assets/documents/user-icon.png";
          }
        } else {
          $photo = "assets/documents/user-icon.png";
        }

        $support_data['photo'] = $photo;
        $result = $this->restapi_model->add_supportticket($support_data);
        if ($result != null) {

          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }
  
  public function validate_otp_post() {

    $data = $this->model->valid_otp($this->input->post('mobile'), $this->input->post('otp'));
    if (!empty($data)) {
      $this->model->u_data("sms_otp_temp", ['valid' => '1'], ['mobile' => $data[0]['mobile'], 'otp' => $data[0]['otp']]);

      $this->response(['status' => '1', 'message' => 'OTP Matched Successfully.']);
    } else {
      $this->response(['status' => '0', 'message' => 'OTP Not Matched, Please Generate New.']);
    }
  }
  
   public function send_otp_post() {

    $data = $this->input->post(['mobile', 'type']);
	if($data['type']=='user'){
		$mobile_count = $this->restapi_model->checkmobile_user($data['mobile']);
	}else{
		$mobile_count = $this->restapi_model->checkmobile_driver($data['mobile']);
	}
	if($mobile_count > 0){
		$curl = curl_init();
	   // $otp = rand(1111, 9999);
		$otp = 1234;
		$message = "Welcome to najez your OTP is ";
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://oursms.net/api/sendsms.php",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}{$otp}&numbers={$data['mobile']}&sender=NAJEZO&return=json",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/x-www-form-urlencoded",
				"postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
			),
		));
		$this->model->i_data("sms_otp_temp", ['mobile' => $data['mobile'], 'otp' => $otp]);
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);


		if ($err) {
		  $output_data['message'] = 'Something went wrong';
			$output_data['status'] = 0;
			$this->response($output_data);
			return;
		} else {
		  $output_data['message'] = 'Success';
			$output_data['status'] = 1;
			$this->response($output_data);
			return;
		}
	}else{
		$output_data['message'] = 'Invalid number';
		$output_data['status'] = 0;
		$this->response($output_data);
		return;
	}
  }
  
  public function supportoption_post() {

    $table = $this->post("type");
    $lang = $this->post("lang");
    $result = $this->restapi_model->get_supportdata($table, $lang);
    $output_data['options'] = $result;
    $output_data['message'] = 'Success';
    $output_data['status'] = 1;
    $this->response($output_data);
    return;
  }

  public function appnotify_post() {
	 // print_r($this->post());die();
	  $notify_type = $this->post('notify_type'); 
	  if($notify_type == 1){
		  
		$ride_data['ride_id'] = $this->post('ride_id');
		$ride_data['notify_type'] = 'accept_ride';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

	
		$msg_payload = array(
			'mtitle' => "Ride Notification",
			'mdesc' => "New Ride Notification",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
    
	  }
	  elseif($notify_type == 2){
		  
		$ride_data['ride_id'] = $this->post('ride_id');
		$ride_data['notify_type'] = 'complete_ride';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Ride Notification",
			'mdesc' => "Total Fare Estimate",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }
	  elseif($notify_type == 'scheduled_ride'){
		  
		$ride_data['ride_id'] = $this->post('ride_id');
		$ride_data['notify_type'] = 'scheduled_ride';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Ride Notification",
			'mdesc' => "New Ride Notification",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'transfer_money'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'transfer_money';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "Money Transfer",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'referal_amount'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'referal_amount';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "Referral Notification",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'transaction_complete'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'transaction_complete';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "Ride Transaction",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'block_user'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'block_user';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "You are Blocked",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'unblock_user'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'unblock_user';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "You are Unblocked",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'deact_user'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'deact_user';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "You are Deactived",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'activate_user'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'activate_user';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "You are Actived",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'vehicle_block'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'vehicle_block';
		$ride_data['vehicle_id'] = $this->post('vehicle_id');
		$ride_data['notify_type'] = 'vehicle_block';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "Your vehicle is blocked",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'vehicle_unblock'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'vehical_unblock';
		$ride_data['vehicle_id'] = $this->post('vehicle_id');
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "Your vehicle is unblocked",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }elseif($notify_type == 'user_cancel_ride'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'user_cancel_ride';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "Ride Cancelled",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }
	  elseif($notify_type == 'driver_cancel_ride'){
		$ride_data['notify_data'] = $this->post('notify_data');
		$ride_data['notify_type'] = 'driver_cancel_ride';
		$device_id = $this->post('device_id');
		$device_type = $this->post('device_type');
		$notify_user = $this->post('notify_user');
		//$ride_data['ride_data'] = $this->post('ride_data');

		
		$msg_payload = array(
			'mtitle' => "Najez",
			'mdesc' => "Ride Cancelled",
			'ride_data' => $ride_data,
		);
		
		if($device_type=='ios'){
		  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
		}
		if($device_type=='android'){
		  $this->notifylib->android($msg_payload, $device_id, $notify_user);
		}
	  }else{
		  $output_data['message'] = 'Error';          
					$output_data['status'] = 0;          
					$this->response($output_data);          
					return;
	  }
	  
  }  
  public function smsnotify_post(){
	  $mobile = $this->post('mobile');
	  $message = $this->post('message');
	  $curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://oursms.net/api/sendsms.php",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}&numbers={$mobile}&sender=NAJEZO&return=json",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/x-www-form-urlencoded",
				"postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if($err){
			$output_data['message'] = 'Error';        
			$output_data['status'] = 0;        
			$this->response($output_data);        
			return;
		}else{
			$output_data['message'] = 'Success';        
			$output_data['status'] = 1;        
			$this->response($output_data);        
			return;
		}
  }
	public function saveusercard_post() {    
		$headers = $this->input->request_headers();    
		if (Authorization::tokenIsExist($headers)) {      
			$token = Authorization::validateToken($headers['Authorization']);      
			if ($token != false) {  
				$card_data = array(); 			
				$card_data['userid'] = $token->id;        
				       
				$card_data['card_number'] = $this->post('card_number');        
				$card_data['expire_month'] = $this->post('expire_month');       
				$card_data['expire_year'] = $this->post('expire_year');        
				$card_data['card_type'] = $this->post('card_type');        
				$card_data['card_username'] = $this->post('card_username');        
				//$card_data['card_cvv'] = $this->post('card_cvv');        
				$card_data['type'] = $this->post('type');
				//print_r($card_data);
				$getcard = $this->model->s_data("id", "card_detail", ['card_number' => $card_data['card_number']]);
				//print_r($getcard);die();
				if($getcard == null){
					//$result = $this->restapi_model->saveusercard($userid, $card_data,$type);    
						$this->model->i_data("card_detail", $card_data);
					//if ($result) {          
						$output_data['message'] = 'Success';          
						$output_data['status'] = 1;          
						$this->response($output_data);          
						return;        
					//} 
				}else{
					$output_data['message'] = 'Card Already Registered.';        
					$output_data['status'] = 0;        
					$this->response($output_data);        
					return; 
				}
				       
				$output_data['message'] = 'Error';        
				$output_data['status'] = 0;        
				$this->response($output_data);        
				return;      
			}      
			$response = [          
				'status' => REST_Controller::HTTP_UNAUTHORIZED,          
				'message' => 'Unauthorized',      
			];      
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);      
			return;    
		}    
		$response = [        
			'status' => REST_Controller::HTTP_UNAUTHORIZED,        
			'message' => 'Unauthorized',    
		];    
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);    
		return;  
	}  
	public function get_user_card_post() 
	{    
		$headers = $this->input->request_headers();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$userid = $token->id;
				$type = $this->post('type');
				$data = $this->model->s_data("id as card_id,card_number,expire_month,expire_year,card_type,card_username", "card_detail", ['userid' => $userid,'type'=>$type]);
				/*$data2 = array();
				foreach ($data as $key=>$value) {

						$data2[$key] = json_decode($value['card_detail']);
						$data2[$key]->card_id = $value['id'];
						
				}*/
				
				//print_r($data2);die();
				$this->response(['status' => 1, 'data' => $data]);
				return;
			}
			$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
		}
		$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
	}
	public function remove_savedcard_post() 
	{    
		$headers = $this->input->request_headers();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$userid = $token->id;
				//echo $userid;die();
				$type = $this->post('type');
				$cardid = $this->post('card_id');
				
				$result = $this->model->d_data("card_detail", ['userid' => $userid,'type'=>$type,'id'=>$cardid]);
				if($result !=null){ 
					$this->response(['status' => 1, 'message' => 'success']);
					return;
				}else{
					$this->response(['status' => 0, 'message' => 'error']);
					return;
				}
			}
			$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
		}
		$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
	}
	public function getridedata_post(){
		$headers = $this->input->request_headers();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$userid = $token->id;
				$ride_id = $this->post('ride_id');
				$type = $this->post('type');
				
				$ride_data = $this->model->s_data("userid,driverid,trip_id,vehicle_type,vehicle_id,pickup_location,drop_location,GPS_Starting_Point,GPS_Destination_Point,ride_flag,delivery_img, delivery_comment", "rides", ['id' => $ride_id]);
				if($ride_data == null){
					$this->response(['status' => 0, 'message' => 'Invalid Trip']);
					return;
				}else{
					
					if($type=='user'){
						$idcount = $this->model->s_data("count(id) as idcount","driver_ride_action",["rideid"=>$ride_id,"driverid"=>$userid]);
						if($idcount[0]['idcount'] > 0){
							$this->response(['status' => 0, 'message' => 'Invalid Trip']);
							return;
						}
						
						$userid = $ride_data[0]['userid'];
						$userdata = $this->model->s_data("fname,chatid,mobile,show_phone", "users", ['id' => $userid]);
						$ride_data[0]['userdata'] = $userdata[0];
						if($userdata[0]['show_phone'] == 0){
							$ride_data[0]['userdata']['mobile']  = '';
						}
						$ride_data[0]['userdata']['rating'] = round($this->restapi_model->get_user_rating($userid));
					}
					if($type=='driver'){
						$driverid = $ride_data[0]['driverid'];
						$driverdata = $this->model->s_data("fname,chatid,photo,mobile", "drivers", ['id' => $driverid]);
						$ride_data[0]['driverdata'] = $driverdata[0];
						$ride_data[0]['driverdata']['rating'] = round($this->restapi_model->get_driver_rating($driverid));
						$ride_data[0]['driverdata']['photo'] = base_url().$ride_data[0]['driverdata']['photo'];
						if($ride_data[0]['vehicle_id']!='' || $ride_data[0]['vehicle_id']!=null){
							$vehicledata = $this->model->q_data("SELECT  t1.vehicle_color,t1.vehicle_number,fcl.car_brand,fcl.car_model,fcl.car_description FROM vehicles as t1, front_carlist as fcl WHERE t1.id = ".$ride_data[0]['vehicle_id']." AND fcl.id = t1.vehicle_model;");
							$vehicledata[0]['vehicle_model'] = $vehicledata[0]['car_brand'].' '.$vehicledata[0]['car_model'];
							$vehicledata[0]['vehicle_logo'] = base_url().$vehicledata[0]['car_description'];
							unset($vehicledata[0]['car_brand']);
							unset($vehicledata[0]['car_model']);
							unset($vehicledata[0]['car_description']);
							//print_r($vehicledata);die();
							$ride_data[0]['vehicledata'] = $vehicledata[0];
						}
					}
					
					//added by Raman
					if($ride_data[0]['vehicle_type'] == 4){
						$isDelivery = 1;
					}else{
						$isDelivery = 0;
					}
					$GPS_Starting_Point = explode(',',$ride_data[0]['GPS_Starting_Point']);
					$ride_data[0]['pickup_lat'] = $GPS_Starting_Point[0];
					$ride_data[0]['pickup_lng'] = $GPS_Starting_Point[1];
					unset($ride_data[0]['GPS_Starting_Point']);
					$GPS_Destination_Point = explode(',',$ride_data[0]['GPS_Destination_Point']);
					$ride_data[0]['drop_lat'] = $GPS_Destination_Point[0];
					$ride_data[0]['drop_lng'] = $GPS_Destination_Point[1];
					unset($ride_data[0]['GPS_Destination_Point']);
					
					
					$trip_id = $this->model->s_data("trip_type_name", "trip_type_master", ['id' =>$ride_data[0]['trip_id']]);
					unset($ride_data[0]['trip_id']);
					$ride_data[0]['trip_type'] = $trip_id[0]['trip_type_name'];
					$vehicle_type = $this->model->s_data("display_name", "vehicle_family", ['id' => $ride_data[0]['vehicle_type']]);
					//unset($ride_data[0]['vehicle_type']);
					$ride_data[0]['vehicle_type'] = $vehicle_type[0]['display_name'];
					if($ride_data[0]['delivery_img']!=null || $ride_data[0]['delivery_img']!=''){
						$ride_data[0]['delivery_img'] = base_url().$ride_data[0]['delivery_img'];
					}
					
					$ride_data[0]['isDelivery'] = $isDelivery;
					$this->response(['status' => 1, 'message' => 'success','ride_data'=>$ride_data[0]]);
					return;
				}
			}
			$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
		}
		$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
	}
	public function pricelist_parameters_get() 
	{    
		
		/*$result = $this->model->q_data("select t.trip_type_name 'trip_type', t.id 'trip_id', v.id 'vehicle_id', v.display_name 'vehicle_name' from trip_type_master t, trip_type_vehicle_family tvf, vehicle_family v where tvf.trip_type_id = t.id and tvf.vehicle_family_id = v.id");
		$this->response(['status' => 1, 'message' => 'success','pricelist_parameters'=>$result]);
		return;*/
		$trip_type = $this->model->s_data("id,trip_type_name", "trip_type_master",[]);
		foreach($trip_type as $key => $trip_data){
			$result = $this->model->q_data("select vf.display_name 'vehicle_family', vf.id 'vehicle_family_id' from trip_type_vehicle_family tvf, vehicle_family vf where tvf.trip_type_id = ".$trip_data['id']." and tvf.vehicle_family_id = vf.id");
			$trip_type[$key]['data'] = $result;
		}
		$this->response(['status' => 1, 'message' => 'success','pricelist_parameters'=>$trip_type]);
		return;
	}
	public function get_pricelist_post(){
		$trip_id = $this->post('trip_id');
		$vehicle_id = $this->post('vehicle_id');
		//$result = $this->model->s_data("charge_type,charges", "fare_ratecard", ['trip_type' =>$trip_id,'vehicle_type'=>$vehicle_id]);
		$result = $this->model->q_data("select c.ChargeName 'chargetype', f.charges 'value' from fare_ratecard f, master_charge_type c where f.charge_type = c.id and f.trip_type = ".$trip_id." and f.vehicle_type= ".$vehicle_id );
		$charge_data = array('status' => 1, 'message' => 'success');
		foreach($result as $charge){
			$charge_data[$charge['chargetype']]= $charge['value'];
		}
		//$charge_data['arp']= "25";
		$this->response($charge_data);
		return;
	}
	public function opt_lang_post(){
		$headers = $this->input->request_headers();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$userid = $token->id;
				$lang = $this->post('lang');
				if($lang == ''){
					$lang = 'en';
				}
				$type = $this->post('type');
				$check = $this->model->s_data("userid", "opt_language", ['userid' => $userid,'type'=>$type]);
				if($check != null){
					$this->model->u_data("opt_language", ['lang' => $lang], ['userid' => $userid,'type'=>$type]);
					$this->response(['status' => 1, 'message' => 'success']);
					return;
				}else{
					$this->model->i_data("opt_language", ['lang' => $lang, 'userid' => $userid, 'type'=>$type]);
					$this->response(['status' => 1, 'message' => 'success']);
					return;
				}
				$this->response(['status' => 0, 'message' => 'error']);
				return;
			}
			$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
		}
		$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
	}
	public function chatid_post(){
		$headers = $this->input->request_headers();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$userid = $token->id;
				$chatid = $this->post('chatid');
				$type = $this->post('type');
				if($type == 'user'){
					$chatemail = $this->post('chatemail');
					$this->model->u_data("users", ['chatid' => $chatid, 'chatemail'=>$chatemail], ['id' => $userid]);
					$this->response(['status' => 1, 'message' => 'success']);
					return;
				}
				if($type == 'driver'){
					$this->model->u_data("drivers", ['chatid' => $chatid], ['id' => $userid]);
					$this->response(['status' => 1, 'message' => 'success']);
					return;
				}
			}
			$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
		}
		$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
	}
	public function upsetting_post(){
		$headers = $this->input->request_headers();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$userid = $token->id;
				$type = $this->post('type');
				$show_phone = $this->post('show_phone');
				
				if($type == 'user'){
					
					$this->model->u_data("users", ['show_phone' => $show_phone], ['id' => $userid]);
					$this->response(['status' => 1, 'message' => 'success']);
					return;
				}
				if($type == 'driver'){
					
					//$this->model->u_data("drivers", ['chatid' => $chatid], ['id' => $userid]);
					$this->response(['status' => 1, 'message' => 'success']);
					return;
				}
			}
			$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
		}
		$this->response(['status' => REST_Controller::HTTP_UNAUTHORIZED, 'message' => 'Unauthorized']);
	}
	public function colorlist_get(){
		$colorlist = array('Red','Silver','White','Black','Brown');
		$this->response(['status' => 1, 'colorlist' => $colorlist]);
		return;
	}
}
