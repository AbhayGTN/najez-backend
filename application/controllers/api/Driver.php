<?php defined('BASEPATH') or exit('.');
//ini_set("display_errors","on");

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
class Driver extends REST_Controller {

  public function __construct() {
    // Call the Model constructor
    parent::__construct();

    $this->load->model('restapi_model');
    $this->load->model('user_model');
    $this->load->model('driver_model');
	$this->load->library('notifylib');
    $this->load->model('model');
  }

  public function driver_register_step2_post() {
    $input = $this->input->post(['mobile', 'otp', 'device', 'device_token','lang','device_model','device_os']);
    if(empty($input['device_token']))
    {
		$lang = $input['lang'];
	  $reponse_msg = $this->notifylib->getresponse('empty_deviceid','common',$lang);
      $this->response(['status'=>0,'message'=>$reponse_msg]);
      return;
    }
    
    $data = $this->model->valid_otp($input['mobile'], $input['otp']);
	//$this->response(['status' => $data);
	//die();
    if(empty($data)) {
		$lang = $input['lang'];
	  $reponse_msg = $this->notifylib->getresponse('opt_not_match','common',$lang);
      $this->response(['status' => '0', 'message' => $reponse_msg]);
    } else {
      $data2 = $this->driver_model->step2_driver($input['mobile']);
      $tokendata['id'] = $data2[0]['id'];
      
      $input['userid'] = $data2[0]['id'];
      $player_id = $this->getPlayerId($input);
      
      @$this->model->i_data("push_notification_driver", ['device' => $input['device'], 'device_token' => $input['device_token'], 'driver_id' => $data2[0]['id'], 'player_id' => $player_id]);
      $vehicle = $this->restapi_model->checkdriver_vehiclereg($data2[0]['id']);
      $vehicle_count = $vehicle;
      $driver_rating = $this->restapi_model->get_driver_rating($data2[0]['id']);
      $data2[0]->rating = round($driver_rating);
      $datass = $data2[0];
	  $lang = $input['lang'];
	  $reponse_msg = $this->notifylib->getresponse('loggedin','common',$lang);
      $this->response(['status' => '1', 'message' => $reponse_msg, 'vehicle_count' => $vehicle_count, 'token' => Authorization::generateToken($tokendata), 'driverdata' => $datass]);
    }
  }

  public function driverregister_post() {

    $can_deliver = $this->post('can_deliver');
    $username = $this->post('fname');
    $emailid = $this->post('emailid');
    $first_name = $this->post('fname');
    $middle_name = $this->post('mname');
    $last_name = $this->post('lname');
    $password = $this->post('password');
    $photo = $this->post('photo');
    $gender = $this->post('gender');
    $mobile = $this->post('mobile');
    $city = $this->post('city');
    $iban = $this->post('iban');
    $bank = $this->post('bank');
    $dob = $this->post('dob');
    $iqama = $this->post('iqama');
    $resident = $this->post('resident');
    $driving_lic = $this->post('driving_lic');
    $city = $this->post('city');
    $lat = $this->post('lat');
    $lng = $this->post('lng');
    $lang = $this->post('lang');
    $latlng = json_encode(array($lat, $lng));
    $referralcode = $this->post('referralcode');
    $status = 1;
    $email_count = $this->restapi_model->checkemailid_driver($emailid);
    if ($email_count > 0) {
		//$lang = 'en';
	  $reponse_msg = $this->notifylib->getresponse('emailid_exists','common',$lang);
      $output_data['message'] = $reponse_msg;
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
	
	$iqama_count = $this->model->s_data("count(id) as iqama_count", "drivers", ['iqama'=>$iqama]);
    if ($iqama_count[0]['iqama_count'] > 0) {
		//$lang = 'en';
	  $reponse_msg = $this->notifylib->getresponse('iqama_exists','driver',$lang);
      $output_data['message'] = $reponse_msg;
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
	
    $mobile_count = 0;
    if ($mobile != '') {
      $mobile_count = $this->restapi_model->checkmobile_driver($mobile);
    }

    if ($mobile_count > 0) {
		//$lang = 'en';
	  $reponse_msg = $this->notifylib->getresponse('mobile_exists','common',$lang);
      $output_data['message'] = $reponse_msg;
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    if ($email_count == 0 && $mobile_count == 0) {






      $curl = curl_init();
     // $otp = rand(1111, 9999);
      $otp = 1234;
	 // $lang = 'en';
	  $reponse_msg = $this->notifylib->getresponse('opt_sms','common',$lang);
      $message = $reponse_msg;
      curl_setopt_array($curl, array(
          CURLOPT_URL => "http://oursms.net/api/sendsms.php",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}{$otp}&numbers={$mobile}&sender=NAJEZO&return=json",
          CURLOPT_HTTPHEADER => array(
              "cache-control: no-cache",
              "content-type: application/x-www-form-urlencoded",
              "postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
          ),
      ));
      $this->model->i_data("sms_otp_temp", ['mobile' => $mobile, 'otp' => $otp]);
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);


      if (!empty($photo)) {
        $data = str_replace('data:image/png;base64,', '', $photo);
        $encodedData = str_replace(' ', '+', $data);
        $decodedData = base64_decode($encodedData);
        $filename = uniqid() . '.jpg';
        file_put_contents('././assets/documents/' . $filename, $decodedData);
        if (file_exists("././assets/documents/" . $filename)) {
          $photo = "assets/documents/" . $filename;
        } else {
          $photo = "assets/documents/user-icon.png";
        }
      } else {
        $photo = "assets/documents/user-icon.png";
      }
	  
	   if (!empty($resident)) {
        $data = str_replace('data:image/png;base64,', '', $resident);
        $encodedData = str_replace(' ', '+', $data);
        $decodedData = base64_decode($encodedData);
        $filename = uniqid() . '.jpg';
        file_put_contents('././assets/documents/' . $filename, $decodedData);
        if (file_exists("././assets/documents/" . $filename)) {
          $resident = "assets/documents/" . $filename;
        } else {
          $resident = '';
        }
      }else{
		  $resident = '';
	  }
	   if (!empty($driving_lic)) {
        $data = str_replace('data:image/png;base64,', '', $driving_lic);
        $encodedData = str_replace(' ', '+', $data);
        $decodedData = base64_decode($encodedData);
        $filename = uniqid() . '.jpg';
        file_put_contents('././assets/documents/' . $filename, $decodedData);
        if (file_exists("././assets/documents/" . $filename)) {
          $driving_lic = "assets/documents/" . $filename;
        } else {
          $driving_lic = '';
        }
      }else{
		  $driving_lic = '';
	  }
	  
      if ($referralcode != '') {
        $reffer_count = $this->restapi_model->checkreferralcode_driver($referralcode);
        if ($reffer_count > 0) {
          $total_amount = 0;
        } else {
          $total_amount = 0;
        }
      } else {
        $total_amount = 0;
      }
      //$user_referral = $username . rand();
	  $split_name = str_split($username, 3);
		$user_referral = strtoupper($split_name[0]) . rand(1111, 9999);
      $data = $this->restapi_model->add_driver($username, $emailid, $first_name,$middle_name,$last_name, $password, $photo, $mobile, $city, $can_deliver, $gender, $status, $latlng, $total_amount, $user_referral,$iban,$bank,$dob,$resident,$driving_lic,$iqama,$referralcode);
      //print_r($data);die();
      if (!empty($data) && $data > 0) {
        $user_data = $this->restapi_model->get_driver($data);
        if (!empty($user_data)) {
          $driver_id = $user_data[0]->id;
          $emailid = $user_data[0]->emailid;
         // $this->senddrivermail($driver_id, $emailid);

          //$tokenData['id'] = $user_data[0]->id;
		  $tokenData = array();
          $tokenData['id'] = $user_data[0]->id;
		  
		  /*$output_data['message'] = $tokenData['id'];
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
		  */
		  
          $output_data['token'] = Authorization::generateToken($tokenData);
          $driver_rating = $this->restapi_model->get_driver_rating($user_data[0]->id);
          $user_data[0]->rating = round($driver_rating);
          $output_data['driverdata'] = $user_data[0];
          $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($user_data[0]->id);
          //$vehicle_doc_count = $this->restapi_model->checkdriver_vehicledoc($user_data[0]->id);
          if ($vehicle_count > 0) {
            $output_data['vehicle_count'] = 1;
          } else {
            $output_data['vehicle_count'] = 0;
            //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
            $typedata = $this->restapi_model->get_vehicletype();
            //print_r($typedata);die();
            foreach ($typedata as $type) {
              $typename = $type->type;

              $type->list = $this->restapi_model->getcar_list($typename);
              //print_r($type);die();
            }
            $output_data['vehicle_list'] = $typedata;
          }
		  //$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('loggedin','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
			//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('errregister','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      } else {
		 // $lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('errregister','common',$lang);
        $output_data['message'] = $reponse_msg;
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    } else {
		//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('errregister','common',$lang);
      $output_data['message'] = $reponse_msg;
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverlogin_post() {
    $dataPost = $this->input->post();
    $driver = $this->user_model->driverlogin($dataPost['mobile'], $dataPost['password']);
	//print_r($driver);die();
    if ($driver->verified == "0") {
      $tokenData['id'] = $driver->id;
      $response['token'] = Authorization::generateToken($tokenData);
      $driver_rating = $this->restapi_model->get_driver_rating($driver->id);
      $driver->rating = round($driver_rating);
      $response['driverdata'] = $driver;
      $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($driver->id);
      $data = $this->model->registerd_car($driver->id);
      $response['vehicle_count'] = count($data);
	  $lang = $dataPost['lang'];
		$reponse_msg = $this->notifylib->getresponse('loggedin','common',$lang);
      $response['message'] = $reponse_msg;
      $response['message'] = $reponse_msg;
      $response['status'] = 1;
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    }



    if ($driver != null) {
      $tokenData = array();
      $tokenData['id'] = $driver->id;
      $response['token'] = Authorization::generateToken($tokenData);
      $driver_rating = $this->restapi_model->get_driver_rating($driver->id);
      $driver->rating = round($driver_rating);

      $input = $this->input->post(['device', 'device_token','device_model','device_os']);
      if(empty($input['device_token']))
    {
		$lang = $dataPost['lang'];
		$reponse_msg = $this->notifylib->getresponse('empty_deviceid','common',$lang);
      $this->response(['status'=>0,'message'=>$reponse_msg]);
      return;
    }

    $input['userid'] = $driver->id;
    $player_id = $this->getPlayerId($input);
	$this->model->d_data('push_notification_driver',['driver_id'=>$driver->id]);
	$this->model->i_data("push_notification_driver", ['driver_id' => $driver->id, 'device_token' => $input['device_token'], 'device' => $input['device'], 'player_id' => $player_id]);
	//$this->model->u_data("push_notification_driver", ['device' => $input['device'], 'device_token' => $input['device_token']], ['driver_id' => $driver->id]);


      $response['driverdata'] = $driver;
      $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($driver->id);
      $data = $this->model->registerd_car($driver->id);
      $response['vehicle_count'] = count($data);
      $response['chat_id'] = $driver->chatid;
	  $lang = $dataPost['lang'];
	  $reponse_msg = $this->notifylib->getresponse('loggedin','common',$lang);
      $response['message'] = $reponse_msg;
      $response['status'] = 1;
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    } else {
	$lang = $dataPost['lang'];
		$reponse_msg = $this->notifylib->getresponse('detailnotvalid','common',$lang);
      $response = [
          'status' => 0,
          'message' => $reponse_msg,
              // 'verified'=>0,
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  
  public function getPlayerId($inputArray){
  
  	/* OneSignal Code */
  	 
  /*	$returnData = $this->model->checkNotificationId($inputArray['userid'], 'push_notification_driver','driver_id');
  	 
  	//print_r($returnData);die("good");
  	if(!empty($returnData[0]['player_id'])){
  		return $returnData[0]['player_id'];
  	}*/
  	 
  	if ($inputArray['device'] == 'android'){
  		$device_type = 1;
  		$app_id = '2049813d-427b-40eb-8179-49bba22bfa4e';
  	}else{
  		$device_type = 0;
  		$app_id = '2049813d-427b-40eb-8179-49bba22bfa4e';
  	}
  	$fields = array(
  			'app_id' => $app_id,
  			'identifier' => $inputArray['device_token'],
  			'language' => "en",
  			'timezone' => "-28800",
  			'game_version' => "1.0",
  			'device_os' => $inputArray['device_os'],
  			'device_type' => $device_type,
  			'device_model' => $inputArray['device_model']
  			//'tags' => array("foo" => "bar")
  	);
  
  	$fields = json_encode($fields);
  	//	print("\nJSON sent:\n");
  		//print_r($fields);die("good");
  
  	$ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players");
  	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  	curl_setopt($ch, CURLOPT_HEADER, FALSE);
  	curl_setopt($ch, CURLOPT_POST, TRUE);
  	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  
  	$response = curl_exec($ch);
  	curl_close($ch);
  
  	//	$return["allresponses"] = $response;
  	//$return = json_encode( $return);
  	$response = json_decode( $response);
  	//print_r($return); die;
  	//echo $response->id; die;
  	if($response->success == 'true'){
  		return $response->id;
  	}else{
  		return ;
  	}
  }

  public function driver_password_reset_post() {
    $input = $this->input->post(['mobile', 'otp', 'new_password', 'lang']);
    $data = $this->model->valid_otp($input['mobile'], $input['otp']);
    if (empty($data)) {
		$lang =  $input['lang'];
		$reponse_msg = $this->notifylib->getresponse('opt_not_match','common',$lang);
      $this->response(['status' => 0, 'message' => $reponse_msg]);
    } else {
      $this->model->u_data("drivers", ['password' => $input['new_password']], ['mobile' => $input['mobile']]);
	  $lang = $input['lang'];
		$reponse_msg = $this->notifylib->getresponse('opt_match','common',$lang);
      $this->response(['status' => 1, 'message' => $reponse_msg]);
    }
  }

  public function driverdevice_post() {

    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $token = $this->post('token');
        $device = $this->post('device');
		
		$lang = $this->notifylib->getlangkey('driver',$userid);
        $result = $this->restapi_model->token_driver($userid, $token, $device);
        if ($result > 0) {
			
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;

          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driver_get($driverid = '') {
    //$data = $this->restapi_model->get_userdata($userid);
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driver_data = $this->restapi_model->get_driver($token->id);
        $driver_rating = $this->restapi_model->get_driver_rating($token->id);
        $driver_data[0]->rating = round($driver_rating);
		
		$lang = $this->notifylib->getlangkey('driver',$token->id);
        if (!empty($driver_data)) {
          $output_data['driverdata'] = $driver_data[0];
          $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($token->id);
          if ($vehicle_count > 0) {
            $output_data['vehicle_count'] = 1;
          } else {
            $output_data['vehicle_count'] = 0;
            $typedata = $this->restapi_model->get_vehicletype();
            //print_r($typedata);die();
            foreach ($typedata as $type) {
              $typename = $type->type;

              $type->list = $this->restapi_model->getcar_list($typename);
              //print_r($type);die();
            }
            $output_data['vehicle_list'] = $typedata;
          }
		  
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
			 //$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverupdate_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        //$emailid = $this->post('emailid');
		$lang = $this->notifylib->getlangkey('driver',$token->id);
        $first_name = $this->post('fname');
        $middle_name = $this->post('mname');
        $last_name = $this->post('lname');
        $photo = $this->post('photo');
        $gender = $this->post('gender');
        // $mobile = $this->post('mobile');
        if (!empty($photo)) {
          $data = str_replace('data:image/png;base64,', '', $photo);
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $photo = "assets/documents/" . $filename;
          } else {
            $photo = "assets/documents/user-icon.png";
          }
        } else {
          $photo = "assets/documents/user-icon.png";
        }

        $data = $this->restapi_model->update_driver($first_name, $middle_name, $last_name, $photo, $gender, $userid);
        //print_r($data);die();
        if ($data >= 0) {
          $user_data = $this->restapi_model->get_driver($userid);
          if (!empty($user_data)) {
            $tokenData['id'] = $user_data[0]->id;
            $output_data['token'] = Authorization::generateToken($tokenData);
            $driver_rating = $this->restapi_model->get_driver_rating($user_data[0]->id);
            $user_data[0]->rating = round($driver_rating);
            $output_data['driverdata'] = $user_data[0];
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('profile_update','common',$lang);
			
            $output_data['message'] = $reponse_msg;
            $output_data['status'] = 1;
            $this->response($output_data);
            return;
          } else {
			  //$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
            $output_data['message'] = $reponse_msg;
            $output_data['status'] = 0;
            $this->response($output_data);
            return;
          }
        } else {
			// $lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverreview_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
		$lang = $this->notifylib->getlangkey('driver',$token->id);
        $ride_id = $this->post('ride_id');
        $star = $this->post('star');
        $review = $this->post('review');
		$user_data = $this->model->s_data("userid", "rides", ['id'=>$ride_id]);
				
		$userid = $user_data[0]['userid'];
        $result = $this->restapi_model->add_user_review($userid, $ride_id, $star, $review);
        if (!empty($result)) {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('driver_review','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function vehicleregister_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
		$lang = $this->notifylib->getlangkey('driver',$token->id);
		/*$output_data['message'] = 'Driveid : '.$driverid;
            $output_data['status'] = 0;
            $this->response($output_data);
            return;
		*/
			
        if ($this->post('vehicle_sno') != null) {
          $vehicle_sno = $this->post('vehicle_sno');
			$vehicle_sno_data = $this->model->s_data("vehicle_sno","vehicles",["vehicle_sno"=>$vehicle_sno]);
			if($vehicle_sno_data[0]['vehicle_sno'] !='' || $vehicle_sno_data[0]['vehicle_sno'] != null){
				$reponse_msg = $this->notifylib->getresponse('vehicle_sno_exists','driver',$lang);
				$this->response(['status' => 0, 'message' => $reponse_msg]);
				return;
			}
        } else {
          $vehicle_sno = '';
        }
        if ($this->post('vehicle_plate_char') != null) {
          $vehicle_no_plate = $this->post('vehicle_plate_char') . '-' . $this->post('vehicle_plate_num');
		  $vehicle_plat_data = $this->model->s_data("vehicle_number","vehicles",["vehicle_number"=>$vehicle_no_plate]);
		  if($vehicle_plat_data[0]['vehicle_number'] !='' || $vehicle_plat_data[0]['vehicle_number'] != null){
				$reponse_msg = $this->notifylib->getresponse('vehicle_plat_exists','driver',$lang);
				$this->response(['status' => 0, 'message' => $reponse_msg]);
				return;
			}
        } else {
          $vehicle_no_plate = '';
        }

        $vehicle_type = $this->post('vehicle_type');
		$checkdelv = $this->model->s_data("can_driver_deliver", "drivers", ['id' => $driverid]);
		if($checkdelv[0]['can_driver_deliver'] == 1){
			$vehicle_type = 'Delivery';
		}
        $car_model = $this->post('car_model');
        $vehicle_year = $this->post('vehicle_year');
        if ($this->post('vehicle_color') != null) {
          $vehicle_color = $this->post('vehicle_color');
        } else {
          $vehicle_color = '';
        }

        /* doc */
        //print_r( $_FILES);die();
        $target_dir = "../../assets/document_uploaded/";
        $path = '';

        if ($this->post('photograph') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('photograph'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $photofile = "assets/documents/" . $filename;
          } else {
            $photofile = "assets/documents/user-icon.png";
          }
        } else {
          $photofile = '';
        }
		//$carModel = explode(' ',$car_model);
		$car_photo = $this->model->s_data("car_description as photo","front_carlist",['id'=>$car_model]);
		$photofile = $car_photo[0]['photo'];
        if ($this->post('residence') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('residence'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $residfile = "assets/documents/" . $filename;
          } else {
            $residfile = "assets/documents/user-icon.png";
          }
        } else {
          $residfile = '';
        }
        if ($this->post('driver_license') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('driver_license'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $drivrlicfile = "assets/documents/" . $filename;
          } else {
            $drivrlicfile = "assets/documents/user-icon.png";
          }
        } else {
          $drivrlicfile = '';
        }
        $driver_license_expire = $this->post('driver_license_expire');

        if ($this->post('vehicle_reg') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('vehicle_reg'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $vregfile = "assets/documents/" . $filename;
          } else {
            $vregfile = "assets/documents/user-icon.png";
          }
        } else {
          $vregfile = '';
        }
        $vehicle_reg_expire = $this->post('vehicle_reg_expire');

        if ($this->post('vehicle_insur') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('vehicle_insur'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $vinsurfile = "assets/documents/" . $filename;
          } else {
            $vinsurfile = "assets/documents/user-icon.png";
          }
        } else {
          $vinsurfile = '';
        }
        $vehicle_insur_expire = $this->post('vehicle_insur_expire');

        if ($this->post('auth_img') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('auth_img'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $authfile = "assets/documents/" . $filename;
          } else {
            $authfile = "assets/documents/user-icon.png";
          }
        } else {
          $authfile = '';
        }

        if ($this->post('tafweeth_img') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('tafweeth_img'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $taffile = "assets/documents/" . $filename;
          } else {
            $taffile = "assets/documents/user-icon.png";
          }
        } else {
          $taffile = '';
        }
        $tafweeth_expire = $this->post('tafweeth_expire');

        $iban = $this->post('iban');
        if ($this->post('iban_bank') != null) {
          $iban_bank = $this->post('iban_bank');
        } else {
          $iban_bank = '';
        }

        $result = $this->restapi_model->add_vehicle($driverid, $vehicle_sno, $vehicle_no_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $photofile, $residfile, $drivrlicfile, $driver_license_expire, $vregfile, $vehicle_reg_expire, $vinsurfile, $vehicle_insur_expire, $authfile, $taffile, $tafweeth_expire, $iban, $iban_bank);

        if ($result > 0) {
          $driver_data = $this->restapi_model->get_driver($driverid);
          if (!empty($driver_data)) {
            $output_data['driverdata'] = $driver_data[0];
            $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($driverid);
            if ($vehicle_count > 0) {
              $output_data['vehicle_count'] = $vehicle_count;
              $output_data['register_vehicle_list'] = $this->restapi_model->getdriver_vehiclereg($deiverid);
              //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
              $typedata = $this->restapi_model->get_vehicletype();
              //print_r($typedata);die();
              foreach ($typedata as $type) {
                $typename = $type->type;

                $type->list = $this->restapi_model->getcar_list($typename);
                //print_r($type);die();
              }
              $output_data['vehicle_list'] = $typedata;
            } else {
              $output_data['vehicle_count'] = 0;
              //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
              $typedata = $this->restapi_model->get_vehicletype();
              //print_r($typedata);die();
              foreach ($typedata as $type) {
                $typename = $type->type;

                $type->list = $this->restapi_model->getcar_list($typename);
                //print_r($type);die();
              }
              $output_data['vehicle_list'] = $typedata;
            }
				//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('vehicle_reg','driver',$lang);
            $this->response(['status' => 1, 'message' => $reponse_msg]);
            return;
          } else {
			 // $lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','driver',$lang);
            $output_data['message'] = $reponse_msg;
            $output_data['status'] = 0;
            $this->response($output_data);
            return;
          }
		  //$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','driver',$lang);
          
          $this->response(['status' => 0, 'message' => $reponse_msg]);
          return;
        }
        $response = [
            'status' => 0,
            'message' => 'Unauthorized',
        ];
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        return;
      }
      $response = [
          'status' => 0,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => 0,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driversetpassword_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
		$lang = $this->notifylib->getlangkey('driver',$token->id);
        $user_data = array();
        $user_data['password'] = $this->post('password');
        $user_data['webstatus'] = 0;
        $result = $this->restapi_model->driversetpassword($userid, $user_data);
        //print_r($result);die();
        if ($result != null) {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('password_reset','driver',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
		//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','driver',$lang);
        $output_data['message'] = $reponse_msg;
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function drivervehicle_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $deiverid = $token->id;
		$lang = $this->notifylib->getlangkey('driver',$token->id);
        $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($deiverid);
        $vehicle_type = $this->restapi_model->checkdriver_vehiclereg_type($deiverid);
        //$vehicle_doc_count = $this->restapi_model->checkdriver_vehicledoc($user_data[0]->id);
        if ($vehicle_count > 0) {
          $output_data['vehicle_count'] = $vehicle_count;
          $output_data['register_vehicle_list'] = $this->restapi_model->getdriver_vehiclereg($deiverid);
          //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
          $typedata = $this->restapi_model->get_vehicletype();
        //  print_r($typedata);
        //  print_r($vehicle_type);die();
		$typearr = array();
			foreach($vehicle_type as $checktype){
				$typearr[] =  $checktype->vehicle_family_id;
			}
			//print_r($typearr);
			//die();
          foreach ($typedata as $type) {
            $typename = $type->type;
            $typeid = $type->id;
			if(!in_array($typeid,$typearr)){
            $type->list = $this->restapi_model->getcar_list($typename);
			}
            //print_r($type);die();
          }
          $output_data['vehicle_list'] = $typedata;
        } else {
          $output_data['vehicle_count'] = 0;
          $output_data['register_vehicle_list'] = [];
          //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
          $typedata = $this->restapi_model->get_vehicletype();
          //print_r($typedata);die();
          foreach ($typedata as $type) {
            $typename = $type->type;

            $type->list = $this->restapi_model->getcar_list($typename);
            //print_r($type);die();
          }
          $output_data['vehicle_list'] = $typedata;
        }


        //$output_data['data'] = $result;
		
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }
	  //$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','driver',$lang);
      $output_data['message'] = $reponse_msg;
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
  }

  public function drivershift_post() {
    $headers = $this->input->request_headers();
    //print_r($headers);die();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $data['vehicle_id'] = $this->post('vehicle_id');
        $parent_vehicle_family = $this->post('parent_vehicle_family');
        $opt_vehicle_family = $this->post('opt_vehicle_family');
        $data['vehicle_type'] = $parent_vehicle_family;
        $data['driver_id'] = $token->id;
        $data['status'] = $this->post('status');
		if($opt_vehicle_family != ''){
			$vehiclefam = explode(',',$opt_vehicle_family);
		
		}else{
			$vehiclefam = array();
		}
		$lang = $this->notifylib->getlangkey('driver',$token->id);
		array_push($vehiclefam,$data['vehicle_type']);
        $result = $this->restapi_model->drivershift($data);
		$this->model->u_data('drivers',['online_flag'=>$data['status'],'vehicle_type'=>$data['vehicle_type'],'online_vehicle_id'=>$data['vehicle_id'],'ride_flag'=>0],["id" =>$data['driver_id']]);
		
		
		if($data['status']==1){
			$this->model->d_data('driver_vehicles_segment',['driver_id'=>$data['driver_id']]);
			$i_arr = array();
			foreach($vehiclefam as $vfm_data){
				$rowarr = array(
					  'driver_id' => $data['driver_id'],
					  'vehicle_type' => $vfm_data
					);
				array_push($i_arr,$rowarr);
			}
			//print_r($i_arr);die();
			
			$this->model->b_i_data('driver_vehicles_segment',$i_arr);
		}else{
			$this->model->d_data('driver_vehicles_segment',['driver_id'=>$data['driver_id']]);
		}
		
        $output_data = array();
        if ($result > 0) {
          //$output_data['latlng'] = $result[0]->latlng;
		  if($data['status']==1){
			 // $lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('driver_online','driver',$lang);
			$output_data['message'] = $reponse_msg;
		  }else{
			//  $lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('driver_offline','driver',$lang);
			$output_data['message'] = $reponse_msg;
		  }
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','driver',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverrideaction_post() {
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $ride_id = $this->post('ride_id');
        $driver_id = $token->id;
        $vehicle_id = $this->post('vehicle_id');

        $result = $this->restapi_model->add_drivertoride($ride_id, $driver_id, $vehicle_id);
		$this->model->u_data('drivers',['ride_flag'=>1],["id" =>$driver_id]);
        $output_data = array();
        if ($result > 0) {
          //$output_data['latlng'] = $result[0]->latlng;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  
  public function registerd_car_post() {
	$headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
		$driver_id = $this->input->post('driver_id');
		$lang = $this->notifylib->getlangkey('driver',$token->id);
		$data = $this->model->registerd_car($driver_id);
		$drv_data = $this->model->s_data("online_vehicle_id","drivers",["id"=>$driver_id]);
		foreach($data as $car=>$value){
			$data[$car]['doc_val'] = base_url().$value['doc_val'];
			//$car_arr = explode(' ',$value['vehicle_model']);
			//print_r($car_arr);die();
			//$carpath = $this->model->s_data("path", "master_car_brand_logo", ['name' => $car_arr[0]]);
			//$data[$car]['vehicle_logo'] = base_url().$carpath[0]['path'];
			$data[$car]['vehicle_model'] = $value['car_brand'].' '.$value['car_model'];
			unset($data[$car]['car_brand']);
			unset($data[$car]['car_model']);
			if($value['vehicle_family_id']==1){
				$data[$car]['opt_vehicle_family'] = array();
			}
			if($value['vehicle_family_id']==2){
				$data[$car]['opt_vehicle_family'] = array(array('family'=>'Medium','id'=>1));
			}
			if($value['vehicle_family_id']==3){
				
				$result = $this->model->q_data("select vf.display_name 'family', vf.id from front_carlist fcl, vehicle_family vf where fcl.car_type = vf.display_name and vf.id != ".$value['vehicle_family_id']." and fcl.car_model= '".$car_arr[1]."'" );
				//print_r($result);die();
				$optvehicle = array();
				array_push($optvehicle,array('family'=>'Medium','id'=>1));
				if($result != null){
					array_push($optvehicle,$result[0]);
				}
				$data[$car]['opt_vehicle_family'] = $optvehicle;
				
			}
			
			if($value['vehicle_id'] == $drv_data[0]['online_vehicle_id']){
					$data[$car]['online_flag'] = 1;
				}else{
					$data[$car]['online_flag'] = 0;
				}
		}
		if (empty($data)) {
			//$lang = 'en';
				$reponse_msg = $this->notifylib->getresponse('nocar','driver',$lang);
		  $this->response(['status' => 0, 'message' => $reponse_msg]);
		  return;
		} else {
		  $this->response(['status' => 1, 'message' => 'success', 'data' => $data]);
		  return;
		}
	  }
		  $response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	$response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function getratecard_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
        $code = $this->post('code');
        $date = date('Y-m-d');
        //echo $date;die();
        $coupan_data = $this->restapi_model->get_ratecard($code, $date);
        if ($coupan_data != null) {
          $transaction_detail = 'Offline Card';
          $type = 'Offline Coupon';
          $userid = $driverid;
          $user_type = 'driver';
          $status = 'success';
          $transaction_id = $this->restapi_model->dotransaction($transaction_detail, $type, $userid, $user_type, $status);
          $this->restapi_model->changecoupan_status($coupan_data[0]->id);
          $this->restapi_model->driver_ratecard($userid, $coupan_data[0]->id, $transaction_id);
          $rides = $coupan_data[0]->total_rides;
          $this->restapi_model->credit_ride($userid, $rides);
          $output_data['data'] = $coupan_data[0];
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  
  public function onlineratecard_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
        $date = date('Y-m-d');
        $card_data = $this->restapi_model->get_online_ratecards();
        if ($card_data != null) {
          $output_data['data'] = $card_data;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
        $response = [
            'status' => REST_Controller::HTTP_UNAUTHORIZED,
            'message' => 'Unauthorized',
        ];
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        return;
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  
  public function ratecardtransac_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
        $amount = $this->post('amount');
        $transaction_token = $this->post('transaction_token');
        $status = $this->post('status');
        $date = date('Y-m-d');
        $coupan_data = $this->restapi_model->get_onlinecard_id($amount, $date);
        //echo $this->db->last_query();
        //print_r($coupan_data);die();
        if ($coupan_data != null) {
          $transaction_detail = $transaction_token;
          $type = 'Online Coupon';
          $userid = $driverid;
          $user_type = 'driver';
          $status = $status;
          $transaction_id = $this->restapi_model->dotransaction($transaction_detail, $type, $userid, $user_type, $status);
          $test = $this->restapi_model->changecoupan_useflag($coupan_data[0]->id);
          //print_r($test);die();
          $this->restapi_model->driver_ratecard($userid, $coupan_data[0]->id, $transaction_id);
          $rides = $coupan_data[0]->total_rides;
          $this->restapi_model->credit_ride($userid, $rides);
          $output_data['data'] = $coupan_data[0];
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error1';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  
  public function getvehicleinfo_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $deiverid = $token->id;
		$lang = $this->notifylib->getlangkey('driver',$token->id);
        $vehicle_id = $this->post('vehicle_id');
        $vehicle_info = $this->restapi_model->getvehicleinfo($deiverid, $vehicle_id);
        //$vehicle_doc_count = $this->restapi_model->checkdriver_vehicledoc($user_data[0]->id);
        if ($vehicle_info != null) {
          $output_data['vehicle_info'] = $vehicle_info[0];
          //$output_data['data'] = $result;
		  
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          //$output_data['data'] = $result;
		 // $lang = 'en';
	  $reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
        //$output_data['data'] = $result;
		//$lang = 'en';
	  $reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
        $output_data['message'] = $reponse_msg;
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }
	
    }
  }
  public function drivercancelride_post() {
    $headers = $this->input->request_headers();
    //print_r($headers);die();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $ride_id = $this->post('ride_id');
        $driver_id = $token->id;
		$lang = $this->notifylib->getlangkey('driver',$token->id);
        $cancel_option = $this->post('cancel_option');

        $this->model->u_data("rides", ['cancel_option' => $cancel_option,'ride_flag'=>4], ['id' => $ride_id, 'driverid' => $driver_id]);
		$this->model->u_data("drivers", ['ride_flag'=>0], ['id'=>$driver_id]);
		
		$user_data = $this->model->s_data("userid", "rides", ['id'=>$ride_id]);
				$device_data = $this->model->s_data("device_token,device", "push_notification_user", ['userid'=>$user_data[0]['userid']]);
				//print_r($device_data);die();
				
			
				$device_type = $device_data[0]['device'];
				$device_id = $device_data[0]['device_token'];
				$notify_user = 'user';
				
				$msg_payload = array(
					'mtitle' => "Ride Notification",
					'mdesc' => "Driver has canceled the ride",
					'ride_data' => array('notify_type'=>'driver_cancel_ride'),
					//'notify_type' => 'ride_notify',
				);
				
				if($device_type=='ios'){
				  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
				}
				if($device_type=='android'){
				   $this->notifylib->android($msg_payload, $device_id, $notify_user);
				}
				
				
				
					$this->response(['status'=>1,'message'=>'Successfully canceled.']);
					return;
				
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  public function ride_history_post(){
		$headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$id = $token->id;
				$lang = $this->notifylib->getlangkey('driver',$token->id);
				$type = $this->post('type');
				$status = $this->post('status');
				$result = $this->restapi_model->get_ridehistory($id,$type,$status);
				 //  print_r($result);die();
				$this->response(['history'=>$result,'status'=>1,'message'=>'Success']);
				return;
			}
			$response = [
				'status' => REST_Controller::HTTP_UNAUTHORIZED,
				'message' => 'Unauthorized',
				];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
	}
	public function drivercancel_option_get(){
		$result = $this->restapi_model->drivercancel_option();
		$output_data['options'] = $result;
		
		$output_data['message'] = 'Success';
		$output_data['status'] = 1;
		$this->response($output_data);
		return;
	}
	public function driverstartride_post(){
		$headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false){
				$driver_id = $token->id;
				$lang = $this->notifylib->getlangkey('driver',$token->id);
				$ride_id = $this->post('ride_id');
				$now = new DateTime();
				//echo $now->format('Y-m-d H:i:s');    // MySQL datetime format
				//$ride_start_time = $now->getTimestamp(); 
				$ride_start_time = round(microtime(true) * 1000);
				
				$this->model->u_data("rides", ['ride_start_time'=>$ride_start_time,'ride_flag'=>2], ['id'=>$ride_id,'driverid'=>$driver_id]);
				
				
				$user_data = $this->model->s_data("userid", "rides", ['id'=>$ride_id]);
				$device_data = $this->model->s_data("device_token,device", "push_notification_user", ['userid'=>$user_data[0]['userid']]);
				//print_r($device_data);die();
				
				
				$device_type = $device_data[0]['device'];
				$device_id = $device_data[0]['device_token'];
				$notify_user = 'user';
				
				$msg_payload = array(
					'mtitle' => "Ride Notification",
					'mdesc' => "Driver has started ride",
					'ride_data' => array('notify_type'=>'ride_start'),
					//'notify_type' => 'ride_notify',
				);
				
				if($device_type=='ios'){
				  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
				}
				if($device_type=='android'){
				  $this->notifylib->android($msg_payload, $device_id, $notify_user);
				}
				$this->model->u_data("drivers", ['ride_flag'=>1], ['id'=>$driver_id]);
				$lang = 'en';
				$reponse_msg = $this->notifylib->getresponse('start_trip','user',$lang);
				$this->response(['status'=>1,'message'=>$reponse_msg]);
				return;
			}
			$response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
			];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
		$response = [
		'status' => REST_Controller::HTTP_UNAUTHORIZED,
		'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	public function GetDrivingDistance($lat1, $lat2, $long1, $long2) {
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    //return $response_a;
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

    return array('distance' => $dist, 'time' => $time);
  }
  public function driverarrival_post(){
		$headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false){
				$driver_id = $token->id;
				$lang = $this->notifylib->getlangkey('driver',$token->id);
				//echo $driver_id;die();
				$ride_id = $this->post('ride_id');
				$now = new DateTime();
				//echo $now->format('Y-m-d H:i:s');    // MySQL datetime format
				//$driver_reach_time = $now->getTimestamp(); 
				$driver_reach_time = round(microtime(true) * 1000);
				
				$this->model->u_data("rides", ['driver_reach_time'=>$driver_reach_time,'ride_flag'=>1], ['id'=>$ride_id,'driverid'=>$driver_id]);
				
				$user_data = $this->model->s_data("userid", "rides", ['id'=>$ride_id]);
				$device_data = $this->model->s_data("device_token,device", "push_notification_user", ['userid'=>$user_data[0]['userid']]);
				//print_r($device_data);die();
				//$this->response(['status'=>1,'message'=>$device_data]);
						//return;
				
				if($device_data != null){
					$device_type = $device_data[0]['device'];
					$device_id = $device_data[0]['device_token'];
					$notify_user = 'user';
					
					$msg_payload = array(
						'mtitle' => "Ride Notification",
						'mdesc' => "Your Driver has arrived",
						'ride_data' => array('notify_type'=>'arrival'),
						//'notify_type' => 'ride_notify',
					);
					
					if($device_type=='ios'){
					  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
					}
					if($device_type=='android'){
					   $this->notifylib->android($msg_payload, $device_id, $notify_user);
					}
					//print_r( $device_data );die();
					
					//$lang = 'en';
					$reponse_msg = $this->notifylib->getresponse('start_trip','user',$lang);
						$this->response(['status'=>1,'message'=>$reponse_msg]);
						return;
					
				}else{
						//$lang = 'en';
					$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
						$this->response(['status'=>0,'message'=>$reponse_msg]);
						return;
					}
			}
			$response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
			];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
		$response = [
		'status' => REST_Controller::HTTP_UNAUTHORIZED,
		'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	public function changepassword_post(){
	  $headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
		  $token = Authorization::validateToken($headers['Authorization']);
		  if ($token != false) {
			  $userid = $token->id;
			  $lang = $this->notifylib->getlangkey('driver',$token->id);
			  $old_pass = $this->post('old_password');
			  $data['password'] = $this->post('new_password');
			  $userdata = $this->model->s_data("id", "drivers", ['password' => $old_pass,'id'=>$userid]);
			  if($userdata != null){
				  $result = $this->model->u_data("drivers", $data, ['id' => $userid]);
				  if($result!=null){
					 // $lang = 'en';
				$reponse_msg = $this->notifylib->getresponse('password_reset','driver',$lang);
					  $this->response(['status'=>1,'message'=>$reponse_msg]);
						return;
				  }else{
					 //  $lang = 'en';
				$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
					  $this->response(['status'=>0,'message'=>$reponse_msg]);
						return;
				  }
			  }else{
				  //$lang = 'en';
				$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
					  $this->response(['status'=>0,'message'=>$reponse_msg]);
						return;
				  }
		  }
		  $response = [
				'status' => REST_Controller::HTTP_UNAUTHORIZED,
				'message' => 'Unauthorized',
			];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
		$response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	public function optdelivery_post(){
	  $headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
		  $token = Authorization::validateToken($headers['Authorization']);
		  if ($token != false) {
			  $driverid = $token->id;
			  $lang = $this->notifylib->getlangkey('driver',$token->id);
			  $opt = $this->post('opt');
			  $delivery_check = $this->model->s_data('can_driver_deliver','drivers',['id'=>$driverid]);
			  if($delivery_check[0]['can_driver_deliver'] == 0){
				  if($opt == 1){
					  $this->model->i_data("driver_vehicles_segment", ['driver_id' => $driverid, 'vehicle_type' => 4]);
					  //$lang = 'en';
					$reponse_msg = $this->notifylib->getresponse('opt_delivery','driver',$lang);
					  $this->response(['status'=>1,'message'=>$reponse_msg]);
						return;
				  }else{
					  // $lang = 'en';
					$reponse_msg = $this->notifylib->getresponse('opt_delivery','driver',$lang);
					  $this->model->d_data("driver_vehicles_segment", ['driver_id' => $driverid, 'vehicle_type' => 4]);
					  $this->response(['status'=>1,'message'=>$reponse_msg]);
						return;
				  }
			  }else{
			   //$lang = 'en';
				$reponse_msg = $this->notifylib->getresponse('detailnotvalid','common',$lang);
				$this->response(['status'=>0,'message'=>$reponse_msg]);
				return;
			  }
			  
		  }
		  $response = [
				'status' => REST_Controller::HTTP_UNAUTHORIZED,
				'message' => 'Unauthorized',
			];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
		$response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	
	public function getLongTripCityList_post(){
		
		$headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
		  $token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$result = $this->restapi_model->getlongtripcity();
				$driverid = $token->id;
				foreach($result as $key => $citydata){
					$cityname = $citydata->city_name_en;
					$location_data = $citydata->location_data;
					if(empty($location_data)){
						$url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$cityname.'&sensor=false';
						$json_data = file_get_contents($url);
						$result2 = json_decode($json_data, TRUE);
						$result[$key]->location = $result2['results'][0]['geometry']['location'];
						$this->restapi_model->updateCityLocation(serialize($result[$key]->location), $citydata->id);
					}else{
						$result[$key]->location = unserialize($location_data);
					}
					
					//$longitude = $result['results'][0]['geometry']['location']['lng'];
				}
				$city_data = $this->model->s_data('cityid',"opt_longtrip", ['userid'=>$driverid ]);
				$output_data['city_list'] = $result;
				$output_data['selected_city_list'] = $city_data;
				$output_data['message'] = 'Success';
				$output_data['status'] = 1;
				$this->response($output_data);
				return;
			}
			$response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
			];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
		$response = [
		'status' => REST_Controller::HTTP_UNAUTHORIZED,
		'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	
	
	public function optlongtrip_post(){
	  $headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
		  $token = Authorization::validateToken($headers['Authorization']);
		  if ($token != false) {
			  $driverid = $token->id;
			  $lang = $this->notifylib->getlangkey('driver',$token->id);
			  $city_arr = explode(',',$this->post('cityid'));
			  $this->model->d_data("opt_longtrip", ['userid'=>$driverid ]);
			  
			  foreach($city_arr as $cityid){
				$this->model->i_data("opt_longtrip", ['userid' => $driverid, 'cityid' => $cityid]);
			  }
				$reponse_msg = $this->notifylib->getresponse('success','common',$lang);
				$this->response(['status'=>1,'message'=>$reponse_msg]);
				return;
		  }
		  $response = [
				'status' => REST_Controller::HTTP_UNAUTHORIZED,
				'message' => 'Unauthorized',
			];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
		$response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
}
