<?php

//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Rest_api extends REST_Controller {

  function __construct() {
    // Call the Model constructor
    parent::__construct();

    $this->load->model('restapi_model');
    $this->load->model('user_model');
    $this->load->model('model');
  }
  
  public function android($data, $reg_id) {
    $fcmApiKey = 'AAAAC4-Bpyk:APA91bH4ecJhI4yxKd2_e6Jjib6SYLzW7fpNaQZfNEyMG26-OKm51arJsr7nbJz4g-nfGvMG6oX3fYbe1bJ1V2q0gGq5gXhJd-ogPH0o7kLfgc90CDoqr6IbZ6cuzEXStdJk_7deM0YQ'; //App API Key(This is google cloud messaging api key not web api key)
    $url = 'https://fcm.googleapis.com/fcm/send'; //Google URL

    $registrationIds = array($reg_id); //Fcm Device ids array
    $fields = array('registration_ids' => $registrationIds, 'data' => $data);
    //echo json_encode( $fields );die();
    $headers = array(
        'Authorization: key=' . $fcmApiKey,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
      die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);
  }

  // Sends Push notification for iOS users
  public function iOS($data, $token) {

    $deviceToken = $token;

    $ctx = stream_context_create();
    // ck.pem is your certificate file
    $cert_file = BASEPATH . '../assets/Certificates.pem';
    //echo $cert_file;
    $passphrase = '';
    stream_context_set_option($ctx, 'ssl', 'local_cert', $cert_file);
    //stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
    // Open a connection to the APNS server
    $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 2, STREAM_CLIENT_CONNECT, $ctx);

    if (!$fp)
      exit("Failed to connect: $err $errstr" . PHP_EOL);

    // Create the payload body
    $body['aps'] = array(
        'alert' => array(
            'title' => $data['mtitle'],
            'body' => $data['mdesc'],
        ),
        'sound' => 'default'
    );

    // Encode the payload as JSON
    $payload = json_encode($body);

    // Build the binary notification
    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

    // Send it to the server
    $result = fwrite($fp, $msg, strlen($msg));

    // Close the connection to the server
    fclose($fp);
  }

}