<?php

set_time_limit(0);
ini_set('max_execution_time', 0); //0=NOLIMIT
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Rest_api extends REST_Controller {

  function __construct() {
    // Call the Model constructor
    parent::__construct();

    $this->load->model('restapi_model');
    $this->load->model('user_model');
    $this->load->model('driver_model');
    $this->load->model('model');
    //$this->load->model('category_model');
    //$this->load->model('case_model');
  }

  public function userdevice_post() {

    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      //print_r($token);die();
      if ($token != false) {
        $userid = $token->id;
        $token = $this->post('device_token');
        $device = $this->post('device_type');
        $result = $this->restapi_model->token_user($userid, $token, $device);
        if ($result > 0) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;

          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;

          $this->response($output_data);
          return;
        }
      }
    } else {
      $output_data['message'] = 'Error';
      $output_data['status'] = 0;

      $this->response($output_data);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverdevice_post() {

    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $token = $this->post('token');
        $device = $this->post('device');
        $result = $this->restapi_model->token_driver($userid, $token, $device);
        if ($result > 0) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;

          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function user_get($userid = '') {
    //$data = $this->restapi_model->get_userdata($userid);
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $user_data = $this->restapi_model->get_user($token->id);
        if (!empty($user_data)) {
          $output_data['userdata'] = $user_data[0];
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driver_get($driverid = '') {
    //$data = $this->restapi_model->get_userdata($userid);
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driver_data = $this->restapi_model->get_driver($token->id);
        $driver_rating = $this->restapi_model->get_driver_rating($token->id);
        $driver_data[0]->rating = $driver_rating;
        if (!empty($driver_data)) {
          $output_data['driverdata'] = $driver_data[0];
          $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($token->id);
          if ($vehicle_count > 0) {
            $output_data['vehicle_count'] = 1;
          } else {
            $output_data['vehicle_count'] = 0;
            $typedata = $this->restapi_model->get_vehicletype();
            //print_r($typedata);die();
            foreach ($typedata as $type) {
              $typename = $type->type;

              $type->list = $this->restapi_model->getcar_list($typename);
              //print_r($type);die();
            }
            $output_data['vehicle_list'] = $typedata;
          }
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function userlogin_post() {
    $dataPost = $this->input->post();
    $user = $this->user_model->userlogin($dataPost['mobile'], $dataPost['password']);

    if ($user != null) {


      @$this->model->u_data("push_notification_user", ['device_token' => $this->post('device_token'), 'device' => $this->post('device')], ['userid' => $user->id]);

      $tokenData = array();
      $tokenData['id'] = $user->id;
      $response['token'] = Authorization::generateToken($tokenData);
      $response['userdata'] = $user;
      $response['message'] = 'Success';
      $response['status'] = 1;
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    } else {
      $response = [
          'status' => 0,
          'message' => 'Invalid Mobile/Password.',
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverlogin_post() {
    $dataPost = $this->input->post();
    $driver = $this->user_model->driverlogin($dataPost['mobile'], $dataPost['password']);

    if ($driver->verified == "0") {
      $tokenData['id'] = $driver->id;
      $response['token'] = Authorization::generateToken($tokenData);
      $driver_rating = $this->restapi_model->get_driver_rating($driver->id);
      $driver->rating = $driver_rating;
      $response['driverdata'] = $driver;
      $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($driver->id);
      $data = $this->model->registerd_car($driver->id);
      $response['vehicle_count'] = count($data);
      $response['message'] = 'Success';
      $response['status'] = 1;
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    }



    if ($driver != null) {
      $tokenData = array();
      $tokenData['id'] = $driver->id;
      $response['token'] = Authorization::generateToken($tokenData);
      $driver_rating = $this->restapi_model->get_driver_rating($driver->id);
      $driver->rating = $driver_rating;

      $input = $this->input->post(['device', 'device_token']);
      @$this->model->u_data("push_notification_driver", ['device' => $input['device'], 'device_token' => $input['device_token']], ['driver_id' => $driver->id]);


      $response['driverdata'] = $driver;
      $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($driver->id);
      $data = $this->model->registerd_car($driver->id);
      $response['vehicle_count'] = count($data);
      $response['message'] = 'Success';
      $response['status'] = 1;
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    } else {

      $response = [
          'status' => 0,
          'message' => 'Driver Not Verified',
              // 'verified'=>0,
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function userregister_post() {

    $username = $this->post('first_name');
    $emailid = $this->post('emailid');
    $first_name = $this->post('first_name');
    $password = $this->post('password');
    $photo = $this->post('photo');
    $gender = $this->post('gender');
    $mobile = $this->post('mobile');
    $city = $this->post('city');
    $lat = $this->post('lat');
    $lng = $this->post('lng');
    $device = $this->post('device');
    $device_token = $this->post('device_token');
    $latlng = json_encode(array($lat, $lng));
    $referralcode = $this->post('referralcode');
    if (!empty($referralcode)) {
      $check_ref = $this->model->s_data("referralcode", "users", ['referralcode' => $referralcode]);
      if (empty($check_ref)) {
        $array = ['status' => 0, 'message' => 'Invalid referral code'];
        //echo json_encode($array);
        $this->response($array);
        return;
      }
    }

    $user_type = 3;
    $status = 0;
    $email_count = $this->restapi_model->checkemailid_user($emailid);
    if ($email_count > 0) {
      $output_data['message'] = 'Email already registered.';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    $mobile_count = 0;
    if ($mobile != '') {
      $mobile_count = $this->restapi_model->checkmobile_user($mobile);
    }

    if ($mobile_count > 0) {
      $output_data['message'] = 'Mobile no. already registered.';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    if ($email_count == 0 && $mobile_count == 0) {


      $curl = curl_init();
      $otp = rand(1111, 9999);
      $message = "Welcome to najez your OTP is ";
      curl_setopt_array($curl, array(
          CURLOPT_URL => "http://oursms.net/api/sendsms.php",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}{$otp}&numbers={$mobile}&sender=NAJEZO&return=json",
          CURLOPT_HTTPHEADER => array(
              "cache-control: no-cache",
              "content-type: application/x-www-form-urlencoded",
              "postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
          ),
      ));

      $this->model->i_data("sms_otp_temp", ['mobile' => $mobile, 'otp' => $otp]);
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);



      //die();

      if (!empty($photo)) {
        $data = str_replace('data:image/png;base64,', '', $photo);
        $encodedData = str_replace(' ', '+', $data);
        $decodedData = base64_decode($encodedData);
        $filename = uniqid() . '.jpg';
        file_put_contents('././assets/documents/' . $filename, $decodedData);
        if (file_exists("././assets/documents/" . $filename)) {
          $photo = "assets/documents/" . $filename;
        } else {
          $photo = "assets/documents/user-icon.png";
        }
      } else {
        $photo = "assets/documents/user-icon.png";
      }
      if ($referralcode != '') {
        $reffer_count = $this->restapi_model->checkreferralcode_user($referralcode);
        if ($reffer_count > 0) {
          $total_amount = 20;
        } else {
          $total_amount = 0;
        }
      } else {
        $total_amount = 0;
      }
      $split_name = str_split($username, 3);
      $user_referral = strtoupper($split_name[0]) . rand(1111, 9999);
      $data = $this->restapi_model->add_user($username, $emailid, $first_name, $password, $photo, $mobile, $city, $user_type, $gender, $status, $latlng, $total_amount, $user_referral);
      //print_r($data);die();
      if (!empty($data) && $data > 0) {
        $user_data = $this->restapi_model->get_user($data);
        if (!empty($user_data)) {
          $user_id = $user_data[0]->id;

          $this->model->i_data("push_notification_user", ['device' => $device, 'device_token' => $device_token, 'user_id' => $user_data[0]->id]);


          $emailid = $user_data[0]->emailid;
          $this->sendusermail($user_id, $emailid);

          $tokenData['id'] = $user_data[0]->id;
          $output_data['token'] = Authorization::generateToken($tokenData);
          $output_data['userdata'] = $user_data[0];
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      } else {
        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    } else {
      $output_data['message'] = 'Already Registered.';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverregister_post() {

    $can_deliver = $this->post('can_deliver');
    $username = $this->post('first_name');
    $emailid = $this->post('emailid');
    $first_name = $this->post('first_name');
    $middle_name = $this->post('middle_name');
    $last_name = $this->post('last_name');
    $password = $this->post('password');
    $photo = $this->post('photo');
    $gender = $this->post('gender');
    $mobile = $this->post('mobile');
    $city = $this->post('city');
    $lat = $this->post('lat');
    $lng = $this->post('lng');
    $latlng = json_encode(array($lat, $lng));
    $referralcode = $this->post('referralcode');
    $status = 0;
    $email_count = $this->restapi_model->checkemailid_driver($emailid);
    if ($email_count > 0) {
      $output_data['message'] = 'Email already registered.';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    $mobile_count = 0;
    if ($mobile != '') {
      $mobile_count = $this->restapi_model->checkmobile_driver($mobile);
    }

    if ($mobile_count > 0) {
      $output_data['message'] = 'Mobile no. already registered.';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    if ($email_count == 0 && $mobile_count == 0) {






      $curl = curl_init();
      $otp = rand(1111, 9999);
      $message = "Welcome to najez your OTP is ";
      curl_setopt_array($curl, array(
          CURLOPT_URL => "http://oursms.net/api/sendsms.php",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}{$otp}&numbers={$mobile}&sender=NAJEZO&return=json",
          CURLOPT_HTTPHEADER => array(
              "cache-control: no-cache",
              "content-type: application/x-www-form-urlencoded",
              "postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
          ),
      ));
      $this->model->i_data("sms_otp_temp", ['mobile' => $mobile, 'otp' => $otp]);
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);


      if (!empty($photo)) {
        $data = str_replace('data:image/png;base64,', '', $photo);
        $encodedData = str_replace(' ', '+', $data);
        $decodedData = base64_decode($encodedData);
        $filename = uniqid() . '.jpg';
        file_put_contents('././assets/documents/' . $filename, $decodedData);
        if (file_exists("././assets/documents/" . $filename)) {
          $photo = "assets/documents/" . $filename;
        } else {
          $photo = "assets/documents/user-icon.png";
        }
      } else {
        $photo = "assets/documents/user-icon.png";
      }
      if ($referralcode != '') {
        $reffer_count = $this->restapi_model->checkreferralcode_driver($referralcode);
        if ($reffer_count > 0) {
          $total_amount = 20;
        } else {
          $total_amount = 0;
        }
      } else {
        $total_amount = 0;
      }
      $user_referral = $username . rand();
      $data = $this->restapi_model->add_driver($username, $emailid, $first_name, $password, $photo, $mobile, $city, $can_deliver, $gender, $status, $latlng, $total_amount, $user_referral);
      //print_r($data);die();
      if (!empty($data) && $data > 0) {
        $user_data = $this->restapi_model->get_driver($data);
        if (!empty($user_data)) {
          $driver_id = $user_data[0]->id;
          $emailid = $user_data[0]->emailid;
          $this->senddrivermail($driver_id, $emailid);

          $tokenData['id'] = $user_data[0]->id;
          $output_data['token'] = Authorization::generateToken($tokenData);
          $driver_rating = $this->restapi_model->get_driver_rating($user_data[0]->id);
          $user_data[0]->rating = $driver_rating;
          $output_data['driverdata'] = $user_data[0];
          $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($user_data[0]->id);
          //$vehicle_doc_count = $this->restapi_model->checkdriver_vehicledoc($user_data[0]->id);
          if ($vehicle_count > 0) {
            $output_data['vehicle_count'] = 1;
          } else {
            $output_data['vehicle_count'] = 0;
            //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
            $typedata = $this->restapi_model->get_vehicletype();
            //print_r($typedata);die();
            foreach ($typedata as $type) {
              $typename = $type->type;

              $type->list = $this->restapi_model->getcar_list($typename);
              //print_r($type);die();
            }
            $output_data['vehicle_list'] = $typedata;
          }
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      } else {
        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    } else {
      $output_data['message'] = 'Already Registered.';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function userupdate_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        //$emailid = $this->post('emailid');
        $first_name = $this->post('first_name');
        $middle_name = $this->post('middle_name');
        $last_name = $this->post('last_name');
        $photo = $this->post('photo');
        $gender = $this->post('gender');
        //$mobile = $this->post('mobile');
        /*
          $email_count = $this->restapi_model->checkemailid_user($emailid);
          if($email_count > 0){
          $output_data['message'] = 'Email already registered.';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
          }
          $mobile_count = 0;
          if($mobile!=''){
          $mobile_count = $this->restapi_model->checkmobile_user($mobile);
          }

          if($mobile_count > 0){
          $output_data['message'] = 'Mobile no. already registered.';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
          } */
        //if($email_count == 0 && $mobile_count == 0){
        if (!empty($photo)) {
          $data = str_replace('data:image/png;base64,', '', $photo);
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $photo = "assets/documents/" . $filename;
          } else {
            $photo = "assets/documents/user-icon.png";
          }
        } else {
          $photo = "assets/documents/user-icon.png";
        }
        /* if($referralcode != ''){
          $reffer_count = $this->restapi_model->checkreferralcode_user($referralcode);
          if($reffer_count > 0){
          $total_amount = 20;
          }else{
          $total_amount = 0;
          }
          }else{
          $total_amount = 0;
          }
          $user_referral = $username.rand(); */
        $data = $this->restapi_model->update_user($first_name, $middle_name, $last_name, $photo, $gender, $userid);
        //print_r($data);die();
        if ($data >= 0) {
          $user_data = $this->restapi_model->get_user($userid);
          if (!empty($user_data)) {
            $tokenData['id'] = $user_data[0]->id;
            $output_data['token'] = Authorization::generateToken($tokenData);
            $output_data['userdata'] = $user_data[0];
            $output_data['message'] = 'Success';
            $output_data['status'] = 1;
            $this->response($output_data);
            return;
          } else {
            $output_data['message'] = 'Error';
            $output_data['status'] = 0;
            $this->response($output_data);
            return;
          }
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
        /* }else{
          $output_data['message'] = 'Already Registered.';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
          } */
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverupdate_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        //$emailid = $this->post('emailid');
        $first_name = $this->post('first_name');
        $middle_name = $this->post('middle_name');
        $last_name = $this->post('last_name');
        $photo = $this->post('photo');
        $gender = $this->post('gender');
        // $mobile = $this->post('mobile');
        if (!empty($photo)) {
          $data = str_replace('data:image/png;base64,', '', $photo);
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $photo = "assets/documents/" . $filename;
          } else {
            $photo = "assets/documents/user-icon.png";
          }
        } else {
          $photo = "assets/documents/user-icon.png";
        }

        $data = $this->restapi_model->update_driver($first_name, $middle_name, $last_name, $photo, $gender, $userid);
        //print_r($data);die();
        if ($data >= 0) {
          $user_data = $this->restapi_model->get_driver($userid);
          if (!empty($user_data)) {
            $tokenData['id'] = $user_data[0]->id;
            $output_data['token'] = Authorization::generateToken($tokenData);
            $driver_rating = $this->restapi_model->get_driver_rating($user_data[0]->id);
            $user_data[0]->rating = $driver_rating;
            $output_data['driverdata'] = $user_data[0];
            $output_data['message'] = 'Success';
            $output_data['status'] = 1;
            $this->response($output_data);
            return;
          } else {
            $output_data['message'] = 'Error';
            $output_data['status'] = 0;
            $this->response($output_data);
            return;
          }
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function vehicleregister_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
        if ($this->post('vehicle_sno') != null) {
          $vehicle_sno = $this->post('vehicle_sno');
        } else {
          $vehicle_sno = '';
        }
        if ($this->post('vehicle_plate_char') != null) {
          $vehicle_no_plate = $this->post('vehicle_plate_char') . '-' . $this->post('vehicle_plate_num');
        } else {
          $vehicle_no_plate = '';
        }

        $vehicle_type = $this->post('vehicle_type');
        $car_model = $this->post('car_model');
        $vehicle_year = $this->post('vehicle_year');
        if ($this->post('vehicle_color') != null) {
          $vehicle_color = $this->post('vehicle_color');
        } else {
          $vehicle_color = '';
        }

        /* doc */
        //print_r( $_FILES);die();
        $target_dir = "../../assets/document_uploaded/";
        $path = '';

        if ($this->post('photograph') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('photograph'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $photofile = "assets/documents/" . $filename;
          } else {
            $photofile = "assets/documents/user-icon.png";
          }
        } else {
          $photofile = '';
        }

        if ($this->post('residence') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('residence'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $residfile = "assets/documents/" . $filename;
          } else {
            $residfile = "assets/documents/user-icon.png";
          }
        } else {
          $residfile = '';
        }
        if ($this->post('driver_license') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('driver_license'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $drivrlicfile = "assets/documents/" . $filename;
          } else {
            $drivrlicfile = "assets/documents/user-icon.png";
          }
        } else {
          $drivrlicfile = '';
        }
        $driver_license_expire = $this->post('driver_license_expire');

        if ($this->post('vehicle_reg') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('vehicle_reg'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $vregfile = "assets/documents/" . $filename;
          } else {
            $vregfile = "assets/documents/user-icon.png";
          }
        } else {
          $vregfile = '';
        }
        $vehicle_reg_expire = $this->post('vehicle_reg_expire');

        if ($this->post('vehicle_insur') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('vehicle_insur'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $vinsurfile = "assets/documents/" . $filename;
          } else {
            $vinsurfile = "assets/documents/user-icon.png";
          }
        } else {
          $vinsurfile = '';
        }
        $vehicle_insur_expire = $this->post('vehicle_insur_expire');

        if ($this->post('auth_img') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('auth_img'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $authfile = "assets/documents/" . $filename;
          } else {
            $authfile = "assets/documents/user-icon.png";
          }
        } else {
          $authfile = '';
        }

        if ($this->post('tafweeth_img') != null) {
          $data = str_replace('data:image/png;base64,', '', $this->post('tafweeth_img'));
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $taffile = "assets/documents/" . $filename;
          } else {
            $taffile = "assets/documents/user-icon.png";
          }
        } else {
          $taffile = '';
        }
        $tafweeth_expire = $this->post('tafweeth_expire');

        $iban = $this->post('iban');
        if ($this->post('iban_bank') != null) {
          $iban_bank = $this->post('iban_bank');
        } else {
          $iban_bank = '';
        }

        $result = $this->restapi_model->add_vehicle($driverid, $vehicle_sno, $vehicle_no_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $photofile, $residfile, $drivrlicfile, $driver_license_expire, $vregfile, $vehicle_reg_expire, $vinsurfile, $vehicle_insur_expire, $authfile, $taffile, $tafweeth_expire, $iban, $iban_bank);

        if ($result > 0) {
          $driver_data = $this->restapi_model->get_driver($driverid);
          if (!empty($driver_data)) {
            $output_data['driverdata'] = $driver_data[0];
            $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($driverid);
            if ($vehicle_count > 0) {
              $output_data['vehicle_count'] = $vehicle_count;
              $output_data['register_vehicle_list'] = $this->restapi_model->getdriver_vehiclereg($deiverid);
              //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
              $typedata = $this->restapi_model->get_vehicletype();
              //print_r($typedata);die();
              foreach ($typedata as $type) {
                $typename = $type->type;

                $type->list = $this->restapi_model->getcar_list($typename);
                //print_r($type);die();
              }
              $output_data['vehicle_list'] = $typedata;
            } else {
              $output_data['vehicle_count'] = 0;
              //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
              $typedata = $this->restapi_model->get_vehicletype();
              //print_r($typedata);die();
              foreach ($typedata as $type) {
                $typename = $type->type;

                $type->list = $this->restapi_model->getcar_list($typename);
                //print_r($type);die();
              }
              $output_data['vehicle_list'] = $typedata;
            }

            $this->response(['status' => 1, 'message' => 'successfully saved']);
            return;
          } else {
            $output_data['message'] = 'Error';
            $output_data['status'] = 0;
            $this->response($output_data);
            return;
          }
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response(['status' => '1', 'message' => 'Saved']);
          return;
        }
        $response = [
            'status' => 0,
            'message' => 'Unauthorized',
        ];
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        return;
      }
      $response = [
          'status' => 0,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => 0,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function userreview_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $ride_id = $this->post('ride_id');
        $star = $this->post('star');
        $review = $this->post('review');
        $result = $this->restapi_model->add_user_review($userid, $ride_id, $star, $review);
        if (!empty($result)) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driverreview_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
        $ride_id = $this->post('ride_id');
        $star = $this->post('star');
        $review = $this->post('review');
        $result = $this->restapi_model->add_driver_review($driverid, $ride_id, $star, $review);
        if (!empty($result)) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  //Offline voucher
  /* public function getratecard_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
    $token = Authorization::validateToken($headers['Authorization']);
    if ($token != false) {
    $driverid = $token->id;
    $code = $this->post('code');
    $date = date('Y-m-d');
    //echo $date;die();
    $coupan_data = $this->restapi_model->get_ratecard($code, $date);
    if ($coupan_data != null) {
    $transaction_detail = 'Offline Card';
    $type = 'Offline Coupon';
    $userid = $driverid;
    $user_type = 'driver';
    $status = 'success';
    $transaction_id = $this->restapi_model->dotransaction($transaction_detail, $type, $userid, $user_type, $status);
    $this->restapi_model->changecoupan_status($coupan_data[0]->id);
    $this->restapi_model->driver_ratecard($userid, $coupan_data[0]->id, $transaction_id);
    $rides = $coupan_data[0]->total_rides;
    $this->restapi_model->credit_ride($userid, $rides);
    $output_data['data'] = $coupan_data[0];
    $output_data['message'] = 'Success';
    $output_data['status'] = 1;
    $this->response($output_data);
    return;
    } else {
    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
    }
    }
    $response = [
    'status' => REST_Controller::HTTP_UNAUTHORIZED,
    'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
    }
    $response = [
    'status' => REST_Controller::HTTP_UNAUTHORIZED,
    'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
    } */
  public function getratecard_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
        $code = $this->post('code');
        $date = date('Y-m-d');
        //echo $date;die();
        $coupan_data = $this->restapi_model->get_ratecard($code, $date);
        if ($coupan_data != null) {
          $transaction_detail = 'Offline Card';
          $type = 'Offline Coupon';
          $userid = $driverid;
          $user_type = 'driver';
          $status = 'success';
          $transaction_id = $this->restapi_model->dotransaction($transaction_detail, $type, $userid, $user_type, $status);
          $this->restapi_model->changecoupan_status($coupan_data[0]->id);
          $this->restapi_model->driver_ratecard($userid, $coupan_data[0]->id, $transaction_id);
          $rides = $coupan_data[0]->total_rides;
          $this->restapi_model->credit_ride($userid, $rides);
          $output_data['data'] = $coupan_data[0];
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  //Price list
  public function onlineratecard_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
        $date = date('Y-m-d');
        $card_data = $this->restapi_model->get_online_ratecards();
        if ($card_data != null) {
          $output_data['data'] = $card_data;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
        $response = [
            'status' => REST_Controller::HTTP_UNAUTHORIZED,
            'message' => 'Unauthorized',
        ];
        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
        return;
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  //online voucher perchase 



  public function ratecardtransac_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $driverid = $token->id;
        $amount = $this->post('amount');
        $transaction_token = $this->post('transaction_token');
        $status = $this->post('status');
        $date = date('Y-m-d');
        $coupan_data = $this->restapi_model->get_onlinecard_id($amount, $date);
        //echo $this->db->last_query();
        //print_r($coupan_data);die();
        if ($coupan_data != null) {
          $transaction_detail = $transaction_token;
          $type = 'Online Coupon';
          $userid = $driverid;
          $user_type = 'driver';
          $status = $status;
          $transaction_id = $this->restapi_model->dotransaction($transaction_detail, $type, $userid, $user_type, $status);
          $test = $this->restapi_model->changecoupan_useflag($coupan_data[0]->id);
          //print_r($test);die();
          $this->restapi_model->driver_ratecard($userid, $coupan_data[0]->id, $transaction_id);
          $rides = $coupan_data[0]->total_rides;
          $this->restapi_model->credit_ride($userid, $rides);
          $output_data['data'] = $coupan_data[0];
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error1';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function usersetpassword_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $user_data = array();
        $user_data['password'] = $this->post('password');
        $user_data['webstatus'] = 0;
        $result = $this->restapi_model->usersetpassword($userid, $user_data);
        //print_r($result);die();
        if ($result != null) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function driversetpassword_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $user_data = array();
        $user_data['password'] = $this->post('password');
        $user_data['webstatus'] = 0;
        $result = $this->restapi_model->driversetpassword($userid, $user_data);
        //print_r($result);die();
        if ($result != null) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function useremailunicode_post() {

    $user_data = array();
    $emailid = $this->post('emailid');
    $email_count = $this->restapi_model->checkemailid_user($emailid);
    if ($email_count > 0) {
      $digits = 4;
      $resetcode = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
      $user_data['resetcode'] = $resetcode;
      $result = $this->restapi_model->useremailunicode($emailid, $user_data);
      if ($result) {
        $message = '<html><body>';
        $message .= '<h3>Hello,</h3>';
        //$message .= '<p>Thankyou for submitting the details. We will keep you posted.</p>';
        $message .= '<p>To verify your emailid, please enter below mentioned code in mobile app</p>';
        //$code = $resetcode;
        //$link = '<a target="_blank" href="'.base_url('verify/activate/'.$code).'">'.base_url('verify/activate/'.$code).'</a>';
        $message .= '<p>Code: <b>' . $resetcode . '</b></p>';
        $message .= '</body></html>';
        $this->email->from('info@najez-online.com', 'Najez');
        $this->email->to($emailid);
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');
        $this->email->subject('Reset Code');
        $this->email->message($message);
        $this->email->set_mailtype("html");
        $this->email->send();

        if ($result != null) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
      }
      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Email not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;

    /* }
      $response = [
      'status' => REST_Controller::HTTP_UNAUTHORIZED,
      'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
      }
      $response = [
      'status' => REST_Controller::HTTP_UNAUTHORIZED,
      'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return; */
  }

  public function userverifyunicode_post() {
    $user_data = array();
    $emailid = $this->post('emailid');
    $code = $this->post('code');
    $email_count = $this->restapi_model->checkemailid_user($emailid);
    if ($email_count > 0) {

      $resetcode = $code;
      $code_check = $this->restapi_model->userverifyunicode($emailid, $resetcode);

      if ($code_check != null && $code_check == 1) {
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }

      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Email not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
  }

  public function userresetpassword_post() {
    $user_data = array();
    $emailid = $this->post('emailid');
    $password = $this->post('password');
    $code = $this->post('code');
    //$email_count = $this->restapi_model->checkemailid_user($emailid);
    $code_check = $this->restapi_model->userverifyunicode($emailid, $code);
    if ($code_check != null && $code_check == 1) {

      $code_check = $this->restapi_model->userresetpassword($emailid, $password, $code);

      if ($code_check != null && $code_check == 1) {
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }

      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Code not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
  }

  public function driveremailunicode_post() {

    $user_data = array();
    $emailid = $this->post('emailid');
    $email_count = $this->restapi_model->checkemailid_driver($emailid);
    if ($email_count > 0) {
      $digits = 4;
      $resetcode = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
      $user_data['resetcode'] = $resetcode;
      $result = $this->restapi_model->driveremailunicode($emailid, $user_data);
      if ($result) {
        $message = '<html><body>';
        $message .= '<h3>Hello,</h3>';
        //$message .= '<p>Thankyou for submitting the details. We will keep you posted.</p>';
        $message .= '<p>To verify your emailid, please enter below mentioned code in mobile app</p>';
        //$code = $resetcode;
        //$link = '<a target="_blank" href="'.base_url('verify/activate/'.$code).'">'.base_url('verify/activate/'.$code).'</a>';
        $message .= '<p>Code: <b>' . $resetcode . '</b></p>';
        $message .= '</body></html>';
        $this->email->from('info@najez-online.com', 'Najez');
        $this->email->to($emailid);
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');
        $this->email->subject('Reset Code');
        $this->email->message($message);
        $this->email->set_mailtype("html");
        $this->email->send();

        if ($result != null) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
      }
      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Email not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;

    /* }
      $response = [
      'status' => REST_Controller::HTTP_UNAUTHORIZED,
      'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
      }
      $response = [
      'status' => REST_Controller::HTTP_UNAUTHORIZED,
      'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return; */
  }

  public function driververifyunicode_post() {
    $user_data = array();
    $emailid = $this->post('emailid');
    $code = $this->post('code');
    $email_count = $this->restapi_model->checkemailid_driver($emailid);
    if ($email_count > 0) {

      $resetcode = $code;
      $code_check = $this->restapi_model->driververifyunicode($emailid, $resetcode);

      if ($code_check != null && $code_check == 1) {
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }

      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Email not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
  }

  public function driverresetpassword_post() {
    $user_data = array();
    $emailid = $this->post('emailid');
    $password = $this->post('password');
    $code = $this->post('code');
    //$email_count = $this->restapi_model->checkemailid_user($emailid);
    $code_check = $this->restapi_model->driververifyunicode($emailid, $code);
    if ($code_check != null && $code_check == 1) {

      $code_check = $this->model->u_data("drivers", ['password' => $password, 'resetcode' => ''], ['emailid' => $emailid, 'resetcode' => $code]);

      if ($code_check != null && $code_check == 1) {
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }

      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Code not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
  }

  public function getpage_post() {
    $user_data = array();
    $page = $this->post('page');
    $lang = $this->post('lang');

    $check_page = $this->restapi_model->check_page($page, $lang);
    if ($check_page != null) {

      $output_data['data'] = $check_page[0];
      $output_data['message'] = 'Success';
      $output_data['status'] = 1;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Page not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
  }

  public function support_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $support_data = array();
        $support_data['userid'] = $userid;
        $support_data['name'] = $this->post('name');
        $support_data['emailid'] = $this->post('emailid');
        $support_data['comment'] = $this->post('comment');
        $support_data['option_type'] = $this->post('option_type');
        $support_data['user_type'] = $this->post('user_type');
        $photo = $this->input->post('photo');

        if (!empty($photo)) {
          $data = str_replace('data:image/png;base64,', '', $photo);
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $photo = "assets/documents/" . $filename;
          } else {
            $photo = "assets/documents/user-icon.png";
          }
        } else {
          $photo = "assets/documents/user-icon.png";
        }

        $support_data['photo'] = $photo;
        $result = $this->restapi_model->add_supportticket($support_data);
        if ($result != null) {

          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }

  public function supportoption_post() {

    $table = $this->post("type");
    $lang = $this->post("lang");
    $result = $this->restapi_model->get_supportdata($table, $lang);
    $output_data['options'] = $result;
    $output_data['message'] = 'Success';
    $output_data['status'] = 1;
    $this->response($output_data);
    return;
  }

  public function bookride_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        //$ride_data = array();

        $ride_data = $this->input->post(array('trip_id', 'vehicle_type', 'booking_time', 'pickup_location', 'pickup_lat', 'pickup_lng', 'drop_location', 'drop_lat', 'drop_lng', 'scheduled', 'longtrip_city_id'));
        $ride_data['userid'] = $userid;
        $ride_data['GPS_Starting_Point'] = json_encode(array($ride_data['pickup_lat'], $ride_data['pickup_lng']));
        unset($ride_data['pickup_lat']);
        unset($ride_data['pickup_lng']);
        $ride_data['GPS_Destination_Point'] = json_encode(array($ride_data['drop_lat'], $ride_data['drop_lng']));
        unset($ride_data['drop_lat']);
        unset($ride_data['drop_lng']);
        $ride_data['promo_code'] = $this->input->post('promo_code');
        $ride_data['payment_method'] = $this->input->post('payment_method');
        $ride_data['distance'] = 50;
        $ride_data['calculated_price'] = 500;
        //print_r($ride_data);die();
        $result = $this->restapi_model->add_ridedata($ride_data);
        if ($result != null) {

          $output_data['data'] = array('driver_data' => 'Test');
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }

  public function confirmride_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $ride_id = $this->post('ride_id');
        $result = $this->restapi_model->confirm_ride($userid, $ride_id);
        if ($result != null) {
          $ride_data = $this->restapi_model->get_ridedetail($ride_id);
          //print_r($ride_data);die();
          $output_data['driver'] = $this->restapi_model->get_driver2($ride_data[0]->driverid);
          $output_data['vehicle'] = $this->restapi_model->getvehicleinfo($ride_data[0]->driverid, $ride_data[0]->vehicle_id);

          $output_data['driver'][0]['latitude'] = "28.4595";
          $output_data['driver'][0]['longitude'] = "77.0266";
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }

  public function triptype_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;

        $result = $this->restapi_model->get_triptype();
        if ($result != null) {
          $output_data['vehicle'] = $this->restapi_model->get_vehicletype();
          $output_data['trip'] = $result;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }

  public function vehicletype_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        //$ride_data = array();
        //$ride_data['userid'] = $userid;
        $result = $this->restapi_model->get_vehicletype();
        if ($result != null) {

          $output_data['data'] = $result;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }

  public function drivervehicle_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $deiverid = $token->id;

        $vehicle_count = $this->restapi_model->checkdriver_vehiclereg($deiverid);
        //$vehicle_doc_count = $this->restapi_model->checkdriver_vehicledoc($user_data[0]->id);
        if ($vehicle_count > 0) {
          $output_data['vehicle_count'] = $vehicle_count;
          $output_data['register_vehicle_list'] = $this->restapi_model->getdriver_vehiclereg($deiverid);
          //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
          $typedata = $this->restapi_model->get_vehicletype();
          //print_r($typedata);die();
          foreach ($typedata as $type) {
            $typename = $type->type;

            $type->list = $this->restapi_model->getcar_list($typename);
            //print_r($type);die();
          }
          $output_data['vehicle_list'] = $typedata;
        } else {
          $output_data['vehicle_count'] = 0;
          $output_data['register_vehicle_list'] = [];
          //$output_data['vehicle_list'] = $this->restapi_model->getcar_list();
          $typedata = $this->restapi_model->get_vehicletype();
          //print_r($typedata);die();
          foreach ($typedata as $type) {
            $typename = $type->type;

            $type->list = $this->restapi_model->getcar_list($typename);
            //print_r($type);die();
          }
          $output_data['vehicle_list'] = $typedata;
        }


        //$output_data['data'] = $result;
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }
      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
  }

  public function getvehicleinfo_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $deiverid = $token->id;
        $vehicle_id = $this->post('vehicle_id');
        $vehicle_info = $this->restapi_model->getvehicleinfo($deiverid, $vehicle_id);
        //$vehicle_doc_count = $this->restapi_model->checkdriver_vehicledoc($user_data[0]->id);
        if ($vehicle_info != null) {
          $output_data['vehicle_info'] = $vehicle_info[0];
          //$output_data['data'] = $result;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          //$output_data['data'] = $result;
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
        //$output_data['data'] = $result;
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }
      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
  }

  /*
    $msg_payload = array (
    'name' => $ownerdata[0]->name,
    'number' => $ownerdata[0]->mobileno,
    'msg' => $message,
    );
    if($data[0]->device == 'ios'){
    //$this->iOS($msg_payload,$data->token);
    }
    if($data[0]->device == 'android'){
    $this->android($msg_payload,$data[0]->token);
    }
   */

  public function android($data, $reg_id, $type) {
    if ($type == 'driver') {
      $fcmApiKey = 'AAAAXWBM9m4:APA91bH-v7CnARBVt73Q0OHqd9xQcW2__BzeZogBj5BcbirU2Z1-0vKIjt8c80TiTOF3Ele6ceCHVcIM40K0PvS56VXHsxO_LpWfNNq7YrXyVcul8G5raHsWDU1gUOtooJn2eV-M_jak'; //App API Key(This is google cloud messaging api key not web api key)
    } else {
      $fcmApiKey = 'AAAAC4-Bpyk:APA91bH4ecJhI4yxKd2_e6Jjib6SYLzW7fpNaQZfNEyMG26-OKm51arJsr7nbJz4g-nfGvMG6oX3fYbe1bJ1V2q0gGq5gXhJd-ogPH0o7kLfgc90CDoqr6IbZ6cuzEXStdJk_7deM0YQ'; //App API Key(This is google cloud messaging api key not web api key)
    }

    $url = 'https://fcm.googleapis.com/fcm/send'; //Google URL

    $registrationIds = array($reg_id); //Fcm Device ids array
    $fields = array('registration_ids' => $registrationIds, 'data' => $data);
    //echo json_encode( $fields );die();
    $headers = array(
        'Authorization: key=' . $fcmApiKey,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
      die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);
  }

  // Sends Push notification for iOS users
  public function iOS($data, $token, $type) {

    /* $deviceToken = $token;

      $ctx = stream_context_create();
      // ck.pem is your certificate file
      //echo $type; die();
      if($type == 'driver'){
      $cert_file = BASEPATH . '../assets/Certificates_driver_dev.pem';
      }else{
      $cert_file = BASEPATH . '../assets/Certificates_custmor_dev.pem';
      }

      $cert_file = BASEPATH . '../assets/Certificates_driver_dev.pem';
      // echo $cert_file;die();
      $passphrase = '';
      stream_context_set_option($ctx, 'ssl', 'local_cert', $cert_file);
      //stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
      // Open a connection to the APNS server
      $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 2, STREAM_CLIENT_CONNECT, $ctx);

      if (!$fp)
      exit("Failed to connect: $err $errstr" . PHP_EOL);

      // Create the payload body
      $body['aps'] = array(
      'alert' => array(
      'title' => $data['mtitle'],
      'body' => $data['mdesc'],
      ),
      'sound' => 'default'
      );

      // Encode the payload as JSON
      $payload = json_encode($body);

      // Build the binary notification
      $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

      // Send it to the server
      $result = fwrite($fp, $msg, strlen($msg));

      // Close the connection to the server
      fclose($fp);
     */


    $apnsHost = 'gateway.sandbox.push.apple.com';
    if ($type == 'driver') {
      $apnsCert = BASEPATH . '../assets/Certificates_driver_dev.pem';
    } else {
      $apnsCert = BASEPATH . '../assets/Certificates_custmor_dev.pem';
    }

    $apnsPort = 2195;
    //$apnsPass = '<PASSWORD_GOES_HERE>';
    $token = $token;

    $payload['aps'] = array('alert' => 'Oh hai!', 'badge' => 1, 'sound' => 'default');
    $output = json_encode($payload);
    $token = pack('H*', str_replace(' ', '', $token));
    $apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;

    $streamContext = stream_context_create();
    stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
    //stream_context_set_option($streamContext, 'ssl', 'passphrase', $apnsPass);

    $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
    if (!$apns)
      exit("Failed to connect: $error $errorString" . PHP_EOL);

    fwrite($apns, $apnsMessage);
    fclose($apns);
  }

  public function get_user_card_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $data = $this->model->s_data("card_detail", "card_detail", ['userid' => $userid]);
        foreach ($data as $key => $value) {
          $data[$key] = json_decode($value['card_detail']);
        }

        $this->response(['status' => 1, 'data' => $data]);
      }
    }
  }

  public function saveusercard_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $card_data = array();
        $card_data['card_number'] = $this->post('card_number');
        $card_data['expire_month'] = $this->post('expire_month');
        $card_data['expire_year'] = $this->post('expire_year');
        $card_data['card_type'] = $this->post('card_type');
        $card_data['card_username'] = $this->post('card_username');
        $result = $this->restapi_model->saveusercard($userid, $card_data);
        if ($result) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  private function set_photoupload_options() {
    //upload an image options
    $config = array();
    $config['upload_path'] = '././assets/documents/';
    $config['allowed_types'] = 'gif|jpg|png';
    //$config['max_size']      = '0';
    $config['overwrite'] = FALSE;

    return $config;
  }

  public function sendlocation_post() {
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $user_id = $this->post('userid');
        $user_type = $this->post('usertype');
        $latlng = $this->post('latlng');
        $result = $this->restapi_model->add_location($user_id, $user_type, $latlng);
        $response_data = array();
        if ($result > 0) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function getlocation_post() {
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $user_id = $this->post('userid');
        $user_type = $this->post('usertype');
        $result = $this->restapi_model->get_location($user_id, $user_type);
        $response_data = array();
        if ($result > 0) {
          $output_data['latlng'] = $result[0]->latlng;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function get_notify($userid) {
    $notify_data = $this->restapi_model->getnotify($userid);
    $notify_count = $this->restapi_model->notify_count($userid);
    if (!empty($notify_data)) {
      $userdata['notify_count'] = $notify_count;
      $userdata['notify_data'] = $notify_data;
      $userdata['status'] = 200;
      $userdata['message'] = 'Success';
    } else {
      $userdata['status'] = -200;
      $userdata['message'] = 'Error';
    }
    echo json_encode($userdata);
  }

  public function up_notify() {
    $postdata = file_get_contents("php://input");
    $data = json_decode($postdata);
    $userid = $data->userid;
    $notify_id = $data->notify_id;
    $result = $this->restapi_model->updatenotify($userid, $notify_id);
    if ($result > 0) {
      $notify_data = $this->restapi_model->getnotify($userid);
      $notify_count = $this->restapi_model->notify_count($userid);
      if (!empty($notify_data)) {
        $userdata['notify_count'] = $notify_count;
        $userdata['notify_data'] = $notify_data;
        $userdata['status'] = 200;
        $userdata['message'] = 'Success';
      } else {
        $userdata['status'] = -200;
        $userdata['message'] = 'Error';
      }
    } else {
      $userdata['status'] = -200;
      $userdata['message'] = 'Error';
    }
    echo json_encode($userdata);
  }

  public function social_login_post() {
    $data = $this->input->post(array('emailid', 'fname', 'gender', 'photo', 'login_type', 'device_token', 'device'));
    $check_email = $this->user_model->get_user($data['emailid']);
    if (!empty($check_email)) {
      $this->model->u_data("users", $data, ['emailid' => $data['emailid']]);

      $user22 = $this->user_model->get_user($data['emailid']);
      @$this->model->u_data("", ['device' => $data['device'], 'device_token' => $data['device_token']], ['userid' => $check_email[0]['id']]);
      $tokenData['id'] = $check_email[0]['id'];
      $this->response(['status' => 1, 'message' => 'Success', 'token' => Authorization::generateToken($tokenData), 'userdata' => $check_email[0]]);
    } else {
      $this->model->i_data("users", $data);

      $user22 = $this->user_model->get_user($data['emailid']);
      @$this->model->i_data("push_notification_user", ['device' => $data['device'], 'device_token' => $data['device_token'], 'userid' => $user22[0]['id']]);
      $tokenData['id'] = $user22[0]['id'];
      $this->response(['status' => 1, 'message' => 'Success', 'token' => Authorization::generateToken($tokenData), 'userdata' => $user22[0]]);
    }
  }

  public function senddrivermail($driver_id, $emailid) {
    $message = '<html><body>';
    $message .= '<h3>Hello,</h3>';
    $message .= '<p>Thankyou for submitting the details. We will keep you posted.</p>';
    $message .= '<p>To verify your emailid, please click on below link</p>';
    $code = base64_encode('encode-driver-' . $driver_id);
    $link = '<a target="_blank" href="' . base_url('verify/activate/' . $code) . '">' . base_url('verify/activate/' . $code) . '</a>';
    $message .= '<p>' . $link . '</p>';
    $message .= '</body></html>';
    $this->email->from('info@najez-online.com', 'Najez');
    $this->email->to($emailid);
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    $this->email->subject('Thankyou');
    $this->email->message($message);
    $this->email->set_mailtype("html");
    $this->email->send();

    $message = '<html><body>';
    $message .= '<h3>Hello,</h3>';
    $message .= '<p>.نشكركم لتزويدنا بمعلوماتكم، سنبقيكم على الاطلاع</p>';
    $message .= '<p>.نشكركم لتزويدنا بمعلوماتكم، يرجى منكم الضغط على الرابط الذي تم إرساله إليكم إلى بريدكم الإلكتروني لنكمل إجراءات التسجيل</p>';
    $code = base64_encode('encode-driver-' . $driver_id);
    $link = '<a target="_blank" href="' . base_url('verify/activate/' . $code) . '">' . base_url('verify/activate/' . $code) . '</a>';
    $message .= '<p>' . $link . '</p>';
    $message .= '</body></html>';
    $this->email->from('info@najez-online.com', 'Najez');
    $this->email->to($emailid);
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    $this->email->subject('Thankyou');
    $this->email->message($message);
    $this->email->set_mailtype("html");
    $this->email->send();
  }

  public function sendusermail($userid, $email) {
    $message = '<html><body>';
    $message .= '<h3>Hello,</h3>';
    $message .= '<p>.نشكركم لتزويدنا بمعلوماتكم، سنبقيكم على الاطلاع</p>';
    $message .= '<p>.نشكركم لتزويدنا بمعلوماتكم، يرجى منكم الضغط على الرابط الذي تم إرساله إليكم إلى بريدكم الإلكتروني لنكمل إجراءات التسجيل</p>';
    $code = base64_encode('encode-user-' . $userid);
    $link = '<a target="_blank" href="' . base_url('verify/activate/' . $code) . '">' . base_url('verify/activate/' . $code) . '</a>';
    $message .= '<p>' . $link . '</p>';
    $message .= '</body></html>';
    $this->email->from('noreply@najez-online.com', 'Najez');
    $this->email->to($email);
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    $this->email->subject('Thankyou');
    $this->email->message($message);
    $this->email->set_mailtype("html");
    $this->email->send();


    $message = '<html><body>';
    $message .= '<h3>Hello,</h3>';
    $message .= '<p>Thankyou for submitting the details. We will keep you posted.</p>';
    $message .= '<p>To verify your emailid, please click on below link</p>';
    $code = base64_encode('encode-user-' . $userid);
    $link = '<a target="_blank" href="' . base_url('verify/activate/' . $code) . '">' . base_url('verify/activate/' . $code) . '</a>';
    $message .= '<p>' . $link . '</p>';
    $message .= '</body></html>';
    $this->email->from('noreply@najez-online.com', 'Najez');
    $this->email->to($email);
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    $this->email->subject('Thankyou');
    $this->email->message($message);
    $this->email->set_mailtype("html");
    $this->email->send();
  }

  // this api update driver lat long in database
  public function update_driver_location_post() {
    $input = $this->input->post(['driver_id', 'latitude', 'longitude']);
    $this->model->i_data("driver_locations", $input);
    $this->response(['status' => '1', 'message' => 'saved']);
  }

  public function city_list_get() {
    $data = $this->model->g_data("city_master", "city_name_" . $this->get('lang') . ",id");
    $this->response(['status' => '1', 'message' => 'found', 'data' => $data]);
  }

  /* frontend captain code existance check */

  public function check_ref_code_get() {

    $data = $this->model->s_data("referralcode", "drivers", ['referralcode' => $this->input->get('code')]);
    if (empty($data)) {
      $data = ['data' => 'not found'];
      echo json_encode($data);
    } else {
      $data = ['data' => 'found'];
      echo json_encode($data);
    }
  }

  /* frontend Mobile number existance check */

  public function check_mobile_no_get() {

    $data = $this->model->s_data("mobile", "users", ['mobile' => $this->input->get('code')]);
    if (empty($data)) {
      $data = ['data' => 'not found'];
      echo json_encode($data);
    } else {
      $data = ['data' => 'found'];
      echo json_encode($data);
    }
  }

  public function registerd_car_post() {

    $data = $this->model->registerd_car($this->input->post('driver_id'));

    if (empty($data)) {
      $this->response(['status' => '0', 'message' => 'No Registerd Cars.']);
    } else {
      $this->response(['status' => '1', 'message' => 'success', 'data' => $data]);
    }
  }

  public function bank_list_get() {
    $data = $this->model->g_data("bank_list", "bank_name,id");
    $this->response(['status' => '1', 'message' => 'found', 'data' => $data]);
  }

  public function cancel_options_get() {
    $data = $this->model->g_data('master_cancel_options');
    $this->response(['status' => '1', 'data' => $data]);
  }

  public function send_otp_post() {

    $data = $this->input->post(['mobile', 'email']);
    $curl = curl_init();
    $otp = rand(1111, 9999);
    $message = "Welcome to najez your OTP is ";
    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://oursms.net/api/sendsms.php",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}{$otp}&numbers={$data['mobile']}&sender=NAJEZO&return=json",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
        ),
    ));
    $this->model->i_data("sms_otp_temp", ['mobile' => $data['mobile'], 'otp' => $otp]);
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);


    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }

  public function social_send_otp_post() {
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;



        $mobile = $this->input->post('mobile');
        $data = $this->model->s_data("mobile", "users", ['mobile' => $mobile]);
        if (!empty($data)) {
          $this->response(['status' => 0, 'message' => 'Please use different number.']);
          return;
        } else {

          $this->model->u_data("users", ['mobile' => $mobile], ['id' => $userid]);
          //$this->restapi_model->save_device_token($mobile,$device_token);
          $curl = curl_init();
          $otp = rand(1111, 9999);
          $message = "Welcome to najez your OTP is ";
          curl_setopt_array($curl, array(
              CURLOPT_URL => "http://oursms.net/api/sendsms.php",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}{$otp}&numbers={$mobile}&sender=NAJEZO&return=json",
              CURLOPT_HTTPHEADER => array(
                  "cache-control: no-cache",
                  "content-type: application/x-www-form-urlencoded",
                  "postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
              ),
          ));
          $this->model->i_data("sms_otp_temp", ['mobile' => $mobile, 'otp' => $otp]);
          $response = curl_exec($curl);
          $err = curl_error($curl);
          curl_close($curl);


          if ($err) {
            $this->response(['status' => 0, 'message' => 'Error, Try again']);
            return;
          } else {
            $this->response(['status' => 1, 'message' => 'Success']);
            return;
          }
        }
      }
    }
  }

  public function validate_otp_post() {

    $data = $this->model->valid_otp($this->input->post('mobile'), $this->input->post('otp'));
    if (!empty($data)) {
      $this->model->u_data("sms_otp_temp", ['valid' => '1'], ['mobile' => $data[0]['mobile'], 'otp' => $data[0]['otp']]);

      $this->response(['status' => '1', 'message' => 'OTP Matched Successfully.']);
    } else {
      $this->response(['status' => '0', 'message' => 'OTP Not Matched, Please Generate New.']);
    }
  }

  public function country_code_list_get() {
    $data = $this->model->s_data("name,phonecode", "master_country_code");
    $this->response(['status' => '1', 'data' => $data]);
  }

  public function driver_register_step2_post() {
    $input = $this->input->post(['mobile', 'otp', 'device', 'device_token']);
    $data = $this->model->valid_otp($input['mobile'], $input['otp']);

    if (empty($data)) {
      $this->response(['status' => '0', 'message' => 'OTP Not Matched, Please Generate New.']);
    } else {
      $data2 = $this->driver_model->step2_driver($input['mobile']);
      $tokendata = $data2[0]['id'];
      $this->model->i_data("push_notification_driver", ['device' => $input['device'], 'device_token' => $input['device_token'], 'driver_id' => $data2[0]['id']]);
      $vehicle = $this->restapi_model->checkdriver_vehiclereg($data2[0]['id']);
      $vehicle_count = $vehicle;
      $driver_rating = $this->restapi_model->get_driver_rating($data2[0]['id']);
      $data2[0]->rating = $driver_rating;
      $datass = $data2[0];
      $this->response(['status' => '1', 'message' => 'success', 'vehicle_count' => $vehicle_count, 'token' => Authorization::generateToken($tokendata), 'driverdata' => $datass]);
    }
  }

  public function driver_password_reset_post() {
    $input = $this->input->post(['mobile', 'otp', 'new_password']);
    $data = $this->model->valid_otp($input['mobile'], $input['otp']);
    if (empty($data)) {
      $this->response(['status' => 0, 'message' => 'OTP Not Matched, Please Generate New.']);
    } else {
      $this->model->u_data("drivers", ['password' => $input['new_password']], ['mobile' => $input['mobile']]);
      $this->response(['status' => 1, 'message' => 'success']);
    }
  }

  public function user_password_reset_post() {
    $input = $this->input->post(['mobile', 'otp', 'new_password']);
    $data = $this->model->valid_otp($input['mobile'], $input['otp']);
    if (empty($data)) {
      $this->response(['status' => 0, 'message' => 'OTP Not Matched, Please Generate New.']);
    } else {
      $this->model->u_data("users", ['password' => $input['new_password']], ['mobile' => $input['mobile']]);
      $this->response(['status' => 1, 'message' => 'success']);
    }
  }

  public function social_login_step2_post() {
    $input = $this->input->post(['mobile', 'otp', 'user_id']);
    $data = $this->model->valid_otp($input['mobile'], $input['otp']);
    if (empty($data)) {
      $this->response(['status' => 0, 'message' => 'OTP Not Matched, Please Generate New.']);
    } else {
      $this->model->u_data("users", ['verified' => 1], ['id' => $input['user_id']]);
      $this->response(['status' => 1, 'message' => 'success']);
    }
  }

  public function GetDrivingDistance($lat1, $lat2, $long1, $long2) {
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    //return $response_a;
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

    return array('distance' => $dist, 'time' => $time);
  }

  public function driverrideaction_post() {
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $ride_id = $this->post('ride_id');
        $driver_id = $token->id;
        $vehicle_id = $this->post('vehicle_id');

        $result = $this->restapi_model->add_drivertoride($ride_id, $driver_id, $vehicle_id);
        $output_data = array();
        if ($result > 0) {
          //$output_data['latlng'] = $result[0]->latlng;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function estimated_post() {
    $ride_id = $this->post('ride_id');
    //$driver_id = $token->id;
    $vehicle_type = $this->post('vehicle_type');
    $trip_type = $this->post('trip_type');
    $promo_code = $this->post('promo_code');
    $lat1 = 28.408912;
    $lat2 = 28.459497;
    $long1 = 77.317789;
    $long2 = 77.026638;
    $distance_data = $this->GetDrivingDistance($lat1, $lat2, $long1, $long2);
    //print_r($distance_data);die();

    $km = str_replace(' km', "", $distance_data['distance']);

    $ratecard_data = $this->restapi_model->getratecard($trip_type, $vehicle_type);
    $charges = $ratecard_data[0]->charges;
    $distance_data['Price'] = ($km * $charges);

    if (!empty($promo_code)) {
      $promo_price = $this->model->s_data("discount_amount,discount_percentage", "promotions", ['promo_code' => $promo_code]);
      if ($promo_price[0]['discount_amount'] != NULL) {
        $distance_data['Price'] = $distance_data['Price'] - $promo_price[0]['discount_amount'];
      } elseif ($promo_price[0]['discount_percentage'] != NULL) {
        $amount = $distance_data['Price'] * $promo_price[0]['discount_percentage'] / 100;
        $distance_data['Price'] = $distance_data['Price'] - $amount;
      }
    }


    $output_data = array();
    if ($distance_data != null) {
      $output_data['distance_data'] = $distance_data;
      $output_data['message'] = 'Success';
      $output_data['status'] = 1;
      $this->response($output_data);
    } else {
      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
    }
  }

  public function drivershift_post() {
    $headers = $this->input->request_headers();
    //print_r($headers);die();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $data['vehicle_id'] = $this->post('vehicle_id');
        //$data['driver_id'] = $token->id;
        $data['status'] = $this->post('status');

        $result = $this->restapi_model->drivershift($data);
        $output_data = array();
        if ($result > 0) {
          //$output_data['latlng'] = $result[0]->latlng;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function applypromo_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $promocode = $this->post('promocode');
        //$data['driver_id'] = $token->id;
        $vehicle_type = $this->post('vehicle_type');
        $trip_type = $this->post('trip_type');
        $lat1 = $this->post('pickup_lat');
        $lat2 = $this->post('drop_lat');
        $long1 = $this->post('pickup_lng');
        $long2 = $this->post('drop_lng');
        /* $lat1 = 28.408912;
          $lat2 = 28.459497;
          $long1 = 77.317789;
          $long2 = 77.026638; */
        $distance_data = $this->GetDrivingDistance($lat1, $lat2, $long1, $long2);
        $date = date('Y-m-d');
        $promodata = $this->restapi_model->getpromocode($promocode, $date);
        print_r($promodata);
        die();
        if ($promodata[0]->discount_amount != null) {
          $km = str_replace(' km', $distance_data[distance]);
          $ratecard_data = $this->restapi_model->getratecard($trip_type, $vehicle_type);
          $charges = $ratecard_data[0]->charges;
          $distance_data['Price'] = ($km * $charges) - $promodata[0]->discount_amount;
        } else {
          $km = str_replace(' km', $distance_data[distance]);
          $ratecard_data = $this->restapi_model->getratecard($trip_type, $vehicle_type);
          $charges = $ratecard_data[0]->charges;
          $distance_data['Price'] = ($km * $charges) - $promodata[0]->discount_amount;
        }
        //$result = $this->restapi_model->drivershift($data);
        $output_data = array();
        if ($result > 0) {
          //$output_data['latlng'] = $result[0]->latlng;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function getlongtripcity_get() {
    $result = $this->restapi_model->getlongtripcity();
    $output_data['city_list'] = $result;
    $output_data['message'] = 'Success';
    $output_data['status'] = 1;
    $this->response($output_data);
    return;
  }

  public function testnotify_get() {
    $msg_payload = array(
        'mtitle' => "Test",
        'mdesc' => "This is Test"
    );
    $this->iOS($msg_payload, "339282C0A5A6A9C94207C96184ECA30BF1B13DF7F2A206E69F6456E4E93D3F73", "driver");

    $this->android($msg_payload, "cpFfGdPeuHE:APA91bGzsrw_9vUWv013nYRzwCaUlzyQHjVny5o0glrl2lu90L1K7YBVBXANC84LYXngTAWY7LwP8k2H8h3xaRw4W9vobU0jjcQQAIyr8JiDyFPLyldcqacYX57VMD-TZ2mRCSInGJfM", "driver");

    $this->iOS($msg_payload, "339282C0A5A6A9C94207C96184ECA30BF1B13DF7F2A206E69F6456E4E93D3F73", "customer");

    $this->android($msg_payload, "cpFfGdPeuHE:APA91bGzsrw_9vUWv013nYRzwCaUlzyQHjVny5o0glrl2lu90L1K7YBVBXANC84LYXngTAWY7LwP8k2H8h3xaRw4W9vobU0jjcQQAIyr8JiDyFPLyldcqacYX57VMD-TZ2mRCSInGJfM", "customer");
  }

  public function cancelride_post() {
    $headers = $this->input->request_headers();
    //print_r($headers);die();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $ride_id = $this->post('ride_id');
        $driver_id = $token->id;
        $cancel_option = $this->post('cancel_option');

        $this->model->u_data("rides", ['cancel_option' => $cancel_option], ['id' => $ride_id, 'driverid' => $driver_id]);

        $this->response(['status' => '1', 'message' => 'Successfully canceled.']);
        return;
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

}
