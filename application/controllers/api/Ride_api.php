<?php

//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Rest_api extends REST_Controller {

  function __construct() {
    // Call the Model constructor
    parent::__construct();

    $this->load->model('restapi_model');
    $this->load->model('user_model');
    $this->load->model('model');
  }
  
  /* ----------
  * Book Ride
  */
  public function prebookride_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        //$ride_data = array();

        $ride_data = $this->input->post(array('trip_id', 'vehicle_type', 'booking_time', 'pickup_location', 'pickup_lat', 'pickup_lng', 'drop_location', 'drop_lat', 'drop_lng'));
        $ride_data['userid'] = $userid;
        $ride_data['GPS_Starting_Point'] = json_encode(array($ride_data['pickup_lat'], $ride_data['pickup_lng']));
        unset($ride_data['pickup_lat']);
        unset($ride_data['pickup_lng']);
        $ride_data['GPS_Destination_Point'] = json_encode(array($ride_data['drop_lat'], $ride_data['drop_lng']));
        unset($ride_data['drop_lat']);
        unset($ride_data['drop_lng']);
        $ride_data['vehicle_id'] = 1;
        $ride_data['driverid'] = 1;
        $ride_data['distance'] = 50;
        $ride_data['calculated_price'] = 500;
        //print_r($ride_data);die();
        $result = $this->restapi_model->add_ridedata($ride_data);
        if ($result != null) {

          $output_data['data'] = array('ride_id' => $result, 'distance' => '50 KM', 'calculated_price' => '500');
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }
  
  /* ------------
  * Confirm ride
  */
  public function confirmride_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        $ride_id = $this->post('ride_id');
        $result = $this->restapi_model->confirm_ride($userid, $ride_id);
        if ($result != null) {
          $ride_data = $this->restapi_model->get_ridedetail($ride_id);
          //print_r($ride_data);die();
          $output_data['driver'] = $this->restapi_model->get_driver2($ride_data[0]->driverid);
          $output_data['vehicle'] = $this->restapi_model->getvehicleinfo($ride_data[0]->driverid, $ride_data[0]->vehicle_id);

          $output_data['driver'][0]['latitude'] = "28.4595";
          $output_data['driver'][0]['longitude'] = "77.0266";
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }
  
  /* ----------------
  * Trip type
  */
  public function triptype_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;

        $result = $this->restapi_model->get_triptype();
        if ($result != null) {
          $output_data['vehicle'] = $this->restapi_model->get_vehicletype();
          $output_data['trip'] = $result;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }
  
  private function set_photoupload_options() {
    //upload an image options
    $config = array();
    $config['upload_path'] = '././assets/documents/';
    $config['allowed_types'] = 'gif|jpg|png';
    //$config['max_size']      = '0';
    $config['overwrite'] = FALSE;

    return $config;
  }
}