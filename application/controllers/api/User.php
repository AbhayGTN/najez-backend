<?php
defined('BASEPATH') or exit('.');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class User extends REST_Controller {

  function __construct() {

    parent::__construct();

    $this->load->model('restapi_model');
    $this->load->model('user_model');
    $this->load->model('driver_model');
    $this->load->model('model');
	$this->load->library('notifylib');
  }

   public function social_login_post() {
    $data = $this->input->post(array('emailid', 'fname', 'gender', 'login_type'));
	$lang = $this->input->post('lang');
    //$data2 = $this->input->post(array('device','device_token'));
	$split_name = str_split($data['fname'], 3);
    $user_referral = strtoupper($split_name[0]) . rand(1111, 9999);
	$data['referralcode'] = $user_referral;
    $check_email = $this->user_model->get_user($data['emailid']);
    if (!empty($check_email)) {
      $this->model->u_data("users", $data, ['emailid' => $data['emailid']]);

      $user22 = $this->user_model->get_user($data['emailid']);
	  if($this->input->post('device_token')!=''){
		  //echo $this->input->post('device_token');die();
		  
	  	$input = array('userid' => $user22[0]['id'],'device' => $this->input->post('device'), 'device_token' => $this->input->post('device_token'), 'device_os' => $device_os, 'device_model' => $device_model);
	  	$player_id = $this->getPlayerId($input);
		$this->model->u_data("push_notification_user", ['device' => $this->input->post('device'), 'device_token' => $this->input->post('device_token'),  'player_id' => $player_id], ['userid' => $user22[0]['id']]);
	
	  }else{
		  //$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('empty_deviceid','common',$lang);
		  $this->response(['status' => 0, 'message' => $reponse_msg]);
		  return;
	  }
      $tokenData['id'] = $user22[0]['id'];
	  $user22[0]['photo'] = base_url().$user22[0]['photo'];
	   //$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('loggedin','common',$lang);
      $this->response(['status' => 1, 'message' => $reponse_msg, 'token' => Authorization::generateToken($tokenData), 'userdata' => $user22[0]]);
	  return;
    } else {
		$digits = 4;
		$data['unique_id'] = "USR" . str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
		$this->model->i_data("users", $data);
		 $user22 = $this->user_model->get_user($data['emailid']);
		$data2 = $this->input->post(array('device','device_token'));
		//print_r($data2);
		$data2['userid'] = $user22[0]['id'];
		//print_r($data2);die();
     // $user22 = $this->user_model->get_user($data['emailid']);
	 //echo $user22[0]['id'];die();
	 
      if($this->input->post('device_token')!=''){
		$this->model->i_data("push_notification_user", $data2);
	  }else{
		  //$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('empty_deviceid','common',$lang);
		  $this->response(['status' => 0, 'message' => $reponse_msg]);
		  return;
	  }
	  $user22[0]['photo'] = base_url().$user22[0]['photo'];
	  //$user22[0]['photo'] = 'test';
      $tokenData['id'] = $user22[0]['id'];
	  //$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('loggedin','common',$lang);
      $this->response(['status' => 1, 'message' => $reponse_msg, 'token' => Authorization::generateToken($tokenData), 'userdata' => $user22[0]]);
	  return;
    }
  }

  public function cancel_options_get() {
    $data = $this->model->g_data('master_user_cancel_options');
    $this->response(['status' => '1', 'data' => $data]);
  }

  public function social_login_step2_post() {
    $input = $this->input->post(['mobile', 'otp', 'user_id', 'lang']);
    $data = $this->model->valid_otp($input['mobile'], $input['otp']);
    if (empty($data)) {
		$lang = $input['lang'];
		$reponse_msg = $this->notifylib->getresponse('opt_not_match','common',$lang);
      $this->response(['status' => 0, 'message' => $reponse_msg]);
    } else {
		$lang = $input['lang'];
		$reponse_msg = $this->notifylib->getresponse('opt_match','common',$lang);
      $this->model->u_data("users", ['verified' => 1], ['id' => $input['user_id']]);
      $this->response(['status' => 1, 'message' => $reponse_msg]);
    }
  }

  public function user_details_get($userid = '') {
    //$data = $this->restapi_model->get_userdata($userid);
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $user_data = $this->restapi_model->get_user($token->id);
		$lang = $this->notifylib->getlangkey('user',$token->id);
        if (!empty($user_data)) {
          $output_data['userdata'] = $user_data[0];
		  
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
			//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  

  public function userdevice_post() {

    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      //print_r($token);die();
      if ($token != false) {
        $userid = $token->id;
		$lang = $this->notifylib->getlangkey('user',$token->id);
        $device_token = $this->post('device_token');
        $device = $this->post('device_type');
        $result = $this->restapi_model->token_user($userid, $device_token, $device);
        if ($result > 0) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;

          $this->response($output_data);
          return;
        } else {
			//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;

          $this->response($output_data);
          return;
        }
      }
    } 
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function user_get($userid = '') {
    //$data = $this->restapi_model->get_userdata($userid);
    $headers = $this->input->request_headers();
   
    if (Authorization::tokenIsExist($headers)) {
      
      $token = Authorization::validateToken($headers['Authorization']);
     
      if ($token != false) {
        
        $user_data = $this->restapi_model->get_user($token->id);
       
		$lang = $this->notifylib->getlangkey('user',$token->id);
        if (!empty($user_data)) {
          $output_data['userdata'] = $user_data[0];
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
			//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function userlogin_post() {
    $dataPost = $this->input->post();
	//$this->response(['status'=>1,'message'=>$dataPost]);
   //  return;
    $user = $this->user_model->userlogin($dataPost['mobile'], $dataPost['password']);
	$lang = $dataPost['lang'];
    if ($user != null) {
    $input = $this->input->post(['device_token','device','device_model','device_os']);
      if(empty($input['device_token']))
    {
		//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('empty_deviceid','common',$lang);
      $this->response(['status'=>0,'message'=>$reponse_msg]);
      return;
    }

    $input['userid'] = $user->id;
    $player_id = $this->getPlayerId($input);
    
		$this->model->d_data('push_notification_user',['userid'=>$user->id]);
		$this->model->i_data("push_notification_user", ['userid' => $user->id, 'device_token' => $input['device_token'], 'device' => $input['device'], 'player_id' => $player_id]);
		//$this->model->u_data("push_notification_user", ['device_token' => $input['device_token'], 'device' => $input['device']], ['userid' => $user->id]);
	// $this->response(['status'=>1,'message'=>$this->db->last_query()]);
	//  return;

      $tokenData = array();
      $tokenData['id'] = $user->id;
      $response['token'] = Authorization::generateToken($tokenData);
      $response['userdata'] = $user;
	  //$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('loggedin','common',$lang);
      $response['message'] = $reponse_msg;
      $response['status'] = 1;
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    } else {
		//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('detailnotvalid','common',$lang);
      $response = [
          'status' => 0,
          'message' => $reponse_msg,
      ];
      $this->set_response($response, REST_Controller::HTTP_OK);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  
  public function getPlayerId($inputArray){
  
  	/* OneSignal Code */
  	 
  /*	$returnData = $this->model->checkNotificationId($inputArray['userid'],'push_notification_user','userid');
  	 
  	//print_r($returnData);die;
  	if(!empty($returnData[0]['player_id'])){
  		return $returnData[0]['player_id'];
  	}*/
  	 
  	if ($inputArray['device'] == 'android'){
  		$device_type = 1;
  		$app_id = '2c11876c-6c3e-471d-b077-987c10c316bd';
  	}else{
  		$device_type = 0;
  		$app_id = '2c11876c-6c3e-471d-b077-987c10c316bd';
  	}
  	$fields = array(
  			'app_id' => $app_id,
  			'identifier' => $inputArray['device_token'],
  			'language' => "en",
  			'timezone' => "-28800",
  			'game_version' => "1.0",
  			'device_os' => $inputArray['device_os'],
  			'device_type' => $device_type,
  			'device_model' => $inputArray['device_model']
  			//'tags' => array("foo" => "bar")
  	);
  
  	$fields = json_encode($fields);
  	//	print("\nJSON sent:\n");
  	//	print($fields);
  
  	$ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players");
  	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  	curl_setopt($ch, CURLOPT_HEADER, FALSE);
  	curl_setopt($ch, CURLOPT_POST, TRUE);
  	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
  	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  
  	$response = curl_exec($ch);
  	curl_close($ch);
  
  	//	$return["allresponses"] = $response;
  	//$return = json_encode( $return);
  	$response = json_decode( $response);
  	//print_r($return); die;
  	//echo $response->id; die;
  	if($response->success == 'true'){
  		return $response->id;
  	}else{
  		return ;
  	}
  }

  public function userregister_post() {

    $username = $this->post('first_name');
    $emailid = $this->post('emailid');
    $first_name = $this->post('first_name');
    $password = $this->post('password');
    $photo = $this->post('photo');
    $gender = $this->post('gender');
    $mobile = $this->post('mobile');
    $city = $this->post('city');
    $lat = $this->post('lat');
    $lng = $this->post('lng');
    $device = $this->post('device');
    $device_token = $this->post('device_token');
    $device_os = $this->post('device_os');
    $device_model = $this->post('device_model');
    $latlng = json_encode(array($lat, $lng));
    $referralcode = $this->post('referralcode');
    $lang = $this->post('lang');
    if(empty($device_token))
    {
		//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('empty_deviceid','common',$lang);
      $this->response(['status'=>0,'message'=>$reponse_msg]);
      return;
    }
    
    if (!empty($referralcode)) {
      $check_ref = $this->model->s_data("referralcode", "users", ['referralcode' => $referralcode]);
      if (empty($check_ref)) {
		  //$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('invalid_ref','common',$lang);
        $array = ['status' => 0, 'message' => $reponse_msg];
        //echo json_encode($array);
        $this->response($array);
        return;
      }
    }

    $user_type = 3;
    $status = 1;
    $email_count = $this->restapi_model->checkemailid_user($emailid);
    if ($email_count > 0) {
		//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('emailid_exists','common',$lang);
      $output_data['message'] = $reponse_msg;
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    $mobile_count = 0;
    if ($mobile != '') {
      $mobile_count = $this->restapi_model->checkmobile_user($mobile);
    }

    if ($mobile_count > 0) {
		//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('mobile_exists','common',$lang);
      $output_data['message'] = $reponse_msg;
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    if ($email_count == 0 && $mobile_count == 0) {


      $curl = curl_init();
      //$otp = rand(1111, 9999);
      $otp = 1234;
	 // $lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('opt_sms','common',$lang);
      $message = $reponse_msg;
      curl_setopt_array($curl, array(
          CURLOPT_URL => "http://oursms.net/api/sendsms.php",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}{$otp}&numbers={$mobile}&sender=NAJEZO&return=json",
          CURLOPT_HTTPHEADER => array(
              "cache-control: no-cache",
              "content-type: application/x-www-form-urlencoded",
              "postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
          ),
      ));

      $this->model->i_data("sms_otp_temp", ['mobile' => $mobile, 'otp' => $otp]);
      $response = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);



      //die();

      if (!empty($photo)) {
        $data = str_replace('data:image/png;base64,', '', $photo);
        $encodedData = str_replace(' ', '+', $data);
        $decodedData = base64_decode($encodedData);
        $filename = uniqid() . '.jpg';
        file_put_contents('././assets/documents/' . $filename, $decodedData);
        if (file_exists("././assets/documents/" . $filename)) {
          $photo = "assets/documents/" . $filename;
        } else {
          $photo = "assets/documents/user-icon.png";
        }
      } else {
        $photo = "assets/documents/user-icon.png";
      }
      if ($referralcode != '') {
        $reffer_count = $this->restapi_model->checkreferralcode_user($referralcode);
        if ($reffer_count > 0) {
          $total_amount = 20;
        } else {
          $total_amount = 0;
        }
      } else {
        $total_amount = 0;
      }
      $split_name = str_split($username, 3);
      $user_referral = strtoupper($split_name[0]) . rand(1111, 9999);
      $data = $this->restapi_model->add_user($username, $emailid, $first_name, $password, $photo, $mobile, $city, $user_type, $gender, $status, $latlng, $total_amount, $user_referral, $referralcode);
      //print_r($data);die();
      if (!empty($data) && $data > 0) {
		 // echo $this->db->last_query();
        $user_data = $this->restapi_model->get_user($data);
		// echo $this->db->last_query();
		//print_r($user_data);
		//die();
        if (!empty($user_data)) {
          $user_id = $user_data[0]->id;

        //onesignal code
          $input = array('userid' => $user_id,'device' => $device, 'device_token' => $device_token, 'device_os' => $device_os, 'device_model' => $device_model);
          $player_id = $this->getPlayerId($input);
          
          $this->model->i_data("push_notification_user", ['device' => $device, 'device_token' => $device_token, 'userid' => $user_data[0]->id, 'player_id' => $player_id]);
			if(!empty($referralcode))
			{
			  $this->model->u_data("users", ['is_referred' => $referralcode], ['id' => $user_data[0]->id]);
			$this->model->i_data("user_referal_code", ['user_id' => $check_ref[0]['id'], 'referal_user_id' => $user_data[0]->id, 'referal_code' => $referralcode,'status'=>'1']);
			}


          $emailid = $user_data[0]->emailid;
         // $this->sendusermail($user_id, $emailid);

          $tokenData['id'] = $user_data[0]->id;
          $output_data['token'] = Authorization::generateToken($tokenData);
          $output_data['userdata'] = $user_data[0];
		 //  $lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('newregister','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      } else {
		 // $lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
        $output_data['message'] = $reponse_msg;
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    } else {
		//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
      $output_data['message'] = $reponse_msg;
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function userupdate_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
		$lang = $this->notifylib->getlangkey('user',$token->id);
        //$emailid = $this->post('emailid');
        $first_name = $this->post('first_name');
        $middle_name = $this->post('middle_name');
        $last_name = $this->post('last_name');
        $photo = $this->post('photo');
        $gender = $this->post('gender');
        //$mobile = $this->post('mobile');
        /*
          $email_count = $this->restapi_model->checkemailid_user($emailid);
          if($email_count > 0){
          $output_data['message'] = 'Email already registered.';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
          }
          $mobile_count = 0;
          if($mobile!=''){
          $mobile_count = $this->restapi_model->checkmobile_user($mobile);
          }

          if($mobile_count > 0){
          $output_data['message'] = 'Mobile no. already registered.';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
          } */
        //if($email_count == 0 && $mobile_count == 0){
        if (!empty($photo)) {
          $data = str_replace('data:image/png;base64,', '', $photo);
          $encodedData = str_replace(' ', '+', $data);
          $decodedData = base64_decode($encodedData);
          $filename = uniqid() . '.jpg';
          file_put_contents('././assets/documents/' . $filename, $decodedData);
          if (file_exists("././assets/documents/" . $filename)) {
            $photo = "assets/documents/" . $filename;
          } else {
            $photo = "assets/documents/user-icon.png";
          }
        } else {
          $photo = "assets/documents/user-icon.png";
        }
        /* if($referralcode != ''){
          $reffer_count = $this->restapi_model->checkreferralcode_user($referralcode);
          if($reffer_count > 0){
          $total_amount = 20;
          }else{
          $total_amount = 0;
          }
          }else{
          $total_amount = 0;
          }
          $user_referral = $username.rand(); */
        $data = $this->restapi_model->update_user($first_name, $middle_name, $last_name, $photo, $gender, $userid);
        //print_r($data);die();
        if ($data >= 0) {
          $user_data = $this->restapi_model->get_user($userid);
          if (!empty($user_data)) {
            $tokenData['id'] = $user_data[0]->id;
            $output_data['token'] = Authorization::generateToken($tokenData);
            $output_data['userdata'] = $user_data[0];
			
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('profile_update','common',$lang);
            $output_data['message'] = $reponse_msg;
            $output_data['status'] = 1;
            $this->response($output_data);
            return;
          } else {
			//  $lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
            $output_data['message'] =$reponse_msg;
            $output_data['status'] = 0;
            $this->response($output_data);
            return;
          }
        } else {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
        /* }else{
          $output_data['message'] = 'Already Registered.';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
          } */
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function userreview_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
		$lang = $this->notifylib->getlangkey('user',$token->id);
        $ride_id = $this->post('ride_id');
        $star = $this->post('star');
        $review = $this->post('review');
		$user_data = $this->model->s_data("driverid", "rides", ['id'=>$ride_id]);
				
		$driverid = $user_data[0]['driverid'];
        $result = $this->restapi_model->add_driver_review($driverid, $ride_id, $star, $review);
        if (!empty($result)) {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('driver_review','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function usersetpassword_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
		$lang = $this->notifylib->getlangkey('user',$token->id);
        $user_data = array();
        $user_data['password'] = $this->post('password');
        $user_data['webstatus'] = 0;
        $result = $this->restapi_model->usersetpassword($userid, $user_data);
        //print_r($result);die();
        if ($result != null) {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('password_reset','common',$lang);
          $output_data['message'] = $reponse_msg;
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
		//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
        $output_data['message'] = $reponse_msg;
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
      $response = [
          'status' => REST_Controller::HTTP_UNAUTHORIZED,
          'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  public function useremailunicode_post() {

    $user_data = array();
    $emailid = $this->post('emailid');
    $email_count = $this->restapi_model->checkemailid_user($emailid);
    if ($email_count > 0) {
      $digits = 4;
      $resetcode = str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
      $user_data['resetcode'] = $resetcode;
      $result = $this->restapi_model->useremailunicode($emailid, $user_data);
      if ($result) {
        $message = '<html><body>';
        $message .= '<h3>Hello,</h3>';
        //$message .= '<p>Thankyou for submitting the details. We will keep you posted.</p>';
        $message .= '<p>To verify your emailid, please enter below mentioned code in mobile app</p>';
        //$code = $resetcode;
        //$link = '<a target="_blank" href="'.base_url('verify/activate/'.$code).'">'.base_url('verify/activate/'.$code).'</a>';
        $message .= '<p>Code: <b>' . $resetcode . '</b></p>';
        $message .= '</body></html>';
        $this->email->from('info@najez-online.com', 'Najez');
        $this->email->to($emailid);
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');
        $this->email->subject('Reset Code');
        $this->email->message($message);
        $this->email->set_mailtype("html");
        $this->email->send();

        if ($result != null) {
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        }
      }
      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Email not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;

    /* }
      $response = [
      'status' => REST_Controller::HTTP_UNAUTHORIZED,
      'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return;
      }
      $response = [
      'status' => REST_Controller::HTTP_UNAUTHORIZED,
      'message' => 'Unauthorized',
      ];
      $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
      return; */
  }

  public function userresetpassword_post() {
    $user_data = array();
    $emailid = $this->post('emailid');
    $password = $this->post('password');
    $code = $this->post('code');
    //$email_count = $this->restapi_model->checkemailid_user($emailid);
    $code_check = $this->restapi_model->userverifyunicode($emailid, $code);
    if ($code_check != null && $code_check == 1) {

      $code_check = $this->restapi_model->userresetpassword($emailid, $password, $code);

      if ($code_check != null && $code_check == 1) {
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }

      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Code not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
  }

  public function user_password_reset_post() {
    $input = $this->input->post(['mobile', 'otp', 'new_password', 'lang']);
    $data = $this->model->valid_otp($input['mobile'], $input['otp']);
    if (empty($data)) {
		$lang = $input['lang'];
		$reponse_msg = $this->notifylib->getresponse('opt_not_match','common',$lang);
      $this->response(['status' => 0, 'message' => $reponse_msg]);
    } else {
		$lang = $input['lang'];
		$reponse_msg = $this->notifylib->getresponse('password_reset','common',$lang);
      $this->model->u_data("users", ['password' => $input['new_password']], ['mobile' => $input['mobile']]);
      $this->response(['status' => 1, 'message' => $reponse_msg]);
    }
  }

  public function bookride_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        //$ride_data = array();

        $ride_data = $this->input->post(array('trip_id', 'vehicle_type', 'booking_time', 'pickup_location', 'pickup_lat', 'pickup_lng', 'drop_location', 'drop_lat', 'drop_lng', 'scheduled', 'longtrip_city_id'));
        $ride_data['userid'] = $userid;
        $ride_data['GPS_Starting_Point'] = json_encode(array($ride_data['pickup_lat'], $ride_data['pickup_lng']));
        unset($ride_data['pickup_lat']);
        unset($ride_data['pickup_lng']);
        $ride_data['GPS_Destination_Point'] = json_encode(array($ride_data['drop_lat'], $ride_data['drop_lng']));
        unset($ride_data['drop_lat']);
        unset($ride_data['drop_lng']);
        $ride_data['promo_code'] = $this->input->post('promo_code');
        $ride_data['payment_method'] = $this->input->post('payment_method');
        $ride_data['distance'] = 50;
        $ride_data['calculated_price'] = 500;
        //print_r($ride_data);die();
        $result = $this->restapi_model->add_ridedata($ride_data);
		//echo $this->db->last_query();die();
        if ($result != null) {

          $output_data['driver_data'] = array('driver_data' => 'Test');
          $output_data['ride_data'] = $result;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error1';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error2';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }

  public function estimated_post() {
   // $ride_id = $this->post('ride_id');
    //$driver_id = $token->id;
	$headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
		  $lang = $this->notifylib->getlangkey('user',$token->id);
		$vehicle_type = $this->post('vehicle_type');
		$trip_type = $this->post('trip_type');
		$promo_code = $this->post('promo_code');
		$lat1 = $this->post('pickup_lat');
		$lat2 = $this->post('drop_lat');
		$long1 = $this->post('pickup_lng');
		$long2 = $this->post('drop_lng');  
		$distance_data = $this->GetDrivingDistance($lat1, $lat2, $long1, $long2);
	  // print_r($distance_data);die();

		$km = str_replace(' km', "", $distance_data['distance']);
		$ride_time = floor($distance_data['time'] / 60);
		//echo $ride_time;die();
		$init = $distance_data['time'];
		$hours = floor($init / 3600);
		$minutes = floor(($init / 60) % 60);
		$seconds = $init % 60;

		$distance_data['time'] = $hours.' Hour '.$minutes.' Minute';
		//$charge_type = 1; /* 1:Per Minute,2:Per KM,3:Ride Now Starting,4:Ride Now Minimum,5:Ride Later Starting,6:Ride Later Minimum */
		
		$ratecard_data = $this->restapi_model->getratecard($trip_type, $vehicle_type);
		//print_r($ratecard_data);die();
		$permin_charge = $ratecard_data[0]->charges;
		$perkm_charge = $ratecard_data[1]->charges;
		$rnstart_charge = $ratecard_data[2]->charges;
		$rnmin_charge = $ratecard_data[3]->charges;
	 //   $rlstart_charge = $ratecard_data[4]->charges;
	  //  $rlmin_charge = $ratecard_data[5]->charges;
	  
		$price = $rnstart_charge + ($km * $perkm_charge) + ($ride_time * $permin_charge);
		if($price<$rnmin_charge){
			$price = $rnmin_charge;
		}
		$range = ($price * 20) / 100;
		$rangeprice = $price - $range;
		$distance_data['Price'] = 'SAR '.$rangeprice.' - SAR '.$price;
		if (!empty($promo_code)) {
		  //$promo_price = $this->model->s_data("discount_amount,discount_percentage", "promotions", ['promo_code' => $promo_code]);
		  $date = date('Y-m-d');
		  $promo_price = $this->restapi_model->getpromocode($promo_code,$date);
		  if(!empty($promo_price)){
			  if ($promo_price[0]->discount_amount != NULL) {
				$distance_data['Price'] = $distance_data['Price'] - $promo_price[0]->discount_amount;
			  } elseif ($promo_price[0]->discount_percentage != NULL) {
				$amount = $distance_data['Price'] * $promo_price[0]->discount_percentage / 100;
				$distance_data['Price'] = $distance_data['Price'] - $amount;
			  }
		  }else{
			  $lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('invalid_promo','common',$lang);
			   $output_data['message'] = $reponse_msg;
			  $output_data['status'] = 0;
			  $this->response($output_data);
			  return;
		  }
		}


		$output_data = array();
		if ($distance_data != null) {
		  $output_data['distance_data'] = $distance_data;
		  $output_data['message'] = 'Success';
		  $output_data['status'] = 1;
		  $this->response($output_data);
		  return;
		} else {
			//$lang = 'en';
			$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
		  $output_data['message'] = $reponse_msg;
		  $output_data['status'] = 0;
		  $this->response($output_data);
		  return;
		}
	 }
		  $response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	$response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }

  

  public function userverifyunicode_post() {
    $user_data = array();
    $emailid = $this->post('emailid');
    $code = $this->post('code');
    $email_count = $this->restapi_model->checkemailid_user($emailid);
    if ($email_count > 0) {

      $resetcode = $code;
      $code_check = $this->restapi_model->userverifyunicode($emailid, $resetcode);

      if ($code_check != null && $code_check == 1) {
        $output_data['message'] = 'Success';
        $output_data['status'] = 1;
        $this->response($output_data);
        return;
      }

      $output_data['message'] = 'Error';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    } else {
      $output_data['message'] = 'Email not valid';
      $output_data['status'] = 0;
      $this->response($output_data);
      return;
    }

    $output_data['message'] = 'Error';
    $output_data['status'] = 0;
    $this->response($output_data);
    return;
  }

  public function social_send_otp_post() {
    $headers = $this->input->request_headers();

    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
		//echo $userid;die();
		 $lang = $this->notifylib->getlangkey('user',$token->id);

        $mobile = $this->input->post('mobile');
        $data = $this->model->s_data("mobile", "users", ['mobile' => $mobile]);
        if (!empty($data)) {
			//$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('social_no','user',$lang);
          $this->response(['status' => 0, 'message' => $reponse_msg]);
          return;
        } else {
			
          $this->model->u_data("users", ['mobile' => $mobile], ['id' => $userid]);

          //$this->restapi_model->save_device_token($mobile,$device_token);
          $curl = curl_init();
         // $otp = rand(1111, 9999);
          $otp = 1234;
		  //$lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('opt_sms','common',$lang);
          $message = $reponse_msg;
          curl_setopt_array($curl, array(
              CURLOPT_URL => "http://oursms.net/api/sendsms.php",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "username=najez-online&password=Najez12&message={$message}{$otp}&numbers={$mobile}&sender=NAJEZO&return=json",
              CURLOPT_HTTPHEADER => array(
                  "cache-control: no-cache",
                  "content-type: application/x-www-form-urlencoded",
                  "postman-token: 95e49bca-26a8-6601-7dd3-4ed5b7809fcb"
              ),
          ));
          $this->model->i_data("sms_otp_temp", ['mobile' => $mobile, 'otp' => $otp]);
          $response = curl_exec($curl);
          $err = curl_error($curl);
          curl_close($curl);


          if ($err) {
			//  $lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
            $this->response(['status' => 0, 'message' => $reponse_msg]);
            return;
          } else {
			 // $lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('opt_match','common',$lang);
            $this->response(['status' => 1, 'message' => $reponse_msg]);
            return;
          }
        }
      }
    }
  }
  
  public function applypromo_post() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
		   $lang = $this->notifylib->getlangkey('user',$token->id);
        $promocode = $this->post('promocode');
        //$data['driver_id'] = $token->id;
        $vehicle_type = $this->post('vehicle_type');
        $trip_type = $this->post('trip_type');
        $lat1 = $this->post('pickup_lat');
        $lat2 = $this->post('drop_lat');
        $long1 = $this->post('pickup_lng');
        $long2 = $this->post('drop_lng');
        /* $lat1 = 28.408912;
          $lat2 = 28.459497;
          $long1 = 77.317789;
          $long2 = 77.026638; */
        $distance_data = $this->GetDrivingDistance($lat1, $lat2, $long1, $long2);
        $date = date('Y-m-d');
        $promodata = $this->restapi_model->getpromocode($promocode, $date);
        print_r($promodata);
        die();
        if ($promodata[0]->discount_amount != null) {
          $km = str_replace(' km', $distance_data[distance]);
          $ratecard_data = $this->restapi_model->getratecard($trip_type, $vehicle_type);
          $charges = $ratecard_data[0]->charges;
          $distance_data['Price'] = ($km * $charges) - $promodata[0]->discount_amount;
        } else {
          $km = str_replace(' km', $distance_data[distance]);
          $ratecard_data = $this->restapi_model->getratecard($trip_type, $vehicle_type);
          $charges = $ratecard_data[0]->charges;
          $distance_data['Price'] = ($km * $charges) - $promodata[0]->discount_amount;
        }
        //$result = $this->restapi_model->drivershift($data);
        $output_data = array();
        if ($result > 0) {
          //$output_data['latlng'] = $result[0]->latlng;
          $output_data['message'] = 'Promo code Successfully applied';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }
      }
    }
    $response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  
  public function vehicletype_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;
        //$ride_data = array();
        //$ride_data['userid'] = $userid;
        $result = $this->restapi_model->get_vehicletype();
        if ($result != null) {

          $output_data['data'] = $result;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }
  
  public function triptype_get() {
    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
        $userid = $token->id;

        $result = $this->restapi_model->get_triptype();
        if ($result != null) {
          $output_data['vehicle'] = $this->restapi_model->get_vehicletype();
          $output_data['trip'] = $result;
          $output_data['message'] = 'Success';
          $output_data['status'] = 1;
          $this->response($output_data);
          return;
        } else {
          $output_data['message'] = 'Error';
          $output_data['status'] = 0;
          $this->response($output_data);
          return;
        }

        $output_data['message'] = 'Error';
        $output_data['status'] = 0;
        $this->response($output_data);
        return;
      }
    }
  }
    
   public function getlongtripcity_get() {
	    $headers = $this->input->request_headers();
    if (Authorization::tokenIsExist($headers)) {
      $token = Authorization::validateToken($headers['Authorization']);
      if ($token != false) {
				$result = $this->restapi_model->getlongtripcity();
				foreach($result as $key => $citydata){
					$cityname = $citydata->city_name_en;
					$location_data = $citydata->location_data;
					if(empty($location_data)){
						$url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$cityname.'&sensor=false';
						$json_data = file_get_contents($url);
						$result2 = json_decode($json_data, TRUE);
						$result[$key]->location = $result2['results'][0]['geometry']['location'];
						$this->restapi_model->updateCityLocation(serialize($result[$key]->location), $citydata->id);
					}else{
						$result[$key]->location = unserialize($location_data);
					}
					//$longitude = $result['results'][0]['geometry']['location']['lng'];
				}
				$output_data['city_list'] = $result;
				$output_data['message'] = 'Success';
				$output_data['status'] = 1;
				$this->response($output_data);
				return;
		}
		  $response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	$response = [
        'status' => REST_Controller::HTTP_UNAUTHORIZED,
        'message' => 'Unauthorized',
    ];
    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
    return;
  }
  public function ride_history_post(){
	  
		$headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$id = $token->id;
				$type = $this->post('type');
				$status = $this->post('status');
				$result = $this->restapi_model->get_ridehistory($id,$type,$status);
				   
				$this->response(['history'=>$result,'status'=>1,'message'=>'Success']);
				return;
			}
			$response = [
				'status' => REST_Controller::HTTP_UNAUTHORIZED,
				'message' => 'Unauthorized',
				];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
	}
	public function usercancelride_post(){
		$headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
		  $token = Authorization::validateToken($headers['Authorization']);
		  if ($token != false) {
				$ride_id = $this->post('ride_id');
				$user_id = $token->id;
				$cancel_option = $this->post('cancel_option');
				
				$this->model->u_data("rides", ['ride_flag'=>4,'cancel_option'=>$cancel_option], ['id'=>$ride_id,'userid'=>$user_id]);
				
				$user_data = $this->model->s_data("driverid", "rides", ['id'=>$ride_id]);
				
				$this->model->u_data('drivers',['ride_flag'=>0],["id" =>$user_data[0]['driverid']]);
				
				$device_data = $this->model->s_data("device_token,device", "push_notification_driver", ['driver_id'=>$user_data[0]['driverid']]);
				//print_r($device_data);die();
			
				
				$device_type = $device_data[0]['device'];
				$device_id = $device_data[0]['device_token'];
				$notify_user = 'driver';
				
				$msg_payload = array(
					'mtitle' => "Ride Notification",
					'mdesc' => "Customer has canceled the ride",
					'ride_data' => array('notify_type'=>'user_cancel_ride'),
					//'notify_type' => 'ride_notify',
				);
				
				if($device_type=='ios'){
				  $this->notifylib->iOS($msg_payload, $device_id, $notify_user);
				}
				if($device_type=='android'){
				   $this->notifylib->android($msg_payload, $device_id, $notify_user);
				}
				
				
				
					$this->response(['status'=>1,'message'=>'Successfully canceled.']);
					return;
				
		  }
		  $response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
			];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
		$response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
	}
	public function GetDrivingDistance($lat1, $lat2, $long1, $long2) {
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
	//print_r($response_a);die();
    //return $response_a;
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['value'];

    return array('distance' => $dist, 'time' => $time);
  }
  
  public function changepassword_post(){
	  $headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
		  $token = Authorization::validateToken($headers['Authorization']);
		  if ($token != false) {
			  $lang = $this->notifylib->getlangkey('user',$token->id);
			  $userid = $token->id;
			  $old_pass = $this->post('old_password');
			  $data['password'] = $this->post('new_password');
			  $userdata = $this->model->s_data("id", "users", ['password' => $old_pass,'id'=>$userid]);
			  if($userdata != null){
				  $result = $this->model->u_data("users", $data, ['id' => $userid]);
				  if($result!=null){
					 // $lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('password_reset','common',$lang);
					  $this->response(['status'=>1,'message'=>$reponse_msg]);
						return;
				  }else{
					 // $lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('server_err','common',$lang);
					  $this->response(['status'=>0,'message'=>$reponse_msg]);
						return;
				  }
			  }else{
				 // $lang = 'en';
		$reponse_msg = $this->notifylib->getresponse('detailnotvalid','common',$lang);
					  $this->response(['status'=>0,'message'=>$reponse_msg]);
						return;
				  }
		  }
		  $response = [
				'status' => REST_Controller::HTTP_UNAUTHORIZED,
				'message' => 'Unauthorized',
			];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
		$response = [
			'status' => REST_Controller::HTTP_UNAUTHORIZED,
			'message' => 'Unauthorized',
		];
		$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
		return;
  }
  public function scheduletrip_modify_post(){
	  
		$headers = $this->input->request_headers();
		//print_r($headers);die();
		if (Authorization::tokenIsExist($headers)) {
			$token = Authorization::validateToken($headers['Authorization']);
			if ($token != false) {
				$id = $token->id;
				$ride_id = $this->post('ride_id');
				$new_time = $this->post('new_time');
				
				$this->model->u_data("rides",["booking_time"=>$new_time],["id"=>$ride_id]);
				   
				$this->response(['status'=>1,'message'=>'Success']);
				return;
			}
			$response = [
				'status' => REST_Controller::HTTP_UNAUTHORIZED,
				'message' => 'Unauthorized',
				];
			$this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
			return;
		}
	}
}
