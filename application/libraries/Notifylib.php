<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifylib {

        protected $CI;

        // We'll use a constructor, as you can't directly call a function
        // from a property definition.
        public function __construct()
        {
                // Assign the CodeIgniter super-object
				//parent::__construct();
               $CI =& get_instance();
			   $CI->load->database();
			   $CI->load->model('model');
			   $CI->load->model('restapi_model');
				//$CI->load->helper('url');
				//$CI->load->library('session');
				//$CI->config->item('base_url');
        }
		
       /* public function bar()
        {
                echo $this->CI->config->item('base_url');
        }*/
		public function android($data, $reg_id, $type) {
			
			return $this->sendAndroidMessage($data, $reg_id, $type);
			/*if ($type == 'driver') {
			  $fcmApiKey = 'AAAAXWBM9m4:APA91bH-v7CnARBVt73Q0OHqd9xQcW2__BzeZogBj5BcbirU2Z1-0vKIjt8c80TiTOF3Ele6ceCHVcIM40K0PvS56VXHsxO_LpWfNNq7YrXyVcul8G5raHsWDU1gUOtooJn2eV-M_jak'; //App API Key(This is google cloud messaging api key not web api key)
			} else {
			  //$fcmApiKey = 'AAAAC4-Bpyk:APA91bH4ecJhI4yxKd2_e6Jjib6SYLzW7fpNaQZfNEyMG26-OKm51arJsr7nbJz4g-nfGvMG6oX3fYbe1bJ1V2q0gGq5gXhJd-ogPH0o7kLfgc90CDoqr6IbZ6cuzEXStdJk_7deM0YQ'; //App API Key(This is google cloud messaging api key not web api key)
			  $fcmApiKey = 'AAAASBERyqY:APA91bGBWDUhjoB9jLQ8eCYQz8n_Z1359XX5fts8LTL-IhyseXenQznMwOjBZ9Tman_yj9ixyHIIa4Nw9psQGjliERSVBscliBQmeOYE8VUDdv-S-rk6vTD9VftiSCUoj_rI2YduXl-S'; //App API Key(This is google cloud messaging api key not web api key)
			}


			$message = array
			(
			'message' 	=> $data['mdesc'],
			'title'		=> $data['mtitle'],
			'ride_data'		=> $data['ride_data'],
			//'notify_type' => $data['notify_type'],
			
			//'notify_type'		=> $data['notify_type'],
			//'subtitle'	=> 'This is a subtitle. subtitle',
			//'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
			//'vibrate'	=> 1,
			//'sound'		=> 1,
			//'largeIcon'	=> 'large_icon',
			//'smallIcon'	=> 'small_icon'
			);
			$fields = array(
			'registration_ids' => array($reg_id),
			'data'=> $message,
			);
			
			//print_r(json_encode($fields));die();
			
			$headers = array(
			'Authorization: key='.$fcmApiKey, // FIREBASE_API_KEY_FOR_ANDROID_NOTIFICATION
			'Content-Type: application/json'
			);
			// Open connection
			$ch = curl_init();
			// Set the url, number of POST vars, POST data
			curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			// Disabling SSL Certificate support temporarly
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			// Execute post
			$result = curl_exec($ch );
			if($result === false){
			die('Curl failed:' .curl_errno($ch));
			}
			// Close connection
			curl_close( $ch );
			//return $result;*/
			
		}

		
		public function sendAndroidMessage($data, $token, $type){
			$content = array(
					"en" => $data['mdesc']
			);
			$CI =& get_instance();
			$CI->load->database();
			$CI->load->model('model');
			 
			if ($type == 'driver') {
				$app_id = '2049813d-427b-40eb-8179-49bba22bfa4e';
				$returnData = $CI->model->getPlayerId($token,'push_notification_driver');
			}else{
				$app_id = '2c11876c-6c3e-471d-b077-987c10c316bd';
				$returnData = $CI->model->getPlayerId($token,'push_notification_user');
			}
			 
			if(empty($returnData[0]['player_id'])){
				return false;
			}
				
			$fields = array(
					'app_id' => $app_id,
					'include_player_ids' => array($returnData[0]['player_id']),
					'data' => array('ride_data' => $data['ride_data']),
					'contents' => $content
			);
		
			$fields = json_encode($fields);
			//print("\nJSON sent:\n");
			//print($fields);
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
			'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
			$response = curl_exec($ch);
			curl_close($ch);
		
			//return $response;
		
		
			$response = sendMessage();
			$return["allresponses"] = $response;
			$return = json_encode( $return);
			return true;
		}
	  // Sends Push notification for iOS users
	  public function iOS($data, $token, $type) {


	  		return $this->sendMessage($data, $token, $type);
			//$apnsHost = 'gateway.push.apple.com';
			//$apnsHost = 'gateway.sandbox.push.apple.com';
			/*if ($type == 'driver') {
			  $apnsCert = BASEPATH . '../assets/Certificates_driver_dev.pem';
			  $apnsHost = 'gateway.sandbox.push.apple.com';
			  //$apnsCert = BASEPATH . '../assets/live/Driver_Certificates_dist.pem';
			} else {
			  //$apnsCert = BASEPATH . '../assets/Certificates_custmor_dev.pem';
			  $apnsHost = 'gateway.push.apple.com';
			  $apnsCert = BASEPATH . '../assets/live/Cust_Certificates_dist.pem';
			}

			$apnsPort = 2195;
			//$apnsPass = '<PASSWORD_GOES_HERE>';
			$token = $token;

			
			  
			  //print_r($payload);
			$payload['aps'] = array('alert' => array('title' => $data['mtitle'],'body' => $data['mdesc']), 'badge' => 1, 'sound' => 'noti.mp3', 'content-available' => 1);
			$payload['custom'] = $data['ride_data'];
			//$payload['notify_type'] = $data['notify_type'];
			
			//echo json_encode($payload); die();
			
			$output = json_encode($payload);

			$token = pack('H*', str_replace(' ', '', $token));
			$apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;

			$streamContext = stream_context_create();
			stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
			//stream_context_set_option($streamContext, 'ssl', 'passphrase', $apnsPass);

			$apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
			if (!$apns)
			  exit("Failed to connect: $error $errorString" . PHP_EOL);

			fwrite($apns, $apnsMessage, strlen($apnsMessage));
			stream_set_blocking ($apns, 0);
			fclose($apns);
			$this->checkAppleErrorResponse($apns);*/
			//return $apns;
		  }
	

		public function sendMessage($data, $token, $type){
		  	$content = array(
		  			"en" => $data['mdesc']
		  	);
		  	$CI =& get_instance();
		  	$CI->load->database();
		  	$CI->load->model('model');
		  	
		  	if ($type == 'driver') {
		  		$app_id = '2049813d-427b-40eb-8179-49bba22bfa4e';
		  		$returnData = $CI->model->getPlayerId($token,'push_notification_driver');
		  	}else{
		  		$app_id = '2c11876c-6c3e-471d-b077-987c10c316bd';
		  		$returnData = $CI->model->getPlayerId($token,'push_notification_user');
		  	}
		  	
		  	if(empty($returnData[0]['player_id'])){
		  		return false;
		  	}
			
		  	$fields = array(
		  			'app_id' => $app_id,
		  			'include_player_ids' => array($returnData[0]['player_id']),
		  			'data' => $data['ride_data'],
		  			'contents' => $content
		  	);
		  
		  	$fields = json_encode($fields);
		  	//print("\nJSON sent:\n");
		  	//print($fields);
		  
		  	$ch = curl_init();
		  	curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		  	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		  	'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
		  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		  	curl_setopt($ch, CURLOPT_HEADER, FALSE);
		  	curl_setopt($ch, CURLOPT_POST, TRUE);
		  	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		  	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		  
		  	$response = curl_exec($ch);
		  	curl_close($ch);
		  
		  	//return $response;
		  
		  
			  $response = sendMessage();
			  $return["allresponses"] = $response;
			  $return = json_encode( $return);
			  return true;
		}
		  
	  /* FUNCTION to check if there is an error response from Apple
	   * Returns TRUE if there was and FALSE if there was not
	  */
	  public function checkAppleErrorResponse($fp) {
	  
	  	//byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID).
	  	// Should return nothing if OK.
	  
	  	//NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait
	  	// forever when there is no response to be sent.
	  	$apple_error_response = @fread($fp, 6);
	  	if ($apple_error_response) {
	  
	  		// unpack the error response (first byte 'command" should always be 8)
	  		$error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);
	  
	  		if ($error_response['status_code'] == '0') {
	  			$error_response['status_code'] = '0-No errors encountered';
	  
	  		} else if ($error_response['status_code'] == '1') {
	  			$error_response['status_code'] = '1-Processing error';
	  
	  		} else if ($error_response['status_code'] == '2') {
	  			$error_response['status_code'] = '2-Missing device token';
	  
	  		} else if ($error_response['status_code'] == '3') {
	  			$error_response['status_code'] = '3-Missing topic';
	  
	  		} else if ($error_response['status_code'] == '4') {
	  			$error_response['status_code'] = '4-Missing payload';
	  
	  		} else if ($error_response['status_code'] == '5') {
	  			$error_response['status_code'] = '5-Invalid token size';
	  
	  		} else if ($error_response['status_code'] == '6') {
	  			$error_response['status_code'] = '6-Invalid topic size';
	  
	  		} else if ($error_response['status_code'] == '7') {
	  			$error_response['status_code'] = '7-Invalid payload size';
	  
	  		} else if ($error_response['status_code'] == '8') {
	  			$error_response['status_code'] = '8-Invalid token';
	  
	  		} else if ($error_response['status_code'] == '255') {
	  			$error_response['status_code'] = '255-None (unknown)';
	  
	  		} else {
	  			$error_response['status_code'] = $error_response['status_code'].'-Not listed';
	  		}
	  
	  		$error_log = '<br><b>+ + + + + + ERROR</b> Response Command:<b>' . $error_response['command'] . '</b>&nbsp;&nbsp;&nbsp;Identifier:<b>' . $error_response['identifier'] . '</b>&nbsp;&nbsp;&nbsp;Status:<b>' . $error_response['status_code'] . '</b><br>';
	  
	  		$error_log .= 'Identifier is the rowID (index) in the database that caused the problem, and Apple will disconnect you from server. To continue sending Push Notifications, just start at the next rowID after this Identifier.<br>';
	  		
	  		$fpp = fopen($_SERVER['DOCUMENT_ROOT']."/najezdev/error_log","a+");
	  		fwrite($fpp, $error_log, strlen($error_log));
	  		fclose($fpp);
	  		log_message('error', $error_log);
	  		 
	  		return true;
	  	}
	  	return false;
	  }
	  
	  
	  
	public function getlangkey($type,$userid){
		$CI =& get_instance();
	   $CI->load->database();
	   $CI->load->model('model');
	   $CI->load->model('restapi_model');
		$data = $CI->model->s_data("lang", "opt_language", ['type' => $type,'userid'=>$userid]);
		return $data[0]['lang'];
	}
	public function getresponse($event,$type,$lang){
		$CI =& get_instance();
	   $CI->load->database();
	   $CI->load->model('model');
	   $CI->load->model('restapi_model');
	   if($lang==''){
		   $lang = 'en';
	   }
		$lang = 'message_'.$lang.' as response';
		$data = $CI->model->s_data("$lang", "notification_data", ['type' => $type,'event'=>$event]);
		return $data[0]['response'];
	}
}