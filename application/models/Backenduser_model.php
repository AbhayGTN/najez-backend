<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backenduser_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	function adduser($emailid,$unique_id,$pass,$cpass,$username,$first_name,$last_name,$type,$path,$mobile,$gender,$user_role){
		$data = array(
		   'username' => $username ,
		   'emailid' => $emailid ,
		   'fname' => $first_name,
		   'lname' => $last_name,
		   'password' => $pass,
		   'user_type' => $type,
		   //'status' => $status,
		   'photo' => $path,
		   'mobile' => $mobile,
		   'gender' => $gender,
                    'active' => '1',
                    'unique_id' => $unique_id,
                    'user_role'=> $user_role
		  // 'created' => time()
		);

		$query = $this->db->insert('backend_users', $data);
		return $query;
	}
        
        
	
	function viewusers(){
		$this->db->select('*');
		$this->db->from('backend_users');
		$this->db->where('user_type !=', '1');
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function hideuser($id){
		$data = array(
		   'status' => 0,
		   'active' => 0
		);
		$this->db->where('id', $id);
		$this->db->update('backend_users', $data); 
		return $this->db->affected_rows();
	}
	function activeuser($id){
		$data = array(
		   'status' => 1,
		   'active' => 1
		);
		$this->db->where('id', $id);
		$this->db->update('backend_users', $data); 
		return $this->db->affected_rows();
	}
	function viewuser($id){
		$this->db->select('*');
		$this->db->from('backend_users');
		$this->db->where('id', $id);  
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function update_user($first_name,$last_name,$mobile,$gender,$id,$user_role){
		$data = array(
		   'fname' => $first_name,
		   'lname' => $last_name,
		   'gender' => $gender,
		   'mobile' => $mobile,
                    'user_role'=>$user_role
		);
		$this->db->where('id', $id);
		$this->db->update('backend_users', $data); 
		return $this->db->affected_rows();
	}
	function update_profile($first_name,$last_name,$path,$username,$user_pass,$role,$id){
		$data = array(
		   'fname' => $first_name,
		   'lname' => $last_name,
		   'username' => $username,
		   'password' => $user_pass,
		   'photo' => $path
		);
		$this->db->where('id', $id);
		$this->db->update('backend_users', $data); 
		return $this->db->affected_rows();
	}
	
	function checkemail_user($email){
		$this->db->select('*');
		$this->db->from('backend_users');
		$this->db->where('emailid', $email);
		$query = $this->db->count_all_results();
		return $query;
	}
	
	function checkuser_status($userid){
		$this->db->select('status');
		$this->db->from('backend_users');
		$this->db->where('id', $userid);
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function setuser_status($userid){
		$data = array(
		   'status' => 1
		);
		$this->db->where('id', $userid);
		$this->db->update('backend_users', $data); 
		return $this->db->affected_rows();
	}
        
        public function get_roles_string($id)
        {
          $query = $this->db->select('id,name_en')->get('master_user_roles');
          $all_roles = $query->result_array();
          $all_roles = array_column($all_roles, 'name_en','id');
         
          $query = $this->db->select('user_role')->get_where("backend_users",['id'=>$id]);
          $user_role = $query->result_array();
          $user_role = explode(',', $user_role[0]['user_role']);
          
          foreach ($user_role as $key => $value)
          {
            $final[$key] = $all_roles[$value];
          }
          return $final;
        }
        
        public function get_all_roles()
        {
          $query = $this->db->get('master_user_roles');
          $data =  $query->result_array();
          $data = array_column($data, 'name_en','id');
          return $data;
        }
	
	
}

