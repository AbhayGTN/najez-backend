<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    public function stats()
    {
      $data['drivers'] = $this->db->count_all('drivers');
      $data['users'] = $this->db->count_all('users');
      $data['backend_users'] = $this->db->count_all('backend_users');
      
      $query = $this->db->query("SELECT SUM(final_price)  as final_price FROM rides;");
      
      $amount = $query->result_array();
      $data['total_earnings'] = $$amount[0]['final_price'] == NULL ?"0":$amount[0]['final_price']; 
      $data['rides'] = $this->db->count_all('rides');
      $query = $this->db->query("SELECT id FROM rides WHERE ride_flag = '2';");
      $data['completed_rides'] = $query->num_rows();
      $this->db->where(['ride_flag'=>'4'])->from('rides');
      $data['canceled_rides'] = $this->db->count_all();
      $this->db->where('ride_flag','2')->from('rides');
      $data['inprogress_rides'] = $this->db->count_all();
      $data['total_vehicles'] = $this->db->count_all('vehicles');
      $this->db->where(['active'=>'0'])->from('vehicles');
      
      $data['active_vehicles'] = $this->db->count_all();
      $this->db->where(['ride_flag'=>'1'])->from('vehicles');
      $data['inactive_vehicles'] = $this->db->count_all();
      
      /* Driver chart data  */
     // $query = $this->db->query("SELECT count(id) as count, month(created) as months,year(created) as years FROM drivers group by month(created);");
     // $data['driver_chart'] = $query->result_array();
      
      /* Rides chart data  */
      //$query = $this->db->query("SELECT count(id) as count, month(created) as months,year(created) as years FROM rides group by month(created);");
     // $data['rides_chart'] = $query->result_array();
      
      /* Users chart data  */
     // $query = $this->db->query("SELECT count(id) as count, month(created) as months,year(created) as years FROM users group by month(created);");
     // $data['users_chart'] = $query->result_array();
      
      return $data;
      
    }
    
    public function driver_single_view_stats($id)
    {
      $query = $this->db->select("count(id) as num")->get_where("vehicles", ['driver_id'=>$id]);
      $data['total_vehical'] = $query->result_array();
      
      $query = $this->db->select("count(id) as num")->get_where("vehicles", ['driver_id'=>$id,'active'=>'1']);
      $data['active_vehical'] = $query->result_array();
      
      $query = $this->db->select("count(id) as num")->get_where("vehicles", ['driver_id'=>$id,'active'=>'0']);
      $data['inactive_vehical'] = $query->result_array();
      
      $query = $this->db->select("count(id) as num")->get_where("rides", ['driverid'=>$id]);
      $data['total_rides'] = $query->result_array();
      
      $query = $this->db->select("total_amount as num")->get_where("drivers", ['id'=>$id]);
      $data['total_earnings'] = $query->result_array();
      $month = date('m');
      $query = $this->db->query("SELECT  * from rides where ride_flag NOT IN(0,1) AND MONTH(created) = '{$month}';");
      $data['last_month_earnings'] = $query->result_array();
      
      $query = $this->db->query("SELECT  * from rides where ride_flag NOT IN(0,1) AND MONTH(created) = Month (CURRENT_DATE());");
      $data['this_month_earnings'] = $query->result_array();
      
      return $data;
    }
	public function s_data($select,$table,$where)
        {
          $query = $this->db->select($select)->get_where($table, $where);
          return $query->result_array();
        }
        
        public function ride_details($id)
        {
          $query = $this->db->query("Select r.id, (Select concat(fname,' ',mname,' ',lname) from users where id=r.userid) as name,
(Select emailid from users where id=r.userid ) as email,r.ride_start_time,r.ride_end_time,date(r.created) as date,rd.distance,rd.distance_amount,
rd.total_amount from  rides r,ride_break_down rd where r.id=rd.ride_id AND r.driverid = '{$id}';");
          return $query->result_array();
        }
	
        public function wallet_trans_details($id)
        {
          $query = $this->db->query("Select description,amount,transaction_mode,transaction_datetime from driver_wallet_details;");
          return $query->result_array();
        }
        
        
        public function wallet_trans_details_user($id)
        {
          $query = $this->db->query("Select description,amount,transaction_mode,transaction_datetime from user_wallet_details;");
          return $query->result_array();
          
        }
        
        public function check_user_ride($id)
        {
          $query = $this->db->query("SELECT * FROM rides as t1 WHERE t1.userid = '{$id}' AND t1.ride_flag IN('1','2')");
          return $this->db->affected_rows();
        }
        
        public function check_driver_ride($id)
        {
          $query = $this->db->query("SELECT * FROM rides as t1 WHERE t1.driverid = '{$id}' AND t1.ride_flag IN('1','2')");
          return $this->db->affected_rows();
        }
        
        public function check_vehicle_ride($id) 
        {
          $query = $this->db->query("SELECT * FROM rides as t1 WHERE t1.vehicle_id = '{$id}' AND t1.ride_flag IN('1','2')");
          return $this->db->affected_rows();
        }
}