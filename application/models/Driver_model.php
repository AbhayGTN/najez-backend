<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	function adddriver($unique_id,$emailid,$pass,$cpass,$username,$first_name,$mname,$last_name,$type,$status,$path,$mobile,$identy,$city,$dob,$candeliver,$iban,$iban_bank){
		$data = array(
		   'username' => $username ,
		   'unique_id' => $unique_id ,
		   'emailid' => $emailid ,
		   'fname' => $first_name,
		   'mname' => $mname,
		   'lname' => $last_name,
		   'password' => $pass,
		   //'user_type' => $type,
		   'status' => $status,
		   'photo' => $path,
		   'mobile' => $mobile,
		   
		   'iqama' => $identy,
		   'work_city' => $city,
		   'iban' => $iban,
		   'bank' => $iban_bank,
		   'dob' => $dob,
		   'can_driver_deliver' => $candeliver,
		  // 'created' => time()
		);

		$query = $this->db->insert('drivers', $data);
		 
		return $query;
	}
	
	function viewdrivers(){
		$this->db->select('*');
		$this->db->from('drivers');
		//$this->db->where('user_type', '2');  
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function hidedriver($id){
		$data = array(
		   'status' => 0
		);
		$this->db->where('id', $id);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
	function activatedriver($id){
		$data = array(
		   'status' => 1
		);
		$this->db->where('id', $id);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
	function viewdriver($id){
		$this->db->select('*');
		$this->db->from('drivers');
		$this->db->where('id', $id);  
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function update_driver($first_name,$mname,$last_name,$mobile,$identy,$city,$dob,$iban,$iban_bank,$id){
		$data = array(
		   'fname' => $first_name,
		   'mname' => $mname,
		   'lname' => $last_name,
		  // 'gender' => $gender,
		   'mobile' => $mobile,
		   'iqama' => $identy,
		   'work_city' => $city,
		   'dob' => $dob,
		   'iban' => $iban,
		   'bank' => $iban_bank,
		);
		$this->db->where('id', $id);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
	function update_profile($first_name,$last_name,$path,$username,$user_pass,$role,$id){
		$data = array(
		   'fname' => $first_name,
		   'lname' => $last_name,
		   'username' => $username,
		   'password' => $user_pass,
		   'photo' => $path
		);
		$this->db->where('id', $id);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
	function reg_driver($fname, $mname, $lname, $emailid, $mobileno, $identy, $date,$city, $code,$can_deliver, $driver_lic){
		$data = array(
		   'username' => $emailid ,
		   'emailid' => $emailid ,
		   'fname' => $fname,
		   'mname' => $mname,
		   'lname' => $lname,
		   'password' => rand(),
		  // 'user_type' => 2,
		   'status' => 1,
		   'mobile' => $mobileno,
		   'identification_no' => $identy,
		   'dob' => $date,
		   'work_city' => $city,
		   'captain_code' => $code,
		   'can_driver_deliver' => $can_deliver,
            'referralcode'=>$fname.rand(1111,9999),
		   'driver_license_no' => $driver_lic
		  // 'created' => time()
		);

		$query = $this->db->insert('drivers', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function checkdriver_status($userid){
		$this->db->select('status');
		$this->db->from('drivers');
		$this->db->where('id', $userid);
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function setdriver_status($userid){
		$data = array(
		   'status' => 1
		);
		$this->db->where('id', $userid);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
	function update_profileimg($path,$id){
		$data = array(
		   'photo' => $path
		);
		$this->db->where('id', $id);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
        
        public function step2_driver($mobile)
        {
          $this->db->update("drivers", ['status'=>'1','verified'=>'1'], ['mobile'=>$mobile]);
          
          $qurey = $this->db->select("id,emailid,fname,mname,lname,gender,photo,mobile,work_city as city,total_amount as balance,can_driver_deliver,webblock,referralcode")->get_where("drivers", ['mobile'=>$mobile]);
          return $qurey->result_array();
        }
        
        
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */