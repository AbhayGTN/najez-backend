<?php defined('BASEPATH') or exit('.');

class Email_template_model extends CI_Model{
  
  protected $table = "email_template";
  
  // ------------------------------------------------------------------------
  /**
 * get_all fatch all data group by tamplate id
 *  
 */
  public function get_all_tamplate() {
   $query = $this->db->select('*')
              ->from($this->table)
              ->group_by('template_id')
              ->get();
   return $query->result_array();
   }
  // ------------------------------------------------------------------------
  /**
 * tamplate_all_meta fatch all meta keys by tamplate id 
 *  
   * param $id = tamplate id
 */
  public function tamplate_all_meta($tamplate_id){
    $query = $this->db->select("*")
            ->from($this->table)
            ->where("template_id", $tamplate_id)
            ->get();
    return $query->result_array();
    
  }
  
 public function save_data($data,$path,$id)
 {
	// print_r($files);die();
	if(isset($data['en_subject'])){
		$this->db->update($this->table, ['meta_value_en'=>$data['en_subject'],'meta_value_ar'=>$data['ar_subject']], ['template_id'=>$id,'meta_key'=>"subject"]);
	}
	if(isset($data['en_title'])){
		$this->db->update($this->table, ['meta_value_en'=>$data['en_title'],'meta_value_ar'=>$data['ar_title']], ['template_id'=>$id,'meta_key'=>"title"]);
	}
	if(isset($data['en_subtitle'])){
		$this->db->update($this->table, ['meta_value_en'=>$data['en_subtitle'],'meta_value_ar'=>$data['ar_subtitle']], ['template_id'=>$id,'meta_key'=>"subtitle"]);
	}
	if(isset($data['en_content'])){
		$this->db->update($this->table, ['meta_value_en'=>$data['en_content'],'meta_value_ar'=>$data['ar_content']], ['template_id'=>$id,'meta_key'=>"content"]);
	}
	if(isset($data['en_footer'])){
		$this->db->update($this->table, ['meta_value_en'=>$data['en_footer'],'meta_value_ar'=>$data['ar_footer']], ['template_id'=>$id,'meta_key'=>"footer"]);
	}
	if(isset($data['en_footer2'])){
		$this->db->update($this->table, ['meta_value_en'=>$data['en_footer2'],'meta_value_ar'=>$data['ar_footer2']], ['template_id'=>$id,'meta_key'=>"footer2"]);
	}
	if(isset($data['en_tnc'])){
		$this->db->update($this->table, ['meta_value_en'=>$data['en_tnc'],'meta_value_ar'=>$data['ar_tnc']], ['template_id'=>$id,'meta_key'=>"tnc"]);
	}
	if($path!=''){
		//echo $path;die();
		$this->db->update($this->table, ['meta_value_en'=>$path,'meta_value_ar'=>$path], ['template_id'=>$id,'meta_key'=>"attch"]);
	}
 }
 
}
