<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fare_ratecard_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
 public function get_data()
 {
   $query = $this->db->get('fare_ratecard');
   return $query->result_array();
   
 }
 
 public function table()
 {
   $query = $this->db->query("SELECT * FROM fare_ratecard WHERE trip_type = '1';");
   $data['table1'] = $query->result_array();
   
   
   
   $query = $this->db->query("SELECT * FROM fare_ratecard WHERE trip_type = '2';");
   $data['table2'] = $query->result_array();
   
   $query = $this->db->query("SELECT * FROM fare_ratecard WHERE trip_type = '3';");
   $data['table3'] = $query->result_array();
   
   return $data;
 }
    
}