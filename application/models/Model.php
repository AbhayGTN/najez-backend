<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model extends CI_Model {

  public function __construct()
    {
      parent::__construct();
      $this->load->database();
    }
	
    public function get_car_list($id)
    {
		$query = $this->db->query("SELECT car_model,car_model as id FROM front_carlist WHERE car_brand = '{$id}' AND car_brand != '' GROUP BY car_model;");
		//$query = $this->db->query("SELECT DISTINCT car_model FROM front_carlist WHERE car_brand = '{$id}';");
		return $query->result_array();
    }
	
	public function get_barnd_list()
	{
		$query = $this->db->query("SELECT DISTINCT car_brand FROM front_carlist ORDER BY car_brand;");
		return $query->result_array();
	}
	
	public function checkNotificationId($userid, $tablename, $fieldname){
		$query = $this->db->query("SELECT $fieldname,player_id FROM $tablename Where $fieldname ='".$userid."'");
		return $query->result_array();
	
	}
	
	public function getPlayerId($token,$tablename){
		$query = $this->db->query("SELECT player_id FROM $tablename Where device_token ='".$token."'");
		return $query->result_array();
	}
	
	public function get_car_type($id)
	{
		$query = $this->db->query("SELECT car_type FROM front_carlist WHERE car_model = '{$id}';");
		return $query->result_array();
	}
        
        public function s_data($select,$table,$where)
        {
          $query = $this->db->select($select)->get_where($table, $where);
          return $query->result_array();
        }
        
        public function u_data($table, $set, $where)
        {
          $this->db->update($table, $set, $where);
          return $this->db->affected_rows();
        }
        
        public function i_data($table, $set)
        {
          $this->db->insert($table, $set);
        }
        
        public function g_data($table,$select='*')
        {
          $query = $this->db->select($select)->get($table);
          return $query->result_array();
        }
        
        public function d_data($table, $where)
        {
          return $this->db->delete($table, $where);
        }
        
        public function registerd_car($id)
        {
          /*$query = $this->db->query("SELECT t1.id as vehicle_id,t1.vehicle_type,t1.vehicle_color,t1.vehicle_sno,t1.vehicle_family_id,t1.vehicle_number,t1.vehicle_model,t1.vehicle_manufacture_year,t1.active,t2.doc_val FROM vehicles as t1 INNER JOIN vehicle_documents as t2 WHERE t1.driver_id = '{$id}' AND t2.vehicle_id = t1.id AND t2.doc_key = 'photograph';");*/
          $query = $this->db->query("SELECT t1.id as vehicle_id,t1.vehicle_type,t1.vehicle_color,t1.vehicle_sno,t1.vehicle_family_id,t1.vehicle_number,fcl.car_brand,fcl.car_model,t1.vehicle_manufacture_year,t1.active,t2.doc_val FROM vehicles as t1, vehicle_documents as t2, front_carlist as fcl WHERE t1.driver_id = '{$id}' AND t2.vehicle_id = t1.id AND t2.doc_key = 'photograph' AND fcl.id = t1.vehicle_model;");
		  
          return $query->result_array();
        }
        
        public function valid_otp($mobile,$otp)
        {
          $query = $this->db->query("SELECT * FROM sms_otp_temp WHERE mobile = '{$mobile}'AND valid = '0' AND otp = '{$otp}' AND time >= now() - interval 15 minute;");
          return $query->result_array();
        }
        
        public function q_data($query){
			$query = $this->db->query($query);
          return $query->result_array();
		}
        public function b_i_data($table,$i_arr){
			$this->db->insert_batch($table, $i_arr);
		}
                
                
                public function add_log($data)
                {
                  $this->db->insert("backend_logs", $data);
                }
       
	
}
