<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotions_model extends CI_Model {

  public $table = "promotions";
  
  public function __construct() {
    parent::__construct();
    $this->load->database();
  }
  
  public function insert_data($data)
  {
    $this->db->insert($this->table, $data);
  }
  
  public function get_data()
  {
     $query = $this->db->select('id,promo_code,discount_percentage,user_no,date(validity_start) as validity_start,date(validity_end) as validity_end')->where('status', '1')->get($this->table);
     return $query->result_array();
  }
  
  public function fatch($id)
  {
    $query = $this->db->get_where($this->table, ['id'=>$id]);
    return $query->result_array();
  }
  

}
