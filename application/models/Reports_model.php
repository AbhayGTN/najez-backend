<?php defined('BASEPATH') or exit('.');

class Reports_model extends CI_Model {

  public function __construct() {
    parent::__construct();

    $this->load->database();
  }

  public function total_income() {
    $query = $this->db->query('Select ride_start_time as date,ride_id,(Select fname from users where id=r.userid) as username,(Select fname from drivers where id=r.driverid) as drivername,fare_amount,najez_amount as najezshare from ride_complete rc,rides r where rc.ride_id=r.id order by ride_start_time desc;');
    return $query->result_array();
  }

  public function total_sales() {
    $query = $this->db->query('Select r.ride_start_time as date,rb.ride_id,rb.waiting_amount,rb.distance,rb.promo_discount as discount, rb.total_amount from ride_break_down rb,rides r where rb.ride_id=r.id order by date desc;');
    return $query->result_array();
  }

  public function total_promotions() {
    $query = $this->db->query("Select r.ride_start_time as date,rb.ride_id,(Select fname from users where id=r.userid) as username,rb.promo_code, rb.promo_discount,rb.total_amount as ride_amount from ride_break_down rb,rides r where rb.promo_code is not null and rb.promo_code !='' order by date desc");
    return $query->result_array();
  }

  public function total_driver_income() {
    $query = $this->db->query("Select r.driverid,(Select fname from drivers where id=r.driverid) as drivername,sum(rc.driver_amount) as ride_amount from ride_complete rc,rides r where r.id=rc.ride_id group by r.driverid");
    return $query->result_array();
  }

  public function active_driver_rides() {
    $query = $this->db->query("Select (Select fname from drivers where id=rides.driverid) as drivername, (Select display_name from vehicle_family where id=rides.vehicle_type) as vehicle_type, (Select vehicle_model from vehicles where id= rides.vehicle_id) as vehicle, ride_start_time,pickup_location, drop_location from rides where ride_flag=2");    
    return $query->result_array();
  }
  
  public function active_driver_last_30_days()
  {
    $query = $this->db->query("Select (Select fname from drivers where id=r.driverid) as drivername, (Select emailid from drivers where id=r.driverid) as email, (Select mobile from drivers where id=r.driverid) as mobile,(Select work_city from drivers where id=r.driverid) as work_city from rides r,drivers d where r.driverid=d.id and date(r.created) <= date(now()) and date(r.created) >= date(now() - INTERVAL 30 DAY ) group by r.driverid");
    return $query->result_array();
  }
  
  public function active_drivers_yesterday()
  {
    $query = $this->db->query("Select (Select fname from drivers where id=r.driverid) as drivername, (Select emailid from drivers where id=r.driverid) as email, (Select mobile from drivers where id=r.driverid) as mobile, (Select work_city from drivers where id=r.driverid) as work_city from rides r,drivers d where r.driverid=d.id and date(r.created) <= date(now()) and date(r.created) >= date(now() - INTERVAL 1 DAY ) group by r.driverid");
    return $query->result_array();
    
  }
  
  public function earnings_per_driver()
  {
    $query = $this->db->query(" Select r.driverid,(Select fname from drivers where id=r.driverid) as drivername,sum(rc.driver_amount) as ride_amount, count(rc.ride_id) as total_rides from ride_complete rc,rides r where r.id=rc.ride_id group by r.driverid");
    return $query->result_array();
    
  }
  
  public function total_daily_sales($date)
  {
    $query = $this->db->query("Select rc.ride_id,(Select fname from drivers where id=r.driverid) as drivername, (Select fname from users where id=r.userid) as username, rc.fare_amount,rc.payment_cash,rc.payment_wallet from ride_complete rc,rides r where r.id=rc.ride_id and date(r.created) = '{$date}'");
    return $query->result_array();
    
  }
  
  public function canceled_rides()
  {
    $query = $this->db->query("SELECT t1.cancel_time,t2.fname,t3.fname,t1.booking_time,t4.option_en FROM rides as t1 INNER JOIN users as t2 ON t1.userid = t2.id INNER JOIN drivers as t3 ON t1.driverid = t3.id INNER JOIN master_cancel_options as t4 ON t1.cancel_option = t4.id WHERE t1.ride_flag = '4';");
    return $query->result_array();
    
  }
  
  public function confirmed_rides()
  {
    $query = $this->db->query("SELECT t1.cancel_time,t2.fname,t3.fname,t1.booking_time,t4.option_en FROM rides as t1 INNER JOIN users as t2 ON t1.userid = t2.id INNER JOIN drivers as t3 ON t1.driverid = t3.id INNER JOIN master_cancel_options as t4 ON t1.cancel_option = t4.id WHERE t1.ride_flag = '3';");
    return $query->result_array();
  }
  
  public function inactive_drivers()
  {
    $query = $this->db->query("SELECT * FROM drivers WHERE status = '0';");
    return $query->result_array();
  }
  
  public function inactive_customers()
  {
    $query = $this->db->query("SELECT * FROM users WHERE status = '0';");
    return $query->result_array();
    }

}
