<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Restapi_model extends CI_Model {

	function __construct(){
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
		$this->load->model('model');
    }
	
	function checkuser($email,$pass){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('emailid', $email);
		$this->db->where('password', $pass);
		$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function get_user($userid){
		$this->db->select('id, emailid, fname, mname, lname, gender, photo, mobile, city, total_amount as balance,webblock,chatemail,chatid,referralcode,verified,show_phone');
		$this->db->from('users');
		$this->db->where('id', $userid); 
		$query = $this->db->get();
		$user_info = $query->result();
		//$data = array();
		if(!empty($user_info)){
			if($user_info[0]->photo != ''){
				$user_info[0]->photo = base_url().$user_info[0]->photo;
			}else{
				$user_info[0]->photo = base_url().'assets/documents/user-icon.png';
			}
			
			//$data['user_info'] = $user_info[0];
		}
		return $user_info;
	}
	function get_driver($driverid){
		$this->db->select('id, emailid, fname, mname, lname, gender, photo, mobile, work_city as city, total_amount as balance,total_rides,can_driver_deliver,webblock,referralcode,verified');
		$this->db->from('drivers');
		$this->db->where('id', $driverid); 
		$query = $this->db->get();
		$user_info = $query->result();
		//$data = array();
		if(!empty($user_info)){
			if($user_info[0]->photo != ''){
				$user_info[0]->photo = base_url().$user_info[0]->photo;
			}else{
				$user_info[0]->photo = base_url().'assets/documents/user-icon.png';
			}
			//$data['user_info'] = $user_info[0];
		}
		return $user_info;
	}
        
       
        
        function get_driver2($driverid){
		$this->db->select('id, emailid, fname, mname, lname, gender, photo, mobile, work_city as city, total_amount as balance,total_rides,can_driver_deliver,webblock,referralcode,verified');
		$this->db->from('drivers');
		$this->db->where('id', $driverid); 
		$query = $this->db->get();
		$user_info = $query->result_array();
		//$data = array();
		if(!empty($user_info)){
			$user_info[0]->photo = base_url().$user_info[0]->photo;
			//$data['user_info'] = $user_info[0];
		}
		return $user_info;
	}
	
	function user_login( $emailid, $password ){
		$this->db->select('sno');
		$this->db->from('users');
		$this->db->where('emailid', $emailid);
		$this->db->where('password', $password);
		//$this->db->where('status', '1');
		$data = $this->db->count_all_results();
		if($data > 0){
			$this->db->select('sno as id,user_type,gender,party_id');
			$this->db->from('users');
			$this->db->where('emailid', $emailid);
			$this->db->where('password', $password);
			$query = $this->db->get();
			return $query->result();

		}else{
			return '';
		}
	}
	function add_user( $username, $emailid, $first_name, $password, $photo, $mobile,$city, $user_type, $gender, $status, $latlng, $total_amount, $user_referral,$referralcode=''){
		$unique_id = "USR" . str_pad(rand(0, pow(10, 4) - 1), 4, '0', STR_PAD_LEFT);
		$data = array(
		   'username' => $username ,
		   'emailid' => $emailid ,
		   'fname' => $first_name,
		   'password' => $password,
		   'photo' => $photo,
		   'mobile' => $mobile,
		   'city' => $city,
		   'gender' => $gender,
		   'latlng' => $latlng,
		   'total_amount' => $total_amount,
		   'referralcode' => $user_referral,
		   'status' => $status,
		   'user_type' => $user_type,
		   'webblock' => 1,
		   'unique_id' => $unique_id,
		   'is_referred' => $referralcode
		  // 'created' => time()
		);
		//print_r($data);
		$query = $this->db->insert('users', $data);
		$insert_id = $this->db->insert_id();
		//print_r($insert_id);die();
		return  $insert_id;
	}
	function add_driver( $username, $emailid, $first_name,$middle_name,$last_name, $password, $photo, $mobile,$city, $can_deliver, $gender, $status, $latlng, $total_amount, $user_referral,$iban,$bank,$dob,$resident,$driving_lic,$iqama, $referralcode=''){
		$unique_id = "DRV" . str_pad(rand(0, pow(10, 4) - 1), 4, '0', STR_PAD_LEFT);
		$data = array(
		   'username' => $username ,
		   'emailid' => $emailid ,
		   'fname' => $first_name,
		   'mname' => $middle_name,
		   'lname' => $last_name,
		   'password' => $password,
		   'photo' => $photo,
		   'mobile' => $mobile,
		   'work_city' => $city,
		   'gender' => $gender,
		   'latlng' => $latlng,
		   'total_amount' => $total_amount,
		   'referralcode' => $user_referral,
		   'status' => $status,
		   'can_driver_deliver' => $can_deliver,
		   'iban' => $iban,
		   'bank' => $bank,
		   'dob' => $dob,
		   'iqama' => $iqama,
		   'resident' => $resident,
		   'driver_license_no' => $driving_lic,
		   'webblock' => 0,
		   'unique_id' => $unique_id,
		   'is_referred' => $referralcode
		  // 'created' => time()
		);

		$query = $this->db->insert('drivers', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	function checkemailid_user($emailid){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('emailid', $emailid);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	function checkmobile_user($mobile){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('mobile', $mobile);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	function checkreferralcode_user($referralcode){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('referralcode', $referralcode);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	function checkemailid_driver($emailid){
		$this->db->select('id');
		$this->db->from('drivers');
		$this->db->where('emailid', $emailid);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	function checkmobile_driver($mobile){
		$this->db->select('id');
		$this->db->from('drivers');
		$this->db->where('mobile', $mobile);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	function checkreferralcode_driver($referralcode){
		$this->db->select('id');
		$this->db->from('drivers');
		$this->db->where('referralcode', $referralcode);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	
	function checkdriver_vehiclereg($deiverid){
		$this->db->select('id');
		$this->db->from('vehicles');
		$this->db->where('driver_id', $deiverid);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	function checkdriver_vehiclereg_type($deiverid){
		$this->db->select('vehicle_family_id');
		$this->db->from('vehicles');
		$this->db->where('driver_id', $deiverid);
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function getdriver_vehiclereg($deiverid){
		$this->db->select('distinct(vehicles.id) ');
		$this->db->from('vehicles');
		$this->db->join('vehicle_documents', 'vehicles.id = vehicle_documents.vehicle_id ');
		$this->db->where('vehicles.driver_id', $deiverid);
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function get_driver_rating($id){
		$this->db->select('AVG(star) as rating');
		$this->db->from('driver_reviews');
		$this->db->where('driverid', $id);
		$query = $this->db->get();
		if($query->row()->rating!=null){
			return $query->row()->rating;
		}
		else{
			return "0";
		}
	}
	function get_user_rating($id){
		$this->db->select('AVG(star) as rating');
		$this->db->from('user_reviews');
		$this->db->where('userid', $id);
		$query = $this->db->get();
		if($query->row()->rating!=null){
			return $query->row()->rating;
		}
		else{
			return "0";
		}
	}
	
	function getcar_list($type){
		$this->db->select('id, car_model, car_brand');
		$this->db->from('front_carlist');
		
		$this->db->where('car_type', $type);
		$this->db->order_by("car_brand", "asc");
		//$this->db->where('notify_status', '1');
		$query = $this->db->get();
		return $query->result();
		
		/*$this->db->select('car_model, car_brand');
		$this->db->from('front_carlist');
		$this->db->where('car_type', 'Family');
		//$this->db->where('notify_status', '1');
		$query = $this->db->get();
		$car_list['Family'] = $query->result();
		
		$this->db->select('car_model, car_brand');
		$this->db->from('front_carlist');
		$this->db->where('car_type', 'Luxury');
		//$this->db->where('notify_status', '1');
		$query = $this->db->get();
		$car_list['Luxury'] = $query->result();
		return $car_list;*/
	}
	function add_user_review($userid, $ride_id, $star, $review){
		$data = array(
			   'userid' => $userid,
			   'rideid' => $ride_id,
			   'star' => $star,
			   'review' => $review
			  // 'created' => time()
		);
		$query = $this->db->insert('user_reviews', $data);
		return $query;
	}
	function add_driver_review($driverid, $ride_id, $star, $review){
		$data = array(
			   'driverid' => $driverid,
			   'rideid' => $ride_id,
			   'star' => $star,
			   'review' => $review
			  // 'created' => time()
		);
		$query = $this->db->insert('driver_reviews', $data);
		return $query;
	}
	function add_location($user_id,$user_type,$latlng){
		$data = array(
			   'user_id' => $user_id ,
			   'user_type' => $user_type ,
			   'latlng' => $latlng
			  // 'created' => time()
		);
		$query = $this->db->insert('locations', $data);
		return $query;
	}
	function get_location($user_id,$user_type){
		$this->db->select('latlng');
		$this->db->from('locations');
		$this->db->where('user_id', $user_id);
		$this->db->where('user_type', $user_type);
		//$this->db->where('notify_status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function notify_count($userid){
		$this->db->select('id');
		$this->db->from('notifications');
		$this->db->where('userid', $userid);
		$this->db->where('notify_status', '1');
		return $this->db->count_all_results();
	}
	function getnotify($userid){
		$this->db->select('id, userid, notify_action, notify_val, notify_status, created');
		$this->db->from('notifications');
		$this->db->where('userid', $userid);
		//$this->db->where('notify_status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function updatenotify($userid, $notify_id){
		$data = array(
		   'notify_status' => 0
		);
		$this->db->where('userid', $userid);
		$this->db->where('id', $notify_id);
		$this->db->update('notifications', $data); 
		return $this->db->affected_rows();
	}
	function add_vehicle($driverid, $vehicle_sno, $vehicle_no_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $photofile, $residfile, $drivrlicfile, $driver_license_expire, $vregfile, $vehicle_reg_expire, $vinsurfile, $vehicle_insur_expire, $authfile,$taffile, $tafweeth_expire, $iban, $iban_bank){
		$vehicle_family_id = 4;
		if($vehicle_type=='Medium'){
			$vehicle_family_id = 1;
		}
		if($vehicle_type=='Family'){
			$vehicle_family_id = 2;
		}
		if($vehicle_type=='Luxury'){
			$vehicle_family_id = 3;
		}
		$data = array(
		   'vehicle_sno' => $vehicle_sno ,
		   'vehicle_number' => $vehicle_no_plate ,
		   'vehicle_manufacture_year' => $vehicle_year ,
		   'vehicle_type' =>  $vehicle_type,
		   'vehicle_model' => $car_model,
		   'vehicle_color' => $vehicle_color ,
		   'bank_card' => $iban,
		   'iban_bank' => $iban_bank,
		   'driver_id' => $driverid,
		   'vehicle_family_id'=>$vehicle_family_id
		  // 'created' => time()
		);

		$query = $this->db->insert('vehicles', $data);
		if($query){
			$vehicle_id = $this->db->insert_id();
			$datavehicledocument = array(
			   array(
				  'driver_id' => $driverid,
				  'vehicle_id' => $vehicle_id,
				  'doc_key' => 'photograph',
				  'doc_val' => $photofile,
				  'doc_expire' => '',
			   ),
			   
				array(
				  'driver_id' => $driverid ,
				  'vehicle_id' => $vehicle_id,
				  'doc_key' => 'vehicle-reg',
				  'doc_val' => $vregfile,
				  'doc_expire' => $vehicle_reg_expire,
			   ),
				array(
				  'driver_id' => $driverid ,
				  'vehicle_id' =>$vehicle_id,
				  'doc_key' => 'vehicle-insur',
				  'doc_val' => $vinsurfile,
				  'doc_expire' =>$vehicle_insur_expire,
			   ),
				array(
				  'driver_id' => $driverid ,
				  'vehicle_id' =>$vehicle_id,
				  'doc_key' => 'auth-img',
				  'doc_val' => $authfile,
				  'doc_expire' => '',
			   ),
				array(
				  'driver_id' => $driverid ,
				  'vehicle_id' =>$vehicle_id,
				  'doc_key' => 'tafweeth-img',
				  'doc_val' => $taffile,
				  'doc_expire' => $tafweeth_expire,
			   )
			);
			$this->db->insert_batch('vehicle_documents', $datavehicledocument);
			//return  $insert_id;
			
			//return $this->db->affected_rows();
		}
		return $query;
	}
	
	function token_user($userID,$token,$device){
		$this->db->select('id');
		$this->db->from('push_notification_user'); 
		$this->db->where('userid', $userID);  
		$this->db->where('device', $device);  
		$count = $this->db->count_all_results();
		if($count==0){
			$data = array(
			   'userid' => $userID ,
			   'device' => $device ,
			   'device_token' => $token
			  // 'created' => time()
			);
			$query = $this->db->insert('push_notification_user', $data);
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		}else{
			$data = array(
			   'device_token' => $token
			);
			$this->db->where('userid', $userID);
			$this->db->where('device', $device);
			$this->db->update('push_notification_user', $data); 
			return $this->db->affected_rows();
		}
	}
	function token_driver($userID,$token,$device){
		$this->db->select('id');
		$this->db->from('push_notification_driver'); 
		$this->db->where('driver_id', $userID);  
		$this->db->where('device', $device);  
		$count = $this->db->count_all_results();
		if($count==0){
			$data = array(
			   'driver_id' => $userID ,
			   'device' => $device ,
			   'device_token' => $token
			  // 'created' => time()
			);
			$query = $this->db->insert('push_notification_driver', $data);
			$insert_id = $this->db->insert_id();
			return  $insert_id;
		}else{
			$data = array(
			   'device_token' => $token
			);
			$this->db->where('driver_id', $userID);
			$this->db->where('device', $device);
			$this->db->update('push_notification_driver', $data); 
			return $this->db->affected_rows();
		}
	}
	function update_user( $first_name, $middle_name, $last_name, $photo, $gender, $userid){
		$data = array(
		   //'emailid' => $emailid,
		   'fname' => $first_name,
		   'mname' => $middle_name,
		   'lname' => $last_name,
		   'photo' => $photo,
		  // 'mobile' => $mobile,
		   'gender' => $gender,
		);
		$this->db->where('id', $userid);
		$this->db->update('users', $data); 
		return $this->db->affected_rows();
	}
	function update_driver( $first_name, $middle_name, $last_name, $photo, $gender, $userid){
		$data = array(
		   //'emailid' => $emailid,
		   'fname' => $first_name,
		   'mname' => $middle_name,
		   'lname' => $last_name,
		   'photo' => $photo,
		  // 'mobile' => $mobile,
		   'gender' => $gender,
		);
		$this->db->where('id', $userid);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
	
	function get_ratecard($code,$date){
		$this->db->select('id, card_code,title, description, price, total_rides');
		$this->db->from('voucher');
		$this->db->where('status', 1);
		$this->db->where('card_code', $code);
		$this->db->where('card_type', 2);
		$this->db->where('use_flag', 0);
		$this->db->where('start_date <=', $date);
		$this->db->where('end_date >=', $date);
		$query = $this->db->get();
		return $query->result();
	}
	function dotransaction($transaction_detail, $type, $userid, $user_type, $status){
		$data = array(
		   'transaction_detail' => $transaction_detail ,
		   'type' => $type ,
		   'userid' => $userid,
		   'user_type' => $user_type,
		   'status' => $status
		  // 'created' => time()
		);
		$query = $this->db->insert('transactions_details', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	function changecoupan_status($id){
		$data = array(
		   'use_flag' => 1,
		   'status' => 0
		);
		$this->db->where('id', $id);
		$this->db->update('voucher', $data); 
		return $this->db->affected_rows();
	}
	function changecoupan_useflag($id){
		$this->db->where('id', $id);
		$use_flag = $this->db->get('voucher')->first_row()->use_flag;
		//return $use_flag;
		$use_flag = $use_flag+1;
		//return $use_flag;
		$data = array(
		   'use_flag' => $use_flag
		);
		$this->db->where('id', $id);
		$this->db->update('voucher', $data); 
		return $this->db->affected_rows();
	}
	function driver_ratecard($userid, $card_id, $transaction_id){
		$data = array(
		   'transaction_id' => $transaction_id ,
		   'rate_card_id' => $card_id ,
		   'driver_id' => $userid
		  // 'created' => time()
		);
		$query = $this->db->insert('driver_rate_card', $data);
		//$insert_id = $this->db->insert_id();
		return  $query;
	}
	function credit_ride($userid,$rides){
		$this->db->where('id', $userid);
		$total_rides = $this->db->get('drivers')->first_row()->total_rides;
		$total_rides = $total_rides + $rides;

		$data = array(
		   'total_rides' => $total_rides
		);
		$this->db->where('id', $userid);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
	function get_online_ratecards(){
		//$this->db->distinct();
		$this->db->select('id,price,rides');
		$this->db->from('voucher_pricelist');
		//$this->db->where('status', 1);
		//$this->db->where('card_code', $code);
		//$this->db->where('card_type', 1);
		//$this->db->where('use_flag', 0);
		//$this->db->where('start_date <=', $date);
		//$this->db->where('end_date >=', $date);
		$query = $this->db->get();
		return $query->result();
	}
	function get_onlinecard_id($amount,$date){
		$this->db->select('id, card_code,title, description, price, total_rides');
		$this->db->from('voucher');
		$this->db->where('status', 1);
		$this->db->where('price', $amount);
		//$this->db->where('card_type', 2);
		$this->db->where('use_flag < user_no');
		$this->db->where('start_date <=', $date);
		$this->db->where('end_date >=', $date);
		$query = $this->db->get();
		return $query->result();
	}
	function saveusercard($userid, $card_data, $type){
		$data = array(
		   'card_detail' => json_encode($card_data) ,
		   //'rate_card_id' => $card_id ,
		   'userid' => $userid,
		   'type' => $type
		  // 'created' => time()
		);
		$query = $this->db->insert('card_detail', $data);
		//$insert_id = $this->db->insert_id();
		return  $query;
	}
	function usersetpassword($userid, $user_data){
		$this->db->where('id', $userid);
		$this->db->where('webblock', 1);
		$this->db->update('users', $user_data); 
		return $this->db->affected_rows();
	}
	function driversetpassword($userid, $user_data){
		$this->db->where('id', $userid);
		$this->db->where('webblock', 1);
		$this->db->update('drivers', $user_data); 
		return $this->db->affected_rows();
	}
	function useremailunicode($emailid, $user_data){
		$this->db->where('emailid', $emailid);
		//$this->db->where('webblock', 1);
		$this->db->update('users', $user_data); 
		return $this->db->affected_rows();
	}
	function userverifyunicode($emailid, $resetcode){
		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('resetcode', $resetcode);
		$this->db->where('emailid', $emailid);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	function userresetpassword($emailid, $password, $code){
		$data = array('password'=>$password,'resetcode'=>'');
		$this->db->where('emailid', $emailid);
		$this->db->where('resetcode', $code);
		$this->db->update('users', $data); 
		return $this->db->affected_rows();
	}
	function driveremailunicode($emailid, $user_data){
		$this->db->where('emailid', $emailid);
		//$this->db->where('webblock', 1);
		$this->db->update('drivers', $user_data); 
		return $this->db->affected_rows();
	}
	function driververifyunicode($emailid, $resetcode){
		$this->db->select('id');
		$this->db->from('drivers');
		$this->db->where('resetcode', $resetcode);
		$this->db->where('emailid', $emailid);
		//$this->db->where('status', '1');
		return $this->db->count_all_results();
	}
	function driverresetpassword($emailid, $password, $code){
		$data = array('password'=>$password,'resetcode'=>'');
		$this->db->where('emailid', $emailid);
		$this->db->where('resetcode', $code);
		$this->db->update('drivers', $data); 
		return $this->db->affected_rows();
	}
	function check_page($page,$lang){
		if($lang == 'en'){
			$this->db->select('description_en');
		}else{
			$this->db->select('description_ar');
		}
		
		$this->db->from('pages');
		$this->db->where('slug', $page);
		$query = $this->db->get();
		return $query->result();
	}
	function add_supportticket($support_data){
		$query = $this->db->insert('support', $support_data);
		//$insert_id = $this->db->insert_id();
		return  $query;
	}
	function get_vehicletype(){
		$this->db->select('id, display_name as type');
		$this->db->from('vehicle_family');
		$query = $this->db->get();
		return $query->result();
	}
	function get_triptype(){
		$this->db->select('id, trip_type_name as trip_type');
		$this->db->from('trip_type_master');
		$query = $this->db->get();
		return $query->result();
	}
	function getvehicleinfo($deiverid, $vehicle_id){
		$this->db->select('*');
		$this->db->from('vehicles');
		$this->db->where('driver_id', $vehicle_id);
		$this->db->where('id', $deiverid);
		$query = $this->db->get();
		return $query->result();
	}
	function add_ridedata($ride_data){
		$query = $this->db->insert('rides', $ride_data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	function confirm_ride($userid, $ride_id){
		$data = array(
		   'ride_flag' => 2
		);
		$this->db->where('id', $ride_id);
		$this->db->where('userid', $userid);
		$this->db->update('rides', $data); 
		return $this->db->affected_rows();
	}
	function get_ridedetail($ride_id){
		$this->db->select('*');
		$this->db->from('rides');
		$this->db->where('id', $ride_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	function set_islogin($id){
		$data = array(
		   'islogin' => 1
		);
		$this->db->where('id', $id);
		$this->db->update('party_info', $data); 
		return $this->db->affected_rows();
	}
	function set_isview($id){
		$data = array(
		   'isview' => 1
		);
		$this->db->where('id', $id);
		$this->db->update('party_info', $data); 
		return $this->db->affected_rows();
	}
	function set_isfeedback($id){
		$data = array(
		   'isfeedback' => 1
		);
		$this->db->where('id', $id);
		$this->db->update('party_info', $data); 
		return $this->db->affected_rows();
	}
	
	function add_drivertoride($ride_id, $driver_id, $vehicle_id){
		$data = array(
		   'driverid' => $driver_id,
		   'vehicle_id' => $vehicle_id
		);
		$this->db->where('id', $ride_id);
		$this->db->update('rides', $data); 
		return $this->db->affected_rows();
	}
	function drivershift($data){
		$query = $this->db->insert('shift', $data);
		//$insert_id = $this->db->insert_id();
		return  $query;
	}
	function getpromocode($promocode,$date){
		$this->db->select('discount_amount, discount_percentage');
		$this->db->from('promotions');
		$this->db->where('status', 1);
		$this->db->where('promo_code', $promocode);
		//$this->db->where('card_type', 2);
		$this->db->where('use_flag < user_no');
		$this->db->where('validity_start <=', $date);
		$this->db->where('validity_end >=', $date);
		$query = $this->db->get();
		return $query->result();
	}
	function getratecard($trip_type,$vehicle_type){
		$this->db->select('charges');
		$this->db->from('fare_ratecard');
		//$this->db->where('status', 1);
		//$this->db->where('voucher_code', $promocode);
		//$this->db->where('card_type', 2);
		//$this->db->where('use_flag < user_no');
		$this->db->where('trip_type', $trip_type);
		//$this->db->where('charge_type', $charge_type);
		$this->db->where('vehicle_type', $vehicle_type);
		$query = $this->db->get();
		return $query->result();
	}
	function get_supportdata($table,$lang){
		if($lang == 'en'){
			$this->db->select('id,en_option as text');
		}else{
			$this->db->select('id,ar_option as text');
		}
		
		$this->db->from($table);
		//$this->db->where('trip_type', $trip_type);
		//$this->db->where('vehicle_type', $vehicle_type);
		$query = $this->db->get();
		return $query->result();
	}
	
        
        public function save_device_token($mobile,$device_token)
        {
          $query = $this->db->select("id")->get_where("users", ['mobile'=>$mobile]);
          $id = $query->result_array();
          
          
          
        }
		function getlongtripcity(){
		$this->db->select('id,city_name_en,city_name_ar,location_data');
		$this->db->from('city_master');
		$this->db->where('long_trip', 1);
		//$this->db->where('vehicle_type', $vehicle_type);
		$query = $this->db->get();
		return $query->result();
	}
	
	function updateCityLocation($location_data, $city_id){
		$data = array(
				'location_data' => $location_data
		);
		$this->db->where('id', $city_id);
		$this->db->update('city_master', $data);
		return $this->db->affected_rows();
	}
	
	function get_ridehistory($id,$type,$status){
		if($type == 'user'){
			$this->db->select('id,GPS_Destination_Point,GPS_Starting_Point,booking_time,trip_id,vehicle_type,payment_method,ride_flag,final_price,pickup_location,drop_location,driverid');
		}else{
			$this->db->select('id,GPS_Destination_Point,GPS_Starting_Point,booking_time,trip_id,vehicle_type,payment_method,ride_flag,final_price,pickup_location,drop_location,userid');
		}
		
		$this->db->from('rides');
		$this->db->where($type.'id', $id);
		if($status!= 5 && $status != 6 && $status != 1){
			$this->db->where('ride_flag', $status);
		}elseif($status== 5){
			$this->db->where('ride_flag !=', 1);
			$this->db->where('scheduled', 0);
		}elseif($status== 6){
			$this->db->where('scheduled', 1);
		}elseif($status== 1){
			//echo date('Y-m-d'); die();
			$this->db->like('created', date('Y-m-d'),'after');
		}
		
		//$this->db->where('vehicle_type', $vehicle_type);
		$query = $this->db->get();
		$result = $query->result();
		// echo $this->db->last_query();die();
		// print_r($result);die();
		foreach($result as $key => $ridedata){
			if($type == 'driver'){
				$userid = $ridedata->userid;
				$userdata = $this->get_user($userid);
				$result[$key]->userdata = $userdata[0];
			}else{
				$driverid = $ridedata->driverid;
				$driverdata = $this->get_driver($driverid);
				$result[$key]->driverdata = $driverdata[0];
			}
			$trip_id = $this->model->s_data("trip_type_name", "trip_type_master", ['id' => $result[$key]->trip_id]);
			unset($result[$key]->trip_id);
			$result[$key]->trip_type = $trip_id[0]['trip_type_name'];
			$vehicle_type = $this->model->s_data("display_name", "vehicle_family", ['id' => $result[$key]->vehicle_type]);
			//unset($result[$key]->vehicle_type);
			$result[$key]->vehicle_type = $vehicle_type[0]['display_name'];
			
			if($status==6){
				unset($result[$key]->ride_flag);
				$result[$key]->ride_status = 'scheduled';
			}else{
				$ride_flag = $this->model->s_data("ride_status", "master_ride_status", ['flag' => $result[$key]->ride_flag]);
				unset($result[$key]->ride_flag);
				$result[$key]->ride_status = $ride_flag[0]['ride_status'];
			}
			
			
			$destinationpoint = explode(',',$ridedata->GPS_Destination_Point);
			$result[$key]->GPS_Destination_Point = array('lat'=>$destinationpoint[0],'lng'=>$destinationpoint[1]);
			$startingpoint = explode(',',$ridedata->GPS_Starting_Point);
			$result[$key]->GPS_Starting_Point = array('lat'=>$startingpoint[0],'lng'=>$startingpoint[1]);
			
			if($status== 6){
				if( $result[$key]->booking_time > round(strtotime('+2 hours')* 1000) ){
					$result[$key]->editable = 0;
				}else{
					$result[$key]->editable = 1;
				}
			}
			unset($result[$key]->created);
			//print_r($result);
			//die();
		}
		
		return $result;
	}
	function drivercancel_option(){
		$this->db->select('*');
		$this->db->from('master_driver_cancel_options');
		//$this->db->where('vehicle_type', $vehicle_type);
		$query = $this->db->get();
		return $query->result();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */