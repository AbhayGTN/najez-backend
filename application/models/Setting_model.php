<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_model extends CI_Model {

	function __construct()
    {
        parent::__construct();
		$this->load->database();
    }
	function get_settings(){
		$this->db->select('meta_key,meta_value');
		$this->db->from('settings');
		$query = $this->db->get();
		return $query->result();
	}
        function cancel_options_driver(){
		
                  
          
                $this->db->select('id,option_en,option_ar');
		$this->db->from("master_cancel_options");
		$query = $this->db->get();
		return $query->result();
	}
        function cancel_options_user(){
		
                  
          
                $this->db->select('id,option_en,option_ar');
		$this->db->from("master_user_cancel_options");
		$query = $this->db->get();
		return $query->result();
	}
	function get_city(){
		$this->db->select('id,city_name_en,city_name_ar');
		$this->db->from('city_master');
		$query = $this->db->get();
		return $query->result();
	}
	function add_city($data){
		$query = $this->db->insert('city_master', $data);
		//$insert_id = $this->db->insert_id();
		return  $query;
	}
	function hidecity($id){
		$this->db->where('id', $id);
		$this->db->delete('city_master'); 
		return $this->db->affected_rows();
	}
	function get_car(){
		$this->db->select('id,car_brand,car_type,car_model');
		$this->db->from('front_carlist');
		$query = $this->db->get();
		return $query->result();
	}
	function get_cardata($id){
		$this->db->select('id,car_brand,car_type,car_model');
		$this->db->from('front_carlist');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}
	function add_car($data){
		$query = $this->db->insert('front_carlist', $data);
		//$insert_id = $this->db->insert_id();
		return  $query;
	}
	function update_car($data,$id){
		$this->db->where('id', $id);
		$this->db->update('front_carlist',$data); 
		return $this->db->affected_rows();
	}
	function hidecar($id){
		$this->db->where('id', $id);
		$this->db->delete('front_carlist'); 
		return $this->db->affected_rows();
	}
	function get_bank(){
		$this->db->select('id,bank_name');
		$this->db->from('bank_list');
		$query = $this->db->get();
		return $query->result();
	}
	function add_bank($data){
		$query = $this->db->insert('bank_list', $data);
		//$insert_id = $this->db->insert_id();
		return  $query;
	}
	function hidebank($id){
		$this->db->where('id', $id);
		$this->db->delete('bank_list'); 
		return $this->db->affected_rows();
	}
        
	public function save_email_content($data)
	{
	  $this->db->update("settings", ['meta_value'=>$data['welcome_mail_en']], ["meta_key"=>"welcome_mail_en"]);
	  $this->db->update("settings", ['meta_value'=>$data['welcome_mail_ar']], ["meta_key"=>"welcome_mail_ar"]);
	}
        
        public function insert_driver_option($data)
        {
          $this->db->insert("master_cancel_options", $data);
        }
        public function insert_user_option($data)
        {
          $this->db->insert("master_user_cancel_options", $data);
        }
        
        public function d_data($table, $where)
        {
          $this->db->delete($table, $where);
          return $this->db->affected_rows();
        }
}