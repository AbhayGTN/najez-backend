<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {
function __construct()
{
        parent::__construct();
	$this->load->database();
    }
	function adduser($unique_id,$emailid,$pass,$cpass,$username,$first_name,$last_name,$type,$status,$path,$mobile,$gender){
		$data = array(
		   'username' => $username ,
		   'unique_id' => $unique_id ,
		   'emailid' => $emailid ,
		   'fname' => $first_name,
		   'lname' => $last_name,
		   'password' => $pass,
		   'user_type' => $type,
		   'status' => $status,
		   'photo' => $path,
		   'mobile' => $mobile,
		   'gender' => $gender,
		  // 'created' => time()
		);

		$query = $this->db->insert('users', $data);
		return $query;
	}
	
	function viewusers(){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_type', '3');
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function hideuser($id){
		$data = array(
		   'status' => 0
		);
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
		return $this->db->affected_rows();
	}
	function activeuser($id){
		$data = array(
		   'status' => 1
		);
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
		return $this->db->affected_rows();
	}
	function viewuser($id){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id', $id);  
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function update_user($first_name,$last_name,$mobile,$gender,$id){
		$data = array(
		   'fname' => $first_name,
		   'lname' => $last_name,
		   'gender' => $gender,
		   'mobile' => $mobile
		);
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
		return $this->db->affected_rows();
	}
	function update_profile($first_name,$last_name,$path,$username,$user_pass,$role,$id){
		$data = array(
		   'fname' => $first_name,
		   'lname' => $last_name,
		   'username' => $username,
		   'password' => $user_pass,
		   'photo' => $path
		);
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
		return $this->db->affected_rows();
	}
	function checkemail_driver($email){
		$this->db->select('*');
		$this->db->from('drivers');
		$this->db->where('emailid', $email);
		$query = $this->db->count_all_results();
		return $query;
	}
	function checkemail_user($email){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('emailid', $email);
		$query = $this->db->count_all_results();
		return $query;
	}
	function reg_user($fname, $mname, $lname,$email,$pass,$mobile,$gender,$user_city){
		$data = array(
		   'username' => $email ,
		   'emailid' => $email ,
		   'fname' => $fname,
		   'mname' => $mname,
		   'lname' => $lname,
		   'password' => $pass,
		   'user_type' => 3,
		   'status' => 0,
		   'mobile' => $mobile,
		   'gender' => $gender,
		   'city' => $user_city
		  // 'created' => time()
		);

		$query = $this->db->insert('users', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	public function userlogin($mobile, $password)
	{
	    
		 $condition = [
	        'mobile' => $mobile,
	        'password' => $password,
	        'status' => 1,
	    ];
		$this->db->select('id, unique_id, emailid, fname, mname, lname, gender, photo, mobile, city, total_amount as balance,webblock,referralcode,chatemail, verified,show_phone');
	    $user = $this->db
	    ->get_where('users', $condition)
	    ->row();
		if(!empty($user)){
			if($user->photo != ''){
				$user->photo = base_url().$user->photo;
			}else{
				$user->photo = base_url().'assets/documents/user-icon.png';
			}
			
			//$data['user_info'] = $user_info[0];
		}
	    return $user;
	}
	public function driverlogin($mobile, $password)
	{
	    $condition = [
	        'mobile' => $mobile,
	        'password' => $password,
            'status' => 1
	    ];
		$this->db->select('id, unique_id, emailid, fname, mname, lname, gender, photo, mobile, work_city as city, total_amount as balance,  total_rides, can_driver_deliver, webblock, referralcode, verified, chatid');
	    $user = $this->db
	    ->get_where('drivers', $condition)
	    ->row();
    
		if(!empty($user)){
			if($user->photo != ''){
				$user->photo = base_url().$user->photo;
			}else{
				$user->photo = base_url().'assets/documents/user-icon.png';
			}
			//$data['user_info'] = $user_info[0];
		}
	    return $user;
	    
	}
	function checkuser_status($userid){
		$this->db->select('status');
		$this->db->from('users');
		$this->db->where('id', $userid);
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function setuser_status($userid){
		$data = array(
		   'status' => 1
		);
		$this->db->where('id', $userid);
		$this->db->update('users', $data); 
		return $this->db->affected_rows();
	}
	
        public function get_user($email)
        {
          $query =$this->db->select("id, unique_id, emailid, fname, mname, lname, gender, photo, mobile, city, total_amount as balance,webblock,referralcode,chatemail,verified,show_phone")->get_where("users", ['emailid' => $email]);
          return $query->result_array();
        }
        
        public function single_view_stats($id)
        { 
          $query = $this->db->query("SELECT count(id) as num FROM rides WHERE userid = '{$id}';");
          $data['total_rides'] = $query->result_array();
          $query = $this->db->select('total_amount')->get_where("users", ['id'=>$id]);
         
                  
          $data['wallet_balance'] = $query->result_array();
          
          
          $query = $this->db->query("SELECT count(id) as num FROM rides WHERE userid = '{$id}' AND scheduled = '1';");
          $data['shaduled_ride'] = $query->result_array();
          return $data;
        }
        
        function update_profileimg($path,$id){
		$data = array(
		   'photo' => $path
		);
		$this->db->where('id', $id);
		$this->db->update('users', $data); 
		return $this->db->affected_rows();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */