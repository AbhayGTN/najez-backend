<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vehicle_model extends CI_Model {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
	//function add_vehicle($driver_id, $vehicle_sno, $vehicle_no_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $photofile, $residfile, $drivrlicfile, $vregfile, $vinsurfile, $authfile,$taffile, $iban, $iban_bank){
	
	function add_vehicle($driver_id, $vehicle_sno='', $vehicle_no_plate='', $vehicle_type='', $car_model='', $vehicle_year='', $vehicle_color='', $iban='', $iban_bank=''){
		$vehicle_family_id = 4;
		if($car_model=='Medium'){
			$vehicle_family_id = 1;
		}
		if($car_model=='Family'){
			$vehicle_family_id = 2;
		}
		if($car_model=='Luxury'){
			$vehicle_family_id = 3;
		}
		$data = array(
		   'vehicle_sno' => $vehicle_sno ,
		   'vehicle_number' => $vehicle_no_plate ,
		   'vehicle_manufacture_year' => $vehicle_year ,
		   'vehicle_type' => $car_model ,
		   'vehicle_model' => $vehicle_type ,
		   'vehicle_color' => $vehicle_color ,		   
		   'bank_card' => $iban ,
		   'iban_bank' => $iban_bank ,
		   'driver_id' => $driver_id,
		   'vehicle_family_id' => $vehicle_family_id
		  // 'created' => time()
		);
		
		$query = $this->db->insert('vehicles', $data);
		//return $query;
		$insert_id = $this->db->insert_id();
		//return $query;
		return $insert_id;
	}
	
	function viewvehicles(){
		$this->db->select('*');
		$this->db->from('vehicles');
		//$this->db->where('user_type', '2');  
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function viewvehicle($id){
		$this->db->select('*');
		$this->db->from('vehicles');
		$this->db->where('id', $id);  
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function update_vehicle($driver, $vehicle_sno, $vehicle_plate, $vehicle_type, $car_model, $vehicle_year, $vehicle_color, $photofile, $residfile, $drivrlicfile, $vregfile, $vinsurfile, $authfile, $iban, $iban_bank, $id){
		$data = array(
		   'vehicle_sno' => $vehicle_sno ,
		   'vehicle_number' => $vehicle_plate ,
		   
		   'vehicle_year' => $vehicle_year ,
		   'vehicle_type' => $car_model ,
		   'vehicle_model' => $vehicle_type ,
		   'vehicle_color' => $vehicle_color ,
		   'photograph' => $photofile ,
		   'identity' => $residfile ,
		   'driver_license' => $drivrlicfile ,
		   'vehicle_regisration' => $vregfile ,
		   'vehicle_insurance' => $vinsurfile ,
		   'authorization_image' => $authfile ,
		   'bank_card' => $iban ,
		   'iban_bank' => $iban_bank ,
		   'driver_id' => $driver_id
		  // 'created' => time()
		);
		$this->db->where('id', $id);
		$this->db->update('vehicles', $data); 
		return $this->db->affected_rows();
	}
	function get_drivervehicle($id){
		$this->db->select('*');
		$this->db->from('vehicles');
		$this->db->where('driver_id', $id);  
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
	function getvehicledoc($id){
		
		$this->db->select('*');
		$this->db->from('vehicle_documents');
		$this->db->where('vehicle_id', $id);  
		//$this->db->where('status', '1');
		$query = $this->db->get();
		return $query->result();
	}
        
      
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */