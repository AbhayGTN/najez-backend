<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Voucher_model extends CI_Model {

  function __construct() {
    parent::__construct();
    $this->load->database();
  }

  function get_ratecardlist() {
    $this->db->select('id, price, rides');
    $this->db->from('voucher_pricelist');
    $query = $this->db->get();
    return $query->result();
  }

  function add_ratecard($data) {
    $query = $this->db->insert('voucher', $data);
    //$insert_id = $this->db->insert_id();
    return $query;
  }

  function viewcards() {
    $this->db->select('id,card_code,price,start_date,end_date');
    $this->db->from('voucher')->where('status','1')->order_by('id', 'DESC');
 //   $this->db->join('voucher_pricelist', 'voucher_pricelist.id=voucher.id');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  public function update_card($data,$id)
  {
    $this->db->update("voucher", $data, ['id'=>$id]);
    
  }

  function viewcard($id) {
    $this->db->select('id,card_code,price,start_date,end_date');
    $this->db->from('voucher');
    $this->db->where('voucher.id', $id);
    $query = $this->db->get();
    return $query->result_array();
  }

  function hidecard($id) {
    $data = array(
        'status' => 0
    );
    $this->db->delete('vehicles', ['id'=>$id]);
    //$this->db->where('id', $id);
    //$this->db->update('voucher', $data);
    return $this->db->affected_rows();
  }

  function hidelist($id) {
    $this->db->where('id', $id);
    $this->db->delete('voucher_pricelist');
    return $this->db->affected_rows();
  }

  function add_pricelist($data) {
    $query = $this->db->insert('voucher_pricelist', $data);
    //$insert_id = $this->db->insert_id();
    return $query;
  }

}
