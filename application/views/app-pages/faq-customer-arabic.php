<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>الأسئلة المتكررۃ</title>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
body {
	font-family: calibri;
	padding: 15px;
}
.panel-group .panel {
	border-radius: 0;
	box-shadow: none;
	border-color: #EEEEEE;
}
.panel.opened{box-shadow: 0 8px 6px -6px rgba(0, 0, 0, 0.14);}
.panel-default > .panel-heading {
	padding: 0;
	border-radius: 0;
	color: #212121;
	background-color: #FAFAFA;
	border-color: #EEEEEE;
}
.panel-group .panel + .panel{margin-top:10px} .panel-title {
	font-size: 14px;
}
.panel-title > a {
	display: block;
	padding: 15px;
	text-decoration: none;
	outline: none;
	font-size: 18px;
}
.more-less {
	float: left;
	color: #212121;
}
.panel-default > .panel-heading + .panel-collapse > .panel-body {
	border-top-color: #FAFAFA;
}
.panel-body{background:#FAFAFA}
body {
	background-color: rgba(204,204,204,0.3);
}
</style>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body dir="rtl">
<div class="demo">
  <h1 align="center" style="margin-top:0; margin-bottom:25px;">الأسئلة المتكررۃ</h1>
  <h2 style="color:#FF9E15"><u>العامة</U> </h2>
  
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <i class="more-less fa fa-plus"></i>ما هي ناجز؟</a> </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body"> ناجز هي خدمة سعودیة100٪  تقدم لمستخدميها خدمة معتمدة وسهلة الاستخدام لتوجيه مركبات الأجرة ومشاركتها مع الآخرين، وتنجز مهامك من أجلك! خدماتنا هي من أجلك.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <i class="more-less fa fa-plus"></i>کیفناجزتعمل؟</a> </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">یمكنك بعد تحمیل تطبیقنا ببساطة أن تسجل بسھولة و تبدأ بحجز المركبات التي ستنجز مهامك اليوم! <br />
إن كنت سائقًا و مهتمًا بالإنضمام إلی ناجز فیمكنك أن تتابع التعلیمات ببساطة علی موقعنا أو التطبيق، وعند حصولك لموافقة الإدارة يمكنك أن تكون شريكًا معنا في ناجز.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingThree">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <i class="more-less fa fa-plus"></i>لماذا ناجز؟</a> </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="panel-body">نحن في ناجز نعمل بجهد أن نقدم لكم أسعار مميزة، خدمة سريعة، وقيمة أفضل لخدماتنا. سائقين ناجز يعتمد عليهم في انجاز مهماتكم!</div>
      </div>
    </div>
    <h2 style="color:#FF9E15"><u>الأسعار</U> </h2>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFour">
        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"> <i class="more-less fa fa-plus"></i>ما هي أسعاركم؟</a> </h4>
      </div>
      <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
        <div class="panel-body">نحن نعمل بجدّ للحفاظ علی الأسعار منخفضا قدر الإمكانعن منافسینا لنضمنأنتكون تجربتك لدينا منأفضل التجارب ونوفر لك تعتمد عليها بشكل دائم. للمزيد من المعلومات سجل دخولك في التطبيق لتتحقق من الأسعار.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFive">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> <i class="more-less fa fa-plus"></i>لماذا أجرة المطار تكون أعلى عن التكلفة العادية؟</a> </h4>
      </div>
      <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
        <div class="panel-body">ستنطبق رسوم أجرة المطار عند استخدام خدماتنا. إن هذه الرسوم هي موجودة لتعويض السائق عن ركن السيارة، دفع الضرائب المتعلقة للمطار، والانتظار (والتي هي مجانية في أول 30 دقيقة).</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingSix">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> <i class="more-less fa fa-plus"></i>متى يتم الخصم من بطاقتي؟</a> </h4>
      </div>
      <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
        <div class="panel-body">لن نخصم من بطاقتك حتى تنتهي الرحلة. عند حجزك للرحلة، كل ما عليك هو اختيار البطاقة التي تريدنا أن نخصم منها مبلغ الرحلة.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingSeven">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"> <i class="more-less fa fa-plus"></i>ھل ھی آمنة؟</a> </h4>
      </div>
      <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
        <div class="panel-body">نعم هيآمنة بلاشك. يمكنك تتبع مسار الرحلة على الخريطة الموجودة في التطبيق، من نقطة البداية إلى نقطة النهاية.<br /><strong>رافقتكم السلامة!</div>
      </div>
    </div>
  </div>
  <!-- panel-group --> 
  
</div>
<!-- container --> 
<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-plus fa-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

<script>
$("a").click(function(){
    $(this).parent().parent().parent().toggleClass("opened");
});
</script>
</body>
</html>
