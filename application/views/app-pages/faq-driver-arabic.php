<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>أسئلة متكررة للكابتن</title>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
body {
	font-family: calibri;
	padding: 15px;
}
.panel-group .panel {
	border-radius: 0;
	box-shadow: none;
	border-color: #EEEEEE;
}
.panel.opened{box-shadow: 0 8px 6px -6px rgba(0, 0, 0, 0.14);}
.panel-default > .panel-heading {
	padding: 0;
	border-radius: 0;
	color: #212121;
	background-color: #FAFAFA;
	border-color: #EEEEEE;
}
.panel-group .panel + .panel{margin-top:10px} .panel-title {
	font-size: 14px;
}
.panel-title > a {
	display: block;
	padding: 15px;
	text-decoration: none;
	outline: none;
	font-size: 18px;
}
.more-less {
	float: left;
	color: #212121;
}
.panel-default > .panel-heading + .panel-collapse > .panel-body {
	border-top-color: #FAFAFA;
}
.panel-body{background:#FAFAFA}
body {
	background-color: rgba(204,204,204,0.3);
}
</style>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body dir="rtl">
<div class="demo">
  <h1 align="center" style="margin-top:0; margin-bottom:25px;">أسئلة متكررة للكابتن</h1>
  
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <i class="more-less fa fa-plus"></i>کیف أصبح سائق /سائق التوصیل لناجز؟</a> </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">لقد عملنا بجدّ لتبسيط عملية التسجيل. بكل بساطة ما عليك إلاملئ الاستمارات ورفع المستندات المطلوبة، منھما من السائق أو سائق التوصیل وتحميل المستندات التيطلبناها منك. سيتم مراجعة طلبك من الإدارة قبل الموافقة عليها.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <i class="more-less fa fa-plus"></i>ما ھو التفويض ولماذا احتاجه؟</a> </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">ان التفويضھی رسالة الاذن منمالكالمرکبة والتي تفوض لسائق آخر بصلاحية استخدام المركبة لأهداف ربحية.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingThree">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <i class="more-less fa fa-plus"></i>کیفیمكنني قبول الرحلات أو طلبات التوصيل؟</a> </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="panel-body">إن تطبيقنا سھل للاستخداموبديهي في التعامل، ويمكنك بكل بساطة قبول المهام التي تظهر لك في التطبيق وإنهاءها.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFour">
        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"> <i class="more-less fa fa-plus"></i>ما هي ساعات العمل الخاص بي؟</a> </h4>
      </div>
      <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
        <div class="panel-body">ستكون ساعات عملك مرنة، عندما تكون متصلًا بالتطبيق، وتقبل المهمات الموجودة، ستكون هذه هي ساعات عملك إلى أن تحدد وتكون غير متصلًا. </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFive">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"> <i class="more-less fa fa-plus"></i>مع من اتصل اذاواجھتمشكلة تقنية في التطبيق؟</a> </h4>
      </div>
      <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
        <div class="panel-body">یمكنك التحدث معنا مباشرة عبر هذا الرقم (###) لحل المشكلة بأسرع وقت ممكن.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingSix">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> <i class="more-less fa fa-plus"></i>ما هي مكاسبي الشهرية؟</a> </h4>
      </div>
      <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
        <div class="panel-body">إن مكاسبك مع ناجز تعتمد على عدد الساعات التي تكون متصلا في التطبيق وتقبل المهمات. <br />
قد تختلف الأرباح من شخص إلى آخر، على حسب ساعات العمل أو إذا كنت سائق، أو سائق توصيل. ولكن عليك أن تعلم دائمًا أننا نسعى أن نقدم أفضل نسبة أرباح عن منافسينا. <br />
<strong>نتمنی لكم تجربة ممتعة مع ناجز</strong></div>
      </div>
    </div>
  </div>
  <!-- panel-group --> 
  
</div>
<!-- container --> 
<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-plus fa-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

<script>
$("a").click(function(){
    $(this).parent().parent().parent().toggleClass("opened");
});
</script>
</body>
</html>
