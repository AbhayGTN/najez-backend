<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>المساعدۃ</title>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
body {
	font-family: calibri;
	padding: 15px;
}
.panel-group .panel {
	border-radius: 0;
	box-shadow: none;
	border-color: #EEEEEE;
}
.panel.opened{box-shadow: 0 8px 6px -6px rgba(0, 0, 0, 0.14);}
.panel-default > .panel-heading {
	padding: 0;
	border-radius: 0;
	color: #212121;
	background-color: #FAFAFA;
	border-color: #EEEEEE;
}
.panel-group .panel + .panel{margin-top:10px} .panel-title {
	font-size: 14px;
}
.panel-title > a {
	display: block;
	padding: 15px;
	text-decoration: none;
	outline: none;
	font-size: 18px;
}
.more-less {
	float: left;
	color: #212121;
}
.panel-default > .panel-heading + .panel-collapse > .panel-body {
	border-top-color: #FAFAFA;
}
.panel-body{background:#FAFAFA}
body {
	background-color: rgba(204,204,204,0.3);
}
.panel-body h4{margin-top:0}
.panel-body p, .panel-body ul{margin-bottom:15px}
</style>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body dir="rtl">
<div class="demo">
  <h1 align="center" style="margin-top:0; margin-bottom:25px;">المساعدۃ</h1>
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <i class="more-less fa fa-plus"></i>رحلاتك</a> </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <h4>كيف يمكنني أن أعرض رحلاتي الحالية أو القادمة؟</h4>
<p>إن كان لدیك حجز حالي، أو حجز مسبق يمكنك عرضه بالذهاب إلى:<br />

<strong>القائمة</strong>  >  <strong>رحلاتي</strong> >  <strong>الرحلات المحجوزة</strong> 
</p>

<h4>کیف یمكنني الحصول علی تقدير تكلفة رحلتي؟ </h4>
<p>إن كنت تريد أن تعرف كم ستكلف الرحلة من النقطة أ إلى النقطة ب، يرجى اختيار نقطة الانطلاق أولا وثم اضغط "احجز الآن." إضغط علی الوجهة – تقدير التكلفة، أدخل وأكّد الوجهة وسيظهر تقدير للتكلفة على الشاشة الرئيسية. </p>

<h4>أين يمكنني أن أجد أسعار ناجز؟ </h4>
<p>إن كنت ترید أن تعرف المزید عن أسعارنا، فیرجی الذھاب إلی قائمة <strong>التطبیق</strong> > <strong>الأسعار.</strong>  یرجی اختیار مدینة الواحدۃ ونوع السیارۃ لایجاد المزید عن أسعارنا. </p>

<h4>أين أجد حجز الذي قمت به لرحلتي؟</h4>
<p>بعد كل رحلة ستستلم في بريدك الإلكتروني إيصال أو فاتورة الحجز، وسيكون رقم الحجز موجود في موضوع البريد الإلكتروني، وبداخلها كذلك. </p>

<h4>أين يمكنني أن أجد رحلاتي السابقة؟</h4>
<p>يمكنك أن رحلاتك السابقة لدى ناجز من التطبيق نفسه. من قائمة التطبيق اذهب إلى "رحلاتي" وستجد هناك كل رحلاتك السابقة مع ناجز. </p>

<h4>كيف يمكنني أن أشارك بتعليقاتي مع سائق ناجز؟ </h4>
<p>إن تعليقاتكم تهمنا لتحسين خدماتنا، ودائمًا تعليقاتكم مرحب بها! <br />

إن كانت لديك شكوى تجاه السائق، يمكنك أن تعطيه تقييم سيء، ومن خلال ذلك يمكنك إضافة تعليقات عن سائق رحلتك. <br />
وبالطبع، إن كانت رحلتك ممتازة، ولم تكن هناك أي مشاكل  في الرحلة، يمكنك إعطاء السائق تقييم عالي، وشكرهم على خدمتهم الممتازة. وإن كانت خدمتنا ممتازة، لا تنسى أن تشارك تجربتك على مواقع التواصل الاجتماعي.</p>

<h4>لماذا وقت الوصول المقدر غير دقيق؟ </h4>
<p>إن دقة وقت الوصول المقدر يعتمد على موقع السائق والجهة التي يتجه إليها. <br />

قد يكون السائق على الجهة الأخرى من الشارع ويتوجب عليه أن يلتف، وبسبب ذلك قد يتأخر السائق. وفي أحيان أخري قد يواجه السائق زحمة في الطريق مما يؤخره ويجعل وقت الوصول المقدر غير دقيقًا. </p>

<h4>ماذا أفعل لو نسيت شيء يخصني في السيارة؟ </h4>
<p> إذا أدركت أنك نسيت شيء في إحدى رحلاتك مع ناجز، يرجى التواصل معنا عبر البريد الإلكتروني: <a href="mailto:customer.care@najez-online.com">customer.care@najez-online.com</a>، ولا تنسى أن تشارك معنا رقم الحجز في موضوع البريد الإلكتروني. <br />

يمكنك أن تجد رقم الحجز في الفاتورة التي تم إرسالها في بريدك الإلكتروني، ولمعرفة المزيد عن رقم الحجز، يمكنك الدخول إلى قائمة تاريخ الرحلات في التطبيق.</p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <i class="more-less fa fa-plus"></i>الالغاءات</a> </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">
          <h4>ھل هناك رسوم إلغاء؟</h4>
<p>نحن نتمنى أن لا نكلفك رسوم للإلغاء، ولكن يحتم واقع الأمر بأن السائق عندما يقبل طلب، فأنه سيرفض أعمال أخرى لإنجاز الحجز الحالي. <br />

فلذلك إذا تك إلغاء الحجز بعدما تم تعيين السائق للحجز، علينا أن ندفع أجرة وقتهم. لقد حاولنا أن نكون كريمين وعادلين لسياسة الإلغاء لدينا. وللعلم، إذا تم إلغاء حجز من دون أي سائق فإن في هذه الحالة لا تنطبق رسوم الإلغاء. </p>

<h4>للحجوزات فی وقت لاحق:</h4>
<p>لإلغاءات التي تطرأ في آخر لحظة، وفي حالتنا، إذا وصل السائق إلى نقطة الانطلاق، وبعد انتظار 5 دقائق، ستنطبق رسوم الانتظار. </p>

<h4>کم سینتظر السائق قبل إلغاء الرحلة؟</h4>
<p>إذا لم تطلب من السائق شخصيًا بأن ينتظر، سينتظر السائق لمدة 5 دقائق عند وصوله لنقطة الانطلاق. يمكن للسائق إلغاء الحجز بعد 5 دقائق من الانتظار.</p>

<h4>لماذا وقت الوصول المقدر للسائق يتزايد؟ </h4>
<p>في هذه الحالة، قد يكون هناك خلل فني، أو قد يكون السائق يتجه بعيدًا عن نقطة الانطلاق. يمكنك الاتصال بالسائق والتأكد، أو يمكنك التواصل معنا عبر البريد الإلكتروني: <a href="mailto:customer.care@najez-online.com">customer.care@najez- online.com</a></a></p>

<h4>ماذا یجب على ان افعله اذا کان السائق یستمر قیادۃ السیارۃ بعیدا عن موقعي؟</h4>
<p>في هذه الحالة، يمكنك الاتصال بالسائق مباشرة والتأكد من أنه هل حقًا يتجه بعيدًا عن نقطة الانطلاق أم يلتف ليصل إلى الموقع. <br />

إن كان السائق يتجه بعيدًا عنك، ولا يريد أن يصل إليك في الموقع المذكور، قم بإلغاء الرحلة، وتواصل معنا عبر التطبيق بالذهاب إلى القائمة > تواصل مع الدعم،</p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingThree">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <i class="more-less fa fa-plus"></i>عن الحجز</a> </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="panel-body">
          <h4>ما ھو نوع السیارۃ؟</h4>
<p>ما ننقصد هنا، أنه يمكنك اختيار نوع السيارة لحجزها في مدينتك أو منطقتك، وكل سيارة لديها خصائصها الفريدة، والمساحة، لتناسب احتياجاتك. <br />

هناك سيارات دائمة، والتي متواجدة دائمًا حسب المدينة التي أنت فيها، وأوقات تواجدها. ولكن هناك أيضًا سيارات تكون موجودة في فترات ترويجية.<br />

إن اختيار نوع السيارة التي ترغب بحجزها سهل جدًا، بكل بساطة ما عليك الضغط على نوع السيارة التي ترغبها. يمكنك أن تجد كل أسعار كل أنواع السيارات ومعلوماتها هنا: <strong>القائمة</strong> > <strong>الأسعار.</strong> </strong>

</p>

<h4>کیف یمكنني أن أحجز رحلة ناجزعلی الآیفون؟</h4>
<p>إن طریقة حجز رحلة ناجز بسیطة جدًا علی الآیفون! یرجی إتباع ھذہ الخطوات البسيطة:</p>
<ul>
<li>افتح تطبيق ناجز وسجل دخولك</li>
<li>اختر نوع السيارة التي ترغب في حجزها (يمكنك اكتشاف المزيد من الأنواع عند سحب القائمة لليمين أو اليسار) </li>
<li>اختر نقطة الانطلاق (بتحريك الخريطة، والبحث عن الموقع، أو اختيار موقع محفوظ مسبقًا) </li>
<li>اضغط على "الآن" أو "لاحقًا" (للحجوزات المسبقة)</li>
<li>للحجوزات المسبقة، يرجى اختيار وقت وتاريخ الحجز. </li>
<li>في صفحة التأكيد يمكنك اختيار الوجهة التي تريد الذهاب إليها وسيظهر لك سعر مقدر للرحلة، ويمكنك في هذه الصفحة كذلك إدخال رمز ترويجي لحجزك. إن لم يكن لديك رمز ترويجي يمكنك متابعة حجزك وإنهاءه. </li>
</ul>

<h4>كيف يمكنني أن أعرض رحلاتي السابقة؟</h4>
<p>يمكنك أن تعرّض جمیع رحلاتك الماضية الخاصة بك داخل تطبیق ناجز كالتالي: <br />
<strong>القائمة</strong> > <strong>رحلاتي</strong> > <strong>تاريخ الرحلات.</strong> </p>

<p>إن النقر على الرحلة يستعرض التالي:</p>
<ul>
<li>نقطة الانطلاق ونقطة الوصول </li>
<li>مدة الرحلة </li>
<li>نوع السيارة المختارة </li>
<li>طريقة الدفع المختارة </li>
<li>تفاصيل السائق </li>
</ul>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFour">
        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"> <i class="more-less fa fa-plus"></i>خیارات الدفع</a> </h4>
      </div>
      <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
        <div class="panel-body"><h4>ما هي طرق الدفع؟</h4>
<p>لدينا ثلاثة خيارات لتسوية أجرۃ رحلتك، يمكنك اختيار الطرق التالية: </p>
<ul>
<li>نقدًا </li>
<li>محفظة ناجز </li>
<li>بطاقة </li>
</ul>

<h4>کیف یمكنني أن أدفع من خلال بطاقة؟</h4>
<p>تخلص من ضغوطات السفر بإضافة بطاقة في تطبيق ناجز لتتجنب حمل نقود معك. يمكنك إضافة بطاقة بالذهاب إلى <strong>القائمة</strong> > <strong>بطاقتي</strong> > <strong>إضافة</strong> بطاقة. </p>

<h4>ماذا أفعل إذا لم يتم إجراء الدفع عن طريق البطاقة بشكل ناجح؟ </h4>
<p>إذا لم تنجح عملية الدفع بالبطاقة في المرة الأولى، سيقوم النظام بمعاودة الإجراء مرة أخرى. إذا لم تنجح عملية الدفع في المرة الثانية، يرجى التواصل مع البنك لتحديد المشكلة وحلها. </p>

<h4>هل من الممكن الدفع بعملات أجنبية؟</h4>
<p>لأن الأسعار المذكورة هي بالعملة المحلية، يتوجب على العملاء دفع السائق بنفس العملة المذكورة. </p>
        </div>
      </div>
    </div>
  </div>
  <!-- panel-group --> 
  
</div>
<!-- container --> 
<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-plus fa-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

<script>
$("a").click(function(){
    $(this).parent().parent().parent().toggleClass("opened");
});
</script>
</body>
</html>