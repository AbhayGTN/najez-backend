<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HELP</title>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
body {
	font-family: calibri;
	padding: 15px;
}
.panel-group .panel {
	border-radius: 0;
	box-shadow: none;
	border-color: #EEEEEE;
}
.panel.opened{box-shadow: 0 8px 6px -6px rgba(0, 0, 0, 0.14);}
.panel-default > .panel-heading {
	padding: 0;
	border-radius: 0;
	color: #212121;
	background-color: #FAFAFA;
	border-color: #EEEEEE;
}
.panel-group .panel + .panel{margin-top:10px} .panel-title {
	font-size: 14px;
}
.panel-title > a {
	display: block;
	padding: 15px;
	text-decoration: none;
	outline: none;
	font-size: 18px;
}
.more-less {
	float: right;
	color: #212121;
}
.panel-default > .panel-heading + .panel-collapse > .panel-body {
	border-top-color: #FAFAFA;
}
.panel-body{background:#FAFAFA}
body {
	background-color: rgba(204,204,204,0.3);
}
.panel-body h4{font-weight:600; margin-top:0}
.panel-body p, .panel-body ul{margin-bottom:15px}
</style>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="demo">
  <h1 align="center" style="margin-top:0; margin-bottom:25px;">Help</h1>
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <i class="more-less fa fa-plus"></i>Your Rides</a> </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <h4>How can I view a current or upcoming ride?</h4>
          <p>If you have a current booking or pre-scheduled Later booking, you can view them by heading over to<br />
            <strong>Menu</strong> > <strong>My rides</strong> > <strong>Scheduled</strong> </p>
          <h4>How can I get the fare estimate for a ride?</h4>
          <p>Want to know how much it will cost to get from point A to point B? Select your pickup location and then press Ride Now. Tap on Drop-off - Fare estimate, enter and confirm a drop-off location, and the fare estimate will now appear on the main screen.</p>
          <h4>Where can I find the rates for Najez?</h4>
          <p>To know more about our fares and rates, go to the app Menu > Price Rates. Select a city and car category to find out more about our rates!</p>
          <h4>Where do I find the booking of my ride?</h4>
          <p>After every ride, you will receive your receipt via the email address you have registered to your account. The booking ID is in the subject line, as well as on the receipt.</p>
          <h4>Where can I find my past rides?</h4>
          <p>You can always find your past rides right in the app itself! On the app Menu, go to My Rides and you will find all your previous rides taken in the History.</p>
          <h4>How can I share feedback about my Driver?</h4>
          <p>Feedback is important for us to enhance our services and is always welcomed!<br />
            In the case of a complaint, you have the option of rating the Driver poorly. This will allow you to add your comments and feedback of the Driver you ended the ride with.<br />
            Of course, when you want to commend the Driver, you can rate them highly, thank them in person, or if it was super spectacular, why not share your experience with us on social media? </p>
          <h4>Why the ETA is not accurate?</h4>
          <p>ETA (Estimated Time of Arrival) accuracy depends on the location of the Driver and the direction in which he is moving.<br />
            Drivers may be on the other side of the road and require to turn around, which is why at times the ETA gets longer. Other times, the Driver may face some traffic on his way, which again reflects as extended or longer ETAs.</p>
          <h4>What if I forget an item of mine in the cab?</h4>
          <p>If you realize you have forgotten an item in one of your rides, please mail us on <a href="mailto:customer.care@najez-online.com">customer.care@najez-online.com</a>, sharing your Booking ID of the ride in subject and details.<br />
            The Booking ID will be on your ride receipt. To know more about the Booking ID, please see the Rides history section. </p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <i class="more-less fa fa-plus"></i>Cancellations</a> </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">
          <h4>Do you charge cancellation fee?</h4>
          <p>We wish we did not have to charge for cancellations at all, but the reality is that a Driver, once assigned to a booking, will often turn down other businesses to fulfill the currently accepted booking.<br />
            So if a booking is canceled after a Driver has been assigned, we need to compensate them.<br />
            With that principle of fairness, we have tried to be generous in our cancellation policy, and bookings cancelled before a Driver has been assigned incur no cost. </p>
          <h4>For LATER bookings</h4>
          <p>For last minute cancellations, or in cases where the “Driver” has already arrived at the desired pickup location, after waiting for the 5 minutes, the waiting charges will be applicable.</p>
          <h4>How long will the driver wait before cancelling the ride?</h4>
          <p>If you have not requested the driver to wait longer for you after they have reached the location, the driver could cancel the ride 5 minutes after their arrival.</p>
          <h4>Why does the ETA keep on increasing?</h4>
          <p>In case the ETA keeps on increasing, it could be due to a technical glitch, or the Driver actually moving further away from you. You can call the Driver to confirm, or send us an email on <a href="mailto:customer.care@najez-online.com.">customer.care@najez-online.com.</a></p>
          <h4>What should I do if the driver keeps driving away from my location?</h4>
          <p>In case of its occurrence, you can call the Driver to confirm whether he is actually moving away, or making a turn to reach to the location.<br />
            If the Driver is moving away and does not wish to arrive at the selected location, cancel the ride and mail us via the Najez app by heading to <strong>Menu</strong> > <strong>Contact support.</strong> </p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingThree">
        <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> <i class="more-less fa fa-plus"></i>About Booking</a> </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="panel-body">
          <h4>What is a Car type?</h4>
          <p>A "Car type" is the choice of a car that's available to book within your city or area.<br />
            Each Car type has its unique features, occupancy, and price to fit your needs.<br />
            There are permanent Car types, based on city, which are available at all times. There are also campaign specific Car types which are available only during promotional campaigns.<br />
            Selecting between the Car types is easy, by simply tapping on the Car type you wish to book, and you can find all Car type pricing and information here: <strong>Menu</strong> > <strong>Price Rates.</strong> </p>
          <h4>How do I book a Najez ride on iOS?</h4>
          <p>Making a Najez booking is really simple on iOS! Simply follow these steps:</p>
          <ul>
            <li>Open the Najez app and log-in</li>
            <li>Select the Car type you wish to book (Slide through the car types to reveal more)</li>
            <li>Select the pickup location (By moving the map, searching for a location or selecting a saved location)</li>
            <li>Tap on Now or Later (pre-scheduled booking)</li>
            <li>For Later Bookings, select the date and time of the booking</li>
            <li>On the confirmation screen, you can select the drop-off location for a fare estimate and enter a promo code to your booking, in case you do not have a promo code, simply move on next step to book your ride.</li>
          </ul>
          <h4>How do I view my past rides?</h4>
          <p>You can view all your past rides within the Najez App by heading over to<br />
            <strong>Menu</strong> > <strong>My rides</strong> > <strong>History</strong></p>
          <p>Tapping on a ride will show you:</p>
          <ul>
            <li>Pickup and drop-off location selected</li>
            <li>Ride duration</li>
            <li>Car type selected</li>
            <li>Fare paid</li>
            <li>Payment method selected</li>
            <li>Driver details</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingFour">
        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"> <i class="more-less fa fa-plus"></i>Your Payment Options</a> </h4>
      </div>
      <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
        <div class="panel-body"><h4>What are the payment options?</h4>
          <p>With  3 options to settle your ride fare, you can pay the way you want:</p>
          <ul>
            <li>Cash</li>
            <li>Najez Wallet.</li>
            <li>Card</li>
          </ul>
          <h4>How do I pay through my card ?</h4>
          <p>Travel stress free by adding your card in the Najez app to avoid carrying cash. You can add your card by going to<br />
            <strong>App Menu</strong> > <strong>My card</strong> > <strong>Add card</strong>. </p>
          <h4>What should I do if card payment is unsuccessful?</h4>
          <p>If a card payment is unable to process, the system will automatically try to process the payment a second time. If we're still unable to process the payment, you can reach out to your bank to identify the issue and resolve it.</p>
          <h4>Is it possible to pay in a foreign currency?</h4>
          <p>As the fare is generated in the local currency, so customers have to pay the driver through the same currency as well. </p>
        </div>
      </div>
    </div>
  </div>
  <!-- panel-group --> 
  
</div>
<!-- container --> 
<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-plus fa-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
</script>

<script>
$("a").click(function(){
    $(this).parent().parent().parent().toggleClass("opened");
});
</script>
</body>
</html>
