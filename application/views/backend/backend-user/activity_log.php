<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo 'Activity Log'; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Activity Log</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('vehicles/hide'); ?>" />
    <!-- Main content -->
	

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Activites</h3>
			  <p class="pull-right"><a href="<?php echo base_url('vehicles/add_vehicle'); ?>"><button type="button" class="btn btn-primary btn-sm">Add New Vehicle</button></a></p>
                          <p style="color:red"><?= $this->session->flashdata('cannot_block_user_msg');  ?></p>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>Vehicle ID</th>
				  <th>Plate Number</th>
				  <th>Vehicle Model</th>
				  <th>Vehicle Type</th>
                  <th style="width:220px;">Action</th>
                </tr>
                </thead>
                <tbody>
				
                </tbody>
                <tfoot>
                <tr>
                 <th>Vehicle ID</th>
				 <th>Plate Number</th>
				   <th>Vehicle Model</th>
				  <th>Vehicle Type</th>
                  <th>Status</th>
                  <!--<th>Action</th>-->
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>