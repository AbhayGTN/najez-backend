
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Backend User
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">User</a></li>
        <li class="active">Add User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      
      <!-- /.box -->

      <div class="row">
		<div class="col-md-1"></div>
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">User Details</h3>
            </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>
			<form method="post" action="<?php echo base_url('backenduser/adduser'); ?>" enctype="multipart/form-data" >
				<div class="box-body">
				  <div class="input-group">
                                      <span class="input-group-addon"><strong>Email</strong></span>
					<input type="email" name="emailid" class="form-control" required placeholder="Email ID" value="<?php echo set_value('emailid'); ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><strong>Username</strong></span>
					<input type="text" name="username" class="form-control" required placeholder="Username" value="<?php echo set_value('username'); ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><strong>First Name</strong></span>
					<input type="text" name="first_name" class="form-control" required placeholder="First Name" value="<?php echo set_value('first_name'); ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><strong>Last Name</strong></span>
                                        <input type="text" name="last_name" class="form-control" required placeholder="Last Name" value="<?php echo set_value('last_name'); ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><strong>Password</strong></span>
					<input type="password" name="password" class="form-control" required placeholder="password">
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><strong>Confirm Password</strong></span>
                                        <input type="password" name="cpassword" class="form-control" required placeholder="Confirm">
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<span class="input-group-addon"><strong>Mobile</strong></span>
                                        <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" required value="<?php echo set_value('mobile'); ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<label>
					  <input type="radio" name="gender" class="minimal" value="male" <?php if(set_value('gender') != null){ if(set_value('gender')=='male'){ echo 'checked'; } }else{ echo 'checked';} ?> /> Male
					</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<label>
					  <input type="radio" name="gender" class="minimal" value="female" <?php if(set_value('gender')=='female'){ echo 'checked'; }?> /> Female
					</label>
				  </div>
				  <br/>
				  <!-- /.form group -->
				   <div class="input-group">
					<span class="input-group-addon"><strong>Select Image</strong></span>
					<input type="file" name="photo" class="form-control photo" />
				  </div>
				  <p class="help-block" style="margin-left:40px;">Upload Profile photo here</p>
				  <br/>
				  <!-- /.form group -->
<div class="input-group">
					<span class="input-group-addon"><strong>Select Roll</strong></span>
                                        <input type="checkbox" name="user_role[]" value="1" /><lable> Backend User</lable><br>
                                        <input type="checkbox" name="user_role[]" value="2" /><lable> Customer</lable><br>
                                        <input type="checkbox" name="user_role[]" value="3" /><lable> Email Template</lable><br>
                                        <input type="checkbox" name="user_role[]" value="4" /><lable> Drivers</lable><br>
                                        <input type="checkbox" name="user_role[]" value="5" /><lable> Pages</lable><br>
                                        <input type="checkbox" name="user_role[]" value="6" /><lable> Vouchers</lable><br>
                                        <input type="checkbox" name="user_role[]" value="7" /><lable> Promotions</lable><br>
                                        <input type="checkbox" name="user_role[]" value="8" /><lable> Fare Rate Card</lable><br>
                                        <input type="checkbox" name="user_role[]" value="9" /><lable> Reports</lable><br>
                                        <input type="checkbox" name="user_role[]" value="10" /><lable> Settings</lable><br>
				  </div>
				  <br/>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
          </div>
          <!-- /.box -->

          
          

        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
          <!-- /.box -->
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>