
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Backend User
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Users</li>
        <li class="active">View</li>
      </ol>
    </section>

    
    <section class="content">
      <div class="row">
		<div class="col-md-1"></div>
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">User Details (<?= $agentdata[0]->unique_id;  ?>)</h3>
            </div>
				<div class="box-body">
				<div class="input-group">
				<img src="<?php echo base_url($agentdata[0]->photo); ?>" />
				  </div>
				  
				  <br/>
				  
				  
				  <div class="input-group">
					<span class="input-group-addon"><p style="margin-bottom: 0;"><label>Email ID</label></p></span>
					<input type="email" name="emailid" class="form-control" disabled placeholder="Email ID" value="<?php echo $agentdata[0]->emailid; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<span class="input-group-addon"><p style="margin-bottom: 0;"><label>Username</label></p></span>
					<input type="text" name="username" class="form-control" disabled placeholder="Username" value="<?php echo $agentdata[0]->username; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<span class="input-group-addon"><p style="margin-bottom: 0;"><label>First Name</label></p></span>
					<input type="text" name="first_name" class="form-control" disabled placeholder="First Name" value="<?php echo $agentdata[0]->fname; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<span class="input-group-addon"><p style="margin-bottom: 0;"><label>Last Name</label></p></span>
					<input type="text" name="last_name" class="form-control" disabled placeholder="Last Name" value="<?php echo $agentdata[0]->lname; ?>" />
				  </div>
				  <br/>
				  
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<span class="input-group-addon"><p style="margin-bottom: 0;"><label>Mobile Number</label></p></span>
					<input type="text" name="mobile" class="form-control" disabled placeholder="Mobile Number" value="<?php echo $agentdata[0]->mobile; ?>" />
				  </div>
				  <br/>
				  <div class="input-group">
					<span class="input-group-addon"><strong>User Roles</strong></span>
					<?php foreach ($user_roles as $key => $value): ?>
                                        <label><?= $value ?></label><br>
                                        
                                        <?php endforeach;  ?>
				  </div>
				  <br/>
				  
				  <div class="input-group">
					<label>
					  <input type="radio" name="gender" class="minimal" value="male" <?php if($agentdata[0]->gender != null){ if($agentdata[0]->gender=='male'){ echo 'checked'; }else{ echo 'disabled';} }else{ echo 'checked';} ?> /> Male
					</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<label>
					  <input type="radio" name="gender" class="minimal" value="female" <?php if($agentdata[0]->gender=='female'){ echo 'checked'; }else{ echo 'disabled';}?> /> Female
					</label>
				  </div>
				  <br/>
				  

				</div>
				<!-- /.box-body -->
				<div class="box-footer">
                                    <a href="<?= base_url('backenduser/edit_user/'.$this->uri->segment(3));  ?>"><button type="button" class="btn btn-primary">Edit</button></a>
                                    <a href="<?= base_url('backenduser/delete_user/'.$this->uri->segment(3));  ?>" onclick="return confirm('Are you sure?')"><button type="button" class="btn btn-danger">Delete</button></a>
                                     
				</div>
          </div>
     </div>
		<div class="col-md-1"></div>
        <div class="col-md-4">	
        </div>
      </div>
    </section>
  </div>