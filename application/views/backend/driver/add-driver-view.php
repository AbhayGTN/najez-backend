<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> Add Driver </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Driver</a></li>
        <li class="active">Add Driver</li>
      </ol>
    </section>
<section class="content">
      <div class="row">
	<div class="col-md-1"></div>
        <div class="col-md-6">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Driver Details</h3>
               </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>
			<form method="post" action="<?php echo base_url('driver/adddriver'); ?>" enctype="multipart/form-data" autocomplete="off" >
				<div class="box-body">
				<div class="input-group">
					<label>
					<input type="radio" name="type" class="minimal" value="0" <?php if(set_value('type') != null){ if(set_value('type')=='0'){ echo 'checked'; } }else{ echo 'checked';} ?> /> Driver
					</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<label>
					  <input type="radio" name="type" class="minimal" value="1" <?php if(set_value('type')=='1'){ echo 'checked'; }?> /> Delivery Man
					</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<label>
					  <input type="radio" name="type" class="minimal" value="2" <?php if(set_value('type')=='2'){ echo 'checked'; }?> /> Both
					</label>
				  </div>
				  <br/>
				  <div class="input-group">
                                      <span class="input-group-addon"><strong>First Name</strong></span>
					<input type="text" name="fname" class="form-control" required placeholder="First Name" value="<?php echo set_value('fname'); ?>" />
				  </div>
				  <br/>
		
				  <div class="input-group">
					<span class="input-group-addon"><strong>Middle Name</strong></span>
					<input type="text" name="mname" class="form-control" required placeholder="Middle Name" value="<?php echo set_value('mname'); ?>" />
				  </div>
				  <br/>
				  <div class="input-group">
					<span class="input-group-addon"><strong>Last Name</strong></span>
                                        <input type="text" name="lname" class="form-control" placeholder="Last Name" value="<?php echo set_value('lname'); ?>" />
				  </div>
				  <br/>
				  <div class="input-group">
					<span class="input-group-addon"><strong>Email</strong></span>
					<input type="email" name="emailid" autocomplete="off" class="form-control" required placeholder="Email ID" value="<?php echo set_value('emailid'); ?>" />
				  </div>
				  <br/>
				  <div class="input-group">
					<span class="input-group-addon"><strong>Password</strong></span>
					<input type="password" name="password" autocomplete="off" class="form-control" required placeholder="password">
				  </div>
				  <br/>
				  
				  <div class="input-group">
					<span class="input-group-addon"><strong>Confirm Password</strong></span>
                                        <input type="password" name="cpassword" class="form-control" required placeholder="Confirm">
				  </div>
				  <br/>
				   <div class="input-group">
					<span class="input-group-addon"><strong>Mobile</strong></span>
					<!-- fixed by Nandini -->
           <!-- <input type="tel" id="demo" name="mobile" class="form-control" required placeholder="Mobile Number With Country Code" value="<?php //echo set_value('mobile'); ?>" /> -->
					 <select style="width: 30%;float: left;" id="countries_phone1" class="form-control bfh-countries" data-country="US"></select>
					<input style="width: 70%;float: left;" type="tel" id="demo" class="form-control bfh-phone" name="mobile" data-country="countries_phone1" placeholder="Mobile Number With Country Code" value="<?php echo set_value('mobile'); ?>">

					
					</div>

					<!-- <select id="countries_phone1" class="form-control bfh-countries" data-country="US"></select>
					<input type="text" class="form-control bfh-phone" name="mobile" data-country="countries_phone1" placeholder="Mobile Number With Country Code" value="<?php echo set_value('mobile'); ?>"> -->




				  <br/>
				  <!-- /.form group --> 
				  <div class="input-group">
					<span class="input-group-addon"><strong>National ID/Iqama</strong></span>
                                        <input type="text" class="form-control" name="iqama" required id="identy" maxlength="10" value="<?php echo set_value('identy'); ?>" placeholder="National ID/Iqama">
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><strong>City</strong></span>
                                        <select class="form-control" name="city" id="city" required>
						<option value="">City</option>
						<option value="Bahrain">Bahrain</option>
						<option value="Dammam">Dammam</option>
						<option value="Dhahran">Dhahran</option>
						<option value="Hassa">Hassa</option>
						<option value="Hafar Al-Batin">Hafar Al-Batin</option>
						<option value="Jeddah">Jeddah</option>
						<option value="Jubail">Jubail</option>
						<option value="Kharj">Kharj</option>
						<option value="Khobar">Khobar</option>
						<option value="Khafji">Khafji</option>
						<option value="Qatif">Qatif</option>
						<option value="Qassim">Qassim</option>
						<option value="Riyadh">Riyadh</option>
					</select>
				  </div>
				  <br/>
				  <!-- /.form group -->
				 
					<div class="input-group">
					<span class="input-group-addon"><strong>IBAN Number</strong></span>
                                        <input type="text" class="form-control" required name="iban" id="iban" placeholder="IBAN Number" value="<?= set_value('iban'); ?>" />
					</div>
					 <br/>
				  <!-- /.form group -->
					
						<div class="input-group">
						<span class="input-group-addon"><strong>Bank</strong></span>
                                                <select class="form-control" name="iban_bank" id="iban_bank" required>
								<option value="">Bank</option>
								<option value="Alinma Bank">Alinma Bank</option>
<option value="Al Rajhi Bank">Al Rajhi Bank</option>
<option value="Arab National Bank">Arab National Bank</option>
<option value="Asia Bank">Asia Bank</option>
<option value="Banque Saudi Fransi">Banque Saudi Fransi</option>
<option value="Bank Al-Bilad">Bank Al-Bilad</option>
<option value="Bank AlJazira">Bank AlJazira</option>
<option value="BNP Paribas">BNP Paribas</option>
<option value="Deutsche Bank">Deutsche Bank</option>
<option value="Emirates NBD">Emirates NBD</option>
<option value="Gulf International Bank (GIB)">Gulf International Bank (GIB)</option>
<option value="Industrial and Commercial Bank of China (ICBC)">Industrial and Commercial Bank of China (ICBC)</option>
<option value="J.P. Morgan Chase N.A">J.P. Morgan Chase N.A</option>
<option value="Muscat Bank">Muscat Bank</option>
<option value="National Bank of Bahrain (NBB)">National Bank of Bahrain (NBB)</option>
<option value="National Bank of Kuwait (NBK)">National Bank of Kuwait (NBK)</option>
<option value="National Bank of Pakistan (NBP)">National Bank of Pakistan (NBP)</option>
<option value="Qatar National Bank (QNB)">Qatar National Bank (QNB)</option>
<option value="Riyad Bank">Riyad Bank</option>
<option value="Saudi Investment Bank">Saudi Investment Bank</option>
<option value="Samba Financial Group (Samba)">Samba Financial Group (Samba)</option>
<option value="Saudi Hollandi Bank (Alawwal)">Saudi Hollandi Bank (Alawwal)</option>
<option value="State Bank of India (SBI)">State Bank of India (SBI)</option>
<option value="The National Commercial Bank">The National Commercial Bank</option>
<option value="The Saudi British Bank">The Saudi British Bank</option>
<option value="T.C.ZIRAAT BANKASI A.S.">T.C.ZIRAAT BANKASI A.S.</option>
							</select>
						</div>
					<br/>
				  <div class="row">
                              <div class="col-md-2 col-sm-2 col-xs-12"><label>DOB</label></div>
                              <div class="col-md-3 col-sm-3 col-xs-12">
																<!-- Fixed by Nandini-->
                                  <select name="dob_dd" id="dob_dd" class="form-control" >
									  <option value="">DD</option>
										  <?php for ($i = 1; $i < 32; $i++) {
										   if($i < 10)
										   {
											 echo "<option value='" . $i . "'>" ."0". $i . "</option>";
										   }
											else {
											  echo "<option value='" . $i . "'>" . $i . "</option>";
											}
										  } ?>
                                  </select>
                              </div>
                              <div class="col-md-4 col-sm-3 col-xs-12">
																<!-- Fixed by Nandini-->
                                  <select class="form-control" name="dob_mm" id="dob_mm" >
                                      <option value="">MM</option>
                                      <?php
                                      for ($m = 1; $m <= 12; $m++) {
                                        $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                        if($m < 10)
                                        {
                                          echo "<option value='" . $m . "'>" . $month . "</option>";
                                        }
                                        else{
                                          echo "<option value='" . $m . "'>" . $month . "</option>";
                                        }
                                      }
                                      ?>
                                  </select>
                                 
                              </div>
                              <div class="col-md-3 col-sm-4 col-xs-12">
															<!-- Fixed by Nandini-->
															
                                  <select name="dob_yy" id="dob_yy" class="form-control" >
																	
                                      <option value="">YYYY</option>
                                      <?php
                                      $starting_year = date('Y', strtotime('-80 year'));
                                      $ending_year = date('Y');
                                      $current_year = date('Y');
                                      for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
                                        echo '<option value="' . $starting_year . '"';
                                        echo ' >' . $starting_year . '</option>';
                                      }?>
                                      </select>
                                  
                              </div>
                          </div>
						  <br/>
				   <div class="input-group">
					<span class="input-group-addon">Select Image</span>
                                        <input type="file" name="photo" class="form-control photo" required/>
				  </div>
				  <p class="help-block" style="margin-left:40px;">Upload Profile photo here</p>
				  <br/>
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
                                  
				  </div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
          </div>
          <!-- /.box -->

          
          

        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
          <!-- /.box -->
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>