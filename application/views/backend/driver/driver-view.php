<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Drivers</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('driver/hide'); ?>" />
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Drivers list</h3>
			  
            </div>
			<div class="text-right" style="margin-right:10px;"> <b>Bulk Action :</b> 
			<input type="hidden" class="action_url" value="<?php echo base_url('driver/bulk_action/'); ?>" />
				<button class="btn btn-app bulk-action" data-action="activate" style="background: #00a65a;color: #fff;border-color: #00a65a;" href="#" ><i class="fa fa-eye"></i> Activate</button>
				
				
				<button class="btn btn-app bulk-action" data-action="deactivate" href="#" ><i class="fa fa-trash"></i> Deactivate</button>
				
				
				<button class="btn btn-app bulk-action" data-action="unblock" style="background: #00a65a;color: #fff;border-color: #00a65a;" href="#"><i class="fa fa-eye"></i> Unblock</button>
				
				
				<button class="btn btn-app bulk-action" data-action="block" href="#" ><i class="fa fa-trash"></i> Block</button>
			 	
			</div>
            <!-- /.box-header -->
            <div class="box-body">
				
              <table id="user-table" class="table table-bordered table-striped">
                <thead>
                <tr>
				<th style="width:60px;" class="no-sort"><input type="checkbox" class="allcheck checkaction" id="allchecktoggle" /> Sno</th>
				<th>Driver ID</th>
				<th>Photo</th>
                <th>Username</th>
                <th>Email ID</th>
                <th>Mobile</th>
                <th>Active/Inactive</th>
                <th>Block/Unblock</th>
                <th class="no-sort">Action</th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($driverlist)){
					foreach($driverlist as $data){ ?>
					<tr id="row_<?php echo $data->id; ?>">
					  <td><input type="checkbox" class="checkaction" name="userid_box" value="<?php echo $data->id; ?>" /> <?php echo $data->id; ?></td>
					  <td><?php echo $data->unique_id; ?></td>
					  <td><?php if($data->photo !='') {?><img src="<?php echo base_url().$data->photo; ?>" width="40" /><?php } ?></td>
            <!--fixed by nandini-->
					  <td style="word-break: break-word;"><?php echo $data->username; ?></td>
					  <td style="word-break: break-word;"><?php echo $data->emailid; ?></td>
					  <td style="word-break: break-word;"><?php echo $data->mobile; ?></td>
					  <td><?= $data->status == 0 ?"Inactive":"Active";?></td>
					  <td><?= $data->webblock == 0 ?"Blocked":"Unblocked";?></td>
					  <td>
						<?php if($data->status==0){ ?>
						<a class="btn btn-app" style="background: #00a65a;color: #fff;border-color: #00a65a;" href="<?php echo base_url('driver/activate/'.$data->id); ?>"><i class="fa fa-eye"></i> Activate</a>
						<?php } ?>
						<a class="btn btn-app" href="<?php echo base_url('vehicles/driver_vehicle/'.$data->id); ?>" ><i class="fa fa-car"></i> Vehicle</a>
						<a class="btn btn-app" href="<?php echo base_url('driver/viewdriver/'.$data->id); ?>"><i class="fa fa-eye"></i> View</a>
						<!--<a class="btn btn-app" href="<?php echo base_url('driver/edit_driver/'.$data->id); ?>"><i class="fa fa-edit"></i> Edit</a>-->
						<?php if($data->status==1){ ?>
                                                <button class="btn btn-app delete-alert" data-id="<?php echo $data->id; ?>" href="#" ><i class="fa fa-trash"></i> Deactivate</button>
                                               <?php } ?>
					  </td>
					</tr>
					<?php }
				}?>
                </tbody>
                <tfoot>
                <tr>
				<th>Sno</th>
				<th>Driver ID</th>
				<th>Photo</th>
                <th>Username</th>
                <th>Email ID</th>
                <th>Mobile</th>
                <th>Active/Inactive</th>
                <th>Block/Unblock</th>
                <th style="width:300px;">Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>