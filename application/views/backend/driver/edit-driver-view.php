
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Driver</li>
        <li class="active">Edit</li>
      </ol>
    </section>

<div class="container">
  
  <ul class="nav nav-tabs">
    <li class="active"><a href="#">Driver Profile</a></li>
    <li><a href="#">Vehicle Details</a></li>
    <li><a href="#">Rides Details</a></li>
    <li><a href="#">Transaction Details</a></li>
  </ul>
</div>
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      
      <!-- /.box -->

      <div class="row">
          
          <div class="profile">
		<div class="col-md-3">
                    
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="http://lorempixel.com/output/people-q-c-100-100-1.jpg" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						Marcus Doe
					</div>
					<div class="profile-usertitle-job">
						Developer
					</div>
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">
					<button type="button" class="btn btn-success btn-sm">Follow</button>
					<button type="button" class="btn btn-danger btn-sm">Message</button>
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="#">
							<i class="glyphicon glyphicon-home"></i>
							Overview </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-user"></i>
							Account Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-ok"></i>
							Tasks </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-flag"></i>
							Help </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		
	</div>
          
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Driver Details</h3>
            </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>
			
				<div class="box-body">
                        <form method="post" action="<?php echo base_url('driver/updatedriver/'.$agentdata[0]->id); ?>" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">            
                                    
                                    <div class="form-group">
                                <?php echo form_label('Email', 'textArea', ['class' => 'col-md-4 control-label']); ?>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span><?php echo form_input(array('id' => 'email', 'name' => 'email', 'value'=> $agentdata[0]->emailid, 'class' => 'form-control', 'placeholder' => 'Email', 'readonly'=>'true')); ?></div> 
                            <?php echo form_error('address'); ?>
                            </div>
                        </div>
                            <div class="form-group">
                                <?php echo form_label('First Name', 'textArea', ['class' => 'col-md-4 control-label']); ?>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span><?php echo form_input(array('id' => 'email', 'name' => 'email', 'value'=> $agentdata[0]->fname, 'class' => 'form-control', 'placeholder' => 'Email')); ?></div> 
                            <?php echo form_error('address'); ?>
                            </div>
                        </div>
				  <div class="form-group">
                                <?php echo form_label('Middle Name', 'textArea', ['class' => 'col-md-4 control-label']); ?>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span><?php echo form_input(array('id' => 'email', 'name' => 'email', 'value'=> $agentdata[0]->mname, 'class' => 'form-control', 'placeholder' => 'Email')); ?></div> 
                            <?php echo form_error('address'); ?>
                            </div>
                        </div>
                        <div class="form-group">
         <?php echo form_label('Last Name', 'textArea', ['class' => 'col-md-4 control-label']); ?>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span><?php echo form_input(array('id' => 'email', 'name' => 'email', 'value'=> $agentdata[0]->emailid, 'class' => 'form-control', 'placeholder' => 'Email')); ?></div> 
                            <?php echo form_error('address'); ?>
                            </div>
                        </div>
				  
				   <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" name="mname" class="form-control" required placeholder="Middle Name" value="<?php echo $agentdata[0]->mname; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?php echo $agentdata[0]->lname; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				 
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <lable>Mobile</lable>
                                        <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" value="<?php echo $agentdata[0]->mobile; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<label>
					  <input type="radio" name="gender" class="minimal" value="male" <?php if($agentdata[0]->gender != null){ if($agentdata[0]->gender=='male'){ echo 'checked'; } }else{ echo 'checked';} ?> /> Male
					</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<label>
					  <input type="radio" name="gender" class="minimal" value="female" <?php if($agentdata[0]->gender=='female'){ echo 'checked'; }?> /> Female
					</label>
				  </div>
				  <br/>
				  <!-- /.form group -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
          </div>
          <!-- /.box -->

          
          

        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->
 
    </section>
    <!-- /.content -->
  </div>