
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Driver
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Drivers</li>
        <li class="active">View</li>
      </ol>
    </section>
    <br>
<div class="container">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#">Driver Profile</a></li>
    <li><a href="#">Vehicle Details</a></li>
    <li><a href="#">Rides Details</a></li>
    <li><a href="#">Transaction Details</a></li>
  </ul>
</div>
    <section class="content">
     <div class="row">
		<div class="col-md-1"></div>
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Driver Details</h3>
            </div>
				<div class="box-body">
				<div class="input-group">
					<img src="<?php echo base_url($agentdata[0]->photo); ?>" />
				  </div>
				  
				  <br/>
				  <!-- /.form group -->
				  <p style="margin-bottom: 0;"><label>Email ID</label></p>
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="email" name="emailid" class="form-control" disabled placeholder="Email ID" value="<?php echo $agentdata[0]->emailid; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <p style="margin-bottom: 0;"><label>First Name</label></p>
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" name="first_name" class="form-control" disabled placeholder="First Name" value="<?php echo $agentdata[0]->fname; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <p style="margin-bottom: 0;"><label>Middle Name</label></p>
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" name="mname" class="form-control" disabled placeholder="Middle Name" value="<?php echo $agentdata[0]->mname; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <p style="margin-bottom: 0;"><label>Last Name</label></p>
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" name="last_name" class="form-control" disabled placeholder="Last Name" value="<?php echo $agentdata[0]->lname; ?>" />
				  </div>
				  <br/>
				  
				  <!-- /.form group -->
				  <p style="margin-bottom: 0;"><label>Mobile Number</label></p>
				  <div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" name="mobile" class="form-control" disabled placeholder="Mobile Number" value="<?php echo $agentdata[0]->mobile; ?>" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<label>
					  <input type="radio" name="gender" class="minimal" value="male" <?php if($agentdata[0]->gender != null){ if($agentdata[0]->gender=='male'){ echo 'checked'; }else{ echo 'disabled';} }else{ echo 'checked';} ?> /> Male
					</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<label>
					  <input type="radio" name="gender" class="minimal" value="female" <?php if($agentdata[0]->gender=='female'){ echo 'checked'; }else{ echo 'disabled';}?> /> Female
					</label>
				  </div>
				  <br/>
				  </div>
				<!-- /.box-body -->
				<div class="box-footer">
					<a href="#"><button type="button" class="btn btn-primary">Edit</button></a>
				</div>
          </div>
        </div>
		<div class="col-md-1"></div>
        <div class="col-md-4">	
        </div>
      </div>
    </section>
  </div>