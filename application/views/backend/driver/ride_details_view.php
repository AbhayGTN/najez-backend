<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Users</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('user/hide'); ?>" />
    <!-- Main content -->
    <div class="container">
  
  <ul class="nav nav-tabs">
      <li><a href="<?= base_url('driver/viewdriver/'.$agentdata[0]->id); ?>">Driver Details</a></li>
    <li><a href="<?php echo base_url('vehicles/driver_vehicle/'.$agentdata[0]->id); ?>">Vehicle Details</a></li>
    <li class="active"><a href="<?= base_url('driver/ride_details/'.$agentdata[0]->id); ?>">Rides Details</a></li>
    <li><a href="<?= base_url('driver/transaction_details/'.$agentdata[0]->id); ?>">Transaction Details</a></li>
  </ul>
</div>
    
    
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Users list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
		<th>Sno</th>
		 <th>Name</th>
		 <th>Email</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>Date</th>
                  <th>Distance</th>
                  <th>Waiting Amount</th>
                  <th>Total Amount</th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($userlist)){
					$x=1; foreach($userlist as $data){ ?>
					<tr id="row_<?php echo $data['id']; ?>">
					  <td><?php echo $x++; ?></td>
					  <td><?php echo $data['name']; ?></td>
					  <td><?php echo $data['email']; ?></td>
					  <td><?php echo date("H:i:s", ($data['ride_start_time'] / 1000) ); ?></td>
                                          <td><?php echo date("H:i:s", ($data['ride_end_time'] / 1000) ); ?></td>
                                          <td><?php echo $data['date']; ?></td>
                                          <td><?php echo $data['distance']; ?> :km</td>
                                          <td><?php echo $data['distance_amount']; ?></td>
                                          <td><?php echo $data['total_amount']; ?></td>
					</tr>
					<?php }
				}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Sno</th>
                  <th>User ID</th>
				  <th>Photo</th>
                  <th>Username</th>
                  <th>Email ID</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>