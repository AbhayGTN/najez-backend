<style>
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>
<input type="hidden" class="delete_url" value="<?php echo base_url('driver/hide'); ?>" />
<input type="hidden" class="redirect_url" value="<?php echo base_url('driver/view_drivers'); ?>" />
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Driver</li>
        <li class="active">Edit</li>
      </ol>
    </section>

<div class="container">
  
  <ul class="nav nav-tabs">
    <li class="active"><a href="#">Driver Details</a></li>
    <li><a href="<?php echo base_url('vehicles/driver_vehicle/'.$agentdata[0]->id); ?>">Vehicle Details</a></li>
    <li><a href="<?= base_url('driver/ride_details/'.$agentdata[0]->id); ?>">Rides Details</a></li>
    <li><a href="<?= base_url('driver/transaction_details/'.$agentdata[0]->id); ?>">Transaction Details</a></li>
  </ul>
</div>
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      
      <!-- /.box -->

      <div class="row">
          
          <div class="profile">
		<div class="col-md-3">
                    
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="<?= base_url($agentdata[0]->photo);  ?>" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						<?= $agentdata[0]->fname; ?> <?= $agentdata[0]->mname; ?> <?= $agentdata[0]->lname; ?>
					</div>
					<!--<div class="profile-usertitle-job">
						<?= $agentdata[0]->lname; ?>
					</div>-->
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<form id="uploadimage" action="<?php echo base_url('driver/update_profileimg/'.$agentdata[0]->id); ?>" method="post" enctype="multipart/form-data">
				<div class="profile-userbuttons">
					
						<div class="fileUpload btn btn-primary">
							<span>Change Image</span>
							<input type="file" class="upload" name="profileimg" id="profileimg" />
						</div>
						<input type="hidden" name="old_profileimg" value="<?php echo $agentdata[0]->photo; ?>" />
					
					<!--<button type="button" class="btn btn-danger btn-sm">Block</button>-->
				<!--	<a class="delete-alert" data-id="<?php echo $agentdata[0]->id; ?>" href="#" ><button type="button" class="btn btn-danger btn-sm">Block</button></a> -->
                                        
                                        <?php if($agentdata[0]->webblock == '0'){ ?>
                                        <a href="<?= base_url('driver/unblock_driver/'.$agentdata[0]->id); ?>" ><button type="button" class="btn btn-success btn-sm">Unblock</button></a>
                                        <?php } if($agentdata[0]->webblock == '1'){ ?> 
                                        <a href="<?= base_url('driver/block_driver/'.$agentdata[0]->id); ?>" ><button type="button" class="btn btn-danger btn-sm">Block</button></a>
                                        <?php }  ?>
                                        <p style="color:red"><?php echo $this->session->flashdata('cannot_block_user_msg'); ?></p>
				</div>
				</form>
                                <div class="profile-usermenu">
					<ul class="nav">
						<li>
							
							<i class="glyphicon glyphicon-home"></i>
							Total Vehicle : <?= $stats['total_vehical'][0]['num']; ?>
						</li>
						<li>
							
							<i class="glyphicon glyphicon-user"></i>
							Active Vehicle : <?= $stats['active_vehical'][0]['num']; ?>
						</li>
						<li>
							
							<i class="glyphicon glyphicon-ok"></i>
							Inactive Vehicle : <?= $stats['inactive_vehical'][0]['num']; ?>
						</li>
						<li>
							
							<i class="glyphicon glyphicon-flag"></i>
							Today Ride : <?= $stats['total_rides'][0]['num']; ?>
						</li>
                                                <li>
							
							<i class="glyphicon glyphicon-flag"></i>
							Today Earning : <?= $stats['total_earnings'][0]['num']; ?>
						</li>
					</ul>
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				
				<!-- END MENU -->
			</div>
		</div>
		
	</div>
          
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Driver Details</h3>
            </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>
			
				<div class="box-body">
                        <form method="post" action="<?php echo base_url('driver/updatedriver/'.$agentdata[0]->id); ?>" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">            
                                    
                                    <div class="form-group">
                                
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><?php echo form_label('Email', 'textArea', ['class' => 'col-md-4 control-label']); ?></span><?php echo form_input(array('id' => 'email', 'name' => 'email', 'value'=> $agentdata[0]->emailid, 'class' => 'form-control', 'placeholder' => 'Email', 'readonly'=>'true')); ?></div> 
                            <?php echo form_error('email'); ?>
                            </div>
                        </div>
						<div class="form-group">
                                
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><?php echo form_label('First Name', 'textArea', ['class' => 'col-md-4 control-label']); ?></span><?php echo form_input(array('id' => 'fname', 'name' => 'fname', 'value'=> $agentdata[0]->fname, 'class' => 'form-control', 'placeholder' => 'First Name')); ?></div> 
                            <?php echo form_error('fname'); ?>
                            </div>
                        </div>
						<div class="form-group">
                                
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><?php echo form_label('Middle Name', 'textArea', ['class' => 'col-md-4 control-label']); ?></span><?php echo form_input(array('id' => 'mname', 'name' => 'mname', 'value'=> $agentdata[0]->mname, 'class' => 'form-control', 'placeholder' => 'Middle Name')); ?></div> 
                            <?php echo form_error('mname'); ?>
                            </div>
                        </div>
                        <div class="form-group">
							
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><?php echo form_label('Last Name', 'textArea', ['class' => 'col-md-4 control-label']); ?></span><?php echo form_input(array('id' => 'lname', 'name' => 'lname', 'value'=> $agentdata[0]->lname, 'class' => 'form-control', 'placeholder' => 'Last Name')); ?></div> 
                            <?php echo form_error('lname'); ?>
                            </div>
                        </div>
					<div class="form-group">
						
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon"><?php echo form_label('Mobile', 'textArea', ['class' => 'col-md-4 control-label']); ?></span><?php echo form_input(array('id' => 'mobile', 'name' => 'mobile', 'value'=> $agentdata[0]->mobile, 'class' => 'form-control', 'placeholder' => 'Mobile')); ?></div> 
						<?php echo form_error('mobile'); ?>
						</div>
					</div> 
					<div class="form-group">
						
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon"><?php echo form_label('National ID/Iqama', 'textArea', ['class' => 'col-md-4 control-label']); ?></span><?php echo form_input(array('id' => 'iqama', 'name' => 'iqama', 'value'=> $agentdata[0]->iqama, 'class' => 'form-control', 'placeholder' => 'National ID/Iqama')); ?></div> 
						<?php echo form_error('identification_no'); ?>
						</div>
					</div>
					<div class="form-group">
						
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon"><?php echo form_label('City', 'textArea', ['class' => 'col-md-4 control-label']); ?></span>
								<select class="form-control" name="city" id="city">
									<option value="">City</option>
									<option value="Bahrain" <?php if($agentdata[0]->work_city == "Bahrain"){ echo 'selected';}?>>Bahrain</option>
									<option value="Dammam" <?php if($agentdata[0]->work_city == "Dammam"){ echo 'selected';}?>>Dammam</option>
									<option value="Dhahran" <?php if($agentdata[0]->work_city == "Dhahran"){ echo 'selected';}?>>Dhahran</option>
									<option value="Hassa" <?php if($agentdata[0]->work_city == "Hassa"){ echo 'selected';}?>>Hassa</option>
									<option value="Hafar Al-Batin" <?php if($agentdata[0]->work_city == "Hafar Al-Batin"){ echo 'selected';}?>>Hafar Al-Batin</option>
									<option value="Jeddah" <?php if($agentdata[0]->work_city == "Jeddah"){ echo 'selected';}?>>Jeddah</option>
									<option value="Jubail" <?php if($agentdata[0]->work_city == "Jubail"){ echo 'selected';}?>>Jubail</option>
									<option value="Kharj" <?php if($agentdata[0]->work_city == "Kharj"){ echo 'selected';}?>>Kharj</option>
									<option value="Khobar" <?php if($agentdata[0]->work_city == "Khobar"){ echo 'selected';}?>>Khobar</option>
									<option value="Khafji" <?php if($agentdata[0]->work_city == "Khafji"){ echo 'selected';}?>>Khafji</option>
									<option value="Qatif" <?php if($agentdata[0]->work_city == "Qatif"){ echo 'selected';}?>>Qatif</option>
									<option value="Qassim" <?php if($agentdata[0]->work_city == "Qassim"){ echo 'selected';}?>>Qassim</option>
									<option value="Riyadh" <?php if($agentdata[0]->work_city == "Riyadh"){ echo 'selected';}?>>Riyadh</option>
								</select>
							</div> 
						<?php echo form_error('city'); ?>
						</div>
					</div>
					<div class="form-group">
						
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon"><?php echo form_label('IBAN', 'textArea', ['class' => 'col-md-4 control-label']); ?></span>
								<input type="text" class="form-control" name="iban" id="iban" placeholder="IBAN Number" value="<?php echo $agentdata[0]->iban;?>" />
							</div>
						</div>
					</div>
					<div class="form-group">
						
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon"><?php echo form_label('Bank', 'textArea', ['class' => 'col-md-4 control-label']); ?></span>
								<select class="form-control" name="iban_bank" id="iban_bank" >
									<option value="">Bank</option>
									<option value="The National Commercial Bank" <?php if($agentdata[0]->iban_bank =='The National Commercial Bank'){ echo'selected'; } ?> >The National Commercial Bank</option>
										<option value="The Saudi British Bank" <?php if($agentdata[0]->iban_bank =='The Saudi British Bank'){ echo'selected'; } ?> >The Saudi British Bank</option>
										<option value="Saudi Investment Bank" <?php if($agentdata[0]->iban_bank =='Saudi Investment Bank'){ echo'selected'; } ?> >Saudi Investment Bank</option>
										<option value="Alinma Bank" <?php if($agentdata[0]->iban_bank =='Alinma Bank'){ echo'selected'; } ?> >Alinma Bank</option>
										<option value="Banque Saudi Fransi" <?php if($agentdata[0]->iban_bank =='Banque Saudi Fransi'){ echo'selected'; } ?> >Banque Saudi Fransi</option>
										<option value="Riyad Bank" <?php if($agentdata[0]->iban_bank =='Riyad Bank'){ echo'selected'; } ?> >Riyad Bank</option>
										<option value="Samba Financial Group (Samba)" <?php if($agentdata[0]->iban_bank =='Samba Financial Group (Samba)'){ echo'selected'; } ?> >Samba Financial Group (Samba)</option>
										<option value="Saudi Hollandi Bank (Alawwal)" <?php if($agentdata[0]->iban_bank =='Saudi Hollandi Bank (Alawwal)'){ echo'selected'; } ?> >Saudi Hollandi Bank (Alawwal)</option>
										<option value="Al Rajhi Bank" <?php if($agentdata[0]->iban_bank =='Al Rajhi Bank'){ echo'selected'; } ?> >Al Rajhi Bank</option>
										<option value="Arab National Bank" <?php if($agentdata[0]->iban_bank =='Arab National Bank'){ echo'selected'; } ?> >Arab National Bank</option>
										<option value="Bank Al-Bilad" <?php if($agentdata[0]->iban_bank =='Bank Al-Bilad'){ echo'selected'; } ?> >Bank Al-Bilad</option>
										<option value="Bank AlJazira" <?php if($agentdata[0]->iban_bank =='Bank AlJazira'){ echo'selected'; } ?> >Bank AlJazira</option>
										<option value="Asia Bank" <?php if($agentdata[0]->iban_bank =='Asia Bank'){ echo'selected'; } ?> >Asia Bank</option>
										<option value="Qatar National Bank (QNB)" <?php if($agentdata[0]->iban_bank =='Qatar National Bank (QNB)'){ echo'selected'; } ?> >Qatar National Bank (QNB)</option>
										<option value="State Bank of India (SBI)" <?php if($agentdata[0]->iban_bank =='State Bank of India (SBI)'){ echo'selected'; } ?> >State Bank of India (SBI)</option>
										<option value="Gulf International Bank (GIB)" <?php if($agentdata[0]->iban_bank =='Gulf International Bank (GIB)'){ echo'selected'; } ?> >Gulf International Bank (GIB)</option>
										<option value="Emirates NBD" <?php if($agentdata[0]->iban_bank =='Emirates NBD'){ echo'selected'; } ?> >Emirates NBD</option>
										<option value="National Bank of Bahrain (NBB)" <?php if($agentdata[0]->iban_bank =='National Bank of Bahrain (NBB)'){ echo'selected'; } ?> >National Bank of Bahrain (NBB)</option>
										<option value="National Bank of Kuwait (NBK)" <?php if($agentdata[0]->iban_bank =='National Bank of Kuwait (NBK)'){ echo'selected'; } ?> >National Bank of Kuwait (NBK)</option>
										<option value="Muscat Bank" <?php if($agentdata[0]->iban_bank =='Muscat Bank'){ echo'selected'; } ?> >Muscat Bank</option>
										<option value="Deutsche Bank" <?php if($agentdata[0]->iban_bank =='Deutsche Bank'){ echo'selected'; } ?> >Deutsche Bank</option>
										<option value="BNP Paribas" <?php if($agentdata[0]->iban_bank =='BNP Paribas'){ echo'selected'; } ?> >BNP Paribas</option>
										<option value="J.P. Morgan Chase N.A" <?php if($agentdata[0]->iban_bank =='J.P. Morgan Chase N.A'){ echo'selected'; } ?> >J.P. Morgan Chase N.A</option>
										<option value="National Bank of Pakistan (NBP)" <?php if($agentdata[0]->iban_bank =='National Bank of Pakistan (NBP)'){ echo'selected'; } ?> >National Bank of Pakistan (NBP)</option>
										<option value="T.C.ZIRAAT BANKASI A.S." <?php if($agentdata[0]->iban_bank =='T.C.ZIRAAT BANKASI A.S.'){ echo'selected'; } ?> >T.C.ZIRAAT BANKASI A.S.</option>
										<option value="Industrial and Commercial Bank of China (ICBC)" <?php if($agentdata[0]->iban_bank =='Industrial and Commercial Bank of China (ICBC)'){ echo'selected'; } ?> >Industrial and Commercial Bank of China (ICBC)</option>
								</select>
							</div>
						</div>
					</div>
                            
                            <div class="form-group">
						
						<div class="col-md-8">
							<div class="input-group">
								<span class="input-group-addon"><?php echo form_label('For Bahrain', 'textArea', ['class' => 'col-md-4 control-label']); ?></span>
                                                                
                                                               <?php echo form_dropdown('For Bahrain', ['0'=>'No','1'=>'Yes'], $agentdata[0]->for_bahrain);?>
								<select class="form-control" name="iban_bank" id="iban_bank" >
									<option value="1">Yes</option>
                                                                        <option value="0" selected="true">No</option>
								</select>
							</div>
						</div>
					</div>
                            
					<div class="form-group">
						<?php echo form_label('DOB', 'textArea', ['class' => 'col-md-4 control-label']); ?>
						<?php if($agentdata[0]->dob !=''){ $dob = explode('/',$agentdata[0]->dob); }?>
						<div class="col-md-8"><div class="row">
						<div class="col-md-4 col-sm-3 col-xs-12">
						  <select name="dob_dd" id="dob_dd" class="form-control">
							  <option value="">DD</option>
								  <?php for ($i = 1; $i < 32; $i++) {
								   if($i < 10)
								   {
									 echo "<option value='" . $i . "' "; if($i==$dob[0]){ echo 'selected';} echo ">" ."0". $i . "</option>";
								   }
									else {
									  echo "<option value='" . $i . "' "; if($i==$dob[0]){ echo 'selected';} echo ">" . $i . "</option>";
									}
								  } ?>
						  </select>
						  </div>
						  <div class="col-md-4 col-sm-3 col-xs-12">
						  <select class="form-control" name="dob_mm" id="dob_mm">
							  <option value="">MM</option>
							  <?php
							  for ($m = 1; $m <= 12; $m++) {
								$month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
								if($m < 10)
								{
								  echo "<option value='" . $m . "' "; if($m==$dob[1]){ echo 'selected';} echo ">" . $month . "</option>";
								}
								else{
								  echo "<option value='" . $m . "' "; if($m==$dob[1]){ echo 'selected';} echo ">" . $month . "</option>";
								}
							  }
							  ?>
						  </select>
						 
					  </div>
					  <div class="col-md-4 col-sm-4 col-xs-12">
						  <select name="dob_yy" id="dob_yy" class="form-control">
							  <option value="">YYYY</option>
							  <?php
							  $starting_year = date('Y', strtotime('-80 year'));
							  $ending_year = date('Y');
							  $current_year = date('Y');
							  for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
								echo '<option value="' . $starting_year . '" ';
								if($starting_year==$dob[2]){ echo 'selected';}
								echo ' >' . $starting_year . '</option>';
							  }?>
							  </select>
						  
					  </div>
					  </div>
					  </div>
					</div>	
				  <!-- /.form group -->
				  <div class="form-group">
					   <?php echo form_label('Gender', 'textArea', ['class' => 'col-md-4 control-label']); ?>
					  <div class="col-md-8">
					  <div class="input-group">
						<label>
						  <input type="radio" name="gender" class="minimal" value="Male" <?php if($agentdata[0]->gender != null){ if($agentdata[0]->gender=='Male'){ echo 'checked'; } }else{ echo 'checked';} ?> /> Male
						</label>&nbsp;&nbsp;&nbsp;&nbsp;
						<label>
						  <input type="radio" name="gender" class="minimal" value="Female" <?php if($agentdata[0]->gender=='Female'){ echo 'checked'; }?> /> Female
						</label>
					  </div>
					  </div>
				  </div>
				  <br/>
				  <!-- /.form group -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
          </div>
          <!-- /.box -->

          
          

        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>