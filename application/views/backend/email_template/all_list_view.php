<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Users</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('user/hide'); ?>" />
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Users list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>S.No</th>
				  <th>Tamplate Name</th>
                  <th>User Type</th>
                  
                  <th style="width:300px;">Action</th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($tamplate_list)){
                                      $x=1;	foreach($tamplate_list as $data){ ?>
					<tr id="row_<?php echo $data['id']; ?>">
					  <td><?php echo $x++; ?></td>
					  <td><?= $data['template_name'];  ?></td>
					  <td style="word-break: break-word;"><?= $data['user_type'];  ?></td>
				
					  <td>
					<!--	<?php if($data->status == 0){ ?>
						<a class="btn btn-app" style="background: #00a65a;color: #fff;border-color: #00a65a;" href="<?php echo base_url(''); ?>"><i class="fa fa-eye"></i> Activate</a>
						<?php } ?>-->
						<a class="btn btn-app" href="<?php echo base_url('email/view/'.$data['template_id']); ?>"><i class="fa fa-eye"></i> Edit</a>
						
						
					  </td>
					</tr>
					<?php }
				}?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>