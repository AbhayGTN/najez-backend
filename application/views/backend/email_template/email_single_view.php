<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->	<style>.anti{margin:0;}</style>
    <section class="content-header">
      <h1>
        <?= $email_temp[0]['template_name']; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Settings</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- SELECT2 EXAMPLE -->
      <!-- /.box -->
      <div class="row">
		<div class="col-md-1"></div>
        <div class="col-md-12">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Email Settings</h3>
            </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>			
      <?php if($email_temp[0]['user_type'] == 'user'){				
          $data['email_temp'] = $email_temp;				
          $this->load->view('backend/email_template/user_welcome_email',$data);			
        } 
        if($email_temp[0]['user_type'] == 'driver'){ 				
          $data['email_temp'] = $email_temp;				
          $this->load->view('backend/email_template/driver_welcome_email',$data);			
        } ?><br>
              
        </div>
          <!-- /.box -->
        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
          <!-- /.box -->
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>