
<form class="form-horizontal" action="<?= base_url('email/save_data/'.$email_temp[0]['template_id']);  ?>" enctype="multipart/form-data" method="post">	

                  <div class="row anti">

                      <div class="col-md-6">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">English Subject</label>

                              </div>

                          </div>

                          

                          <input type="text" class="form-control" name="en_subject" value="<?php echo $email_temp[0]['meta_value_en'];?>" />

                      </div>

                      <div class="col-md-6">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Arabic Subject</label>

                              </div>

                          </div>

                          <input type="text" class="form-control" name="ar_subject" value="<?php echo $email_temp[0]['meta_value_ar'];?>" />

                      </div>

                  </div>

                  

                  

                  

                  <div class="row anti">

                      <div class="col-md-6">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">English Title</label>

                              </div>

                          </div>

                          

                          <input type="text" class="form-control" name="en_title" value="<?php echo $email_temp[1]['meta_value_en'];?>" />

                      </div>

                      <div class="col-md-6">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Arabic Title</label>

                              </div>

                          </div>

                          <input type="text" class="form-control" name="ar_title" value="<?php echo $email_temp[1]['meta_value_ar'];?>" />

                      </div>

                  </div>

                  

                  

                  <div class="row anti">

                      <div class="col-md-6">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">English Sub Title</label>

                              </div>

                          </div>

                          

                          <input type="text" class="form-control" name="en_subtitle" value="<?php echo $email_temp[2]['meta_value_en'];?>" />

                      </div>

                      <div class="col-md-6">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Arabic Sub Title</label>

                              </div>

                          </div>

                          <input type="text" class="form-control" name="ar_subtitle" value="<?php echo $email_temp[2]['meta_value_ar'];?>" />

                      </div>

                  </div>

                  

                  

                  <div class="row anti">

                      <div class="col-md-12">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">English Content</label>

                              </div>

                          </div>

                          

                          <textarea class="form-control" rows="3" id="textArea" name="en_content"><?php echo $email_temp[3]['meta_value_en'];?></textarea>

                      </div>

                      <div class="col-md-12">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Arabic Content</label>

                              </div>

                          </div>

                          <textarea class="form-control" rows="3" id="textArea" name="ar_content"><?php echo $email_temp[3]['meta_value_ar'];?></textarea>

                      </div>

                  </div>

                  

                  

                  <div class="row anti">

                      <div class="col-md-12">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">English Footer</label>

                              </div>

                          </div>

                          

                          <textarea class="form-control" rows="3" id="textArea" name="en_footer"><?php echo $email_temp[4]['meta_value_en'];?></textarea>

                      </div>

                      <div class="col-md-12">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Arabic Footer</label>

                              </div>

                          </div>

                          <textarea class="form-control" rows="3" id="textArea" name="ar_footer"><?php echo $email_temp[4]['meta_value_ar'];?></textarea>

                      </div>

                  </div>
				  
				  <div class="row anti">

                      <div class="col-md-12">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Terms and Condition English</label>

                              </div>

                          </div>

                          

                          <textarea class="form-control" rows="3" name="en_tnc"><?php echo $email_temp[5]['meta_value_en'];?></textarea>

                      </div>

                      <div class="col-md-12">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Terms and Condition Arabic</label>

                              </div>

                          </div>

                          <textarea class="form-control" rows="3" id="textArea" name="ar_tnc"><?php echo $email_temp[5]['meta_value_ar'];?></textarea>

                      </div>

                  </div>
				<div class="row anti">

                      <div class="col-md-6">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">English Footer2</label>

                              </div>

                          </div>

                          

                          <input type="text" class="form-control" name="en_footer2" value="<?php echo $email_temp[6]['meta_value_en'];?>" />

                      </div>

                      <div class="col-md-6">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Arabic Footer2</label>

                              </div>

                          </div>

                          <input type="text" class="form-control" name="ar_footer2" value="<?php echo $email_temp[6]['meta_value_ar'];?>" />

                      </div>

                  </div>
					<div class="row anti">

                      <div class="col-md-12">

                          <div class="row">

                              <div class="col-md-12">

                                <label for="textArea" class="control-label">Attachment</label>
								<input type="file" name="attch" /><br/>
								<?php
                                //print_r($email_temp);
                                
                                //Fixed By Nandini

								if($email_temp[7]['meta_value_en'] != ''){
									echo '<img src="'.base_url().'/assets/images/attachment-icon.png" width="40"/> <a href="'.base_url().''.$email_temp[7]['meta_value_en'].'" target="_blank">View Attachment</a>';
								}?>
                              </div>

                          </div>

                      </div>
					 </div>
				<div class="row anti" style="margin-top:10px;">

				  <div class="col-lg-12">

					

					<button type="submit" class="btn btn-primary">Submit</button>

				  </div>

				</div>
              </form>