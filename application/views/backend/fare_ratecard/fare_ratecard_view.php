<div class="content-wrapper" style="min-height: 946px;">
    <section class="content">
         <div class="box box-info">
            <section class="content-header">
        <h1>
            Fare Rate Card<hr>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cards</a></li>
            <li class="active">Add Card</li>
        </ol>
    </section>
            
             <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th></th>
                  <td></td>
                  <td></td>
                  <td></td>
                  <th>now</th>
                  <td></td>
                  <th>Later</th>
                </tr>
                  <tr>
                  <th>Category</th>
                  <th>Km</th>
                  <th>Waiting Per Minutes</th>
                  <th>Starting</th>
                  <th>Minimum</th>
                  <th>Starting</th>
                  <th>Minimum</th>
                </tr>
                <tr>
                  <th>Delievery</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table1'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table1'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table1'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table1'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table1'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table1'][23]['charges'];?>" name=""></td>
                </tr>
                <tr>
                  <th>Medium</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table1'][0]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table1'][4]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table1'][8]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table1'][12]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table1'][16]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table1'][20]['charges'];?>" name=""></td>
                </tr>
                <tr>
                  <th>Family</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table1'][1]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table1'][5]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table1'][9]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table1'][13]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table1'][17]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table1'][21]['charges'];?>" name=""></td>
                </tr>
                <tr>
                  <th>Luxary</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table1'][2]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table1'][6]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table1'][10]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table1'][14]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table1'][18]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table1'][22]['charges'];?>" name=""></td>
                </tr>
                <tr>
                  <th></th>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <th>Delievery</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table2'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table2'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table2'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table2'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table2'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table2'][23]['charges'];?>" name=""></td>
                  
                </tr>
                <tr>
                  <th>From City to City Medium</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table2'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table2'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table2'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum"type="text" value="<?= $fare['table2'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table2'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table2'][23]['charges'];?>" name=""></td>
                  
                </tr>
                <tr>
                  <th>From City to City Family</th>
                 <td><input class="form-control" title="km" type="text" value="<?= $fare['table2'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table2'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table2'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table2'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table2'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table2'][23]['charges'];?>" name=""></td>
                  
                </tr>
                <tr>
                  <th>From City to City Luxary</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table2'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table2'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table2'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table2'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table2'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table2'][23]['charges'];?>" name=""></td>
                  
                </tr>
                <tr>
                  <th></th>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                <tr>
                  <th>To Bahrain Medium</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table3'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table3'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table3'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table3'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table3'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="" name=""></td>
                  
                </tr>
                <tr>
                  <th>To Bahrain Family</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table3'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table3'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table3'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table3'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table3'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="" name=""></td>
                  
                </tr>
                <tr>
                  <th>To Bahrain Luxary</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table3'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table3'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table3'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table3'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table3'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="" name=""></td>
                  
                </tr>
                <tr>
                  <th>Airport</th>
                  <td><input class="form-control" title="km" type="text" value="<?= $fare['table3'][3]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Waiting Per Minutes" type="text" value="<?= $fare['table3'][7]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table3'][11]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="<?= $fare['table3'][15]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Starting" type="text" value="<?= $fare['table3'][19]['charges'];?>" name=""></td>
                  <td><input class="form-control" title="Minimum" type="text" value="" name=""></td>
                  
                </tr>
              </table>
            </div>
          </div>     
    </section>
</div>

