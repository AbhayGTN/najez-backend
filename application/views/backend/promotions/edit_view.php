<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Promotion's
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cards</a></li>
            <li class="active">Add Card</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-6">

                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Promotion Details</h3>
                    </div>
                    <div style="color:#ff0000;margin: 10px;">
                        <?php echo validation_errors(); ?>
                        <?php
                        if ($this->session->flashdata('flashdata')) {
                          echo '<p>' . $this->session->flashdata('flashdata') . '</p>';
                        }
                        ?>
                    </div>
                    <form method="post" action="<?php echo base_url('promotions/add_submit'); ?>" enctype="multipart/form-data" >
                        <div class="box-body">
                            <div class="input-group">
                                <span class="input-group-addon"><strong>Promo Code</strong></i></span>
                                <input type="text" name="promo_code" class="form-control" required placeholder="Promo Code" value="<?php echo $promo_data[0]['promo_code']; ?>" />
                            </div>
                            <br/>

                            <div class="input-group">
                                <span class="input-group-addon"><strong>Discount Percentage</strong></i></span>
                                <input type="text" name="discount_percentage" class="form-control" required placeholder="Discount Percentage" value="<?php echo $promo_data[0]['discount_percentage']; ?>" />
                            </div>
                            <br/>
                            <div class="input-group">
                                <span class="input-group-addon"><strong>User Limit</strong></i></span>
                                <input type="text" name="user_no" class="form-control" required placeholder="User Limit" value="<?php echo $promo_data[0]['user_no']; ?>" />
                            </div>
                            <br/>
                            <div class="input-group">
                                <span class="input-group-addon"><strong>Start Date</strong></i></span>
                                <input type="date" name="validity_start" class="form-control" required placeholder="Start Date" value="<?php echo $promo_data[0]['validity_start']; ?>" />
                            </div>
                            <br/>
                            <div class="input-group">
                                <span class="input-group-addon"><strong>End Date</strong></i></span>
                                <input type="date" name="validity_end" class="form-control" required placeholder="End Date" value="<?php echo $promo_data[0]['validity_end']; ?>" />
                            </div>
                            <br/>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
            </div>
        </div>
    </section>
</div>