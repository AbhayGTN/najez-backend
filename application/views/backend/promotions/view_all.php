<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title;  ?>
          
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Cards</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('promotions/hide'); ?>" />
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Cards list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Sno</th>
		<th>Promo Code</th>
                <th>Discount Percentage</th>
                <th>User Limit</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th style="width:300px;">Action</th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($cardlist)){
					//print_r($cardlist);die();
				$x=1;	foreach($cardlist as $data){
						
					?>
					<tr id="row_<?php echo $data['id']; ?>">
					  <td><?php echo $x++; ?></td>
					  <td><?php echo $data['promo_code']; ?></td>
					  <td style="word-break: break-word;"><?php echo $data['discount_percentage']; ?></td>
					  <td style="word-break: break-word;"><?php echo $data['user_no']; ?></td>
                                          <td style="word-break: break-word;"><?php echo $data['validity_start']; ?></td>
                                          <td style="word-break: break-word;"><?php echo $data['validity_end']; ?></td>
            
                                          
					  <td>						
						<a class="btn btn-app" href="<?php echo base_url('promotions/view/'.$data['id']); ?>"><i class="fa fa-eye"></i>View/Edit</a>
						<a class="btn btn-app delete-alert" data-id="<?php echo $data['id']; ?>" href="#" ><i class="fa fa-trash"></i>Delete</a>
					  </td>
					</tr>
					<?php }
				}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Sno</th>
		  <th>Card No</th>
                  <th>Price</th>
                  <th>Rides</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>