
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Card
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Cards</a></li>
        <li class="active">Add Card</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      
      <!-- /.box -->

      <div class="row">
		<div class="col-md-1"></div>
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Card Details</h3>
            </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>
			<form method="post" action="<?php echo base_url('ratecard/addcard'); ?>" enctype="multipart/form-data" >
				<div class="box-body">
					<div class="input-group">
                                            <span class="input-group-addon"><strong>Card Code</strong></i></span>
						<input type="text" name="card_code" class="form-control" required placeholder="Unique Card Code" value="<?php echo set_value('card_code'); ?>" />
					</div>
					<br/>
					<!-- /.form group -->
					<div class="input-group">
						<span class="input-group-addon"><strong>Title</strong></span>
						<input type="text" name="title" class="form-control" required placeholder="Title" value="<?php echo set_value('title'); ?>" />
					</div>
					<br/>
					<!-- /.form group -->
					<div class="input-group">
						
						<textarea name="description" class="form-control" required placeholder="Description"><?php echo set_value('title'); ?>Description will be here</textarea>
					</div>
					<br/>
					<!-- /.form group -->
					<div class="input-group">
						<span class="input-group-addon"><strong>Card Type</strong></span>
						<select name="card_type" class="form-control card_type" required >
							<option value="">Card Type</option>
							<option value="1">Online</option>
							<option value="2">Physical</option>
						</select>
					</div>
					<br/>
					<!-- /.form group -->
					<div class="input-group">
						<span class="input-group-addon"><strong>Price</strong></span>
						<select name="rate_card_pricelist_id" class="form-control" required >
							<option value="">Price</option>
							<?php if($ratecard_list != null){
								foreach($ratecard_list as $card_data){
									echo '<option value="'.$card_data->id.'">Price : '.$card_data->price.' => '.$card_data->rides.' Rides</option>';
								}
							}?>
						</select>
					</div>
					<br/>
					<!-- /.form group -->
					<div class="row online_card">
						<div class="col-md-6">
							
							<div class="input-group">
								<span class="input-group-addon"><strong>Start Date</strong></span>
								<input type="text" id="datemask" name="start_date" class="form-control" required placeholder="Start Date" />
							</div>
							<br/>
							<!-- /.form group -->
						</div>
						<div class="col-md-6">
							
							<div class="input-group">
								<span class="input-group-addon"><strong>End Date</strong></span>
								<input type="text" id="datemask-2" name="end_date" class="form-control" required placeholder="End Date" />
							</div>
							<br/>
							<!-- /.form group -->
						</div>
					</div>
					
					<div class="input-group online_card">
						<span class="input-group-addon"><strong>Usage Limit</strong></span>
						<input type="number" name="user_no" class="form-control" required placeholder="User Number" value="<?php echo set_value('user_no'); ?>" />
					</div>
					<br/>
					<!-- /.form group -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
          </div>
          <!-- /.box -->

          
          

        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
          <!-- /.box -->
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>