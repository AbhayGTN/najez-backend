
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Card
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Cards</li>
        <li class="active">View</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      
      <!-- /.box -->

      <div class="row">
		<div class="col-md-1"></div>
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Card Details</h3>
            </div>
			<?php //print_r($carddata); ?>
				<div class="box-body">
					<div class="input-group">
						<span class="input-group-addon"><strong>Card Code</strong></span>
						<input type="text" name="card_code" class="form-control" disabled required placeholder="Unique Card Code" value="<?php echo $carddata[0]['card_code']; ?>" />
					</div>
					<br/>
					<!-- /.form group -->
					<div class="input-group">
						<span class="input-group-addon"><strong>Title</strong></span>
						<input type="text" name="title" class="form-control" disabled required placeholder="Title" value="<?php echo $carddata[0]['title']; ?>" />
					</div>
					<br/>
					<!-- /.form group -->
					<div class="input-group">
						<textarea name="description" class="form-control" disabled required placeholder="Description"><?php echo $carddata[0]['description']; ?></textarea>
					</div>
					<br/>
					<!-- /.form group -->
					<div class="input-group">
						<span class="input-group-addon"><strong>Card Type</strong></span>
						<select name="card_type" class="form-control card_type" required disabled >
							<option value="" selected ><?php echo $carddata[0]['card_type'] == "0"? "online":"physical";?></option>
							
						</select>
					</div>
					<br/>
					<!-- /.form group -->
					<div class="input-group">
                                            <span class="input-group-addon"><strong>Price</strong></span>
						<select name="rate_card_pricelist_id" disabled class="form-control" required >
							<option value="" selected ><?php echo $carddata[0]['price'];?></option>
							
						</select>
					</div>
					<br/>
					<!-- /.form group -->
					<div class="row online_card">
						<div class="col-md-6">
							
							<div class="input-group">
                                                            <span class="input-group-addon"><strong>Start Date</strong></span>
								<input type="text" name="start_date" class="form-control" disabled value="<?php echo $carddata[0]['start_date']; ?>" required placeholder="Start Date" />
							</div>
							<br/>
							<!-- /.form group -->
						</div>
						<div class="col-md-6">
							
							<div class="input-group">
								<span class="input-group-addon"><strong>End Date</strong></span>
								<input type="text" name="end_date" disabled class="form-control" value="<?php echo $carddata[0]['end_date']; ?>" required placeholder="End Date" />
							</div>
							<br/>
							<!-- /.form group -->
						</div>
					</div>
					
					<div class="input-group online_card">
                                            <span class="input-group-addon"><strong>Usage Limit</strong></span>
						<input type="number" name="user_no" disabled class="form-control" required placeholder="User Number" value="<?php echo $carddata[0]['user_no']; ?>" />
					</div>
					<br/>
					<!-- /.form group -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
                                   <!-- <a href="<?= base_url('voucher/edit_card/'.$this->uri->segment(3)); ?>"><button type="button" class="btn btn-primary">Edit</button></a>-->
				</div>

          </div>
          <!-- /.box -->

          
          

        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>