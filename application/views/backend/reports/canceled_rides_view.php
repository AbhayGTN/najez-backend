<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title;  ?>
          
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Cards</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('promotions/hide'); ?>" />
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Cards list</h3>
            </div>
              
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Sno</th>
		<th>Cancel Time</th>
                <th>Customer Name</th>
                <th>Booking Time</th>
                <th>Option</th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($cardlist)){
					//print_r($cardlist);die();
				$x= count($cardlist);	foreach($cardlist as $data){
						
					?>
					<tr id="row_<?php echo $x; ?>">
					  <td><?php echo $x--; ?></td>
					  <td><?php echo date("d-m-Y", ($data['cancel_time'] / 1000) ); ?></td>
					  <td style="word-break: break-word;"><?php echo $data['fname']; ?></td>
					  <td style="word-break: break-word;"><?php echo date("d-m-Y", ($data['booking_time'] / 1000) ); ?></td>
                                          <td style="word-break: break-word;"><?php echo $data['option_en']; ?></td>
                                          
            
                                          
					  
					</tr>
					<?php }
				}?>
                </tbody>
                <tfoot>
                
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>