<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Airport List</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('settings/hideairport'); ?>" />
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-6">
				<div class="box box-danger">
					<div class="box-header with-border">
					  <h3 class="box-title">Airport Details</h3>
					</div>
					<div style="color:#ff0000;margin: 10px;">
					<?php echo validation_errors(); ?>
					<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
					</div>
					<form method="post" action="<?php echo base_url('settings/addairport'); ?>" enctype="multipart/form-data" >
						<div class="box-body">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-circle-o"></i></span>
								<input type="text" name="airport_name" class="form-control" required placeholder="Airport Name" value="<?php echo set_value('airport_name'); ?>" />
							</div>
							<br/>
							<!-- /.form group -->
							
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Add</button>
						</div>
					</form>
				  </div>
				  <!-- /.box -->
			</div>
			<div class="col-xs-6">
				<div class="box">
            <div class="box-header">
              <h3 class="box-title">All Airport list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th style="width:20%">Sno</th>
                  <th style="width:60%">Name</th>
                  <th style="width:20%">Action</th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($airport_list)){
					//print_r($ratecard_list);die();
					foreach($airport_list as $data){
						
					?>
					<tr id="row_<?php echo $data['id']; ?>">
					  <td><?php echo $data['id']; ?></td>
					  <td style="word-break: break-word;"><?php echo $data['name']; ?></td>
					  <td>
						<a class="btn btn-app delete-alert" data-id="<?php echo $data['id']; ?>" href="#" ><i class="fa fa-trash"></i> Delete</a>
					  </td>
					</tr>
					<?php }
				}?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
			</div>
		</div>
	</section>
</div>