<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Cars List</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('settings/hidecar'); ?>" />
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-6">
				<div class="box box-danger">
					<div class="box-header with-border">
					  <h3 class="box-title">Car Details</h3>
					</div>
					<div style="color:#ff0000;margin: 10px;">
					<?php echo validation_errors(); ?>
					<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
					</div>
					<?php //print_r($car_data); ?>
					<form method="post" action="<?php echo base_url('settings/updatecar/'.$car_data[0]->id); ?>" enctype="multipart/form-data" >
						<div class="box-body">
							<label>Car Brand</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-circle-o"></i></span>
								<input type="text" name="car_brand" class="form-control" required placeholder="Car Brand" value="<?php echo $car_data[0]->car_brand; ?>" />
							</div>
							<br/>
							<!-- /.form group -->
							<label>Car Model</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-circle-o"></i></span>
								<input type="text" name="car_model" class="form-control" required placeholder="Car Model" value="<?php echo $car_data[0]->car_model; ?>" />
							</div>
							<br/>
							<!-- /.form group -->
							<label>Car Type</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-circle-o"></i></span>
								<!--<input type="text" name="car_type" class="form-control" required placeholder="Car Type" value="<?php echo $car_data[0]->car_type; ?>" />-->
								<select name="car_type" class="form-control" required >
									<option value="">Car Type</option>
									<option value="Medium" <?php if($car_data[0]->car_type =='Medium'){ echo 'Selected';}?>>Medium</option>
									<option value="Luxury" <?php if($car_data[0]->car_type =='Luxury'){ echo 'Selected';}?>>Luxury</option>
									<option value="Family" <?php if($car_data[0]->car_type =='Family'){ echo 'Selected';}?>>Family</option>
								</select>
							</div>
							<br/>
							<!-- /.form group -->
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</form>
				  </div>
				  <!-- /.box -->
			</div>
			<div class="col-xs-6">
				<div class="box">
            <div class="box-header">
              <h3 class="box-title">All Cars list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>Sno</th>
                  <th>Brand</th>
                  <th>Model</th>
                  <th>Type</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($car_list)){
					//print_r($ratecard_list);die();
					foreach($car_list as $data){
						
					?>
					<tr id="row_<?php echo $data->id; ?>">
					  <td><?php echo $data->id; ?></td>
					  <td style="word-break: break-word;"><?php echo $data->car_brand; ?></td>
					  <td style="word-break: break-word;"><?php echo $data->car_model; ?></td>
					  <td style="word-break: break-word;"><?php echo $data->car_type; ?></td>
					  <td>
						<a class="btn btn-app" href="<?php echo base_url('settings/edit_car/'.$data->id); ?>"><i class="fa fa-edit"></i> Edit</a>
						<a class="btn btn-app delete-alert" data-id="<?php echo $data->id; ?>" href="#" ><i class="fa fa-trash"></i> Delete</a>
					  </td>
					</tr>
					<?php }
				}?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
			</div>
		</div>
	</section>
</div>