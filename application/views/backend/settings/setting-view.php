
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Settings
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Settings</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      
      <!-- /.box -->

      <div class="row">
		<div class="col-md-1"></div>
        <div class="col-md-12">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Setting Details</h3>
            </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>
			<form method="post" action="<?php echo base_url('settings/modify'); ?>" enctype="multipart/form-data" >
				<div class="box-body">
					<div class="col-xs-6">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-circle-o"></i></span>
							<input type="text" name="card_code" class="form-control" required placeholder="Unique Card Code" value="<?php echo set_value('card_code'); ?>" />
						</div>
						<br/>
						<!-- /.form group -->
					</div>
					<div class="col-xs-6">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-circle-o"></i></span>
							<input type="text" name="card_code" class="form-control" required placeholder="Unique Card Code" value="<?php echo set_value('card_code'); ?>" />
						</div>
						<br/>
						<!-- /.form group -->
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</form>
          </div>
          <!-- /.box -->

          
          

        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
        <!-- /.box -->
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>