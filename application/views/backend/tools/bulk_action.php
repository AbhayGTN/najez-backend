<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Bulk Action
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cards</a></li>
            <li class="active">Add Card</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-6">

                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Upload CSV</h3>
                    </div>
                    <div style="color:#ff0000;margin: 10px;">
                        <?php echo validation_errors(); ?>
                        <?php
                        if ($this->session->flashdata('flashdata')) {
                          echo '<p>' . $this->session->flashdata('flashdata') . '</p>';
                        }
                        ?>
                    </div>
                    
                        <?php echo form_open_multipart('tools/block/');?>
                        <div class="box-body">
                            
                            
                            <div class="input-group">
                                <span class="input-group-addon"><strong>Select CSV File</strong></i></span>
                                <input type="file" name="csv_file" class="form-control" required placeholder="Select Your File" value="<?php echo set_value('start_date'); ?>" />
                            </div>
                            <br/>
                            <div class="input-group">
                                <span class="input-group-addon"><strong>Select Action</strong></i></span>
                                <select name="action" class="form-control">
                                    <option>Block/Unblock BY Mobile</option>
                                                                    
                                </select>
                            </div>
                            <br/>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
            </div>
        </div>
    </section>
</div>
        
        