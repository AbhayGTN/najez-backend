<style>
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>

<input type="hidden" class="delete_url" value="<?php echo base_url('driver/hide'); ?>" />
<input type="hidden" class="redirect_url" value="<?php echo base_url('driver/view_drivers'); ?>" />
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Customer</li>
        <li class="active">Edit</li>
      </ol>
    </section>

<div class="container">
  
  <ul class="nav nav-tabs">
    <li class="active"><a href="#">Customer Details</a></li>
    

    <li><a href="<?= base_url('user/transaction_details/'.$this->uri->segment(3));  ?>">Transaction Details</a></li>
  </ul>
</div>
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      
      <!-- /.box -->

      <div class="row">
          
          <div class="profile">
		<div class="col-md-3">
                    
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
					<img src="<?= base_url($agentdata[0]->photo);  ?>" class="img-responsive" alt="">
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">
						<?= $agentdata[0]->fname; ?> <?= $agentdata[0]->mname; ?> <?= $agentdata[0]->lname; ?>
					</div>
					<!--<div class="profile-usertitle-job">
						<?= $agentdata[0]->lname; ?>
					</div>-->
				</div>
				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<form id="uploadimage" action="<?php echo base_url('user/update_profileimg/'.$agentdata[0]->id); ?>" method="post" enctype="multipart/form-data">
				<div class="profile-userbuttons">
					
						<div class="fileUpload btn btn-primary">
							<span>Change Image</span>
							<input type="file" class="upload" name="profileimg" id="profileimg" />
						</div>
						<input type="hidden" name="old_profileimg" value="<?php echo $agentdata[0]->photo; ?>" />
					
					<!--<button type="button" class="btn btn-danger btn-sm">Block</button>-->
				<!--	<a class="delete-alert" data-id="<?php echo $agentdata[0]->id; ?>" href="#" ><button type="button" class="btn btn-danger btn-sm">Block</button></a> -->
                                        
                                        <?php if($agentdata[0]->webblock == '0'){ ?>
                                        <a href="<?= base_url('user/unblock/'.$agentdata[0]->id); ?>" ><button type="button" class="btn btn-danger btn-sm">Unblock</button></a>
                                        <?php } if($agentdata[0]->webblock == '1'){ ?> 
                                        <a href="<?= base_url('user/block/'.$agentdata[0]->id); ?>" ><button type="button" class="btn btn-success btn-sm">block</button></a>
                                        <?php }   ?>
                                        <p style="color:red"><?php echo $this->session->flashdata('cannot_block_user_msg'); ?></p>   
				</div>
				</form>
                                <div class="profile-usermenu">
					<ul class="nav">
						<li>
                                                   
							<a href="#">
							<i class="glyphicon glyphicon-home"></i>
							Total Ride : <?= $stats['total_rides'][0]['num']; ?></a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-envelope"></i>
							Wallet Balance <?= $stats['wallet_balance'][0]['total_amount']; ?></a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-ok"></i>
							Scheduled Rides <?= $stats['shaduled_ride'][0]['num']; ?></a>
						</li>
						
					</ul>
				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				
				<!-- END MENU -->
			</div>
		</div>
		
	</div>
          
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">User Details</h3>
            </div>
				<div class="box-body">
				<form method="post" action="<?php echo base_url('user/updateuser/'.$agentdata[0]->id); ?>" enctype="multipart/form-data" >
				<div class="box-body">
				  <div class="input-group">
					<span class="input-group-addon"><strong>Email</strong></span>
					<input type="email" name="" class="form-control" required placeholder="Email ID" value="<?php echo $agentdata[0]->emailid; ?>" disabled />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><strong>Username</strong></span>
					<input type="text" name="" class="form-control" required placeholder="Username" value="<?php echo $agentdata[0]->username; ?>" disabled />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  <div class="input-group">
					<span class="input-group-addon"><strong>First Name</strong></span>
					<input type="text" name="first_name" class="form-control" required placeholder="First Name" value="<?php echo $agentdata[0]->fname; ?>" />
				  </div>
				  <br/>
		
				 
				  <div class="input-group">
					<span class="input-group-addon"><strong>Mobile</strong></span>
                                        <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" value="<?php echo $agentdata[0]->mobile; ?>" readonly="true" />
				  </div>
				  <br/>
				  <!-- /.form group -->
				  
				  <div class="input-group">
					<label>
					  <input type="radio" name="gender" class="minimal" value="male" <?php if($agentdata[0]->gender != null){ if($agentdata[0]->gender=='male'){ echo 'checked'; } }else{ echo 'checked';} ?> /> Male
					</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<label>
					  <input type="radio" name="gender" class="minimal" value="female" <?php if($agentdata[0]->gender=='female'){ echo 'checked'; }?> /> Female
					</label>
				  </div>
				  <br/>
				  <!-- /.form group -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Update</button>
                                        <a href="<?= base_url('user/delete_user/'.$this->uri->segment(3)); ?>" class="btn btn-danger">Delete</a>
				</div>
			</form>
				</div>
				

          </div>
          <!-- /.box -->

          
          

        </div>
        <!-- /.col (left) -->
		<div class="col-md-1"></div>
        <div class="col-md-4">
			
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>