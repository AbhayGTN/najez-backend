<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Users</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('user/hide'); ?>" />
    <!-- Main content -->
    <div class="container">
  
  <ul class="nav nav-tabs">
      <li><a href="<?= base_url('user/viewuser/'.$this->uri->segment(3)); ?>">Customer Details</a></li>
    
  
    <li class="active"><a href="#">Transaction Details</a></li>
  </ul>
</div>
    
    
    
    
    
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Users list</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
		  <th>Sno</th>
		  <th>Description</th>
		  <th>Amount</th>
                  <th>Mode of transaction</th>
                  <th>Date Time</th>
                  
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($userlist)){
                	$x=1;	foreach($userlist as $data){ ?>
					<tr id="row_<?php echo $data->id; ?>">
                                            <td><?= $x++;   ?></td>
                                            <td><?= $data['description'];  ?></td>
					  <td><?= $data['amount'];  ?></td>
					  <td><?= $data['transaction_mode'];  ?></td>
                                          <td><?= $data['transaction_datetime'];  ?></td>
					</tr>
					<?php }
				}?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Sno</th>
                  <th>User ID</th>
				  <th>Photo</th>
                  <th>Username</th>
                  <th>Email ID</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>