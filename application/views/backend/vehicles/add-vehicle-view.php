
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Vehicle
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Vehicles</a></li>
        <li class="active">Add Vehicle</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Vehicle Details</h3>
            </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>
			<form method="post" action="<?php echo base_url('vehicles/addvehicle'); ?>" enctype="multipart/form-data" >
				<div class="box-body">
					<div>
					<!--<div class="field_val" style="color:#ff0000; text-align: center;padding-bottom: 10px;display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row" style="margin-top:15px;">
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
                                                                    <select class="form-control" name="driver" id="driver" required>
									<option value="">Select Driver</option>
									<?php foreach($driverlist as $driver) {
										echo '<option value="'.$driver->id.'">'.$driver->fname.' '.$driver->lname.'</option>';
									}?>
									</select>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<input type="text" class="form-control" name="vehicle_sno" id="vehicle_sno" placeholder="Serial number of the vehicle" />
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12" style="margin-top:-24px;">
								<label style="font-size: 12px;">Vehicle Plate Number (xyz1234)</label>
								<div class="row">
								
									<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
										<div class="form-group">
											<input type="text" class="form-control text-uppercase" name="vehicle_plate_char" id="vehicle_plate_char" placeholder="xyz" maxlength="3" />
										</div>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-12 col-sx-12">
										
											<div class="form-group">
												<input type="text" class="form-control" name="vehicle_plate_num" id="vehicle_plate_num" placeholder="1234" maxlength="4" />
											</div>
									
									</div>
								</div>
							</div>
							
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="vehicle_type" id="vehicle_type">
										<option value="">Type of the vehicle</option>
										<!--<option value="Delivery">Delivery</option>-->
										<?php foreach($brand_list as $brand_list):     ?>
										<optgroup label="<?=$brand_list['car_brand']; ?>">
										<?php $ss = $this->model->get_car_list($brand_list['car_brand']); ?>
										<?php foreach($ss as $ss):?>
										<option value="<?=  $ss['id']; ?>"><?=$ss['car_model'];  ?></option>							
										<?php endforeach; ?>
										</optgroup>
										<?php endforeach;    ?>
										</select>
										
								</div>
							</div>
						
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="car_model1">
									<input type="text" class="form-control" placeholder="Car Model" id="car_model" />
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="vehicle_year" id="vehicle_year">
									<option value="">Year of manufacture</option>
									<?php for($starting_year = 2010; $starting_year <= date('Y'); $starting_year++) {
										echo '<option value="'.$starting_year.'">'.$starting_year.'</option>';
									}?>
									</select>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<!--<input type="text" class="form-control" name="vehicle_color" id="vehicle_color" placeholder="Color" />-->
									<select class="form-control" name="vehicle_color">
										<option value="">Vehicle Color</option>
										<option value="Red">Red</option>
										<option value="Blue">Blue</option>
										<option value="Green">Green</option>
										<option value="Yellow">Yellow</option>
										<option value="Black">Black</option>
										<option value="White">White</option>
										<!-- fixed by nandini-->
										<option value="White">Silver</option>
									</select>  
                                                                        
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<input type="text" class="form-control" name="iban" id="iban" placeholder="IBAN Number" />
								</div>
									
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="iban_bank" id="iban_bank" >
										<option value="">Bank</option>
						<option value="Alinma Bank">Alinma Bank</option>
<option value="Al Rajhi Bank">Al Rajhi Bank</option>
<option value="Arab National Bank">Arab National Bank</option>
<option value="Asia Bank">Asia Bank</option>
<option value="Banque Saudi Fransi">Banque Saudi Fransi</option>
<option value="Bank Al-Bilad">Bank Al-Bilad</option>
<option value="Bank AlJazira">Bank AlJazira</option>
<option value="BNP Paribas">BNP Paribas</option>
<option value="Deutsche Bank">Deutsche Bank</option>
<option value="Emirates NBD">Emirates NBD</option>
<option value="Gulf International Bank (GIB)">Gulf International Bank (GIB)</option>
<option value="Industrial and Commercial Bank of China (ICBC)">Industrial and Commercial Bank of China (ICBC)</option>
<option value="J.P. Morgan Chase N.A">J.P. Morgan Chase N.A</option>
<option value="Muscat Bank">Muscat Bank</option>
<option value="National Bank of Bahrain (NBB)">National Bank of Bahrain (NBB)</option>
<option value="National Bank of Kuwait (NBK)">National Bank of Kuwait (NBK)</option>
<option value="National Bank of Pakistan (NBP)">National Bank of Pakistan (NBP)</option>
<option value="Qatar National Bank (QNB)">Qatar National Bank (QNB)</option>
<option value="Riyad Bank">Riyad Bank</option>
<option value="Saudi Investment Bank">Saudi Investment Bank</option>
<option value="Samba Financial Group (Samba)">Samba Financial Group (Samba)</option>
<option value="Saudi Hollandi Bank (Alawwal)">Saudi Hollandi Bank (Alawwal)</option>
<option value="State Bank of India (SBI)">State Bank of India (SBI)</option>
<option value="The National Commercial Bank">The National Commercial Bank</option>
<option value="The Saudi British Bank">The Saudi British Bank</option>
<option value="T.C.ZIRAAT BANKASI A.S.">T.C.ZIRAAT BANKASI A.S.</option>
				
									</select>
								</div>
									
							</div>
							
						</div>
						<div class="row">
						<hr/>
							<!--<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Photograph</label>
									<input type="file" class="form-control photo" name="photograph" id="photograph" placeholder="Photograph" required/>
								</div>
							</div>-->
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>National ID / Iqama</label>
									<input type="file" class="form-control photo" name="residence" id="residence" placeholder="National ID / Iqama" required/>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Driving License</label>
									<input type="file" class="form-control photo" name="driver_license" id="driver_license" placeholder="Driving License" required/>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Vehicle registration</label>
									<input type="file" class="form-control photo" name="vehicle_reg" id="vehicle_reg" placeholder="Vehicle registration" required/>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Insurance of vehicles</label>
									<input type="file" class="form-control photo" name="vehicle_insur" id="vehicle_insur" placeholder="Insurance of vehicles" required/>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Authorization image</label>
									<input type="file" class="form-control photo" name="auth_img" id="auth_img" placeholder="Authorization image"/>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Tafweeth</label>
                                                                        <input type="file" class="form-control photo" name="tafweeth_img" id="tafweeth_img" placeholder="Tafweeth image"/>
								</div>
							</div>
							
						</div>
						
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" id="addvehicle">Add Vehicle</button>
				</div>
			</form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>