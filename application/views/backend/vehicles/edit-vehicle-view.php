
<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Vehicle
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Vehicles</a></li>
        <li class="active">Edit Vehicle</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Vehicle Details</h3>
            </div>
			<div style="color:#ff0000;margin: 10px;">
			<?php echo validation_errors(); ?>
			<?php if($this->session->flashdata('flashdata')){ echo '<p>'.$this->session->flashdata('flashdata').'</p>'; }?>
			</div>
			<form method="post" action="<?php echo base_url('vehicles/updatevehicle/'.$vehicledata[0]->id); ?>" enctype="multipart/form-data" >
				<div class="box-body">
					<div>
					<!--<div class="field_val" style="color:#ff0000; text-align: center;padding-bottom: 10px;display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row" style="margin-top:15px;">
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="driver" id="driver">
									<option value="">Select Driver</option>
									<?php foreach($driverlist as $driver) {
										echo '<option value="'.$driver->id.'" ';
										if($vehicledata[0]->driver_id == $driver->id){ echo 'selected';}
										echo '>'.$driver->fname.' '.$driver->lname.'</option>';
									}?>
									</select>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<input type="text" class="form-control" name="vehicle_sno" id="vehicle_sno" placeholder="Serial number of the vehicle" value="<?php echo $vehicledata[0]->vehicle_sno; ?>" />
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12" style="margin-top:-24px;">
								<label style="font-size: 12px;">Vehicle Plate Number (xyz1234)</label>
								<div class="row">
								<?php $platno = explode('-',$vehicledata[0]->vehicle_number); ?>
									<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
										<div class="form-group">
											<input type="text" class="form-control" name="vehicle_plate_char" id="vehicle_plate_char" placeholder="xyz" maxlength="3" value="<?php echo $platno[0]; ?>" />
										</div>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-12 col-sx-12">
										
											<div class="form-group">
												<input type="text" class="form-control" name="vehicle_plate_num" id="vehicle_plate_num" placeholder="1234" maxlength="4" value="<?php echo $platno[1]; ?>" />
											</div>
									
									</div>
								</div>
							</div>
							
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="vehicle_type" id="vehicle_type">
										<option value="">Type of the vehicle</option>
										<!--<option value="Delivery">Delivery</option>-->
										<?php foreach($brand_list as $brand_list):     ?>
										<optgroup label="<?=$brand_list['car_brand']; ?>">
										<?php $ss = $this->model->get_car_list($brand_list['car_brand']); ?>
										<?php foreach($ss as $ss):?>
										<option value="<?=  $ss['id']; ?>" <?php if($vehicledata[0]->vehicle_model == $ss['id']){ echo 'selected';}?>><?=$ss['car_model'];  ?></option>							
										<?php endforeach; ?>
										</optgroup>
										<?php endforeach;    ?>
										</select>
										
								</div>
							</div>
						
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="car_model1">
									<input type="text" class="form-control" name="car_model" value="<?php echo $vehicledata[0]->vehicle_type; ?>" placeholder="Car Model" disabled id="car_model" />
									<input type="hidden" name="car_model" value="<?php echo $vehicledata[0]->vehicle_type; ?>" />
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="vehicle_year" id="vehicle_year">
									<option value="">Year of manufacture</option>
									<?php for($starting_year = 2013; $starting_year <= date('Y'); $starting_year++) {
										echo '<option value="'.$starting_year.'" ';
										if($vehicledata[0]->vehicle_manufacture_year == $starting_year){ echo 'selected';}
										echo '>'.$starting_year.'</option>';
									}?>
									</select>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<!--<input type="text" class="form-control" name="vehicle_color" id="vehicle_color" placeholder="Color"  value="<?php echo $vehicledata[0]->vehicle_color; ?>" />-->
									<select class="form-control" name="vehicle_color">
										<option value="">Vehicle Color</option>
										<option value="Red" <?php if($vehicledata[0]->vehicle_color == 'Red'){echo 'selected';}?>>Red</option>
										<option value="Blue" <?php if($vehicledata[0]->vehicle_color == 'Blue'){echo 'selected';}?>>Blue</option>
										<option value="Green" <?php if($vehicledata[0]->vehicle_color == 'Green'){echo 'selected';}?>>Green</option>
										<option value="Yellow" <?php if($vehicledata[0]->vehicle_color == 'Yellow'){echo 'selected';}?>>Yellow</option>
										<option value="Black" <?php if($vehicledata[0]->vehicle_color == 'Black'){echo 'selected';}?>>Black</option>
										<option value="White" <?php if($vehicledata[0]->vehicle_color == 'White'){echo 'selected';}?>>White</option>
										<option value="Silver" <?php if($vehicledata[0]->vehicle_color == 'Silver'){echo 'selected';}?>>Silver</option>
									</select>
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<input type="text" class="form-control" name="iban" id="iban" placeholder="IBAN Number"  value="<?php echo $vehicledata[0]->vehicle_color; ?>" />
								</div>
									
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="iban_bank" id="iban_bank" >
										<option value="">Bank</option>
										<option value="The National Commercial Bank" <?php if($vehicledata[0]->iban_bank=='The National Commercial Bank'){ echo 'selected';}?>>The National Commercial Bank</option>
										<option value="The Saudi British Bank" <?php if($vehicledata[0]->iban_bank=='The Saudi British Bank'){ echo 'selected';}?>>The Saudi British Bank</option>
										<option value="Saudi Investment Bank" <?php if($vehicledata[0]->iban_bank=='Saudi Investment Bank'){ echo 'selected';}?>>Saudi Investment Bank</option>
										<option value="Alinma Bank" <?php if($vehicledata[0]->iban_bank=='Alinma Bank'){ echo 'selected';}?>>Alinma Bank</option>
										<option value="Banque Saudi Fransi" <?php if($vehicledata[0]->iban_bank=='Banque Saudi Fransi'){ echo 'selected';}?>>Banque Saudi Fransi</option>
										<option value="Riyad Bank" <?php if($vehicledata[0]->iban_bank=='Riyad Bank'){ echo 'selected';}?>>Riyad Bank</option>
										<option value="Samba Financial Group (Samba)" <?php if($vehicledata[0]->iban_bank=='Samba Financial Group (Samba)'){ echo 'selected';}?>>Samba Financial Group (Samba)</option>
										<option value="Saudi Hollandi Bank (Alawwal)" <?php if($vehicledata[0]->iban_bank=='Saudi Hollandi Bank (Alawwal)'){ echo 'selected';}?>>Saudi Hollandi Bank (Alawwal)</option>
										<option value="Al Rajhi Bank" <?php if($vehicledata[0]->iban_bank=='Al Rajhi Bank'){ echo 'selected';}?>>Al Rajhi Bank</option>
										<option value="Arab National Bank" <?php if($vehicledata[0]->iban_bank=='Arab National Bank'){ echo 'selected';}?>>Arab National Bank</option>
										<option value="Bank Al-Bilad" <?php if($vehicledata[0]->iban_bank=='Bank Al-Bilad'){ echo 'selected';}?>>Bank Al-Bilad</option>
										<option value="Bank AlJazira" <?php if($vehicledata[0]->iban_bank=='Bank AlJazira'){ echo 'selected';}?>>Bank AlJazira</option>
										<option value="Asia Bank" <?php if($vehicledata[0]->iban_bank=='Asia Bank'){ echo 'selected';}?>>Asia Bank</option>
										<option value="Qatar National Bank (QNB)" <?php if($vehicledata[0]->iban_bank=='Qatar National Bank (QNB)'){ echo 'selected';}?>>Qatar National Bank (QNB)</option>
										<option value="State Bank of India (SBI)" <?php if($vehicledata[0]->iban_bank=='State Bank of India (SBI)'){ echo 'selected';}?>>State Bank of India (SBI)</option>
										<option value="Gulf International Bank (GIB)" <?php if($vehicledata[0]->iban_bank=='Gulf International Bank (GIB)'){ echo 'selected';}?>>Gulf International Bank (GIB)</option>
										<option value="Emirates NBD" <?php if($vehicledata[0]->iban_bank=='Emirates NBD'){ echo 'selected';}?>>Emirates NBD</option>
										<option value="National Bank of Bahrain (NBB)" <?php if($vehicledata[0]->iban_bank=='National Bank of Bahrain (NBB)'){ echo 'selected';}?>>National Bank of Bahrain (NBB)</option>
										<option value="National Bank of Kuwait (NBK)" <?php if($vehicledata[0]->iban_bank=='National Bank of Kuwait (NBK)'){ echo 'selected';}?>>National Bank of Kuwait (NBK)</option>
										<option value="Muscat Bank" <?php if($vehicledata[0]->iban_bank=='Muscat Bank'){ echo 'selected';}?>>Muscat Bank</option>
										<option value="Deutsche Bank" <?php if($vehicledata[0]->iban_bank=='Deutsche Bank'){ echo 'selected';}?>>Deutsche Bank</option>
										<option value="BNP Paribas" <?php if($vehicledata[0]->iban_bank=='BNP Paribas'){ echo 'selected';}?>>BNP Paribas</option>
										<option value="J.P. Morgan Chase N.A" <?php if($vehicledata[0]->iban_bank=='J.P. Morgan Chase N.A'){ echo 'selected';}?>>J.P. Morgan Chase N.A</option>
										<option value="National Bank of Pakistan (NBP)" <?php if($vehicledata[0]->iban_bank=='National Bank of Pakistan (NBP)'){ echo 'selected';}?>>National Bank of Pakistan (NBP)</option>
										<option value="T.C.ZIRAAT BANKASI A.S." <?php if($vehicledata[0]->iban_bank=='T.C.ZIRAAT BANKASI A.S.'){ echo 'selected';}?>>T.C.ZIRAAT BANKASI A.S.</option>
										<option value="Industrial and Commercial Bank of China (ICBC)" <?php if($vehicledata[0]->iban_bank=='Industrial and Commercial Bank of China (ICBC)'){ echo 'selected';}?>>Industrial and Commercial Bank of China (ICBC)</option>
									</select>
								</div>
									
							</div>
							
						</div>
						<div class="row">
						<hr/>
						<?php //echo '<pre>'; print_r($vehicle_document); echo '</pre>'; ?>
							<?php if($vehicle_document[0]->doc_key =='photograph' && $vehicle_document[0]->doc_val !=''){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Photograph</label>
									<input type="file" name="photograph" class="photo" />
									<p><a target="_blank" href="<?php echo base_url($vehicle_document[0]->doc_val); ?>">View Document</a></p>
								</div>
							</div>
							<?php } ?>
							<?php if($vehicle_document[1]->doc_key =='residence-identity' && $vehicle_document[1]->doc_val !=''){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>National ID / Iqama</label>
									<input type="file" name="residence-identity" class="photo" />
									<p><a target="_blank" href="<?php echo base_url($vehicle_document[1]->doc_val); ?>">View Document</a></p>
								</div>
							</div>
							<?php } ?>
							<?php if($vehicle_document[2]->doc_key =='driver-license' && $vehicle_document[2]->doc_val !=''){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Driving License</label>
									<input type="file" name="driver-license" class="photo" />
									<p><a target="_blank" href="<?php echo base_url($vehicle_document[2]->doc_val); ?>">View Document</a></p>
								</div>
							</div>
							<?php } ?>
							<?php if($vehicle_document[3]->doc_key =='vehicle-reg' && $vehicle_document[3]->doc_val !=''){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Vehicle registration</label>
									<input type="file" name="vehicle-reg" class="photo" />
									<p><a target="_blank" href="<?php echo base_url($vehicle_document[3]->doc_val); ?>">View Document</a></p>
								</div>
							</div>
							<?php } ?>
							<?php if($vehicle_document[4]->doc_key =='vehicle-insur' && $vehicle_document[4]->doc_val !=''){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Insurance of vehicles</label>
									<input type="file" name="vehicle-insur" class="photo" />
									<p><a target="_blank" href="<?php echo base_url($vehicle_document[4]->doc_val); ?>">View Document</a></p>
								</div>
							</div>
							<?php } ?>
							<?php if($vehicle_document[5]->doc_key =='auth-img' && $vehicle_document[5]->doc_val !=''){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Authorization image</label>
									<input type="file" name="auth-img" class="photo" />
									<p><a target="_blank" href="<?php echo base_url($vehicle_document[5]->doc_val); ?>">View Document</a></p>
								</div>
							</div>
							<?php } ?>
							<?php if($vehicle_document[6]->doc_key =='tafweeth-img' && $vehicle_document[6]->doc_val !=''){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Tafweeth</label>
									<input type="file" name="tafweeth-img" class="photo" />
									<p><a target="_blank" href="<?php echo base_url($vehicle_document[6]->doc_val); ?>">View Document</a></p>
								</div>
							</div>
							<?php } ?>
						</div>
						
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary" id="addvehicle">Update Vehicle</button>
				</div>
			</form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>