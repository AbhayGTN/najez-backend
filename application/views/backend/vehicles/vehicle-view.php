<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $head_title; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Vehicles</li>
        <li class="active"><?php echo $head_title; ?></li>
      </ol>
    </section>
	<input type="hidden" class="delete_url" value="<?php echo base_url('vehicles/hide'); ?>" />
    <!-- Main content -->
	
<div class="container-fluid">
  
  <ul class="nav nav-tabs">
    <li><a href="<?php echo base_url('driver/viewdriver/'.$driverid); ?>">Driver Details</a></li>
    <li class="active"><a href="#">Vehicle Details</a></li>
    <li><a href="<?= base_url('driver/ride_details/'.$driverid);?>">Rides Details</a></li>
    <li><a href="<?= base_url('driver/transaction_details/'.$driverid);?>">Transaction Details</a></li>
  
  
  </ul>
</div>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">All Vehicles list</h3>
			  <p class="pull-right"><a href="<?php echo base_url('vehicles/add_vehicle'); ?>"><button type="button" class="btn btn-primary btn-sm">Add New Vehicle</button></a></p>
                          <p style="color:red"><?= $this->session->flashdata('cannot_block_user_msg');  ?></p>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>Vehicle ID</th>
				  <th>Plate Number</th>
				  <th>Vehicle Model</th>
				  <th>Vehicle Type</th>
                  <th style="width:220px;">Action</th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($vehiclelist)){
          //echo "<pre>"; print_r($vehiclelist);
					foreach($vehiclelist as $data){ ?>
					<tr id="row_<?php echo $data->id; ?>">
					  <td><?php echo $data->id; ?></td>
					  <td><span style="text-transform:uppercase;"><?php echo $data->vehicle_number; ?></span></td>
					   <td style="word-break: break-word;"><?php echo $data->vehicle_model; ?></td>
					  <td style="word-break: break-word;"><?php echo $data->vehicle_type; ?></td>
					  <td>
						<a class="btn btn-app" href="<?php echo base_url('vehicles/view_vehicle/'.$data->id); ?>"><i class="fa fa-eye"></i> View</a>
						<a class="btn btn-app" href="<?php echo base_url('vehicles/edit_vehicle/'.$data->id); ?>"><i class="fa fa-edit"></i> Edit</a>
						<a class="btn btn-app delete-alert" data-id="<?php echo $data->id; ?>" href="#" ><i class="fa fa-trash"></i> Delete</a>
                                                <?php if($data->active == '0'):   ?>
                                                <a class="btn btn-app" href="<?= base_url('vehicles/unblock/'.$this->uri->segment(3)).'/'.$data->id; ?>" ><i class="fa fa-trash"></i> Activate</a>
                                                <?php endif;   ?>
                                                <?php if($data->active == '1'):   ?>
                                                <a class="btn btn-app" href="<?= base_url('vehicles/block/'.$this->uri->segment(3)).'/'.$data->id; ?>" ><i class="fa fa-trash"></i>Deactivate</a>
                                                <?php endif;   ?>
					  </td>
					</tr>
					<?php }
				}?>
                </tbody>
                <tfoot>
                <tr>
                 <th>Vehicle ID</th>
				 <th>Plate Number</th>
				   <th>Vehicle Model</th>
				  <th>Vehicle Type</th>
                  <th>Status</th>
                  <!--<th>Action</th>-->
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>