<div class="content-wrapper" style="min-height: 946px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Voucher
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Cards</a></li>
            <li class="active">Add Card</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-6">

                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Voucher Details</h3>
                    </div>
                    <div style="color:#ff0000;margin: 10px;">
                        <?php echo validation_errors(); ?>
                        <?php
                        if ($this->session->flashdata('flashdata')) {
                          echo '<p>' . $this->session->flashdata('flashdata') . '</p>';
                        }
                        ?>
                    </div>
                    <form method="post" action="<?php echo base_url('voucher/addcard'); ?>" enctype="multipart/form-data" >
                        <div class="box-body">
                            <div class="input-group">
                                <span class="input-group-addon"><strong>Card Code</strong></i></span>
                                <input type="text" name="card_code" class="form-control" required placeholder="Unique Card Code" value="<?php echo set_value('card_code'); ?>" />
                            </div>
                            <br/>

                            <div class="input-group">
                                <span class="input-group-addon"><strong>Price</strong></i></span>
                                <input type="text" name="price" class="form-control" required placeholder="Price" value="<?php echo set_value('price'); ?>" />
                            </div>
                            <br/>
                            <div class="input-group">
                                <span class="input-group-addon"><strong>Start Date</strong></i></span>
                                <input type="text" id ="vouchdate1" name="start_date" class="form-control" required placeholder="Start Date" value="<?php echo set_value('start_date'); ?>" />
                            </div>
                            <br/>
                            <div class="input-group">
                                <span class="input-group-addon"><strong>End Date</strong></i></span>
                                <input type="text" id="vouchdate2" name="end_date" class="form-control" required placeholder="End Date" value="<?php echo set_value('end_date'); ?>" />
                            </div>
                            <br/>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
            </div>
        </div>
    </section>
</div>
        
        