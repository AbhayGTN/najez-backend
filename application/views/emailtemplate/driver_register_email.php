<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<style>
ul li{list-style-type:none !important;}
@media only screen and (max-width:480px) {
table[class="MainContainer"], td[class="cell"] {
	width: 100% !important;
	height: auto !important;
}
td[class="specbundle"] {
	width: 100% !important;
	float: left !important;
	font-size: 13px !important;
	line-height: 17px !important;
	display: block !important;
	padding-bottom: 15px !important;
}
td[class="specbundle_img"] {
	width: 100% !important;
	float: left !important;
	display: block !important;
	font-size: 13px !important;
	line-height: 17px !important;
}
td[class="specbundle2"] {
	width: 90% !important;
	float: left !important;
	font-size: 13px !important;
	line-height: 17px !important;
	display: block !important;
	padding-bottom: 10px !important;
	padding-left: 5% !important;
	padding-right: 5% !important;
	text-align: center !important;
}
td[class="specbundle3"] {
	width: 70% !important;
	float: left !important;
	display: block !important;
	padding-bottom: 10px !important;
	padding-left: 15% !important;
	padding-right: 15% !important;
}
td[class="spechide"] {
	display: none !important;
}
img[class="banner"] {
	width: 100% !important;
	height: auto !important;
}
img[class="banner1"] {
	width: 95% !important;
	height: auto !important;
}
td[class="left_pad"] {
	padding-left: 15px !important;
	padding-right: 15px !important;
}
}
 @media only screen and (max-width:540px) {
table[class="MainContainer"], td[class="cell"] {
	width: 100% !important;
	height: auto !important;
}
td[class="specbundle"] {
	width: 100% !important;
	float: left !important;
	font-size: 13px !important;
	line-height: 17px !important;
	display: block !important;
	padding-bottom: 15px !important;
}
td[class="specbundle2"] {
	width: 90% !important;
	float: left !important;
	font-size: 13px !important;
	line-height: 17px !important;
	display: block !important;
	padding-bottom: 10px !important;
	padding-left: 5% !important;
	padding-right: 5% !important;
	text-align: center !important;
}
td[class="specbundle_img"] {
	width: 100% !important;
	float: left !important;
	display: block !important;
	font-size: 13px !important;
	line-height: 17px !important;
}
td[class="specbundle3"] {
	width: 70% !important;
	float: left !important;
	display: block !important;
	padding-bottom: 10px !important;
	padding-left: 15% !important;
	padding-right: 15% !important;
}
td[class="spechide"] {
	display: none !important;
}
img[class="banner"] {
	width: 100% !important;
	height: auto !important;
}
img[class="banner1"] {
	width: 95% !important;
	height: auto !important;
}
td[class="left_pad"] {
	padding-left: 15px !important;
	padding-right: 15px !important;
}
}
</style>
<body paddingwidth="0" paddingheight="0"  style="padding-top: 0 !important;
	padding-bottom: 0 !important;
	padding-top: 0 !important;
	padding-bottom: 0 !important;
	margin: 0 !important;
	width: 100% !important;
	-webkit-text-size-adjust: 100% !important;
	-ms-text-size-adjust: 100% !important;
	-webkit-font-smoothing: antialiased !important; padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center" style='font-family:Helvetica, Arial,serif; padding:25px 0;'>
  <tbody>
    <tr>
      <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="MainContainer" bgcolor="#333333">
          <tbody>
            <tr> 
              
              <!-- =============================== Header ====================================== --> 
              
              <!-- =============================== Body ====================================== -->
              <td class='movableContentContainer'><div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                    <tbody>
                      <tr>
                        <td height="15"></td>
                      </tr>
                      <tr>
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>
                                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                      <tr>
                                        <td valign="top" align="right"><div class="contentEditableContainer contentImageEditable">
                                            <div class="contentEditable" > <img style="border: 0 !important; display: block !important; outline: none !important;" src="<?php echo base_url('assets/home/img'); ?>/mail_logo.png" alt='Najez' border="0"> </div>
                                          </div></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                              </tr>
                            </tbody>
                          </table></td>
                      </tr>
                      <tr>
                        <td height="15"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <table cellspacing="0" cellpadding="0" border="0" align="center" style="border-collapse:collapse;padding:0;margin:0 auto;width:600px" class="m_1563584831278401067m_7831808304245819767container-table">
                  <tbody>
                    <tr>
                      <td valign="top" style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0px;margin:0;background:#fff" class="m_1563584831278401067m_7831808304245819767top-content"><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;padding:0;margin:0;width:100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0"><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;padding:0;margin:0">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0;background:#FFf;text-align:left;" class="m_1563584831278401067m_7831808304245819767email-heading"><h1 style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10 0; font-size:25px; text-align:center; color:rgba(196,89,17,1); padding:5px 10px; margin-top:15px"><strong><?php echo $email_temp[1]['meta_value_en'];?></strong></h1>
                                          <p style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10 0; font-size:15px; text-align:justify; padding:5px 10px; color:rgba(0,0,0,1)"><?php echo $email_temp[2]['meta_value_en'];?></p><br/><p><strong>Your Referral Code is :</strong><?php echo $referralcode;   ?></p></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr class="m_1563584831278401067m_7831808304245819767order-information">
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0"><table width="650" cellspacing="0" cellpadding="0" border="0" style="border:none;border-collapse:collapse;padding:0;margin:0;width:100%">
                                  <tbody>
                                    <tr style="background:#313131;" class="m_1563584831278401067m_7831808304245819767subtotal"> </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr class="m_1563584831278401067m_7831808304245819767order-information"> </tr>
                            <tr> </tr>
                            <tr>
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0;background:#FFf;text-align:left;" class="m_1563584831278401067m_7831808304245819767email-heading"><p style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10px 0 0 0; font-size:15px; text-align:justify; padding:5px 10px 0 10px; color:rgba(0,0,0,1)"><small>Terms and conditions:</small></p>
                                <ul style="margin-top:0;">
                                 <?php echo $email_temp[3]['meta_value_en'];?>
                                </ul></td>
                            </tr>
                            <tr class="m_1563584831278401067m_7831808304245819767order-information">
                              <td style="padding:10px;"></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table>
                
                <table cellspacing="0" cellpadding="0" border="0" align="center" style="border-collapse:collapse;padding:0;margin:0 auto;width:600px" class="m_1563584831278401067m_7831808304245819767container-table">
                  <tbody>
                    <tr>
                      <td valign="top" style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0px;margin:0;background:#fff" class="m_1563584831278401067m_7831808304245819767top-content"><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;padding:0;margin:0;width:100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0"><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;padding:0;margin:0">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0;background:#FFf;text-align:left;" class="m_1563584831278401067m_7831808304245819767email-heading"><h1 dir="rtl" style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10 0; font-size:25px; text-align:center; color:rgba(196,89,17,1); padding:5px 10px; margin-top:15px"><strong><?php echo $email_temp[1]['meta_value_ar'];?></strong></h1>
                                        <p dir="rtl" style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10 0; font-size:17px; text-align:center; padding:5px 10px; color:rgba(0,0,0,1)"><?php echo $email_temp[2]['meta_value_ar'];?></p></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr class="m_1563584831278401067m_7831808304245819767order-information">
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0"><table width="650" cellspacing="0" cellpadding="0" border="0" style="border:none;border-collapse:collapse;padding:0;margin:0;width:100%">
                                  <tbody>
                                    <tr style="background:#313131;" class="m_1563584831278401067m_7831808304245819767subtotal"> </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <tr class="m_1563584831278401067m_7831808304245819767order-information"> </tr>
                            <tr> </tr>
                            <tr>
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;margin:0;background:#FFf;text-align:left;" class="m_1563584831278401067m_7831808304245819767email-heading"><p dir="rtl" style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10px 0 0 0; font-size:15px; text-align:right; padding:5px 10px 0 10px; color:rgba(0,0,0,1)"><small>*  تنطبق الشروط و الاحكام.</small></p>
                                  <ul style="margin-top:0;list-style: none;" dir="ltr">
                                  <?php echo $email_temp[3]['meta_value_ar'];?>
                                </ul></td>
                            </tr>
                            <tr class="m_1563584831278401067m_7831808304245819767order-information">
                              <td style="padding:10px;"></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table>
                
                <!-- =============================== footer ====================================== --></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
</body>
</html>