<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
@media only screen and (max-width:480px) {
table[class="MainContainer"], td[class="cell"] {
	width: 100% !important;
	height: auto !important;
}
td[class="specbundle"] {
	width: 100% !important;
	float: left !important;
	font-size: 13px !important;
	line-height: 17px !important;
	display: block !important;
	padding-bottom: 15px !important;
}
td[class="specbundle_img"] {
	width: 100% !important;
	float: left !important;
	display: block !important;
	font-size: 13px !important;
	line-height: 17px !important;
}
td[class="specbundle2"] {
	width: 90% !important;
	float: left !important;
	font-size: 13px !important;
	line-height: 17px !important;
	display: block !important;
	padding-bottom: 10px !important;
	padding-left: 5% !important;
	padding-right: 5% !important;
	text-align: center !important;
}
td[class="specbundle3"] {
	width: 70% !important;
	float: left !important;
	display: block !important;
	padding-bottom: 10px !important;
	padding-left: 15% !important;
	padding-right: 15% !important;
}
td[class="spechide"] {
	display: none !important;
}
img[class="banner"] {
	width: 100% !important;
	height: auto !important;
}
img[class="banner1"] {
	width: 95% !important;
	height: auto !important;
}
td[class="left_pad"] {
	padding-left: 15px !important;
	padding-right: 15px !important;
}
}
 @media only screen and (max-width:540px) {
table[class="MainContainer"], td[class="cell"] {
	width: 100% !important;
	height: auto !important;
}
td[class="specbundle"] {
	width: 100% !important;
	float: left !important;
	font-size: 13px !important;
	line-height: 17px !important;
	display: block !important;
	padding-bottom: 15px !important;
}
td[class="specbundle2"] {
	width: 90% !important;
	float: left !important;
	font-size: 13px !important;
	line-height: 17px !important;
	display: block !important;
	padding-bottom: 10px !important;
	padding-left: 5% !important;
	padding-right: 5% !important;
	text-align: center !important;
}
td[class="specbundle_img"] {
	width: 100% !important;
	float: left !important;
	display: block !important;
	font-size: 13px !important;
	line-height: 17px !important;
}
td[class="specbundle3"] {
	width: 70% !important;
	float: left !important;
	display: block !important;
	padding-bottom: 10px !important;
	padding-left: 15% !important;
	padding-right: 15% !important;
}
td[class="spechide"] {
	display: none !important;
}
img[class="banner"] {
	width: 100% !important;
	height: auto !important;
}
img[class="banner1"] {
	width: 95% !important;
	height: auto !important;
}
td[class="left_pad"] {
	padding-left: 15px !important;
	padding-right: 15px !important;
}
}
</style>
</head>
<body paddingwidth="0" paddingheight="0" style="padding-top: 0 !important;
	padding-bottom: 0 !important;
	padding-top: 0 !important;
	padding-bottom: 0 !important;
	margin: 0 !important;
	width: 100% !important;
	-webkit-text-size-adjust: 100% !important;
	-ms-text-size-adjust: 100% !important;
	-webkit-font-smoothing: antialiased !important; padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<table class="tableContent" style="font-family:Helvetica, Arial,serif; background: #313131; padding:25px 0;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
  <tbody>
    <tr>
      <td><table class="MainContainer" width="600" cellspacing="0" cellpadding="0" border="0" bgcolor="#333333" align="center">
          <tbody>
            <tr> 
              
              <!-- =============================== Header ====================================== --> 
              
              <!-- =============================== Body ====================================== -->
              <td class="movableContentContainer"><div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                  <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff">
                    <tbody>
                      <tr>
                        <td height="15"></td>
                      </tr>
                      <tr>
                        <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                              <tr>
                                <td width="15" valign="top">&nbsp;</td>
                                <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                      <tr>
                                        <td valign="top" align="center"><div class="contentEditableContainer contentImageEditable">
                                            <div class="contentEditable"> <img style="border: 0 !important; display: block !important; outline: none !important;" src="<?php echo base_url('assets/home/'); ?>/img/emaillogo/logo.png" alt=""border="0"> </div>
                                          </div></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                                <td width="15" valign="top">&nbsp;</td>
                              </tr>
                            </tbody>
                          </table></td>
                      </tr>
                      <tr>
                        <td height="15"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <table style="border-collapse:collapse;padding:0;margin:0 auto;width:600px" class="m_1563584831278401067m_7831808304245819767container-table" cellspacing="0" cellpadding="0" border="0" align="center">
                  <tbody>
                    <tr>
                      <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0px;margin:0;background:#fff" class="m_1563584831278401067m_7831808304245819767top-content" valign="top"><table style="border-collapse:collapse;padding:0;margin:0;width:100%" cellspacing="0" cellpadding="0" border="0">
                          <tbody>
                            <tr>
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0"><table style="border-collapse:collapse;padding:0;margin:0" cellspacing="0" cellpadding="0" border="0">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0 15px;margin:0;background:#ff9e15;text-align:center;" class="m_1563584831278401067m_7831808304245819767email-heading"><h1 style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10 0; font-size:25px; text-align:center; color:rgba(255,255,255,1); padding:5px 10px; margin-top:15px">مرحبا" بك</h1>
                                        <p style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10 0; font-size:15px; text-align:center; color:rgba(255,255,255,1); padding:5px 10px;">يسرنا انضمامك الينا كشريك في ناجز, ويسعدنا اخبارك انه سوف تصلك قريبا" رسالة نصية او بريد الكتروني عند انطلاقنا في السوق لتحميل التطبيق و جني الكثير الارباح.</p></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table>
                
                <!-- =============================== footer ====================================== --></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
</body>
</html>
