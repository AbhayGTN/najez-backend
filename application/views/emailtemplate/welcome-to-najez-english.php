<!DOCTYPE html>
  <html>
     <title>Welcome to Najez</title>
  </html>
  <head>
<style>
@media only screen and (max-width:480px) {
table[class="MainContainer"], td[class="cell"] {
  width: 100% !important;
  height: auto !important;
}
td[class="specbundle"] {
  width: 100% !important;
  float: left !important;
  font-size: 13px !important;
  line-height: 17px !important;
  display: block !important;
  padding-bottom: 15px !important;
}
td[class="specbundle_img"] {
  width: 100% !important;
  float: left !important;
  display: block !important;
  font-size: 13px !important;
  line-height: 17px !important;
}
td[class="specbundle2"] {
  width: 90% !important;
  float: left !important;
  font-size: 13px !important;
  line-height: 17px !important;
  display: block !important;
  padding-bottom: 10px !important;
  padding-left: 5% !important;
  padding-right: 5% !important;
  text-align: center !important;
}
td[class="specbundle3"] {
  width: 70% !important;
  float: left !important;
  display: block !important;
  padding-bottom: 10px !important;
  padding-left: 15% !important;
  padding-right: 15% !important;
}
td[class="spechide"] {
  display: none !important;
}
img[class="banner"] {
  width: 100% !important;
  height: auto !important;
}
img[class="banner1"] {
  width: 95% !important;
  height: auto !important;
}
td[class="left_pad"] {
  padding-left: 15px !important;
  padding-right: 15px !important;
}
}
 @media only screen and (max-width:540px) {
table[class="MainContainer"], td[class="cell"] {
  width: 100% !important;
  height: auto !important;
}
td[class="specbundle"] {
  width: 100% !important;
  float: left !important;
  font-size: 13px !important;
  line-height: 17px !important;
  display: block !important;
  padding-bottom: 15px !important;
}
td[class="specbundle2"] {
  width: 90% !important;
  float: left !important;
  font-size: 13px !important;
  line-height: 17px !important;
  display: block !important;
  padding-bottom: 10px !important;
  padding-left: 5% !important;
  padding-right: 5% !important;
  text-align: center !important;
}
td[class="specbundle_img"] {
  width: 100% !important;
  float: left !important;
  display: block !important;
  font-size: 13px !important;
  line-height: 17px !important;
}
td[class="specbundle3"] {
  width: 70% !important;
  float: left !important;
  display: block !important;
  padding-bottom: 10px !important;
  padding-left: 15% !important;
  padding-right: 15% !important;
}
td[class="spechide"] {
  display: none !important;
}
img[class="banner"] {
  width: 100% !important;
  height: auto !important;
}
img[class="banner1"] {
  width: 95% !important;
  height: auto !important;
}
td[class="left_pad"] {
  padding-left: 15px !important;
  padding-right: 15px !important;
}
}
</style>
</head>
<body paddingwidth="0" paddingheight="0"  style="padding-top: 0 !important;
	padding-bottom: 0 !important;
	padding-top: 0 !important;
	padding-bottom: 0 !important;
	margin: 0 !important;
	width: 100% !important;
	-webkit-text-size-adjust: 100% !important;
	-ms-text-size-adjust: 100% !important;
	-webkit-font-smoothing: antialiased !important; padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center" style='font-family:Helvetica, Arial,serif; background: #313131; padding:25px 0;'>
  <tbody>
    <tr>
      <td><table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class="MainContainer" bgcolor="#333333">
          <tbody>
            <tr> 
              
              <!-- =============================== Header ====================================== --> 
              
              <!-- =============================== Body ====================================== -->
              <td class='movableContentContainer'><div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                    <tbody>
                      <tr>
                        <td height="15"></td>
                      </tr>
                      <tr>
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <tr>
                                <td valign="top" width="15">&nbsp;</td>
                                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                      <tr>
                                        <td valign="top" align="center"><div class="contentEditableContainer contentImageEditable">
                                            <div class="contentEditable" > <img style="border: 0 !important; display: block !important; outline: none !important;" src="<?php echo base_url('assets/home/'); ?>/img/emaillogo/logo.png" alt=""border="0"> </div>
                                          </div></td>
                                      </tr>
                                    </tbody>
                                  </table></td>
                                <td valign="top" width="15">&nbsp;</td>
                              </tr>
                            </tbody>
                          </table></td>
                      </tr>
                      <tr>
                        <td height="15"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <table cellspacing="0" cellpadding="0" border="0" align="center" style="border-collapse:collapse;padding:0;margin:0 auto;width:600px" class="m_1563584831278401067m_7831808304245819767container-table">
                  <tbody>
                    <tr>
                      <td valign="top" style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0px;margin:0;background:#fff" class="m_1563584831278401067m_7831808304245819767top-content"><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;padding:0;margin:0;width:100%">
                          <tbody>
                            <tr>
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0"><table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;padding:0;margin:0">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0 15px;margin:0;background:#ff9e15;text-align:center;" class="m_1563584831278401067m_7831808304245819767email-heading"><h1 style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10 0; font-size:25px; text-align:center; color:rgba(255,255,255,1); padding:5px 10px; margin-top:15px">Welcome to Najez</h1>
                                        <p style="font-family: Helvetica,Arial,serif;font-weight:normal;line-height:24px;margin:10 0; font-size:15px; text-align:center; color:rgba(255,255,255,1); padding:5px 10px;">Thank you for registration as partner of Najez, and please be informed that you will receive an E-mail or SMS soon to download the application and earn more money.</p></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                            <!-- <tr class="m_1563584831278401067m_7831808304245819767order-information">
                              <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:0;margin:0"><table width="650" cellspacing="0" cellpadding="0" border="0" style="border:1px solid #eaeaea;border-collapse:collapse;padding:0;margin:0;width:100%">
                                  <tbody>
                                    <tr style="background:#e1f0f8;" class="m_1563584831278401067m_7831808304245819767subtotal">
                                      <td style="font-family: Helvetica,Arial,serif;font-weight:normal;border-collapse:collapse;vertical-align:top;padding:5px 15px;margin:0;text-align:left; vertical-align:middle; border-right:1px dashed #c3ced4" class="m_1563584831278401067m_7831808304245819767order-details"><h3 style="font-family: Helvetica,Arial,serif;font-weight:normal;font-size:14px;margin-bottom:10px;margin-top:15px">Use the following values when prompted to log in:</h3>
                                        <ul style="padding-left:10px;">
                                                <li style="font-size:13px; list-style:none; line-height:20px; margin-bottom:5px;"><img style="margin-right:10px; margin-top:2px; float:left" src="tick.png" />Email: mahesh.sharma@techconlabs.com</li>
                                                
                                                <li style="font-size:13px; list-style:none; line-height:20px; margin-bottom:5px;"><img style="margin-right:10px; margin-top:2px; float:left" src="tick.png" />Password: Mahesh@123</li>
                                            </ul></td>
                                      
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr> -->
                            
                            <!-- <tr class="m_1563584831278401067m_7831808304245819767order-information">
                              <td style="padding:15px; background:rgba(255,255,255,1);"><p style="font-family: Helvetica,Arial,serif; font-weight:normal; font-size:13px">Click here to confirm your email and instantly log in (the link is valid only once):</p></td>
                            </tr>
                            <tr>
<td style="font-family:Verdana,Arial;font-weight:normal;border-collapse:collapse;vertical-align:middle;color:#fff;width:100%;display:block;border:0 none;text-align:center;text-transform:uppercase;white-space:nowrap">
                                    <a data-saferedirecturl="" target="_blank" style="background-color:#F59B3A;width:200px; margin:0 auto;line-height:40px;font-size:15px;display:inline-block;text-decoration:none; margin:0 15px;" href=""><span style="color:#fff">Confirm Account</span></a>
                                </td>
                            </tr>
                            <tr class="m_1563584831278401067m_7831808304245819767order-information">
                              <td style="padding:15px; background:rgba(255,255,255,1);"><p style="font-family: Helvetica,Arial,serif; font-weight:normal; font-size:13px">
                If you have any questions, please feel free to contact us at
                <a href="mailto:no-reply@proteink.com" style="color:#3696c2" target="_blank">no-reply@proteink.com</a><br>
                 or by phone at <a href="tel:1%20614-500-0540" style="color:#3696c2" target="_blank">1 614-500-0540</a>.
            </p></td>
                            </tr> -->
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table>
                
                
                
                <!-- =============================== footer ====================================== --></td>
            </tr>
            <!-- <tr><td style="text-align:center;color:rgba(255,255,255,1); font-weight:400; font-family: Helvetica,Arial,serif;"><h2 style="margin:20px 0 0 0;">Thank you, Proteink!</h2></td></tr> -->
            
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
</body>
