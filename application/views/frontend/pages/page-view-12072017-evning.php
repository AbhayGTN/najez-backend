﻿

       


    <!--second-->
    <section id="service" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="color:#000;">
				<?php 
				if($this->session->userdata['site_lang'] =='arabic'){
				    echo $pagedata[0]['description_ar'];
				}else{
				    echo $pagedata[0]['description_en'];
				}
				?>
				
			</div>
		</div>
      </div>
    </section>
	
    
	<section id="feature1" class="section-padding wow fadeIn delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">GET IN TOUCH</h2>
          </div>
		</div>
		<div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/phone.png">
              </div>
              <p>Phone : +966540988577</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/mssg.png">
              </div>
              <p>Email : <a href="mailto:info@najez-online.com" style="color:#333;">info@najez-online.com</a></p>
            </div>
            
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="display:none;">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/web.png">
              </div>
              <p>Web : www.yoursite.com</p>
            </div>
          </div>
        </div>
		<div class="row" style="display:none;">
			<div class="col-lg-12 text-center">
				<div class="wrap-item text-center">
				  <div class="item-img">
					 <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/fb.jpg" style="margin-top: 50px;"></a>
					  <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter.jpg" style="margin-top: 50px;"></a>
					  <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/google.jpg" style="margin-top: 50px;"></a>
				  </div>
				</div>
			</div>
		</div>
      </div>
    </section>   

    <footer id="footer">
      <div class="container">
        <div class="row text-center" style="padding-top:40px;">
          <p>&copy; 2017 Najez. All right reserved. | <a href="<?php echo base_url('page/privacy-policy');?>">Privacy Policy</a></p>
          
        </div>
      </div>
    </footer>
    <!---->
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/wow.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/custom.js"></script>
    <!--<script src="contactform/contactform.js"></script>-->
    <script src="<?php echo base_url('assets/home'); ?>/js/owl.carousel.min.js"></script>
    

<script type="text/javascript">
$(document).ready(function(){
        var itaImgLink = "https://techconlabs.net/najez/assets/home/img/saudi-arabia.jpg";
		var deuImgLink = "https://techconlabs.net/najez/assets/home/img/urdu.jpg";
    	var engImgLink = "https://techconlabs.net/najez/assets/home/img/Grossbritanien.jpg";
		//var fraImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Frankreich.gif";

		var imgBtnSel = $('.imgBtnSel');
		var imgBtnIta = $('.imgBtnIta');
		var imgBtnEng = $('.imgBtnEng');
		var imgBtnDeu = $('.imgBtnDeu');
		//var imgBtnFra = $('#imgBtnFra');

		var imgNavSel = $('.imgNavSel');
		var imgNavIta = $('.imgNavIta');
		var imgNavEng = $('.imgNavEng');
		var imgNavDeu = $('.imgNavDeu');
		//var imgNavFra = $('#imgNavFra');

		var spanNavSel = $('.lanNavSel');
		var spanBtnSel = $('.lanBtnSel');

		imgBtnSel.attr("src",itaImgLink);
		imgBtnIta.attr("src",itaImgLink);
		imgBtnEng.attr("src",engImgLink);
		imgBtnDeu.attr("src",deuImgLink);
		//imgBtnFra.attr("src",fraImgLink);

		imgNavSel.attr("src",engImgLink);
		imgNavIta.attr("src",itaImgLink);
		imgNavEng.attr("src",engImgLink);
		imgNavDeu.attr("src",deuImgLink);
		//imgNavFra.attr("src",fraImgLink);

		$( ".language" ).on( "click", function( event ) {
			var currentId = $(this).attr('id');

			if(currentId == "navIta") {
				imgNavSel.attr("src",itaImgLink);
				spanNavSel.text("AR");
			} else if (currentId == "navEng") {
				imgNavSel.attr("src",engImgLink);
				spanNavSel.text("ENG");
			} else if (currentId == "navDeu") {
				imgNavSel.attr("src",deuImgLink);
				spanNavSel.text("UR");
			} else if (currentId == "navFra") {
				imgNavSel.attr("src",fraImgLink);
				spanNavSel.text("FRA");
			}

			if(currentId == "btnIta") {
				imgBtnSel.attr("src",itaImgLink);
				spanBtnSel.text("AR");
			} else if (currentId == "btnEng") {
				imgBtnSel.attr("src",engImgLink);
				spanBtnSel.text("ENG");
			} else if (currentId == "btnDeu") {
				imgBtnSel.attr("src",deuImgLink);
				spanBtnSel.text("UR");
			} else if (currentId == "btnFra") {
				imgBtnSel.attr("src",fraImgLink);
				spanBtnSel.text("FRA");
			}
			
		});
});
</script>
</body>
</html>