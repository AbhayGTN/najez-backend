﻿<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $page_title; ?></title>
    <meta name="description" content="Najez">
    <meta name="keywords" content="Najez">
    <link rel="icon" href="<?php echo base_url('assets/home/'); ?>/img/favicon.ico" >
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300|Raleway:300,400,900,700italic,700,300,600">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/style.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/home/'); ?>/css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/home/'); ?>/css/owl.theme.default.min.css">
</head>
  <body class="<?php echo $body_class; ?> single-page">
	<div class="loader"></div>
    <div id="myDiv">
    <!--HEADER-->
     <div class="header">
      <div class="bg-color">
        <header id="main-header">
        
			<div class="row anti">
				<div class="col-lg-3 col-sm-3 col-xs-6 anti">
					<a href="<?php echo base_url(); ?>"><img src="https://techconlabs.net/najez/assets/home//img/logo.png" class="img-responsive" alt="logo" style="position: relative;z-index: 9999;" /></a>
				</div>
				<div class="col-lg-9 col-sm-9">
					<nav class="navbar navbar-default navbar-fixed-top"> <!-- navbar-fixed-top -->
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
					 
					  <ul class="nav navbar-nav navbar-right">
						<li class=""><a href="<?php echo base_url(); ?>">HOME</a></li>
						<li class=""><a href="<?php echo base_url(); ?>#about-us">ABOUT US</a></li>
						<li class=""><a href="<?php echo base_url(); ?>#services">Services</a></li>
						<li class=""><a href="<?php echo base_url(); ?>#download-app">DOWNLOAD APP</a></li>
						<li><a  class="main-active" href="<?php echo base_url(); ?>#join-us">Join Us</a></li>
						<li class="dropdown countryDropdown hidden-xs">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a id="navIta" href="#" class="language"> <img id="" src="" alt="..." class="imgNavIta img-thumbnail icon-small">  <span class="lanNavIta">Arabic</span></a></li>
								<!--<li><a id="navDeu" href="#" class="language"> <img id="" src="" alt="..." class="imgNavDeu img-thumbnail icon-small">  <span class="lanNavDeu">Urdu</span></a></li>-->	
								<!--<li><a id="navFra" href="#" class="language"><img id="imgNavFra" src="" alt="..." class="img-thumbnail icon-small">  <span id="lanNavFra">Francais</span></a></li>-->
								<li><a id="navEng" href="#" class="language"><img id="" src="" alt="..." class="imgNavEng img-thumbnail icon-small">  <span class="lanNavEng">English</span></a></li>
							</ul>
						</li>
					  </ul>
					
					</div>
					
					</nav>
				</div>
				
				<div class="col-lg-2 col-sm-2 visible-xs">
					<div id="navbar">
						<ul>
							<li class="dropdown countryDropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a id="navIta" href="#" class="language"> <img id="" src="" alt="..." class="imgNavIta img-thumbnail icon-small">  <span class="lanNavIta">Arabic</span></a></li>
									<!--<li><a id="navDeu" href="#" class="language"> <img id="" src="" alt="..." class="imgNavDeu img-thumbnail icon-small">  <span class="lanNavDeu">Urdu</span></a></li>-->
									<!--<li><a id="navFra" href="#" class="language"><img id="imgNavFra" src="" alt="..." class="img-thumbnail icon-small">  <span id="lanNavFra">Francais</span></a></li>-->
									<li><a id="navEng" href="#" class="language"><img id="" src="" alt="..." class="imgNavEng img-thumbnail icon-small">  <span class="lanNavEng">English</span></a></li>
								</ul>
							</li>
						</ul>
					</div> <!--/.navbar-collapse -->
				</div>
			</div>
        
        </header>
        <div class="wrapper">
			<div class="container">
				<div class="row">
					<div class="banner-info text-center wow fadeIn delay-05s col-lg-12">
						<h2 class="bnr-sub-title"><?php echo $page_title; ?></h2>
						<!--<p class="bnr-para">Anywhere Anytime! Just book online</p>-->
					</div>
				</div>
				<!--
				<div class="row">
					<div class="col-lg-6 col-md-6 col-xs-12 text-center"><a href="#contact"><button class="button1 head-btn">JOIN AS A PARTNER</button></a></div>
					<div class="col-lg-6 col-md-6 col-xs-12 text-center"><a href="#contact"><button class="button2 head-btn">JOIN AS A USER</button></a></div>
					<div class="overlay-detail">
					</div>
				</div>
			  -->
			</div>
        </div>
      </div>
    </div>
    <!--/ HEADER-->

       


    <!--second-->
    <section id="service" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="color:#000;">
				<?php echo $pagedata[0]['description_en'];?>
				<!--<h3>USER PRIVACY STATEMENT</h3>
				<p>At Najez we understand the meaning of personal information, so we are always committed to protect and respect your privacy.</p>
				<p>When you install the app of Najez and start using it, it collects your information through different interactions and communications.</p>
				<h3>Scope</h3>
				<p>This privacy statement applies to anyone who uses our website or mobile apps, the persons, Services to request transportation, delivery, or other on-demand services (“Users”). This Statement does not apply to information we collect from or about drivers, couriers, partner transportation companies, or any other persons who use the Najez platform under license (collectively “Drivers”). If you interact with the Services as both a User and a Driver, the respective privacy statements apply to your different interactions.</p>
				<h3>Information which is provided by you</h3>
				<p>We collect information directly from you, such as when you create or modify your account, request on-demand services, contact customer support, or otherwise communicate with us. This information may include: name, email, phone number, postal address, profile picture, payment method, or any other source you use.</p>
				<h3>Information We Collect Through Your Use of Our Services</h3>
				<p>When you use our Services, we collect information about you in the following general categories:</p>
				<ul style="list-style-type: disc;">
					<li><b>Your location information:</b> When you use our services, we collect precise location data about your trip from the Najez app used by the driver and you. If you permit the Najez app to access location of your device when the app is running in the foreground or background. We may also derive your approximate location from your IP address.</li>
					<li><b>Contacts Information:</b> If you permit the Najez app to access the address book on your device through the permission system used by your mobile platform, we may access the stored names and numbers for social interactions through our services, which are defined in other services and are declared consent.</li>
					<li><b>Transaction Information:</b> We collect transaction detail related to your use of our services requested, data and time the service was provided amount charged, distance travelled, and other related details.</li>
					<li><b>Usage and Preference Information:</b> We collect information about how you and site interact with our services, preferred expressed, and setting chosen.</li>
					<li><b>Device Information:</b> We collect information about your mobile device, including, the hardware model, operating system and version, file names, preferred language, unique device identifier, advertising identifiers, serial numbers, mobile network information.</li>
					<li><b>Call and SMS Data:</b> Our services facilitate communications between users and drivers. In connection with facilitating this service, we receive call data, including the date and time of the call or SMS message, the parties’ phone numbers, and the content of the SMS message.</li>
					<li><b>Log Information:</b> When you interact with our services, we collect server logs, which may include information like device IP address, access dates and times, app features or pages viewed, app crashes and other system activity, type of browser, and the third-party site or service you were using before interacting with our services.</li>
					
				</ul>
				<h3>Information We Collect From Other Sources</h3>
				<p>We may also receive information from other sources and combine that with information we collect through our Services. For example:</p>
				<ul style="list-style-type: disc;">
					<li>If you choose to link, create, or log in to your Najez account with a payment provider (e.g., Google Wallet) or social media service (e.g., Facebook), or if you engage with a separate app or website that uses our API (or whose API we use), we may receive information about you or your connections from that site or app.</li>
					<li>When you request on demand services, our Drivers may provide us with a User rating after providing services to you.</li>
					<li>If you also interact with our Services in another capacity, for instance as a Driver or user of other apps we provide, we may combine or associate that information with information we have collected from you in your capacity as a User or rider.</li>
				</ul>
				<h3>Use of Information</h3>
				<p>We may use the information we collect about you to:</p>
				<ul style="list-style-type: disc;">
					<li>Provide, maintain, and improve our Services, including, for example, to facilitate payments, send receipts, provide products and services you request (and send related information), develop new features, provide customer support to Users and Drivers, develop safety features, authenticate users, and send product updates and administrative messages;</li>
					<li>Perform internal operations, including, for example, to prevent fraud and abuse of our Services; to troubleshoot software bugs and operational problems; to conduct data analysis, testing, and research; and to monitor and analyze usage and activity trends;</li>
					<li>Send or facilitate communications (i) between you and a Driver, such as estimated times of arrival (ETAs), or (ii) between you and a contact of yours at your direction in connection with your use of certain features, such as referrals, invites, split fare requests, or ETA sharing;</li>
					<li>Send you communications we think will be of interest to you, including information about products, services, promotions, news, and events of Najez and other companies, where permissible and according to local applicable laws; and to process contest, sweepstake, or other promotion entries and fulfil any related awards;</li>
					<li>Personalize and improve the Services, including to provide or recommend features, content, social connections, referrals, and advertisements.</li>
					
				</ul>
				<h3>Sharing of Information</h3>
				<p>We may share the information we collect about you as described in this Statement or as described at the time of collection or sharing, including as follows:</p>
				<p>Through Our Services</p>
				<p>We may share your information:</p>
				<ul>
					<li>With Drivers to enable them to provide the Services you request. For example, we share your name, photo (if you provide one), average User rating given by Drivers, and pickup and/or drop-off locations with Drivers;</li>
					<li>With the general public if you submit content in a public forum, such as blog comments, social media posts, or other features of our Services that are viewable by the general public;</li>
				</ul>
				<h3>Other Important Sharing</h3>
				<p>We may share your information:</p>
				<ul style="list-style-type: disc;">
					<li>With Najez subsidiaries and affiliated entities that provide services or conduct data processing on our behalf, or for data centralization and / or logistics purposes;</li>
					<li>In response to a request for information by a competent authority if we believe disclosure is in accordance with, or is otherwise required by, any applicable law, regulation, or legal process;</li>
					<li>With law enforcement officials, government authorities, or other third parties if we believe your actions are inconsistent with our User agreements, Terms of Service, or policies, or to protect the rights, property, or safety of Najez or others;</li>
					<li>In connection with, or during negotiations of, any merger, sale of company assets, consolidation or restructuring, financing, or acquisition of all or a portion of our business by or into another company;</li>
					<li>If we otherwise notify you and you consent to the sharing; and</li>
				</ul>
				<h3>Social Sharing Features</h3>
				<p>The Services may integrate with social sharing features and other related tools which let you share actions you take on our Services with other apps, sites, or media, and vice versa. Your use of such features enables the sharing of information with your friends or the public, depending on the settings you establish with the social sharing service. Please refer to the privacy policies of those social sharing services for more information about how they handle the data you provide to or share through them.</p>
				<h3>Your Choices</h3>
				<h4>Account Information</h4>
				<p>You may correct your account information at any time by logging into your online or in-app account. If you wish to delete your account, please log into your Najez account at najez-online.com or in the app, and the follow the directions or “Delete my Najez Account” under the “Account and Payment” option. Please note that in some cases we may retain certain information about you as required by law, or for legitimate business purposes to the extent permitted by law. For instance, if you have a standing credit or debt on your account, or if we believe you have committed fraud or violated our Terms, we may seek to resolve the issue before deleting your information.</p>
				<h4>Access Rights</h4>
				<p>Najez will comply with individual’s requests regarding access, correction, and/or deletion of the personal data it stores in accordance with applicable law.</p>
				<h4>Contact Information</h4>
				<p>We may also seek permission for our app’s collection and syncing of contact information from your device per the permission system used by your mobile operating system. If you initially permit the collection of this information, iOS users can later disable it by changing the contacts settings on your mobile device. The Android platform does not provide such a setting.</p>
				<h4>Promotional Communications</h4>
				<p>You may opt out of receiving promotional messages from us by following the instructions in those messages. If you opt out, we may still send you non-promotional communications, such as those about your account, about Services you have requested, or our ongoing business relations.</p>
				<h3>Changes to the Statement</h3>
				<p>We may change this Statement from time to time. If we make significant changes in the way we treat your personal information, or to the Statement, we will provide you notice through the Services or by some other means, such as email. Your continued use of the Services after such notice constitutes your consent to the changes. We encourage you to periodically review the Statement for the latest information on our privacy practices.</p> -->
			</div>
		</div>
      </div>
    </section>
	
    
	<section id="feature1" class="section-padding wow fadeIn delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">GET IN TOUCH</h2>
          </div>
		</div>
		<div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/phone.png">
              </div>
              <p>Phone : +966540988577</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/mssg.png">
              </div>
              <p>Email : <a href="mailto:info@najez-online.com" style="color:#333;">info@najez-online.com</a></p>
            </div>
            
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="display:none;">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/web.png">
              </div>
              <p>Web : www.yoursite.com</p>
            </div>
          </div>
        </div>
		<div class="row" style="display:none;">
			<div class="col-lg-12 text-center">
				<div class="wrap-item text-center">
				  <div class="item-img">
					 <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/fb.jpg" style="margin-top: 50px;"></a>
					  <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter.jpg" style="margin-top: 50px;"></a>
					  <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/google.jpg" style="margin-top: 50px;"></a>
				  </div>
				</div>
			</div>
		</div>
      </div>
    </section>   

    <footer id="footer">
      <div class="container">
        <div class="row text-center" style="padding-top:40px;">
          <p>&copy; 2017 Najez. All right reserved. | <a href="<?php echo base_url('page/privacy-policy');?>">Privacy Policy</a></p>
          
        </div>
      </div>
    </footer>
    <!---->
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/wow.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/custom.js"></script>
    <!--<script src="contactform/contactform.js"></script>-->
    <script src="<?php echo base_url('assets/home'); ?>/js/owl.carousel.min.js"></script>
    

<script type="text/javascript">
$(document).ready(function(){
        var itaImgLink = "https://techconlabs.net/najez/assets/home/img/saudi-arabia.jpg";
		var deuImgLink = "https://techconlabs.net/najez/assets/home/img/urdu.jpg";
    	var engImgLink = "https://techconlabs.net/najez/assets/home/img/Grossbritanien.jpg";
		//var fraImgLink = "http://www.roemheld.de/IT/Data/Images/Address/Frankreich.gif";

		var imgBtnSel = $('.imgBtnSel');
		var imgBtnIta = $('.imgBtnIta');
		var imgBtnEng = $('.imgBtnEng');
		var imgBtnDeu = $('.imgBtnDeu');
		//var imgBtnFra = $('#imgBtnFra');

		var imgNavSel = $('.imgNavSel');
		var imgNavIta = $('.imgNavIta');
		var imgNavEng = $('.imgNavEng');
		var imgNavDeu = $('.imgNavDeu');
		//var imgNavFra = $('#imgNavFra');

		var spanNavSel = $('.lanNavSel');
		var spanBtnSel = $('.lanBtnSel');

		imgBtnSel.attr("src",itaImgLink);
		imgBtnIta.attr("src",itaImgLink);
		imgBtnEng.attr("src",engImgLink);
		imgBtnDeu.attr("src",deuImgLink);
		//imgBtnFra.attr("src",fraImgLink);

		imgNavSel.attr("src",engImgLink);
		imgNavIta.attr("src",itaImgLink);
		imgNavEng.attr("src",engImgLink);
		imgNavDeu.attr("src",deuImgLink);
		//imgNavFra.attr("src",fraImgLink);

		$( ".language" ).on( "click", function( event ) {
			var currentId = $(this).attr('id');

			if(currentId == "navIta") {
				imgNavSel.attr("src",itaImgLink);
				spanNavSel.text("AR");
			} else if (currentId == "navEng") {
				imgNavSel.attr("src",engImgLink);
				spanNavSel.text("ENG");
			} else if (currentId == "navDeu") {
				imgNavSel.attr("src",deuImgLink);
				spanNavSel.text("UR");
			} else if (currentId == "navFra") {
				imgNavSel.attr("src",fraImgLink);
				spanNavSel.text("FRA");
			}

			if(currentId == "btnIta") {
				imgBtnSel.attr("src",itaImgLink);
				spanBtnSel.text("AR");
			} else if (currentId == "btnEng") {
				imgBtnSel.attr("src",engImgLink);
				spanBtnSel.text("ENG");
			} else if (currentId == "btnDeu") {
				imgBtnSel.attr("src",deuImgLink);
				spanBtnSel.text("UR");
			} else if (currentId == "btnFra") {
				imgBtnSel.attr("src",fraImgLink);
				spanBtnSel.text("FRA");
			}
			
		});
});
</script>
</body>
</html>