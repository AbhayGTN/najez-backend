﻿   


    <!--second-->
    <section id="service" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="color:#000;">
				<?php 
				if(isset($this->session->userdata['site_lang']) && $this->session->userdata['site_lang'] =='arabic'){
				    echo $pagedata[0]['description_ar'];
				}else{
				    echo $pagedata[0]['description_en'];
				}
				?>
				
			</div>
		</div>
      </div>
    </section>
	
    
	<section id="feature1" class="section-padding wow fadeIn delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15"><?php echo $this->lang->line('get_in_touch');?></h2>
          </div>
		</div>
		<div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/phone.png">
              </div>
              <?php if(empty($this->session->userdata['site_lang'])){ ?> 
		 	<p dir=ltr lang=ar><?php echo $this->lang->line('phone');?> :<span dir=ltr lang= en-us><?php echo $this->lang->line('+966540988577');?></span></p>
		<?php }elseif($this->session->userdata['site_lang'] =='english'){?> 
			<p dir=ltr lang=ar><?php echo $this->lang->line('phone');?> :<span dir=ltr lang= en-us><?php echo $this->lang->line('+966540988577');?></span></p>	 
		<?php }elseif($this->session->userdata['site_lang'] =='arabic'){ ?> 
			<p dir=rtl lang=ar><?php echo $this->lang->line('phone');?> :<span dir=ltr lang= en-us><?php echo $this->lang->line('+966540988577');?></span></p>	 
		<?php } ?>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/mssg.png">
              </div>
              <p dir=rtl lang=ar><?php echo $this->lang->line('email');?> : <a href="mailto:info@najez-online.com" style="color:#333;"><?php echo $this->lang->line('infonajwz-onlinecom');?>info@najez-online.com</a></p>
            </div>
            
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="display:none;">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/web.png">
              </div>
              <p><?php echo $this->lang->line('web:wwwyoursitecom');?></p>
            </div>
          </div>
        </div>
		<div class="row">
			<div class="col-lg-12 text-center">
				<div class="wrap-item text-center">
				  <div class="item-img">
				  
				  
				   
            		<?php if(empty($this->session->userdata['site_lang'])){ ?> 
            		 	<a href="https://twitter.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter.png" style="margin-top: 50px;"></a>
            		<?php }elseif($this->session->userdata['site_lang'] =='english'){?> 
            			<a href="https://twitter.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter.png" style="margin-top: 50px;"></a>	 
            		<?php }elseif($this->session->userdata['site_lang'] =='arabic'){ ?> 
            			<a href="https://twitter.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter-arabic.png" style="margin-top: 50px;"></a>	 
            		<?php } ?>
            		
            		
            		<?php if(empty($this->session->userdata['site_lang'])){ ?> 
            		 	<a href="https://instagram.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/instagram.png" style="margin-top: 50px;"></a>
            		<?php }elseif($this->session->userdata['site_lang'] =='english'){?> 
            			<a href="https://instagram.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/instagram.png" style="margin-top: 50px;"></a>	 
            		<?php }elseif($this->session->userdata['site_lang'] =='arabic'){ ?> 
            			<a href="https://instagram.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/insta2-arabic.png" style="margin-top: 50px;"></a>	 
            		<?php } ?>
				  
				  </div>
				</div>
			</div>
		</div>
      </div>
    </section>   

    <footer id="footer">
      <div class="container">
        <div class="row text-center" style="padding-top:40px;">
          <p> <?php echo $this->lang->line('all_right_reserved');?> | <a href="<?php echo base_url('page/privacy-policy');?>"><?php echo $this->lang->line('privacy_policy');?></a></p>
          
        </div>
      </div>
    </footer>
    <!---->
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/wow.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/custom.js"></script>
    <!--<script src="contactform/contactform.js"></script>-->
    <script src="<?php echo base_url('assets/home'); ?>/js/owl.carousel.min.js"></script>
    

<script type="text/javascript">

</script>
</body>
</html>