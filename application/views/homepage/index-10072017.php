﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Najez</title>
    <meta name="description" content="Najez">
    <meta name="keywords" content="Najez">
    <link rel="icon" href="<?php echo base_url('assets/home/'); ?>/img/favicon.ico" >
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300|Raleway:300,400,900,700italic,700,300,600">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/style.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/home/'); ?>/css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/home/'); ?>/css/owl.theme.default.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/home'); ?>/css/bootstrap-datepicker.css">
	 
    <!-- =======================================================
        Theme Name: Baker
        Theme URL: https://bootstrapmade.com/baker-free-onepage-bootstrap-theme/
        Author: BootstrapMade.com
        Author URL: https://bootstrapmade.com
    ======================================================= -->
  </head>
  <body>
  <?php //if($this->session->flashdata('flashdata')){ ?> 
<p class="msgpop" style="display:none;text-align: center;background: #ff9e15;padding: 20px;color: #fff; margin: 0;z-index: 99999;position: fixed;width:100%;">Thank you for submitting your details, We will keep you posted.</p>
<?php //}?>
    <div class="loader"></div>
    <div id="myDiv">
    <!--HEADER-->
    <div class="header">
      <div class="bg-color">
        <header id="main-header">
        
			<div class="row anti">
				<div class="col-lg-3 col-sm-3 col-xs-6 anti">
					<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/home/'); ?>/img/logo.png" class="img-responsive" alt="logo" style="position: relative;z-index: 9999;" /></a>
				</div>
				<div class="col-lg-7 col-sm-7">
				<nav class="navbar navbar-default navbar-fixed-top"> <!-- navbar-fixed-top -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
				 
				  <ul class="nav navbar-nav navbar-right">
					<li class=""><a href="#">HOME</a></li>
					<li class=""><a href="#about-us">ABOUT US</a></li>
					<li class=""><a href="#services">Services</a></li>
					<li class=""><a href="#download-app">DOWNLOAD APP</a></li>
					<li><a  class="main-active" href="#join-us">Join Us</a></li>
					<li class="dropdown countryDropdown hidden-xs">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a id="navIta" href="#" class="language"> <img id="" src="" alt="..." class="imgNavIta img-thumbnail icon-small">  <span class="lanNavIta">Arabic</span></a></li>
							<!--<li><a id="navDeu" href="#" class="language"> <img id="" src="" alt="..." class="imgNavDeu img-thumbnail icon-small">  <span class="lanNavDeu">Urdu</span></a></li>-->
							<!--<li><a id="navFra" href="#" class="language"><img id="imgNavFra" src="" alt="..." class="img-thumbnail icon-small">  <span id="lanNavFra">Francais</span></a></li>-->
							<li><a id="navEng" href="#" class="language"><img id="" src="" alt="..." class="imgNavEng img-thumbnail icon-small">  <span class="lanNavEng">English</span></a></li>
						</ul>
					</li>
				  </ul>
				
				</div>
				
				</nav>
				</div>
				<div class="col-lg-2 col-sm-2 visible-xs">
						<div id="navbar">
							<ul>
								<li class="dropdown countryDropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a id="navIta" href="#" class="language"> <img id="" src="" alt="..." class="imgNavIta img-thumbnail icon-small">  <span class="lanNavIta">Arabic</span></a></li>
										<!--<li><a id="navDeu" href="#" class="language"> <img id="" src="" alt="..." class="imgNavDeu img-thumbnail icon-small">  <span class="lanNavDeu">Urdu</span></a></li>-->
										<!--<li><a id="navFra" href="#" class="language"><img id="imgNavFra" src="" alt="..." class="img-thumbnail icon-small">  <span id="lanNavFra">Francais</span></a></li>-->
										<li><a id="navEng" href="#" class="language"><img id="" src="" alt="..." class="imgNavEng img-thumbnail icon-small">  <span class="lanNavEng">English</span></a></li>
									</ul>
								</li>
							</ul>
						</div> <!--/.navbar-collapse -->
				</div>
			</div>
        
        </header>
        <div class="wrapper">
			<div class="container">
				<div class="row">
					<div class="banner-info text-center wow fadeIn delay-05s col-lg-12">
						<h2 class="bnr-sub-title">Welcome to Najez</h2>
						<p class="bnr-para">Anywhere Anytime! Just book online</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-xs-12 text-center"><a href="#join-us"><button class="button1 head-btn">JOIN AS A PARTNER</button></a></div>
					<div class="col-lg-6 col-md-6 col-xs-12 text-center"><a href="#join-us"><button class="button2 head-btn">JOIN AS A USER</button></a></div>
					<div class="overlay-detail">
					</div>
				</div>
			  
			</div>
        </div>
      </div>
    </div>
    <!--/ HEADER-->

    <!--second-->
    <section id="about-us" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 text-center">
              <img src="<?php echo base_url('assets/home/'); ?>/img/mobile1.png" class="img-responsive" style="margin:0 auto;">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content">
			<p class="space-150"></p>
			   <p>Najez</p>
			   <h1>ABOUT US</h1>
			   <h5>A 100% Saudi owned brand coming across stunningly. Najez is an amazing venture committed towards providing the best services across the Kingdom of Saudi Arabia. Our vision is to become a business leader dedicated towards delivering excellent quality services with evolving growth and success.</h5>
			   <div class="more_about" style="display:none;padding:0 !important;margin:0 !important;">
				<h5>A pioneering company exceptionally creating and implementing reliable and high quality services to meet the customers and partners needs successfully. Najez is determined to never let their partners & customers down with their uniqueness.</h5>
				<h5>In all aspects, Najez is there for you 24x7. Najez is all set to deliver unbeatable innovative quality services with everlasting commitment driven through our professional class management practices</h5>
			   </div>
			  
			   <p><button class="butn about_btn1">Learn More</button><button class="butn  about_btn2" style="display:none;">Hide</button></p>
			</div>
		</div>
      </div>
    </section>
    <!---->

  
    </section>
    <!---->
    <!---->

     <!--third-->
    <section id="feature" class="section-padding wow fadeIn delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">BENEFITS</h2>
            <p class="sub-title pad-bt35"><i>We give you better benefits</i></p>
          </div>
          <div class="col-sm-4 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/icon1.png">
                
                <p>Better customer satisfaction</p>
              </div>
              
            </div>
          </div>
          <div class="col-sm-4 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/quality-partner.png">
              <p>Easy to use and understand</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/incentive.png">
              <p>Incentives at its best</p>
              </div>
            </div>
          </div>
        </div> 
      </div>
    </section>                  
    <!---->




    <!---->
    <section id="services" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title" style="color:#fff;">SERVICES</h2>
            <p class="sub-title task "><i>a lot of amazing & cool features</i></p>
          </div>
        </div>
        <div class="row images">
           <div class="col-lg-4 content1">
            <div class="row row-margin">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3>SAVE YOUR ARRIVAL AND DEPARTURE LOCATION</h3>
					<P>Save your arrival and departure points in the sophisticated application to ease the booking process.</P>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 heart text-center">
				  <img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
			</div>
            <div class="row row-margin">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3>RIGHT VEHICLE JUST ONE CLICK AWAY</h3>
					<P>You can choose any type of vehicle which fulfils your needs - delivery/ medium/ family/ luxury.</P>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-right">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
					<h3>ANALYSING YOUR CAPTAIN</h3>
					<P>You can have a look at your captain’s picture and rating based on customer opinions and evaluate him.</P>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
			</div>
            
          </div>  
          <div class="col-lg-4">
            
          </div>
          <div class="col-lg-4 content2">
            <div class="row row-margin">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3>DISCOUNTS &amp; STUNNING SERVICE</h3>
					<P>We always strive to be competitive in the market and give you the best discounts and offers.</P>
				</div> 
			</div>
            <div class="row row-margin">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3>TRACKING THE ROUTES</h3>
					<P>You can have a look at your captain’s path and track him for your convenience until you reach your destination.</P>
				</div>
              
			</div>
			<div class="row row-margin">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3>RATE &amp; FEEDBACK</h3>
					<P>Did you liked the ride? Rate it!<br/>Rating your captain is valuable, it gives us the assurance that the captain is fulfilling all his duties in the right manner.</P>
				</div>
              
			</div> 
            
        </div>
      </div>
    </section>
    <!---->
    <!---->
    <!---->
    <section id="portfolio1" class="section-padding wow fadeInUp delay-05s">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title">TERMS OF USE</h2>
            <p class="sub-title pad-bt15" style="display:none;"><i style="color:#7f7c7c;">Get the app flow here</i></p>
          </div>
       </div>
      </div><!--.container-->
      <div class="container-fluid">
        <div class="row">
            <div class="loop owl-carousel owl-theme"> 
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide8.jpg" alt="Owl Image1" class="img-responsive"></div>
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide1.jpg" class="img-responsive"></div>
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide2.jpg" class="img-responsive"></div>
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide3.jpg" class="img-responsive"></div>
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide5.jpg" class="img-responsive"></div>
 
</div>
        </div>
      </div>
    </section>

    <!---->
    <!---->
    <section id="download-app" class=" wow fadeInUp delay-05s">
      <div class="bg-testicolor section-padding">
        <div class="container centered">
        <div class="row">
          <div class="testimonial-item col-lg-12">
               <h1>DOWNLOAD THE APP</h1>
                <p><i>Register to get an early access</i></p>
          </div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
				<a href="#join-us"><button class="pull-right"><img src="<?php echo base_url('assets/home/'); ?>/img/android1.png"> ANDROID</button></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
				<a href="#join-us"><button class="pull-left"><img src="<?php echo base_url('assets/home/'); ?>/img/icon.png"> IOS</button></a>
			</div>
        </div>
        </div>
      </div>
    </section>
    <!---->
    <section id="blog" class="section-padding wow fadeInUp delay-05s" style="display:none;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">BLOG</h2>
            <p class="sub-title pad-bt15"><i style="color:#7f7c7c;">Have look our blog</i></p>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="blog-sec" style="background-color:#fafafa;">
              <div class="blog-img">
                <a href="">
                  <img src="<?php echo base_url('assets/home/'); ?>/img/car.jpg" class="img-responsive">
                </a>
              </div>
              <div class="blog-info">
                <h2>QUIS AUTEM VEL EUM IURE QUI IN</h2>
                <p>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
				<hr/>
                <a href="" class="read-more">DEC 11, 2016</a>
                <a href="" class="read-more pull-right">READ MORE</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="blog-sec" style="background-color:#fafafa;">
              <div class="blog-img">
                <a href="">
                  <img src="<?php echo base_url('assets/home/'); ?>/img/hand.jpg" class="img-responsive">
                </a>
              </div>
              <div class="blog-info">
                <h2>QUIS AUTEM VEL EUM IURE QUI IN</h2>
                <p>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
                <hr/>
				<a href="" class="read-more">DEC 11, 2016</a>
                <a href="" class="read-more pull-right">READ MORE</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="blog-sec" style="background-color:#fafafa;">
              <div class="blog-img">
                <a href="">
                  <img src="<?php echo base_url('assets/home/'); ?>/img/car1.jpg" class="img-responsive">
                </a>
              </div>
              <div class="blog-info">
                <h2>QUIS AUTEM VEL EUM IURE QUI IN</h2>
                <p>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
                <hr/>
				<a href="" class="read-more">DEC 11, 2016</a>
                <a href="" class="read-more pull-right">READ MORE</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!---->
    <section id="join-us" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center white">
            <h2 class="service-title pad-bt15" style="color:#fff; ">JOIN US</h2>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 anti">
					<p class="btn1 pull-right">JOIN AS A PARTNER</p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 anti">
					<p class="btn2 pull-left">JOIN AS A USER</p>
				</div>
			</div>
          </div>
		</div>
		<div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl">
            <hr class="hrclass">
             <div class="circle active">1</div>
             <p>PERSONAL INFORMATION</p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl">
		   <hr class="hrclass">
             <div class="circle">2</div>
             <p>PROVISIONS</p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl">
		   <hr class="hrclass">
             <div class="circle">3</div>
             <p>VEHICLE INFORMATION</p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl">
             <div class="circle">4</div>
             <p>DOCUMENTS</p>
          </div>
		</div>
		<div class="row toggledriver" style="margin-top:25px;">
			<div class="col-md-4 col-sm-6 col-xs-12"></div>
			
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="input-group" style="margin: 0 auto;">
					<div id="radioBtn" class="btn-group">
						<a class="btn btn-primary btn-sm active" data-toggle="driver_type" data-title="Driver">Driver</a>
						<a class="btn btn-primary btn-sm notActive" data-toggle="driver_type" data-title="Delivery Man">Delivery Man</a>
					</div>
					<input type="hidden" name="driver_type" id="driver_type">
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12"></div>
		</div>
		<div class="row">
          <!-- first form start -->
          <div class="col-lg-12">
            <div class="contact-form" id="form1">
				
            </div>
          </div>
          <!-- first form end -->
          <!-- second form start -->
          <div class="col-lg-12">
            <div class="contact-form1" id="form2" style="display:none;">
                
                <form action="<?php echo base_url('frontend_register/user_reg');?>" method="post" autocomplete="off" role="form" class="contactForm userform">
				<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
					<div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="user_fname" class="form-control" id="user_fname" placeholder="First Name" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only alphabets allowed</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="user_lname" id="user_lname" placeholder="Last Name" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only alphabets allowed</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control checkemail_user" name="user_email" id="user_email" placeholder="Email ID" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: abcd12@test.com</div>
							<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Emailid Already Registered</div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="user_mobile" maxlength="9" id="user_mobile" placeholder="Mobile Number" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: 123456789</div>
                        </div>
                    </div>
					<div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <select class="form-control" name="user_gender" id="user_gender">
								<option value="">Gender</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
							<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
                        </div>
                    </div>
					<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<select class="form-control" name="user_city" id="user_city">
										<option value="">City</option>
										<option value="Al-Abwa">Al-Abwa</option>
										<option value="Artaweeiyah">Al Artaweeiyah</option>
										<option value="Badr">Badr</option>
										<option value="Baljurashi">Baljurashi</option>
										<option value="Bisha">Bisha</option>
										<option value="Bareg">Bareg</option>
										<option value="Buraydah">Buraydah</option>
										<option value="Al Bahah">Al Bahah</option>
										<option value="Buq a">Buq a</option>
										<option value="Dammam">Dammam</option>
										<option value="Dhahran">Dhahran</option>
										<option value="Dhurma">Dhurma</option>
										<option value="Dahaban">Dahaban</option>
										<option value="Diriyah">Diriyah</option>
										<option value="Duba">Duba</option>
										<option value="Dumat Al-Jandal">Dumat Al-Jandal</option>
										<option value="Dawadmi">Dawadmi</option>
										<option value="Farasan city">Farasan city</option>
										<option value="Gatgat">Gatgat</option>
										<option value="Gerrha">Gerrha</option>
										<option value="Gurayat">Gurayat</option>
										<option value="Al-Gwei'iyyah">Al-Gwei'iyyah</option>
										<option value="Hautat Sudair">Hautat Sudair</option>
										<option value="Habala">Habala</option>
										<option value="Hajrah">Hajrah</option>
										<option value="Haql">Haql</option>
										<option value="Al-Hareeq">Al-Hareeq</option>
										<option value="Harmah">Harmah</option>
										<option value="Ha'il">Ha'il</option>
										<option value="Hotat Bani Tamim">Hotat Bani Tamim</option>
										<option value="Hofuf">Hofuf</option>
										<option value="Huraymila">Huraymila</option>
										<option value="Hafr Al-Batin">Hafr Al-Batin</option>
										<option value="Jabal Umm al Ru'us">Jabal Umm al Ru'us</option>
										<option value="Jalajil">Jalajil</option>
										<option value="Al Jawf">Al Jawf</option>
										<option value="Jeddah">Jeddah</option>
										<option value="Jizan">Jizan</option>
										<option value="Jizan Economic City">Jizan Economic City</option>
										<option value="Jubail">Jubail</option>
										<option value="Al Jafer">Al Jafer</option>
										<option value="Khafji">Khafji</option>
										<option value="Khaybar">Khaybar</option>
										<option value="King Abdullah Economic City">King Abdullah Economic City</option>
										<option value="Khamis Mushayt">Khamis Mushayt</option>
										<option value="Al Kharj">Al Kharj</option>
										<option value="Knowledge Economic City, Medina">Knowledge Economic City, Medina</option>
										<option value="Khobar">Khobar</option>
										<option value="Al-Khutt">Al-Khutt</option>
										<option value="Layla">Layla</option>
										<option value="Lihyan">Lihyan</option>
										<option value="Al Lith">Al Lith</option>
										<option value="Al Majma'ah">Al Majma'ah</option>
										<option value="Mastoorah">Mastoorah</option>
										<option value="Al Mikhwah">Al Mikhwah</option>
										<option value="Al-Mubarraz">Al-Mubarraz</option>
										<option value="Al Mawain">Al Mawain</option>
										<option value="Mecca">Mecca</option>
										<option value="Medina">Medina</option>
										<option value="Muzahmiyya">Muzahmiyya</option>
										<option value="Najran">Najran</option>
										<option value="Al-Namas">Al-Namas</option>
										<option value="Omloj">Omloj</option>
										<option value="Al-Omran">Al-Omran</option>
										<option value="Al-Oyoon">Al-Oyoon</option>
										<option value="Qadeimah">Qadeimah</option>
										<option value="Qatif">Qatif</option>
										<option value="Qaisumah">Qaisumah</option>
										<option value="Al Qunfudhah">Al Qunfudhah</option>
										<option value="Rabigh">Rabigh</option>
										<option value="Rafha">Rafha</option>
										<option value="Ar Rass">Ar Rass</option>
										<option value="Ras Tanura">Ras Tanura</option>
										<option value="Riyadh">Riyadh</option>
										<option value="Riyadh Al-Khabra">Riyadh Al-Khabra</option>
										<option value="Rumailah">Rumailah</option>
										<option value="Sabt Al Alaya">Sabt Al Alaya</option>
										<option value="Saihat">Saihat</option>
										<option value="Safwa city">Safwa city</option>
										<option value="Sakakah">Sakakah</option>
										<option value="Sharurah">Sharurah</option>
										<option value="Shaqraa">Shaqraa</option>
										<option value="Shaybah">Shaybah</option>
										<option value="As Sulayyil">As Sulayyil</option>
										<option value="Taif">Taif</option>
										<option value="Tabuk">Tabuk</option>
										<option value="Tanomah">Tanomah</option>
										<option value="Tarout">Tarout</option>
										<option value="Tayma">Tayma</option>
										<option value="Thadiq">Thadiq</option>
										<option value="Thuwal">Thuwal</option>
										<option value="Thuqbah">Thuqbah</option>
										<option value="Turaif">Turaif</option>
										<option value="Tabarjal">Tabarjal</option>
										<option value="Udhailiyah">Udhailiyah</option>
										<option value="Al-`Ula">Al-`Ula</option>
										<option value="Um Al-Sahek">Um Al-Sahek</option>
										<option value="Unaizah">Unaizah</option>
										<option value="Uqair">Uqair</option>
										<option value="Uyayna">Uyayna</option>
										<option value="Uyun AlJiwa">Uyun AlJiwa</option>
										<option value="Wadi Al-Dawasir">Wadi Al-Dawasir</option>
										<option value="Al Wajh">Al Wajh</option>
										<option value="Yanbu">Yanbu</option>
										<option value="Az Zaimah">Az Zaimah</option>
										<option value="Zulfi">Zulfi</option>
										<option value="Abqaiq">Abqaiq</option>
										<option value="Hassa">Hassa</option>
										<option value="Bahrain">Bahrain</option>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
                    </div>
					<div class="row" style="margin-top:0;">
						<div class="col-md-4 col-sm-12 col-xs-12"></div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<input type="submit" class="button3" id="user_regs" name="user_reg" />
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12"></div>
                    </div>
                </form>
              
            </div>
          </div>
          <!-- sencond form end -->

          
        </div>
      </div>
    </section>
    <!---->


     <section id="feature1" class="section-padding wow fadeIn delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15">GET IN TOUCH</h2>
          </div>
		</div>
		<div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/phone.png">
              </div>
              <p>Phone : +966540988577</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/mssg.png">
              </div>
              <p>Email : <a href="mailto:info@najez-online.com" style="color:#333;">info@najez-online.com</a></p>
            </div>
            
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="display:none;">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/web.png">
              </div>
              <p>Web : www.yoursite.com</p>
            </div>
          </div>
        </div>
		<div class="row" style="display:none;">
			<div class="col-lg-12 text-center">
				<div class="wrap-item text-center">
				  <div class="item-img">
					 <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/fb.jpg" style="margin-top: 50px;"></a>
					  <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter.jpg" style="margin-top: 50px;"></a>
					  <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/google.jpg" style="margin-top: 50px;"></a>
				  </div>
				</div>
			</div>
		</div>
      </div>
    </section>   

<div class="partner_driver" style="display:none;">
	<form action="<?php echo base_url('frontend_register/partner_reg');?>" method="post" role="form" class="contactForm partnerForm" enctype="multipart/form-data" autocomplete="off" onsubmit="event.preventDefault(); formsubmitfunc();">
					<div class="step1">
						<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<input type="hidden" name="driver_type" id="driver_type">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" name="fname" class="form-control" id="fname" placeholder="First Name" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only Alphabets Allowed</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only Alphabets Allowed</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control checkemail_driver" name="emailid" id="emailid" placeholder="Email ID" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: abcd12@test.com</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Emailid Already Registered</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="mobileno" id="mobileno" maxlength="9" placeholder="Mobile Number" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: 123456789</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="identy" id="identy" maxlength="10" placeholder="National ID/Iqama" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: 1234567890</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group" id="">
									<select class="form-control" name="city" id="city" >
										<option value="">City</option>
										<option value="Riyadh">Riyadh</option>
										<option value="Jeddah">Jeddah</option>
										<option value="Dammam">Dammam</option>
										<option value="Khobar">Khobar</option>
										<option value="Jubail">Jubail</option>
										<option value="Hassa">Hassa</option>
										<option value="Dhahran">Dhahran</option>
										<option value="Qatif">Qatif</option>
										<option value="Kharj">Kharj</option>
										<option value="Qassim">Qassim</option>
										<option value="Khafji">Khafji</option>
										<option value="Hafar Al-Batin">Hafar Al-Batin</option>
										<option value="Bahrain">Bahrain</option>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12 delfield" >
								<div class="form-group">
									<input type="text" id="date" name="date" class="form-control" data-date-end-date="0d" placeholder="DOB (DD/MM/YYYY)" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Min age 21 years</div>
								</div>
							</div>
							
							
							<div class="col-md-4 col-sm-6 col-xs-12 delfield">
								<div class="form-group">
									<input type="text" class="form-control" name="code" id="code" placeholder="Captain's code that nominated you" />
								</div>
							</div>
							
						</div>
						<div class="row formshow" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="Next" />
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step2" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<textarea class="form-control" disabled id="tnc" rows="8" placeholder="Terms and conditions" style="color: #000 !important;" />
- The minimum age to join is 21 years
- Car Model should be Year 2010 and Above
- Arabic Speaking is Mandatory Condition
									
- For Non-Saudis: You must apply for accommodation on a special fare and a car licensed by the Ministry of Transport
									</textarea>
									
								</div>
								<label style="color:#fff;">Declaration*</label>
								<div class="form-group" style="color:#fff;">
									<input type="checkbox" class="" name="agree" id="agree" value="1" /> <span class="declbox form-group">I have read and agree to the terms and conditions</span>
									
								</div>
							</div>
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step0_btn" onclick="return step0_btn();" value="Back" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="Next" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step3" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000; text-align: center;padding-bottom: 10px;display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<input type="text" class="form-control" name="vehicle_sno" id="vehicle_sno" maxlength="100" placeholder="Serial number of the vehicle" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only A-Z 0-9 allowed</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12" style="margin-top:-25px;">
								<label style="color: #ddd;font-weight: normal;font-size: 12px;">Vehicle Plate Number (xyz1234)</label>
								<div class="row">
								
									<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
										<div class="form-group">
											<input type="text" class="form-control" name="vehicle_plate_char" id="vehicle_plate_char" placeholder="xyz" maxlength="3" />
											<div class="validation" style="top:-42px;"><i style="position: absolute;bottom: -20px;left: 30px;" class="fa fa-caret-down" aria-hidden="true"></i> Mandatory Field</div>
											<div class="validation" style="top:-42px;width: 175px;left: 7px;"><i style="position: absolute;bottom: -20px;left: 30px;" class="fa fa-caret-down" aria-hidden="true"></i> Only alphabets allowed</div>
										</div>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-12 col-sx-12">
										
											<div class="form-group">
												<input type="text" class="form-control" name="vehicle_plate_num" id="vehicle_plate_num" placeholder="1234" maxlength="4" />
												<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
												<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only numeric allowed</div>
											</div>
									
									</div>
								</div>
							</div>
							
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="vehicle_type" id="vehicle_type">
										<option value="">Type of the vehicle</option>
										<!--<option value="Delivery">Delivery</option>-->
										<?php foreach($brand_list as $brand_list1):     ?>
										<optgroup label="<?=$brand_list1['car_brand']; ?>">
										<?php $ss = $this->model->get_car_list($brand_list1['car_brand']); ?>
										<?php foreach($ss as $ss):?>
										<option value="<?=  $ss['id']; ?>"><?=$ss['car_model'];  ?></option>							
										<?php endforeach; ?>
										</optgroup>
										<?php endforeach;    ?>
										</select>
										<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="car_model1">
									<input type="text" class="form-control" placeholder="Car Model" id="car_model" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="vehicle_year_dropdown">
									<select class="form-control" name="vehicle_year" id="vehicle_year">
									<option value="">Year of manufacture</option>
									<?php for($starting_year = 2010; $starting_year <= date('Y'); $starting_year++) {
										echo '<option value="'.$starting_year.'">'.$starting_year.'</option>';
									}?>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<input type="text" class="form-control" name="vehicle_color" id="vehicle_color" placeholder="Color" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only alphabets allowed</div>
								</div>
							</div>
							
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="Back" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step3_btn" onclick="return step3_btn();" value="Next" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step4" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Photograph</label>
									<input type="file" class="form-control photo" name="photograph" id="photograph" placeholder="Photograph" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Residence / Identity</label>
									<input type="file" class="form-control photo" name="residence" id="residence" placeholder="Residence / Identity" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Driving License</label>
									<input type="file" class="form-control photo" name="driver_license" id="driver_license" placeholder="Driving License" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Vehicle registration</label>
									<input type="file" class="form-control photo" name="vehicle_reg" id="vehicle_reg" placeholder="Vehicle registration" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Insurance of vehicles</label>
									<input type="file" class="form-control photo" name="vehicle_insur" id="vehicle_insur" placeholder="Insurance of vehicles" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Authorization image</label>
									<input type="file" class="form-control photo" name="auth_img" id="auth_img" placeholder="Authorization image" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Tafweeth</label>
									<input type="file" class="form-control photo" name="tafweeth_img" id="tafweeth_img" placeholder="Tafweeth image" />
									
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
								<label>IBAN Number</label>
									<input type="text" class="form-control" name="iban" id="iban" maxlength="24" placeholder="IBAN Number" />
									<div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: SA1234000001234567890000</div>
								</div>
									
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
								<label>Bank</label>
									<select class="form-control" name="iban_bank" id="iban_bank" >
										<option value="">Bank</option>
										<option value="The National Commercial Bank">The National Commercial Bank</option>
										<option value="The Saudi British Bank">The Saudi British Bank</option>
										<option value="Saudi Investment Bank">Saudi Investment Bank</option>
										<option value="Alinma Bank">Alinma Bank</option>
										<option value="Banque Saudi Fransi">Banque Saudi Fransi</option>
										<option value="Riyad Bank">Riyad Bank</option>
										<option value="Samba Financial Group (Samba)">Samba Financial Group (Samba)</option>
										<option value="Saudi Hollandi Bank (Alawwal)">Saudi Hollandi Bank (Alawwal)</option>
										<option value="Al Rajhi Bank">Al Rajhi Bank</option>
										<option value="Arab National Bank">Arab National Bank</option>
										<option value="Bank Al-Bilad">Bank Al-Bilad</option>
										<option value="Bank AlJazira">Bank AlJazira</option>
										<option value="Asia Bank">Asia Bank</option>
										<option value="Qatar National Bank (QNB)">Qatar National Bank (QNB)</option>
										<option value="State Bank of India (SBI)">State Bank of India (SBI)</option>
										<option value="Gulf International Bank (GIB)">Gulf International Bank (GIB)</option>
										<option value="Emirates NBD">Emirates NBD</option>
										<option value="National Bank of Bahrain (NBB)">National Bank of Bahrain (NBB)</option>
										<option value="National Bank of Kuwait (NBK)">National Bank of Kuwait (NBK)</option>
										<option value="Muscat Bank">Muscat Bank</option>
										<option value="Deutsche Bank">Deutsche Bank</option>
										<option value="BNP Paribas">BNP Paribas</option>
										<option value="J.P. Morgan Chase N.A">J.P. Morgan Chase N.A</option>
										<option value="National Bank of Pakistan (NBP)">National Bank of Pakistan (NBP)</option>
										<option value="T.C.ZIRAAT BANKASI A.S.">T.C.ZIRAAT BANKASI A.S.</option>
										<option value="Industrial and Commercial Bank of China (ICBC)">Industrial and Commercial Bank of China (ICBC)</option>
									</select>
									<div style="top:25px;"  class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
									
							</div>
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="Back" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="submit" class="button3" name="partner_submit" id="partner_submit" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
                </form>
</div>
<div class="partner_delivery" style="display:none;">
	<form action="<?php echo base_url('frontend_register/partner_reg');?>" method="post" role="form" class="contactForm partnerForm" enctype="multipart/form-data" autocomplete="off" onsubmit="event.preventDefault(); return formsubmitfunc(this);">
					<div class="step1">
						<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<input type="hidden" name="driver_type" id="driver_type">
						<div class="row formshow">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" name="fname" class="form-control" id="fname" placeholder="First Name" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only Alphabets Allowed</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Only Alphabets Allowed</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="identy" id="identy" maxlength="10" placeholder="National ID/Iqama" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: 1234567890</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control checkemail_driver" name="emailid" id="emailid" placeholder="Email ID" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: abcd12@test.com</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Emailid Already Registered</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="mobileno" id="mobileno" maxlength="9" placeholder="Mobile Number" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: 123456789</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group" id="">
									<select class="form-control" name="city" id="city" >
										<option value="">City</option>
										<option value="Riyadh">Riyadh</option>
										<option value="Jeddah">Jeddah</option>
										<option value="Dammam">Dammam</option>
										<option value="Khobar">Khobar</option>
										<option value="Jubail">Jubail</option>
										<option value="Hassa">Hassa</option>
										<option value="Dhahran">Dhahran</option>
										<option value="Qatif">Qatif</option>
										<option value="Kharj">Kharj</option>
										<option value="Qassim">Qassim</option>
										<option value="Khafji">Khafji</option>
										<option value="Hafar Al-Batin">Hafar Al-Batin</option>
										<option value="Bahrain">Bahrain</option>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							
							
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="button" onclick="return step1_btn();" class="button3 step1_btn" value="Next" />
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step2" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									
									<textarea class="form-control" disabled id="tnc" rows="8" placeholder="Terms and conditions" style="color: #000 !important;" />
- The minimum age to join is 21 years
- Car Model should be Year 2000 and Above
- Arabic Speaking is Mandatory Condition
									
- For Non-Saudis: You must apply for accommodation on a special fare and a car licensed by the Ministry of Transport
									</textarea>
								</div>
								<label style="color:#fff;">Declaration*</label>
								<div class="form-group" style="color:#fff;">
									<input type="checkbox" class="" name="agree" id="agree" value="1" /> <span class="declbox form-group">I have read and agree to the terms and conditions</span>
									
								</div>
							</div>
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step0_btn" onclick="return step0_btn();" value="Back" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="Next" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step3" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000; text-align: center;padding-bottom: 10px;display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="vehicle_type" id="vehicle_type">
										<option value="">Type of the vehicle</option>
										<!--<option value="Delivery">Delivery</option>-->
										<?php //print_r($brand_list);die(); ?>
										<?php foreach($brand_list as $brand_list2):     ?>
										<optgroup label="<?=$brand_list2['car_brand']; ?>">
										<?php $ss = $this->model->get_car_list($brand_list2['car_brand']); ?>
										<?php foreach($ss as $ss):?>
										<option value="<?=  $ss['id']; ?>"><?=$ss['car_model'];  ?></option>							
										<?php endforeach; ?>
										</optgroup>
										<?php endforeach;    ?>
										</select>
										<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
						
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="car_model1">
									<input type="text" class="form-control" placeholder="Car Model" id="car_model" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="vehicle_year_dropdown">
									<select class="form-control" name="vehicle_year" id="vehicle_year">
									<option value="">Year of manufacture</option>
									<?php for($starting_year = 2000; $starting_year <= date('Y'); $starting_year++) {
										echo '<option value="'.$starting_year.'">'.$starting_year.'</option>';
									}?>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
								</div>
							</div>
							
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="Back" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step3_btn" onclick="return step3_btn();" value="Next" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step4" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
								<label>IBAN Number</label>
									<input type="text" class="form-control" name="iban" id="iban" maxlength="24" placeholder="IBAN Number" />
									<div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Mandatory Field</div>
									<div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> ex: SA1234000001234567890000</div>
								</div>
									
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label>Tafweeth</label>
									<input type="file" class="form-control photo" name="tafweeth_img" id="tafweeth_img" placeholder="Tafweeth image" />
									
								</div>
							</div>
							
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="Back" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="submit" class="button3" name="partner_submit" id="partner_submit" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
                </form>
</div>
    <!---->
    <footer id="footer">
      <div class="container">
        <div class="row text-center" style="padding-top:40px;">
          <p>&copy; 2017 Najez. All right reserved. | <a href="<?php echo base_url('page/privacy-policy');?>">Privacy Policy</a></p>
          
        </div>
      </div>
    </footer>
    <!---->
  </div>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/wow.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/custom.js"></script>
    <!--<script src="contactform/contactform.js"></script>-->
    <script src="<?php echo base_url('assets/home'); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/bootstrap-datepicker.js"></script>
	
    <script src="<?php echo base_url('assets/home'); ?>/js/form_validation.js"></script>

<!-- btn  -->
<style>
#radioBtn a {
    padding: 6px 34px;
    font-size: 15px;
}
#radioBtn .notActive{
    color: #ff9e15;
    background-color: #fff;
}
#radioBtn .active{
	color: #fff;
    background-color: #ff9e15;
    border-color: #ff9e15;
}
.more_about{padding:0 !important;padding:0 !important;}
</style>
<script>
var html = $('.partner_driver').html();
$('#form1').html(html);
$('#form1').find('#driver_type').prop('value', 'Driver');
jQuery('.form-group input, .form-group select').bind("focusout",function(){
		jQuery(this).removeClass('error');
		jQuery(this).next('.validation').hide();
		jQuery(this).next('.validation').next('.validation').hide();
		jQuery(this).next('.validation').next('.validation').next('.validation').hide();
	});
	var d = new Date();
	$('#form1').find('#date').datepicker({
		//format: 'yyyy-mm-dd',
		format: 'dd/mm/yyyy',
		startDate:'01/01/1940',
		autoclose: true
	});
	$("#vehicle_type").bind("change",function(){
		   $.ajax({
			url:'home/get_car_type/?id=',
		type:'POST',
		ContentType : 'application/json',
		beforeSend:function(){
		$('#form1').find("#car_model").empty();
	   },
	  data:{'id':$(this).val()},
	  success:function(data){ 
	  console.log(data);
	   var datas = JSON.parse(data);
	   //console.log(datas);
	 var option = '<select id="car_model" name="car_model" class="form-control">';
	   $.each(datas,function(i,val){
		console.log(val.car_type);
	   option += '<option value="'+val.car_type+'">'+val.car_type+'</option>';
	   
	   } );
	   option += '</select><div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Required Field</div>';
	   console.log(option);
	   $('#form1').find('#car_model1').html(option);
	  }, 
	  error:function(){
	   alert('Network Error Please Select Activity Again');
	   $('#form1').find("#select").val("");
	  }
	  });
	 });
$('#radioBtn a').on('click', function(){
	$('.formshow').show();
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    
    if(sel == 'Delivery Man'){
		var html = $('.partner_delivery').html();
		$('#form1').html(html);
		$('#form1').find('#'+tog).prop('value', sel);
		
	}else{
		var html = $('.partner_driver').html();
		$('#form1').html(html);
		$('#form1').find('#'+tog).prop('value', sel);
	}
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
	jQuery('.form-group input, .form-group select').bind("focusout",function(){
		jQuery(this).removeClass('error');
		jQuery(this).next('.validation').hide();
		jQuery(this).next('.validation').next('.validation').hide();
		jQuery(this).next('.validation').next('.validation').next('.validation').hide();
	});
	$("#vehicle_type").bind("change",function(){
		   $.ajax({
			url:'home/get_car_type/?id=',
		type:'POST',
		ContentType : 'application/json',
		beforeSend:function(){
		$('#form1').find("#car_model").empty();
	   },
	  data:{'id':$(this).val()},
	  success:function(data){ 
	  console.log(data);
	   var datas = JSON.parse(data);
	   //console.log(datas);
	 var option = '<select id="car_model" name="car_model" class="form-control">';
	   $.each(datas,function(i,val){
		console.log(val.car_type);
	   option += '<option value="'+val.car_type+'">'+val.car_type+'</option>';
	   
	   } );
	   option += '</select><div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Required Field</div>';
	   console.log(option);
	   $('#form1').find('#car_model1').html(option);
	  }, 
	  error:function(){
	   alert('Network Error Please Select Activity Again');
	   $('#form1').find("#select").val("");
	  }
	  });
	 });
	 $('#form1').find('#date').datepicker({
		//format: 'yyyy-mm-dd',
		format: 'dd/mm/yyyy',
		startDate:'01/01/1940',
		autoclose: true
	});
});

//function formsubmitfunc(test){ console.log(test);}


       </script>

  </body>
</html>

	