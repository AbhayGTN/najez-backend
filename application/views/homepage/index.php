<section id="about-us" class="section-padding wow fadeInUp delay-05s">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 text-center"> <img src="<?php echo base_url('assets/home/'); ?>/img/mobile1.png" class="img-responsive" style="margin:0 auto;"> </div>
      <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content">
        <p class="space-150"></p>
        <!--<p><?php echo $this->lang->line('najez'); ?></p>-->
        <h1><?php echo $this->lang->line('content_heading_aboutus'); ?></h1>
        <h5><?php echo $this->lang->line('content_aboutus_paragraph1'); ?></h5>
        <div class="more_about" style="display:none;padding:0 !important;margin:0 !important;">
          <h5><?php echo $this->lang->line('content_aboutus_paragraph2'); ?></h5>
          <h5><?php echo $this->lang->line('content_aboutus_paragraph3'); ?></h5>
        </div>
        <p>
          <button class="butn about_btn1"><?php echo $this->lang->line('learn_more'); ?></button>
          <button class="butn  about_btn2" style="display:none;"><?php echo $this->lang->line('hide'); ?></button>
        </p>
      </div>
    </div>
  </div>
</section>
<!---->

</section>
<!----> 
<!----> 

<!--third-->
<section id="feature" class="section-padding wow fadeIn delay-05s">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="service-title pad-bt15"><?php echo $this->lang->line('benefits'); ?></h2>
        <p class="sub-title pad-bt35"><i><?php echo $this->lang->line('we_give_you_better_benefits'); ?></i></p>
      </div>
      <div class="col-sm-4 col-xs-12 box-one">
        <div class="wrap-item text-center">
          <div class="item-img"> <img src="<?php echo base_url('assets/home/'); ?>/img/icon1.png">
            <p><?php echo $this->lang->line('better_customer_satisfaction'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12 box-two">
        <div class="wrap-item text-center">
          <div class="item-img"> <img src="<?php echo base_url('assets/home/'); ?>/img/quality-partner.png">
            <p><?php echo $this->lang->line('easy_to_use_and_understand'); ?></p>
          </div>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12 box-three">
        <div class="wrap-item text-center">
          <div class="item-img"> <img src="<?php echo base_url('assets/home/'); ?>/img/incentive.png">
            <p><?php echo $this->lang->line('incentives_at_its_best'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!----> 

<!---->
<section id="services" class="section-padding wow fadeInUp delay-05s">
  <div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <h2 class="service-title" style="color:#fff;"><?php echo $this->lang->line('content_services_heading'); ?></h2>
      <p class="sub-title task "><i><?php echo $this->lang->line('content_services_sub_title'); ?></i></p>
    </div>
  </div>
  <div class="row images">
    <div class="col-lg-4 content1">
      <div class="row row-margin">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
          <h3><?php echo $this->lang->line('save_your_arrival'); ?></h3>
          <P><?php echo $this->lang->line('save_your_arrival_and_departure'); ?></P>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 heart text-center"> <img src="<?php echo base_url('assets/home/'); ?>/img/heart.png"> </div>
      </div>
      <div class="row row-margin">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
          <h3><?php echo $this->lang->line('right_vehicle_just_one_click_away'); ?></h3>
          <P><?php echo $this->lang->line('you_can_choose_any_type_of_vehicle'); ?></P>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-right"> <img src="<?php echo base_url('assets/home/'); ?>/img/heart.png"> </div>
      </div>
      <div class="row row-margin">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
          <h3><?php echo $this->lang->line('analysing_your_captain'); ?></h3>
          <P><?php echo $this->lang->line('you_can_have_a_look_at_your_captain'); ?></P>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart"> <img src="<?php echo base_url('assets/home/'); ?>/img/heart.png"> </div>
      </div>
    </div>
    <div class="col-lg-4 contentcenter"> </div>
    <div class="col-lg-4 content2">
      <div class="row row-margin">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left"> <img src="<?php echo base_url('assets/home/'); ?>/img/heart.png"> </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
          <h3><?php echo $this->lang->line('discounts_stunning_service'); ?></h3>
          <P><?php echo $this->lang->line('we_always_strive_to_be_competitive'); ?></P>
        </div>
      </div>
      <div class="row row-margin">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left"> <img src="<?php echo base_url('assets/home/'); ?>/img/heart.png"> </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
          <h3><?php echo $this->lang->line('tracking_the_routes'); ?></h3>
          <P><?php echo $this->lang->line('you_can_have_a_look_at_your_captain'); ?></P>
        </div>
      </div>
      <div class="row row-margin">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left"> <img src="<?php echo base_url('assets/home/'); ?>/img/heart.png"> </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
          <h3><?php echo $this->lang->line('rate_feedback'); ?></h3>
          <P><?php echo $this->lang->line('did_you_liked_the_ride_rate_it'); ?></P>
        </div>
      </div>
    </div>
  </div>
</section>
<!----> 
<!----> 
<!---->
<section id="portfolio1" class="section-padding wow fadeInUp delay-05s">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="service-title"><?php echo $this->lang->line('terms_of_use'); ?></h2>
        <p class="sub-title pad-bt15" style="display:none;"><i style="color:#7f7c7c;">Get the app flow here</i></p>
      </div>
    </div>
  </div>
  <!--.container-->
  <div class="container-fluid">
    <div class="row">
      <div class="loop owl-carousel owl-theme"> 
        <!-- Start img 1 -->
        <?php if (empty($this->session->userdata['site_lang'])) { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide8.jpg" alt="Owl Image1" class="img-responsive"></div>
        <?php } elseif ($this->session->userdata['site_lang'] == 'english') { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide8.jpg" alt="Owl Image1" class="img-responsive"></div>
        <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide8-arabic.jpg" alt="Owl Image1" class="img-responsive"></div>
        <?php } ?>
        <!-- End img 1 --> 
        
        <!-- Start img 2 -->
        <?php if (empty($this->session->userdata['site_lang'])) { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide1.jpg" class="img-responsive"></div>
        <?php } elseif ($this->session->userdata['site_lang'] == 'english') { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide1.jpg" class="img-responsive"></div>
        <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide1-arabic.jpg" class="img-responsive"></div>
        <?php } ?>
        <!-- End img 2 -->
        
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide2.jpg" class="img-responsive"></div>
        
        <!-- Start img 3 -->
        <?php if (empty($this->session->userdata['site_lang'])) { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide3.jpg" class="img-responsive"></div>
        <?php } elseif ($this->session->userdata['site_lang'] == 'english') { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide3.jpg" class="img-responsive"></div>
        <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide3-arabic.jpg" class="img-responsive"></div>
        <?php } ?>
        <!-- End img 3 --> 
        
        <!-- Start img 4 -->
        <?php if (empty($this->session->userdata['site_lang'])) { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide5.jpg" class="img-responsive"></div>
        <?php } elseif ($this->session->userdata['site_lang'] == 'english') { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide5.jpg" class="img-responsive"></div>
        <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
        <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide5-arabic.jpg" class="img-responsive"></div>
        <?php } ?>
        <!-- End img 4 --> 
        
      </div>
    </div>
  </div>
</section>

<!----> 
<!---->
<section id="download-app" class=" wow fadeInUp delay-05s">
  <div class="bg-testicolor section-padding">
    <div class="container centered">
      <div class="row">
        <div class="testimonial-item col-lg-12">
          <h1><?php echo $this->lang->line('download_the_app'); ?></h1>
          <p><i><?php echo $this->lang->line('register_to_get_an_early_access'); ?></i></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center white"> 
          <!--<h2 class="service-title pad-bt15" style="color:#fff; "><?php echo $this->lang->line('joinus'); ?></h2>-->
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 anti tg-one">
              <p class="btn1 pull-right"><?php echo $this->lang->line('join_as_a_partner_bellow'); ?></p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 anti tg-two">
              <p class="btn2 pull-left"><?php echo $this->lang->line('join_as_a_user_bellow'); ?></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row"> 
        <!-- first form start -->
        <div class="contact-form" id="form1">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 btns">
            <h3><i><?php echo $this->lang->line('home-text-1'); ?></i></h3>
            <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
            <a href="#join-us">
            <button class=""><img src="<?php echo base_url('assets/home/'); ?>/img/and-roid.png"> <?php echo $this->lang->line('android'); ?></button>
            </a>
            <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
            <a href="#join-us">
            <button class=""><?php echo $this->lang->line('android'); ?><img src="<?php echo base_url('assets/home/'); ?>/img/and-roid.png"> </button>
            </a>
            <?php } ?>
            <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
            <a href="#join-us">
            <button class=""><img src="<?php echo base_url('assets/home/'); ?>/img/ios.png"> <?php echo $this->lang->line('ios'); ?></button>
            </a>
            <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
            <a href="#join-us">
            <button class=""> <?php echo $this->lang->line('ios'); ?><img src="<?php echo base_url('assets/home/'); ?>/img/ios.png"></button>
            </a>
            <?php } ?>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center downloadapp-img"><img class="app-download img-responsive" src="http://najez-online.com/assets/home//img/download-app-driver.png" /></div>
        </div>
        <!-- first form end --> 
        <!-- second form start -->
        <div class="contact-form1" id="form2" style="display:none;">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 btns">
            <h3><i><?php echo $this->lang->line('home-text-2'); ?></i></h3>
            <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
            <a href="#join-us">
            <button class=""><img src="<?php echo base_url('assets/home/'); ?>/img/and-roid.png"> <?php echo $this->lang->line('android'); ?></button>
            </a>
            <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
            <a href="#join-us">
            <button class=""><?php echo $this->lang->line('android'); ?><img src="<?php echo base_url('assets/home/'); ?>/img/and-roid.png"> </button>
            </a>
            <?php } ?>
            <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
            <a href="#join-us">
            <button class=""><img src="<?php echo base_url('assets/home/'); ?>/img/ios.png"> <?php echo $this->lang->line('ios'); ?></button>
            </a>
            <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
            <a href="#join-us">
            <button class=""> <?php echo $this->lang->line('ios'); ?><img src="<?php echo base_url('assets/home/'); ?>/img/ios.png"></button>
            </a>
            <?php } ?>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center downloadapp-img"><img class="app-download img-responsive" src="http://najez-online.com/assets/home//img/download-app.png" /></div>
        </div>
        <!-- sencond form end --> 
        
      </div>
      <!--<div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                    <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
                      <a href="#join-us"><button class="pull-right"><img src="<?php echo base_url('assets/home/'); ?>/img/android1.png"> <?php echo $this->lang->line('android'); ?></button></a>
                    <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?> 
                      <a href="#join-us"><button class="pull-right"><?php echo $this->lang->line('android'); ?><img src="<?php echo base_url('assets/home/'); ?>/img/android1.png"> </button></a>
                    <?php } ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                    <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
                      <a href="#join-us"><button class="pull-left"><img src="<?php echo base_url('assets/home/'); ?>/img/icon.png"> <?php echo $this->lang->line('ios'); ?></button></a>
                    <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
                      <a href="#join-us"><button class="pull-left"> <?php echo $this->lang->line('ios'); ?><img src="<?php echo base_url('assets/home/'); ?>/img/icon.png"></button></a>
                    <?php } ?>	
                </div>
            </div>--> 
    </div>
  </div>
</section>
<!---->
<section id="blog" class="section-padding wow fadeInUp delay-05s" style="display:none;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="service-title pad-bt15"><?php echo $this->lang->line('blog'); ?>BLOG</h2>
        <p class="sub-title pad-bt15"><i style="color:#7f7c7c;"><?php echo $this->lang->line('have_look_our_blog'); ?>Have look our blog</i></p>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blog-sec" style="background-color:#fafafa;">
          <div class="blog-img"> <a href=""> <img src="<?php echo base_url('assets/home/'); ?>/img/car.jpg" class="img-responsive"> </a> </div>
          <div class="blog-info">
            <h2><?php echo $this->lang->line('quis_autem_bel_eum_iure_qui_in'); ?>QUIS AUTEM VEL EUM IURE QUI IN</h2>
            <p><?php echo $this->lang->line('we_cannot_expect_people_to_have_respect_for'); ?>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
            <hr/>
            <a href="" class="read-more"><?php echo $this->lang->line('dec112016_1'); ?>DEC 11, 2016</a> <a href="" class="read-more pull-right"> <?php echo $this->lang->line('read_more'); ?>READ MORE</a> </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blog-sec" style="background-color:#fafafa;">
          <div class="blog-img"> <a href=""> <img src="<?php echo base_url('assets/home/'); ?>/img/hand.jpg" class="img-responsive"> </a> </div>
          <div class="blog-info">
            <h2><?php echo $this->lang->line('quis_autem_vel_eum_iure_qui_in'); ?>QUIS AUTEM VEL EUM IURE QUI IN</h2>
            <p><?php echo $this->lang->line('we_cannot_expect_people_to_have_respect'); ?>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
            <hr/>
            <a href="" class="read-more"><?php echo $this->lang->line('dec112016_2'); ?>DEC 11, 2016</a> <a href="" class="read-more pull-right"><?php echo $this->lang->line('read_more'); ?>READ MORE</a> </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blog-sec" style="background-color:#fafafa;">
          <div class="blog-img"> <a href=""> <img src="<?php echo base_url('assets/home/'); ?>/img/car1.jpg" class="img-responsive"> </a> </div>
          <div class="blog-info">
            <h2><?php echo $this->lang->line('quis_autem_vel_eum_iure_qui_in'); ?>QUIS AUTEM VEL EUM IURE QUI IN</h2>
            <p><?php echo $this->lang->line('we_cannot_expect_people_to_have_respect'); ?>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
            <hr/>
            <a href="" class="read-more"><?php echo $this->lang->line('dec112016_3'); ?>DEC 11, 2016</a> <a href="" class="read-more pull-right"><?php echo $this->lang->line('read_more'); ?>READ MORE</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!---->
<section id="join-us" class="section-padding wow fadeInUp delay-05s">
  <div class="container"> 
    <!--<div class="row">
            <div class="col-md-12 text-center white">
                <h2 class="service-title pad-bt15" style="color:#fff; "><?php echo $this->lang->line('joinus'); ?></h2>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 anti tg-one">
                        <p class="btn1 pull-right"><?php echo $this->lang->line('join_as_a_partner_bellow'); ?></p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 anti tg-two">
                        <p class="btn2 pull-left"><?php echo $this->lang->line('join_as_a_user_bellow'); ?></p>
                    </div>
                </div>
            </div>
        </div>--> 
    <!--<div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl circle-one">
                <hr class="hrclass">
                <div class="circle active"><?php echo $this->lang->line('number_1'); ?></div>
                <p><?php echo $this->lang->line('personal_information'); ?></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl circle-two">
                <hr class="hrclass">
                <div class="circle"><?php echo $this->lang->line('number_2'); ?></div>
                <p><?php echo $this->lang->line('provisions'); ?></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl circle-three">
                <hr class="hrclass">
                <div class="circle"><?php echo $this->lang->line('number_3'); ?></div>
                <p><?php echo $this->lang->line('vehicle_information'); ?></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl circle-four">
                <div class="circle"><?php echo $this->lang->line('number_4'); ?></div>
                <p><?php echo $this->lang->line('documents'); ?></p>
            </div>
        </div>--> 
    <!--<div class="row toggledriver" style="margin-top:25px;">
            <div class="col-md-4 col-sm-6 col-xs-12"></div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="input-group" style="margin: 0 auto;">
                    <div id="radioBtn" class="btn-group">
                        <a class="btn btn-primary btn-sm active Btn-radio1" data-toggle="driver_type" data-title="Driver"> <?php echo $this->lang->line('driver'); ?></a>
                        <a class="btn btn-primary btn-sm notActive Btn-radio2" data-toggle="driver_type" data-title="Delivery Man"><?php echo $this->lang->line('delivery_man'); ?></a>
                    </div>
                    <center><span class="sem-text"><?php echo $this->lang->line('form_top_message'); ?></span></center>
                    <input type="hidden" name="driver_type" id="driver_type">
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12"></div>
        </div>--> 
    <!--<div class="row">--> 
    <!-- first form start --> 
    <!--<div class="col-lg-12">
                <div class="contact-form" id="form1">
					This is test1
                </div>
            </div>--> 
    <!-- first form end --> 
    <!-- second form start --> 
    <!--<div class="col-lg-12">
                <div class="contact-form1" id="form2" style="display:none;">
                    This is test2
                </div>
            </div>--> 
    <!-- sencond form end --> 
    
    <!--</div>--> 
  </div>
</section>
<!----> 

