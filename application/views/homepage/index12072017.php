﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Najez</title>
    <meta name="description" content="Najez">
    <meta name="keywords" content="Najez">
    <link rel="icon" href="<?php echo base_url('assets/home/'); ?>/img/favicon.ico" >
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300|Raleway:300,400,900,700italic,700,300,600">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/style.css">
     <link rel="stylesheet" href="<?php echo base_url('assets/home/'); ?>/css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/home/'); ?>/css/owl.theme.default.min.css">
      <link rel="stylesheet" href="<?php echo base_url('assets/home'); ?>/css/bootstrap-datepicker.css">
	 
    <!-- =======================================================
        Theme Name: Baker
        Theme URL: https://bootstrapmade.com/baker-free-onepage-bootstrap-theme/
        Author: BootstrapMade.com
        Author URL: https://bootstrapmade.com
    ======================================================= -->
  </head>
  <body>
  <?php //if($this->session->flashdata('flashdata')){ ?> 
<p class="msgpop" style="display:none;text-align: center;background: #ff9e15;padding: 20px;color: #fff; margin: 0;z-index: 99999;position: fixed;width:100%;">Thank you for submitting your details, We will keep you posted.</p>
<?php //}?>
    <div class="loader"></div>
    <div id="myDiv">
    <!--HEADER-->
    <div class="header">
      <div class="bg-color">
        <header id="main-header">
        
			<div class="row anti">
				<div class="col-lg-3 col-sm-3 col-xs-6 anti">
					<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/home/'); ?>/img/logo.png" class="img-responsive" alt="logo" style="position: relative;z-index: 9999;" /></a>
				</div>
				<div class="col-lg-7 col-sm-7">
				<nav class="navbar navbar-default navbar-fixed-top"> <!-- navbar-fixed-top -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
				 
				  <ul class="nav navbar-nav navbar-right">
					<li class=""><a href="#"><?php echo  $this->lang->line('home_menu'); ?></a></li>
					<li class=""><a href="#about-us"><?php echo  $this->lang->line('aboutus_mene'); ?></a></li>
					<li class=""><a href="#services"><?php echo  $this->lang->line('services_menu'); ?></a></li>
					<li class=""><a href="#download-app"><?php echo  $this->lang->line('downloadapp_menu'); ?></a></li>
					<li><a  class="main-active" href="#join-us"><?php echo  $this->lang->line('joinus_menu'); ?></a></li>
					<li class="dropdown countryDropdown hidden-xs">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a id="navIta" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="language"> <img id="" src="" alt="..." class="imgNavIta img-thumbnail icon-small">  <span class="lanNavIta">Arabic</span></a></li>
							<!--<li><a id="navDeu" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/urdu" class="language"> <img id="" src="" alt="..." class="imgNavDeu img-thumbnail icon-small">  <span class="lanNavDeu">Urdu</span></a></li>-->
							<!--<li><a id="navFra" href="#" class="language"><img id="imgNavFra" src="" alt="..." class="img-thumbnail icon-small">  <span id="lanNavFra">Francais</span></a></li>-->
							<li><a id="navEng" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="language"><img id="" src="" alt="..." class="imgNavEng img-thumbnail icon-small">  <span class="lanNavEng">English</span></a></li>
						</ul>
					</li>
				  </ul>
				
				</div>
				
				</nav>
				</div>
				<div class="col-lg-2 col-sm-2 visible-xs">
						<div id="navbar">
							<ul>
								<li class="dropdown countryDropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a id="navIta" href="#" class="language"> <img id="" src="" alt="..." class="imgNavIta img-thumbnail icon-small">  <span class="lanNavIta">Arabic</span></a></li>
										<!--<li><a id="navDeu" href="#" class="language"> <img id="" src="" alt="..." class="imgNavDeu img-thumbnail icon-small">  <span class="lanNavDeu">Urdu</span></a></li>-->
										<!--<li><a id="navFra" href="#" class="language"><img id="imgNavFra" src="" alt="..." class="img-thumbnail icon-small">  <span id="lanNavFra">Francais</span></a></li>-->
										<li><a id="navEng" href="#" class="language"><img id="" src="" alt="..." class="imgNavEng img-thumbnail icon-small">  <span class="lanNavEng">English</span></a></li>
									</ul>
								</li>
							</ul>
						</div> <!--/.navbar-collapse -->
				</div>
			</div>
        
        </header>
        <div class="wrapper">
			<div class="container">
				<div class="row">
					<div class="banner-info text-center wow fadeIn delay-05s col-lg-12">
						<h2 class="bnr-sub-title"><?php echo  $this->lang->line('welcome_message'); ?></h2>
						<p class="bnr-para"><?php echo  $this->lang->line('anywhere_anytime'); ?></p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-xs-12 text-center"><a href="#join-us"><button class="button1 head-btn"><?php echo  $this->lang->line('join_as_a_partner'); ?></button></a></div>
					<div class="col-lg-6 col-md-6 col-xs-12 text-center"><a href="#join-us"><button class="button2 head-btn"><?php echo  $this->lang->line('join_as_a_user'); ?></button></a></div>
					<div class="overlay-detail">
					</div>
				</div>
			  
			</div>
        </div>
      </div>
    </div>
    <!--/ HEADER-->

    <!--second-->
    <section id="about-us" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 text-center">
              <img src="<?php echo base_url('assets/home/'); ?>/img/mobile1.png" class="img-responsive" style="margin:0 auto;">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content">
			<p class="space-150"></p>
			   <p><?php echo  $this->lang->line('najez'); ?></p>
			   <h1><?php echo  $this->lang->line('content_heading_aboutus'); ?></h1>
			   <h5><?php echo  $this->lang->line('content_aboutus_paragraph1');?></h5>
			   <div class="more_about" style="display:none;padding:0 !important;margin:0 !important;">
				<h5><?php echo  $this->lang->line('content_aboutus_paragraph2');?></h5>
				<h5><?php echo  $this->lang->line('content_aboutus_paragraph3');?></h5>
			   </div>
			  
			   <p><button class="butn about_btn1"><?php echo  $this->lang->line('learn_more');?></button><button class="butn  about_btn2" style="display:none;"><?php echo  $this->lang->line('hide');?></button></p>
			</div>
		</div>
      </div>
    </section>
    <!---->

  
    </section>
    <!---->
    <!---->

     <!--third-->
    <section id="feature" class="section-padding wow fadeIn delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15"><?php echo  $this->lang->line('benefits');?></h2>
            <p class="sub-title pad-bt35"><i><?php echo  $this->lang->line('we_give_you_better_benefits');?></i></p>
          </div>
          <div class="col-sm-4 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/icon1.png">
                
                <p><?php echo  $this->lang->line('better_customer_satisfaction');?></p>
              </div>
              
            </div>
          </div>
          <div class="col-sm-4 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/quality-partner.png">
              <p><?php echo  $this->lang->line('easy_to_use_and_understand');?></p>
              </div>
            </div>
          </div>
          <div class="col-sm-4 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/incentive.png">
              <p><?php echo  $this->lang->line('incentives_at_its_best');?></p>
              </div>
            </div>
          </div>
        </div> 
      </div>
    </section>                  
    <!---->




    <!---->
    <section id="services" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title" style="color:#fff;"><?php echo  $this->lang->line('content_services_heading');?></h2>
            <p class="sub-title task "><i><?php echo  $this->lang->line('content_services_sub_title');?></i></p>
          </div>
        </div>
        <div class="row images">
           <div class="col-lg-4 content1">
            <div class="row row-margin">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3><?php echo  $this->lang->line('save_your_arrival');?></h3>
					<P><?php echo  $this->lang->line('save_your_arrival_and_departure');?></P>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 heart text-center">
				  <img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
			</div>
            <div class="row row-margin">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3><?php echo  $this->lang->line('right_vehicle_just_one_click_away');?></h3>
					<P><?php echo  $this->lang->line('you_can_choose_any_type_of_vehicle');?></P>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-right">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
			</div>
			<div class="row row-margin">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
					<h3><?php echo  $this->lang->line('analysing_your_captain');?></h3>
					<P><?php echo  $this->lang->line('you_can_have_a_look_at_your_captain');?></P>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
			</div>
            
          </div>  
          <div class="col-lg-4">
            
          </div>
          <div class="col-lg-4 content2">
            <div class="row row-margin">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3><?php echo  $this->lang->line('discounts_stunning_service');?></h3>
					<P><?php echo  $this->lang->line('we_always_strive_to_be_competitive');?></P>
				</div> 
			</div>
            <div class="row row-margin">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3><?php echo  $this->lang->line('tracking_the_routes');?></h3>
					<P><?php echo  $this->lang->line('you_can_have_a_look_at_your_captain');?></P>
				</div>
              
			</div>
			<div class="row row-margin">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 text-center heart anti-left">
					<img src="<?php echo base_url('assets/home/'); ?>/img/heart.png">
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 anti">
					<h3><?php echo  $this->lang->line('rate_feedback');?></h3>
					<P><?php echo  $this->lang->line('did_you_liked_the_ride_rate_it');?></P>
				</div>
              
			</div> 
            
        </div>
      </div>
    </section>
    <!---->
    <!---->
    <!---->
    <section id="portfolio1" class="section-padding wow fadeInUp delay-05s">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title"><?php echo  $this->lang->line('terms_of_use');?></h2>
            <p class="sub-title pad-bt15" style="display:none;"><i style="color:#7f7c7c;">Get the app flow here</i></p>
          </div>
       </div>
      </div><!--.container-->
      <div class="container-fluid">
        <div class="row">
            <div class="loop owl-carousel owl-theme"> 
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide8.jpg" alt="Owl Image1" class="img-responsive"></div>
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide1.jpg" class="img-responsive"></div>
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide2.jpg" class="img-responsive"></div>
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide3.jpg" class="img-responsive"></div>
            <div class="item"><img src="<?php echo base_url('assets/home/'); ?>/img/slide5.jpg" class="img-responsive"></div>
 
</div>
        </div>
      </div>
    </section>

    <!---->
    <!---->
    <section id="download-app" class=" wow fadeInUp delay-05s">
      <div class="bg-testicolor section-padding">
        <div class="container centered">
        <div class="row">
          <div class="testimonial-item col-lg-12">
               <h1><?php echo  $this->lang->line('download_the_app');?></h1>
                <p><i><?php echo  $this->lang->line('register_to_get_an_early_access');?></i></p>
          </div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
				<a href="#join-us"><button class="pull-right"><img src="<?php echo base_url('assets/home/'); ?>/img/android1.png"> <?php echo  $this->lang->line('android');?></button></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
				<a href="#join-us"><button class="pull-left"><img src="<?php echo base_url('assets/home/'); ?>/img/icon.png"> <?php echo  $this->lang->line('ios');?></button></a>
			</div>
        </div>
        </div>
      </div>
    </section>
    <!---->
    <section id="blog" class="section-padding wow fadeInUp delay-05s" style="display:none;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15"><?php echo  $this->lang->line('blog');?>BLOG</h2>
            <p class="sub-title pad-bt15"><i style="color:#7f7c7c;"><?php echo  $this->lang->line('have_look_our_blog');?>Have look our blog</i></p>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="blog-sec" style="background-color:#fafafa;">
              <div class="blog-img">
                <a href="">
                  <img src="<?php echo base_url('assets/home/'); ?>/img/car.jpg" class="img-responsive">
                </a>
              </div>
              <div class="blog-info">
                <h2><?php echo  $this->lang->line('quis_autem_bel_eum_iure_qui_in');?>QUIS AUTEM VEL EUM IURE QUI IN</h2>
                <p><?php echo  $this->lang->line('we_cannot_expect_people_to_have_respect_for');?>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
				<hr/>
                <a href="" class="read-more"><?php echo  $this->lang->line('dec112016_1');?>DEC 11, 2016</a>
                <a href="" class="read-more pull-right"> <?php echo  $this->lang->line('read_more');?>READ MORE</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="blog-sec" style="background-color:#fafafa;">
              <div class="blog-img">
                <a href="">
                  <img src="<?php echo base_url('assets/home/'); ?>/img/hand.jpg" class="img-responsive">
                </a>
              </div>
              <div class="blog-info">
                <h2><?php echo  $this->lang->line('quis_autem_vel_eum_iure_qui_in');?>QUIS AUTEM VEL EUM IURE QUI IN</h2>
                <p><?php echo  $this->lang->line('we_cannot_expect_people_to_have_respect');?>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
                <hr/>
				<a href="" class="read-more"><?php echo  $this->lang->line('dec112016_2');?>DEC 11, 2016</a>
                <a href="" class="read-more pull-right"><?php echo  $this->lang->line('read_more');?>READ MORE</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="blog-sec" style="background-color:#fafafa;">
              <div class="blog-img">
                <a href="">
                  <img src="<?php echo base_url('assets/home/'); ?>/img/car1.jpg" class="img-responsive">
                </a>
              </div>
              <div class="blog-info">
                <h2><?php echo  $this->lang->line('quis_autem_vel_eum_iure_qui_in');?>QUIS AUTEM VEL EUM IURE QUI IN</h2>
                <p><?php echo  $this->lang->line('we_cannot_expect_people_to_have_respect');?>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time. we always want to help people cordially.</p>
                <hr/>
				<a href="" class="read-more"><?php echo  $this->lang->line('dec112016_3');?>DEC 11, 2016</a>
                <a href="" class="read-more pull-right"><?php echo  $this->lang->line('read_more');?>READ MORE</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!---->
    <section id="join-us" class="section-padding wow fadeInUp delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center white">
            <h2 class="service-title pad-bt15" style="color:#fff; "><?php echo  $this->lang->line('joinus');?></h2>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 anti">
					<p class="btn1 pull-right"><?php echo  $this->lang->line('join_as_a_partner_bellow');?></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 anti">
					<p class="btn2 pull-left"><?php echo  $this->lang->line('join_as_a_user_bellow');?></p>
				</div>
			</div>
          </div>
		</div>
		<div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl">
            <hr class="hrclass">
             <div class="circle active"><?php echo  $this->lang->line('number_1');?></div>
             <p><?php echo  $this->lang->line('personal_information');?></p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl">
		   <hr class="hrclass">
             <div class="circle"><?php echo  $this->lang->line('number_2');?></div>
             <p><?php echo  $this->lang->line('provisions');?></p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl">
		   <hr class="hrclass">
             <div class="circle"><?php echo  $this->lang->line('number_3');?></div>
             <p><?php echo  $this->lang->line('vehicle_information');?></p>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center crcl">
             <div class="circle"><?php echo  $this->lang->line('number_4');?></div>
             <p><?php echo  $this->lang->line('documents');?></p>
          </div>
		</div>
		<div class="row toggledriver" style="margin-top:25px;">
			<div class="col-md-4 col-sm-6 col-xs-12"></div>
			
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="input-group" style="margin: 0 auto;">
					<div id="radioBtn" class="btn-group">
						<a class="btn btn-primary btn-sm active" data-toggle="driver_type" data-title="Driver"> <?php echo  $this->lang->line('driver');?>Driver</a>
						<a class="btn btn-primary btn-sm notActive" data-toggle="driver_type" data-title="Delivery Man"><?php echo  $this->lang->line('delivery_man');?>Delivery Man</a>
					</div>
					<input type="hidden" name="driver_type" id="driver_type">
				</div>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12"></div>
		</div>
		<div class="row">
          <!-- first form start -->
          <div class="col-lg-12">
            <div class="contact-form" id="form1">
				
            </div>
          </div>
          <!-- first form end -->
          <!-- second form start -->
          <div class="col-lg-12">
            <div class="contact-form1" id="form2" style="display:none;">
                
                <form action="<?php echo base_url('frontend_register/user_reg');?>" method="post" autocomplete="off" role="form" class="contactForm userform">
				<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
					<div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="user_fname" class="form-control" id="user_fname" placeholder="<?php echo $this->lang->line('first_name');?>" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed');?>Only alphabets allowed</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="user_lname" id="user_lname" placeholder="<?php echo $this->lang->line('last_name');?>" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed');?>Only alphabets allowed</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control checkemail_user" name="user_email" id="user_email" placeholder="<?php echo $this->lang->line('email_id');?>" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex_abcd12testcom');?>ex: abcd12@test.com</div>
							<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('emailid_already_registered');?>Emailid Already Registered</div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" name="user_mobile" maxlength="9" id="user_mobile" placeholder="<?php echo $this->lang->line('mobile_number');?>" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex1234567890');?>ex: 123456789</div>
                        </div>
                    </div>
					<div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <select class="form-control" name="user_gender" id="user_gender">
								<option value=""><?php echo $this->lang->line('gender');?></option>
								<option value="Male"><?php echo $this->lang->line('male');?></option>
								<option value="Female"><?php echo $this->lang->line('female');?></option>
							</select>
							<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
                        </div>
                    </div>
					<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<select class="form-control" name="user_city" id="user_city">
										<option value=""><?php echo $this->lang->line('city');?></option>
										<option value="Al-Abwa">Al-Abwa</option>
										<option value="Artaweeiyah">Al Artaweeiyah</option>
										<option value="Badr">Badr</option>
										<option value="Baljurashi">Baljurashi</option>
										<option value="Bisha">Bisha</option>
										<option value="Bareg">Bareg</option>
										<option value="Buraydah">Buraydah</option>
										<option value="Al Bahah">Al Bahah</option>
										<option value="Buq a">Buq a</option>
										<option value="Dammam">Dammam</option>
										<option value="Dhahran">Dhahran</option>
										<option value="Dhurma">Dhurma</option>
										<option value="Dahaban">Dahaban</option>
										<option value="Diriyah">Diriyah</option>
										<option value="Duba">Duba</option>
										<option value="Dumat Al-Jandal">Dumat Al-Jandal</option>
										<option value="Dawadmi">Dawadmi</option>
										<option value="Farasan city">Farasan city</option>
										<option value="Gatgat">Gatgat</option>
										<option value="Gerrha">Gerrha</option>
										<option value="Gurayat">Gurayat</option>
										<option value="Al-Gwei'iyyah">Al-Gwei'iyyah</option>
										<option value="Hautat Sudair">Hautat Sudair</option>
										<option value="Habala">Habala</option>
										<option value="Hajrah">Hajrah</option>
										<option value="Haql">Haql</option>
										<option value="Al-Hareeq">Al-Hareeq</option>
										<option value="Harmah">Harmah</option>
										<option value="Ha'il">Ha'il</option>
										<option value="Hotat Bani Tamim">Hotat Bani Tamim</option>
										<option value="Hofuf">Hofuf</option>
										<option value="Huraymila">Huraymila</option>
										<option value="Hafr Al-Batin">Hafr Al-Batin</option>
										<option value="Jabal Umm al Ru'us">Jabal Umm al Ru'us</option>
										<option value="Jalajil">Jalajil</option>
										<option value="Al Jawf">Al Jawf</option>
										<option value="Jeddah">Jeddah</option>
										<option value="Jizan">Jizan</option>
										<option value="Jizan Economic City">Jizan Economic City</option>
										<option value="Jubail">Jubail</option>
										<option value="Al Jafer">Al Jafer</option>
										<option value="Khafji">Khafji</option>
										<option value="Khaybar">Khaybar</option>
										<option value="King Abdullah Economic City">King Abdullah Economic City</option>
										<option value="Khamis Mushayt">Khamis Mushayt</option>
										<option value="Al Kharj">Al Kharj</option>
										<option value="Knowledge Economic City, Medina">Knowledge Economic City, Medina</option>
										<option value="Khobar">Khobar</option>
										<option value="Al-Khutt">Al-Khutt</option>
										<option value="Layla">Layla</option>
										<option value="Lihyan">Lihyan</option>
										<option value="Al Lith">Al Lith</option>
										<option value="Al Majma'ah">Al Majma'ah</option>
										<option value="Mastoorah">Mastoorah</option>
										<option value="Al Mikhwah">Al Mikhwah</option>
										<option value="Al-Mubarraz">Al-Mubarraz</option>
										<option value="Al Mawain">Al Mawain</option>
										<option value="Mecca">Mecca</option>
										<option value="Medina">Medina</option>
										<option value="Muzahmiyya">Muzahmiyya</option>
										<option value="Najran">Najran</option>
										<option value="Al-Namas">Al-Namas</option>
										<option value="Omloj">Omloj</option>
										<option value="Al-Omran">Al-Omran</option>
										<option value="Al-Oyoon">Al-Oyoon</option>
										<option value="Qadeimah">Qadeimah</option>
										<option value="Qatif">Qatif</option>
										<option value="Qaisumah">Qaisumah</option>
										<option value="Al Qunfudhah">Al Qunfudhah</option>
										<option value="Rabigh">Rabigh</option>
										<option value="Rafha">Rafha</option>
										<option value="Ar Rass">Ar Rass</option>
										<option value="Ras Tanura">Ras Tanura</option>
										<option value="Riyadh">Riyadh</option>
										<option value="Riyadh Al-Khabra">Riyadh Al-Khabra</option>
										<option value="Rumailah">Rumailah</option>
										<option value="Sabt Al Alaya">Sabt Al Alaya</option>
										<option value="Saihat">Saihat</option>
										<option value="Safwa city">Safwa city</option>
										<option value="Sakakah">Sakakah</option>
										<option value="Sharurah">Sharurah</option>
										<option value="Shaqraa">Shaqraa</option>
										<option value="Shaybah">Shaybah</option>
										<option value="As Sulayyil">As Sulayyil</option>
										<option value="Taif">Taif</option>
										<option value="Tabuk">Tabuk</option>
										<option value="Tanomah">Tanomah</option>
										<option value="Tarout">Tarout</option>
										<option value="Tayma">Tayma</option>
										<option value="Thadiq">Thadiq</option>
										<option value="Thuwal">Thuwal</option>
										<option value="Thuqbah">Thuqbah</option>
										<option value="Turaif">Turaif</option>
										<option value="Tabarjal">Tabarjal</option>
										<option value="Udhailiyah">Udhailiyah</option>
										<option value="Al-`Ula">Al-`Ula</option>
										<option value="Um Al-Sahek">Um Al-Sahek</option>
										<option value="Unaizah">Unaizah</option>
										<option value="Uqair">Uqair</option>
										<option value="Uyayna">Uyayna</option>
										<option value="Uyun AlJiwa">Uyun AlJiwa</option>
										<option value="Wadi Al-Dawasir">Wadi Al-Dawasir</option>
										<option value="Al Wajh">Al Wajh</option>
										<option value="Yanbu">Yanbu</option>
										<option value="Az Zaimah">Az Zaimah</option>
										<option value="Zulfi">Zulfi</option>
										<option value="Abqaiq">Abqaiq</option>
										<option value="Hassa">Hassa</option>
										<option value="Bahrain">Bahrain</option>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
                    </div>
					<div class="row" style="margin-top:0;">
						<div class="col-md-4 col-sm-12 col-xs-12"></div>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
								<input type="submit" class="button3" id="user_regs" name="user_reg" />
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12"></div>
                    </div>
                </form>
              
            </div>
          </div>
          <!-- sencond form end -->

          
        </div>
      </div>
    </section>
    <!---->


     <section id="feature1" class="section-padding wow fadeIn delay-05s">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="service-title pad-bt15"><?php echo $this->lang->line('get_in_touch');?>GET IN TOUCH</h2>
          </div>
		</div>
		<div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/phone.png">
              </div>
              <p><?php echo $this->lang->line('phone');?> : +966540988577</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/mssg.png">
              </div>
              <p><?php echo $this->lang->line('email');?> : <a href="mailto:info@najez-online.com" style="color:#333;"><?php echo $this->lang->line('infonajwz-onlinecom');?>info@najez-online.com</a></p>
            </div>
            
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12" style="display:none;">
            <div class="wrap-item text-center">
              <div class="item-img">
                <img src="<?php echo base_url('assets/home/'); ?>/img/web.png">
              </div>
              <p><?php echo $this->lang->line('web:wwwyoursitecom');?>Web : www.yoursite.com</p>
            </div>
          </div>
        </div>
		<div class="row" style="display:none;">
			<div class="col-lg-12 text-center">
				<div class="wrap-item text-center">
				  <div class="item-img">
					 <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/fb.jpg" style="margin-top: 50px;"></a>
					  <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter.jpg" style="margin-top: 50px;"></a>
					  <a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/google.jpg" style="margin-top: 50px;"></a>
				  </div>
				</div>
			</div>
		</div>
      </div>
    </section>   

<div class="partner_driver" style="display:none;">
	<form action="<?php echo base_url('frontend_register/partner_reg');?>" method="post" role="form" class="contactForm partnerForm" enctype="multipart/form-data" autocomplete="off" onsubmit="event.preventDefault(); formsubmitfunc();">
					<div class="step1">
						<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<input type="hidden" name="driver_type" id="driver_type">
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" name="fname" class="form-control" id="fname" placeholder="<?php echo $this->lang->line('first_name');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed');?>Only Alphabets Allowed</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="lname" id="lname" placeholder="<?php echo $this->lang->line('last_name');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed');?></div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control checkemail_driver" name="emailid" id="emailid" placeholder="<?php echo $this->lang->line('email_id');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex_abcd12testcom');?>ex: abcd12@test.com</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('emailid_already_registered');?>Emailid Already Registered</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="mobileno" id="mobileno" maxlength="9" placeholder="<?php echo $this->lang->line('mobile_number');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex1234567890');?>ex: 123456789</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="identy" id="identy" maxlength="10" placeholder="<?php echo $this->lang->line('nationalidiqama');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex1234567890');?>ex: 1234567890</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group" id="">
									<select class="form-control" name="city" id="city" >
										<option value=""><?php echo $this->lang->line('city');?></option>
										<option value="Riyadh"><?php echo $this->lang->line('riyadh');?></option>
										<option value="Jeddah"><?php echo $this->lang->line('jeddah');?></option>
										<option value="Dammam"><?php echo $this->lang->line('dammam');?></option>
										<option value="Khobar"><?php echo $this->lang->line('khobar');?></option>
										<option value="Jubail"><?php echo $this->lang->line('jubail');?></option>
										<option value="Hassa"><?php echo $this->lang->line('hassa');?></option>
										<option value="Dhahran"><?php echo $this->lang->line('dhahran');?></option>
										<option value="Qatif"><?php echo $this->lang->line('qatif');?></option>
										<option value="Kharj"><?php echo $this->lang->line('kharj');?></option>
										<option value="Qassim"><?php echo $this->lang->line('qassim');?></option>
										<option value="Khafji"><?php echo $this->lang->line('khafji');?></option>
										<option value="Hafar Al-Batin"><?php echo $this->lang->line('hafar_al_batin');?></option>
										<option value="Bahrain"><?php echo $this->lang->line('bahrain');?></option>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12 delfield" >
								<div class="form-group">
									<input type="text" id="date" name="date" class="form-control" data-date-end-date="0d" placeholder="<?php echo $this->lang->line('dobyyyymmdd');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('min_age21_years');?></div>
								</div>
							</div>
							
							
							<div class="col-md-4 col-sm-6 col-xs-12 delfield">
								<div class="form-group">
									<input type="text" class="form-control" name="code" id="code" placeholder="<?php echo $this->lang->line('captains_code_that_nominated_you');?>" />
								</div>
							</div>
							
						</div>
						<div class="row formshow" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="<?php echo $this->lang->line('next');?>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step2" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<textarea class="form-control" disabled id="tnc" rows="8" placeholder="Terms and conditions" style="color: #000 !important;" />
- <?php echo $this->lang->line('the_minimum_age_to_join_is_years');?>
- <?php echo $this->lang->line('car_model_should_be_year_and_above');?>
- <?php echo $this->lang->line('arabic_speaking_is_mandatory_condition');?>
									
- <?php echo $this->lang->line('for_non_saudis_you_must_apply_for_accommodation');?>
									</textarea>
									
								</div>
								<label style="color:#fff;"><?php echo $this->lang->line('declaration');?></label>
								<div class="form-group" style="color:#fff;">
									<input type="checkbox" class="" name="agree" id="agree" value="1" /> <span class="declbox form-group"><?php echo $this->lang->line('i_have_read_and_agree_to_the_terms_and_conditions');?></span>
									
								</div>
							</div>
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step0_btn" onclick="return step0_btn();" value="<?php echo $this->lang->line('back');?>" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="<?php echo $this->lang->line('next');?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step3" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000; text-align: center;padding-bottom: 10px;display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<input type="text" class="form-control" name="vehicle_sno" id="vehicle_sno" maxlength="100" placeholder="<?php echo $this->lang->line('serial_number_of_the_vehicle');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_a-z_0-9_allowed');?>Only A-Z 0-9 allowed</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12" style="margin-top:-25px;">
								<label style="color: #ddd;font-weight: normal;font-size: 12px;"><?php echo $this->lang->line('vehicle_plate_number_xyz1234');?></label>
								<div class="row">
								
									<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
										<div class="form-group">
											<input type="text" class="form-control" name="vehicle_plate_char" id="vehicle_plate_char" placeholder="<?php echo $this->lang->line('xyz');?>" maxlength="3" />
											<div class="validation" style="top:-42px;"><i style="position: absolute;bottom: -20px;left: 30px;" class="fa fa-caret-down" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
											<div class="validation" style="top:-42px;width: 175px;left: 7px;"><i style="position: absolute;bottom: -20px;left: 30px;" class="fa fa-caret-down" aria-hidden="true"></i> Only alphabets allowed</div>
										</div>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-12 col-sx-12">
										
											<div class="form-group">
												<input type="text" class="form-control" name="vehicle_plate_num" id="vehicle_plate_num" placeholder="<?php echo $this->lang->line('1234');?>" maxlength="4" />
												<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
												<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_numeric_allowed');?>Only numeric allowed</div>
											</div>
									
									</div>
								</div>
							</div>
							
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="vehicle_type" id="vehicle_type">
										<option value=""><?php echo $this->lang->line('type_of_the_vehicle');?></option>
										<!--<option value="Delivery">Delivery</option>-->
										<?php foreach($brand_list as $brand_list1):     ?>
										<optgroup label="<?=$brand_list1['car_brand']; ?>">
										<?php $ss = $this->model->get_car_list($brand_list1['car_brand']); ?>
										<?php foreach($ss as $ss):?>
										<option value="<?=  $ss['id']; ?>"><?=$ss['car_model'];  ?></option>							
										<?php endforeach; ?>
										</optgroup>
										<?php endforeach;    ?>
										</select>
										<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="car_model1">
									<input type="text" class="form-control" placeholder="<?php echo $this->lang->line('car_model');?>" id="car_model" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="vehicle_year_dropdown">
									<select class="form-control" name="vehicle_year" id="vehicle_year">
									<option value=""><?php echo $this->lang->line('year_of_manufacture');?></option>
									<?php for($starting_year = 2010; $starting_year <= date('Y'); $starting_year++) {
										echo '<option value="'.$starting_year.'">'.$starting_year.'</option>';
									}?>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<input type="text" class="form-control" name="vehicle_color" id="vehicle_color" placeholder="<?php echo $this->lang->line('color');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed');?>Only alphabets allowed</div>
								</div>
							</div>
							
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="<?php echo $this->lang->line('back');?>" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step3_btn" onclick="return step3_btn();" value="<?php echo $this->lang->line('next');?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step4" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label><?php echo $this->lang->line('photograph');?></label>
									<input type="file" class="form-control photo" name="photograph" id="photograph" placeholder="<?php echo $this->lang->line('photograph');?>" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label><?php echo $this->lang->line('residence_identity');?></label>
									<input type="file" class="form-control photo" name="residence" id="residence" placeholder="<?php echo $this->lang->line('residence_identity');?>" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label><?php echo $this->lang->line('driving_license');?>Driving License</label>
									<input type="file" class="form-control photo" name="driver_license" id="driver_license" placeholder="<?php echo $this->lang->line('driving_license');?>Driving License" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label><?php echo $this->lang->line('vehicle_registration');?></label>
									<input type="file" class="form-control photo" name="vehicle_reg" id="vehicle_reg" placeholder="<?php echo $this->lang->line('vehicle_registration');?>" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label><?php echo $this->lang->line('insurance_of_vehicles');?></label>
									<input type="file" class="form-control photo" name="vehicle_insur" id="vehicle_insur" placeholder="<?php echo $this->lang->line('insurance_of_vehicles');?>" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label><?php echo $this->lang->line('authorization_image');?></label>
									<input type="file" class="form-control photo" name="auth_img" id="auth_img" placeholder="<?php echo $this->lang->line('authorization_image');?>" />
									<div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label><?php echo $this->lang->line('tafweeth');?>Tafweeth</label>
									<input type="file" class="form-control photo" name="tafweeth_img" id="tafweeth_img" placeholder="<?php echo $this->lang->line('tafweeth_image');?>Tafweeth image" />
									
								</div>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
								<label>IBAN Number</label>
									<input type="text" class="form-control" name="iban" id="iban" maxlength="24" placeholder="<?php echo $this->lang->line('iban_number');?>" />
									<div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex_SA1234000001234567890000');?>ex: SA1234000001234567890000</div>
								</div>
									
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
								<label>Bank</label>
									<select class="form-control" name="iban_bank" id="iban_bank" >
										<option value=""><?php echo $this->lang->line('bank');?>Bank</option>
										<option value="The National Commercial Bank">The National Commercial Bank</option>
										<option value="The Saudi British Bank">The Saudi British Bank</option>
										<option value="Saudi Investment Bank">Saudi Investment Bank</option>
										<option value="Alinma Bank">Alinma Bank</option>
										<option value="Banque Saudi Fransi">Banque Saudi Fransi</option>
										<option value="Riyad Bank">Riyad Bank</option>
										<option value="Samba Financial Group (Samba)">Samba Financial Group (Samba)</option>
										<option value="Saudi Hollandi Bank (Alawwal)">Saudi Hollandi Bank (Alawwal)</option>
										<option value="Al Rajhi Bank">Al Rajhi Bank</option>
										<option value="Arab National Bank">Arab National Bank</option>
										<option value="Bank Al-Bilad">Bank Al-Bilad</option>
										<option value="Bank AlJazira">Bank AlJazira</option>
										<option value="Asia Bank">Asia Bank</option>
										<option value="Qatar National Bank (QNB)">Qatar National Bank (QNB)</option>
										<option value="State Bank of India (SBI)">State Bank of India (SBI)</option>
										<option value="Gulf International Bank (GIB)">Gulf International Bank (GIB)</option>
										<option value="Emirates NBD">Emirates NBD</option>
										<option value="National Bank of Bahrain (NBB)">National Bank of Bahrain (NBB)</option>
										<option value="National Bank of Kuwait (NBK)">National Bank of Kuwait (NBK)</option>
										<option value="Muscat Bank">Muscat Bank</option>
										<option value="Deutsche Bank">Deutsche Bank</option>
										<option value="BNP Paribas">BNP Paribas</option>
										<option value="J.P. Morgan Chase N.A">J.P. Morgan Chase N.A</option>
										<option value="National Bank of Pakistan (NBP)">National Bank of Pakistan (NBP)</option>
										<option value="T.C.ZIRAAT BANKASI A.S.">T.C.ZIRAAT BANKASI A.S.</option>
										<option value="Industrial and Commercial Bank of China (ICBC)">Industrial and Commercial Bank of China (ICBC)</option>
									</select>
									<div style="top:25px;"  class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
									
							</div>
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="<?php echo $this->lang->line('back');?>" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="submit" class="button3" name="partner_submit" id="partner_submit" value="<?php echo $this->lang->line('submit');?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
                </form>
</div>
<div class="partner_delivery" style="display:none;">
	<form action="<?php echo base_url('frontend_register/partner_reg');?>" method="post" role="form" class="contactForm partnerForm" enctype="multipart/form-data" autocomplete="off" onsubmit="event.preventDefault(); return formsubmitfunc(this);">
					<div class="step1">
						<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<input type="hidden" name="driver_type" id="driver_type">
						<div class="row formshow">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" name="fname" class="form-control" id="fname" placeholder="<?php echo $this->lang->line('first_name');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed');?>Only Alphabets Allowed</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="lname" id="lname" placeholder="<?php echo $this->lang->line('last_name');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed');?>Only Alphabets Allowed</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="identy" id="identy" maxlength="10" placeholder="<?php echo $this->lang->line('nationalidiqama');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex1234567890');?>ex: 1234567890</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control checkemail_driver" name="emailid" id="emailid" placeholder="<?php echo $this->lang->line('email_id');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('exabcd12@test.com');?>ex: abcd12@test.com</div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('emailid_already_registered');?>Emailid Already Registered</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="mobileno" id="mobileno" maxlength="9" placeholder="<?php echo $this->lang->line('mobile_number');?>" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex123456789');?>ex: 123456789</div>
								</div>
							</div>
							
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="form-group" id="">
									<select class="form-control" name="city" id="city" >
										<option value=""><?php echo $this->lang->line('city');?></option>
										<option value="Riyadh">Riyadh</option>
										<option value="Jeddah">Jeddah</option>
										<option value="Dammam">Dammam</option>
										<option value="Khobar">Khobar</option>
										<option value="Jubail">Jubail</option>
										<option value="Hassa">Hassa</option>
										<option value="Dhahran">Dhahran</option>
										<option value="Qatif">Qatif</option>
										<option value="Kharj">Kharj</option>
										<option value="Qassim">Qassim</option>
										<option value="Khafji">Khafji</option>
										<option value="Hafar Al-Batin">Hafar Al-Batin</option>
										<option value="Bahrain">Bahrain</option>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							
							
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="form-group">
									<input type="button" onclick="return step1_btn();" class="button3 step1_btn" value="<?php echo $this->lang->line('next');?>" />
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step2" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									
									<textarea class="form-control" disabled id="tnc" rows="8" placeholder="Terms and conditions" style="color: #000 !important;" />
- <?php echo $this->lang->line('the_minimum_age_to_join_is_years');?> 
-  <?php echo $this->lang->line('car_model_should_be_year_2000_and_above');?>
- <?php echo $this->lang->line('arabic_speaking_is_mandatory_condition');?>
									
- <?php echo $this->lang->line('for_non-saudis_you_must_apply_for_accommodation_on_a_special');?>
									</textarea>
								</div>
								<label style="color:#fff;">Declaration*</label>
								<div class="form-group" style="color:#fff;">
									<input type="checkbox" class="" name="agree" id="agree" value="1" /> <span class="declbox form-group"><?php echo $this->lang->line('i_have_read_and_agree_to_the_terms_and_conditions');?></span>
									
								</div>
							</div>
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step0_btn" onclick="return step0_btn();" value="<?php echo $this->lang->line('back');?>" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="<?php echo $this->lang->line('next');?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step3" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000; text-align: center;padding-bottom: 10px;display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<select class="form-control" name="vehicle_type" id="vehicle_type">
										<option value=""><?php echo $this->lang->line('type_of_the_vehicle');?></option>
										<!--<option value="Delivery">Delivery</option>-->
										<?php //print_r($brand_list);die(); ?>
										<?php foreach($brand_list as $brand_list2):     ?>
										<optgroup label="<?=$brand_list2['car_brand']; ?>">
										<?php $ss = $this->model->get_car_list($brand_list2['car_brand']); ?>
										<?php foreach($ss as $ss):?>
										<option value="<?=  $ss['id']; ?>"><?=$ss['car_model'];  ?></option>							
										<?php endforeach; ?>
										</optgroup>
										<?php endforeach;    ?>
										</select>
										<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
						
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="car_model1">
									<input type="text" class="form-control" placeholder="<?php echo $this->lang->line('car_model');?>" id="car_model" />
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group" id="vehicle_year_dropdown">
									<select class="form-control" name="vehicle_year" id="vehicle_year">
									<option value=""><?php echo $this->lang->line('year_of_manufacture');?></option>
									<?php for($starting_year = 2000; $starting_year <= date('Y'); $starting_year++) {
										echo '<option value="'.$starting_year.'">'.$starting_year.'</option>';
									}?>
									</select>
									<div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
								</div>
							</div>
							
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="<?php echo $this->lang->line('back');?>" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step3_btn" onclick="return step3_btn();" value="<?php echo $this->lang->line('next');?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
					<div class="step4" style="display:none;">
					<!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
						<div class="row">
							
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
								<label>IBAN Number</label>
									<input type="text" class="form-control" name="iban" id="iban" maxlength="24" placeholder="<?php echo $this->lang->line('iban_number');?>" />
									<div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field');?></div>
									<div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('exSA1234000001234567890000');?>ex: SA1234000001234567890000</div>
								</div>
									
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
								<div class="form-group">
									<label><?php echo $this->lang->line('tafweeth');?>Tafweeth</label>
									<input type="file" class="form-control photo" name="tafweeth_img" id="tafweeth_img" placeholder="<?php echo $this->lang->line('tafweeth_image');?>Tafweeth image" />
									
								</div>
							</div>
							
						</div>
						<div class="row" style="margin-top:0;">
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="<?php echo $this->lang->line('back');?>" />
										</div>
									</div>
									<div class="col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="submit" class="button3" name="partner_submit" id="partner_submit" value="<?php echo $this->lang->line('submit');?>" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12"></div>
						</div>
					</div>
                </form>
</div>
    <!---->
    <footer id="footer">
      <div class="container">
        <div class="row text-center" style="padding-top:40px;">
          <p>&copy; 2017 <?php echo $this->lang->line('najez');?>. <?php echo $this->lang->line('all_right_reserved');?> | <a href="<?php echo base_url('page/privacy-policy');?>"><?php echo $this->lang->line('privacy_policy');?></a></p>
          
        </div>
      </div>
    </footer>
    <!---->
  </div>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/wow.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/custom.js"></script>
    <!--<script src="contactform/contactform.js"></script>-->
    <script src="<?php echo base_url('assets/home'); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/home'); ?>/js/bootstrap-datepicker.js"></script>
	
    <script src="<?php echo base_url('assets/home'); ?>/js/form_validation.js"></script>

<!-- btn  -->
<style>
#radioBtn a {
    padding: 6px 34px;
    font-size: 15px;
}
#radioBtn .notActive{
    color: #ff9e15;
    background-color: #fff;
}
#radioBtn .active{
	color: #fff;
    background-color: #ff9e15;
    border-color: #ff9e15;
}
.more_about{padding:0 !important;padding:0 !important;}
</style>
<script>
var html = $('.partner_driver').html();
$('#form1').html(html);
$('#form1').find('#driver_type').prop('value', 'Driver');
jQuery('.form-group input, .form-group select').bind("focusout",function(){
		jQuery(this).removeClass('error');
		jQuery(this).next('.validation').hide();
		jQuery(this).next('.validation').next('.validation').hide();
		jQuery(this).next('.validation').next('.validation').next('.validation').hide();
	});
	var d = new Date();
	$('#form1').find('#date').datepicker({
		//format: 'yyyy-mm-dd',
		format: 'dd/mm/yyyy',
		startDate:'01/01/1940',
		autoclose: true
	});
	$("#vehicle_type").bind("change",function(){
		   $.ajax({
			url:'home/get_car_type/?id=',
		type:'POST',
		ContentType : 'application/json',
		beforeSend:function(){
		$('#form1').find("#car_model").empty();
	   },
	  data:{'id':$(this).val()},
	  success:function(data){ 
	  console.log(data);
	   var datas = JSON.parse(data);
	   //console.log(datas);
	 var option = '<select id="car_model" name="car_model" class="form-control">';
	   $.each(datas,function(i,val){
		console.log(val.car_type);
	   option += '<option value="'+val.car_type+'">'+val.car_type+'</option>';
	   
	   } );
	   option += '</select><div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Required Field</div>';
	   console.log(option);
	   $('#form1').find('#car_model1').html(option);
	  }, 
	  error:function(){
	   alert('Network Error Please Select Activity Again');
	   $('#form1').find("#select").val("");
	  }
	  });
	 });
$('#radioBtn a').on('click', function(){
	$('.formshow').show();
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    
    if(sel == 'Delivery Man'){
		var html = $('.partner_delivery').html();
		$('#form1').html(html);
		$('#form1').find('#'+tog).prop('value', sel);
		
	}else{
		var html = $('.partner_driver').html();
		$('#form1').html(html);
		$('#form1').find('#'+tog).prop('value', sel);
	}
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
	jQuery('.form-group input, .form-group select').bind("focusout",function(){
		jQuery(this).removeClass('error');
		jQuery(this).next('.validation').hide();
		jQuery(this).next('.validation').next('.validation').hide();
		jQuery(this).next('.validation').next('.validation').next('.validation').hide();
	});
	$("#vehicle_type").bind("change",function(){
		   $.ajax({
			url:'home/get_car_type/?id=',
		type:'POST',
		ContentType : 'application/json',
		beforeSend:function(){
		$('#form1').find("#car_model").empty();
	   },
	  data:{'id':$(this).val()},
	  success:function(data){ 
	  console.log(data);
	   var datas = JSON.parse(data);
	   //console.log(datas);
	 var option = '<select id="car_model" name="car_model" class="form-control">';
	   $.each(datas,function(i,val){
		console.log(val.car_type);
	   option += '<option value="'+val.car_type+'">'+val.car_type+'</option>';
	   
	   } );
	   option += '</select><div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Required Field</div>';
	   console.log(option);
	   $('#form1').find('#car_model1').html(option);
	  }, 
	  error:function(){
	   alert('Network Error Please Select Activity Again');
	   $('#form1').find("#select").val("");
	  }
	  });
	 });
	 $('#form1').find('#date').datepicker({
		//format: 'yyyy-mm-dd',
		format: 'dd/mm/yyyy',
		startDate:'01/01/1940',
		autoclose: true
	});
});

//function formsubmitfunc(test){ console.log(test);}


       </script>

  </body>
</html>

	