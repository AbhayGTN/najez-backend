
<!-- Footer start -->
<section id="feature1" class="section-padding wow fadeIn delay-05s">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="service-title pad-bt15"><?php echo $this->lang->line('get_in_touch'); ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="wrap-item text-center">
                    <div class="item-img">
                        <img src="<?php echo base_url('assets/home/'); ?>/img/phone.png">
                    </div>

<?php if (empty($this->session->userdata['site_lang'])) { ?> 
                      <p dir=ltr lang=ar><?php echo $this->lang->line('phone'); ?> :<span dir=ltr lang= en-us><?php echo $this->lang->line('+966540988577'); ?></span></p>
<?php } elseif ($this->session->userdata['site_lang'] == 'english') { ?> 
                      <p dir=ltr lang=ar><?php echo $this->lang->line('phone'); ?> :<span dir=ltr lang= en-us><?php echo $this->lang->line('+966540988577'); ?></span></p>	 
<?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?> 
                      <p dir=rtl lang=ar><?php echo $this->lang->line('phone'); ?> :<span dir=ltr lang= en-us><?php echo $this->lang->line('+966540988577'); ?></span></p>	 
<?php } ?>



                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="wrap-item text-center">
                    <div class="item-img">
                        <img src="<?php echo base_url('assets/home/'); ?>/img/mssg.png">
                    </div>
                    <p dir=rtl lang=ar><?php echo $this->lang->line('email'); ?> : <a href="mailto:info@najez-online.com" style="color:#333;"><?php echo $this->lang->line('infonajwz-onlinecom'); ?>info@najez-online.com</a></p>
                </div>

            </div>
            <div class="col-md-4 col-sm-4 col-xs-12" style="display:none;">
                <div class="wrap-item text-center">
                    <div class="item-img">
                        <img src="<?php echo base_url('assets/home/'); ?>/img/web.png">
                    </div>
                    <p><?php echo $this->lang->line('web:wwwyoursitecom'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="wrap-item text-center">
                    <div class="item-img">


<!--<a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/fb.jpg" style="margin-top: 50px;"></a>-->
<!--<a href="#"><img src="<?php echo base_url('assets/home/'); ?>/img/google.jpg" style="margin-top: 50px;"></a>-->


<?php if (empty($this->session->userdata['site_lang'])) { ?> 
                          <a href="https://twitter.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter.png" style="margin-top: 50px;"></a>
<?php } elseif ($this->session->userdata['site_lang'] == 'english') { ?> 
                          <a href="https://twitter.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter.png" style="margin-top: 50px;"></a>	 
<?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?> 
                          <a href="https://twitter.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/twitter-arabic.png" style="margin-top: 50px;"></a>	 
<?php } ?>


<?php if (empty($this->session->userdata['site_lang'])) { ?> 
                          <a href="https://instagram.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/instagram.png" style="margin-top: 50px;"></a>
<?php } elseif ($this->session->userdata['site_lang'] == 'english') { ?> 
                          <a href="https://instagram.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/instagram.png" style="margin-top: 50px;"></a>	 
<?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?> 
                          <a href="https://instagram.com/najez_ksa" target="_blank"><img src="<?php echo base_url('assets/home/'); ?>/img/insta2-arabic.png" style="margin-top: 50px;"></a>	 
<?php } ?>





                    </div>
                </div>
            </div>
        </div>
    </div>
</section>   


<!---->
<footer id="footer">
    <div class="container">
        <div class="row text-center" style="padding-top:40px;">
            <p><?php echo $this->lang->line('all_right_reserved'); ?> | <a href="<?php echo base_url('page/privacy-policy'); ?>"><?php echo $this->lang->line('privacy_policy'); ?></a></p>

        </div>
    </div>
</footer>
<!---->
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php echo base_url('assets/home'); ?>/js/jquery.js"></script>
<script src="<?php echo base_url('assets/home'); ?>/js/jquery.min.js"></script>
<script src="<?php echo base_url('assets/home'); ?>/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url('assets/home'); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/home'); ?>/js/wow.js"></script>
<script src="<?php echo base_url('assets/home'); ?>/js/jquery.bxslider.min.js"></script>
<script src="<?php echo base_url('assets/home'); ?>/js/custom.js"></script>
<!--<script src="contactform/contactform.js"></script>-->
<script src="<?php echo base_url('assets/home'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assets/home'); ?>/js/bootstrap-datepicker.js"></script>

<script src="<?php echo base_url('assets/home'); ?>/js/form_validation.js"></script>
<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

<!-- btn  -->
<style>
    #radioBtn a {
        padding: 6px 34px;
        font-size: 15px;
    }
    #radioBtn .notActive{
        color: #ff9e15;
        background-color: #fff;
    }
    #radioBtn .active{
        color: #fff;
        background-color: #ff9e15;
        border-color: #ff9e15;
    }
    .more_about{padding:0 !important;padding:0 !important;}
</style>
<script>
                                  var html = $('.partner_driver').html();
                                  $('#form1').html(html);
                                  $('#form1').find('#driver_type').prop('value', 'Driver');
                                  jQuery('.form-group input, .form-group select').bind("focusout", function () {
                                      jQuery(this).removeClass('error');
                                      jQuery(this).next('.validation').hide();
                                      jQuery(this).next('.validation').next('.validation').hide();
                                      jQuery(this).next('.validation').next('.validation').next('.validation').hide();
                                  });
                                  var d = new Date();
                                  $('#form1').find('#date').datepicker({
                                      //format: 'yyyy-mm-dd',
                                      format: 'dd/mm/yyyy',
                                      startDate: '01/01/1940',
                                      autoclose: true
                                  });
                                  $('#form1').find('#date_driverlicenseinput, #date_vehicleinsurinput, #date_tafweethimginput, #date_vehiclereginput').datepicker({
                                      //format: 'yyyy-mm-dd',
                                      format: 'dd/mm/yyyy',
                                      startDate: '0d',
                                      autoclose: true
                                  });
                                  $("#vehicle_type").bind("change", function () {
                                      $.ajax({
                                          url: 'home/get_car_type/?id=',
                                          type: 'POST',
                                          ContentType: 'application/json',
                                          beforeSend: function () {
                                              $('#form1').find("#car_model").empty();
                                          },
                                          data: {'id': $(this).val()},
                                          success: function (data) {
                                              console.log(data);
                                              var datas = JSON.parse(data);
                                              //console.log(datas);
                                              var option = '<select id="car_model" name="car_model" class="form-control" dir="<?php if($this->session->userdata('site_lang') == "arabic"){echo "rtl";}?>">';
                                              $.each(datas, function (i, val) {
                                                  console.log(val.car_type);
                                                  option += '<option value="' + val.car_type + '">' + val.car_type + '</option>';

                                              });
                                              option += '</select><div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Required Field</div>';
                                              console.log(option);
                                              $('#form1').find('#car_model1').html(option);
                                          },
                                          error: function () {
                                              alert('Network Error Please Select Activity Again');
                                              $('#form1').find("#select").val("");
                                          }
                                      });
                                  });
                                  $('.photo').bind('change', function (e) {

                                      var photo = e.target.files[0].name;
                                      var ext = photo.split('.').pop().toLowerCase();
                                      if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                                          alert('Invalid File. Use only gif, png, jpg, jpeg');
                                          $(this).val('');
                                          var id = $(this).attr('id');
                                          $('#form1').find('#' + id + '_date').hide();
                                      } else {
                                          var id = $(this).attr('id');
                                          $('#form1').find('#hidez').hide();
                                          $('#form1').find('#' + id + '_date').show();
                                      }

                                  });
                                  $('#radioBtn a').on('click', function () {
                                      $('.formshow').show();
                                      var sel = $(this).data('title');
                                      var tog = $(this).data('toggle');

                                      if (sel == 'Delivery Man') {
                                          var html = $('.partner_delivery').html();
                                          $('#form1').html(html);
                                          $('#form1').find('#' + tog).prop('value', sel);

                                      } else {
                                          var html = $('.partner_driver').html();
                                          $('#form1').html(html);
                                          $('#form1').find('#' + tog).prop('value', sel);
                                      }
                                      $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
                                      $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
                                      jQuery('.form-group input, .form-group select').bind("focusout", function () {
                                          jQuery(this).removeClass('error');
                                          jQuery(this).next('.validation').hide();
                                          jQuery(this).next('.validation').next('.validation').hide();
                                          jQuery(this).next('.validation').next('.validation').next('.validation').hide();
                                      });
                                      $("#vehicle_type").bind("change", function () {
                                          $.ajax({
                                              url: 'home/get_car_type/?id=',
                                              type: 'POST',
                                              ContentType: 'application/json',
                                              beforeSend: function () {
                                                  $('#form1').find("#car_model").empty();
                                              },
                                              data: {'id': $(this).val()},
                                              success: function (data) {
                                                  console.log(data);
                                                  var datas = JSON.parse(data);
                                                  //console.log(datas);
                                                  var option = '<select id="car_model" name="car_model" class="form-control" dir="<?php if($this->session->userdata('site_lang') == "arabic"){echo "rtl";}?>">';
                                                  $.each(datas, function (i, val) {
                                                      console.log(val.car_type);
                                                      option += '<option value="' + val.car_type + '">' + val.car_type + '</option>';

                                                  });
                                                  option += '</select><div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Required Field</div>';
                                                  console.log(option);
                                                  $('#form1').find('#car_model1').html(option);
                                              },
                                              error: function () {
                                                  alert('Network Error Please Select Activity Again');
                                                  $('#form1').find("#select").val("");
                                              }
                                          });
                                      });
                                      var d = new Date();
                                      $('#form1').find('#date').datepicker({
                                          //format: 'yyyy-mm-dd',
                                          format: 'dd/mm/yyyy',
                                          startDate: '01/01/1940',
                                          autoclose: true
                                      });
                                      $('#form1').find('#date_driverlicenseinput, #date_vehicleinsurinput, #date_tafweethimginput, #date_vehiclereginput').datepicker({
                                          //format: 'yyyy-mm-dd',
                                          format: 'dd/mm/yyyy',
                                          startDate: '0d',
                                          autoclose: true
                                      });

                                      $('.photo').bind('change', function (e) {

                                          var photo = e.target.files[0].name;
                                          var ext = photo.split('.').pop().toLowerCase();
                                          if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                                              alert('Invalid File. Use only gif, png, jpg, jpeg');
                                              $(this).val('');
                                              var id = $(this).attr('id');
                                              $('#form1').find('#' + id + '_date').hide();
                                          } else {
                                              var id = $(this).attr('id');
                                              $('#form1').find('#hidez').hide();
                                              $('#form1').find('#' + id + '_date').show();
                                          }

                                      });
                                  });

//function formsubmitfunc(test){ console.log(test);}


</script>

</body>
</html>