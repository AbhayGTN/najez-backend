 <div class="partner_driver" style="display:none;">

      <form action="<?php echo base_url('frontend_register/partner_reg'); ?>" method="post" role="form" class="contactForm partnerForm partner_form" enctype="multipart/form-data" autocomplete="off" onsubmit="event.preventDefault(); formsubmitfunc();">
          <div class="step1">

              <!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
              <input type="hidden" name="driver_type" id="driver_type">
              <div class="row">
                  <div class="col-md-4 col-sm-6 col-xs-12"><!--one-->
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" name="fname" class="form-control" id="fname" placeholder="<?php echo $this->lang->line('first_name'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                      </div>
                  </div>

                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" name="mname" class="form-control" id="mname" placeholder="<?php echo $this->lang->line('middle_name'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12"><!--two-->
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="lname" id="lname" placeholder="<?php echo $this->lang->line('last_name'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12"><!--three-->
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control checkemail_driver" name="emailid" id="emailid" placeholder="<?php echo $this->lang->line('email_id'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex_abcd12testcom'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('emailid_already_registered'); ?></div>
                      </div>
                  </div>

                  <div class="col-md-4 col-sm-6 col-xs-12"><!--four-->
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="mobileno" id="mobileno" maxlength="9" placeholder="<?php echo $this->lang->line('mobile_number'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex1234567890'); ?></div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12"><!--five-->
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="identy" id="identy" maxlength="10" placeholder="<?php echo $this->lang->line('nationalidiqama'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex1234567890'); ?></div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12"><!--six-->
                      <div class="form-group" id="">
                          <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
                            <i class="mandatestar">*</i><select class="form-control" name="city" id="city" >
                                <option value=""><?php echo $this->lang->line('city'); ?></option>
                                <option value="Bahrain"><?php echo $this->lang->line('bahrain'); ?></option>
                                <option value="Dammam"><?php echo $this->lang->line('dammam'); ?></option>
                                <option value="Dhahran"><?php echo $this->lang->line('dhahran'); ?></option>
                                <option value="Hassa"><?php echo $this->lang->line('hassa'); ?></option>
                                <option value="Hafar Al-Batin"><?php echo $this->lang->line('hafar_al_batin'); ?></option>
                                <option value="Jeddah"><?php echo $this->lang->line('jeddah'); ?></option>
                                <option value="Jubail"><?php echo $this->lang->line('jubail'); ?></option>
                                <option value="Kharj"><?php echo $this->lang->line('kharj'); ?></option>
                                <option value="Khobar"><?php echo $this->lang->line('khobar'); ?></option>
                                <option value="Khafji"><?php echo $this->lang->line('khafji'); ?></option>
                                <option value="Qatif"><?php echo $this->lang->line('qatif'); ?></option>
                                <option value="Qassim"><?php echo $this->lang->line('qassim'); ?></option>
                                <option value="Riyadh"><?php echo $this->lang->line('riyadh'); ?></option>
                                
                                
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?> 
                            <i class="mandatestar">*</i><select class="form-control" name="city" id="city" dir="rtl">
                                <option value=""><?php echo $this->lang->line('city'); ?></option>
                                <option value="<?php echo $this->lang->line('riyadh'); ?>"><?php echo $this->lang->line('riyadh'); ?></option>
                                <option value="<?php echo $this->lang->line('jeddah'); ?>"><?php echo $this->lang->line('jeddah'); ?></option>
                                <option value="<?php echo $this->lang->line('dammam'); ?>"><?php echo $this->lang->line('dammam'); ?></option>
                                <option value="<?php echo $this->lang->line('khobar'); ?>"><?php echo $this->lang->line('khobar'); ?></option>
                                <option value="<?php echo $this->lang->line('jubail'); ?>"><?php echo $this->lang->line('jubail'); ?></option>
                                <option value="<?php echo $this->lang->line('hassa'); ?>"><?php echo $this->lang->line('hassa'); ?></option>
                                <option value="<?php echo $this->lang->line('dhahran'); ?>"><?php echo $this->lang->line('dhahran'); ?></option>
                                <option value="<?php echo $this->lang->line('qatif'); ?>"><?php echo $this->lang->line('qatif'); ?></option>
                                <option value="<?php echo $this->lang->line('kharj'); ?>"><?php echo $this->lang->line('kharj'); ?></option>
                                <option value="<?php echo $this->lang->line('qassim'); ?>"><?php echo $this->lang->line('qassim'); ?></option>
                                <option value="<?php echo $this->lang->line('khafji'); ?>"><?php echo $this->lang->line('khafji'); ?></option>
                                <option value="<?php echo $this->lang->line('hafar_al_batin'); ?>"><?php echo $this->lang->line('hafar_al_batin'); ?></option>
                                <option value="<?php echo $this->lang->line('bahrain'); ?>"><?php echo $this->lang->line('bahrain'); ?></option>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>

                          <?php } ?>

                      </div>
                  </div>

                  <div class="col-md-4 col-sm-6 col-xs-12 delfield" ><!--Seven-->
                      <div class="form-group">
                          <i class="mandatestar">*</i>
                          <div class="row">
                              <div class="col-md-2 col-sm-2 col-xs-12"><label>DOB</label></div>
                              <div class="col-md-3 col-sm-3 col-xs-12">
                                  <select name="dob_dd" id="dob_dd" class="form-control">
                                          <option value="">DD</option>
                                              <?php for ($i = 1; $i < 32; $i++) {
                                               if($i < 10)
                                               {
                                                 echo "<option value='" . $i . "'>" ."0". $i . "</option>";
                                               }
                                                else {
                                                  echo "<option value='" . $i . "'>" . $i . "</option>";
                                                }
                                              
                                                
                                                
                                              } ?>
                                  </select>
                                  <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                                  <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('min_age21_years'); ?></div>
                              </div>
                              <div class="col-md-4 col-sm-3 col-xs-12">
                                  <select class="form-control" name="dob_mm" id="dob_mm">
                                      <option value="">MM</option>
                                      <?php
                                      for ($m = 1; $m <= 12; $m++) {
                                        $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                        if($m < 10)
                                        {
                                          echo "<option value='" . $m . "'>" . $month . "</option>";
                                        }
                                        else{
                                          echo "<option value='" . $m . "'>" . $month . "</option>";
                                        }
                                      }
                                      ?>
                                  </select>
                                  <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                              </div>
                              <div class="col-md-3 col-sm-4 col-xs-12">
                                  <select name="dob_yy" id="dob_yy" class="form-control">
                                      <option value="">YYYY</option>
                                      <?php
                                      $starting_year = date('Y', strtotime('-80 year'));
                                      $ending_year = date('Y');
                                      $current_year = date('Y');
                                      for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
                                        echo '<option value="' . $starting_year . '"';
                                        echo ' >' . $starting_year . '</option>';
                                      }?>
                                      </select>
                                  <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                              </div>
                          </div>
                          <!-- <input type="text" id="date" name="date" class="form-control" data-date-end-date="0d" placeholder="<?php echo $this->lang->line('dobyyyymmdd'); ?>" /> -->
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('min_age21_years'); ?></div>
                      </div>
                  </div>


                  <div class="col-md-4 col-sm-6 col-xs-12 delfield"><!--eight-->
                      <div class="form-group">
                          <input type="text" class="form-control" name="code" id="code" placeholder="<?php echo $this->lang->line('captains_code_that_nominated_you'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> Invalid Code</div>
                      </div>
                  </div>

              </div>
              <div class="row formshow" style="margin-top:0;">
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="<?php echo $this->lang->line('next'); ?>" />
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
              </div>
          </div>
          <div class="step2" style="display:none;">
              <!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
              <div class="row">
                  <div class="col-lg-12">
                      <div class="form-group">

  <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?> 
                            <textarea class="form-control" disabled id="tnc" rows="8" placeholder="Terms and conditions" style="color: #000 !important;" />- <?php echo $this->lang->line('the_minimum_age_to_join_is_years'); ?> 
- <?php echo $this->lang->line('car_model_should_be_year_and_above'); ?> 
- <?php echo $this->lang->line('arabic_speaking_is_mandatory_condition'); ?> 
- <?php echo $this->lang->line('for_non_saudis_you_must_apply_for_accommodation'); ?>
</textarea>

  <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
                            <textarea class="form-control" disabled id="tnc" dir="rtl" rows="8" placeholder="Terms and conditions" style="color: #000 !important;" />
- <?php echo $this->lang->line('the_minimum_age_to_join_is_years'); ?> 
- <?php echo $this->lang->line('car_model_should_be_year_and_above'); ?> 
- <?php echo $this->lang->line('arabic_speaking_is_mandatory_condition'); ?> 
- <?php echo $this->lang->line('for_non_saudis_you_must_apply_for_accommodation'); ?>
                            </textarea>
  <?php } ?>

                      </div>
                      <label style="color:#fff;"><?php echo $this->lang->line('declaration'); ?><span style="color:red">*</span> </label>
                      <div class="form-group" style="color:#fff;">
                          <div class="declbox form-group"><input type="checkbox" class="checkboxhome" name="agree" id="agree" value="1" /><?php echo $this->lang->line('i_have_read_and_agree_to_the_terms_and_conditions'); ?></div>

                      </div>
                  </div>
              </div>
              <div class="row" style="margin-top:0;">
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="row">
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step0_btn" onclick="return step0_btn();" value="<?php echo $this->lang->line('back'); ?>" />
                              </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="<?php echo $this->lang->line('next'); ?>" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
              </div>
          </div>
          <div class="step3" style="display:none;">
              <!--<div class="field_val" style="color:#ff0000; text-align: center;padding-bottom: 10px;display:none;">All heighlighted fields are mandatory</div>-->
              <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="vehicle_sno" id="vehicle_sno" maxlength="100" placeholder="<?php echo $this->lang->line('serial_number_of_the_vehicle'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_a-z_0-9_allowed'); ?></div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12" style="margin-top:-25px;">
                      <label style="color: #ddd;font-weight: normal;font-size: 12px;"><?php echo $this->lang->line('vehicle_plate_number_xyz1234'); ?></label>


  <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?> 
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                                <div class="form-group">
                                    <i class="mandatestar">*</i><input type="text" class="form-control tcl-custom-1" name="vehicle_plate_char" id="vehicle_plate_char" placeholder="<?php echo $this->lang->line('xyz'); ?>" maxlength="3" />
                                    <div class="validation" style="top:-42px;"><i style="position: absolute;bottom: -20px;left: 30px;" class="fa fa-caret-down" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                                    <div class="validation" style="top:-42px;width: 175px;left: 7px;"><i style="position: absolute;bottom: -20px;left: 30px;" class="fa fa-caret-down" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-sx-12">

                                <div class="form-group">
                                    <i class="mandatestar">*</i><input type="text" class="form-control" name="vehicle_plate_num" id="vehicle_plate_num" placeholder="<?php echo $this->lang->line('placeholder1234'); ?>" maxlength="4" />
                                    <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                                    <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_numeric_allowed'); ?></div>
                                </div>
                            </div>
                        </div> 
  <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>

                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-12 col-sx-12">
                                <div class="form-group">
                                    <i class="mandatestar">*</i><input type="text" class="form-control" name="vehicle_plate_num" id="vehicle_plate_num" placeholder="<?php echo $this->lang->line('placeholder1234'); ?>" maxlength="4" />
                                    <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                                    <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_numeric_allowed'); ?></div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                                <div class="form-group">
                                    <i class="mandatestar">*</i><input type="text" class="form-control tcl-custom-1" name="vehicle_plate_char" id="vehicle_plate_char" placeholder="<?php echo $this->lang->line('xyz'); ?>" maxlength="3" />
                                    <div class="validation" style="top:-42px;"><i style="position: absolute;bottom: -20px;left: 30px;" class="fa fa-caret-down" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                                    <div class="validation" style="top:-42px;width: 175px;left: 7px;"><i style="position: absolute;bottom: -20px;left: 30px;" class="fa fa-caret-down" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                                </div>
                            </div>
                        </div>
  <?php } ?>


                  </div>


                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">

                                  <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
                            <i class="mandatestar">*</i><select class="form-control" name="vehicle_type" id="vehicle_type">
                                <option value=""><?php echo $this->lang->line('type_of_the_vehicle'); ?></option>
                                <!--<option value="Delivery">Delivery</option>-->
                                    <?php foreach ($brand_list as $brand_list1): ?>
                                  <optgroup label="<?= $brand_list1['car_brand']; ?>">
                                  <?php $ss = $this->model->get_car_list($brand_list1['car_brand']); ?>
      <?php foreach ($ss as $ss): ?>
                                        <option value="<?= $ss['id']; ?>"><?= $ss['car_model']; ?></option>							
                              <?php endforeach; ?>
                                  </optgroup>
    <?php endforeach; ?>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                                  <?php }elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
                            <i class="mandatestar">*</i><select class="form-control" name="vehicle_type" id="vehicle_type" dir="rtl">
                                <option value=""><?php echo $this->lang->line('type_of_the_vehicle'); ?></option>
                                <!--<option value="Delivery">Delivery</option>-->
                                    <?php foreach ($brand_list as $brand_list1): ?>
                                  <optgroup label="<?= $brand_list1['car_brand']; ?>">
                                  <?php $ss = $this->model->get_car_list($brand_list1['car_brand']); ?>
      <?php foreach ($ss as $ss): ?>
                                        <option value="<?= $ss['id']; ?>"><?= $ss['car_model']; ?></option>							
                              <?php endforeach; ?>
                                  </optgroup>
    <?php endforeach; ?>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>

              <?php } ?>

                      </div>
                  </div>
              </div>
  <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?> 
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                        <div class="form-group" id="car_model1">
                            <i class="mandatestar">*</i><input type="text" class="form-control" placeholder="<?php echo $this->lang->line('car_model'); ?>" id="car_model" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                        <div class="form-group" id="vehicle_year_dropdown">

                            
                              <i class="mandatestar">*</i><select class="form-control" name="vehicle_year" id="vehicle_year">
                                  <option value=""><?php echo $this->lang->line('year_of_manufacture'); ?></option>
                              <?php
                              for ($starting_year = 2010; $starting_year <= date('Y'); $starting_year++) {
                                echo '<option value="' . $starting_year . '">' . $starting_year . '</option>';
                              }
                              ?>
                              </select>
                              <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
    


                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                        <div class="form-group">
                            <i class="mandatestar">*</i><input type="text" class="form-control tcl-custom-1" name="vehicle_color" id="vehicle_color" placeholder="<?php echo $this->lang->line('color'); ?>" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                        </div>
                    </div>	
                </div>
  <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                        <div class="form-group">
                            <i class="mandatestar">*</i><input type="text" class="form-control tcl-custom-1" name="vehicle_color" id="vehicle_color" placeholder="<?php echo $this->lang->line('color'); ?>" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                        </div>
                    </div>	
                    <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                        
                        <div class="form-group" id="vehicle_year_dropdown">


                              <i class="mandatestar">*</i><select class="form-control" name="vehicle_year" id="vehicle_year">
                                  <option value=""><?php echo $this->lang->line('year_of_manufacture'); ?></option>
                                  <?php
                                  for ($starting_year = 2010; $starting_year <= date('Y'); $starting_year++) {
                                    echo '<option value="' . $starting_year . '">' . $starting_year . '</option>';
                                  }
                                  ?>
                              </select>
                              <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
    


                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                        <div class="form-group" id="car_model1">
                            <i class="mandatestar">*</i><input type="text" class="form-control" placeholder="<?php echo $this->lang->line('car_model'); ?>" id="car_model" />
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                        </div>
                    </div>
                </div>


  <?php } ?>


              <div class="row" style="margin-top:0;">
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="row">
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="<?php echo $this->lang->line('back'); ?>" />
                              </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step3_btn" onclick="return step3_btn();" value="<?php echo $this->lang->line('next'); ?>" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
              </div>
          </div>
          <div class="step4" style="display:none;">
              <!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
              <div class="row">

                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">								
                      <div class="form-group">
                          <label><?php echo $this->lang->line('photograph'); ?></label>
                          <i class="mandatestar">*</i>
                          <input type="file" class="form-control photo" name="photograph" id="photograph" placeholder="<?php echo $this->lang->line('photograph'); ?>" />
                          <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                      </div>
                  </div>


                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('residence_identity'); ?></label>
                          <i class="mandatestar">*</i><input type="file" class="form-control photo" name="residence" id="residence" placeholder="<?php echo $this->lang->line('residence_identity'); ?>" />
                          <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('driving_license'); ?></label>
                          <i class="mandatestar">*</i><input type="file" class="form-control photo" name="driver_license" id="driver_license" placeholder="<?php echo $this->lang->line('driving_license'); ?>" />

                          <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                      </div>
                  </div>

                 <!-- <div class="col-md-4 col-sm-12 col-xs-12 delfield" id="driver_license_date" style="display:none;"> 
                      
<div class="form-group">
                      <div class="row">
                          <p style="margin:0 0 0 15px;"><label><?php echo $this->lang->line('expirationdate_driver_license'); ?></label></p>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                               <i class="mandatestar">*</i> <select name="driverlic_dd" id="driverlic_dd" class="form-control">
                                          <option value="">DD</option>
                                              <?php for ($i = 1; $i < 32; $i++) {
                                               if($i < 10)
                                               {
                                                 echo "<option value='" . $i . "'>" ."0". $i . "</option>";
                                               }
                                                else {
                                                  echo "<option value='" . $i . "'>" . $i . "</option>";
                                                }
                                              
                                                
                                                
                                              } ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                                  <div style="top:25px;" class="validation expired"><i class="fa fa-caret-left" aria-hidden="true"></i> The entered date has expired already</div>
                             

                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                              
                                
                                  <i class="mandatestar">*</i> <select class="form-control" name="driverlic_mm" id="driverlic_mm">
                                      <option value="">MM</option>
                                      <?php
                                      for ($m = 1; $m <= 12; $m++) {
                                        $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                        echo "<option value='" . $m . "'>" . $month . "</option>";
                                      }
                                      ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                              
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                              
                                  
                                  <i class="mandatestar">*</i> <select name="driverlic_yy" id="driverlic_yy" class="form-control">
                                      <option value="">YYYY</option>
                                      <?php
                                      $ending_year = date('Y', strtotime('+80 year'));
                                      $starting_year = date('Y');
                                      $current_year = date('Y');
                                      for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
                                        echo '<option value="' . $starting_year . '"';
                                        echo ' >' . $starting_year . '</option>';
                                      }?>
                                      </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                            
                          </div>
                      </div>
</div> 









                  </div>-->


                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('vehicle_registration'); ?></label>
                          <i class="mandatestar">*</i><input type="file" class="form-control photo" name="vehicle_reg" id="vehicle_reg" placeholder="<?php echo $this->lang->line('vehicle_registration'); ?>" />

                          <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                      </div>
                  </div>

                  <!--<div class="col-md-4 col-sm-12 col-xs-12 delfield" id="vehicle_reg_date" style="display:none;"> 
                     
                     <div class="form-group">
                      <div class="row">
                          <p style="margin:0 0 0 15px;"><label>Vehicle registration expiration date</label></p>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                               <i class="mandatestar">*</i> <select name="vehiclereg_dd" id="vehiclereg_dd" class="form-control">
                                          <option value="">DD</option>
                                              <?php for ($i = 1; $i < 32; $i++) {
                                               if($i < 10)
                                               {
                                                 echo "<option value='" . $i . "'>" ."0". $i . "</option>";
                                               }
                                                else {
                                                  echo "<option value='" . $i . "'>" . $i . "</option>";
                                                }
                                              
                                                
                                                
                                              } ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                             <div style="top:25px;" class="validation expired"><i class="fa fa-caret-left" aria-hidden="true"></i> The entered date has expired already</div>

                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12"> 
                              
                                
                                  <i class="mandatestar">*</i> <select class="form-control" name="vehiclereg_mm" id="vehiclereg_mm">
                                      <option value="">MM</option>
                                      <?php
                                      for ($m = 1; $m <= 12; $m++) {
                                        $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                        echo "<option value='" . $m . "'>" . $month . "</option>";
                                      }
                                      ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                              
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                              
                                  
                                  <i class="mandatestar">*</i> <select name="vehiclereg_yy" id="vehiclereg_yy" class="form-control">
                                      <option value="">YYYY</option>
                                      <?php
                                      $ending_year = date('Y', strtotime('+80 year'));
                                      $starting_year = date('Y');
                                      $current_year = date('Y');
                                      for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
                                        echo '<option value="' . $starting_year . '"';
                                        echo ' >' . $starting_year . '</option>';
                                      }?>
                                      </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                            
                          </div>
                      </div>
</div>
                     
                     
                  </div>-->

                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('insurance_of_vehicles'); ?></label>
                          <i class="mandatestar">*</i><input type="file" class="form-control photo" name="vehicle_insur" id="vehicle_insur" placeholder="<?php echo $this->lang->line('insurance_of_vehicles'); ?>" />

                          <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                      </div>
                  </div>

                  <!--<div class="col-md-4 col-sm-12 col-xs-12 delfield" id="vehicle_insur_date" style="display:none;"> 
                     
                     <div class="form-group">
                      <div class="row">
                          <p style="margin:0 0 0 15px;"><label><?php echo $this->lang->line('expirationdate_insurance_of_vehicles'); ?></label></p>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                               <i class="mandatestar">*</i> <select name="vehicleins_dd" id="vehicleins_dd" class="form-control">
                                          <option value="">DD</option>
                                              <?php for ($i = 1; $i < 32; $i++) {
                                               if($i < 10)
                                               {
                                                 echo "<option value='" . $i . "'>" ."0". $i . "</option>";
                                               }
                                                else {
                                                  echo "<option value='" . $i . "'>" . $i . "</option>";
                                                }
                                              
                                                
                                                
                                              } ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                             <div style="top:25px;" class="validation expired"><i class="fa fa-caret-left" aria-hidden="true"></i> The entered date has expired already</div>

                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                              
                                
                                  <i class="mandatestar">*</i> <select class="form-control" name="vehicleins_mm" id="vehicleins_mm">
                                      <option value="">MM</option>
                                      <?php
                                      for ($m = 1; $m <= 12; $m++) {
                                        $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                        echo "<option value='" . $m . "'>" . $month . "</option>";
                                      }
                                      ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                              
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                              
                                  
                                  <i class="mandatestar">*</i> <select name="vehicleins_yy" id="vehicleins_yy" class="form-control">
                                      <option value="">YYYY</option>
                                      <?php
                                      $ending_year = date('Y', strtotime('+80 year'));
                                      $starting_year = date('Y');
                                      $current_year = date('Y');
                                      for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
                                        echo '<option value="' . $starting_year . '"';
                                        echo ' >' . $starting_year . '</option>';
                                      }?>
                                      </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                            
                          </div>
                      </div>
</div>
                     
                     
                  </div>-->

                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('authorization_image'); ?></label>
                          <i class="mandatestar">*</i><input type="file" class="form-control photo" name="auth_img" id="auth_img" placeholder="<?php echo $this->lang->line('authorization_image'); ?>" />

                          <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('tafweeth'); ?></label>
                          <input type="file" class="form-control photo" name="tafweeth_img" id="tafweeth_img" placeholder="<?php echo $this->lang->line('tafweeth_image'); ?>" />									

                      </div>
                  </div>

                  <!--<div class="col-md-4 col-sm-12 col-xs-12 delfield" id="tafweeth_img_date" style="display:none;"> 
                      <div class="form-group">
                      <div class="row">
                          <p style="margin:0 0 0 15px;"><label><?php echo $this->lang->line('expirationdate_tafweeth'); ?></label></p>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                               <i class="mandatestar">*</i> <select name="tafw_dd" id="tafw_dd" class="form-control">
                                          <option value="">DD</option>
                                              <?php for ($i = 1; $i < 32; $i++) {
                                               if($i < 10)
                                               {
                                                 echo "<option value='" . $i . "'>" ."0". $i . "</option>";
                                               }
                                                else {
                                                  echo "<option value='" . $i . "'>" . $i . "</option>";
                                                }
                                              
                                                
                                                
                                              } ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                             <div style="top:25px;" class="validation expired"><i class="fa fa-caret-left" aria-hidden="true"></i> The entered date has expired already</div>

                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                              
                                
                                  <i class="mandatestar">*</i> <select class="form-control" name="tafw_mm" id="tafw_mm">
                                      <option value="">MM</option>
                                      <?php
                                      for ($m = 1; $m <= 12; $m++) {
                                        $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                        echo "<option value='" . $m . "'>" . $month . "</option>";
                                      }
                                      ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                              
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                              <i class="mandatestar">*</i> <select name="tafw_yy" id="tafw_yy" class="form-control">
                                      <option value="">YYYY</option>
                                      <?php
                                      $ending_year = date('Y', strtotime('+80 year'));
                                      $starting_year = date('Y');
                                      $current_year = date('Y');
                                      for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
                                        echo '<option value="' . $starting_year . '"';
                                        echo ' >' . $starting_year . '</option>';
                                      }?>
                                      </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                            
                          </div>
                      </div>
</div>
                     
                     
                     
                     
                  
                  </div>-->

                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('iban_number'); ?></label>
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="iban" id="iban" maxlength="24" placeholder="<?php echo $this->lang->line('iban_number'); ?>" />
                          <div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex_SA1234000001234567890000'); ?></div>
                      </div>

                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('bank'); ?></label>

  
                            <i class="mandatestar">*</i><select class="form-control" name="iban_bank" id="iban_bank" >
                                <option value=""><?php echo $this->lang->line('bank'); ?></option>
                                <option value="Alinma Bank"><?php echo $this->lang->line('alinma_bank'); ?></option>
<option value="Al Rajhi Bank"><?php echo $this->lang->line('al_rajhi_bank'); ?></option>
<option value="Arab National Bank"><?php echo $this->lang->line('arab_national_bank'); ?></option>
<option value="Asia Bank"><?php echo $this->lang->line('asia_bank'); ?></option>
<option value="Banque Saudi Fransi"><?php echo $this->lang->line('banque_saudi_fransi'); ?></option>
<option value="Bank Al-Bilad"><?php echo $this->lang->line('bank_al_bilad'); ?></option>
<option value="Bank AlJazira"><?php echo $this->lang->line('bank_alJazira'); ?></option>
<option value="BNP Paribas"><?php echo $this->lang->line('BNP_Paribas'); ?></option>
<option value="Deutsche Bank"><?php echo $this->lang->line('Deutsche_Bank'); ?></option>
<option value="Emirates NBD"><?php echo $this->lang->line('emirates_NBD'); ?></option>
<option value="Gulf International Bank (GIB)"><?php echo $this->lang->line('gulf_international_bank_gib'); ?></option>
<option value="Industrial and Commercial Bank of China (ICBC)"><?php echo $this->lang->line('industrial_and_Commercial_Bank_of_China_ICBC'); ?></option>
<option value="J.P. Morgan Chase N.A"><?php echo $this->lang->line('JP_Morgan_Chase_NA'); ?></option>
<option value="Muscat Bank"><?php echo $this->lang->line('Muscat_Bank'); ?></option>
<option value="National Bank of Bahrain (NBB)"><?php echo $this->lang->line('national_Bank_of_Bahrain_NBB'); ?></option>
<option value="National Bank of Kuwait (NBK)"><?php echo $this->lang->line('national_Bank_of_Kuwait_NBK'); ?></option>
<option value="National Bank of Pakistan (NBP)"><?php echo $this->lang->line('National_Bank_of_PakistanNBP'); ?></option>
<option value="Qatar National Bank (QNB)"><?php echo $this->lang->line('batar_national_bank_qnb'); ?></option>
<option value="Riyad Bank"><?php echo $this->lang->line('riyad_bank'); ?></option>
<option value="Samba Financial Group (Samba)"><?php echo $this->lang->line('samba_financial_group_samba'); ?></option>
<option value="Saudi Hollandi Bank (Alawwal)"><?php echo $this->lang->line('saudi_hollandi_bank_alawwal'); ?></option>
<option value="Saudi Investment Bank"><?php echo $this->lang->line('saudi_investment_bank'); ?></option>
<option value="State Bank of India (SBI)"><?php echo $this->lang->line('state_bank_of_india_sbi'); ?></option>
<option value="T.C.ZIRAAT BANKASI A.S."><?php echo $this->lang->line('TCZIRAAT_BANKASI_AS'); ?></option>
<option value="The National Commercial Bank"><?php echo $this->lang->line('the_national_commercial_bank'); ?></option>
<option value="The Saudi British Bank"><?php echo $this->lang->line('the_saudi_british_bank'); ?></option>


                            </select>
                            <div style="top:25px;"  class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>

  

                      </div>

                  </div>
              </div>
              <div class="row" style="margin-top:0;">
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="row">
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="<?php echo $this->lang->line('back'); ?>" />
                              </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="submit" class="button3" name="partner_submit" id="partner_submit" value="Submit" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
              </div>
          </div>
      </form>
  </div>
  <div class="partner_delivery" style="display:none;">

      <form action="<?php echo base_url('frontend_register/partner_reg'); ?>" method="post" role="form" class="contactForm partnerForm partner_form" enctype="multipart/form-data" autocomplete="off" onsubmit="event.preventDefault(); return formsubmitfunc(this);">
          <div class="step1">
              <!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
              <input type="hidden" name="driver_type" id="driver_type">
              <div class="row formshow">
                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" name="fname" class="form-control" id="fname" placeholder="<?php echo $this->lang->line('first_name'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                      </div>
                  </div>

                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" name="mname" class="form-control" id="mname" placeholder="<?php echo $this->lang->line('middle_name'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="lname" id="lname" placeholder="<?php echo $this->lang->line('last_name'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('only_alphabets_allowed'); ?></div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="identy" id="identy" maxlength="10" placeholder="<?php echo $this->lang->line('nationalidiqama'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex1234567890'); ?></div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control checkemail_driver" name="emailid" id="emailid" placeholder="<?php echo $this->lang->line('email_id'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex_abcd12testcom'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('emailid_already_registered'); ?></div>
                      </div>
                  </div>

                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group">
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="mobileno" id="mobileno" maxlength="9" placeholder="<?php echo $this->lang->line('mobile_number'); ?>" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex1234567890'); ?></div>
                      </div>
                  </div>

                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="form-group" id="">

  <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?> 
                            <i class="mandatestar">*</i><select class="form-control" name="city" id="city" >
                                <option value=""><?php echo $this->lang->line('city'); ?></option>
                                <option value="Bahrain"><?php echo $this->lang->line('bahrain'); ?></option>
                                <option value="Dammam"><?php echo $this->lang->line('dammam'); ?></option>
                                <option value="Dhahran"><?php echo $this->lang->line('dhahran'); ?></option>
                                <option value="Hassa"><?php echo $this->lang->line('hassa'); ?></option>
                                <option value="Hafar Al-Batin"><?php echo $this->lang->line('hafar_al_batin'); ?></option>
                                <option value="Jeddah"><?php echo $this->lang->line('jeddah'); ?></option>
                                <option value="Jubail"><?php echo $this->lang->line('jubail'); ?></option>
                                <option value="Kharj"><?php echo $this->lang->line('kharj'); ?></option>
                                <option value="Khobar"><?php echo $this->lang->line('khobar'); ?></option>
                                <option value="Khafji"><?php echo $this->lang->line('khafji'); ?></option>
                                <option value="Qatif"><?php echo $this->lang->line('qatif'); ?></option>
                                <option value="Qassim"><?php echo $this->lang->line('qassim'); ?></option>
                                <option value="Riyadh"><?php echo $this->lang->line('riyadh'); ?></option>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>

  <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
                            <i class="mandatestar">*</i><select class="form-control" name="city" id="city" dir="rtl">
                                <option value=""><?php echo $this->lang->line('city'); ?></option>
                                <option value="<?php echo $this->lang->line('riyadh'); ?>"><?php echo $this->lang->line('riyadh'); ?></option>
                                <option value="<?php echo $this->lang->line('jeddah'); ?>"><?php echo $this->lang->line('jeddah'); ?></option>
                                <option value="<?php echo $this->lang->line('dammam'); ?>"><?php echo $this->lang->line('dammam'); ?></option>
                                <option value="<?php echo $this->lang->line('khobar'); ?>"><?php echo $this->lang->line('khobar'); ?></option>
                                <option value="<?php echo $this->lang->line('jubail'); ?>"><?php echo $this->lang->line('jubail'); ?></option>
                                <option value="<?php echo $this->lang->line('hassa'); ?>"><?php echo $this->lang->line('hassa'); ?></option>
                                <option value="<?php echo $this->lang->line('dhahran'); ?>"><?php echo $this->lang->line('dhahran'); ?></option>
                                <option value="<?php echo $this->lang->line('qatif'); ?>"><?php echo $this->lang->line('qatif'); ?></option>
                                <option value="<?php echo $this->lang->line('kharj'); ?>"><?php echo $this->lang->line('kharj'); ?></option>
                                <option value="<?php echo $this->lang->line('qassim'); ?>"><?php echo $this->lang->line('qassim'); ?></option>
                                <option value="<?php echo $this->lang->line('khafji'); ?>"><?php echo $this->lang->line('khafji'); ?></option>
                                <option value="<?php echo $this->lang->line('hafar_al_batin'); ?>"><?php echo $this->lang->line('hafar_al_batin'); ?></option>
                                <option value="<?php echo $this->lang->line('bahrain'); ?>"><?php echo $this->lang->line('bahrain'); ?></option>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
  <?php } ?>


                      </div>
                  </div>


              </div>
              <div class="row" style="margin-top:0;">
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="form-group">
                          <input type="button" onclick="return step1_btn();" class="button3 step1_btn" value="<?php echo $this->lang->line('next'); ?>" />
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
              </div>
          </div>
          <div class="step2" style="display:none;">
              <!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
              <div class="row">
                  <div class="col-lg-12">
                      <div class="form-group">
  <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?> 
                            <textarea class="form-control" disabled id="tnc"  rows="8" placeholder="Terms and conditions" style="color: #000 !important;" />
- <?php echo $this->lang->line('the_minimum_age_to_join_is_years'); ?> 
- <?php echo $this->lang->line('car_model_should_be_year_2000_and_above'); ?>
- <?php echo $this->lang->line('arabic_speaking_is_mandatory_condition'); ?>
- <?php echo $this->lang->line('for_non-saudis_you_must_apply_for_accommodation_on_a_special'); ?>
                            </textarea>
  <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>
                            <textarea class="form-control" disabled id="tnc" dir="rtl" rows="8" placeholder="Terms and conditions" style="color: #000 !important;" />
- <?php echo $this->lang->line('the_minimum_age_to_join_is_years'); ?> 
- <?php echo $this->lang->line('car_model_should_be_year_2000_and_above'); ?>
- <?php echo $this->lang->line('arabic_speaking_is_mandatory_condition'); ?>
- <?php echo $this->lang->line('for_non-saudis_you_must_apply_for_accommodation_on_a_special'); ?>
                            </textarea>
  <?php } ?>


                      </div>
                      <label style="color:#fff;"> <?php echo $this->lang->line('declaration'); ?><span style="color:red">*</span> </label>
                      <div class="form-group" style="color:#fff;">
                          <div class="declbox form-group"><input type="checkbox" class="checkboxhome" name="agree" id="agree" value="1" /><?php echo $this->lang->line('i_have_read_and_agree_to_the_terms_and_conditions'); ?></div>
                      </div>
                  </div>
              </div>
              <div class="row" style="margin-top:0;">
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="row">
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step0_btn" onclick="return step0_btn();" value="<?php echo $this->lang->line('back'); ?>" />
                              </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="<?php echo $this->lang->line('next'); ?>" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
              </div>
          </div>
          <div class="step3" style="display:none;">
              <!--<div class="field_val" style="color:#ff0000; text-align: center;padding-bottom: 10px;display:none;">All heighlighted fields are mandatory</div>-->
              <div class="row">

                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">

  <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?> 
                            <i class="mandatestar">*</i><select class="form-control" name="vehicle_type" id="vehicle_type">
                                <option value=""><?php echo $this->lang->line('type_of_the_vehicle'); ?></option>
                                <!--<option value="Delivery">Delivery</option>-->
    <?php //print_r($brand_list);die();  ?>
    <?php foreach ($brand_list as $brand_list2): ?>
                                  <optgroup label="<?= $brand_list2['car_brand']; ?>">
                                  <?php $ss = $this->model->get_car_list($brand_list2['car_brand']); ?>
                                  <?php foreach ($ss as $ss): ?>
                                        <option value="<?= $ss['id']; ?>"><?= $ss['car_model']; ?></option>							
                                      <?php endforeach; ?>
                                  </optgroup>
                                    <?php endforeach; ?>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>

                              <?php }elseif ($this->session->userdata['site_lang'] == 'arabic') { ?> 

                            <i class="mandatestar">*</i><select class="form-control" name="vehicle_type" id="vehicle_type" dir="rtl">
                                <option value=""><?php echo $this->lang->line('type_of_the_vehicle'); ?></option>
                                <!--<option value="Delivery">Delivery</option>-->
                            <?php //print_r($brand_list);die(); ?>
    <?php foreach ($brand_list as $brand_list2): ?>
                                  <optgroup label="<?= $brand_list2['car_brand']; ?>">
      <?php $ss = $this->model->get_car_list($brand_list2['car_brand']); ?>
      <?php foreach ($ss as $ss): ?>
                                        <option value="<?= $ss['id']; ?>"><?= $ss['car_model']; ?></option>							
      <?php endforeach; ?>
                                  </optgroup>
    <?php endforeach; ?>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>


  <?php } ?>


                      </div>
                  </div>

                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group" id="car_model1">
                          <i class="mandatestar">*</i><input type="text" class="form-control" placeholder="<?php echo $this->lang->line('car_model'); ?>" id="car_model" />
                          <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                      </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group" id="vehicle_year_dropdown">

                              <?php if (empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] == 'english') { ?>
                            <i class="mandatestar">*</i><select class="form-control" name="vehicle_year" id="vehicle_year">
                                <option value=""><?php echo $this->lang->line('year_of_manufacture'); ?></option>
                            <?php
                            for ($starting_year = 2000; $starting_year <= date('Y'); $starting_year++) {
                              echo '<option value="' . $starting_year . '">' . $starting_year . '</option>';
                            }
                            ?>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
  <?php } elseif ($this->session->userdata['site_lang'] == 'arabic') { ?>

                            <i class="mandatestar">*</i><select class="form-control" name="vehicle_year" id="vehicle_year" dir="rtl">
                                <option value=""><?php echo $this->lang->line('year_of_manufacture'); ?></option>
    <?php
    for ($starting_year = 2000; $starting_year <= date('Y'); $starting_year++) {
      echo '<option value="' . $starting_year . '">' . $starting_year . '</option>';
    }
    ?>
                            </select>
                            <div class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
  <?php } ?>


                      </div>
                  </div>

              </div>
              <div class="row" style="margin-top:0;">
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="row">
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step1_btn" onclick="return step1_btn();" value="<?php echo $this->lang->line('back'); ?>" />
                              </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step3_btn" onclick="return step3_btn();" value="<?php echo $this->lang->line('next'); ?>" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
              </div>
          </div>
          <div class="step4" style="display:none;">
              <!--<div class="field_val" style="color:#ff0000;text-align: center;padding-bottom: 10px; display:none;">All heighlighted fields are mandatory</div>-->
              <div class="row">


                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('iban_number'); ?></label>
                          <i class="mandatestar">*</i><input type="text" class="form-control" name="iban" id="iban" maxlength="24" placeholder="<?php echo $this->lang->line('iban_number'); ?>" />
                          <div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                          <div  style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('ex_SA1234000001234567890000'); ?></div>
                      </div>

                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-sx-12">
                      <div class="form-group">
                          <label><?php echo $this->lang->line('tafweeth'); ?></label>
                          <input type="file" class="form-control photo" name="tafweeth_img" id="tafweeth_img" placeholder="<?php echo $this->lang->line('tafweeth_image'); ?>" />

                      </div>
                  </div>

                  <!--<div class="col-md-4 col-sm-6 col-xs-12 delfield" id="tafweeth_img_date" style="display:none;"> 
                  
                   
                   
<div class="form-group">
                      <div class="row">
                          <p style="margin:0 0 0 15px;"><label>Expiration date of Tafweeth</label></p>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                               <i class="mandatestar">*</i> <select name="tafw_dd" id="tafw_dd" class="form-control">
                                          <option value="">DD</option>
                                              <?php for ($i = 1; $i < 32; $i++) {
                                               if($i < 10)
                                               {
                                                 echo "<option value='" . $i . "'>" ."0". $i . "</option>";
                                               }
                                                else {
                                                  echo "<option value='" . $i . "'>" . $i . "</option>";
                                                }
                                              
                                                
                                                
                                              } ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                             <div style="top:25px;" class="validation expired"><i class="fa fa-caret-left" aria-hidden="true"></i> The entered date has expired already</div>

                          </div>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                              
                                
                                  <i class="mandatestar">*</i> <select class="form-control" name="tafw_mm" id="tafw_mm">
                                      <option value="">MM</option>
                                      <?php
                                      for ($m = 1; $m <= 12; $m++) {
                                        $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                        echo "<option value='" . $m . "'>" . $month . "</option>";
                                      }
                                      ?>
                                  </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                              
                          </div>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                              
                                  
                                  <i class="mandatestar">*</i> <select name="tafw_yy" id="tafw_yy" class="form-control">
                                      <option value="">YYYY</option>
                                      <?php
                                      $ending_year = date('Y', strtotime('+80 year'));
                                      $starting_year = date('Y');
                                      $current_year = date('Y');
                                      for ($starting_year; $starting_year <= $ending_year; $starting_year++) {
                                        echo '<option value="' . $starting_year . '"';
                                        echo ' >' . $starting_year . '</option>';
                                      }?>
                                      </select>
                                  <div style="top:25px;" class="validation"><i class="fa fa-caret-left" aria-hidden="true"></i> <?php echo $this->lang->line('mandatory_field'); ?></div>
                            
                          </div>
                      </div>
</div>                   
                   
                   
                   
                   
                   
                  </div>-->

              </div>
              <div class="row" style="margin-top:0;">
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
                  <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="row">
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="button" class="button3 step2_btn" onclick="return step2_btn();" value="<?php echo $this->lang->line('back'); ?>" />
                              </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                              <div class="form-group">
                                  <input type="submit" class="button3" name="partner_submit" id="partner_submit" value="Submit" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12"></div>
              </div>
          </div>
      </form>
  </div>
  <!-- --- -->
