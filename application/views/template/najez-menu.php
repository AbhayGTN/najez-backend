<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Najez</title>
    <meta name="description" content="Najez">
    <meta name="keywords" content="Najez">
    <link rel="icon" href="<?php echo base_url('assets/home/'); ?>/img/favicon.ico" >
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:400,300|Raleway:300,400,900,700italic,700,300,600">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/home/'); ?>/css/style-arabic.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/home/'); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/home/'); ?>/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/home'); ?>/css/bootstrap-datepicker.css">
	 
    <!-- =======================================================
        Theme Name: Baker
        Theme URL: https://bootstrapmade.com/baker-free-onepage-bootstrap-theme/
        Author: BootstrapMade.com
        Author URL: https://bootstrapmade.com
    ======================================================= -->
  </head>
 
  <?php if(empty($this->session->userdata['site_lang']))
    { 
      $bodyID="english-lang";
    }
    elseif($this->session->userdata['site_lang'] =='english')
    {
      $bodyID="english-lang";
    }
    elseif($this->session->userdata['site_lang'] =='arabic')
    {
      $bodyID="arabic-lang";
    }
   ?>
  <body  class="<?php echo $body_class; ?>" id="<?php echo $bodyID;?>">
  <?php //if($this->session->flashdata('flashdata')){ ?> 

<?php if(empty($this->session->userdata['site_lang']) || $this->session->userdata['site_lang'] =='english'){ ?>
<p class="msgpop" style="display:none;text-align: center;background: #ff9e15;padding: 20px;color: #fff; margin: 0;z-index: 99999;position: fixed;width:100%;">Thank you for submitting your details, We will keep you posted.</p>
<?php }elseif($this->session->userdata['site_lang'] =='arabic'){ ?>
<p class="msgpop" style="display:none;text-align: center;background: #ff9e15;padding: 20px;color: #fff; margin: 0;z-index: 99999;position: fixed;width:100%;">‘‘شکراً لکم علی تقدیم تفاصیلکم ،و سوف نبقی لکم مطلّعًا’’</p>
<?php }?>



<?php //}?>
<?php if($this->session->flashdata('flashdata')){ ?> 
<p class="msgpop1" style="text-align: center;background: #ff9e15;padding: 20px;color: #fff; margin: 0;z-index: 99999;position: fixed;width:100%;"><?php echo $this->session->flashdata('flashdata'); ?></p>
<?php }?>
    <div class="loader"></div>
    <div id="myDiv">
    <!--HEADER-->
    <div class="header">
      <div class="bg-color">
        <header id="main-header">

			<div class="row anti">
				<div class="col-lg-3 col-sm-3 col-xs-6 anti">
					<a class="logos" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/home/'); ?>/img/logo.png" class="img-responsive" alt="logo" style="position: relative;z-index: 9999;" /><img src="<?php echo base_url('assets/home/'); ?>/img/logo-arabic.png" class="img-responsive arabic-only" alt="logo" style="position: relative;z-index: 9999;" /></a>
				</div>
				<div class="col-lg-7 col-sm-7">
				<nav class="navbar navbar-default navbar-fixed-top"> <!-- navbar-fixed-top -->
				<div class="navbar-header">
				
				
				
				
				
				
				  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  
				  
				  <div class="visible-xs btn-arab">
						<div id="navbar">
							<ul>
								<li class="dropdown countryDropdown"> 
									
										
										
										
										 
										
										<?php if(empty($this->session->userdata['site_lang'])){ ?> 
											<a aa href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="<?php echo base_url(); ?>assets/home/img/Grossbritanien.jpg" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
										<?php }elseif($this->session->userdata['site_lang'] =='english'){?>
											<a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="<?php echo base_url(); ?>assets/home/img/Grossbritanien.jpg" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
										<?php }elseif($this->session->userdata['site_lang'] =='arabic'){ ?>
											<a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="<?php echo base_url(); ?>assets/home/img/saudi-arabia.jpg" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavIta">AR</span> <span class="caret"></span></a>
										<?php }?>
										 
										
										
									<ul class="dropdown-menu" role="menu" aaa>
										<li><a id="navIta" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="language"><img id="" src="<?php echo base_url(); ?>assets/home/img/saudi-arabia.jpg" alt="..." class="imgNavIta img-thumbnail icon-small">  <span class="lanNavIta">Arabic</span></a></li>
										<li><a id="navEng" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="language"><img id="" src="<?php echo base_url(); ?>assets/home/img/Grossbritanien.jpg" alt="..." class="imgNavEng img-thumbnail icon-small">  <span class="lanNavEng">English</span></a></li>
									</ul>
								</li>
							</ul>
						</div> <!--/.navbar-collapse -->
				</div>
				  
				  
				  
				  
				  
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">
				 
				  <ul class="nav navbar-nav navbar-right">
				   <?php if ($active== 'home'){?>
    					<li class=""><a href="#"><?php echo  $this->lang->line('home_menu'); ?></a></li>
    					<li class=""><a href="#about-us"><?php echo  $this->lang->line('aboutus_mene'); ?></a></li>
    					<li class=""><a href="#services"><?php echo  $this->lang->line('services_menu'); ?></a></li>
    					<li class=""><a class="main-active" href="#download-app"><?php echo  $this->lang->line('downloadapp_menu'); ?></a></li>
    					<?php /*?><li><a  class="main-active" href="#download-app"><?php echo  $this->lang->line('joinus_menu'); ?></a></li><?php */?>
					<?php }else{?>
    					<li class=""><a href="<?php echo base_url(); ?>"><?php echo  $this->lang->line('home_menu'); ?></a></li>
    					<li class=""><a href="<?php echo base_url(); ?>#about-us"><?php echo  $this->lang->line('aboutus_mene'); ?></a></li>
    					<li class=""><a href="<?php echo base_url(); ?>#services"><?php echo  $this->lang->line('services_menu'); ?></a></li>
    					<li class=""><a class="main-active" href="<?php echo base_url(); ?>#download-app"><?php echo  $this->lang->line('downloadapp_menu'); ?></a></li>
    					<?php /*?><li><a  class="main-active" href="<?php echo base_url(); ?>#download-app"><?php echo  $this->lang->line('joinus_menu'); ?></a></li><?php */?>
					<?php }?>	
					
					<li class="dropdown countryDropdown hidden-xs"> 
					 
					<?php if(empty($this->session->userdata['site_lang'])){ ?>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="<?php echo base_url(); ?>assets/home/img/Grossbritanien.jpg" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
					<?php }elseif($this->session->userdata['site_lang'] =='english'){?> 
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="<?php echo base_url(); ?>assets/home/img/Grossbritanien.jpg" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">ENG</span> <span class="caret"></span></a>
					<?php }else{ ?>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><img id="" src="<?php echo base_url(); ?>assets/home/img/saudi-arabia.jpg" alt="..." class="imgNavSel img-thumbnail icon-small">  <span class="lanNavSel">AR</span> <span class="caret"></span></a>
					<?php } ?>
						
						<ul class="dropdown-menu" role="menu" bbb>
							<li><a id="navIta" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/arabic" class="language"> <img id="" src="<?php echo base_url(); ?>assets/home/img/saudi-arabia.jpg" alt="..." class="imgNavIta img-thumbnail icon-small">  <span class="lanNavIta">Arabic</span></a></li>
							
							<li><a id="navEng" href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english" class="language"><img id="" src="<?php echo base_url(); ?>assets/home/img/Grossbritanien.jpg" alt="..." class="imgNavEng img-thumbnail icon-small">  <span class="lanNavEng">English</span></a></li>
						</ul>
					</li>
				  </ul>
				
				</div>
				
				</nav>
				</div>
				
			</div>
			
			
			
        
        </header>
        
        
        <div class="wrapper">
        
        	<?php if ($active== 'home'){?>
			<div class="container">
				<div class="row">
					<div class="banner-info text-center wow fadeIn delay-05s col-lg-12">
						<h2 class="bnr-sub-title"><?php echo  $this->lang->line('welcome_message'); ?></h2>
						<p class="bnr-para"><?php echo  $this->lang->line('anywhere_anytime'); ?></p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-xs-12 text-center"><a href="#download-app"><button class="button1 head-btn"><?php echo  $this->lang->line('join_as_a_partner'); ?></button></a></div>
					<div class="col-lg-6 col-md-6 col-xs-12 text-center"><a href="#download-app"><button class="button2 head-btn"><?php echo  $this->lang->line('join_as_a_user'); ?></button></a></div>
					<div class="overlay-detail">
					</div>
				</div>			  
			</div>
			<?php }else{?>			
			<div class="container">
				<div class="row">
					<div class="banner-info text-center wow fadeIn delay-05s col-lg-12">
						<h2 class="bnr-sub-title"><?php echo $page_title; ?> </h2>
				
					</div>
				</div>
			</div>
			<?php }?>
			
        </div>
      </div>
 </div>