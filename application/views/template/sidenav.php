<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url() . $userdata->photo; ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $userdata->username; ?></p>
                <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
            </div>
        </div>
        <ul class="sidebar-menu">
            <!--<li class="header">MAIN NAVIGATION</li>-->
            <li class="<?php if ($active == 'dashboard') {
  echo 'active';
} ?> treeview">
                <a href="<?php echo base_url('dashboard'); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>

                </a>

            </li>
           <?php if(in_array('1', $this->session->userdata('user_roles'))){ ?>
    
            <li class="<?php if ($active == 'backenduser') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Backend Users</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if ($active_sub == 'add_user') {
  echo 'active';
} ?> "><a href="<?php echo base_url('backenduser/add_user'); ?>"><i class="fa fa-circle-o"></i> Add Backend User</a></li>
                    <li <?php if ($active_sub == 'view_user') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('backenduser/view_users'); ?>"><i class="fa fa-circle-o"></i>View/Edit</a></li>
                   <li <?php if ($active_sub == 'activity_log') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('backenduser/activity_log'); ?>"><i class="fa fa-circle-o"></i>Activity Log</a></li>
                </ul>
            </li>
           <?php }if (in_array('2', $this->session->userdata('user_roles'))) { ?>
            
            <li class="<?php if ($active == 'user') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Customer</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($active_sub == 'add_user') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('user/add_user'); ?>"><i class="fa fa-circle-o"></i> Add Customer</a></li>
                    <li <?php if ($active_sub == 'view_user') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('user/view_users'); ?>"><i class="fa fa-circle-o"></i> View / Edit Customer</a></li>
                </ul>
            </li>
<?php } if (in_array('3', $this->session->userdata('user_roles'))) { ?>
            <li class="<?php if ($active == 'email') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-envelope"></i>
                    <span>Email Template</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($active_sub == 'view_email') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('email/view_all'); ?>"><i class="fa fa-circle-o"></i> View / Edit template</a></li>
                </ul>
            </li>

<?php } if (in_array('4', $this->session->userdata('user_roles'))) { ?>
            <li class="<?php if ($active == 'driver') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-male"></i>
                    <span>Drivers</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($active_sub == 'add_driver') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('driver/add_driver'); ?>"><i class="fa fa-circle-o"></i> Add Driver</a></li>
                    <li <?php if ($active_sub == 'view_driver') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('driver/view_drivers'); ?>"><i class="fa fa-circle-o"></i> View / Edit Drivers</a></li>
                </ul>
            </li>



<!--<li class="<?php if ($active == 'vehicle') {
  echo 'active';
} ?> treeview">
<a href="#">
<i class="fa fa-car"></i>
<span>Vehicles</span>
<span class="pull-right-container">
<i class="fa fa-angle-left pull-right"></i>
</span>
</a>
<ul class="treeview-menu">
<li <?php if ($active_sub == 'add_vehicle') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('vehicles/add_vehicle'); ?>"><i class="fa fa-circle-o"></i> Add Vehicle</a></li>
<li <?php if ($active_sub == 'view_service') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('vehicles/view_vehicles'); ?>"><i class="fa fa-circle-o"></i> View / Edit Vehicles</a></li>
</ul>
</li>-->
<?php } if (in_array('5', $this->session->userdata('user_roles'))) { ?>            
<li class="<?php if ($active == 'page') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Pages</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($active_sub == 'add_page') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('pages/add_page'); ?>"><i class="fa fa-circle-o"></i> Add Page</a></li>
                    <li <?php if ($active_sub == 'view_page') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('pages/view_pages'); ?>"><i class="fa fa-circle-o"></i> View / Edit Pages</a></li>
                </ul>
            </li>
            <?php } if (in_array('6', $this->session->userdata('user_roles'))) { ?>
            <li class="<?php if ($active == 'ratecard') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-gift"></i>
                    <span>Voucher</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($active_sub == 'add_card') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('voucher/add_card'); ?>"><i class="fa fa-circle-o"></i> Add Voucher</a></li>
                    <li <?php if ($active_sub == 'view_card') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('voucher/view_cards'); ?>"><i class="fa fa-circle-o"></i>View all</a></li>
                                <!--<li <?php if ($active_sub == 'pricelist') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('voucher/pricelist'); ?>"><i class="fa fa-circle-o"></i> Online voucher</a></li>-->
                </ul>
            </li>
<?php } if (in_array('7', $this->session->userdata('user_roles'))) { ?>
            <!-- fixed By Nandini-->
            <li class="<?php if ($this->router->fetch_class() == 'promotions') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Promotion's</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($this->router->fetch_method() == 'add') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('promotions/add'); ?>"><i class="fa fa-circle-o"></i> Add Card</a></li>
                    <li <?php if ($this->router->fetch_method() == 'view') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('promotions/view_all'); ?>"><i class="fa fa-circle-o"></i> View / Edit Cards</a></li>

                </ul>
            </li>
<?php } if (in_array('8', $this->session->userdata('user_roles'))) { ?>
            <li class="<?php if ($active == 'farerate') {
  echo 'active';
} ?> treeview">
                <a href="<?= base_url('fare_ratecard/view'); ?>">
                    <i class="fa fa-list-alt"></i>
                    <span>Fare Rate Card</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

            </li>
<?php } if (in_array('9', $this->session->userdata('user_roles'))) { ?>
            <li class="<?php if ($active == 'reports') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($active_sub == 'total_sales') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/total_sales/'); ?>"><i class="fa fa-circle-o"></i> Total Sales</a></li>
                    <li <?php if ($active_sub == 'total_income') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/total_income/'); ?>"><i class="fa fa-circle-o"></i> Total Income</a></li>
                    <li <?php if ($active_sub == 'total_promotion_details') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/total_promotion_details/'); ?>"><i class="fa fa-circle-o"></i> Total Promotion Details</a></li>
                    <li <?php if ($active_sub == 'total_driver_income') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/total_driver_income/'); ?>"><i class="fa fa-circle-o"></i> Total Driver Income</a></li>
                    <li <?php if ($active_sub == 'driver_active_trips') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/driver_active_trips/'); ?>"><i class="fa fa-circle-o"></i> Driver Active Trips</a></li>
                    <li <?php if ($active_sub == 'last_30_days') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/active_driver_past_30_days/'); ?>"><i class="fa fa-circle-o"></i>30 Days Active Drivers</a></li>
                    <li <?php if ($active_sub == 'active_yesterday') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/active_drivers_yesterday'); ?>"><i class="fa fa-circle-o"></i>Active Drivers Yesterday</a></li>
                    <li <?php if ($active_sub == 'earnings_per_deriver') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/earnings_per_driver/'); ?>"><i class="fa fa-circle-o"></i>Earnings Per Driver</a></li>
                    <li <?php if ($active_sub == 'total_daily_sales') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/total_daily_sales/'); ?>"><i class="fa fa-circle-o"></i> Total Daily Sales</a></li>
                    <li <?php if ($active_sub == 'canceled_rides') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/canceled_rides'); ?>"><i class="fa fa-circle-o"></i> Total Canceled Rides</a></li>
                    <li <?php if ($active_sub == 'confirm_rides') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/confirm_rides/'); ?>"><i class="fa fa-circle-o"></i> Total Confirmed Rides</a></li>
                    <li <?php if ($active_sub == 'inactive_drivers') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/inactive_drivers/'); ?>"><i class="fa fa-circle-o"></i> Inactive Drivers</a></li>
                    <li <?php if ($active_sub == 'inactive_customers') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('reports/inactive_customers/'); ?>"><i class="fa fa-circle-o"></i> Inactive Customers</a></li>
                </ul>
            </li>
<?php } if (in_array('10', $this->session->userdata('user_roles'))) { ?>
            <li class="<?php if ($active == 'settings') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    <li <?php if ($active_sub == 'settings') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('settings'); ?>"><i class="fa fa-circle-o"></i>  Setting Details</a></li>
                    <li <?php if ($active_sub == 'add_city') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('settings/add_city'); ?>"><i class="fa fa-circle-o"></i> Cities List</a></li>
                    <li <?php if ($active_sub == 'add_car') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('settings/add_car'); ?>"><i class="fa fa-circle-o"></i> Cars List</a></li>
                    <li <?php if ($active_sub == 'add_bank') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('settings/add_bank'); ?>"><i class="fa fa-circle-o"></i> Banks list</a></li>
                    <li <?php if ($active_sub == 'add_bank') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('settings/driver_cancle_options'); ?>"><i class="fa fa-circle-o"></i> Driver Cancel Options</a></li>
                    <li <?php if ($active_sub == 'add_bank') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('settings/user_cancle_options'); ?>"><i class="fa fa-circle-o"></i> Users Cancel Options</a></li>
					<li <?php if ($active_sub == 'airport_zone') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('settings/airport_zone'); ?>"><i class="fa fa-circle-o"></i> Airport Zone</a></li>

                </ul>
            </li>
<?php }  ?>
            
            
            <li class="<?php if ($active == 'wallet') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Wallet</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($active_sub == 'driver_trans') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('wallet/driver_transactions'); ?>"><i class="fa fa-circle-o"></i>Driver Transactions</a></li>
                    <li <?php if ($active_sub == 'customer_trans') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('wallet/customer_transactions'); ?>"><i class="fa fa-circle-o"></i>Customer Transactions</a></li>

                </ul>
            </li>
            
            
            <li class="<?php if ($active == 'tools') {
  echo 'active';
} ?> treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Tools</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php if ($active_sub == 'broadcast') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('tools/broadcast/'); ?>"><i class="fa fa-circle-o"></i>Broadcast</a></li>
                    <li <?php if ($active_sub == 'bulk_action') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('tools/bulk_action/'); ?>"><i class="fa fa-circle-o"></i>Bulk Action</a></li>

                </ul>
            </li>
            
            
            
            
            
            
            
            
                    <!--<li class="<?php if ($active == 'transaction') {
  echo 'active';
} ?> treeview">
              <a href="#">
                <i class="fa fa-folder"></i>
                <span>Transaction History</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if ($active_sub == 'view_transaction') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('transactions/view_transactions'); ?>"><i class="fa fa-circle-o"></i> View History</a></li>
              </ul>
            </li>
                    
                    <li class="<?php if ($active == 'order') {
  echo 'active';
} ?> treeview">
              <a href="#">
                <i class="fa fa-credit-card"></i>
                <span>Orders</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if ($active_sub == 'view_order') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('orders/view_orders'); ?>"><i class="fa fa-circle-o"></i> View Orders</a></li>
              </ul>
            </li>
                    
                    <li class="<?php if ($active == 'home') {
  echo 'active';
} ?> treeview">
              <a href="#">
                <i class="fa fa-desktop"></i>
                <span>Homepage</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if ($active_sub == 'home') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('home/home_settings'); ?>"><i class="fa fa-circle-o"></i> Homepage Settings</a></li>
              </ul>
            </li>
                    
                    <li class="<?php if ($active == 'setting') {
  echo 'active';
} ?> treeview">
              <a href="#">
                <i class="fa fa-gears"></i>
                <span>Settings</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li <?php if ($active_sub == 'view_setting') {
  echo 'class="active"';
} ?>><a href="<?php echo base_url('settings'); ?>"><i class="fa fa-circle-o"></i> Change Settings</a></li>
              </ul>
            </li>-->

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>