-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2018 at 11:00 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `najezdev`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`%` PROCEDURE `Driver_transfer` (IN `driver_id` INT, IN `transfer_driver_id` INT, IN `amount` DOUBLE, IN `description` VARCHAR(150), IN `transaction_detail1` VARCHAR(150), IN `transaction_detail2` VARCHAR(150), IN `amount_detail1` VARCHAR(150), IN `amount_detail2` VARCHAR(150), IN `transfer_driver` VARCHAR(150), IN `driver_name` VARCHAR(150))  BEGIN

Insert into driver_transactions(driver_id,transfer_driverid,transaction_type_id,transaction_amount,transaction_detail,status)
values(driver_id,transfer_driver_id,'1',amount,transaction_detail1,'1');

  Insert into driver_wallet_details(driver_id,description,transfer_name,amount,transaction_type_id,transaction_mode,transaction_datetime,status) 
         values(driver_id,transaction_detail1,transfer_driver,amount_detail1,'1','debit',now(),'1');

Insert into driver_transactions(driver_id,transfer_driverid,transaction_type_id,transaction_amount,transaction_detail,status) 
values(transfer_driver_id,driver_id,'6',amount,transaction_detail2,'1');

  Insert into driver_wallet_details(driver_id,description,transfer_name,amount,transaction_type_id,transaction_mode,transaction_datetime,status) 
         values(transfer_driver_id,transaction_detail2,driver_name,amount_detail2,'6','credit',now(),'1');

Update drivers set total_amount=total_amount-amount where id=driver_id;

Update drivers set total_amount=total_amount+amount where id=transfer_driver_id;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `partial_payment` (IN `ride_id` INT, IN `userId` INT, IN `ridedriverId` INT, IN `paying_amount` DOUBLE, OUT `param_sp_success` TINYINT)  BEGIN
DECLARE v_finished INTEGER DEFAULT 0;
Declare driverId int;
Declare pendingAmount double;
Declare rideType int;
Declare rideId int;
Declare sattle_amount double;
Declare str_amount varchar(50);
DEClARE cur_pending_balance CURSOR FOR Select ride_id,driver_id,amount,ride_type from driver_pending_balance where user_id=userId and status='1' order by id asc;
  DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

START TRANSACTION;
SET param_sp_success = 0;
set sattle_amount=paying_amount;

 OPEN cur_pending_balance;
 pending_bal: LOOP
 FETCH cur_pending_balance INTO rideId,driverId,pendingAmount,rideType;
 IF v_finished = 1 THEN 
 LEAVE pending_bal;
 END IF;
 
 if pendingAmount <= sattle_amount then
 set sattle_amount= sattle_amount - pendingAmount;
if rideType = 1 then 
Update users set cancel_charge = cancel_charge + pendingAmount where id=userId;
else 
Update users set total_amount = total_amount + pendingAmount where id=userId;
End If;
Update driver_pending_balance set status='2' where ride_id=rideId and user_id=userId ;
Insert into driver_clear_balance(ride_id,user_id,driver_id,amount) values(rideId,userId,driverId,pendingAmount);
Update drivers set total_amount=total_amount+pendingAmount ,pending_amount=pending_amount-pendingAmount where id=driverId;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driverId,rideId,'12',pendingAmount,'Driver ride share','1');
  set str_amount = concat('+ SAR ',pendingAmount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driverId,rideId,'Driver ride share','12',str_amount,pendingAmount,'credit',now(),'1');

 
 else
 if rideType = 1 then 
Update users set cancel_charge = cancel_charge + sattle_amount where id=userId;
else 
Update users set total_amount = total_amount + sattle_amount where id=userId;
End If;
Update driver_pending_balance set amount=amount - sattle_amount where ride_id=rideId and user_id=userId ;
Insert into driver_clear_balance(ride_id,user_id,driver_id,amount) values(rideId,userId,driverId,sattle_amount);
Update drivers set total_amount=total_amount+sattle_amount ,pending_amount=pending_amount-sattle_amount where id=driverId;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driverId,rideId,'12',sattle_amount,'Driver ride share','1');
  set str_amount = concat('+ SAR ',sattle_amount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driverId,rideId,'Driver ride share','12',str_amount,sattle_amount,'credit',now(),'1');
 set sattle_amount=0;
 End If;

 if sattle_amount=0 then 
 LEAVE pending_bal;
 End If;
 END LOOP pending_bal;
 CLOSE cur_pending_balance;

 if sattle_amount > 0 then 
 Update drivers set total_amount=total_amount-sattle_amount where id = ridedriverId;
Insert into driver_transactions(driver_id,ride_id,transfer_driverid,transaction_type_id,transaction_amount,transaction_detail,status)
values(ridedriverId,ride_id,userId,'9',sattle_amount,'Money transfer to Customer','1');
set str_amount=concat('- SAR ',sattle_amount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(ridedriverId,ride_id,'Money transfer to Customer','9',str_amount,sattle_amount,'debit',now(),'1');

Update users set total_amount=total_amount+sattle_amount where id=userId;
Insert into user_transactions(user_id,ride_id,transfer_userid,transaction_type_id,transaction_amount,transaction_detail,status) 
values(userId,ride_id,ridedriverId,'10',sattle_amount,'Money transfer from Driver','1');
set str_amount=concat('+ SAR ',sattle_amount);
Insert into user_wallet_details(user_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)
values(userId,ride_id,'Money transfer from Driver','10',str_amount,sattle_amount,'credit',now(),'1'); 
 
 End If;
SET param_sp_success = 1;
COMMIT;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `Payment_transfer` (IN `driver_id` INT, IN `transfer_user_id` INT, IN `ride_id` INT, IN `amount` DOUBLE, IN `description` VARCHAR(150), IN `transaction_detail1` VARCHAR(150), IN `transaction_detail2` VARCHAR(150), IN `amount_detail1` VARCHAR(150), IN `amount_detail2` VARCHAR(150))  BEGIN

Insert into driver_transactions(driver_id,ride_id,transfer_driverid,transaction_type_id,transaction_amount,transaction_detail,status)
values(driver_id,ride_id,transfer_user_id,'9',amount,transaction_detail1,'1');

  Insert into driver_wallet_details(driver_id,ride_id,description,amount,transaction_mode,transaction_datetime,status) 
         values(driver_id,ride_id,transaction_detail1,amount_detail1,'debit',now(),'1');

Insert into user_transactions(user_id,ride_id,transfer_userid,transaction_type_id,transaction_amount,transaction_detail,status) 
values(transfer_user_id,ride_id,driver_id,'10',amount,transaction_detail2,'1');

  Insert into user_wallet_details(user_id,ride_id,description,amount,transaction_mode,transaction_datetime,status) 
         values(transfer_user_id,ride_id,transaction_detail2,amount_detail2,'credit',now(),'1');


Update drivers set total_amount=total_amount-amount where id=driver_id;

Update users set total_amount=total_amount+amount where id=transfer_user_id;

END$$

CREATE DEFINER=`root`@`%` PROCEDURE `ride_complete_transaction` (IN `user_id` INT, IN `driver_id` INT, IN `ride_id` INT, IN `fare_amount` DOUBLE, IN `previous_remaining` DOUBLE, IN `ride_status` INT, IN `driver_share` DOUBLE, IN `najez_share` DOUBLE, IN `halal` DOUBLE, IN `najez_share_percentage` DOUBLE, IN `driver_share_percentage` DOUBLE, IN `user_referal_id` VARCHAR(50), IN `user_referal_amount` DOUBLE, IN `driver_referal_id` VARCHAR(50), IN `driver_referal_amount` DOUBLE, IN `payment_method` VARCHAR(50), OUT `param_sp_success` TINYINT)  BEGIN
Declare driver_share_cal double default 0;
Declare najez_share_cal double default 0;
Declare str_amount varchar(50);
Declare user_balance double default 0;
Declare payment_amount double default 0;
Declare remaining_amount double default 0;
Declare a double default 0; 
Declare driver_referal_amount double;
Declare total_driver_referal_amount double default 0;

DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

START TRANSACTION;

SET param_sp_success = 0;

Select total_amount+cancel_charge into user_balance from users where id=user_id;

if ride_status = '3' then 

if (payment_method ='cash' or previous_remaining !=0) then
set najez_share_cal=(fare_amount*najez_share_percentage);
Insert into driver_pending_balance(ride_id,user_id,driver_id,amount,ride_type,status) 
values(ride_id,user_id,driver_id,fare_amount,'2','1');

Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driver_id,ride_id,'11',najez_share_cal * (-1),'Najez share','1');

  set str_amount = concat('- SAR ',najez_share_cal);

Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driver_id,ride_id,'Najez share','11',str_amount,najez_share_cal * (-1),'debit',now(),'1');

   Update drivers set total_amount=total_amount - najez_share_cal - halal,pending_amount=pending_amount+fare_amount  where id = driver_id;
 
   Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driver_id,ride_id,'13',halal * (-1),'Halal','1');

  set str_amount = concat('- SAR ',halal);

Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driver_id,ride_id,'Halal','13',str_amount,halal * (-1),'debit',now(),'1');

   
   if (previous_remaining !=0 and payment_method = 'wallet') then 
   Update users set total_amount=total_amount - fare_amount  where id=user_id ;
   Insert into user_transactions (user_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (user_id,ride_id,'3',fare_amount,'Ride transaction','1');
set str_amount = concat('- SAR ',fare_amount);
Insert into user_wallet_details(user_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(user_id,ride_id,'Ride transaction','3',str_amount,fare_amount,'debit',now(),'1');
   End If;
	
   
   
  ELSEIF  (payment_method = 'wallet') then 
   set remaining_amount = fare_amount - user_balance;
 	
 if (previous_remaining = 0 and remaining_amount <= 0) then 
   Update users set total_amount=total_amount - fare_amount  where id=user_id ;
Insert into user_transactions (user_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (user_id,ride_id,'3',fare_amount,'Ride transaction','1');
set str_amount = concat('- SAR ',fare_amount);
Insert into user_wallet_details(user_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(user_id,ride_id,'Ride transaction','3',str_amount,fare_amount,'debit',now(),'1');
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (driver_id,ride_id,'12',driver_share,'Driver ride share','1');
  set str_amount = concat('+ SAR ',driver_share);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)   
values(driver_id,ride_id,'Driver ride share','12',str_amount,driver_share,'credit',now(),'1');
Update drivers set total_amount=total_amount + driver_share - halal where id=driver_id;
 Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driver_id,ride_id,'13',halal * (-1),'Halal','1');
  set str_amount = concat('- SAR ',halal);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driver_id,ride_id,'Halal','13',str_amount,halal * (-1),'debit',now(),'1');
     
  elseif (previous_remaining = 0 and remaining_amount > 0) then 
   set payment_amount = user_balance;
 set remaining_amount = fare_amount - user_balance;
   set najez_share_cal=(fare_amount*najez_share_percentage);
   set a=payment_amount-najez_share_cal;
  Update users set total_amount = total_amount - fare_amount  where id=user_id;
    Insert into user_transactions (user_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (user_id,ride_id,'3',fare_amount,'ride transaction','1');
set str_amount = concat('- SAR ',fare_amount);
Insert into user_wallet_details(user_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(user_id,ride_id,'Ride transaction','3',str_amount,fare_amount,'debit',now(),'1');
Insert into driver_pending_balance(ride_id,user_id,driver_id,amount,ride_type,status) 
values(ride_id,user_id,driver_id,remaining_amount,'2','1');
 Update drivers set pending_amount=pending_amount + remaining_amount where id=driver_id;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driver_id,ride_id,'13',halal * (-1),'Halal','1');
  set str_amount = concat('- SAR ',halal);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driver_id,ride_id,'Halal','13',str_amount,halal * (-1),'debit',now(),'1');

if a > 0 then 
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (driver_id,ride_id,'12',a,'Driver ride share','1');
  set str_amount = concat('+ SAR ',a);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)   
values(driver_id,ride_id,'Driver ride share','12',str_amount,a,'credit',now(),'1');
Update drivers set total_amount=total_amount + a - halal where id=driver_id;

elseif a < 0 then 
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (driver_id,ride_id,'11',a,'Najez share','1');
  set str_amount = concat('- SAR ',(a*(-1)));
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)   
values(driver_id,ride_id,'Najez share','11',str_amount,a,'debit',now(),'1');
Update drivers set total_amount=total_amount + a - halal where id=driver_id;
	End IF;
	End If; 
                                                            
End IF;

	
	Insert into najez_transactions(ride_id,amount) values(ride_id,najez_share);
Update najez set total_amount = total_amount + najez_share where id='1';


If driver_referal_id !=0 and driver_referal_amount !=0 then 
IF EXISTS (Select id from driver_referal_code where driver_id =driver_referal_id and referal_driver_id = driver_id and ride_complete > 0) then
       Begin 
IF EXISTS (Select current_balance from driver_referal_code 
where driver_id =driver_referal_id and referal_driver_id = driver_id and ride_complete >= 59) then  
         Begin
      Select current_balance into driver_referal_amount from driver_referal_code 
where driver_id =driver_referal_id and referal_driver_id = driver_id and ride_complete >= 59;
               set total_driver_referal_amount = driver_referal_amount + driver_referal_amount;
      Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
         values (driver_referal_id,ride_id,'4',total_driver_referal_amount,'Driver referal amount','1');
                                set str_amount = concat('+ SAR ',total_driver_referal_amount);
        Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)   
values(driver_referal_id,ride_id,'Referral Amount','4',str_amount,total_driver_referal_amount,'credit',now(),'1');
	Update driver_referal_code set current_balance = 0,ride_complete=ride_complete+1 where driver_id =driver_referal_id and referal_driver_id =driver_id;
   Update drivers set total_amount=total_amount + total_driver_referal_amount where id=driver_referal_id;
     End;
       else 
       BEGIN
      Update driver_referal_code set current_balance = current_balance + driver_referal_amount,ride_complete=ride_complete+1 where driver_id =driver_referal_id and referal_driver_id =driver_id;
       End;  
      End If;
      End;
       else 
       BEGIN
       Update driver_referal_code set current_balance = current_balance + driver_referal_amount,ride_complete=ride_complete+1 where driver_id =driver_referal_id and referal_driver_id =driver_id;
       End;
           End If;
			                                          
end If;
              
			     
			  if (user_referal_id != 0 and user_referal_amount != 0) then
   Insert into user_transactions (user_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
   values (user_referal_id,ride_id,'4',user_referal_amount,'Referral amount','1');
   set str_amount = concat('+ SAR ',user_referal_amount);
   Insert into user_wallet_details(user_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(user_id,ride_id,'Referral Amount','4',str_amount,user_referal_amount,'credit',now(),'1');
  Update users set total_amount=total_amount + user_referal_amount  where id = user_referal_id;
           end If;
			    

ELSEIF (ride_status='4') then 

if user_balance >= fare_amount then 
 Update users set total_amount=total_amount - fare_amount  where id=user_id ;
Insert into user_transactions (user_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (user_id,ride_id,'3',fare_amount,'ride transaction','1');
set str_amount = concat('- SAR ',fare_amount);
Insert into user_wallet_details(user_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(user_id,ride_id,'Ride transaction','3',str_amount,fare_amount,'debit',now(),'1');
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (driver_id,ride_id,'12',driver_share,'Driver ride share','1');
  set str_amount = concat('+ SAR ',driver_share);
  set najez_share_cal=fare_amount-driver_share;
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)   
values(driver_id,ride_id,'Driver ride share','12',str_amount,driver_share,'credit',now(),'1');
Update drivers set total_amount=total_amount + driver_share where id=driver_id;
Insert into najez_transactions(ride_id,amount) values(ride_id,najez_share_cal);
Update najez set total_amount = total_amount + najez_share_cal  where id='1';
 
 else 
 if user_balance < 0 then 
 set remaining_amount=fare_amount;
  set payment_amount = 0;
 else 
  set remaining_amount = fare_amount - user_balance;
  set payment_amount = user_balance;
 End If;
   set najez_share_cal=fare_amount-driver_share;
   set a=payment_amount-najez_share_cal;
  Update users set total_amount = total_amount - payment_amount,cancel_charge = cancel_charge - remaining_amount  where id=user_id ;
 
 Insert into user_transactions (user_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (user_id,ride_id,'3',fare_amount,'ride transaction','1');
set str_amount = concat('- SAR ',fare_amount);
Insert into user_wallet_details(user_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(user_id,ride_id,'Ride transaction','3',str_amount,fare_amount,'debit',now(),'1');
Insert into driver_pending_balance(ride_id,user_id,driver_id,amount,ride_type,status) 
values(ride_id,user_id,driver_id,remaining_amount,'1','1');
Update drivers set pending_amount=pending_amount + remaining_amount where id=driver_id;

if a > 0 then 
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (driver_id,ride_id,'12',a,'Driver ride share','1');
  set str_amount = concat('+ SAR ',a);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)   
values(driver_id,ride_id,'Driver ride share','12',str_amount,a,'credit',now(),'1');
Update drivers set total_amount=total_amount + a where id=driver_id;

elseif a < 0 then 
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status) 
values (driver_id,ride_id,'11',a,'Najez share','1');
  set str_amount = concat('- SAR ',(a*(-1)));
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)   
values(driver_id,ride_id,'Najez share','11',str_amount,a,'debit',now(),'1');
Update drivers set total_amount=total_amount + a where id=driver_id;
	End IF;
	
Insert into najez_transactions(ride_id,amount) values(ride_id,najez_share_cal);
Update najez set total_amount = total_amount + najez_share_cal  where id='1';                            
End IF;

End If;


SET param_sp_success = 1;
COMMIT;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `user_add_money` (IN `userId` INT, IN `payingamount` DOUBLE, IN `transactionId` VARCHAR(50), OUT `param_sp_success` TINYINT)  BEGIN
DECLARE v_finished INTEGER DEFAULT 0;
Declare driverId int;
Declare pendingAmount double default 0;
Declare rideType int;
Declare rideId int;
Declare sattle_amount double;
Declare str_amount varchar(50);
DEClARE cur_pending_balance CURSOR FOR 
Select ride_id,driver_id,amount,ride_type from driver_pending_balance where user_id=userId and status='1' order by id asc;
  DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

START TRANSACTION;
SET param_sp_success = 0;
set sattle_amount=payingamount;
 
 OPEN cur_pending_balance;
 pending_bal: LOOP
 FETCH cur_pending_balance INTO rideId,driverId,pendingAmount,rideType;
 
 IF v_finished = 1 THEN 
 LEAVE pending_bal;
 END IF;

 
 if pendingAmount <= sattle_amount then
 set sattle_amount= sattle_amount - pendingAmount;
if rideType = 1 then 
Update users set cancel_charge = cancel_charge + pendingAmount where id=userId;
else 
Update users set total_amount = total_amount + pendingAmount where id=userId;
End If;
Update driver_pending_balance set status='2' where ride_id=rideId and user_id=userId ;
Insert into driver_clear_balance(ride_id,user_id,driver_id,amount) values(rideId,userId,driverId,pendingAmount);
Update drivers set total_amount=total_amount+pendingAmount ,pending_amount=pending_amount-pendingAmount where id=driverId;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driverId,rideId,'12',pendingAmount,'Driver ride share','1');
  set str_amount = concat('+ SAR ',pendingAmount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driverId,rideId,'Driver ride share','12',str_amount,pendingAmount,'credit',now(),'1');

 
 elseif pendingAmount > sattle_amount then
 if rideType = 1 then 
Update users set cancel_charge = cancel_charge + sattle_amount where id=userId;
else 
Update users set total_amount = total_amount + sattle_amount where id=userId;
End If;
Update driver_pending_balance set amount=amount - sattle_amount where ride_id=rideId and user_id=userId ;
Insert into driver_clear_balance(ride_id,user_id,driver_id,amount) values(rideId,userId,driverId,sattle_amount);
Update drivers set total_amount=total_amount+sattle_amount ,pending_amount=pending_amount-sattle_amount where id=driverId;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driverId,rideId,'12',sattle_amount,'Driver ride share','1');
  set str_amount = concat('+ SAR ',sattle_amount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driverId,rideId,'Driver ride share','12',str_amount,sattle_amount,'credit',now(),'1');

 set sattle_amount=0;
 End If;

 if sattle_amount=0 then 
 LEAVE pending_bal;
 End If;
 END LOOP pending_bal;
 CLOSE cur_pending_balance;
 
 if sattle_amount > 0 then 
 Update users set total_amount = total_amount + sattle_amount where id=userId;
 End If;
 
Insert into user_transactions(user_id,transaction_type_id,transaction_amount,transaction_detail,transaction_id,status)
            values(userId,'2',payingamount,'Add Money',transactionId,'1') ;
               set str_amount = concat('+ SAR ',payingamount);
Insert into user_wallet_details(user_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(userId,'Add Money','2',str_amount,payingamount,'credit',now(),'1');

SET param_sp_success = 1;
COMMIT;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `user_offline_voucher` (IN `userId` INT, IN `voucher_amount` DOUBLE, IN `voucher_id` INT, OUT `param_sp_success` TINYINT)  BEGIN
DECLARE v_finished INTEGER DEFAULT 0;
Declare driverId int;
Declare pendingAmount double;
Declare rideType int;
Declare rideId int;
Declare sattle_amount double;
Declare str_amount varchar(50);
DEClARE cur_pending_balance CURSOR FOR Select ride_id,driver_id,amount,ride_type from driver_pending_balance where user_id=userId and status='1' order by id asc;
  DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

START TRANSACTION;
SET param_sp_success = 0;
set sattle_amount=voucher_amount;
 
 OPEN cur_pending_balance;
 pending_bal: LOOP
 FETCH cur_pending_balance INTO rideId,driverId,pendingAmount,rideType;
 IF v_finished = 1 THEN 
 LEAVE pending_bal;
 END IF;
 
 if pendingAmount <= sattle_amount then
 set sattle_amount= sattle_amount - pendingAmount;
if rideType = 1 then 
Update users set cancel_charge = cancel_charge + pendingAmount where id=userId;
else 
Update users set total_amount = total_amount + pendingAmount where id=userId;
End If;
Update driver_pending_balance set status='2' where ride_id=rideId and user_id=userId ;
Insert into driver_clear_balance(ride_id,user_id,driver_id,amount) values(rideId,userId,driverId,pendingAmount);
Update drivers set total_amount=total_amount+pendingAmount ,pending_amount=pending_amount-pendingAmount where id=driverId;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driverId,rideId,'12',pendingAmount,'Driver ride share','1');
  set str_amount = concat('+ SAR ',pendingAmount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driverId,rideId,'Driver ride share','12',str_amount,pendingAmount,'credit',now(),'1');

 
 else
 if rideType = 1 then 
Update users set cancel_charge = cancel_charge + sattle_amount where id=userId;
else 
Update users set total_amount = total_amount + sattle_amount where id=userId;
End If;
Update driver_pending_balance set amount=amount - sattle_amount where ride_id=rideId and user_id=userId ;
Insert into driver_clear_balance(ride_id,user_id,driver_id,amount) values(rideId,userId,driverId,sattle_amount);
Update drivers set total_amount=total_amount+sattle_amount ,pending_amount=pending_amount-sattle_amount where id=driverId;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driverId,rideId,'12',sattle_amount,'Driver ride share','1');
  set str_amount = concat('+ SAR ',sattle_amount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driverId,rideId,'Driver ride share','12',str_amount,sattle_amount,'credit',now(),'1');
 set sattle_amount=0;
 End If;

 if sattle_amount=0 then 
 LEAVE pending_bal;
 End If;
 END LOOP pending_bal;
 CLOSE cur_pending_balance;
 
 if sattle_amount > 0 then 
 Update users set total_amount = total_amount + sattle_amount where id=userId;
 End If;
Insert into user_transactions(user_id,voucher_id,transaction_type_id,transaction_amount,transaction_detail,status) 
         values(userId,voucher_id,'8',voucher_amount,'offline Coupon','1');
               set str_amount = concat('+ SAR ',voucher_amount);
Insert into user_wallet_details(user_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(userId,'Recharge coupon','8',str_amount,voucher_amount,'credit',now(),'1');
Update voucher set status='0' where id=voucher_id ;
SET param_sp_success = 1;
COMMIT;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `User_transfer` (IN `user_id` INT, IN `t_user_id` INT, IN `t_amount` DOUBLE, IN `desccription` VARCHAR(150), IN `transaction_detail1` VARCHAR(150), IN `transaction_detail2` VARCHAR(150), IN `amount_detail1` VARCHAR(50), IN `amount_detail2` VARCHAR(50), IN `transfer_user` VARCHAR(150), IN `user_name` VARCHAR(150), OUT `param_sp_success` TINYINT)  BEGIN
DECLARE v_finished INTEGER DEFAULT 0;
  DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

START TRANSACTION;
SET param_sp_success = 0;


 Insert into user_transactions(user_id,transfer_userid,transaction_type_id,transaction_amount,transaction_detail,status)
values(user_id,t_user_id,'1',t_amount,transaction_detail1,'1');

Insert into user_wallet_details(user_id,description,transfer_name,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(user_id,transaction_detail1,transfer_user,'1',amount_detail1,t_amount,'debit',now(),'1');

Insert into user_transactions(user_id,transfer_userid,transaction_type_id,transaction_amount,transaction_detail,status)  
values(t_user_id,user_id,'6',t_amount,transaction_detail2,'1');

Insert into user_wallet_details(user_id,description,transfer_name,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status) 
values(t_user_id,transaction_detail2,user_name,'6',amount_detail2,t_amount,'credit',now(),'1');

Update users set total_amount=total_amount-t_amount where id=user_id;
 

SET param_sp_success = 1;
COMMIT;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `user_transfer_amount_sattle` (IN `userId` INT, IN `transfering_amount` DOUBLE, OUT `param_sp_success` TINYINT)  BEGIN
DECLARE v_finished INTEGER DEFAULT 0;
Declare driverId int;
Declare pendingAmount double;
Declare rideType int;
Declare rideId int;
Declare sattle_amount double;
Declare str_amount varchar(50);
DEClARE cur_pending_balance CURSOR FOR Select ride_id,driver_id,amount,ride_type from driver_pending_balance where user_id=userId and status='1' order by id asc;
  DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

START TRANSACTION;
SET param_sp_success = 0;
set sattle_amount=transfering_amount;
 
 OPEN cur_pending_balance;
 pending_bal: LOOP
 FETCH cur_pending_balance INTO rideId,driverId,pendingAmount,rideType;
 IF v_finished = 1 THEN 
 LEAVE pending_bal;
 END IF;
 
 if pendingAmount <= sattle_amount then
 set sattle_amount= sattle_amount - pendingAmount;
if rideType = 1 then 
Update users set cancel_charge = cancel_charge + pendingAmount where id=userId;
else 
Update users set total_amount = total_amount + pendingAmount where id=userId;
End If;
Update driver_pending_balance set status='2' where ride_id=rideId and user_id=userId ;
Insert into driver_clear_balance(ride_id,user_id,driver_id,amount) values(rideId,userId,driverId,pendingAmount);
Update drivers set total_amount=total_amount+pendingAmount ,pending_amount=pending_amount-pendingAmount where id=driverId;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driverId,rideId,'12',pendingAmount,'Driver ride share','1');
  set str_amount = concat('+ SAR ',pendingAmount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driverId,rideId,'Driver ride share','12',str_amount,pendingAmount,'credit',now(),'1');

 
 else
 if rideType = 1 then 
Update users set cancel_charge = cancel_charge + sattle_amount where id=userId;
else 
Update users set total_amount = total_amount + sattle_amount where id=userId;
End If;
Update driver_pending_balance set amount=amount - sattle_amount where ride_id=rideId and user_id=userId ;
Insert into driver_clear_balance(ride_id,user_id,driver_id,amount) values(rideId,userId,driverId,sattle_amount);
Update drivers set total_amount=total_amount+sattle_amount ,pending_amount=pending_amount-sattle_amount where id=driverId;
Insert into driver_transactions (driver_id,ride_id,transaction_type_id,transaction_amount,transaction_detail,status)
values (driverId,rideId,'12',sattle_amount,'Driver ride share','1');
  set str_amount = concat('+ SAR ',sattle_amount);
Insert into driver_wallet_details(driver_id,ride_id,description,transaction_type_id,amount,amount_num,transaction_mode,transaction_datetime,status)  
values(driverId,rideId,'Driver ride share','12',str_amount,sattle_amount,'credit',now(),'1');
 set sattle_amount=0;
 End If;

 if sattle_amount=0 then 
 LEAVE pending_bal;
 End If;
 END LOOP pending_bal;
 CLOSE cur_pending_balance;
 
 if sattle_amount > 0 then 
 Update users set total_amount = total_amount + sattle_amount where id=userId;
 End If;

SET param_sp_success = 1;
COMMIT;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `app_setting`
--

CREATE TABLE `app_setting` (
  `id` int(12) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `userid` int(12) DEFAULT '0',
  `show_phone` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `backend_logs`
--

CREATE TABLE `backend_logs` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL DEFAULT '0',
  `action` varchar(255) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `backend_logs`
--

INSERT INTO `backend_logs` (`id`, `user_id`, `action`, `datetime`) VALUES
(1, 'admin', 'just logged in', '2017-12-18 07:26:21'),
(2, 'admin', 'just logged in', '2017-12-18 08:51:19'),
(3, 'admin', 'just logged in', '2017-12-18 13:01:43'),
(4, 'admin', 'just logged in', '2017-12-19 07:27:03'),
(5, 'admin', 'just logged in', '2017-12-20 09:29:30'),
(6, 'admin', 'just logged in', '2017-12-20 11:10:58'),
(7, 'admin', 'just logged in', '2017-12-20 14:47:41'),
(8, 'admin', 'just logged in', '2017-12-21 06:35:35'),
(9, 'admin', 'just logged in', '2017-12-21 08:58:24'),
(10, 'admin', 'just logged in', '2017-12-21 14:42:33'),
(11, 'admin', 'just logged in', '2017-12-22 10:44:19'),
(12, 'admin', 'just logged in', '2017-12-25 07:51:55'),
(13, 'admin', 'just logged in', '2017-12-25 07:56:00'),
(14, 'admin', 'just logged in', '2017-12-25 19:46:50'),
(15, 'admin', 'just logged in', '2017-12-26 06:50:27'),
(16, 'admin', 'just logged in', '2017-12-27 10:50:40'),
(17, 'admin', 'just logged in', '2017-12-27 12:17:11'),
(18, 'admin', 'just logged in', '2017-12-29 05:56:39'),
(19, 'admin', 'just logged in', '2017-12-29 10:46:10'),
(20, 'admin', 'just logged in', '2017-12-29 10:51:24'),
(21, 'admin', 'just logged in', '2017-12-29 11:27:06'),
(22, 'admin', 'just logged in', '2017-12-29 11:38:33'),
(23, 'admin', 'just logged in', '2017-12-31 07:47:36'),
(24, 'admin', 'just logged in', '2017-12-31 07:47:36'),
(25, 'admin', 'just logged in', '2018-01-03 09:56:46'),
(26, 'admin', 'just logged in', '2018-01-03 11:39:39'),
(27, 'admin', 'just logged in', '2018-01-03 12:45:06'),
(28, 'admin', 'just logged out', '2018-01-03 13:15:42'),
(29, 'admin', 'just logged in', '2018-01-03 13:54:09'),
(30, 'admin', 'just logged in', '2018-01-04 06:21:08'),
(31, 'admin', 'just logged in', '2018-01-04 06:57:11'),
(32, 'admin', 'just logged in', '2018-01-05 12:29:20'),
(33, 'admin', 'just logged in', '2018-01-05 12:32:20'),
(34, 'admin', 'just logged out', '2018-01-05 12:32:32'),
(35, 'admin', 'just logged in', '2018-01-05 13:54:38'),
(36, 'admin', 'just logged in', '2018-01-07 06:14:12'),
(37, 'admin', 'just logged in', '2018-01-07 06:22:44'),
(38, 'admin', 'just logged in', '2018-01-07 08:44:42'),
(39, 'admin', 'just logged in', '2018-01-08 07:29:01'),
(40, 'admin', 'just logged in', '2018-01-08 13:39:27'),
(41, 'admin', 'just logged in', '2018-01-09 05:30:47'),
(42, 'admin', 'just logged in', '2018-01-09 09:28:12'),
(43, 'admin', 'just logged in', '2018-01-09 11:17:21'),
(44, 'admin', 'just logged in', '2018-01-11 05:16:07'),
(45, 'admin', 'just logged out', '2018-01-11 06:51:41'),
(46, 'admin', 'just logged in', '2018-01-11 10:04:51'),
(47, 'admin', 'just logged out', '2018-01-11 10:07:10'),
(48, 'admin', 'just logged in', '2018-01-11 10:11:43'),
(49, 'admin', 'just logged in', '2018-01-12 06:50:50'),
(50, 'admin', 'just logged in', '2018-01-12 12:34:42'),
(51, 'admin', 'just logged in', '2018-01-15 07:07:48'),
(52, 'admin', 'just logged in', '2018-01-15 13:28:45'),
(53, 'admin', 'just logged in', '2018-01-16 07:08:26'),
(54, 'admin', 'just logged in', '2018-01-16 08:29:33'),
(55, 'admin', 'just logged in', '2018-01-16 09:52:15'),
(56, 'admin', 'just logged in', '2018-01-18 10:58:08'),
(57, 'admin', 'just logged in', '2018-01-19 05:49:43'),
(58, 'admin', 'just logged out', '2018-01-19 05:49:54'),
(59, 'admin', 'just logged in', '2018-01-19 05:53:54'),
(60, 'admin', 'just logged in', '2018-01-19 06:35:31'),
(61, 'admin', 'just logged out', '2018-01-19 06:36:00'),
(62, 'admin', 'just logged in', '2018-01-19 06:36:56'),
(63, 'admin', 'just logged in', '2018-01-19 10:29:23'),
(64, 'admin', 'just logged in', '2018-02-08 12:45:19'),
(65, 'admin', 'just logged in', '2018-03-06 14:24:08'),
(66, 'admin', 'just logged in', '2018-03-16 12:22:36'),
(67, 'admin', 'just logged in', '2018-03-19 09:46:25'),
(68, 'admin', 'just logged in', '2018-03-20 10:17:42'),
(69, 'admin', 'just logged in', '2018-03-21 09:24:02'),
(70, 'admin', 'just logged in', '2018-03-22 11:33:27'),
(71, 'admin', 'just logged in', '2018-03-22 16:03:38'),
(72, 'admin', 'just logged in', '2018-03-23 17:49:47'),
(73, 'admin', 'just logged in', '2018-03-27 14:08:21'),
(74, 'admin', 'just logged in', '2018-03-28 11:25:08');

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `unique_id` varchar(255) DEFAULT NULL,
  `emailid` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `fname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(255) CHARACTER SET utf8 NOT NULL,
  `gender` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `photo` longtext,
  `mobile` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `user_type` int(12) DEFAULT NULL,
  `user_role` varchar(50) DEFAULT NULL,
  `status` int(12) NOT NULL DEFAULT '1',
  `resetcode` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backend_users`
--

INSERT INTO `backend_users` (`id`, `username`, `unique_id`, `emailid`, `password`, `fname`, `lname`, `gender`, `photo`, `mobile`, `address`, `active`, `user_type`, `user_role`, `status`, `resetcode`, `created`) VALUES
(1, 'superadmin', NULL, 'admin@techconlabs.com', 'superadmin', 'admin', 'admin', 'male', 'assets/documents/user-icon.png', NULL, NULL, 1, 1, '1,2,3,4,5,6,7,8,9,10', 1, '632707748', '2017-07-11 00:00:00'),
(12, 'Haider', 'USR1424', 'hbonagh@yahoo.com', 'Haider996', 'Haider', 'Albonagh', 'male', 'assets/documents/IMG_78806.JPG', '0543558931', NULL, 1, 2, '1,2,3,4,5,6,7,8,9,10', 1, NULL, '0000-00-00 00:00:00'),
(15, 'shiwanshu.mishra@techconlabs.com', 'USR6597', 'shiwanshu.mishra@techconlabs.com', 'shashi@1992', 'Shiwanshu', 'Mishra', 'male', NULL, '9617858550', NULL, 0, 2, '1,2,3,4,5,6,7,8,9,10,11,12', 0, '948245416', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bank_list`
--

CREATE TABLE `bank_list` (
  `id` int(12) NOT NULL,
  `bank_name` longtext,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_list`
--

INSERT INTO `bank_list` (`id`, `bank_name`, `created`) VALUES
(2, 'Alinma Bank', '2017-08-09 01:04:25'),
(3, 'Al Rajhi Bank', '2017-08-09 01:04:46'),
(4, 'Arab National Bank', '2017-08-09 01:04:57'),
(5, 'Asia Bank', '2017-08-09 01:05:08'),
(6, 'Banque Saudi Fransi', '2017-08-09 01:05:17'),
(7, 'Bank Al-Bilad', '2017-08-09 01:05:25'),
(8, 'BNP Paribas', '2017-08-10 12:08:24'),
(10, 'Deutsche Bank', '2017-08-12 00:34:17'),
(11, 'Emirates NBD', '2017-08-12 00:34:23'),
(12, 'Gulf International Bank (GIB)', '2017-08-12 00:34:30'),
(13, 'Industrial and Commercial Bank of China (ICBC)', '2017-08-12 00:34:39'),
(14, 'J.P. Morgan Chase N.A', '2017-08-12 00:34:46'),
(15, 'Muscat Bank', '2017-08-12 00:34:59'),
(16, 'National Bank of Bahrain (NBB)', '2017-08-12 00:35:05'),
(17, 'National Bank of Kuwait (NBK)', '2017-08-12 00:35:11'),
(18, 'National Bank of Pakistan (NBP)', '2017-08-12 00:35:15'),
(19, 'Qatar National Bank (QNB)', '2017-08-12 00:35:20'),
(20, 'Riyad Bank', '2017-08-12 00:35:24'),
(21, 'Samba Financial Group (Samba)', '2017-08-12 00:35:29'),
(22, 'Saudi Hollandi Bank (Alawwal)', '2017-08-12 00:35:34'),
(23, 'Saudi Investment Bank', '2017-08-12 00:35:41'),
(24, 'State Bank of India (SBI)', '2017-08-12 00:35:46'),
(25, 'T.C.ZIRAAT BANKASI A.S.', '2017-08-12 00:35:53'),
(26, 'The National Commercial Bank', '2017-08-12 00:35:55'),
(27, 'The Saudi British Bank', '2017-08-12 00:36:00');

-- --------------------------------------------------------

--
-- Table structure for table `card_detail`
--

CREATE TABLE `card_detail` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `card_number` longtext,
  `expire_month` varchar(255) DEFAULT NULL,
  `expire_year` varchar(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `card_username` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `card_detail`
--

INSERT INTO `card_detail` (`id`, `userid`, `type`, `card_number`, `expire_month`, `expire_year`, `card_type`, `card_username`, `created`) VALUES
(1, 1, 'driver', '4544523556556565', '4', '19', 'Credit/Debit', 'fjfjfd', '2018-01-08 13:23:42'),
(3, 7, 'user', '6666889999999696', '05', '21', 'card type', 'bnnn', '2018-01-10 07:26:10'),
(5, 9, 'driver', '22558888', '05', '21', 'card type', 'raman', '2018-01-10 07:38:45'),
(8, 2, 'driver', '5558555555855549', '08', '23', 'card type', 'dgghhh', '2018-01-11 13:21:27'),
(9, 2, 'driver', '55566', '4', '21', 'card type', 'cghhjjhg', '2018-01-12 09:42:17'),
(10, 2, 'driver', '5655556655552555', '2', '19', 'card type', 'fghhu', '2018-01-12 10:01:02'),
(11, 2, 'driver', '5655555555555556', '6', '25', 'card type', 'ghjj', '2018-01-12 10:01:46');

-- --------------------------------------------------------

--
-- Table structure for table `city_master`
--

CREATE TABLE `city_master` (
  `id` int(11) NOT NULL,
  `city_name_en` varchar(255) DEFAULT NULL,
  `city_name_ar` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `long_trip` int(1) DEFAULT '0',
  `delivery` int(1) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `location_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city_master`
--

INSERT INTO `city_master` (`id`, `city_name_en`, `city_name_ar`, `long_trip`, `delivery`, `created`, `location_data`) VALUES
(1, 'Riyadh', 'ریاض', 1, 0, '2017-07-21 02:55:31', 'a:2:{s:3:\"lat\";d:24.7135517;s:3:\"lng\";d:46.675295699999999;}'),
(2, 'Jeddah', 'الجدة', 0, 0, '2017-07-21 02:55:31', ''),
(3, 'Khobar', 'الخبر', 1, 0, '2017-07-21 03:01:30', 'a:2:{s:3:\"lat\";d:26.217190599999999;s:3:\"lng\";d:50.197138099999997;}'),
(4, 'Jubail', 'الجبيل', 1, 0, '2017-07-21 03:02:05', 'a:2:{s:3:\"lat\";d:26.959770899999999;s:3:\"lng\";d:49.568741600000003;}'),
(7, 'Qassim', 'القصيم', 0, 0, '2017-07-21 03:02:05', ''),
(8, 'Qatif', 'القطيف', 0, 0, '2017-08-09 00:55:31', ''),
(9, 'Khafji', 'الخفجي', 0, 0, '2017-08-09 00:55:51', ''),
(10, 'Kharj', 'الخرج', 0, 0, '2017-08-09 00:56:21', ''),
(11, 'Hafar Al-Batin', 'حفر الباطن', 0, 0, '2017-08-09 00:56:49', ''),
(12, 'Hassa', 'الأحساء', 1, 0, '2017-08-09 00:57:10', 'a:2:{s:3:\"lat\";d:36.798634999999997;s:3:\"lng\";d:36.521963;}'),
(13, 'Dhahran', 'الظهران', 0, 0, '2017-08-09 00:57:43', ''),
(14, 'Dammam', 'دمام', 0, 0, '2017-08-09 00:57:55', ''),
(15, 'Bahrain', 'البحرين', 0, 0, '2017-08-09 00:58:10', '');

-- --------------------------------------------------------

--
-- Table structure for table `cms_log`
--

CREATE TABLE `cms_log` (
  `id` int(11) NOT NULL,
  `log_key` varchar(255) DEFAULT NULL,
  `log_val` longtext,
  `created` datetime NOT NULL,
  `backend_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `unique_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `emailid` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `fname` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `mname` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `lname` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `dob` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `photo` longtext CHARACTER SET latin1,
  `mobile` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `iqama` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `iban` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `bank` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `work_city` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `captain_code` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `latlng` longtext CHARACTER SET latin1,
  `can_driver_deliver` int(11) DEFAULT '0',
  `device_token` longtext CHARACTER SET latin1,
  `referralcode` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `for_bahrain` int(1) NOT NULL DEFAULT '0',
  `is_referred` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `webblock` int(12) DEFAULT '0',
  `total_amount` double(11,2) DEFAULT '0.00',
  `pending_amount` double(11,2) DEFAULT '0.00',
  `total_rides` int(12) DEFAULT '0',
  `online_flag` int(11) DEFAULT '0',
  `ride_flag` int(11) DEFAULT '0',
  `vehicle_type` int(11) DEFAULT '0',
  `online_vehicle_id` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `verified` int(11) DEFAULT '0',
  `resetcode` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `resident` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `driver_license_no` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `expiry_date` datetime(3) DEFAULT NULL,
  `latitude` longtext CHARACTER SET latin1,
  `longitude` longtext CHARACTER SET latin1,
  `chatid` varchar(255) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `username`, `unique_id`, `emailid`, `password`, `fname`, `mname`, `lname`, `gender`, `age`, `dob`, `photo`, `mobile`, `iqama`, `iban`, `bank`, `work_city`, `captain_code`, `latlng`, `can_driver_deliver`, `device_token`, `referralcode`, `for_bahrain`, `is_referred`, `webblock`, `total_amount`, `pending_amount`, `total_rides`, `online_flag`, `ride_flag`, `vehicle_type`, `online_vehicle_id`, `status`, `verified`, `resetcode`, `created`, `resident`, `driver_license_no`, `expiry_date`, `latitude`, `longitude`, `chatid`) VALUES
(1, 'Fghgh', 'DRV3280', 'test@mail.com', '78907890', 'Fghgh', 'Fdbh', '', 'Male', NULL, '05/01/1998', 'assets/documents/5a4f83397032f.jpg', '919030090300', '637774777', 'dh583636663663763)636???36', 'Bank Al-Bilad', 'Hafar Al-Batin', NULL, '[\"28.443397\",\"77.055872\"]', 0, NULL, 'FGH1219', 0, '', 1, -42.50, 0.00, 0, 0, 0, 3, 7, 1, 1, NULL, '2018-01-05 13:52:57', 'assets/documents/5a4f833970881.jpg', 'assets/documents/5a4f833970bde.jpg', NULL, '28.443463', '77.056013', '40289803'),
(2, 'anddriver', 'DRV4576', 'testdriverand@mail.com', '67896789', 'anddriver', 'tezt', '', 'Male', NULL, '5/1/1998', 'assets/documents/5a4f89214e21f.jpg', '9669230092300', '6737637663', 'gdbv6476477477376476376638', 'BNP Paribas', 'Khafji', NULL, '[\"\",\"\"]', 0, NULL, 'AND5319', 0, '', 1, 158.00, 0.00, 0, 1, 0, 2, 2, 1, 1, NULL, '2018-01-05 14:18:09', 'assets/documents/5a4f89214e3b2.jpg', 'assets/documents/5a4f89214e512.jpg', NULL, '28.5941124', '77.3075487', NULL),
(3, 'raman', 'DRV2663', 'rn@gmail.com', '900900900', 'raman', 'd', 'dhiman', 'Male', NULL, '5/1/1998', 'assets/documents/5a4f8db67e7a9.jpg', '966499448746', '6789oooo77', 'cjjf617w829w9283u363733383', 'Banque Saudi Fransi', 'Bahrain', NULL, '[\"\",\"\"]', 0, NULL, 'RAM5117', 0, '', 1, 0.00, 0.00, 0, 1, 0, 3, 3, 1, 1, NULL, '2018-01-05 14:37:42', 'assets/documents/5a4f8db67e9ad.jpg', 'assets/documents/5a4f8db67eb2e.jpg', NULL, '28.4431494', '77.0558377', NULL),
(4, 'K', 'DRV9486', 'k@k.com', '1111111111', 'K', 'b', '', 'Male', NULL, '11/6/1997', 'assets/documents/5a51b558b3c24.jpg', '9661111111111', '1111111111', 'SA123400000000000000000000', 'Deutsche Bank', 'Dammam', NULL, '[\"\",\"\"]', 0, NULL, 'K3817', 0, '', 1, 8.50, 0.00, 0, 1, 0, 3, 4, 1, 1, NULL, '2018-01-07 05:51:20', 'assets/documents/5a51b558b3e1b.jpg', 'assets/documents/5a51b558b3fc3.jpg', NULL, '0.0', '0.0', NULL),
(5, 'Mohammed', 'DRV3500', 'najez.2017@gmail.com', 'M769200m', 'Mohammed', 'Ab', 'Alu', 'Male', NULL, '7/1/1979', 'assets/documents/5a51dba83a075.jpg', '966554804848', '1071878092', 'sa123456789078965412365478', 'Alinma Bank', 'Khobar', NULL, '[\"\",\"\"]', 0, NULL, 'MOH6984', 0, '', 1, 0.00, 0.00, 0, 1, 0, 1, 5, 1, 1, NULL, '2018-01-07 08:34:48', 'assets/documents/5a51dba83a2a8.jpg', 'assets/documents/5a51dba83a47a.jpg', NULL, '26.3148195', '50.2235774', NULL),
(6, 'najzdriver', 'DRV5139', 'najz@mail.com', '5432154321', 'najzdriver', 'test', '', 'Male', NULL, '8/1/1998', 'assets/documents/5a531c7ec21ee.jpg', '9669630096300', '8453666746', 'fg2ryyru746637366636646346', 'Bank Al-Bilad', '??????', NULL, '[\"\",\"\"]', 0, NULL, 'NAJ4277', 0, '', 1, 9.50, 0.00, 0, 0, 0, 2, 6, 1, 1, NULL, '2018-01-08 07:23:42', 'assets/documents/5a531c7ec23b7.jpg', 'assets/documents/5a531c7ec2560.jpg', NULL, '28.4433867', '77.0558755', NULL),
(7, 'raman', 'DRV4383', 'h@gmail.com', '900900900', 'raman', 'h', 'd', 'Male', NULL, '8/1/1998', 'assets/documents/5a535033029fe.jpg', '9669988556633', '838373y363', 'ufhf848r8r4494948755njjjjj', 'National Bank of Pakistan (NBP)', '?????', NULL, '[\"\",\"\"]', 0, NULL, 'RAM3186', 0, '', 1, 0.00, 0.00, 0, 0, 0, 3, 10, 1, 1, NULL, '2018-01-08 11:04:19', 'assets/documents/5a53503302bac.jpg', 'assets/documents/5a53503302d20.jpg', NULL, '28.4431419', '77.0558376', NULL),
(8, 'Raman', 'DRV1179', 'aaman@gmail.com', '900900900', 'Raman', 'D', 'Ss', 'Female', NULL, '09/01/1998', 'assets/documents/5a5450ded0795.jpg', '91946446464', 'nsndhdhdld', 'didhdhdjdodjfjfldldjdhdkwo', 'Alinma Bank', 'Bahrain', NULL, '[\"28.443663\",\"77.055942\"]', 0, NULL, 'RAM5136', 0, '', 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 1, NULL, '2018-01-09 05:19:26', 'assets/documents/5a5450ded08e1.jpg', 'assets/documents/5a5450ded09e1.jpg', NULL, NULL, NULL, '40509104'),
(9, 'raman', 'DRV9017', 'ramands@gmail.com', '900900900', 'jackson', 'd', 's', 'Male', NULL, '9/1/1998', 'assets/documents/5a5491258e8f7.jpg', '9669229229229', 'hh748riirr', 'jdhhrjeowieu3737737e7e7e73', '0', 'Hassa', NULL, '[\"\",\"\"]', 0, NULL, 'RAM7993', 0, '', 1, -1064.50, 0.00, 0, 1, 0, 1, 12, 1, 1, NULL, '2018-01-09 09:53:41', 'assets/documents/5a5491258eaa3.jpg', 'assets/documents/5a5491258ec24.jpg', NULL, '0.0', '0.0', NULL),
(10, 'ankit', 'DRV8633', 'ankit@gmail.com', '900900900', 'ankit', 'd', 'mish', 'female', NULL, '9/1/1998', 'assets/documents/5a5495fa787d6.jpg', '9668228228228', 'hhs8888766', 'dhdg7e733738339939383833', 'Industrial and Commercial Bank of China (ICBC)', 'Bahrain', NULL, '[\"\",\"\"]', 1, NULL, 'ANK4347', 0, '', 1, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 1, NULL, '2018-01-09 10:14:18', '', '', NULL, '28.4438578', '77.0560966', NULL),
(11, 'Drivertest', 'DRV2914', 'drivertest@mail.com', '34563456', 'Drivertest', 'Driver', '', 'Male', NULL, '09/01/1998', 'assets/documents/5a549fb4e748e.jpg', '919530095300', '7366376376', 'vhu63663764663763663663736', 'Banque Saudi Fransi', 'Hassa', NULL, '[\"28.443504\",\"77.055976\"]', 0, NULL, 'DRI7223', 0, '', 1, 5.00, 12.00, 0, 0, 0, 3, 14, 1, 1, NULL, '2018-01-09 10:55:48', 'assets/documents/5a549fb4e7871.jpg', 'assets/documents/5a549fb4e7c0d.jpg', NULL, '26.315330', '50.183314', '40521116'),
(12, 'Testdrover', 'DRV5548', 'chbs@mail.com', '76547654', 'Testdrover', 'Hdbvd', '', 'Male', NULL, '09/01/1996', 'assets/documents/5a54a0ad65954.jpg', '918025680256', '7468436638', 'gbh63636736637377628368396', 'Emirates NBD', 'Hafar Al-Batin', NULL, '[\"28.443596\",\"77.055970\"]', 0, NULL, 'TES5092', 0, 'DRI7223', 1, -34.50, 0.00, 0, 0, 0, 3, 15, 1, 1, NULL, '2018-01-09 10:59:57', 'assets/documents/5a54a0ad65eca.jpg', 'assets/documents/5a54a0ad66273.jpg', NULL, '28.443399', '77.055876', '40521275'),
(13, 'jacksonmat', 'DRV6867', 'jack@gmail.com', '900900900', 'jacksonmat', 'g', 'mathews', 'Male', NULL, '9/1/1998', 'assets/documents/5a54a9fd6e631.jpg', '9662292292299', 'zhhsgsheke', 'hsdndkffdkdlskskjjdjdhdjdj', 'Emirates NBD', 'Jeddah', NULL, '[\"\",\"\"]', 0, NULL, 'JAC6290', 0, 'hahshzdd', 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-01-09 11:39:41', 'assets/documents/5a54a9fd6e811.jpg', 'assets/documents/5a54a9fd6e9b3.jpg', NULL, NULL, NULL, NULL),
(14, 'driver', 'DRV7891', 'najdriver@mail.com', '1234567890', 'driver', 'najez', '', NULL, NULL, '11/6/1974', 'assets/documents/driver.png', '919350935000', NULL, 'Sd156375376575783578457866i876237489', 'Industrial and Commercial Bank of China (ICBC)', 'Khafji', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-01-15 13:31:55', NULL, NULL, NULL, '26.313174', '50.222031', NULL),
(15, 'nandini', 'DRV9586', 'n@mail.com', 'n123456', 'nandini', 'shobh', 'rai', NULL, NULL, '18/9/1952', 'assets/documents/dum.png', '9898989899', '', 'nana1232323213213232', '0', 'Qassim', NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-16 12:44:09', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'john', 'DRV2414', 'j@mail.com', 'j12345', 'john', 'jh', 'doe', NULL, NULL, '18/7/1953', NULL, '098878787878', '', 'aaaaaaaaaaa1111111111111111dd', '0', 'Khafji', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-16 12:50:13', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'johnn', 'DRV6830', 'jj@mail.com', '123456hh', 'johnn', 'john', 'doe', NULL, NULL, '17/4/1953', NULL, '98989898989', NULL, '12345565rr', 'Muscat Bank', 'Dammam', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-16 13:01:31', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'nandu', 'DRV3772', 'n@gmail.com', '123456', 'nandu', 'nandini', 'rai', NULL, NULL, '17/2/1953', 'assets/documents/dum1.png', '1234567890', NULL, '1111111111aaaaaaaaaa', 'National Bank of Bahrain (NBB)', 'Hassa', NULL, NULL, 0, NULL, NULL, 0, NULL, 1, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-16 15:40:02', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'gajni', 'DRV2567', 'gajni@gmail.com', 'g12345', 'gajni', 'gajju', 'ganjawala', NULL, NULL, '17/8/1951', 'assets/documents/dum8.png', '1234123444', NULL, '111wq1a1', 'National Bank of Pakistan (NBP)', 'Bahrain', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-16 16:51:02', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'test', 'DRV9083', 'test@gmail.com', '123456', 'test', 'test1', 'test2', NULL, NULL, '16/4/1952', 'assets/documents/Koala.jpg', '9869393912', NULL, '111111aaaaa111111111aaasa', 'Industrial and Commercial Bank of China (ICBC)', 'Khobar', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-19 12:18:39', NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'jony', 'DRV6183', 'jo@mail.com', '123456', 'jony', 'john', 'jon', NULL, NULL, '12/5/1952', 'assets/documents/Chrysanthemum.jpg', '9898989898', NULL, 'aaaaaaaaa12121212121aas213', 'National Bank of Bahrain (NBB)', 'Kharj', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-19 17:35:30', NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'ritu', 'DRV9949', 'r@mail.com', 'r12345', 'ritu', 's', 'rai', NULL, NULL, '10/9/1953', 'assets/documents/Desert.jpg', '0909898989', NULL, 'nnnnnnnnnn33333333333', 'J.P. Morgan Chase N.A', 'Khafji', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-20 10:56:00', NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'nini', 'DRV4047', 'nnn@mail.com', 'nan123123', 'nini', 'ni', 'nnnnn', NULL, NULL, '14/8/1951', 'assets/documents/Jellyfish1.jpg', '+971 666666688', '', 'aaaaaaaaaa1111111111ddd', '0', 'Khobar', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-20 14:08:10', NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'rakesh', 'DRV4953', 'ra@gmail.com', 'ra12345', 'rakesh', 'budd', 'budhlakoti', NULL, NULL, '15/7/1952', 'assets/documents/Koala1.jpg', '+1 (232) 242-432', NULL, '243erwr', 'National Bank of Bahrain (NBB)', 'Qatif', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-20 14:51:57', NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'dvxzcv', 'DRV5207', 'dvvdzxvxzvxcv@mail.com', '1111111', 'dvxzcv', 'dvdv', 'dvdvdcxv', NULL, NULL, '29/8/1951', 'assets/documents/Hydrangeas1.jpg', '+1 (342) 343-24', NULL, '111wdsads', 'National Bank of Bahrain (NBB)', 'Khobar', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-20 14:54:16', NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'qqqq', 'DRV9010', 'aa@gmail.com', '111111', 'qqqq', 'qqsqasd', 'vxcvcxvxcv', NULL, NULL, '29/9/1952', 'assets/documents/Hydrangeas2.jpg', '+1 (121) 212-1', NULL, '121dasasa', 'Muscat Bank', 'Khafji', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-20 14:55:45', NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'nida', 'DRV1400', 'nida@mail.com', '111111', 'nida', 'nidaa', 'nidaaa', NULL, NULL, '13/6/1951', 'assets/documents/Chrysanthemum1.jpg', '+1 (121) 212-1212', NULL, '12121qsqsqs', 'J.P. Morgan Chase N.A', 'Khafji', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-22 16:17:43', NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'joh', 'DRV4971', 'jd@mail.com', 'jj1234', 'joh', 'jo', 'doe', NULL, NULL, '14/8/1953', 'assets/documents/countrycode.GIF', '+1 (452) 35', '', 'vzvcvc', '0', 'Khobar', NULL, NULL, 0, NULL, NULL, 0, NULL, 0, 0.00, 0.00, 0, 0, 0, 0, 0, 1, 0, NULL, '2018-03-27 14:45:14', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `driver_clear_balance`
--

CREATE TABLE `driver_clear_balance` (
  `id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `amount` double(11,2) DEFAULT '0.00',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_clear_balance`
--

INSERT INTO `driver_clear_balance` (`id`, `ride_id`, `user_id`, `driver_id`, `amount`, `created`) VALUES
(1, 1, 1, 1, 8.00, '2018-01-05 13:59:52'),
(2, 5, 2, 1, 8.00, '2018-01-05 14:50:32'),
(3, 6, 3, 4, 12.00, '2018-01-07 06:28:44'),
(4, 10, 6, 6, 12.00, '2018-01-08 09:39:00'),
(5, 11, 6, 6, 12.00, '2018-01-08 10:02:20'),
(6, 12, 6, 6, 12.00, '2018-01-08 10:09:40'),
(7, 15, 1, 1, 8.00, '2018-01-08 13:44:06'),
(8, 17, 7, 12, 12.00, '2018-01-09 11:22:49'),
(9, 29, 11, 9, 8.00, '2018-01-10 12:35:19'),
(10, 31, 11, 9, 8.00, '2018-01-10 12:40:09'),
(11, 32, 11, 9, 8.00, '2018-01-10 12:41:20'),
(12, 43, 1, 11, 12.00, '2018-01-11 11:27:46'),
(13, 45, 13, 2, 12.00, '2018-01-11 11:34:55'),
(14, 47, 13, 2, 12.00, '2018-01-11 11:48:32'),
(15, 49, 13, 2, 12.00, '2018-01-11 11:56:02'),
(16, 50, 13, 2, 12.00, '2018-01-11 12:06:28'),
(17, 52, 13, 2, 12.00, '2018-01-11 13:38:15'),
(18, 53, 13, 2, 12.00, '2018-01-11 13:41:44'),
(19, 56, 13, 2, 12.00, '2018-01-12 08:28:20'),
(20, 57, 13, 2, 12.00, '2018-01-12 08:31:17'),
(21, 58, 13, 2, 12.00, '2018-01-12 08:34:18'),
(22, 60, 13, 2, 12.00, '2018-01-12 08:51:18'),
(23, 61, 13, 2, 12.00, '2018-01-12 09:11:11'),
(24, 62, 13, 2, 12.00, '2018-01-12 09:17:33'),
(25, 66, 13, 2, 12.00, '2018-01-12 10:11:34'),
(26, 67, 13, 2, 12.00, '2018-01-12 10:24:28'),
(27, 68, 13, 2, 12.00, '2018-01-12 10:26:20'),
(28, 69, 13, 2, 12.00, '2018-01-12 10:30:11'),
(29, 71, 13, 2, 12.00, '2018-01-12 10:49:06'),
(30, 73, 13, 2, 12.00, '2018-01-12 10:59:26'),
(31, 74, 13, 2, 12.00, '2018-01-12 11:23:18');

-- --------------------------------------------------------

--
-- Table structure for table `driver_feedback`
--

CREATE TABLE `driver_feedback` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `decription` varchar(255) DEFAULT NULL,
  `feedback_master_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driver_locations`
--

CREATE TABLE `driver_locations` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `vehicle_type` int(11) DEFAULT NULL,
  `latitude` longtext NOT NULL,
  `longitude` longtext NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ride_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driver_payment_transfer`
--

CREATE TABLE `driver_payment_transfer` (
  `id` int(11) NOT NULL,
  `driverid` int(11) NOT NULL,
  `transfer_driverid` int(11) NOT NULL,
  `transaction_amount` double NOT NULL DEFAULT '0',
  `description` varchar(50) DEFAULT NULL,
  `transaction_mode` varchar(50) DEFAULT NULL COMMENT 'debit,credit',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `driver_pending_balance`
--

CREATE TABLE `driver_pending_balance` (
  `id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `ride_type` tinyint(4) DEFAULT NULL COMMENT '1=>cancel,2=>complete',
  `status` tinyint(4) DEFAULT NULL COMMENT '1=>pending,2=>complete',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_pending_balance`
--

INSERT INTO `driver_pending_balance` (`id`, `ride_id`, `user_id`, `driver_id`, `amount`, `ride_type`, `status`, `created`, `updated`) VALUES
(1, 1, 1, 1, 8, 2, 2, '2018-01-05 13:59:32', '2018-01-05 13:59:52'),
(2, 5, 2, 1, 8, 2, 2, '2018-01-05 14:50:23', '2018-01-05 14:50:32'),
(3, 6, 3, 4, 12, 2, 2, '2018-01-07 06:28:38', '2018-01-07 06:28:44'),
(4, 10, 6, 6, 12, 2, 2, '2018-01-08 09:38:00', '2018-01-08 09:39:00'),
(5, 11, 6, 6, 12, 2, 2, '2018-01-08 10:01:47', '2018-01-08 10:02:20'),
(6, 12, 6, 6, 12, 2, 2, '2018-01-08 10:07:15', '2018-01-08 10:09:40'),
(7, 15, 1, 1, 8, 2, 2, '2018-01-08 13:43:55', '2018-01-08 13:44:06'),
(8, 17, 7, 12, 12, 2, 2, '2018-01-09 11:22:41', '2018-01-09 11:22:49'),
(9, 29, 11, 9, 8, 2, 2, '2018-01-10 12:35:10', '2018-01-10 12:35:19'),
(10, 31, 11, 9, 8, 2, 2, '2018-01-10 12:39:58', '2018-01-10 12:40:09'),
(11, 32, 11, 9, 8, 2, 2, '2018-01-10 12:41:14', '2018-01-10 12:41:20'),
(12, 43, 1, 11, 12, 2, 2, '2018-01-11 11:27:34', '2018-01-11 11:27:46'),
(13, 45, 13, 2, 12, 2, 2, '2018-01-11 11:34:51', '2018-01-11 11:34:55'),
(14, 46, 1, 11, 12, 2, 1, '2018-01-11 11:38:11', '2018-01-11 11:38:11'),
(15, 47, 13, 2, 12, 2, 2, '2018-01-11 11:48:27', '2018-01-11 11:48:32'),
(16, 49, 13, 2, 12, 2, 2, '2018-01-11 11:55:57', '2018-01-11 11:56:02'),
(17, 50, 13, 2, 12, 2, 2, '2018-01-11 12:06:22', '2018-01-11 12:06:28'),
(18, 52, 13, 2, 12, 2, 2, '2018-01-11 13:37:23', '2018-01-11 13:38:15'),
(19, 53, 13, 2, 12, 2, 2, '2018-01-11 13:41:39', '2018-01-11 13:41:44'),
(20, 56, 13, 2, 12, 2, 2, '2018-01-12 08:28:14', '2018-01-12 08:28:20'),
(21, 57, 13, 2, 12, 2, 2, '2018-01-12 08:31:06', '2018-01-12 08:31:17'),
(22, 58, 13, 2, 12, 2, 2, '2018-01-12 08:34:13', '2018-01-12 08:34:18'),
(23, 60, 13, 2, 12, 2, 2, '2018-01-12 08:51:15', '2018-01-12 08:51:18'),
(24, 61, 13, 2, 12, 2, 2, '2018-01-12 09:11:07', '2018-01-12 09:11:11'),
(25, 62, 13, 2, 12, 2, 2, '2018-01-12 09:17:26', '2018-01-12 09:17:33'),
(26, 66, 13, 2, 12, 2, 2, '2018-01-12 10:10:09', '2018-01-12 10:11:34'),
(27, 67, 13, 2, 12, 2, 2, '2018-01-12 10:24:17', '2018-01-12 10:24:28'),
(28, 68, 13, 2, 12, 2, 2, '2018-01-12 10:25:54', '2018-01-12 10:26:20'),
(29, 69, 13, 2, 12, 2, 2, '2018-01-12 10:27:36', '2018-01-12 10:30:11'),
(30, 71, 13, 2, 12, 2, 2, '2018-01-12 10:46:00', '2018-01-12 10:49:06'),
(31, 73, 13, 2, 12, 2, 2, '2018-01-12 10:58:32', '2018-01-12 10:59:26'),
(32, 74, 13, 2, 12, 2, 2, '2018-01-12 11:23:14', '2018-01-12 11:23:18');

-- --------------------------------------------------------

--
-- Table structure for table `driver_rate_card`
--

CREATE TABLE `driver_rate_card` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `rate_card_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `purchase_ride` int(11) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driver_referal_code`
--

CREATE TABLE `driver_referal_code` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `referal_driver_id` int(11) NOT NULL,
  `referal_code` varchar(50) DEFAULT NULL,
  `current_balance` double DEFAULT '0',
  `ride_complete` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `driver_reviews`
--

CREATE TABLE `driver_reviews` (
  `id` int(11) NOT NULL,
  `driverid` int(11) NOT NULL,
  `rideid` int(11) NOT NULL,
  `option_type` int(11) DEFAULT '0',
  `star` int(11) DEFAULT '0',
  `review` longtext,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_reviews`
--

INSERT INTO `driver_reviews` (`id`, `driverid`, `rideid`, `option_type`, `star`, `review`, `created`) VALUES
(1, 6, 11, 0, 5, '', '2018-01-08 10:03:07'),
(2, 6, 12, 0, 5, '', '2018-01-08 10:34:39'),
(3, 1, 15, 0, 5, '', '2018-01-08 13:44:38'),
(4, 9, 29, 0, 5, '', '2018-01-10 12:35:34'),
(5, 9, 31, 0, 1, '', '2018-01-10 12:40:16'),
(6, 9, 32, 0, 1, '', '2018-01-10 12:44:34'),
(7, 2, 45, 0, 4, '', '2018-01-11 11:35:02'),
(8, 2, 47, 0, 4, '', '2018-01-11 11:48:43'),
(9, 2, 49, 0, 3, '', '2018-01-11 11:56:12'),
(10, 2, 50, 0, 3, '', '2018-01-11 12:06:39'),
(11, 2, 52, 0, 3, '', '2018-01-11 13:39:57'),
(12, 2, 53, 0, 3, '', '2018-01-11 13:46:10'),
(13, 2, 60, 0, 4, '', '2018-01-12 08:52:36'),
(14, 2, 61, 0, 4, '', '2018-01-12 09:11:34'),
(15, 2, 62, 0, 3, '', '2018-01-12 09:18:46'),
(16, 2, 66, 0, 3, '', '2018-01-12 10:19:50'),
(17, 2, 71, 0, 3, '', '2018-01-12 10:52:03'),
(18, 2, 73, 0, 3, '', '2018-01-12 11:09:54');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_action`
--

CREATE TABLE `driver_ride_action` (
  `id` int(12) NOT NULL,
  `rideid` int(12) DEFAULT '0',
  `driverid` int(12) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_ride_action`
--

INSERT INTO `driver_ride_action` (`id`, `rideid`, `driverid`, `created`) VALUES
(1, 2, 2, '2018-01-05 08:52:47'),
(2, 2, 2, '2018-01-05 08:52:49'),
(3, 3, 3, '2018-01-05 09:18:31'),
(4, 3, 3, '2018-01-05 09:18:33'),
(5, 4, 3, '2018-01-05 09:19:21'),
(6, 3, 3, '2018-01-05 09:19:21'),
(7, 9, 2, '2018-01-08 04:03:28'),
(8, 6, 6, '2018-01-08 04:03:40'),
(9, 9, 6, '2018-01-08 04:03:45'),
(10, 10, 2, '2018-01-08 04:04:54'),
(11, 11, 2, '2018-01-08 04:21:32'),
(12, 13, 1, '2018-01-08 08:06:41'),
(13, 13, 1, '2018-01-08 08:06:44'),
(14, 14, 1, '2018-01-08 08:07:03'),
(15, 20, 1, '2018-01-10 01:24:41'),
(16, 20, 9, '2018-01-10 01:24:58'),
(17, 12, 9, '2018-01-10 01:24:58'),
(18, 21, 1, '2018-01-10 01:26:20'),
(19, 21, 1, '2018-01-10 01:26:22'),
(20, 22, 1, '2018-01-10 01:26:55'),
(21, 22, 1, '2018-01-10 01:26:56'),
(22, 23, 1, '2018-01-10 01:27:38'),
(23, 23, 1, '2018-01-10 01:27:48'),
(24, 24, 1, '2018-01-10 01:28:06'),
(25, 24, 1, '2018-01-10 01:28:06'),
(26, 25, 9, '2018-01-10 01:44:14'),
(27, 28, 9, '2018-01-10 07:02:15'),
(28, 33, 1, '2018-01-10 07:18:25'),
(29, 35, 1, '2018-01-10 07:25:43'),
(30, 35, 1, '2018-01-10 07:25:46'),
(31, 36, 1, '2018-01-10 07:26:17'),
(32, 36, 1, '2018-01-10 07:26:17'),
(33, 37, 1, '2018-01-10 08:10:37'),
(34, 40, 1, '2018-01-11 05:52:49'),
(35, 2, 2, '2018-01-11 05:54:48'),
(36, 42, 2, '2018-01-11 05:54:49'),
(37, 44, 2, '2018-01-11 06:00:33'),
(38, 2, 2, '2018-01-11 08:26:20'),
(39, 55, 2, '2018-01-11 08:26:24'),
(40, 59, 2, '2018-01-12 03:12:05'),
(41, 63, 2, '2018-01-12 04:35:25'),
(42, 65, 2, '2018-01-12 04:36:02');

-- --------------------------------------------------------

--
-- Table structure for table `driver_support_option`
--

CREATE TABLE `driver_support_option` (
  `id` int(12) NOT NULL,
  `en_option` longtext,
  `ar_option` longtext,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_support_option`
--

INSERT INTO `driver_support_option` (`id`, `en_option`, `ar_option`, `created`) VALUES
(1, 'This is test', 'This is test', '2017-09-01 02:24:53'),
(2, 'This is test2', 'This is test2', '2017-09-01 02:24:53');

-- --------------------------------------------------------

--
-- Table structure for table `driver_transactions`
--

CREATE TABLE `driver_transactions` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `transfer_driverid` int(11) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `transaction_type_id` int(11) DEFAULT NULL,
  `transaction_amount` double(11,2) NOT NULL DEFAULT '0.00',
  `transaction_detail` longtext,
  `transaction_id` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_transactions`
--

INSERT INTO `driver_transactions` (`id`, `driver_id`, `ride_id`, `transfer_driverid`, `voucher_id`, `transaction_type_id`, `transaction_amount`, `transaction_detail`, `transaction_id`, `status`, `created`) VALUES
(1, 1, 1, NULL, NULL, 11, -2.00, 'Najez share', NULL, 1, NULL),
(2, 1, 1, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(3, 1, 1, NULL, NULL, 12, 8.00, 'Driver ride share', NULL, 1, NULL),
(4, 1, 5, NULL, NULL, 11, -2.00, 'Najez share', NULL, 1, NULL),
(5, 1, 5, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(6, 1, 5, NULL, NULL, 12, 8.00, 'Driver ride share', NULL, 1, NULL),
(7, 1, 5, 2, NULL, 9, 47.00, 'Money transfer to Customer', NULL, 1, NULL),
(8, 4, 6, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(9, 4, 6, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(10, 4, 6, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(11, 6, 10, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(12, 6, 10, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(13, 6, 10, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(14, 6, 10, 6, NULL, 9, 8.00, 'Money transfer to Customer', NULL, 1, NULL),
(15, 6, 11, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(16, 6, 11, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(17, 6, 11, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(18, 6, 11, 6, NULL, 9, 8.00, 'Money transfer to Customer', NULL, 1, NULL),
(19, 6, 12, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(20, 6, 12, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(21, 6, 12, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(22, 1, 15, NULL, NULL, 11, -2.00, 'Najez share', NULL, 1, NULL),
(23, 1, 15, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(24, 1, 15, NULL, NULL, 12, 8.00, 'Driver ride share', NULL, 1, NULL),
(25, 1, 15, 1, NULL, 9, 12.00, 'Money transfer to Customer', NULL, 1, NULL),
(26, 12, 17, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(27, 12, 17, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(28, 12, 17, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(29, 12, 17, 7, NULL, 9, 43.00, 'Money transfer to Customer', NULL, 1, NULL),
(30, 9, 29, NULL, NULL, 11, -2.00, 'Najez share', NULL, 1, NULL),
(31, 9, 29, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(32, 9, 29, NULL, NULL, 12, 8.00, 'Driver ride share', NULL, 1, NULL),
(33, 9, 31, NULL, NULL, 11, -2.00, 'Najez share', NULL, 1, NULL),
(34, 9, 31, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(35, 9, 31, NULL, NULL, 12, 8.00, 'Driver ride share', NULL, 1, NULL),
(36, 9, 31, 11, NULL, 9, 90.00, 'Money transfer to Customer', NULL, 1, NULL),
(37, 9, 32, NULL, NULL, 11, -2.00, 'Najez share', NULL, 1, NULL),
(38, 9, 32, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(39, 9, 32, NULL, NULL, 12, 8.00, 'Driver ride share', NULL, 1, NULL),
(40, 9, 32, 11, NULL, 9, 991.00, 'Money transfer to Customer', NULL, 1, NULL),
(41, 2, 38, NULL, NULL, 12, 9.00, 'Driver ride share', NULL, 1, NULL),
(42, 2, 38, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(43, 2, 38, 13, NULL, 9, 12.00, 'Money transfer to Customer', NULL, 1, NULL),
(44, 11, 43, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(45, 11, 43, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(46, 11, 43, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(47, 2, 45, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(48, 2, 45, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(49, 2, 45, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(50, 11, 46, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(51, 11, 46, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(52, 2, 47, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(53, 2, 47, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(54, 2, 47, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(55, 2, 49, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(56, 2, 49, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(57, 2, 49, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(58, 2, 50, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(59, 2, 50, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(60, 2, 50, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(61, 2, 52, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(62, 2, 52, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(63, 2, 52, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(64, 2, 53, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(65, 2, 53, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(66, 2, 53, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(67, 2, 56, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(68, 2, 56, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(69, 2, 56, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(70, 2, 57, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(71, 2, 57, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(72, 2, 57, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(73, 2, 58, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(74, 2, 58, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(75, 2, 58, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(76, 2, 60, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(77, 2, 60, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(78, 2, 60, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(79, 2, 61, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(80, 2, 61, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(81, 2, 61, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(82, 2, 62, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(83, 2, 62, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(84, 2, 62, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(85, 2, 66, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(86, 2, 66, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(87, 2, 66, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(88, 2, 67, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(89, 2, 67, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(90, 2, 67, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(91, 2, 68, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(92, 2, 68, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(93, 2, 68, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(94, 2, 69, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(95, 2, 69, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(96, 2, 69, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(97, 2, 71, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(98, 2, 71, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(99, 2, 71, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(100, 2, 73, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(101, 2, 73, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(102, 2, 73, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL),
(103, 2, 74, NULL, NULL, 11, -3.00, 'Najez share', NULL, 1, NULL),
(104, 2, 74, NULL, NULL, 13, -0.50, 'Halal', NULL, 1, NULL),
(105, 2, 74, NULL, NULL, 12, 12.00, 'Driver ride share', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `driver_vehicles`
--

CREATE TABLE `driver_vehicles` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `medium` int(11) NOT NULL DEFAULT '0',
  `family` int(11) NOT NULL DEFAULT '0',
  `luxury` int(11) NOT NULL DEFAULT '0',
  `delivery` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `driver_vehicles_segment`
--

CREATE TABLE `driver_vehicles_segment` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) DEFAULT '0',
  `vehicle_type` int(11) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_vehicles_segment`
--

INSERT INTO `driver_vehicles_segment` (`id`, `driver_id`, `vehicle_type`, `created`) VALUES
(12, 3, 3, '2018-01-05 14:47:58'),
(21, 4, 3, '2018-01-07 06:26:46'),
(27, 5, 1, '2018-01-07 15:37:31'),
(178, 9, 1, '2018-01-16 11:05:35'),
(179, 2, 2, '2018-01-18 12:53:59'),
(180, 2, 4, '2018-01-22 09:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `driver_wallet_details`
--

CREATE TABLE `driver_wallet_details` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `transfer_name` varchar(200) DEFAULT NULL,
  `transaction_type_id` varchar(200) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL,
  `amount_num` double DEFAULT NULL,
  `transaction_mode` varchar(50) DEFAULT NULL,
  `transaction_datetime` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_wallet_details`
--

INSERT INTO `driver_wallet_details` (`id`, `driver_id`, `ride_id`, `description`, `transfer_name`, `transaction_type_id`, `amount`, `amount_num`, `transaction_mode`, `transaction_datetime`, `status`) VALUES
(1, 1, 1, 'Najez share', NULL, '11', '- SAR 2', -2, 'debit', '2018-01-05 13:59:32', 1),
(2, 1, 1, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-05 13:59:32', 1),
(3, 1, 1, 'Driver ride share', NULL, '12', '+ SAR 8', 8, 'credit', '2018-01-05 13:59:52', 1),
(4, 1, 5, 'Najez share', NULL, '11', '- SAR 2', -2, 'debit', '2018-01-05 14:50:23', 1),
(5, 1, 5, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-05 14:50:23', 1),
(6, 1, 5, 'Driver ride share', NULL, '12', '+ SAR 8', 8, 'credit', '2018-01-05 14:50:32', 1),
(7, 1, 5, 'Money transfer to Customer', NULL, '9', '- SAR 47', 47, 'debit', '2018-01-05 14:50:32', 1),
(8, 4, 6, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-07 06:28:38', 1),
(9, 4, 6, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-07 06:28:38', 1),
(10, 4, 6, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-07 06:28:44', 1),
(11, 6, 10, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-08 09:38:00', 1),
(12, 6, 10, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-08 09:38:00', 1),
(13, 6, 10, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-08 09:39:00', 1),
(14, 6, 10, 'Money transfer to Customer', NULL, '9', '- SAR 8', 8, 'debit', '2018-01-08 09:39:00', 1),
(15, 6, 11, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-08 10:01:47', 1),
(16, 6, 11, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-08 10:01:47', 1),
(17, 6, 11, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-08 10:02:20', 1),
(18, 6, 11, 'Money transfer to Customer', NULL, '9', '- SAR 8', 8, 'debit', '2018-01-08 10:02:20', 1),
(19, 6, 12, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-08 10:07:15', 1),
(20, 6, 12, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-08 10:07:15', 1),
(21, 6, 12, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-08 10:09:40', 1),
(22, 1, 15, 'Najez share', NULL, '11', '- SAR 2', -2, 'debit', '2018-01-08 13:43:55', 1),
(23, 1, 15, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-08 13:43:55', 1),
(24, 1, 15, 'Driver ride share', NULL, '12', '+ SAR 8', 8, 'credit', '2018-01-08 13:44:06', 1),
(25, 1, 15, 'Money transfer to Customer', NULL, '9', '- SAR 12', 12, 'debit', '2018-01-08 13:44:06', 1),
(26, 12, 17, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-09 11:22:41', 1),
(27, 12, 17, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-09 11:22:41', 1),
(28, 12, 17, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-09 11:22:49', 1),
(29, 12, 17, 'Money transfer to Customer', NULL, '9', '- SAR 43', 43, 'debit', '2018-01-09 11:22:49', 1),
(30, 9, 29, 'Najez share', NULL, '11', '- SAR 2', -2, 'debit', '2018-01-10 12:35:10', 1),
(31, 9, 29, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-10 12:35:10', 1),
(32, 9, 29, 'Driver ride share', NULL, '12', '+ SAR 8', 8, 'credit', '2018-01-10 12:35:19', 1),
(33, 9, 31, 'Najez share', NULL, '11', '- SAR 2', -2, 'debit', '2018-01-10 12:39:58', 1),
(34, 9, 31, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-10 12:39:58', 1),
(35, 9, 31, 'Driver ride share', NULL, '12', '+ SAR 8', 8, 'credit', '2018-01-10 12:40:09', 1),
(36, 9, 31, 'Money transfer to Customer', NULL, '9', '- SAR 90', 90, 'debit', '2018-01-10 12:40:09', 1),
(37, 9, 32, 'Najez share', NULL, '11', '- SAR 2', -2, 'debit', '2018-01-10 12:41:14', 1),
(38, 9, 32, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-10 12:41:14', 1),
(39, 9, 32, 'Driver ride share', NULL, '12', '+ SAR 8', 8, 'credit', '2018-01-10 12:41:20', 1),
(40, 9, 32, 'Money transfer to Customer', NULL, '9', '- SAR 991', 991, 'debit', '2018-01-10 12:41:20', 1),
(41, 2, 38, 'Driver ride share', NULL, '12', '+ SAR 9', 9, 'credit', '2018-01-11 11:16:31', 1),
(42, 2, 38, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 11:16:31', 1),
(43, 2, 38, 'Money transfer to Customer', NULL, '9', '- SAR 12', 12, 'debit', '2018-01-11 11:18:08', 1),
(44, 11, 43, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-11 11:27:34', 1),
(45, 11, 43, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 11:27:34', 1),
(46, 11, 43, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-11 11:27:46', 1),
(47, 2, 45, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-11 11:34:51', 1),
(48, 2, 45, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 11:34:51', 1),
(49, 2, 45, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-11 11:34:55', 1),
(50, 11, 46, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-11 11:38:11', 1),
(51, 11, 46, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 11:38:11', 1),
(52, 2, 47, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-11 11:48:27', 1),
(53, 2, 47, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 11:48:27', 1),
(54, 2, 47, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-11 11:48:32', 1),
(55, 2, 49, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-11 11:55:57', 1),
(56, 2, 49, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 11:55:57', 1),
(57, 2, 49, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-11 11:56:02', 1),
(58, 2, 50, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-11 12:06:22', 1),
(59, 2, 50, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 12:06:22', 1),
(60, 2, 50, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-11 12:06:28', 1),
(61, 2, 52, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-11 13:37:23', 1),
(62, 2, 52, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 13:37:23', 1),
(63, 2, 52, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-11 13:38:15', 1),
(64, 2, 53, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-11 13:41:39', 1),
(65, 2, 53, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-11 13:41:39', 1),
(66, 2, 53, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-11 13:41:44', 1),
(67, 2, 56, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 08:28:14', 1),
(68, 2, 56, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 08:28:14', 1),
(69, 2, 56, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 08:28:20', 1),
(70, 2, 57, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 08:31:06', 1),
(71, 2, 57, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 08:31:06', 1),
(72, 2, 57, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 08:31:17', 1),
(73, 2, 58, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 08:34:13', 1),
(74, 2, 58, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 08:34:13', 1),
(75, 2, 58, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 08:34:18', 1),
(76, 2, 60, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 08:51:15', 1),
(77, 2, 60, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 08:51:15', 1),
(78, 2, 60, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 08:51:18', 1),
(79, 2, 61, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 09:11:07', 1),
(80, 2, 61, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 09:11:07', 1),
(81, 2, 61, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 09:11:11', 1),
(82, 2, 62, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 09:17:26', 1),
(83, 2, 62, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 09:17:26', 1),
(84, 2, 62, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 09:17:33', 1),
(85, 2, 66, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 10:10:09', 1),
(86, 2, 66, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 10:10:09', 1),
(87, 2, 66, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 10:11:34', 1),
(88, 2, 67, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 10:24:17', 1),
(89, 2, 67, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 10:24:17', 1),
(90, 2, 67, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 10:24:28', 1),
(91, 2, 68, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 10:25:54', 1),
(92, 2, 68, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 10:25:54', 1),
(93, 2, 68, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 10:26:20', 1),
(94, 2, 69, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 10:27:36', 1),
(95, 2, 69, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 10:27:36', 1),
(96, 2, 69, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 10:30:11', 1),
(97, 2, 71, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 10:46:00', 1),
(98, 2, 71, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 10:46:00', 1),
(99, 2, 71, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 10:49:06', 1),
(100, 2, 73, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 10:58:32', 1),
(101, 2, 73, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 10:58:32', 1),
(102, 2, 73, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 10:59:26', 1),
(103, 2, 74, 'Najez share', NULL, '11', '- SAR 3', -3, 'debit', '2018-01-12 11:23:14', 1),
(104, 2, 74, 'Halal', NULL, '13', '- SAR 0.5', -0.5, 'debit', '2018-01-12 11:23:14', 1),
(105, 2, 74, 'Driver ride share', NULL, '12', '+ SAR 12', 12, 'credit', '2018-01-12 11:23:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_template`
--

CREATE TABLE `email_template` (
  `id` int(11) NOT NULL,
  `template_id` int(11) DEFAULT '0',
  `template_name` varchar(100) NOT NULL DEFAULT '0',
  `meta_key` varchar(255) NOT NULL DEFAULT '0',
  `user_type` varchar(255) NOT NULL DEFAULT '0',
  `meta_value_en` longtext NOT NULL,
  `meta_value_ar` longtext CHARACTER SET utf8 NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_template`
--

INSERT INTO `email_template` (`id`, `template_id`, `template_name`, `meta_key`, `user_type`, `meta_value_en`, `meta_value_ar`, `created_date`, `modified_date`) VALUES
(1, 123, 'User Welcome', 'subject', 'user', 'Welcome', 'Welcome', '2017-08-01 00:11:26', '2017-08-01 23:14:29'),
(2, 123, 'User Welcome', 'title', 'user', 'Welcome to Najez', 'مرحبا', '2017-08-01 00:11:26', '2017-08-01 23:34:59'),
(3, 123, 'User Welcome', 'subtitle', 'user', 'Anywhere anytime! Book online', 'في أي وقتٍ ومكان، احجز الان', '2017-08-01 00:11:26', '2017-08-01 22:33:48'),
(4, 123, 'User Welcome', 'content', 'user', '<p>Thank you for registration, and you will receive an E-mail or SMS soon to download the application, and <u>please be informed that you can be our partner and getting 2.5%10% from Najez percentage from&nbsp;your friend trips by inviting them and putting your code in the code field.</u></p>', '<p>يسرنا انضمامك الينا وسوف تصلك قريبا\" رسالة نصية او بريد الكتروني عند تفعيل التطبيق كما&nbsp;<u>يسعدنا ان نخبرك انه يمكنك انت تكون شريكنا بنسبة <strong>10%</strong> من ارباح ناجز عبر رحلات اصدقائك من خلال دعوتهم عن طريق ارسال الكود الخاص بك و وضعه في خانة التسجيل.</u></p>', '2017-08-01 00:11:26', '2017-08-16 22:54:46'),
(5, 123, 'User Welcome', 'footer', 'user', '<p>Always remember that!</p>', '<p>و تذكر دائما\"</p>', '2017-08-01 00:11:26', '2017-10-26 10:01:52'),
(6, 124, 'Captain welcome', 'subject', 'driver', 'Welcome', 'أهلا بك', '2017-08-01 00:11:26', '2018-01-15 10:28:24'),
(7, 124, 'Captain welcome', 'title', 'driver', 'Welcome to Najez', 'مرحبا', '2017-08-01 02:09:23', '2017-08-01 23:34:33'),
(9, 124, 'Captain welcome', 'content', 'driver', '<p>Thank you for registration as partner of <strong>Najez</strong>, and please be informed that you will receive an E-mail or SMS soon to download the application and earn more money, <u> and we are happy to tell you that you can be our partner and getting 10% from <strong>Najez </strong>percentage from your friend trips by inviting them and putting your code in the code field. </u></p>\r\n<div><u>Also, we will not take any percentage if you registered&nbsp;as delivery man.</u></div>', '<p>يسرنا انضمامك الينا كشريك في ناجز, ويسعدنا اخبارك انه سوف تصلك قريبا\" رسالة نصية او بريد الكتروني عند انطلاقنا في السوق لتحميل التطبيق و جني الكثير الارباح.</p>\r\n<div>اذا كنت قد سجلت رجل توصيل يسرنا ان نخبرك اننا لن نأخذ اي عمولة عليك لمده 6 اشهر من انطلاق البرنامج.<br /> <br /> <u>كما يسعدنا ان نخبرك انه يمكنك انت تكون شريكنا بنسبة <strong>10%</strong> من ارباح الشركة في رحلات اصدقائك من خلال دعوتهم عن طريق ارسال الكود الخاص بك و وضعه في خانة التسجيل.</u></div>', '2017-08-01 02:09:23', '2017-08-16 05:58:55'),
(10, 124, 'Captain welcome', 'tnc', 'driver', '<ul>\r\n<li><small>1. You can use it to pay for your trips in Najez.</small></li>\r\n<li><small>2. You can take it cash.</small></li>\r\n<li><small>3. Maximum time for enjoying this credit is 6 months if Najez didn&rsquo;t announce extend the period.</small></li>\r\n<li><small>4. This offer will be expired after 6 month from launch if Najez didn&rsquo;t announce extend the period.</small></li>\r\n<li><small>5. Partners cannot get this benefit after 6 months if Najez didn&rsquo;t announce extend the period.</small></li>\r\n<li><small>6. After completing 60 trips per month <br /></small></li>\r\n</ul>', '<ul>\r\n<li><small>1- يمكن ان يقبض الثمن نقدا\".</small></li>\r\n<li><small>2-يستخدم لتحويله لحسابه كرصيد رحلات.</small></li>\r\n<li><small>3- يستفيد من الرصيد لمدة اقصاها 6 اشهرمالم تعلن الشركة تمديد الفتره.</small></li>\r\n<li><small>4- مدة هذا العرض 6 اشهر من تاريخ انطلاق البرنامج مالم تعلن الشركة تمديد الفتره.</small></li>\r\n<li><small>5- يعتبر العرض لاغي بعد 6 اشهر من تاريخ الانطلاق مالم تعلن الشركة تمديد الفتره.</small></li>\r\n<li><small>6-لا يحق للمستفيد المطالبة بعد انقضاء المدة مالم تعلن الشركة تمديد الفتره.</small></li>\r\n<li><small>7- بعد اتمام ٦٠ رحله في الشهر <br /></small></li>\r\n</ul>', '2017-08-01 02:09:23', '2017-08-16 05:58:55'),
(13, 123, 'User Welcome', 'footer2', 'user', '<p>(With Najez you will save more)</p>', '<p>(ناجز اوفرلك)</p>', '2017-08-01 22:28:21', '2018-03-19 12:24:01'),
(14, 123, 'User Welcome', 'tnc', 'user', '<ul>\r\n<li><small>1. You can use it to pay for your trips in <strong>Najez</strong> only.</small></li>\r\n<li><small>2. You cannot take it cash.</small></li>\r\n<li><small>3. Maximum time for enjoying this credit is 6 months if <strong>Najez</strong> didn&rsquo;t announce extend the period.</small></li>\r\n<li><small>4. This offer will be expired after 6 month from launch if <strong>Najez</strong> didn&rsquo;t announce extend the period.</small></li>\r\n<li><small>5. Customers cannot get this benefit after 6 months if <strong>Najez</strong> didn&rsquo;t announce extend the period.</small></li>\r\n<li><small>6. After completing 60 trips per month <br /></small></li>\r\n</ul>', '<ul>\r\n<li><small>1-لا يمكن ان يقبض الثمن نقدا</small></li>\r\n</ul>', '2017-08-01 22:35:38', '2018-03-19 12:24:01'),
(15, 123, 'User Welcome', 'attch', 'user', 'assets/documents/Tulips.jpg', 'assets/documents/Tulips.jpg', '2017-08-01 00:11:26', '2018-03-19 12:24:01'),
(16, 124, 'Captain welcome', 'attch', 'driver', 'assets/documents/Jellyfish.jpg', 'assets/documents/Jellyfish.jpg', '2017-08-01 00:11:26', '2018-03-19 12:19:11');

-- --------------------------------------------------------

--
-- Table structure for table `fare_ratecard`
--

CREATE TABLE `fare_ratecard` (
  `id` int(12) NOT NULL,
  `trip_type` int(12) DEFAULT NULL,
  `vehicle_type` int(12) DEFAULT NULL,
  `charges` float DEFAULT NULL,
  `charge_type` int(12) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fare_ratecard`
--

INSERT INTO `fare_ratecard` (`id`, `trip_type`, `vehicle_type`, `charges`, `charge_type`, `created`) VALUES
(1, 1, 1, 0.9, 1, '2017-08-30 22:23:21'),
(2, 1, 2, 1.25, 1, '2017-08-30 22:23:21'),
(3, 1, 3, 1.25, 1, '2017-08-30 22:23:21'),
(4, 2, 1, 1.5, 1, '2017-08-30 22:23:21'),
(5, 2, 2, 2, 1, '2017-08-30 22:23:21'),
(6, 2, 3, 2, 1, '2017-08-30 22:23:21'),
(7, 3, 1, 0.9, 1, '2017-08-30 22:23:21'),
(8, 3, 2, 1.25, 1, '2017-08-30 22:23:21'),
(9, 3, 3, 1.25, 1, '2017-08-30 22:23:21'),
(13, 1, 4, 0.75, 1, '2017-09-12 15:00:02'),
(14, 2, 4, 1, 1, '2017-09-12 15:00:52'),
(15, 1, 1, 0.25, 2, '2017-09-12 15:08:59'),
(16, 1, 2, 0.4, 2, '2017-09-12 15:09:19'),
(17, 1, 3, 0.4, 2, '2017-09-12 15:11:33'),
(18, 1, 4, 0.2, 2, '2017-09-12 15:11:47'),
(19, 2, 1, 0.25, 2, '2017-09-12 15:12:06'),
(20, 2, 2, 0.4, 2, '2017-09-12 15:12:20'),
(21, 2, 3, 0.4, 2, '2017-09-12 15:12:38'),
(22, 2, 4, 0.2, 2, '2017-09-12 15:12:52'),
(23, 3, 1, 0.8, 2, '2017-09-12 15:13:35'),
(24, 3, 2, 1.5, 2, '2017-09-12 15:13:49'),
(25, 3, 3, 1.5, 2, '2017-09-12 15:14:05'),
(26, 1, 1, 3.5, 3, '2017-09-12 15:14:48'),
(27, 1, 2, 5.5, 3, '2017-09-12 15:15:03'),
(28, 1, 3, 5.5, 3, '2017-09-12 15:15:20'),
(29, 1, 4, 3.5, 3, '2017-09-12 09:43:58'),
(30, 2, 1, 3.5, 3, '2017-09-12 15:15:55'),
(31, 2, 2, 5.5, 3, '2017-09-12 15:16:08'),
(32, 2, 3, 5.5, 3, '2017-09-12 15:16:22'),
(33, 2, 4, 3.5, 3, '2017-09-12 09:45:01'),
(34, 3, 1, 250, 3, '2017-09-12 09:45:28'),
(35, 3, 2, 350, 3, '2017-09-12 09:45:41'),
(36, 3, 3, 350, 3, '2017-09-12 09:45:53'),
(37, 3, 4, 0, 3, '2017-09-12 09:46:06'),
(38, 1, 1, 8, 4, '2017-09-12 15:19:24'),
(39, 1, 2, 12, 4, '2017-09-12 15:19:39'),
(40, 1, 3, 12, 4, '2017-09-12 15:19:59'),
(41, 1, 4, 8, 4, '2017-09-12 15:20:12'),
(42, 2, 1, 8, 4, '2017-09-12 09:49:20'),
(43, 2, 2, 12, 4, '2017-09-12 15:21:10'),
(44, 2, 3, 12, 4, '2017-09-12 15:21:22'),
(45, 2, 4, 8, 4, '2017-09-12 15:21:33'),
(46, 3, 1, 250, 4, '2017-09-12 15:21:55'),
(47, 3, 2, 350, 4, '2017-09-12 15:22:07'),
(48, 3, 3, 350, 4, '2017-09-12 15:22:18'),
(49, 3, 4, 0, 4, '2017-09-12 15:22:30'),
(50, 1, 1, 7, 5, '2017-09-12 15:23:32'),
(51, 1, 2, 11, 5, '2017-09-12 15:23:44'),
(52, 1, 3, 11, 5, '2017-09-12 15:23:56'),
(53, 1, 4, 7, 5, '2017-09-12 15:24:09'),
(54, 2, 1, 7, 5, '2017-09-12 15:24:25'),
(55, 2, 2, 11, 5, '2017-09-12 15:24:37'),
(56, 2, 3, 11, 5, '2017-09-12 15:24:49'),
(57, 2, 4, 7, 5, '2017-09-12 15:25:01'),
(58, 3, 1, 0, 5, '2017-09-12 15:25:15'),
(59, 3, 2, 0, 5, '2017-09-12 15:25:25'),
(60, 3, 3, 0, 5, '2017-09-12 15:25:35'),
(61, 3, 4, 0, 5, '2017-09-12 15:25:48'),
(62, 1, 1, 16, 6, '2017-09-12 15:26:10'),
(63, 1, 2, 24, 6, '2017-09-12 15:26:28'),
(64, 1, 3, 24, 6, '2017-09-12 15:26:40'),
(65, 1, 4, 16, 6, '2017-09-12 15:26:53'),
(66, 2, 1, 16, 6, '2017-09-12 15:27:05'),
(67, 2, 2, 24, 6, '2017-09-12 15:27:17'),
(68, 2, 3, 24, 6, '2017-09-12 15:27:29'),
(69, 2, 4, 16, 6, '2017-09-12 15:27:42'),
(70, 3, 1, 0, 6, '2017-09-12 15:28:00'),
(71, 3, 2, 0, 6, '2017-09-12 15:28:11'),
(72, 3, 3, 0, 6, '2017-09-12 15:28:22'),
(73, 3, 4, 0, 6, '2017-09-12 15:28:32'),
(74, 3, 4, 0, 2, '2017-10-11 18:32:00'),
(75, 3, 4, 0, 1, '2017-10-11 18:32:36'),
(76, 1, 1, 25, 7, '2017-10-13 11:47:27'),
(77, 1, 2, 25, 7, '2017-10-13 11:47:44'),
(78, 1, 3, 25, 7, '2017-10-13 11:48:00'),
(79, 1, 4, 25, 7, '2017-10-13 11:48:18'),
(80, 2, 1, 25, 7, '2017-10-13 11:48:32'),
(81, 2, 2, 25, 7, '2017-10-13 11:48:45'),
(82, 2, 3, 25, 7, '2017-10-13 11:48:56'),
(83, 2, 4, 25, 7, '2017-10-13 11:49:07'),
(84, 3, 1, 25, 7, '2017-10-13 11:49:17'),
(85, 3, 2, 25, 7, '2017-10-13 11:49:30'),
(86, 3, 3, 25, 7, '2017-10-13 11:49:41'),
(87, 3, 4, 25, 7, '2017-10-13 11:49:53');

-- --------------------------------------------------------

--
-- Table structure for table `front_carlist`
--

CREATE TABLE `front_carlist` (
  `id` int(11) NOT NULL,
  `car_brand` varchar(255) NOT NULL,
  `car_type` varchar(255) NOT NULL,
  `car_description` varchar(255) DEFAULT NULL,
  `car_model` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `front_carlist`
--

INSERT INTO `front_carlist` (`id`, `car_brand`, `car_type`, `car_description`, `car_model`) VALUES
(1, 'Audi', 'Medium', 'assets/cars/Audi/Audi_A4_Sedan_2013_360_720_50-1.png', 'A6'),
(2, 'Audi', 'Medium', 'assets/cars/Audi/Audi_A5_Mk1f_8F7_cabriolet_2012_360_720_50-1.png', 'A5'),
(3, 'Audi', 'Luxury', 'assets/cars/Audi/Audi_A6_Avant_2012_360_720_50-1.png', 'A6'),
(4, 'Audi', 'Luxury', 'assets/cars/Audi/Audi_A7_Sportback_2011_360_1.png', 'A7'),
(5, 'Audi', 'Luxury', 'assets/cars/Audi/Audi_A8_Mk3f_D4_2014_360_720_50-1.png', 'A8'),
(6, 'Audi', 'Luxury', 'assets/cars/Audi/Audi_SQ5_2013_360_720_50-1.png', 'Q5'),
(7, 'Audi', 'Family', 'assets/cars/Audi/Audi_Q7_Mk2_e-tron_2017_360_720_50-1.png', 'Q7'),
(8, 'Audi', 'Luxury', 'assets/cars/Audi/Audi_Q7_Mk2_e-tron_2017_360_720_50-1.png', 'Q7'),
(9, 'Infiniti', 'Medium', 'assets/cars/Infiniti/Infiniti_Q30_S_2015_360_720_50-1.png', 'Q30'),
(10, 'Infiniti', 'Family', 'assets/cars/Infiniti/Infiniti_Q50_2013_360_720_50-1.png', 'Q50'),
(11, 'Infiniti', 'Luxury', 'assets/cars/Infiniti/Infiniti_Q50_2013_360_720_50-1.png', 'Q50'),
(12, 'Infiniti', 'Luxury', 'assets/cars/caar.png', 'QX60'),
(13, 'Infiniti', 'Medium', 'assets/cars/Infiniti/Infiniti_QX70_S51_S_Ultimate_2015_360_720_50-1.png', 'QX70'),
(14, 'Infiniti', 'Luxury', 'assets/cars/Infiniti/Infiniti_QX70_S51_S_Ultimate_2015_360_720_50-1.png', 'QX70'),
(15, 'Infiniti', 'Family', 'assets/cars/caar.png', 'QX80'),
(16, 'Infiniti', 'Luxury', 'assets/cars/caar.png', 'QX80'),
(17, 'BMW', 'Medium', 'assets/cars/BMW/BMW_1-series_Mk2f_F20_5door_M_Sport_Package_2015_360_720_50-1.png', '1 Series'),
(18, 'BMW', 'Medium', 'assets/cars/BMW/BMW_2-series_F45_Active_Tourer_Electrical_2016_360_720_50-1.png', '2 Series'),
(19, 'BMW', 'Medium', 'assets/cars/BMW/BMW_3-series_Mk6_F30_sedan_E_2016_360_720_50-1.png', '3 Series'),
(21, 'BMW', 'Medium', 'assets/cars/BMW/BMW_5-series_Mk7_G30_sedan_M_Performance_Parts_2017_360_720_50-1.png', '5 Series'),
(22, 'BMW', 'Medium', 'assets/cars/BMW/BMW_6-series_Mk3f_F06_Gran_Coupe_2015_360_720_50-1.png', '6 Series'),
(23, 'BMW', 'Medium', 'assets/cars/BMW/BMW_7-series_Mk6_G12_L_M_Sport_Package_2015_360_720_50-1.png', '7 Series'),
(24, 'BMW', 'Medium', 'assets/cars/BMW/BMW_X1_Mk2_F48_2015_360_720_50-1.png', 'X1'),
(26, 'BMW', 'Medium', 'assets/cars/BMW/BMW_X3_Mk2f_F25_2014_360_720_50-1.png', 'X3'),
(27, 'BMW', 'Medium', 'assets/cars/BMW/BMW_X4_Concept_2013_360_720_50-1.png', 'X4'),
(28, 'BMW', 'Medium', 'assets/cars/BMW/BMW_X5_Mk3_F15_M_2014_360_720_50-1.png', 'X5'),
(29, 'BMW', 'Medium', 'assets/cars/BMW/BMW_X6_2011_360_1.png', 'X6'),
(30, 'BMW', 'Luxury', 'assets/cars/BMW/BMW_5-series_Mk7_G30_sedan_M_Performance_Parts_2017_360_720_50-1.png', '5 Series'),
(31, 'BMW', 'Luxury', 'assets/cars/BMW/BMW_6-series_Mk3f_F06_Gran_Coupe_2015_360_720_50-1.png', '6 Series'),
(32, 'BMW', 'Luxury', 'assets/cars/BMW/BMW_7-series_Mk6_G12_L_M_Sport_Package_2015_360_720_50-1.png', '7 Series'),
(33, 'BMW', 'Luxury', 'assets/cars/BMW/BMW_X5_Mk3_F15_M_2014_360_720_50-1.png', 'X5'),
(34, 'BMW', 'Luxury', 'assets/cars/BMW/BMW_X6_2011_360_1.png', 'X6'),
(35, 'Toyota', 'Medium', '\r\nassets/cars/Toyota/Toyota_Avanza_Mk2f_F650_SE_2015_360_720_50-1.png', 'Avanza'),
(36, 'Toyota', 'Medium', 'assets/cars/Toyota/Toyota_RAV4_Mk4f_XA40_VXR_2016_360_720_50-1.png', 'Rav4'),
(37, 'Toyota', 'Medium', 'assets/cars/Toyota/Toyota_Avalon_Mk4f_XX40_Limited_2015_360_720_50-1.png', 'Avalon'),
(38, 'Toyota', 'Medium', 'assets/cars/caar.png', 'Aurion'),
(39, 'Toyota', 'Medium', 'assets/cars/Toyota/Toyota_Innova_Mk2_AN140_Crysta_TH-spec_2017_360_720_50-1.png', 'Innova'),
(40, 'Toyota', 'Medium', 'assets/cars/Toyota/Toyota_Previa_Mk3f_XR50_SE_2016_360_720_50-1.png', 'Previa'),
(41, 'Toyota', 'Medium', 'assets/cars/Toyota/Toyota_Fortuner_Mk2_AN160_VXR_2016_360_720_50-1.png', 'Fortuner'),
(42, 'Toyota', 'Medium', 'assets/cars/Toyota/Toyota_Camry_EU_2012_360_720_50-1.png', 'Camry'),
(43, 'Toyota', 'Medium', 'assets/cars/Toyota/Toyota_Corolla_Mk11f_E170_2016_360_720_50-1.png', 'Corolla'),
(44, 'Toyota', 'Medium', 'assets/cars/Toyota/Toyota_Prius_Mk4_Iconic_2016_360_720_50-1.png', 'Prius'),
(45, 'Toyota', 'Luxury', 'assets/cars/caar.png', 'Prado'),
(46, 'Toyota', 'Family', 'assets/cars/Toyota/Toyota_Sequoia_2011_360_720_50-1.png', 'Sequoia'),
(47, 'Toyota', 'Family', 'assets/cars/Toyota/Toyota_Land_Cruiser_Mk5_J79_DoubleCab_Pickup_2012_360_720_50-1.png', 'Land Cruiser'),
(48, 'Toyota', 'Family', 'assets/cars/Toyota/Toyota_Previa_Mk3f_XR50_SE_2016_360_720_50-1.png', 'Previa'),
(49, 'Toyota', 'Family', 'assets/cars/Toyota/Toyota_Fortuner_Mk2_AN160_VXR_2016_360_720_50-1.png', 'Fortuner'),
(50, 'GMC', 'Medium', 'assets/cars/GMC/GMC_Terrain_2010_360_720_50-1.png', 'Terrain'),
(51, 'GMC', 'Family', 'assets/cars/GMC/GMC_Acadia_Mk2_2017_360_720_50-1.png', 'Acadia'),
(52, 'GMC', 'Family', 'assets/cars/GMC/GMC_Yukon_Mk4_GMTK2UG_Denali_2014_360_720_50-1.png', 'Yukon'),
(53, 'GMC', 'Family', 'assets/cars/GMC/GMC_Yukon_Mk4_GMTK2UG_XL_2014_360_720_50-1.png', 'Yukon XL'),
(54, 'Jeep', 'Medium', 'assets/cars/Jeep/Jeep_Grand_Cherokee_Mk4_WK2_TrackHawk_2017_1000_0001.png', 'Grand Cherokee'),
(55, 'Jeep', 'Medium', 'assets/cars/Jeep/Jeep_compass_2012_360_720_50-1.png', 'Compass'),
(56, 'Chevrolet ', 'Medium', 'assets/cars/chevrolet/Chevrolet_Traverse_Mk2_2017_360_720_50-1.png', 'Traverse'),
(57, 'Chevrolet ', 'Medium', 'assets/cars/caar.png', 'LTZ'),
(58, 'Chevrolet ', 'Medium', 'assets/cars/chevrolet/Chevrolet_Impala_2013_360_720_50-1.png', 'Impala'),
(59, 'Chevrolet ', 'Medium', 'assets/cars/chevrolet/Chevrolet_Trailblazer_2012_360_720_50-1.png', 'Trailblazer'),
(60, 'Chevrolet', 'Medium', 'assets/cars/chevrolet/Chevrolet_Cruze_Mk2f_J300_sedan_2012_360_720_50-1.png', 'Cruze'),
(61, 'Chevrolet', 'Medium', 'assets/cars/chevrolet/Chevrolet_Malibu_Mk9_2016_360_720_50-1.png', 'Malibu'),
(62, 'Chevrolet', 'Family', 'assets/cars/chevrolet/Chevrolet_Tahoe_Mk4_GMTK2UC_2014_360_720_50-1.png', 'Tahoe'),
(63, 'Chevrolet', 'Family', 'assets/cars/chevrolet/Chevrolet_Suburban_Mk12_GMTK2YC_LTZ_2014_360_720_50-1.png', 'Suburban'),
(64, 'Chevrolet', 'Family', 'assets/cars/chevrolet/Chevrolet_Traverse_Mk2_2017_360_720_50-1.png', 'Traverse'),
(65, 'Chevrolet', 'Family', 'assets/cars/chevrolet/Chevrolet_Trailblazer_2012_360_720_50-1.png', 'Trailblazer'),
(66, 'Volvo', 'Medium', 'assets/cars/volvo/Volvo_S60_Mk2f_2013_360_720_50-1.png', 'S60'),
(67, 'Volvo', 'Medium', 'assets/cars/volvo/Volvo_S90_Mk2_2017_360_720_50-1.png', 'V90'),
(68, 'Volvo', 'Medium', 'assets/cars/volvo/Volvo_V60_Mk1f_2013_360_720_50-1.png', 'V60'),
(69, 'Volvo', 'Medium', 'assets/cars/volvo/Volvo_S90_Mk2_2017_360_720_50-1.png', 'S90'),
(70, 'Volvo', 'Medium', 'assets/cars/volvo/Volvo_XC90_Mk2_Heico_2016_360_720_50-1.png', 'XC90'),
(71, 'Volvo', 'Medium', 'assets/cars/volvo/Volvo_V40_T5_R-Design_2016_360_720_50-1.png', 'V40'),
(72, 'Volvo', 'Family', 'assets/cars/volvo/Volvo_V90_Mk2_T6_Inscription_2016_360_720_50-1.png', 'V90'),
(73, 'Volvo', 'Family', 'assets/cars/volvo/Volvo_V60_Mk1f_2013_360_720_50-1.png', 'V60'),
(74, 'Volvo', 'Luxury', 'assets/cars/volvo/Volvo_S90_Mk2_2017_360_720_50-1.png', 'S90'),
(75, 'Volkswagen', 'Medium', 'assets/cars/volkswagen/Volkswagen_Passat_NMS_R-line_2016_360_720_50-1.png', 'Passat'),
(76, 'Volkswagen', 'Medium', 'assets/cars/volkswagen/Volkswagen_Golf_Mk7_Hatchback_5door_GTE_2015_360_720_50-1.png', 'Golf'),
(77, 'Volkswagen', 'Medium', 'assets/cars/volkswagen/Volkswagen_Jetta_A5_2010_360_720_50-1.png', 'Jetta'),
(78, 'Volkswagen', 'Family', 'assets/cars/volkswagen/Volkswagen_Tiguan_Mk2_Highline_2015_360_720_50-1.png', 'Tiguan'),
(79, 'Volkswagen', 'Luxury', 'assets/cars/volkswagen/Volkswagen_CC_Mk2_R-Line_2013_360_720_50-1.png', 'CC'),
(80, 'Volkswagen', 'Luxury', 'assets/cars/volkswagen/Volkswagen_Tiguan_Mk2_Highline_2015_360_720_50-1.png', 'Tiguan'),
(81, 'Volkswagen', 'Luxury', 'assets/cars/caar.png', 'Volkswagen '),
(82, 'Cadillac', 'Medium', 'assets/cars/cadillac/Cadillac_ATS_Mk1f_coupe_V_2017_1000_0001.png', 'ATS'),
(83, 'Cadillac', 'Medium', 'assets/cars/cadillac/Cadillac_XT5_2016_360_720_50-1.png', 'XT5'),
(84, 'Cadillac', 'Medium', 'assets/cars/cadillac/Cadillac_XTS_Mk1f_2016_360_720_50-1.png', 'XTS'),
(85, 'Cadillac', 'Luxury', 'assets/cars/cadillac/Cadillac_ATS_Mk1f_coupe_V_2017_1000_0001.png', 'ATS'),
(86, 'Cadillac', 'Luxury', 'assets/cars/cadillac/Cadillac_CTS_Mk3_Premium_Luxury_2017_1000_0001.png', 'CTS'),
(87, 'Cadillac', 'Luxury', 'assets/cars/cadillac/Cadillac_XT5_2016_360_720_50-1.png', 'XT5'),
(88, 'Cadillac', 'Luxury', 'assets/cars/cadillac/Cadillac_XTS_Mk1f_2016_360_720_50-1.png', 'XTS'),
(89, 'Cadillac', 'Luxury', 'assets/cars/cadillac/Cadillac_Escalade_Mk4_GMTK2XL_EU-spec_2015_360_720_50-1.png', 'Escalade'),
(90, 'Chrysler', 'Medium', 'assets/cars/Chrysler/Chrysler_300_SRT8_2012_360_720_50-1.png', '300'),
(91, 'Land Rover', 'Luxury', 'assets/cars/land rover/Land-Rover_Range_Rover_Mk4_L405_Vogue_2014_360_720_50-1.png', 'Land Rover'),
(92, 'KIA', 'Medium', 'assets/cars/KIA/Kia_Sorento_2011_360_720_50-1.png', 'Sorento'),
(93, 'KIA', 'Medium', 'assets/cars/KIA/Kia_Optima_Mk4_wagon_2017_360_720_50-1.png', 'Optima'),
(94, 'KIA', 'Medium', 'assets/cars/KIA/Kia_Sportage_2011_360_720_50-1.png', 'Sportage'),
(95, 'KIA', 'Medium', 'assets/cars/caar.png', 'Cerato'),
(96, 'KIA', 'Medium', 'assets/cars/KIA/Kia_Cadenza_2014_360_720_50-1.png', 'Cadenza'),
(97, 'KIA', 'Medium', 'assets/cars/KIA/Kia_Carens_Mk2_UN_2006_360_720_50-1.png', 'Carens'),
(98, 'KIA', 'Medium', 'assets/cars/caar.png', 'Carnival'),
(99, 'KIA', 'Medium', 'assets/cars/KIA/Kia_Mohave_HM_2012_360_720_50-1.png', 'Mohave'),
(100, 'KIA', 'Family', 'assets/cars/KIA/Kia_Sorento_2011_360_720_50-1.png', 'Sorento'),
(101, 'KIA', 'Family', 'assets/cars/caar.png', 'Cerato'),
(102, 'KIA', 'Family', 'assets/cars/caar.png', 'Carnival'),
(103, 'KIA', 'Luxury', 'assets/cars/caar.png', 'Quoris'),
(105, 'Lexus', 'Family', 'assets/cars/lexus/Lexus_GX_Mk2f_J150_2013_360_720_50-1.png', 'GX'),
(106, 'Lexus', 'Luxury', 'assets/cars/lexus/Lexus_ES_Mk6f_XV60_hybrid_2015_360_720_50-1.png', 'ES'),
(107, 'Lexus', 'Luxury', 'assets/cars/lexus/Lexus_GS_Mk4_L10_F-sport_2015_360_720_50-1.png', 'GS'),
(108, 'Lexus', 'Luxury', 'assets/cars/caar.png', 'IS'),
(109, 'Lexus', 'Luxury', 'assets/cars/lexus/Lexus_LS_Mk5_XF50_F_Sport_2018_360_720_50-1.png', 'LS'),
(110, 'Lexus', 'Luxury', 'assets/cars/lexus/Lexus_LX_Mk3f_J200_2016_360_720_50-1.png', 'LX'),
(111, 'Lexus', 'Luxury', 'assets/cars/lexus/Lexus_NX_F-sport_HQinterior_2014_1000_0001.png', 'NX'),
(112, 'Lexus', 'Luxury', 'assets/cars/lexus/Lexus_GX_Mk2f_J150_2013_360_720_50-1.png', 'GX'),
(113, 'Lexus', 'Luxury', 'assets/cars/lexus/Lexus_RX_Mk4_350_2016_360_720_50-1.png', 'RX'),
(114, 'Mazda', 'Medium', 'assets/cars/Mazda/Mazda_CX-5_Mk2_2017_360_720_50-1.png', 'CX-5'),
(115, 'Mazda', 'Medium', 'assets/cars/Mazda/Mazda_CX-9_Mk3_2016_360_720_50-1.png', 'CX-9'),
(116, 'Mazda', 'Medium', 'assets/cars/Mazda/Mazda_3_Mk3_BM_sedan_2013_360_720_50-1.png', 'Mazda3'),
(117, 'Mazda', 'Medium', 'assets/cars/Mazda/Mazda_6_Mk3f_GJ_sedan_2015_360_720_50-1.png', 'Mazda6'),
(118, 'Mazda', 'Family', 'assets/cars/Mazda/Mazda_CX-9_Mk3_2016_360_720_50-1.png', 'CX-9'),
(119, 'Ford', 'Medium', 'assets/cars/Ford/Ford_Explorer_Mk5f_U502_Platinum_HQinterior_2015_1000_0001.png', 'Explorer'),
(120, 'Ford', 'Medium', 'assets/cars/Ford/Ford_Escape_HQinterior_2013_360_720_50-1.png', 'Escape'),
(121, 'Ford', 'Medium', 'assets/cars/Ford/Ford_Edge_Mk3_Vignale_2016_360_720_50-1.png', 'Edge'),
(122, 'Ford', 'Medium', 'assets/cars/Ford/Ford_Taurus_SHO_2013_360_720_50-1.png', 'Taurus'),
(123, 'Ford', 'Medium', 'assets/cars/Ford/Ford_Flex_2013_360_720_50-1.png', 'Flex'),
(124, 'Ford', 'Medium', 'assets/cars/Ford/Ford_Fusion_Mk5_sedan_Vignale_2015_360_720_50-1.png', 'Fusion'),
(125, 'Ford', 'Medium', 'assets/cars/Ford/Ford_Focus_Mk3f_hatchback_ST_2015_360_720_50-1.png', 'Focus'),
(126, 'Ford', 'Medium', 'assets/cars/Ford/Ford_Mondeo_Mk5_sedan_Vignale_2015_360_720_50-1.png', 'Mondeo'),
(127, 'Ford', 'Family', 'assets/cars/Ford/Ford_Explorer_Mk5f_U502_Platinum_HQinterior_2015_1000_0001.png', 'Explorer'),
(128, 'Ford', 'Family', 'assets/cars/Ford/Ford_Expedition_Mk4_U553_MAX_Platinum_2017_360_720_50-1.png', 'Expedition'),
(129, 'Ford', 'Family', 'assets/cars/Ford/Ford_Flex_2013_360_720_50-1.png', 'Flex'),
(130, 'Ford', 'Luxury', 'assets/cars/Ford/Ford_Edge_Mk3_Vignale_2016_360_720_50-1.png', 'Edge'),
(131, 'Dodge', 'Medium', 'assets/cars/Dodge/Dodge_Neon_Mk3_MX-spec_2016_360_720_50-1.png', 'Neon'),
(132, 'Dodge', 'Medium', 'assets/cars/Dodge/Dodge_Charger_Mk7f_LD_2015_360_720_50-1.png', 'Charger'),
(133, 'Dodge', 'Family', 'assets/cars/Dodge/Dodge_Durango_Mk3_WD_SRT_2017_360_720_50-1.png', 'Durango'),
(134, 'Dodge', 'Luxury', 'assets/cars/Dodge/Dodge_Charger_Mk7f_LD_2015_360_720_50-1.png', 'charger'),
(135, 'Dodge', 'Luxury', 'assets/cars/Dodge/Dodge_Durango_Mk3_WD_SRT_2017_360_720_50-1.png', 'Durango'),
(136, 'Mercedes Benz', 'Medium', 'assets/cars/caar.png', 'A Class'),
(137, 'Mercedes Benz', 'Luxury', 'assets/cars/caar.png', 'CLA'),
(138, 'Mercedes Benz', 'Luxury', 'assets/cars/caar.png', 'C class'),
(139, 'Mercedes Benz', 'Luxury', 'assets/cars/caar.png', 'E class'),
(140, 'Mercedes Benz', 'Luxury', 'assets/cars/caar.png', 'S class'),
(141, 'Mitsubishi', 'Medium', 'assets/cars/caar.png', 'Lancer'),
(142, 'Mitsubishi', 'Medium', 'assets/cars/caar.png', 'ASX'),
(143, 'Mitsubishi', 'Medium', 'assets/cars/caar.png', 'Galant'),
(144, 'Mitsubishi', 'Medium', 'assets/cars/caar.png', 'Outlander'),
(145, 'Mitsubishi', 'Medium', 'assets/cars/caar.png', 'pajero'),
(157, 'Nissan', 'Medium', 'assets/cars/caar.png', 'Altima'),
(158, 'Nissan', 'Medium', 'assets/cars/caar.png', 'Sunny'),
(159, 'Nissan', 'Family', 'assets/cars/caar.png', 'Armada'),
(160, 'Nissan', 'Luxury', 'assets/cars/caar.png', 'Armada'),
(161, 'Nissan', 'Medium', 'assets/cars/caar.png', 'Sentra'),
(162, 'Nissan', 'Family', 'assets/cars/caar.png', 'Patrol'),
(163, 'Nissan', 'Family', 'assets/cars/caar.png', 'Pathfinder'),
(164, 'Nissan', 'Luxury', 'assets/cars/caar.png', 'Pathfinder'),
(165, 'Nissan', 'Medium', 'assets/cars/caar.png', 'Maxima'),
(166, 'Nissan', 'Medium', 'assets/cars/caar.png', 'Murano'),
(167, 'Honda', 'Medium', 'assets/cars/caar.png', 'Civic'),
(168, 'Honda', 'Medium', 'assets/cars/caar.png', 'CR-V'),
(169, 'Honda', 'Medium', 'assets/cars/caar.png', 'Accord'),
(170, 'Honda', 'Family', 'assets/cars/caar.png', 'Pilot'),
(171, 'Mitsubishi', 'Family', 'assets/cars/caar.png', 'pajero'),
(187, 'Hyundai', 'Medium', 'assets/cars/caar.png', 'Azera'),
(188, 'Hyundai', 'Luxury', 'assets/cars/caar.png', 'Centnetial'),
(189, 'Hyundai', 'Medium', 'assets/cars/caar.png', 'Elantra'),
(190, 'Hyundai', 'Medium', 'assets/cars/caar.png', 'Tucson'),
(191, 'Hyundai', 'Medium', 'assets/cars/caar.png', 'Genesis'),
(192, 'Hyundai', 'Luxury', 'assets/cars/caar.png', 'Genesis'),
(193, 'Hyundai', 'Medium', 'assets/cars/caar.png', 'Santa fe'),
(194, 'Hyundai', 'Family', 'assets/cars/caar.png', 'Santa fe'),
(195, 'Hyundai', 'Medium', 'assets/cars/caar.png', 'Accent'),
(196, 'Hyundai', 'Medium', 'assets/cars/caar.png', 'Sonata'),
(197, 'Hyundai', 'Medium', 'assets/cars/caar.png', 'Creta'),
(198, 'Renault', 'Medium', 'assets/cars/caar.png', 'Talisman'),
(199, 'Renault', 'Medium', 'assets/cars/caar.png', 'Koleos'),
(200, 'Renault', 'Family', 'assets/cars/caar.png', 'Koleos'),
(201, 'Renault', 'Medium', 'assets/cars/caar.png', 'Duster'),
(202, 'MG', 'Medium', 'assets/cars/caar.png', 'MG3'),
(203, 'MG', 'Medium', 'assets/cars/caar.png', 'MG350'),
(204, 'MG', 'Medium', 'assets/cars/caar.png', 'MG5'),
(205, 'MG', 'Medium', 'assets/cars/caar.png', 'MG550'),
(206, 'MG', 'Medium', 'assets/cars/caar.png', 'MG6'),
(207, 'MG', 'Medium', 'assets/cars/caar.png', 'MG750'),
(208, 'MG', 'Luxury', 'assets/cars/caar.png', 'MG750'),
(209, 'Lincoln', 'Medium', 'assets/cars/Lincoln/Lincoln_Continental_concept_2015_360_720_50-1.png', 'Continental'),
(210, 'Lincoln', 'Luxury', 'assets/cars/Lincoln/Lincoln_Continental_concept_2015_360_720_50-1.png', 'Continental'),
(211, 'Lincoln', 'Medium', 'assets/cars/Lincoln/Lincoln_MKZ_Mk2f_2017_360_720_50-1.png', 'MKZ'),
(212, 'Lincoln', 'Luxury', 'assets/cars/Lincoln/Lincoln_MKZ_Mk2f_2017_360_720_50-1.png', 'MKZ'),
(213, 'Lincoln', 'Family', 'assets/cars/Lincoln/Lincoln_MKC_2013_360_720_50-1.png', 'MKC'),
(214, 'Lincoln', 'Family', 'assets/cars/Lincoln/Lincoln_MKX_Mk2_2016_360_720_50-1.png', 'MKX'),
(215, 'Lincoln', 'Family', 'assets/cars/Lincoln/Lincoln_Navigator_Mk4_U554_Black_Label_2017_360_720_50-1.png', 'Navigator'),
(216, 'Lincoln', 'Luxury', 'assets/cars/Lincoln/Lincoln_Navigator_Mk4_U554_Black_Label_2017_360_720_50-1.png', 'Navigator'),
(217, 'Suzuki', 'Medium', 'assets/cars/Suzuki/Suzuki_Baleno_2016_360_720_50-1.png', 'Baleno'),
(218, 'Suzuki', 'Medium', 'assets/cars/Suzuki/Suzuki_Maruti_Celerio_2014_360_720_50-1.png', 'Celerio'),
(219, 'Suzuki', 'Medium', 'assets/cars/Suzuki/Suzuki_Maruti_Ciaz_2014_360_720_50-1.png', 'Ciaz'),
(220, 'Suzuki', 'Medium', 'assets/cars/Suzuki/Suzuki_Maruti_Ertiga_2012_360_720_50-1.png', 'Ertiga'),
(221, 'Peugeot', 'Medium', 'assets/cars/Peugeot/Peugeot_208_Mk1f_3door_GTI_2015_360_720_50-1.png', '208'),
(222, 'Peugeot', 'Medium', 'assets/cars/Peugeot/Peugeot_308_R_HYbrid_concept_2015_360_720_50-1.png', '308'),
(223, 'Peugeot', 'Medium', 'assets/cars/Peugeot/Peugeot_508_Mk1f_RXH_2014_360_720_50-1.png', '508'),
(224, 'Peugeot', 'Luxury', 'assets/cars/caar.png', '608'),
(226, 'Honda', 'Luxury', 'assets/cars/caar.png', 'Pilot'),
(265, 'Jaguar', 'Luxury', 'assets/cars/Jaguar/Jaguar_XE_R-Sport_2017_360_720_50-1.png', 'XE'),
(266, 'Jaguar', 'Luxury', 'assets/cars/Jaguar/Jaguar_XF_Mk2_S_2016_360_720_50-1.png', 'XF'),
(267, 'Jaguar', 'Luxury', 'assets/cars/caar.png', 'XJ'),
(268, 'Jaguar', 'Luxury', 'assets/cars/caar.png', 'F-Pace'),
(269, 'Porsche', 'Luxury', 'assets/cars/Porsche/Porsche_Panamera_Mk2_Turbo_2017_360_720_50-1.png', 'Panamera'),
(270, 'Porsche', 'Luxury', 'assets/cars/Porsche/Porsche_Macan_GTS_2017_360_720_50-1.png', 'Macan'),
(271, 'Porsche', 'Luxury', 'assets/cars/Porsche/Porsche_Cayenne_Mk3f_958_Turbo_2014_360_720_50-1.png', 'Cayenne'),
(272, 'Geely', 'Medium', 'assets/cars/caar.png', 'EC7'),
(273, 'Geely', 'Medium', 'assets/cars/Geely/Emgrand_X7_2014_360_720_50-1.png', 'X7'),
(274, 'Geely', 'Medium', 'assets/cars/Geely/Emgrand_EC8_2010_360_720_50-1.png', 'EC8'),
(275, 'Geely', 'Medium', 'assets/cars/Geely/Geely_GC6_2014_360_720_50-1.png', 'GC6'),
(276, 'Geely', 'Medium', 'assets/cars/Geely/Geely_GC7_Vision_2015_360_720_50-1.png', 'GC7'),
(277, 'Geely', 'Medium', 'assets/cars/Geely/Geely_Emgrand_EC7_Mk1f_2014_360_720_50-1.png', 'Emgrand'),
(292, 'KIA', 'Family', 'assets/cars/KIA/Kia_Mohave_HM_2012_360_720_50-1.png', 'bmw');

-- --------------------------------------------------------

--
-- Table structure for table `halal_details`
--

CREATE TABLE `halal_details` (
  `id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `halal` double(11,2) DEFAULT '0.00',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `last_location`
--

CREATE TABLE `last_location` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `last_location`
--

INSERT INTO `last_location` (`id`, `driver_id`, `city`, `longitude`, `latitude`, `created`) VALUES
(1, 2, 'Khobar', '50.1971381', '26.2171906', '2018-01-09 07:27:18'),
(2, 2, 'Khafji', '48.4887224', '28.4256618', '2018-01-22 09:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` longtext,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` double DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `luggage`
--

CREATE TABLE `luggage` (
  `id` int(11) DEFAULT NULL,
  `type_of_luggage` varchar(255) DEFAULT NULL,
  `total_no_baggage` int(11) DEFAULT NULL,
  `total_weight` varchar(255) DEFAULT NULL,
  `created` datetime(3) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_airport_zone`
--

CREATE TABLE `master_airport_zone` (
  `id` int(12) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_airport_zone`
--

INSERT INTO `master_airport_zone` (`id`, `name`, `lat`, `lng`, `created`) VALUES
(1, 'Abu Dhabi International Airport', '24.4419429', '54.6478849', '2017-10-14 12:47:45'),
(2, 'Al Dhafra Military Airport', '24.25041', '54.5466147', '2017-10-14 12:48:04'),
(3, 'Bateen Airport', '24.4288169', '54.4537414', '2017-10-14 12:48:56'),
(4, 'Al Ain Airport', '24.2585201', '55.6172501', '2017-10-14 12:49:29'),
(5, 'Fujairah International Airport', '25.1097268', '56.3283673', '2017-10-14 12:49:45'),
(6, 'Dubai International Airport', '25.2531793', '55.3634841', '2017-10-14 12:50:21'),
(7, 'Ras Al Khaimah Airport', '25.6158607', '55.9371963', '2017-10-14 12:50:46'),
(8, 'Sharjah International Airport', '25.32844', '55.510069', '2017-10-14 12:51:01'),
(10, 'مطار الملك فهد الدولي', NULL, NULL, '2017-11-04 14:26:23');

-- --------------------------------------------------------

--
-- Table structure for table `master_cancel_options`
--

CREATE TABLE `master_cancel_options` (
  `id` int(11) NOT NULL,
  `option_en` longtext CHARACTER SET latin1 NOT NULL,
  `option_ar` longtext CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_cancel_options`
--

INSERT INTO `master_cancel_options` (`id`, `option_en`, `option_ar`) VALUES
(1, 'Cab is running late.', 'arabic data will be here'),
(2, 'Cab is running opposite direction.', 'arabic data will be here'),
(3, 'Driver denied for duty.', 'arabic data will be here'),
(4, 'I am usning any other mode of commut.', 'arabic data will be here'),
(5, 'I might be late for cab.', 'arabic data will be here');

-- --------------------------------------------------------

--
-- Table structure for table `master_car_brand_logo`
--

CREATE TABLE `master_car_brand_logo` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_car_brand_logo`
--

INSERT INTO `master_car_brand_logo` (`id`, `name`, `path`) VALUES
(1, 'Audi', 'assets/cars/audi.png'),
(2, 'BMW', 'assets/cars/BMW.png'),
(3, 'Cadillac', 'assets/cars/Cadillac.png'),
(4, 'Chevolet', 'assets/cars/chevolet.png'),
(5, 'Chrysler', 'assets/cars/Chrysler.png'),
(6, 'Dodge', 'assets/cars/dodge.png'),
(7, 'Ford', 'assets/cars/Ford.png'),
(8, 'Geely', 'assets/cars/geely.png'),
(9, 'GMC', 'assets/cars/GMC.png'),
(10, 'Honda', 'assets/cars/Honda.png'),
(11, 'Hyundai', 'assets/cars/Hyundai.png'),
(12, 'Infiniti', 'assets/cars/infiniti.png'),
(13, 'Jeep', 'assets/cars/JEEP.png'),
(14, 'Jeguar', 'assets/cars/Jeguar.png'),
(15, 'KIA', 'assets/cars/KIA.png'),
(16, 'Land Rover', 'assets/cars/Land_Rover.png'),
(17, 'Lexus', 'assets/cars/Lexus.png'),
(18, 'Lincoln', 'assets/cars/Lincoln.png'),
(19, 'Mazda', 'assets/cars/Mazda.png'),
(20, 'Mercedes', 'assets/cars/Mercedes.png'),
(21, 'MG', 'assets/cars/MG.png'),
(22, 'Mitsubushi', 'assets/cars/mitsubushi.png'),
(23, 'Nissan', 'assets/cars/nissan.png'),
(24, 'Peugeot', 'assets/cars/peugeot1.png'),
(25, 'Porsche', 'assets/cars/porsche.png'),
(26, 'Renault', 'assets/cars/Renault.png'),
(27, 'Suzuki', 'assets/cars/suzuki.png'),
(28, 'Toyota', 'assets/cars/toyola.png'),
(29, 'Volvo', 'assets/cars/volvo.png'),
(30, 'VW', 'assets/cars/VW.png');

-- --------------------------------------------------------

--
-- Table structure for table `master_charge_type`
--

CREATE TABLE `master_charge_type` (
  `id` int(12) NOT NULL,
  `ChargeName` varchar(255) DEFAULT '0',
  `DisplayName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_charge_type`
--

INSERT INTO `master_charge_type` (`id`, `ChargeName`, `DisplayName`) VALUES
(1, 'pkm', 'Per Kilo Meter'),
(2, 'wpm', 'Waiting Per Minute'),
(3, 'rns', 'Ride Now Starting'),
(4, 'rnm', 'Ride Now Minimun'),
(5, 'rls', 'Ride Later Starting'),
(6, 'rlm', 'Ride Later Minimum'),
(7, 'arp', 'Airport Charge');

-- --------------------------------------------------------

--
-- Table structure for table `master_country_code`
--

CREATE TABLE `master_country_code` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_country_code`
--

INSERT INTO `master_country_code` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `master_driver_cancel_options`
--

CREATE TABLE `master_driver_cancel_options` (
  `id` int(11) NOT NULL,
  `option_en` longtext CHARACTER SET latin1 NOT NULL,
  `option_ar` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `master_driver_cancel_options`
--

INSERT INTO `master_driver_cancel_options` (`id`, `option_en`, `option_ar`) VALUES
(1, 'I have waited more than 5 minutes and I am unable to make contact with Customer.', 'لقد إنتظرت أکثر من خمس دقائق و أنا غیر قادر علی التواصل مع العمیل');

-- --------------------------------------------------------

--
-- Table structure for table `master_feedback`
--

CREATE TABLE `master_feedback` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_ride_status`
--

CREATE TABLE `master_ride_status` (
  `id` int(12) NOT NULL,
  `ride_status` varchar(255) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_ride_status`
--

INSERT INTO `master_ride_status` (`id`, `ride_status`, `flag`) VALUES
(1, 'Accepted', 1),
(2, 'Ongoing', 2),
(3, 'Completed', 3),
(4, 'Canceled', 4);

-- --------------------------------------------------------

--
-- Table structure for table `master_user_cancel_options`
--

CREATE TABLE `master_user_cancel_options` (
  `id` int(11) NOT NULL,
  `option_en` longtext CHARACTER SET latin1 NOT NULL,
  `option_ar` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `master_user_cancel_options`
--

INSERT INTO `master_user_cancel_options` (`id`, `option_en`, `option_ar`) VALUES
(1, 'There is delay in cab arrival.', 'ھناك تأخیر فی وصول سيارة الأجرة'),
(2, 'I am not able to contact driver.', 'لا استطيع أن اتصل بالسائق'),
(3, 'Driver\'s ETA is longer than expected.', 'الوقت المقدّر لوصول السائق أطول من ما توقعنا'),
(4, 'I don\'t want to ride anymore.', 'لا أريد هذه الرحلة الآن'),
(5, 'My pickup location was incorrect.', 'إن موقع نقطة بداية الرحلة كانت خاطئة'),
(6, 'Driver asked me to cancel the ride.', 'طلب السائق مني إلغاء الرحلة');

-- --------------------------------------------------------

--
-- Table structure for table `master_user_roles`
--

CREATE TABLE `master_user_roles` (
  `id` int(11) NOT NULL,
  `name_en` varchar(50) DEFAULT NULL,
  `name_ar` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `master_user_roles`
--

INSERT INTO `master_user_roles` (`id`, `name_en`, `name_ar`) VALUES
(1, 'Backend User', NULL),
(2, 'Customer', NULL),
(3, 'Email Template', NULL),
(4, 'Drivers', NULL),
(5, 'Pages', NULL),
(6, 'Vouchers', NULL),
(7, 'Promotions', NULL),
(8, 'Fare Rate Card', NULL),
(9, 'Reports', NULL),
(10, 'Settings', NULL),
(11, 'wallet transaction details', NULL),
(12, 'Tools', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `najez`
--

CREATE TABLE `najez` (
  `id` int(11) NOT NULL,
  `total_amount` double(13,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `najez`
--

INSERT INTO `najez` (`id`, `total_amount`) VALUES
(1, 93.00);

-- --------------------------------------------------------

--
-- Table structure for table `najez_transactions`
--

CREATE TABLE `najez_transactions` (
  `id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `najez_transactions`
--

INSERT INTO `najez_transactions` (`id`, `ride_id`, `amount`, `created`) VALUES
(1, 1, 2, '2018-01-05 13:59:32'),
(2, 5, 2, '2018-01-05 14:50:23'),
(3, 6, 3, '2018-01-07 06:28:38'),
(4, 10, 3, '2018-01-08 09:38:00'),
(5, 11, 3, '2018-01-08 10:01:47'),
(6, 12, 3, '2018-01-08 10:07:15'),
(7, 15, 2, '2018-01-08 13:43:55'),
(8, 17, 3, '2018-01-09 11:22:41'),
(9, 29, 2, '2018-01-10 12:35:10'),
(10, 31, 2, '2018-01-10 12:39:58'),
(11, 32, 2, '2018-01-10 12:41:14'),
(12, 38, 3, '2018-01-11 11:16:31'),
(13, 43, 3, '2018-01-11 11:27:34'),
(14, 45, 3, '2018-01-11 11:34:51'),
(15, 46, 3, '2018-01-11 11:38:11'),
(16, 47, 3, '2018-01-11 11:48:27'),
(17, 49, 3, '2018-01-11 11:55:57'),
(18, 50, 3, '2018-01-11 12:06:22'),
(19, 52, 3, '2018-01-11 13:37:23'),
(20, 53, 3, '2018-01-11 13:41:39'),
(21, 56, 3, '2018-01-12 08:28:14'),
(22, 57, 3, '2018-01-12 08:31:06'),
(23, 58, 3, '2018-01-12 08:34:13'),
(24, 60, 3, '2018-01-12 08:51:15'),
(25, 61, 3, '2018-01-12 09:11:07'),
(26, 62, 3, '2018-01-12 09:17:26'),
(27, 66, 3, '2018-01-12 10:10:09'),
(28, 67, 3, '2018-01-12 10:24:17'),
(29, 68, 3, '2018-01-12 10:25:54'),
(30, 69, 3, '2018-01-12 10:27:36'),
(31, 71, 3, '2018-01-12 10:46:00'),
(32, 73, 3, '2018-01-12 10:58:32'),
(33, 74, 3, '2018-01-12 11:23:14');

-- --------------------------------------------------------

--
-- Table structure for table `notification_data`
--

CREATE TABLE `notification_data` (
  `id` int(12) NOT NULL,
  `event` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `type` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `message_en` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `message_ar` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin7 COLLATE=latin7_bin;

--
-- Dumping data for table `notification_data`
--

INSERT INTO `notification_data` (`id`, `event`, `type`, `message_en`, `message_ar`) VALUES
(1, 'book_ride', 'user', 'Dear  %person_name%, \r\nYour driver %driver_name% will arrive at your location in %eta%. \r\n%driver_mobile%,\r\n%vehicle_number%, %car_model%.', 'عزیزي %person_name%\r\nإن سائقك %driver_name%  یصل إلي موقعك في [الوقت المقدّر للوصول(%eta%)]. \r\n%driver_mobile%,\r\n%vehicle_number%, %car_model%.'),
(2, 'driver_ride_notification', 'driver', 'Dear %person_name% your ride is waiting for you at %pickup_location%.', 'Dear %person_name% your ride is waiting for you at %pickup_location%.'),
(3, 'driver_arrival1', 'user', 'Dear rider, your ride is arriving soon. Your driver will wait for 5 minutes before leaving.\r\nEnjoy the ride!', 'Dear rider, your ride is arriving soon. Your driver will wait for 5 minutes before leaving.\r\nEnjoy the ride!'),
(4, 'driver_arrival2', 'user', 'Dear rider %customer_name% your driver %driver_name% is coming with car %car_number% is arriving in Minutes %arrival_time%.', 'Dear rider %customer_name% your driver %driver_name% is coming with car %car_number% is arriving in Minutes %arrival_time%.'),
(5, 'driver_ride_cancel', 'user', 'Dear rider, Your driver %person_name% has cancelled the ride after waiting for 5 minutes.\r\n%driver_cancel_ride_reason%', 'Dear rider, Your driver %person_name% has cancelled the ride after waiting for 5 minutes.\r\n%driver_cancel_ride_reason%'),
(6, 'user_ride_cance', 'driver', 'Dear Driver your rider has cancelled the ride due to ETA is more.\r\n%user_cancel_ride_reason%', 'Dear Driver your rider has cancelled the ride due to ETA is more.\r\n%user_cancel_ride_reason%'),
(7, 'start_trip', 'user', 'Your trip has started.', 'Your trip has started.'),
(8, 'trip_complete', 'user', 'Dear %person_name% You have reached your destination.', 'Dear %person_name% You have reached your destination.'),
(9, 'password_reset', 'user', 'Dear %person_name% your password has been reset.', 'Dear %person_name% your password has been reset.'),
(10, 'password_reset', 'common', 'password changed', 'تم تغییر کلمة المرور'),
(11, 'low_earning', 'driver', 'Dear %person_name% your account is running low on balance. Please recharge and enjoy the rides.', 'Dear %person_name% your account is running low on balance. Please recharge and enjoy the rides.'),
(12, 'ride_payment', 'user', 'Dear %person_name%, your payment was successful.', 'Dear %person_name%, your payment was successful.'),
(13, 'vehicle_approved', 'driver', 'Dear %person_name%, your documents have been approved, you can drive and earn with Najez.', 'Dear %person_name%, your documents have been approved, you can drive and earn with Najez.'),
(14, 'profile_update', 'user', 'Dear %person_name% Your information is updated.', 'Dear %person_name% Your information is updated.'),
(15, 'promotion_referral', 'driver', 'Want more great rides?\r\nGet free rides worth up to xxxxx ,when you refer to a friend to try Najez.\r\nYour invite code:\r\n%referral_code%', 'Want more great rides?\r\nGet free rides worth up to xxxxx ,when you refer to a friend to try Najez.\r\nYour invite code:\r\n%referral_code%'),
(16, 'promotion_referral', 'user', 'Want more great rides?\r\nGet free rides worth up to xxxxx ,when you refer to a friend to try Najez.\r\nYour invite code:\r\n%referral_code%', 'Want more great rides?\r\nGet free rides worth up to xxxxx ,when you refer to a friend to try Najez.\r\nYour invite code:\r\n%referral_code%'),
(17, 'schedule_ride', 'user', 'Dear %person_name%, your ride is scheduled from %pickup_location% to %drop_location% at %booking_time%.\r\nEnjoy the ride!', 'Dear %person_name%, your ride is scheduled from %pickup_location% to %drop_location% at %booking_time%.\r\nEnjoy the ride!'),
(18, 'ride_cancel2', 'user', 'Dear %person_name%,You cancelled the ride. Your charges for cancellation are %ride_amount% SAR. Thanks for choosing Najez !', 'عزیزي %person_name%\r\n\r\nأنت قد إلغیت الرحلة ۔ تکالیف الإلغاء الخاص بك ھو %ride_amount% ریال سعودي۔ شکرا لك لإستخدام ناجز۔'),
(19, 'schedule_ride2', 'user', 'Dear %person_name%, There are no drivers available near your location. Please book another ride. We regret for the inconvenice.', 'عزیزي %person_name%\r\n\r\nلا یوجد أي سائق قریبا من موقعك۔ یرجي تحجیز الرحلة الأخري ۔نحن نأسف للإزعاج۔'),
(20, 'transfer_money', 'user', 'Your wallet is updated with %balance% SAR. Book your ride soon.', 'قد تم تحدیث محفظتك مع %balance% ریال سعودي۔أحرحلتك فی وقت ممکن۔جز'),
(21, 'transfer_money', 'driver', 'Your wallet is updated with %balance% SAR. You can now continue with more rides, Enjoy with Najez.', 'قد تم تحدیث محفظتك مع %balance% ریال سعودي۔أحرحلتك فی وقت ممکن۔جز'),
(22, 'referal_amount', 'user', 'Dear %person_name%,Refer your friends and earn rides with Najez.Your referral code is \"%referral_code%\"', 'عزیزي %person_name%،\r\nیرجی الإحالة إلی أصدقائك ویمکنك حصول الرحلات مع ناجز۔رمز إحالتك ھو %ride_amount%'),
(23, 'referal_amount', 'driver', 'Dear %person_name%,Refer your friends and earn rides with Najez.Your referral code is \"%referral_code%\"', 'عزیزي %person_name%،\r\nیرجی الإحالة إلی أصدقائك ویمکنك حصول الرحلات مع ناجز۔رمز إحالتك ھو %ride_amount%'),
(24, 'ride_cancel1', 'user', 'Dear %person_name%,You have not been charged any cancellation fee for [%ride_id%]', 'عزیزي %person_name% \r\nإنك لم تکن تفرض علي أي دفع رسوم الإلغاء لـ[%ride_id%]'),
(25, 'trip_scheduled_for_later1', 'user', 'Dear %person_name%,You have scheduled a ride from %pickup_name% on %date_time%.', 'عزیزي %person_name%\r\nأنت قد قرّرت رحلة لـ%pickup_name% في %date_time%۔'),
(26, 'trip_scheduled_for_later2', 'user', 'Dear %person_name%\r\nYour scheduled ride %date_time% for %destination_name% will arrive in 15 minutes.Your driver will be %driver_name%. \r\n%vehicle_number% %car_model%.', 'عزیزي %person_name%\r\nإن رحلتك المقرّرۃ فی %date_time% لـ%destination_name% تصل إلیك في (15)خمس عشر دقیقة۔ یکون سائقك ھو%driver_name%. \r\n%vehicle_plate_number% %car_model%.'),
(27, 'ride_cancel_to_driver', 'driver', 'Dear %person_name%,Ride [%ride_id%] has been canceled by user.', 'Dear %person_name%,Ride [%ride_id%] has been canceled by user.'),
(28, 'ride_complete', 'user', 'Dear %person_name%, Your ride is complete. The fare charged is %total_fare% SAR. Rate your Najez Driver.Thanks for choosing Najez !', 'عزیزي %person_name%\r\n\r\nقد تم تکمیل رحلتك ۔ تکالیف الأجرۃ ھو  %total_fare% ریال سعودي۔ یرجي منك أن تقیم سائق ناجز۔ شکرًا لك لإستخدام ناجز۔'),
(29, 'transfer_money_message', 'user', 'Hi %person%,You will be glad to know that your Najez wallet is updated with %balance% SAR.Thanks for using Najez and stay updated with all our offers.', 'أھلا %person%,\r\n\r\nأنت ستکون مسرورا لمعرفة أن یتم تحدیث محفظة ناجز الخاصة بك مع %balance% ریال سعودی۔شکراً لإستخدام ناجز و بقاء محدثة مع جمیع عروضنا۔\r\n\r\n'),
(30, 'transfer_money_message', 'driver', 'Hi %person%,You will be glad to know that your Najez wallet is updated with %balance% SAR.Thanks for using Najez and stay updated with all our offers.', 'أھلا %person%,\r\n\r\nأنت ستکون مسرورا لمعرفة أن یتم تحدیث محفظة ناجز الخاصة بك مع %balance% ریال سعودی۔شکراً لإستخدام ناجز و بقاء محدثة مع جمیع عروضنا۔\r\n\r\n'),
(31, 'empty_deviceid', 'common', 'Device Id is empty', 'قطعة الجھاز فارغة'),
(32, 'opt_not_match', 'common', 'Verification number not matched, please generate new.', 'رقم التحقق غیر متطابق، الرجاء إنشاء الجدید'),
(33, 'loggedin', 'common', 'success', 'نجاح'),
(34, 'emailid_exists', 'common', 'Email ID is already registered', 'تم تسجیل ھویة البرید الإلکترونی مسبقا'),
(35, 'mobile_exists', 'common', 'Mobile number is already registered', 'تم تسجیل رقم الھاتف مسبقا'),
(36, 'opt_sms', 'common', 'Welcome to najez, your verification number is', 'مرحباً بکم في ناجز، رقم تحققك الخاص بك ھو'),
(37, 'newregister', 'common', 'Successfully Registered', 'تم التسجیل بنجاح۔'),
(38, 'errregister', 'common', 'Error', 'خطأ'),
(39, 'detailnotvalid', 'common', 'invalid user detail.', 'تفاصیل المستخدم غیر صالح'),
(40, 'opt_match', 'common', 'successfully validated.', 'تم التحقق من الصحة بنجاح'),
(41, 'profile_update', 'common', 'profile updated successfully', 'تم تحدیث الملف الشخصیّ بنجاح'),
(42, 'server_err', 'common', 'something went wrong please try again.', 'قد حدّث خطأ،یرجي المحاولة مرۃ أخری'),
(43, 'driver_review', 'common', 'success', 'نجاح'),
(44, 'vehicle_reg', 'driver', 'vehicle registered successfully', 'تم تجسیل المرکبة بنجاح'),
(45, 'driver_online', 'driver', 'You are online now.', 'أنت متصل الآن'),
(46, 'driver_offline', 'driver', 'You are offline now.', 'أنت غیر متصل الآن'),
(47, 'nocar', 'driver', 'No car registered', 'No car registered'),
(48, 'opt_delivery', 'driver', 'you have opted for delivery', 'أنت قد إخترت للتوصیل'),
(49, 'opt_driver', 'driver', 'delivery is now off', 'التوصیل غیر متوافر الآن'),
(50, 'invalid_ref', 'common', 'Invalid Code', 'الرمز غیر صالح'),
(51, 'invalid_promo', 'common', 'Invalid Code', 'الرمز غیر صالح'),
(52, 'social_no', 'user', 'please use different number', 'یرجي إستخدام الرقم المختلف'),
(53, 'support', 'common', 'success', 'نجاح'),
(54, 'card_exists', 'common', 'card already regisered', 'تم تسجیل البطاقة مسبقاً'),
(55, 'card_save', 'common', 'card saved', 'تم حفظ البطاقة'),
(56, 'cancel for driver', 'driver', 'Dear %driver_name%,Ride %req.body.ride_id% has been canceled by user.', 'Dear %driver_name%,Ride %req.body.ride_id% has been canceled by user.'),
(57, 'success', 'common', 'success', 'نجاح'),
(58, 'iqama_exists', 'driver', 'Iqama should be unique', 'یجب أن تکون الإقامة فریدة من نوعھا۔'),
(59, 'vehicle_sno_exists', 'driver', 'Vehicle Serial Number should be unique', 'یجب أن یکون رقم المرکبة التسلسلیّ فریداً'),
(60, 'vehicle_plat_exists', 'driver', 'Vehicle Plat Number should be unique', 'یجب أن یکون رقم لوحة المرکبة فریداً۔');

-- --------------------------------------------------------

--
-- Table structure for table `opt_language`
--

CREATE TABLE `opt_language` (
  `id` int(12) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `lang` varchar(255) DEFAULT NULL,
  `userid` int(12) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opt_language`
--

INSERT INTO `opt_language` (`id`, `type`, `lang`, `userid`, `created`, `modified`) VALUES
(1, 'driver', 'en', 1, '2018-01-05 13:53:26', '2018-01-08 10:31:58'),
(2, 'driver', 'en', 2, '2018-01-08 07:49:33', '2018-01-12 11:24:43'),
(3, 'driver', 'ar', 6, '2018-01-08 07:54:33', '2018-01-08 10:29:58'),
(4, 'user', 'en', 6, '2018-01-08 10:36:32', '2018-01-08 11:40:50'),
(5, 'driver', 'en', 7, '2018-01-08 11:14:51', '2018-01-08 11:15:36'),
(6, 'user', 'en', 1, '2018-01-08 13:31:09', '2018-01-08 13:31:09'),
(7, 'driver', 'en', 8, '2018-01-09 05:19:37', '2018-01-09 05:19:37'),
(8, 'driver', 'en', 11, '2018-01-09 10:55:58', '2018-01-09 10:55:58'),
(9, 'driver', 'en', 12, '2018-01-09 11:00:04', '2018-01-09 11:00:04'),
(10, 'user', 'en', 5, '2018-01-09 11:08:52', '2018-01-09 11:08:52'),
(11, 'user', 'en', 17, '2018-01-09 11:12:57', '2018-01-09 11:12:57'),
(12, 'user', 'en', 7, '2018-01-10 06:33:56', '2018-01-10 06:33:56'),
(13, 'user', 'en', 11, '2018-01-10 12:10:48', '2018-01-10 12:10:48'),
(14, 'user', 'en', 20, '2018-01-10 12:19:38', '2018-01-10 12:19:38'),
(15, 'user', 'ar', 10, '2018-01-10 13:37:40', '2018-01-10 13:37:40'),
(16, 'user', 'ar', 21, '2018-01-10 13:41:52', '2018-01-10 13:41:52'),
(17, 'user', 'en', 22, '2018-01-10 13:50:20', '2018-01-11 06:35:52'),
(18, 'user', 'en', 23, '2018-01-11 06:39:16', '2018-01-11 11:07:08'),
(19, 'user', 'en', 13, '2018-01-11 11:07:41', '2018-01-11 11:07:41');

-- --------------------------------------------------------

--
-- Table structure for table `opt_longtrip`
--

CREATE TABLE `opt_longtrip` (
  `id` int(12) NOT NULL,
  `cityid` int(12) DEFAULT '0',
  `userid` int(12) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `opt_longtrip`
--

INSERT INTO `opt_longtrip` (`id`, `cityid`, `userid`, `created`, `modified`) VALUES
(2, 3, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(12) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_ar` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description_en` longtext,
  `description_ar` longtext,
  `status` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `title_ar`, `slug`, `description_en`, `description_ar`, `status`, `created`) VALUES
(1, 'Privacy Policy', 'سياسة الخصوصية', 'privacy-policy', '<h3>USER PRIVACY STATEMENT :</h3>\r\n<p>At Najez we understand the meaning of personal information, so we are always committed to protect and respect your privacy.</p>\r\n<p>When you install the app of Najez and start using it, it collects your information through different interactions and communications.</p>\r\n<h3>To whom this statement applies:</h3>\r\n<p>This privacy statement applies to anyone who uses our website or mobile apps, the persons, Services to request transportation, delivery, or other on-demand services (\"Users\"). This Statement does not apply to information we collect from or about drivers, delivery man, partner transportation companies, or any other persons who use the Najez platform under license (collectively \"Partners\"). If you interact with the Services as both a User and a Partner, the respective privacy statements apply to your different interactions as described before.\r\n</p>\r\n<h3>Information which is provided by you</h3>\r\n<p>We collect information directly from you, such as when you create or modify your account, request on-demand services, contact customer support, or otherwise communicate with us. This information may include: name, email, phone number, postal address, profile picture, payment method, or any other source you use.</p>\r\n<h3>Information We Collect Through Your Use of Our Services</h3>\r\n<p>When you use our Services, we collect information about you in the following general categories:</p>\r\n<ul style=\"list-style-type: disc;\">\r\n\r\n<li><b>Your location information: </b>When you use our services, we collect precise location data about your trip from the Najez app used by the partner and you. If you permit the Najez app to access location of your device when the app is running in the foreground or background. We may also derive your approximate location from your IP address.</li>\r\n\r\n<li><b>Contacts Information:</b>If you permit the Najez app to access the address book on your device through the permission system used by your mobile platform, we may access the stored names and numbers for social interactions through our services, which are defined in other services and are declared consent.</li>\r\n\r\n<li><b>Transaction Information:</b>We collect transaction detail related to your use of our services requested, data and time the service was provided amount charged, distance travelled, and other related details.</li>\r\n\r\n<li><b>Usage and Preference Information:</b>We collect information about how you and site interact with our services, preferred expressed, and setting chosen.</li>\r\n\r\n<li><b>Device Information:</b>We collect information about your mobile device, including, the hardware model, operating system and version, file names, preferred language, unique device identifier, advertising identifiers, serial numbers, mobile network information.</li>\r\n\r\n<li><b>Call and SMS Data:</b>Our services facilitate communications between users and partners. In connection with facilitating this service, we receive call data, including the date and time of the call or SMS message, the parties’ phone numbers, and the content of the SMS message.</li>\r\n\r\n<li><b>Log Information:</b>When you interact with our services, we collect server logs, which may include information like device IP address, access dates and times, app features or pages viewed, app crashes and other system activity, type of browser, and the third-party site or service you were using before interacting with our services.</li>\r\n\r\n</ul>\r\n\r\n<h3>Information We Collect From Other Sources</h3>\r\n<p>We may also receive information from other sources and combine that with information we collect through our Services. For example:</p>\r\n\r\n\r\n\r\n\r\n<ul style=\"list-style-type: disc;\">\r\n\r\n<li>If you choose to link, create, or log in to your Najez account with a payment provider (e.g., Google Wallet) or social media service (e.g., Facebook), or if you engage with a separate app or website that uses our API (or whose API we use), we may receive information about you or your connections from that site or app.</li>\r\n\r\n<li>When you request on demand services, our Partners may provide us with a User rating after providing services to you.</li>\r\n\r\n<li>If you also interact with our Services in another capacity, for instance as a Partner or user of other apps we provide, we may combine or associate that information with information we have collected from you in your capacity as a User or rider.</li>\r\n\r\n\r\n</ul>\r\n\r\n<h3>Use of Information</h3>\r\n<p>We may use the information we collect about you to:</p>\r\n\r\n<ul style=\"list-style-type: disc;\">\r\n\r\n<li>Provide, maintain, and improve our Services, including, for example, to facilitate payments, send receipts, provide products and services you request (and send related information), develop new features, provide customer support to Users and Partners, develop safety features, authenticate users, and send product updates and administrative messages.</li>\r\n\r\n<li>Perform internal operations, including, for example, to prevent fraud and abuse of our Services; to troubleshoot software bugs and operational problems; to conduct data analysis, testing, and research; and to monitor and analyze usage and activity trends.</li>\r\n\r\n<li>Send or facilitate communications (i) between you and a Partner, such as estimated times of arrival (ETAs), or (ii) between you and a contact of yours at your direction in connection with your use of certain features, such as referrals, invites, split fare requests, or ETA sharing.</li>\r\n\r\n<li>Send you communications we think will be of interest to you, including information about products, services, promotions, news, and events of Najez and other companies, where permissible and according to local applicable laws; and to process contest, sweepstake, or other promotion entries and fulfil any related awards.</li>\r\n\r\n<li>Personalize and improve the Services, including to provide or recommend features, content, social connections, referrals, and advertisements.</li>\r\n\r\n</ul>\r\n\r\n<h3>Sharing of Information</h3>\r\n<p>We may share the information we collect about you as described in this Statement or as described at the time of collection or sharing, including as follows:</p>\r\n<p>Through Our Services</p>\r\n<p>We may share your information:</p>\r\n\r\n<ul style=\"list-style-type: disc;\">\r\n\r\n<li>With Partners to enable them to provide the Services you request. For example, we share your name, photo (if you provide one), average User rating given by Partners, and pickup and/or drop-off locations with Partners.</li>\r\n\r\n<li>With the general public if you submit content in a public forum, such as blog comments, social media posts, or other features of our Services that are viewable by the general public.</li>\r\n\r\n</ul>\r\n\r\n<h3>Other Important Sharing</h3>\r\n<p>We may share your information:</p>\r\n\r\n<ul style=\"list-style-type: disc;\">\r\n<li>With Najez subsidiaries and affiliated entities that provide services or conduct data processing on our behalf, or for data centralization and / or logistics purposes.</li>\r\n\r\n<li>In response to a request for information by a competent authority if we believe disclosure is in accordance with, or is otherwise required by, any applicable law, regulation, or legal process.</li>\r\n\r\n<li>With law enforcement officials, government authorities, or other third parties if we believe your actions are inconsistent with our User agreements, Terms of Service, or policies, or to protect the rights, property, or safety of Najez or others.</li>\r\n\r\n<li>If we otherwise notify you and you consent to the sharing; then we\'ll ask you for permission before we share your private data to third-party.</li>\r\n\r\n</ul>\r\n\r\n\r\n<h3>Social Sharing Features</h3>\r\n<p>The Services may integrate with social sharing features and other related tools which let you share actions you take on our Services with other apps, sites, or media, and vice versa. Your use of such features enables the sharing of information with your friends or the public, depending on the settings you establish with the social sharing service. Please refer to the privacy policies of those social sharing services for more information about how they handle the data you provide to or share through them.</p>\r\n\r\n<h3>Your Choices</h3>\r\n<h4>Account Information</h4>\r\n<p>You may correct your account information at any time by logging into your online or in-app account. If you wish to delete your account, please log into your Najez account at najez-online.com or in the app, and the follow the directions or \"Delete my Najez Account\" under the \"Account and Payment\" option. Please note that in some cases we may retain certain information about you as required by law, or for legitimate business purposes to the extent permitted by law. For instance, if you have a standing credit or debt on your account, or if we believe you have committed fraud or violated our Terms, we may seek to resolve the issue before deleting your information.</p>\r\n\r\n<h4>Access Rights</h4>\r\n\r\n<p>Najez will comply with individual\'s requests regarding access, correction, and/or deletion of the personal data it stores in accordance with applicable law.</p>\r\n\r\n<h4>Promotional Communications</h4>\r\n\r\n<p>You may opt out of receiving promotional messages from us by following the instructions in those messages. If you opt out, we may still send you non-promotional communications, such as those about your account, about Services you have requested, or our ongoing business relations.</p>\r\n\r\n<h3>Changes to the Statement</h3>\r\n\r\n<p>We may change this Statement from time to time. If we make significant changes in the way we treat your personal information, or to the Statement, we will provide you notice through the Services or by some other means, such as email. Your continued use of the Services after such notice constitutes your consent to the changes. We encourage you to periodically review the Statement for the latest information on our privacy practices.</p>', '<style>\r\n<!--\r\n /* Font Definitions */\r\n @font-face\r\n	{font-family:Wingdings;\r\n	panose-1:5 0 0 0 0 0 0 0 0 0;\r\n	mso-font-charset:2;\r\n	mso-generic-font-family:auto;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:0 268435456 0 0 -2147483648 0;}\r\n@font-face\r\n	{font-family:\"Cambria Math\";\r\n	panose-1:2 4 5 3 5 4 6 3 2 4;\r\n	mso-font-charset:1;\r\n	mso-generic-font-family:roman;\r\n	mso-font-format:other;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:0 0 0 0 0 0;}\r\n@font-face\r\n	{font-family:Calibri;\r\n	panose-1:2 15 5 2 2 2 4 3 2 4;\r\n	mso-font-charset:0;\r\n	mso-generic-font-family:swiss;\r\n	mso-font-pitch:variable;\r\n	mso-font-signature:-520092929 1073786111 9 0 415 0;}\r\n /* Style Definitions */\r\n p.MsoNormal, li.MsoNormal, div.MsoNormal\r\n	{mso-style-unhide:no;\r\n	mso-style-qformat:yes;\r\n	mso-style-parent:\"\";\r\n	margin-top:0in;\r\n	margin-right:0in;\r\n	margin-bottom:10.0pt;\r\n	margin-left:0in;\r\n	text-align:right;\r\n	line-height:115%;\r\n	mso-pagination:widow-orphan;\r\n	direction:rtl;\r\n	unicode-bidi:embed;\r\n	font-size:11.0pt;\r\n	font-family:\"Calibri\",\"sans-serif\";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-fareast-font-family:Calibri;\r\n	mso-fareast-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:Arial;\r\n	mso-bidi-theme-font:minor-bidi;}\r\np\r\n	{mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-margin-top-alt:auto;\r\n	margin-right:0in;\r\n	mso-margin-bottom-alt:auto;\r\n	margin-left:0in;\r\n	mso-pagination:widow-orphan;\r\n	font-size:12.0pt;\r\n	font-family:\"Times New Roman\",\"serif\";\r\n	mso-fareast-font-family:\"Times New Roman\";}\r\n.MsoChpDefault\r\n	{mso-style-type:export-only;\r\n	mso-default-props:yes;\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-fareast-font-family:Calibri;\r\n	mso-fareast-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:Arial;\r\n	mso-bidi-theme-font:minor-bidi;}\r\n.MsoPapDefault\r\n	{mso-style-type:export-only;\r\n	margin-bottom:10.0pt;\r\n	line-height:115%;}\r\n@page Section1\r\n	{size:595.3pt 841.9pt;\r\n	margin:1.0in 1.25in 1.0in 1.25in;\r\n	mso-header-margin:35.4pt;\r\n	mso-footer-margin:35.4pt;\r\n	mso-paper-source:0;\r\n	mso-gutter-direction:rtl;}\r\ndiv.Section1\r\n	{page:Section1;}\r\n /* List Definitions */\r\n @list l0\r\n	{mso-list-id:31461854;\r\n	mso-list-template-ids:1294640710;}\r\n@list l0:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:.5in;\r\n	mso-level-number-position:left;\r\n	text-indent:-.25in;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\n@list l1\r\n	{mso-list-id:118035107;\r\n	mso-list-template-ids:1829955978;}\r\n@list l1:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:.5in;\r\n	mso-level-number-position:left;\r\n	text-indent:-.25in;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\n@list l2\r\n	{mso-list-id:129639463;\r\n	mso-list-template-ids:1553749798;}\r\n@list l2:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:.5in;\r\n	mso-level-number-position:left;\r\n	text-indent:-.25in;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\n@list l3\r\n	{mso-list-id:588733525;\r\n	mso-list-template-ids:192427260;}\r\n@list l3:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:.5in;\r\n	mso-level-number-position:left;\r\n	text-indent:-.25in;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\n@list l4\r\n	{mso-list-id:1841772049;\r\n	mso-list-template-ids:-560460872;}\r\n@list l4:level1\r\n	{mso-level-number-format:bullet;\r\n	mso-level-text:;\r\n	mso-level-tab-stop:.5in;\r\n	mso-level-number-position:left;\r\n	text-indent:-.25in;\r\n	mso-ansi-font-size:10.0pt;\r\n	font-family:Symbol;}\r\nol\r\n	{margin-bottom:0in;}\r\nul\r\n	{margin-bottom:0in;}\r\n-->\r\n</style>\r\n<!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:\"Table Normal\";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-qformat:yes;\r\n	mso-style-parent:\"\";\r\n	mso-padding-alt:0in 5.4pt 0in 5.4pt;\r\n	mso-para-margin-top:0in;\r\n	mso-para-margin-right:0in;\r\n	mso-para-margin-bottom:10.0pt;\r\n	mso-para-margin-left:0in;\r\n	line-height:115%;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	font-family:\"Calibri\",\"sans-serif\";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:Arial;\r\n	mso-bidi-theme-font:minor-bidi;}\r\n</style>\r\n<![endif]--><!--[if gte mso 9]><xml>\r\n <o:shapedefaults v:ext=\"edit\" spidmax=\"3074\"/>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <o:shapelayout v:ext=\"edit\">\r\n  <o:idmap v:ext=\"edit\" data=\"1\"/>\r\n </o:shapelayout></xml><![endif]-->\r\n</head>\r\n\r\n<body lang=EN-US style=\'tab-interval:.5in\'>\r\n\r\n<div class=Section1 dir=RTL>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:20.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>بيان خصوصية المستخدم: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>نحن في ناجز نراعي المعلومات الشخصية\r\nالتي نستلمها منكم، فنحن نعمل على حماية معلوماتكم الشخصية الخاصة من الجميع. </span><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>عندما تُثبت تطبيق ناجز على هاتفك\r\nوتبدأ باستعماله، يعمل التطبيق على جمع المعلومات من خلال عدة طرق تضم كيفية\r\nتعاملك مع التطبيق وطرق التواصل التي تستخدمها. </span><span dir=LTR\r\nstyle=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\r\n\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>على من يُطبق هذا البيان: </span><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>هذا البيان يطبق على الأشخاص الذين\r\nيستخدمون موقعنا والتطبيق، الأشخاص، والخدمات التي بحاجة إلى توصيل، وغيرها من\r\nالخدمات المطلوبة (يُشار فيما بعد بـ&quot;المستخدمون&quot;). لا ينطبق هذا البيان\r\nمع المعلومات التي نستجمعها أو تعطى لنا من قبل الشركاء، و رجال التوصيل، شركات\r\nالمواصلات المشاركة، أو أي شخص يستخدم خدمات ناجز تحت رخصتها (اجماعًا: &quot;الشركاء&quot;).\r\nإذا تفاعلت مع الخدمة كمستخدم وشريك، البيان سيُطبق عليك حسب ما هو مذكور لكل من\r\nالمستخدم و شريك. </span><span dir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:16.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>المعلومات التي نجمعها منك: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>نستلم جميع المعلومات مباشرةً منك\r\nعندما تبدأ بفتح حسابك معنا أو إذا قمت بتعديل المعلومات بداخل الحساب أو عندما\r\nتطلب خدمات خاصة، أو تتواصل مع خدمة العملاء لدينا أو بالعموم إذا تواصلت معنا\r\nبشكلٍ ما أو آخر. المعلومات التي نستلمها منك تضم: الاسم، البريد الالكتروني،\r\nأرقام الهاتف، عناوين، صور الحساب، طرق الدفع أو أي مصدر آخر قد تستخدمه. </span><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>المعلومات التي نستلمها نحن من خلال\r\nاستخدامك لخدماتنا: </span><span dir=LTR style=\'font-size:12.0pt;font-family:\r\n\"Times New Roman\",\"serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>عندما تستخدم خدمتنا، نجمع منك\r\nالمعلومات الآتية: </span><span dir=LTR style=\'font-size:12.0pt;font-family:\r\n\"Times New Roman\",\"serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<ul style=\'margin-top:0in\' type=disc>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;\r\n     vertical-align:baseline\'><b><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>معلومات عن\r\n     موقعك</span></b><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\n     Calibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:\r\n     Calibri;mso-bidi-font-family:Calibri\'>: عندما تستخدم خدماتنا نجمع منك\r\n     معلومات دقيقة عن موقعك ورحلتك من تطبيق ناجز الذي تستخدمه أنت أو الشريك.\r\n     عند سماحك لتطبيق ناجز بدخول موقعك من خلال هاتفك، سيحدد موقعك في حال تشغيل التطبيق\r\n     سواء بالخلفية أو خلافه. وعلاوة على ذلك نحدد موقعك من خلال عنوان بروتوكول\r\n     الانترنت (</span><span dir=LTR style=\'font-size:12.0pt;mso-ascii-font-family:\r\n     Calibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:\r\n     Calibri;mso-bidi-font-family:Calibri\'>IP</span><span dir=RTL></span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;\r\n     mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\n     mso-bidi-font-family:Calibri\'><span dir=RTL></span>). </span><span\r\n     dir=LTR style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;\r\n     vertical-align:baseline\'><b><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>معلومات\r\n     التواصل</span></b><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\n     Calibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:\r\n     Calibri;mso-bidi-font-family:Calibri\'>: عند سماحك لتطبيق ناجز بدخول لائحة\r\n     الأسماء في هاتفك، سنستخدم هذه المعلومات لتحليل تفاعلاتك الاجتماعية من خلال\r\n     تطبيقنا كما هي مذكورة في خدماتنا وكما هو مسموح لنا من قِبلك. </span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;\r\n     vertical-align:baseline\'><b><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>معلومات\r\n     التعاملات</span></b><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\n     Calibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:\r\n     Calibri;mso-bidi-font-family:Calibri\'>: نجمع منك التعاملات التي تبدأها\r\n     خلال استخدامك لخدماتنا، كالمعلومات المستخدمة، الوقت، والمبلغ الذي تم دفعه،\r\n     المسافة التي تم قطعها في الرحلة، وغيرها من التفاصيل المتعلقة بالتعاملات\r\n     عند استخدام خدماتنا. </span><span lang=AR-SA style=\'font-size:12.0pt;\r\n     font-family:\"Arial\",\"sans-serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;\r\n     vertical-align:baseline\'><b><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>معلومات\r\n     الاستخدام وماذا تُفضّل</span></b><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>: نجمع معلومات\r\n     عن تفاعلك مع التطبيق والموقع، وماذا تفضّل، والاختيارات التي اخترتها في\r\n     التطبيق والموقع. </span><span lang=AR-SA style=\'font-size:12.0pt;\r\n     font-family:\"Arial\",\"sans-serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;\r\n     vertical-align:baseline\'><b><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>معلومات عن\r\n     الجهاز</span></b><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\n     Calibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:\r\n     Calibri;mso-bidi-font-family:Calibri\'>: نجمع معلومات عن جهازك المحمول،\r\n     وتحتوي هذه المعلومات نوع الجهاز الذي تستخدمه، نظام التشغيل ونوع الإصدار،\r\n     أسماء الملفات، معرف الجهاز الفريد، اللغة المفضلة، معرفات الإعلانات،\r\n     الأرقام المسلسلة ومعلومات عن الشبكة المستخدمة في هاتفك المحمول. </span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;\r\n     vertical-align:baseline\'><b><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>معلومات\r\n     المكالمات والرسائل النصية</span></b><span lang=AR-SA style=\'font-size:\r\n     12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>: خدماتنا\r\n     تعتمد على التواصل المستمر بين المستخدمين و الشركاء لذلك نجمع معلومات عن\r\n     المكالمات التي تقام بهدف استخدام خدمة ناجز، وتحتوي هذه المعلومات وقت اجراء\r\n     المكالمة والرسالة النصية، وأرقام الهواتف من كلا الطرفين ومحتوى الرسالة\r\n     النصية. </span><span lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l1 level1 lfo1;tab-stops:list .5in;\r\n     vertical-align:baseline\'><b><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>سجل المعلومات</span></b><span\r\n     lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;\r\n     mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\n     mso-bidi-font-family:Calibri\'>: عندما تتعامل مع خدماتنا نجمع معلومات من\r\n     خلال خوادمنا والتي تحتوي على معلومات مثل عنوان بروتوكول الانترنت (</span><span\r\n     dir=LTR style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\r\n     \"Times New Roman\";mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>IP</span><span\r\n     dir=RTL></span><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\n     Calibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:\r\n     Calibri;mso-bidi-font-family:Calibri\'><span dir=RTL></span>) الخاص بك،\r\n     أوقات استخدام التطبيق، الصفحات والمميزات التي تم استخدامها في التطبيق،\r\n     والأخطاء التقنية التي تحدث في استخدام التطبيق أو الموقع، ونوع المتصفح الذي\r\n     تم استخدامه لتصفح الموقع، وأية تطبيقات خارجية تمت استخدامها أو خدمات أخرى\r\n     مماثلة استخدمتها قبل التفاعل مع ناجز. </span><span lang=AR-SA\r\n     style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";mso-fareast-font-family:\r\n     \"Times New Roman\"\'><o:p></o:p></span></li>\r\n</ul>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:12.0pt;text-align:left;\r\nline-height:normal;direction:ltr;unicode-bidi:embed\'><span lang=AR-SA dir=RTL\r\nstyle=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\r\n\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:16.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>المعلومات التي نجمعها من مصادر أخرى: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>يمكننا كذلك أن نستلم معلومات من مصادر\r\nأخرى غير التي ذكرناها في هذا البيان، فعلى سبيل المثال: </span><span dir=LTR\r\nstyle=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\r\n\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<ul style=\'margin-top:0in\' type=disc>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>إذا اخترت أن\r\n     تُفعّل وتنشأ حسابك معنا في ناجز عن طريق خدمات مزودي الدفع (مثل: محفظة قوقل\r\n     &quot;</span><span dir=LTR style=\'font-size:12.0pt;mso-ascii-font-family:\r\n     Calibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:\r\n     Calibri;mso-bidi-font-family:Calibri\'>Google Wallet</span><span dir=RTL></span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;\r\n     mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\n     mso-bidi-font-family:Calibri\'><span dir=RTL></span>&quot;)، أو عن طريق\r\n     الشبكات التواصل الاجتماعية (مثل فيس بوك وغيرها)، أو أنك تفاعلت مع تطبيق أو\r\n     موقع آخر يستخدم إحدى واجهات برمجة التطبيقات (</span><span dir=LTR\r\n     style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\r\n     \"Times New Roman\";mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>API</span><span\r\n     dir=RTL></span><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\n     Calibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:\r\n     Calibri;mso-bidi-font-family:Calibri\'><span dir=RTL></span>) الخاصة بنا\r\n     (أو نستخدم واجهاتهم الخاصة بهم) سنستلم من خلالها المعلومات الخاصة بك،\r\n     والعلاقات التي لديك مع هذه المواقع الأخرى </span><span dir=LTR\r\n     style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";mso-fareast-font-family:\r\n     \"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>عندما تطلب\r\n     خدماتنا عند الطلب، سيوفر للشريك تقييم المستخدم بعد انتهاء الخدمة. </span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l0 level1 lfo2;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>إذا كنت تستخدم\r\n     خدماتنا بشكل آخر، كشريك على سبيل المثال، أو تستخدم تطبيق آخر من طرفنا، فإن\r\n     المعلومات التي تتوفر لدينا عنك من خلال هذه التطبيقات سترتبط بالمعلومات\r\n     التي لدينا عنك كمستخدم أو راكب. </span><span lang=AR-SA style=\'font-size:\r\n     12.0pt;font-family:\"Arial\",\"sans-serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n</ul>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:16.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>استخدامنا للمعلومات: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>قد نستخدم هذه المعلومات التي نستلمها\r\nكما ذكرنا مسبقًا كالتالي: </span><span dir=LTR style=\'font-size:12.0pt;\r\nfont-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<ul style=\'margin-top:0in\' type=disc>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l3 level1 lfo3;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>لصيانة وتحسين\r\n     الخدمات التي نقدمها على سبيل المثال، تسهيل خدمات الدفع، وإرسال إيصال\r\n     الدفع، وتوفير خدمات التي يتم طلبها من قبلكم (والمعلومات المتعلقة\r\n     بالخدمات). وقد نستخدم هذه المعلومات لبناء خدمات ومميزات جديدة، وتوفير خدمة\r\n     عملاء للمستخدمين والشركاء، وتطوير الجوانب الأمنية المتعلقة بخدماتنا.\r\n     ويمكننا كذلك أن نوفر تحديثات ورسائل إدارية وتوثيق المستخدمين. </span><span\r\n     dir=LTR style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l3 level1 lfo3;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>تستخدم هذه\r\n     المعلومات في العمليات الداخلية الخاصة بالشركة، على سبيل المثال لنجتنب\r\n     حوادث الاحتيال وسوء الاستخدام لخدماتنا. ونستخدم هذه المعلومات كذلك في\r\n     استكشاف الأخطاء واصلاحها، وأخطاء أخرى في العمليات. فلهذا تستخدم هذه\r\n     المعلومات في أغراض التحليل، والبحث والتحري عن الاستخدام واتجاهات النشاط. </span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l3 level1 lfo3;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>لإرسال وتسهيل\r\n     التواصل بينك وبين ١) الشريك، وتحتوي على معلومات مثل وقت الوصول إلى وجهتك؛\r\n     وبين ٢) أحد جهات التواصل الخاص بك المتجه تجاهك باستخدامك للميزات مثل\r\n     الإحالات وطلبات تقسيم الأجرة، ونظام الدعوات، أو نظام مشاركة الوقت إلى\r\n     الوجهة. </span><span lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l3 level1 lfo3;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>نستخدم هذه\r\n     المعلومات لإرسال معلومات التواصل معك التي قد تثير اهتمامك مثل معلومات عن\r\n     منتجاتنا وخدماتنا الجديدة، وكذلك عروضنا الجديدة، والأخبار والأحداث التي\r\n     تتعلق بناجز والشركات الأخرى حيثما هو مسموح حسب القوانين المحلية. ويمكننا\r\n     أيضًا تزويدكم بمعلومات عن الجوائز والعروض القائمة عن شركتنا. </span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l3 level1 lfo3;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>نستخدم\r\n     المعلومات أيضًا على تكوين محتوى وتفعيل الخدمة بشكل شخصي لتناسب احتياجك\r\n     الخاصة، حيث من خلال المعلومات التي تتوفر لدينا نستطيع أن نقدم لكم اقتراحات\r\n     ودعايات بناءً على احتياجاتكم، وخلق نقاط اجتماعية لتتفاعلوا معها. </span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n</ul>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:16.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>مشاركة المعلومات:</span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>قد نشارك بمعلوماتك التي نجمعها عنك\r\nكما ذكرنا مسبقًا في البيان كما تصل لدينا من قبلكم أو من المصادر الأخرى، ونشارك\r\nهذه المعلومات كالتالي: </span><span dir=LTR style=\'font-size:12.0pt;font-family:\r\n\"Times New Roman\",\"serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>عن طريق خدماتنا</span><span dir=LTR\r\nstyle=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\r\n\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>سنشارك معلوماتك لـ: </span><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<ul style=\'margin-top:0in\' type=disc>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l2 level1 lfo4;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>الشركاء لدينا\r\n     حتى يستطيعوا أن يقدموا لكم الخدمة التي طلبتموها، فعلى سبيل المثال، سنشارك\r\n     باسمك وصورتك (إذا توفر)، وتقييمك كمستخدم من قبل سائقون آخرون، ونقاط الوصول\r\n     والمغادرة. </span><span dir=LTR style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-bottom:\r\n     .0001pt;line-height:normal;mso-list:l2 level1 lfo4;tab-stops:list .5in;\r\n     vertical-align:baseline\'><span lang=AR-SA style=\'font-size:12.0pt;\r\n     mso-ascii-font-family:Calibri;mso-fareast-font-family:\"Times New Roman\";\r\n     mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>مع عموم\r\n     مستخدمين الشبكة إذا قدّمت محتوى في منتدى عام مثل التعليقات في مدونة، محتويات\r\n     شبكات التواصل الاجتماعية، أو أية خدمات أخرى التي نقدمها لعامة الناس. </span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n</ul>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:16.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>تعليمات إضافية عن مشاركة المعلومات: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-top:0in;margin-right:.5in;margin-bottom:\r\n0in;margin-left:0in;margin-bottom:.0001pt;line-height:normal\'><span lang=AR-SA\r\nstyle=\'font-size:12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\r\n\"Times New Roman\";mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri;\r\ncolor:black\'>قد نشارك معلوماتك مع الأطراف التالية: </span><span dir=LTR\r\nstyle=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\r\n\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<ul style=\'margin-top:0in\' type=disc>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-left:\r\n     .5in;margin-bottom:.0001pt;line-height:normal;mso-list:l4 level1 lfo5;\r\n     tab-stops:list .5in;vertical-align:baseline\'><span lang=AR-SA\r\n     style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\r\n     \"Times New Roman\";mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>شركات\r\n     ناجز الفرعية والشركات المشاركة في مشروع ناجز التي تقدم الخدمات أو تساعدنا\r\n     في تحليل البيانات، أو لتمركز البيانات أو استخدامها لأغراض لوجستية. </span><span\r\n     dir=LTR style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-left:\r\n     .5in;margin-bottom:.0001pt;line-height:normal;mso-list:l4 level1 lfo5;\r\n     tab-stops:list .5in;vertical-align:baseline\'><span lang=AR-SA\r\n     style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\r\n     \"Times New Roman\";mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>ردًا\r\n     على طلبات الجهات المعنية إذا وجدنا أن مشاركة هذه المعلومات لا تخالف\r\n     القوانين والاجراءات المحلية. </span><span lang=AR-SA style=\'font-size:\r\n     12.0pt;font-family:\"Arial\",\"sans-serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-left:\r\n     .5in;margin-bottom:.0001pt;line-height:normal;mso-list:l4 level1 lfo5;\r\n     tab-stops:list .5in;vertical-align:baseline\'><span lang=AR-SA\r\n     style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\r\n     \"Times New Roman\";mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>سنشارك\r\n     المعلومات مع الجهات الحكومية، أو أي جهة أخرى إذا وجدنا أن أفعالك تنافي\r\n     اتفاقية المستخدم، شروط الاستخدام، أو سياساتنا لدى ناجز. وسنشارك هذه\r\n     المعلومات إن احتجنا أن نحمي حقوقنا الأمنية و ممتلكات ناجز. </span><span\r\n     lang=AR-SA style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";\r\n     mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-left:\r\n     .5in;margin-bottom:.0001pt;line-height:normal;mso-list:l4 level1 lfo5;\r\n     tab-stops:list .5in;vertical-align:baseline\'><span lang=AR-SA\r\n     style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\r\n     \"Times New Roman\";mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>سنشارك\r\n     هذه المعلومات إن كانت ناجز متعلقة في تفاوضات الدمج أو بيع الشركة من قبل\r\n     شركة أخرى.</span><span lang=AR-SA style=\'font-size:12.0pt;font-family:\r\n     \"Arial\",\"sans-serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></li>\r\n <li class=MsoNormal dir=RTL style=\'color:black;margin-bottom:0in;margin-left:\r\n     .5in;margin-bottom:.0001pt;line-height:normal;mso-list:l4 level1 lfo5;\r\n     tab-stops:list .5in;vertical-align:baseline\'><span lang=AR-SA\r\n     style=\'font-size:12.0pt;mso-ascii-font-family:Calibri;mso-fareast-font-family:\r\n     \"Times New Roman\";mso-hansi-font-family:Calibri;mso-bidi-font-family:Calibri\'>على\r\n     خلاف ذلك، سنعلمك عن مشاركة المعلومات الخاصة بك، وسنطلب منك إذنًا قبل أن\r\n     نشارك بياناتك الخاصة لجهات الغير مذكورة أعلاه. </span><span lang=AR-SA\r\n     style=\'font-size:12.0pt;font-family:\"Arial\",\"sans-serif\";mso-fareast-font-family:\r\n     \"Times New Roman\"\'><o:p></o:p></span></li>\r\n</ul>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:16.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>مميزات المشاركة الاجتماعية:</span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>عليك أن تعلم أن خدماتنا تحتوي على\r\nميزات المشاركة الاجتماعية وغيرها من الأدوات المتعلقة بذلك، مما يسمح لك أن تشارك\r\nما تفعل مع خدماتنا في مواقع أخرى كشبكات التواصل الاجتماعي والعكس صحيح. استخدامك\r\nلهذه الميزات تسمح لك بأن تشارك معلوماتك مع أصدقائك والشبكة، وذلك تباعًا حسب ما\r\nفعّلته من الخاصيات في حسابك. يرجى مراجعة اتفاقية الخصوصية أعلاه المتعلقة بشبكات\r\nالتواصل الاجتماعي لمزيد من المعلومات عن المعلومات التي يتم مشاركتها والتي\r\nتوفرها لنا. </span><span dir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:16.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>خياراتك:</span></b><span dir=LTR\r\nstyle=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\r\n\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>معلومات الحساب: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>يمكنك أن تصحح وتحرر المعلومات الخاصة\r\nفي أي وقت، ما عليك إلا أن تدخل في حسابك إما على الموقع أو التطبيق. إذا أردت أن\r\nتحذف حسابك فيتوجب عليك أن تدخل في حسابك عبر موقعنا أو عبر التطبيق واتبع\r\nالتعليمات، أو يمكنك أن تنقر على &quot;احذف حسابي لدى ناجز&quot; في خانة\r\n&quot;الحسابات والدفع.&quot; يرجى الملاحظة أنه في بعض الحالات سنبقي بعض\r\nالمعلومات كما يسمح به القانون، وذلك لأسباب تجارية وقانونية. فعلى سبيل المثال إن\r\nوجدنا بأنك قد خرقت شروطنا أو وجدناك في قضية احتيال، أو هناك رصيد ما زال لم يتم\r\nدفعه أو وجب دفعه سنسعى أن نحل المشكلة قبل أن يُحذف حسابك. </span><span dir=LTR\r\nstyle=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\r\n\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>حقوق التمكن من الدخول: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>تسعى ناجز بأن تلبي طلبات المستخدمين\r\nالتي تتعلق بالتمكن من الدخول لموقعنا وتطبيقنا، أو تصحيح و/أو حذف معلوماتك\r\nالشخصية المخزونة حسب القوانين المحلية. </span><span dir=LTR style=\'font-size:\r\n12.0pt;font-family:\"Times New Roman\",\"serif\";mso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n \r\n\r\n \r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:14.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>الرسائل الدعائية: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>يمكنكم أن تلغوا اشتراككم من الرسائل\r\nالدعائية عن طريق اتباع التعليمات المذكورة في أسفل الرسالة المرسلة إليكم في\r\nالبريد الإلكتروني. إذا ألغيتم اشتراككم من الرسائل الدعائية، ستصلكم رسائل غير\r\nدعائية والتي تحتوي معلومات تهمك عن حسابك، والخدمات التي تطلبها والعلاقات\r\nالتجارية. </span><span dir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><b><span lang=AR-SA style=\'font-size:16.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>التغييرات في البيان: </span></b><span\r\ndir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=LTR style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\ntext-align:left;line-height:normal;direction:ltr;unicode-bidi:embed\'><span\r\nlang=AR-SA dir=RTL style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL style=\'margin-bottom:0in;margin-bottom:.0001pt;\r\nline-height:normal\'><span lang=AR-SA style=\'font-size:12.0pt;mso-ascii-font-family:\r\nCalibri;mso-fareast-font-family:\"Times New Roman\";mso-hansi-font-family:Calibri;\r\nmso-bidi-font-family:Calibri;color:black\'>قد تطرأ تغييرات في هذا البيان من وقت\r\nلوقتٍ آخر، ولكن إن كانت هناك تغييرات كبيرة في تعاملنا مع معلوماتك الشخصية أو في\r\nالبيان، سنرسل لكم التحديثات عبر خدماتنا، أو عبر بريدكم الالكتروني المسجل لدينا،\r\nاستخدامكم المستمر لخدمتنا نعتبره موافقة على التحديثات في هذه الاتفاقية. نشجعكم\r\nأن تتحققوا هذه الصفحة بشكل مستمر لتتعرفوا على أحدث التغييرات التي تطرأ على\r\nالبيان. </span><span dir=LTR style=\'font-size:12.0pt;font-family:\"Times New Roman\",\"serif\";\r\nmso-fareast-font-family:\"Times New Roman\"\'><o:p></o:p></span></p>\r\n\r\n<p class=MsoNormal dir=RTL><span lang=AR-SA style=\'font-family:\"Arial\",\"sans-serif\";\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:Arial;mso-bidi-theme-font:\r\nminor-bidi\'><o:p>&nbsp;</o:p></span></p>\r\n\r\n</div>\r\n', '1', '2017-07-06 08:00:34');
INSERT INTO `pages` (`id`, `title`, `title_ar`, `slug`, `description_en`, `description_ar`, `status`, `created`) VALUES
(2, 'About Us', '', 'about-us', '<p><strong>About Us</strong><br />A 100% Saudi owned brand coming across stunningly. Najez is an amazing venture committed towards providing the best services across the Kingdom of Saudi Arabia. Our vision is to become a business leader dedicated towards delivering excellent quality services with evolving growth and success.</p>\r\n<p>A pioneering company exceptionally creating and implementing reliable and high-quality services to meet the customers and partners needs successfully. Najez is determined to never let their partners &amp;amp; customers down with their uniqueness.</p>\r\n<p>In all aspects, Najez is there for you 24x7. Najez is all set to deliver unbeatable innovative quality services with everlasting commitment driven through our professional class management practices</p>', '<p style=\"text-align: right;\"><strong>من نحن؟</strong></p>\r\n<p style=\"text-align: right;\"><br /> شركة مرخصة سعودية ومملوكة 100٪ من مواطنين سعوديين، ناجز تسعى لأن نقدم لكم من خلال شركتنا أفضل الخدمات في المملكة العربية السعودية. رؤيتنا هو أن نصبح علامة تجارية رائدة في أعمالها وملتزمة بتقديم خدمات ممتازة وذات جودة عالية وننمو ونتطور بشكل ناجح خلال السنين القادمة.</p>\r\n<p style=\"text-align: right;\">نفتخر بكوننا شركة استثنائية، ونكرّس جهودنا بأن نقدم لكم خدمات سيارة الأجرة التي يمكنكم كعملاء وشركاء الاعتماد عليها. ونعمل نحن في ناجز بأن لا نخيب ظن عملائنا وشركائنا من خلال إبراز ما يميزنا عن باقي المنافسين.</p>\r\n<p style=\"text-align: right;\">من كل النواحي، ناجز تعمل من أجلك على مدار الوقت، 24 ساعة في اليوم، و7 أيام في الأسبوع لتقديم خدمات مبتكرة وعالية الجودة مع ضمان استمرارية الجودة من خلال إدارتنا المهنية المحترفة. </p>', '1', '2017-07-07 07:37:02'),
(3, 'Driver FAQ', '', 'driver-faq', '<p><strong>Driver&rsquo;s FAQs:</strong></p>\r\n<p><br /><strong>How do I become a driver/delivery driver for Najez?</strong><br />We worked hard to simplify the process. Simply, fill the forms for either<br />driver or delivery driver and upload the documents requested. Once we<br />receive your application, it will be under our review.</p>\r\n<p><br /><strong>What is Tafweeth, and why do I need it?</strong><br />Tafweeth is an authorization letter that allows the owner of the vehicle<br />to allow a driver to drive a vehicle and make profit from it.</p>\r\n<p><br /><strong>How do I accept rides, or delivery orders?</strong><br />Our app is simple to use, and it is very intuitive to navigate through.<br />You can simply proceed accept a ride or a delivery request, and proceed<br />with the task until it is done.</p>\r\n<p><br /><strong>What are my working hours?</strong><br />Your working hours are flexible. Whenever you are online in the app,<br />and accepting tasks they will be your working hours, until you decide to go offline.</p>\r\n<p><br /><strong>Who do I contact if I face issues with the app?</strong><br />You can directly talk to us on this number [###] to resolve the issue as<br />quick as we can once we understand what&rsquo;s the problem.</p>\r\n<p><br /><strong>How much will I make?</strong><br />Your earnings with Najez will depend on how many hours you work. It<br />may vary from person to person, depending on the hours, and whether<br />you are delivery driver or a regular driver. Nonetheless, we always try<br />to offer you the best earnings and rates the market has to offer.</p>\r\n<p><strong>We wish you a pleasant experience with Najez.</strong></p>', '<p style=\"text-align: right;\"><strong>أسئلة متكررة للكابتن</strong></p>\r\n<p style=\"text-align: right;\"><br /><strong>کیف أصبح سائق /سائق التوصیل لناجز؟</strong><br /><span style=\"font-weight: 400;\">لقد عملنا بجدّ لتبسيط عملية التسجيل. بكل بساطة ما عليك إلا ملئ الاستمارات ورفع المستندات المطلوبة، منھما من السائق أو سائق التوصیل وتحميل المستندات التي طلبناها منك. سيتم مراجعة طلبك من الإدارة قبل الموافقة عليها.</span></p>\r\n<p style=\"text-align: right;\"><br /><strong>ما ھو التفويض ولماذا احتاجه؟</strong><br /><span style=\"font-weight: 400;\">ان التفويض ھی رسالة الاذن من مالك المرکبة والتي تفوض لسائق آخر بصلاحية استخدام المركبة لأهداف ربحية.</span></p>\r\n<p style=\"text-align: right;\"><br /><strong>کیف یمكنني قبول الرحلات أو طلبات التوصيل؟</strong><br /><span style=\"font-weight: 400;\">إن تطبيقنا سھل للاستخدام وبديهي في التعامل، ويمكنك بكل بساطة قبول المهام التي تظهر لك في التطبيق وإنهاءها. </span></p>\r\n<p style=\"text-align: right;\"><strong>ما هي ساعات العمل الخاص بي؟</strong><br /><span style=\"font-weight: 400;\">ستكون ساعات عملك مرنة، عندما تكون متصلًا بالتطبيق، وتقبل المهمات الموجودة، ستكون هذه هي ساعات عملك إلى أن تحدد وتكون غير متصلًا. </span></p>\r\n<p style=\"text-align: right;\"><br /><strong>مع من اتصل اذا واجھت مشكلة تقنية في التطبيق؟</strong><br /><span style=\"font-weight: 400;\">یمكنك التحدث معنا مباشرة عبر هذا الرقم (###) لحل المشكلة بأسرع وقت ممكن.</span></p>\r\n<p style=\"text-align: right;\"><br /><strong> ما هي مكاسبي الشهرية؟ </strong></p>\r\n<p style=\"text-align: right;\"><span style=\"font-weight: 400;\">إن مكاسبك مع ناجز تعتمد على عدد الساعات التي تكون متصلا في التطبيق وتقبل المهمات. </span></p>\r\n<p style=\"text-align: right;\"><span style=\"font-weight: 400;\">قد تختلف الأرباح من شخص إلى آخر، على حسب ساعات العمل أو إذا كنت سائق، أو سائق توصيل. ولكن عليك أن تعلم دائمًا أننا نسعى أن نقدم أفضل نسبة أرباح عن منافسينا. </span></p>\r\n<p style=\"text-align: right;\"><strong>نتمنی لكم تجربة ممتعة مع ناجز</strong></p>', '1', '2017-08-14 01:08:43'),
(4, 'User Faq', '', 'user-faq', '<p>&nbsp;</p>\r\n<div class=\"demo\">\r\n<h1 style=\"margin-top: 0; margin-bottom: 25px;\" align=\"center\">Customer FAQs</h1>\r\n<h2 style=\"color: #ff9e15;\"><u>General:</u></h2>\r\n<div id=\"accordion\" class=\"panel-group\" role=\"tablist\" aria-multiselectable=\"true\">\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingOne\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a role=\"button\" href=\"#collapseOne\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"true\" aria-controls=\"collapseOne\"> What is Najez?</a></h4>\r\n</div>\r\n<div id=\"collapseOne\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingOne\">\r\n<div class=\"panel-body\">Najez is a 100% Saudi ride sharing, car booking service that offers reliable, easy to use services for the Saudi market, and will get things done for you. Our services are dedicated to you and your needs.</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingTwo\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseTwo\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseTwo\"> How Najez works?</a></h4>\r\n</div>\r\n<div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">\r\n<div class=\"panel-body\">Simply by downloading our app, you can easily register and start booking cars so can you accomplish your tasks for the day! <br /> If you are a driver, and interested to join, you can simply follow the instructions on our website, and once you are approved, you can start working for Najez!</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingThree\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseThree\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseThree\"> Why Najez?</a></h4>\r\n</div>\r\n<div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\">\r\n<div class=\"panel-body\">We believe that we can offer you better prices, faster service, and better value. Our drivers are reliable and are equipped to handle all of your situations.</div>\r\n</div>\r\n</div>\r\n<h2 style=\"color: #ff9e15;\"><u>Pricing:</u></h2>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingFour\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a role=\"button\" href=\"#collapseFour\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"true\" aria-controls=\"collapseFour\"> What are your prices? </a></h4>\r\n</div>\r\n<div id=\"collapseFour\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingFour\">\r\n<div class=\"panel-body\">We work hard to keep our prices as low as possible from our competitors to ensure that you can have the best experience and so you can rely on us. You can check the fare estimates in our app.</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingFive\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseFive\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseFive\"> Why do you charge more for airport pick-up?</a></h4>\r\n</div>\r\n<div id=\"collapseFive\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingFive\">\r\n<div class=\"panel-body\">Airport pick-up fees apply to all airport pick-up bookings. This charge is there to compensate the driver for parking the car, paying applicable airport taxes, and waiting for up to 30 minutes free of cost.</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingSix\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseSix\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseSix\"> When is my credit charged?</a></h4>\r\n</div>\r\n<div id=\"collapseSix\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingSix\">\r\n<div class=\"panel-body\">Your credit card is not charged until after your trip is finished. At the time of booking, we simply want to know which card you want to use for the trip.</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingSeven\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseSeven\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseSeven\"> Is it safe? </a></h4>\r\n</div>\r\n<div id=\"collapseSeven\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingSeven\">\r\n<div class=\"panel-body\">Yes, it definitely is. You can track the whole trip on the map within the app from your starting point till the end.</div>\r\n</div>\r\n</div>\r\n</div>\r\n<!-- panel-group --></div>\r\n<!-- container -->', '<p>&nbsp;</p>\r\n<div class=\"demo\">\r\n<h1 style=\"margin-top: 0; margin-bottom: 25px;\" align=\"center\">الأسئلة المتكررۃ</h1>\r\n<h2 style=\"color: #ff9e15;\"><u>العامة</u></h2>\r\n<div id=\"accordion\" class=\"panel-group\" role=\"tablist\" aria-multiselectable=\"true\">\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingOne\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a role=\"button\" href=\"#collapseOne\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"true\" aria-controls=\"collapseOne\"> ما هي ناجز؟</a></h4>\r\n</div>\r\n<div id=\"collapseOne\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingOne\">\r\n<div class=\"panel-body\">ناجز هي خدمة سعودیة100٪ تقدم لمستخدميها خدمة معتمدة وسهلة الاستخدام لتوجيه مركبات الأجرة ومشاركتها مع الآخرين، وتنجز مهامك من أجلك! خدماتنا هي من أجلك.</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingTwo\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseTwo\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseTwo\"> کیفناجزتعمل؟</a></h4>\r\n</div>\r\n<div id=\"collapseTwo\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingTwo\">\r\n<div class=\"panel-body\">یمكنك بعد تحمیل تطبیقنا ببساطة أن تسجل بسھولة و تبدأ بحجز المركبات التي ستنجز مهامك اليوم! <br /> إن كنت سائقًا و مهتمًا بالإنضمام إلی ناجز فیمكنك أن تتابع التعلیمات ببساطة علی موقعنا أو التطبيق، وعند حصولك لموافقة الإدارة يمكنك أن تكون شريكًا معنا في ناجز.</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingThree\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseThree\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseThree\"> لماذا ناجز؟</a></h4>\r\n</div>\r\n<div id=\"collapseThree\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingThree\">\r\n<div class=\"panel-body\">نحن في ناجز نعمل بجهد أن نقدم لكم أسعار مميزة، خدمة سريعة، وقيمة أفضل لخدماتنا. سائقين ناجز يعتمد عليهم في انجاز مهماتكم!</div>\r\n</div>\r\n</div>\r\n<h2 style=\"color: #ff9e15;\"><u>الأسعار</u></h2>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingFour\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a role=\"button\" href=\"#collapseFour\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"true\" aria-controls=\"collapseFour\"> ما هي أسعاركم؟</a></h4>\r\n</div>\r\n<div id=\"collapseFour\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingFour\">\r\n<div class=\"panel-body\">نحن نعمل بجدّ للحفاظ علی الأسعار منخفضا قدر الإمكانعن منافسینا لنضمنأنتكون تجربتك لدينا منأفضل التجارب ونوفر لك تعتمد عليها بشكل دائم. للمزيد من المعلومات سجل دخولك في التطبيق لتتحقق من الأسعار.</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingFive\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseFive\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseFive\"> لماذا أجرة المطار تكون أعلى عن التكلفة العادية؟</a></h4>\r\n</div>\r\n<div id=\"collapseFive\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingFive\">\r\n<div class=\"panel-body\">ستنطبق رسوم أجرة المطار عند استخدام خدماتنا. إن هذه الرسوم هي موجودة لتعويض السائق عن ركن السيارة، دفع الضرائب المتعلقة للمطار، والانتظار (والتي هي مجانية في أول 30 دقيقة).</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingSix\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseSix\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseSix\"> متى يتم الخصم من بطاقتي؟</a></h4>\r\n</div>\r\n<div id=\"collapseSix\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingSix\">\r\n<div class=\"panel-body\">لن نخصم من بطاقتك حتى تنتهي الرحلة. عند حجزك للرحلة، كل ما عليك هو اختيار البطاقة التي تريدنا أن نخصم منها مبلغ الرحلة.</div>\r\n</div>\r\n</div>\r\n<div class=\"panel panel-default\">\r\n<div id=\"headingSeven\" class=\"panel-heading\" role=\"tab\">\r\n<h4 class=\"panel-title\"><a class=\"collapsed\" role=\"button\" href=\"#collapseSeven\" data-toggle=\"collapse\" data-parent=\"#accordion\" aria-expanded=\"false\" aria-controls=\"collapseSeven\"> ھل ھی آمنة؟</a></h4>\r\n</div>\r\n<div id=\"collapseSeven\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"headingSeven\">\r\n<div class=\"panel-body\">نعم هيآمنة بلاشك. يمكنك تتبع مسار الرحلة على الخريطة الموجودة في التطبيق، من نقطة البداية إلى نقطة النهاية.<br /><strong>رافقتكم السلامة!</strong></div>\r\n</div>\r\n</div>\r\n</div>\r\n<!-- panel-group --></div>\r\n<!-- container -->', '1', '2017-09-07 07:55:15');

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions` (
  `id` int(11) NOT NULL,
  `promo_code` varchar(255) NOT NULL,
  `discount_amount` varchar(255) DEFAULT NULL,
  `discount_percentage` varchar(255) DEFAULT NULL,
  `voucher_description` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `user_no` int(11) DEFAULT '1',
  `use_number` int(12) DEFAULT '1',
  `validity_start` datetime DEFAULT NULL,
  `validity_end` datetime DEFAULT NULL,
  `use_flag` int(11) DEFAULT '0',
  `status` int(12) DEFAULT '1',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`id`, `promo_code`, `discount_amount`, `discount_percentage`, `voucher_description`, `city`, `user_no`, `use_number`, `validity_start`, `validity_end`, `use_flag`, `status`, `created`) VALUES
(1, 'PROMO100', NULL, '20', NULL, NULL, 5, 1, '2018-01-09 00:00:00', '2018-01-10 00:00:00', 0, 1, '2018-01-09 10:49:54'),
(2, 'PROMO200', NULL, '20', NULL, NULL, 2, 1, '2018-01-09 00:00:00', '2018-01-10 00:00:00', 0, 1, '2018-01-09 10:50:55');

-- --------------------------------------------------------

--
-- Table structure for table `promotion_order`
--

CREATE TABLE `promotion_order` (
  `id` int(12) NOT NULL,
  `user_type` int(12) DEFAULT NULL,
  `user_id` int(12) DEFAULT NULL,
  `promotion_id` int(12) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `push_notification_driver`
--

CREATE TABLE `push_notification_driver` (
  `id` int(11) NOT NULL,
  `device_token` longtext,
  `device` varchar(255) DEFAULT NULL,
  `player_id` longtext,
  `device_os` varchar(50) DEFAULT NULL,
  `device_model` varchar(50) DEFAULT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_notification_driver`
--

INSERT INTO `push_notification_driver` (`id`, `device_token`, `device`, `player_id`, `device_os`, `device_model`, `driver_id`, `created`, `modified`) VALUES
(4, 'eeaW1GSzTvA:APA91bGBfmYWMBtOlI5ZylVggo_bfXtZD98VeF5SNE5SpaJQLtcMf_Xzyxy1lqK9nmqVlS5uTSB07tXyUC9Ov2X99_jHpw-I7eZRtUehdScR9VRfF0NQxNHXjq7hH7Og7fHuTHsS9R2r', 'android', '0a7fb561-8f4f-4ba4-bcf5-f1ed6e57b7ea', NULL, NULL, 3, '2018-01-05 14:37:53', '2018-01-05 14:37:53'),
(5, 'fn5l3DYgHE0:APA91bEFq13YW7Gi3moN3-3UjtNePSCCy-TaC1oEQxMv-oDiDKkVDYVlvGhNrF4T91M3hRtQ6Wq41CWVCivnaqkYlwdvJGslq24FAaesYFPplOmQhF5lTXat9ONzsFlfVHt7-ge3cRzc', 'android', '2723568a-2829-43e3-80e3-6214314b52f6', NULL, NULL, 4, '2018-01-07 05:51:25', '2018-01-07 05:51:25'),
(7, 'ew2kZs0CQN8:APA91bHU3suKCSJl-Py3wI2glTMXYwPii2VXwzZ8jIBUyk262CperooYb-ZAig2mg-0AVgL4RBr0N58UEaJR_HFSSqNoNwgGsxHs6Fkp6raKl5PA8UPYmbWN7SleEjgpnQIhuolb7fPB', 'android', 'b7b758b1-bf31-4d90-8a89-c0abd554e0f6', NULL, NULL, 5, '2018-01-07 15:37:23', '2018-01-07 15:37:23'),
(11, NULL, 'android', 'ba415487-f9c3-4ed3-b338-32e16e988ba6', NULL, NULL, 6, '2018-01-08 10:56:46', '2018-01-08 11:00:24'),
(12, NULL, 'android', 'ba415487-f9c3-4ed3-b338-32e16e988ba6', NULL, NULL, 7, '2018-01-08 11:04:29', '2018-01-08 12:51:18'),
(15, '3599CD20052CE53EBE9F8091F3C5E6506AEFBB5C7EAD53F93D816C93B45FBA23', 'ios', 'cf79b8c1-218c-46a0-8219-cb00747e1e0d', NULL, NULL, 8, '2018-01-09 05:43:31', '2018-01-09 05:43:31'),
(20, NULL, 'android', 'ba415487-f9c3-4ed3-b338-32e16e988ba6', NULL, NULL, 10, '2018-01-09 10:14:25', '2018-01-09 10:16:04'),
(22, '87475AAF088490C31879C15437BB47C04EEE846D6D449DEB6DDF05C262A593AC', 'ios', 'cf79b8c1-218c-46a0-8219-cb00747e1e0d', NULL, NULL, 12, '2018-01-09 11:00:03', '2018-01-09 11:00:03'),
(29, NULL, 'ios', 'cf79b8c1-218c-46a0-8219-cb00747e1e0d', NULL, NULL, 1, '2018-01-10 12:47:01', '2018-01-11 11:23:23'),
(31, NULL, 'ios', 'cf79b8c1-218c-46a0-8219-cb00747e1e0d', NULL, NULL, 11, '2018-01-11 11:24:35', '2018-01-19 09:01:45'),
(48, 'hhhhhhhhhhh', 'android', '9e6c961b-60bd-48f8-a511-d4dc0cc994f9', NULL, NULL, 2, '2018-01-12 22:45:52', '2018-01-12 22:45:52'),
(49, 'cTzm6aWXFD4:APA91bGNNoa6IXsai2e-5O-05qLXtWz7rDWJvlzgeUIcXuA6qr0nUNFF4vkvH8W58Mlh5-NTZpzLT2FDU88Ged1CsmrnjYRw3y3RtTIk7YavooWew8WuHAk6a3notX6a9j_qI_5BGvAx', 'android', '7871056b-ab8d-4fbe-b80a-d0ac409de4c4', NULL, NULL, 9, '2018-01-16 11:05:29', '2018-01-16 11:05:29');

-- --------------------------------------------------------

--
-- Table structure for table `push_notification_user`
--

CREATE TABLE `push_notification_user` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `device_token` longtext,
  `player_id` longtext,
  `device` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `device_os` varchar(50) DEFAULT NULL,
  `device_model` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_notification_user`
--

INSERT INTO `push_notification_user` (`id`, `userid`, `device_token`, `player_id`, `device`, `created`, `modified`, `device_os`, `device_model`) VALUES
(2, 2, 'eCsgX5MPNEM:APA91bHTXJAdFAlrsB-KLUQySWKo1L5dPVDC-O2rD4rd5GmZ6L3Ezccauby7d1sxGELEDBW4J3EL5TvAu6ujQui7zosvHgTQ2ioyj4kzfYHWQZ49vFT0nZh9tm5VywWa8nAa3EbMXz-H', '81e8d167-3954-47d9-9bd1-0cfdb460ff13', 'android', '2018-01-05 14:47:25', '2018-01-05 14:47:25', NULL, NULL),
(3, 3, 'c9J58l2pXVQ:APA91bH24aGgxUnUyLYqYedlbNMMQncj3DzCKoxRhDxLpBi7Fx-rsZopCkfdhkOx1W8sK1byzuS5B6jo3HwnynEJsrS7_XlA60Di5n1RPO8UTZoVQQkANyZgQjzUAtj4-nEagmNXmw9H', '45b9593f-f8a8-4a6e-b5bc-81c08fc584f8', 'android', '2018-01-07 06:27:42', '2018-01-07 06:27:42', NULL, NULL),
(6, 4, 'dEu7FNLjBxY:APA91bE7jLlHusoRO0YwUQPQK2oLXuSalymoBW4yoooWQbRh3kq3Z12IfN91XHRTAQ4XmIdAsmygOil0qRvm_ngc0UGZE3AM0QPi2_AX6Hjvb6fO8ikpWOKJT9KY7aumsMe5AAoHvK5g', 'e2c88328-ce54-4422-884e-a50e9edef3b1', 'android', '2018-01-07 15:38:37', '2018-01-07 15:38:37', NULL, NULL),
(10, 6, '55bc3f44281862ee39e03055394131798f008e75226e4ae94d0b02a6a4b36df8', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-08 09:32:31', '2018-01-08 11:40:10', NULL, NULL),
(14, 8, 'eRpsTObPeYw:APA91bGcQU5hLgTtu8mJT_up-7C28cuZ7AWP2CERkEyJO56mLsEzxnQwrKNRG9SYxw1xDAd5-AkEzQG8B9rMuKE8_P7yO28vEaX76PQVL4xRwtWd5stUrdq91pI8jzxyoDzYDeVB0Wys', '6e9e29a9-5803-4a9c-9205-bf42ddc4cb31', 'android', '2018-01-08 12:47:26', '2018-01-08 12:47:26', NULL, NULL),
(15, 9, 'eRpsTObPeYw:APA91bGcQU5hLgTtu8mJT_up-7C28cuZ7AWP2CERkEyJO56mLsEzxnQwrKNRG9SYxw1xDAd5-AkEzQG8B9rMuKE8_P7yO28vEaX76PQVL4xRwtWd5stUrdq91pI8jzxyoDzYDeVB0Wys', NULL, NULL, '2018-01-08 12:48:03', '2018-01-08 12:48:03', NULL, NULL),
(16, 1, '55bc3f44281862ee39e03055394131798f008e75226e4ae94d0b02a6a4b36df8', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-08 13:31:09', '2018-01-09 05:24:36', NULL, NULL),
(17, 5, '', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-09 05:44:41', '2018-01-09 11:04:37', NULL, NULL),
(21, 12, 'eV9qX0_kRtk:APA91bEIc7Y6cS2n4GxkKoHaJH_cfi2fTlz4CeTafF8Ud5ihYkqJ5UK9rdz-bi-koyvbTG37pqtpe9XK8YGB1u-rnJPGTZsA3CpE6A-OEsK-nMqa77uHTs91ijNns1ixX0vyyibgwyEW', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-09 09:49:06', '2018-01-09 09:49:06', NULL, NULL),
(23, 14, 'eV9qX0_kRtk:APA91bEIc7Y6cS2n4GxkKoHaJH_cfi2fTlz4CeTafF8Ud5ihYkqJ5UK9rdz-bi-koyvbTG37pqtpe9XK8YGB1u-rnJPGTZsA3CpE6A-OEsK-nMqa77uHTs91ijNns1ixX0vyyibgwyEW', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-09 10:55:31', '2018-01-09 10:55:31', NULL, NULL),
(24, 15, 'eV9qX0_kRtk:APA91bEIc7Y6cS2n4GxkKoHaJH_cfi2fTlz4CeTafF8Ud5ihYkqJ5UK9rdz-bi-koyvbTG37pqtpe9XK8YGB1u-rnJPGTZsA3CpE6A-OEsK-nMqa77uHTs91ijNns1ixX0vyyibgwyEW', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-09 10:56:05', '2018-01-09 10:56:05', NULL, NULL),
(25, 16, 'eV9qX0_kRtk:APA91bEIc7Y6cS2n4GxkKoHaJH_cfi2fTlz4CeTafF8Ud5ihYkqJ5UK9rdz-bi-koyvbTG37pqtpe9XK8YGB1u-rnJPGTZsA3CpE6A-OEsK-nMqa77uHTs91ijNns1ixX0vyyibgwyEW', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-09 10:57:37', '2018-01-09 10:57:37', NULL, NULL),
(29, 17, '55bc3f44281862ee39e03055394131798f008e75226e4ae94d0b02a6a4b36df8', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-09 11:12:57', '2018-01-09 11:12:57', NULL, NULL),
(32, 18, 'dL46XBgZhE8:APA91bHGkUVWAooJSbMFmH6wYhOanDRxLf2wcbhOF5zRnlDXJwGcY_N-cnuxVC9MDmTBpwC8kDGysEHiKxWBlKUBL6LlxWbQfprqLhxxEnG8QJNkzxgcz3K-o4_SVwFUeGUY_lDRNjDm', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-10 10:34:18', '2018-01-10 10:34:18', NULL, NULL),
(33, 19, 'dL46XBgZhE8:APA91bHGkUVWAooJSbMFmH6wYhOanDRxLf2wcbhOF5zRnlDXJwGcY_N-cnuxVC9MDmTBpwC8kDGysEHiKxWBlKUBL6LlxWbQfprqLhxxEnG8QJNkzxgcz3K-o4_SVwFUeGUY_lDRNjDm', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-10 11:19:49', '2018-01-10 11:19:49', NULL, NULL),
(34, 20, 'dL46XBgZhE8:APA91bHGkUVWAooJSbMFmH6wYhOanDRxLf2wcbhOF5zRnlDXJwGcY_N-cnuxVC9MDmTBpwC8kDGysEHiKxWBlKUBL6LlxWbQfprqLhxxEnG8QJNkzxgcz3K-o4_SVwFUeGUY_lDRNjDm', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-10 11:25:42', '2018-01-10 11:25:42', NULL, NULL),
(35, 11, '', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-10 12:10:48', '2018-01-10 12:53:00', NULL, NULL),
(36, 10, 'dL46XBgZhE8:APA91bHGkUVWAooJSbMFmH6wYhOanDRxLf2wcbhOF5zRnlDXJwGcY_N-cnuxVC9MDmTBpwC8kDGysEHiKxWBlKUBL6LlxWbQfprqLhxxEnG8QJNkzxgcz3K-o4_SVwFUeGUY_lDRNjDm', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-10 12:51:12', '2018-01-10 12:51:12', NULL, NULL),
(37, 21, '', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-10 12:54:42', '2018-01-10 13:42:25', NULL, NULL),
(42, 22, '', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-11 06:35:05', '2018-01-11 06:36:59', NULL, NULL),
(44, 23, '5f1ece10961f575bf1b460c0c7a47523290a36ece21967626c81a97d064a3a1f', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-11 06:38:42', '2018-01-11 08:20:15', NULL, NULL),
(45, 24, '5f1ece10961f575bf1b460c0c7a47523290a36ece21967626c81a97d064a3a1f', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', '', '2018-01-11 07:16:49', '2018-01-11 07:16:49', NULL, NULL),
(46, 25, '5f1ece10961f575bf1b460c0c7a47523290a36ece21967626c81a97d064a3a1f', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', '', '2018-01-11 07:19:06', '2018-01-11 07:19:06', NULL, NULL),
(53, 13, '5f1ece10961f575bf1b460c0c7a47523290a36ece21967626c81a97d064a3a1f', 'da090a9f-eded-406c-ae60-e0dd604cb9e0', 'ios', '2018-01-12 10:21:34', '2018-01-30 07:19:48', NULL, NULL),
(54, 7, 'ewaSu6NUmpI:APA91bEz2B77BNj-zWmAZEWh-s_e3UhKe1wjjkjs-iJOBRSUob2PR3Gv7K9WMY1QhSlNhZ0KQUZZ8AIEGCCFtTeYNM9oBfqSVV9kBy5POflk7tDKtKj7zgW6o6hezb8ceVEiMc76DPoP', '58a81998-4ebf-4fe5-94c6-7c5088221efa', 'android', '2018-01-16 12:22:31', '2018-01-16 12:22:31', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `referal_driver_detail`
--

CREATE TABLE `referal_driver_detail` (
  `id` int(11) NOT NULL,
  `referby_driver_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `referral_code` varchar(50) NOT NULL,
  `current_balance` double NOT NULL DEFAULT '0',
  `start_date` varchar(50) DEFAULT NULL,
  `end_date` varchar(50) DEFAULT NULL,
  `ride_complete` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `refer_amount`
--

CREATE TABLE `refer_amount` (
  `id` int(11) NOT NULL,
  `amount_refer_type` varchar(50) NOT NULL,
  `percentage_value` float(5,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `refer_amount`
--

INSERT INTO `refer_amount` (`id`, `amount_refer_type`, `percentage_value`) VALUES
(1, 'najez', 25.00),
(2, 'driver', 75.00),
(3, 'invited_customer', 2.50),
(4, 'invited_driver', 2.50);

-- --------------------------------------------------------

--
-- Table structure for table `response_message`
--

CREATE TABLE `response_message` (
  `id` int(11) NOT NULL,
  `message_en` varchar(200) DEFAULT NULL,
  `message_ar` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `response_message`
--

INSERT INTO `response_message` (`id`, `message_en`, `message_ar`) VALUES
(1, 'SUCCESS', 'نجاح'),
(2, 'Fail', 'فشل'),
(3, 'User Not Found', 'لم یوجد المستخدم'),
(4, 'Driver Not Found', 'لم یوجد السائق'),
(5, 'Transaction Failed', 'قد فشلت المعاملة'),
(6, 'Amount credit successfully', 'تم إئتمان المبلغ بنجاح'),
(7, 'Invalid Coupon Code', 'رمز الکوبون غیر صالح'),
(8, 'sorry your have insufficient balance', 'عذراً من أن لدیك النقود غیر کاف'),
(9, 'User does not Exists', 'المستخدم غیر موجود'),
(10, 'No transactions available', 'المعاملات غیر متوافرۃ'),
(11, 'Invalid ride id', 'ھویة الرحلة غیر صالحة'),
(12, 'no promotion value found', 'لم توجد قیمة ترویجیة'),
(13, 'no data', 'البیانات غیر متوافرۃ'),
(14, 'low balance', 'النقود القلیلة'),
(15, 'user location not found', 'لم یوجد موقع المستخدم');

-- --------------------------------------------------------

--
-- Table structure for table `rides`
--

CREATE TABLE `rides` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `driverid` int(11) DEFAULT NULL,
  `trip_id` int(12) DEFAULT NULL,
  `vehicle_type` int(12) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `booking_time` varchar(255) DEFAULT NULL,
  `driver_reach_location` longtext,
  `driver_reach_time` varchar(255) DEFAULT NULL,
  `ride_start_time` varchar(255) DEFAULT NULL,
  `distance` double(11,4) DEFAULT '0.0000',
  `calculated_price` decimal(15,4) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `pickup_location` longtext,
  `drop_location` longtext,
  `ride_progress` int(11) DEFAULT NULL,
  `promo_code` varchar(255) DEFAULT NULL,
  `ride_flag` int(11) DEFAULT '0' COMMENT 'newride-0,driver arrived-1,accept-2,complete-3,cancel-4',
  `cancel_option` int(11) DEFAULT '0',
  `review_flag_user` int(11) DEFAULT '0',
  `review_flag_driver` int(11) DEFAULT '0',
  `GPS_Destination_Point` mediumtext CHARACTER SET utf8,
  `GPS_Starting_Point` mediumtext CHARACTER SET utf8,
  `ride_end_time` varchar(255) DEFAULT NULL,
  `final_price` decimal(15,4) DEFAULT '0.0000',
  `trip_type_vehicle_family` int(11) DEFAULT NULL,
  `scheduled` int(1) DEFAULT '0',
  `longtrip_city_id` int(12) DEFAULT '0',
  `cancel_time` varchar(50) DEFAULT NULL,
  `cancel_user_type` varchar(50) DEFAULT NULL,
  `delivery_img` varchar(150) DEFAULT NULL,
  `delivery_comment` varchar(150) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rides`
--

INSERT INTO `rides` (`id`, `userid`, `driverid`, `trip_id`, `vehicle_type`, `vehicle_id`, `booking_time`, `driver_reach_location`, `driver_reach_time`, `ride_start_time`, `distance`, `calculated_price`, `payment_method`, `transaction_id`, `pickup_location`, `drop_location`, `ride_progress`, `promo_code`, `ride_flag`, `cancel_option`, `review_flag_user`, `review_flag_driver`, `GPS_Destination_Point`, `GPS_Starting_Point`, `ride_end_time`, `final_price`, `trip_type_vehicle_family`, `scheduled`, `longtrip_city_id`, `cancel_time`, `cancel_user_type`, `delivery_img`, `delivery_comment`, `created`) VALUES
(1, 1, 1, 1, 1, 1, '1515160687568', NULL, '1515160711367', '1515160748035', 0.0000, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 3, 0, 0, 0, '28.4431548,77.0558356', '28.44343165816553,77.05591410398483', '1515160772022', '8.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-05 13:58:08'),
(2, 1, NULL, 1, 2, NULL, '1515162149583', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 0, 0, 0, 0, '28.4431419,77.0558376', '28.443155426266685,77.05583833158016', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-05 14:22:30'),
(3, 2, NULL, 1, 3, NULL, '1515163693506', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 0, 0, 0, 0, '28.491005297419942,77.08965461701155', '28.443141865237067,77.05583766102791', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-05 14:48:14'),
(4, 2, NULL, 1, 3, NULL, '1515163743384', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'Gurugram', 'Belverede Apartments II DLF Cyber City DLF Phase 2 Sector 25 Gurugram Haryana 122002 India', NULL, '', 0, 0, 0, 0, '28.491005297419942,77.08965461701155', '28.443141865237067,77.05583766102791', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-05 14:49:04'),
(5, 2, 1, 1, 1, 1, '1515163789028', NULL, '1515163806471', '1515163816296', 0.0000, NULL, 'Cash', NULL, 'Gurugram', '809-A Udyog Vihar Phase V Phase V Udyog Vihar Sector 19 Gurugram Haryana 122022 India', NULL, '', 3, 0, 0, 0, '28.50068039297903,77.08599172532558', '28.443141865237067,77.05583766102791', '1515163823157', '8.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-05 14:49:49'),
(6, 3, 4, 1, 3, 4, '1515306472604', NULL, '1515306496931', '1515306507039', 0.0100, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 3, 0, 0, 0, '28.409272,77.034218', '28.409271899326907,77.03421805053948', '1515306518169', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-07 06:27:54'),
(7, 4, NULL, 1, 1, NULL, '1515355200000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'Dhahran Mall Food Court', '', NULL, '', 0, 0, 0, 0, '26.3152039,50.183184', '26.305004781062713,50.165239199996', NULL, '0.0000', NULL, 1, 0, NULL, NULL, NULL, NULL, '2018-01-07 09:00:09'),
(8, 4, 5, 1, 1, 5, '1515339547839', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, '?????', '', NULL, '', 4, 6, 0, 0, '26.3155465,50.2288937', '26.31555266777556,50.22889204323292', NULL, '0.0000', NULL, 0, 0, '1515339681657', 'user', '', '', '2018-01-07 15:39:08'),
(9, 6, NULL, 1, 2, NULL, '1515403920000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Nathupur Sector 24 Gurugram Haryana', NULL, '', 0, 0, 0, 0, '28.4858404635481,77.0987885445356', '28.4434897385592,77.0559262056274', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-08 09:33:11'),
(10, 6, 6, 1, 2, 6, '1515404040000', NULL, '1515404179097', '1515404244361', 0.1500, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'DLF Golf Course Sector 42 Gurugram Haryana', NULL, '', 4, 2, 0, 0, '28.4566141856725,77.1102804690599', '28.4435325796709,77.0559796258773', '1515404280583', '12.0000', NULL, 0, 0, '1515405022111', 'user', '', '', '2018-01-08 09:34:37'),
(11, 6, 6, 1, 2, 6, '1515405000000', NULL, '1515405575308', '1515405671331', 6.9800, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Acharya Shree Tulsi Marg Andheria Mor Village Vasant Kunj New Delhi Delhi 110070', NULL, '', 3, 0, 0, 0, '28.5067572607532,77.1748124063015', '28.4433690360927,77.0560913770322', '1515405707748', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-08 09:51:15'),
(12, 6, 6, 1, 2, 6, '1515405840000', NULL, '1515405967235', '1515406015913', 15.7500, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Qutab Institutional Area Block B Road Block A Qutab Institutional Area New Delhi Delhi 110016', NULL, '', 3, 0, 0, 0, '28.5396808714731,77.1818706393242', '28.4434611191983,77.0559925800162', '1515406035029', '12.0000', NULL, 0, 0, NULL, NULL, 'assets/documents/image-36e08ed3baf59fea236a068d13cd97621beabdb9.jpg', 'Hello', '2018-01-08 10:05:19'),
(13, 1, NULL, 1, 1, NULL, '1515418380000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4434659221647,77.0560160680282', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-08 13:36:24'),
(14, 1, NULL, 1, 1, NULL, '1515418380000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4434689028296,77.0560129546423', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-08 13:36:46'),
(15, 1, 1, 1, 1, 1, '1515418380000', NULL, '1515418989089', '1515419022809', 0.0100, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.443471,77.056009', '28.4434701947958,77.0560061152953', '1515419035608', '8.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-08 13:37:15'),
(16, 5, NULL, 1, 2, NULL, '1515999960000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4434564551603,77.0558390022042', NULL, '0.0000', NULL, 1, 0, NULL, NULL, NULL, NULL, '2018-01-09 06:28:13'),
(17, 7, 12, 1, 3, 15, '1515496781005', NULL, '1515496839098', '1515496934774', 0.0000, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 3, 0, 0, 0, '28.4431595,77.0558361', '28.44525883883317,77.05601535737514', '1515496961523', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-09 11:19:41'),
(18, 7, 12, 1, 3, 15, '1515497240468', NULL, NULL, NULL, 0.0000, NULL, 'Wallet', NULL, 'Gurugram', '71 Block G South City I Sector 41 Gurugram Haryana 122003 India', NULL, 'PROMO100', 4, 0, 0, 0, '28.455631721182794,77.06429734826088', '28.434144623331708,77.06649575382471', NULL, '0.0000', NULL, 0, 0, '1515497251700', 'user', '', '', '2018-01-09 11:27:21'),
(19, 7, 9, 1, 1, 12, '1515566100000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 0, 0, 0, '0.0,0.0', '28.4434856073209,77.0560029521585', NULL, '0.0000', NULL, 0, 0, '1515566792884', 'user', '', '', '2018-01-10 06:35:34'),
(20, 7, NULL, 1, 1, NULL, '1515567180000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Jille Singh Farm Road Vasant Kunj New Delhi Delhi 110070', NULL, 'PROMO200', 0, 0, 0, 0, '28.5089719526354,77.1479313075542', '28.4434755272574,77.0560741933097', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 06:54:24'),
(21, 7, NULL, 1, 3, NULL, '1515567300000', NULL, NULL, NULL, 0.0500, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Unnamed Road Rajokri Village Rajokri New Delhi Delhi 110038', NULL, '', 0, 0, 0, 0, '28.5083293815295,77.1109741553664', '28.4434487450868,77.0560582298323', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 06:56:03'),
(22, 7, NULL, 1, 3, NULL, '1515567360000', NULL, NULL, NULL, 0.0300, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'H-2 Road Block H DLF Phase 1 Sector 26 Gurugram Haryana 122002', NULL, '', 0, 0, 0, 0, '28.4773593567581,77.098448574543', '28.4434487450868,77.0560582298323', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 06:56:38'),
(23, 7, NULL, 1, 3, NULL, '1515567420000', NULL, NULL, NULL, 0.0200, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 29 Gurugram Haryana 122022', NULL, '', 0, 0, 0, 0, '28.4688814004609,77.0697925239801', '28.4434487450868,77.0560582298323', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 06:57:21'),
(24, 7, NULL, 1, 3, NULL, '1515567420000', NULL, NULL, NULL, 2.4900, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 29 Gurugram Haryana 122022', NULL, '', 0, 0, 0, 0, '28.4688814004609,77.0697925239801', '28.4434487450868,77.0560582298323', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 06:57:49'),
(25, 7, NULL, 1, 1, NULL, '1515568380000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4434380421999,77.0561950544168', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 07:13:57'),
(26, 7, 9, 1, 1, 12, '1515568380000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 1, 0, 0, '0.0,0.0', '28.4434977591948,77.055981812065', NULL, '0.0000', NULL, 0, 0, '1515568681028', 'user', '', '', '2018-01-10 07:16:23'),
(27, 7, NULL, 1, 1, NULL, '1515583980000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, '1089 Sector 40 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4434629628299,77.0560051366867', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 11:33:46'),
(28, 11, NULL, 1, 1, NULL, '1515587460000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'Hyadea India Pvt Ltd 75C Wapcos Road Sector 18 Gurugram Haryana 122008', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.443449216847,77.0559310319166', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 12:31:58'),
(29, 11, 9, 1, 1, 12, '1515587460000', NULL, '1515587625861', '1515587636374', 0.0900, NULL, 'Cash', NULL, 'Hyadea India Pvt Ltd 75C Wapcos Road Sector 18 Gurugram Haryana 122008', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.4434660665864,77.0559449280325', '1515587710805', '8.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 12:33:14'),
(30, 11, 9, 1, 1, 12, '1515587880000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 6, 0, 0, '0.0,0.0', '28.4434395859596,77.0560074959753', NULL, '0.0000', NULL, 0, 0, '1515587952236', 'user', '', '', '2018-01-10 12:38:28'),
(31, 11, 9, 1, 1, 12, '1515587940000', NULL, '1515587977134', '1515587988159', 0.6400, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.4434430158374,77.0560136685287', '1515587998227', '8.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 12:39:20'),
(32, 11, 9, 1, 1, 12, '1515588000000', NULL, '1515588060155', '1515588067998', 2.1600, NULL, 'Cash', NULL, '84 Netaji Subhash Marg Block C Uday Nagar Sector 45 Gurugram Haryana 122003', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.4413199545815,77.0626078918576', '1515588074412', '8.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 12:40:37'),
(33, 11, NULL, 1, 1, NULL, '1515588480000', NULL, NULL, NULL, 0.0800, NULL, 'Cash', NULL, '1084 Sector 40 Gurugram Haryana 122001', '', NULL, '', 4, 0, 0, 0, '0.0,0.0', '28.4433778786521,77.0561288616381', NULL, '0.0000', NULL, 0, 0, '1515588491088', 'user', '', '', '2018-01-10 12:48:06'),
(34, 10, 1, 1, 1, 1, '1515588707978', NULL, NULL, NULL, 0.2400, NULL, 'Cash', NULL, 'Gurugram', '1571 Carterpuri Rd Palam Farms Gurugram Haryana 110061 India', NULL, '', 4, 0, 0, 0, '28.52566888466953,77.05206245183945', '28.44321998853603,77.05587889999151', NULL, '0.0000', NULL, 0, 0, '1515588752424', 'user', '', 'des', '2018-01-10 12:51:48'),
(35, 21, NULL, 1, 4, NULL, '1515588900000', NULL, NULL, NULL, 0.0200, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '43 Vivekanand Marg Sector 4 RK Puram New Delhi Delhi 110022', NULL, '', 0, 0, 0, 0, '28.5613970934457,77.1752680465579', '28.4434547632136,77.0560203780765', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', 'Des', '2018-01-10 12:55:26'),
(36, 21, NULL, 1, 4, NULL, '1515588900000', NULL, NULL, NULL, 0.2200, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '43 Vivekanand Marg Sector 4 RK Puram New Delhi Delhi 110022', NULL, '', 0, 0, 0, 0, '28.5613970934457,77.1752680465579', '28.4434386839824,77.0559610090545', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', 'Des', '2018-01-10 12:56:00'),
(37, 21, NULL, 1, 1, NULL, '1515591600000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, '676 Durga Colony Jharsa Village Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4435469386662,77.056041533897', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-10 13:40:20'),
(38, 13, 2, 1, 2, 2, '1515669120000', NULL, '1515669371365', '1515669387207', 0.0000, NULL, 'Wallet', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 4, 3, 0, 0, '28.5642463,77.1817439', '28.4434219636288,77.0559355617287', '1515669391805', '12.0000', NULL, 0, 0, '1515669558877', 'user', 'assets/documents/image-c2b388bc6dde56f13a066b9f45e842e633c31d58.jpg', 'Hi', '2018-01-11 11:13:11'),
(39, 13, 2, 1, 2, 2, '1515669600000', NULL, '1515669651400', '1515669663708', 0.8400, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 2, 0, 0, '0.0,0.0', '28.4434101032358,77.0559139364185', NULL, '0.0000', NULL, 0, 0, '1515669697239', 'user', '', '', '2018-01-11 11:20:18'),
(40, 13, NULL, 1, 3, NULL, '1515669720000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4434478918225,77.0559917159962', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 11:22:32'),
(41, 1, NULL, 1, 2, NULL, '1515669866652', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 0, 0, 0, 0, '28.4432827,77.0559251', '28.44324357291684,77.05589633435011', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 11:24:27'),
(42, 1, NULL, 1, 2, NULL, '1515669871819', NULL, NULL, NULL, 0.3500, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 0, 0, 0, 0, '28.4432827,77.0559251', '28.44324357291684,77.05589633435011', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 11:24:32'),
(43, 1, 11, 1, 3, 14, '1515669966090', NULL, '1515669990203', '1515670014049', 0.0000, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 4, 0, 0, 0, '28.4432433,77.0559002', '28.4432780650643,77.05591779202223', '1515670054010', '12.0000', NULL, 0, 0, '1515670540448', 'user', '', '', '2018-01-11 11:26:06'),
(44, 13, NULL, 1, 2, NULL, '1515670140000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4431621246305,77.0564104803624', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 11:30:16'),
(45, 13, 2, 1, 2, 2, '1515670140000', NULL, '1515670280885', '1515670290745', 0.3900, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432548,77.055904', '28.4433714626622,77.0561037865254', '1515670491087', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 11:30:39'),
(46, 1, 11, 1, 3, 14, '1515670561585', NULL, '1515670580738', '1515670586024', 0.0000, NULL, 'Cash', NULL, 'Gurugram', '', NULL, '', 4, 3, 0, 0, '28.4432171,77.0558827', '28.443223231388696,77.05588359385729', '1515670691403', '12.0000', NULL, 0, 0, '1515670708404', 'user', '', '', '2018-01-11 11:36:02'),
(47, 13, 2, 1, 2, 2, '1515671220000', NULL, '1515671246067', '1515671281546', 0.0400, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.443468261842,77.0560103344469', '1515671307387', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 11:47:06'),
(48, 13, 2, 1, 2, 2, '1515671340000', NULL, '1515671432269', NULL, 0.0600, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 2, 0, 0, '0.0,0.0', '28.4433533158418,77.0558215678456', NULL, '0.0000', NULL, 0, 0, '1515671697172', 'user', '', '', '2018-01-11 11:49:54'),
(49, 13, 2, 1, 2, 2, '1515671700000', NULL, '1515671740644', '1515671746764', 3.1200, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.4433665592488,77.0558706857982', '1515671757206', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 11:55:15'),
(50, 13, 2, 1, 2, 2, '1515672300000', NULL, '1515672372213', '1515672377093', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.4434353392373,77.0559702117889', '1515672382693', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 12:05:54'),
(51, 13, 2, 1, 2, 2, '1515674700000', NULL, '1515674777956', NULL, 0.2600, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 3, 0, 0, '0.0,0.0', '28.4433561882627,77.0559328794479', NULL, '0.0000', NULL, 0, 0, '1515675188013', 'user', '', '', '2018-01-11 12:45:38'),
(52, 13, 2, 1, 2, 2, '1515677580000', NULL, '1515677744625', '1515677840329', 0.0800, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432372,77.0558803', '28.4434528184823,77.0559729564554', '1515677843240', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 13:33:48'),
(53, 13, 2, 1, 2, 2, '1515678000000', NULL, '1515678041964', '1515678097283', 3.2200, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432829,77.0559319', '28.4434343114033,77.0559875294566', '1515678099207', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-11 13:40:19'),
(54, 13, 2, 1, 2, 2, '1515678660000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 2, 0, 0, '0.0,0.0', '28.4434153419253,77.0559006091925', NULL, '0.0000', NULL, 0, 0, '1515678693633', 'user', '', '', '2018-01-11 13:51:03'),
(55, 13, NULL, 1, 4, NULL, '1515678840000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 0, 0, 0, 0, '28.5642463,77.1817439', '28.4438113868501,77.05574328087', NULL, '0.0000', NULL, 0, 0, NULL, NULL, 'assets/documents/image-d608ee7317a19c324037ae799d0aab24c829df36.jpg', 'Ghj', '2018-01-11 13:56:07'),
(56, 13, 2, 1, 2, 2, '1515745500000', NULL, '1515745613890', '1515745670050', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 3, 0, 0, '28.4432672,77.0559109', '28.4434567639888,77.0559306319406', '1515745694291', '12.0000', NULL, 0, 0, '1515745714700', 'user', '', '', '2018-01-12 08:25:57'),
(57, 13, 2, 1, 2, 2, '1515745800000', NULL, '1515745837905', '1515745848615', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 4, 3, 0, 0, '28.5642463,77.1817439', '28.4434593826084,77.055932769177', '1515745866615', '12.0000', NULL, 0, 0, '1515745913473', 'user', '', '', '2018-01-12 08:30:18'),
(58, 13, 2, 1, 2, 2, '1515745920000', NULL, '1515746032303', '1515746038932', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 4, 4, 0, 0, '28.5642463,77.1817439', '28.443463291139,77.0559366377232', '1515746053653', '12.0000', NULL, 0, 0, '1515746478507', 'user', '', '', '2018-01-12 08:32:21'),
(59, 13, NULL, 1, 2, NULL, '1515746460000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4435046392226,77.056147369676', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 08:41:48'),
(60, 13, 2, 1, 2, 2, '1515746460000', NULL, '1515746901684', '1515747049509', 0.0500, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432338,77.0558952', '28.4434728302027,77.0560059836096', '1515747075109', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 08:48:02'),
(61, 13, 2, 1, 2, 2, '1515747120000', NULL, '1515747414673', '1515748265867', 0.3800, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 3, 0, 0, 0, '28.5642463,77.1817439', '28.4436132968835,77.0559202643158', '1515748267582', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 08:55:54'),
(62, 13, 2, 1, 2, 2, '1515748560000', NULL, '1515748637164', '1515748641054', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 3, 0, 0, 0, '28.5642463,77.1817439', '28.4434887283188,77.0560069304689', '1515748646784', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 09:16:55'),
(63, 13, NULL, 1, 2, NULL, '1515751500000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 0, 0, 0, 0, '28.5642463,77.1817439', '28.4434792860519,77.0558579692089', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 10:05:08'),
(64, 13, NULL, 1, 2, NULL, '1515751500000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 0, 0, 0, 0, '28.5642463,77.1817439', '28.4434707584628,77.0560064194775', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 10:05:37'),
(65, 13, NULL, 1, 2, NULL, '1515751500000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 0, 0, 0, 0, '0.0,0.0', '28.4434722040271,77.0560051075118', NULL, '0.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 10:05:45'),
(66, 13, 2, 1, 2, 2, '1515751680000', NULL, '1515751804562', '1515751807268', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', 'Sector 1 Market RK Puram New Delhi Delhi', NULL, '', 3, 0, 0, 0, '28.5642463,77.1817439', '28.4434862032061,77.0559924020136', '1515751809439', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 10:09:07'),
(67, 13, 2, 1, 2, 2, '1515752520000', NULL, '1515752651564', '1515752654597', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 5, 0, 0, '28.4433047,77.0559395', '28.4434568955068,77.0559323632644', '1515752657939', '12.0000', NULL, 0, 0, '1515752679021', 'user', '', '', '2018-01-12 10:23:53'),
(68, 13, 2, 1, 2, 2, '1515752700000', NULL, '1515752743559', '1515752745088', 0.0100, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 3, 0, 0, '28.443324,77.0559527', '28.4434804996393,77.0559913789644', '1515752754894', '12.0000', NULL, 0, 0, '1515752788023', 'user', '', '', '2018-01-12 10:25:16'),
(69, 13, 2, 1, 2, 2, '1515752760000', NULL, '1515752831134', '1515752845783', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 3, 0, 0, '28.4432833,77.0559159', '28.4434646144292,77.0559673937883', '1515752856134', '12.0000', NULL, 0, 0, '1515753020419', 'user', '', '', '2018-01-12 10:26:50'),
(70, 13, 2, 1, 2, 2, '1515753840000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 3, 0, 0, '0.0,0.0', '28.4437256794213,77.0560123152782', NULL, '0.0000', NULL, 0, 0, '1515753874405', 'user', '', '', '2018-01-12 10:44:11'),
(71, 13, 2, 1, 2, 2, '1515753840000', NULL, '1515753921663', '1515753941223', 0.1100, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.4436346917925,77.0559797011927', '1515753960764', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 10:44:57'),
(72, 13, 2, 1, 2, 2, '1515754500000', NULL, NULL, NULL, 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 4, 3, 0, 0, '0.0,0.0', '28.4435297914129,77.0559468873236', NULL, '0.0000', NULL, 0, 0, '1515754534186', 'user', '', '', '2018-01-12 10:55:12'),
(73, 13, 2, 1, 2, 2, '1515754680000', NULL, '1515754704511', '1515754707491', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.4435198467714,77.055958241581', '1515754712901', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 10:58:08'),
(74, 13, 2, 1, 2, 2, '1515755400000', NULL, '1515756190335', '1515756192530', 0.0000, NULL, 'Cash', NULL, 'L8L9 Tower D Unitech Cyber Park Arya Samaj Road Durga Colony Sector 39 Gurugram Haryana 122001', '', NULL, '', 3, 0, 0, 0, '28.4432106,77.0558788', '28.4434705723131,77.0559509843588', '1515756194832', '12.0000', NULL, 0, 0, NULL, NULL, '', '', '2018-01-12 11:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `ride_break_down`
--

CREATE TABLE `ride_break_down` (
  `ride_break_down_id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `promo_code` varchar(50) DEFAULT NULL,
  `waiting_time` double(11,2) DEFAULT '0.00' COMMENT 'In Minute',
  `driver_waiting_time` double(11,2) DEFAULT '0.00' COMMENT 'In Minute',
  `distance` double(11,2) DEFAULT '0.00' COMMENT 'In KM',
  `fixed_amount` double(11,2) DEFAULT '0.00',
  `distance_amount` double(11,2) DEFAULT '0.00',
  `waiting_amount` double(11,2) DEFAULT '0.00',
  `promo_discount` double(11,2) DEFAULT '0.00',
  `airport_charge` double DEFAULT '0',
  `other_charge` double(11,2) DEFAULT '0.00',
  `total_amount` double(11,2) DEFAULT '0.00',
  `airport_id` int(11) DEFAULT NULL,
  `total_tax` double(11,2) DEFAULT '0.00',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ride_break_down`
--

INSERT INTO `ride_break_down` (`ride_break_down_id`, `ride_id`, `promo_code`, `waiting_time`, `driver_waiting_time`, `distance`, `fixed_amount`, `distance_amount`, `waiting_amount`, `promo_discount`, `airport_charge`, `other_charge`, `total_amount`, `airport_id`, `total_tax`, `created`) VALUES
(1, 1, '', 0.00, 0.00, 0.00, 3.50, 0.00, 0.00, 0.00, 0, 0.00, 8.00, 0, 0.00, '2018-01-05 13:59:32'),
(2, 5, '', 0.00, 0.00, 0.00, 3.50, 0.00, 0.00, 0.00, 0, 0.00, 8.00, 0, 0.00, '2018-01-05 14:50:23'),
(3, 6, '', 0.00, 0.00, 0.01, 5.50, 0.01, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-07 06:28:38'),
(4, 10, '', 1.00, 0.00, 0.15, 5.50, 0.19, 0.40, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-08 09:38:00'),
(5, 10, NULL, 9.00, 9.00, 0.00, 5.50, 0.00, 3.60, 0.00, 0, 0.00, 9.00, NULL, 0.00, '2018-01-08 09:50:22'),
(6, 11, '', 1.00, 0.00, 2.63, 5.50, 3.29, 0.40, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-08 10:01:47'),
(7, 12, '', 0.00, 0.00, 2.16, 5.50, 2.70, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-08 10:07:15'),
(8, 15, '', 0.00, 0.00, 0.01, 3.50, 0.01, 0.00, 0.00, 0, 0.00, 8.00, 0, 0.00, '2018-01-08 13:43:55'),
(9, 17, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-09 11:22:41'),
(10, 29, '', 1.00, 0.00, 0.09, 3.50, 0.08, 0.25, 0.00, 0, 0.00, 8.00, 0, 0.00, '2018-01-10 12:35:10'),
(11, 31, '', 0.00, 0.00, 0.08, 3.50, 0.07, 0.00, 0.00, 0, 0.00, 8.00, 0, 0.00, '2018-01-10 12:39:58'),
(12, 32, '', 0.00, 0.00, 0.46, 3.50, 0.41, 0.00, 0.00, 0, 0.00, 8.00, 0, 0.00, '2018-01-10 12:41:14'),
(13, 38, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 11:16:31'),
(14, 43, '', 1.00, 0.00, 0.00, 5.50, 0.00, 0.40, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 11:27:33'),
(15, 45, '', 3.00, 0.00, 0.39, 5.50, 0.49, 1.20, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 11:34:51'),
(16, 43, NULL, 4.00, 4.00, 0.00, 5.50, 0.00, 1.60, 0.00, 0, 0.00, 7.00, NULL, 0.00, '2018-01-11 11:35:40'),
(17, 46, '', 2.00, 0.00, 0.00, 5.50, 0.00, 0.80, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 11:38:11'),
(18, 47, '', 0.00, 0.00, 0.04, 5.50, 0.05, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 11:48:27'),
(19, 49, '', 0.00, 0.00, 0.12, 5.50, 0.15, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 11:55:57'),
(20, 50, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 12:06:22'),
(21, 51, NULL, 2.00, 2.00, 0.00, 5.50, 0.00, 0.80, 0.00, 0, 0.00, 6.00, NULL, 0.00, '2018-01-11 12:53:07'),
(22, 52, '', 0.00, 0.00, 0.07, 5.50, 0.09, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 13:37:23'),
(23, 53, '', 0.00, 0.00, 0.56, 5.50, 0.70, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-11 13:41:39'),
(24, 56, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 08:28:14'),
(25, 57, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 08:31:06'),
(26, 58, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 08:34:13'),
(27, 58, NULL, 2.00, 2.00, 0.00, 5.50, 0.00, 0.80, 0.00, 0, 0.00, 6.00, NULL, 0.00, '2018-01-12 08:41:18'),
(28, 60, '', 0.00, 0.00, 0.05, 5.50, 0.06, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 08:51:15'),
(29, 61, '', 9.00, 9.00, 0.38, 5.50, 0.48, 3.60, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 09:11:07'),
(30, 62, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 09:17:26'),
(31, 66, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 10:10:09'),
(32, 67, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 10:24:17'),
(33, 68, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 10:25:54'),
(34, 69, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 10:27:36'),
(35, 71, '', 0.00, 0.00, 0.06, 5.50, 0.08, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 10:46:00'),
(36, 73, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 10:58:32'),
(37, 74, '', 0.00, 0.00, 0.00, 5.50, 0.00, 0.00, 0.00, 0, 0.00, 12.00, 0, 0.00, '2018-01-12 11:23:14');

-- --------------------------------------------------------

--
-- Table structure for table `ride_complete`
--

CREATE TABLE `ride_complete` (
  `id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `fare_amount` double(11,2) DEFAULT '0.00',
  `halal` double(11,2) DEFAULT '0.00',
  `previous_remaining` double(11,2) DEFAULT '0.00',
  `payment_cash` double(11,2) DEFAULT '0.00',
  `payment_wallet` double(11,2) DEFAULT '0.00',
  `payment_eft` double(11,2) DEFAULT '0.00',
  `current_wallet_balance` double(11,2) DEFAULT '0.00',
  `total_pay_amount` double(11,2) DEFAULT '0.00',
  `remaining_amount` double(11,2) DEFAULT '0.00',
  `driver_amount` double(11,2) DEFAULT '0.00',
  `najez_amount` double(11,2) DEFAULT '0.00',
  `invite_user_id` varchar(50) DEFAULT NULL,
  `invite_user_amount` double(11,2) DEFAULT '0.00',
  `invite_driver_id` varchar(50) DEFAULT NULL,
  `invite_driver_amount` double(11,2) DEFAULT '0.00',
  `transfer_amount` double(11,2) DEFAULT '0.00',
  `rounding` double(11,2) DEFAULT '0.00',
  `transaction_detail` text,
  `status` varchar(50) DEFAULT NULL COMMENT 'inprocess,complete,cancel',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ride_complete`
--

INSERT INTO `ride_complete` (`id`, `ride_id`, `fare_amount`, `halal`, `previous_remaining`, `payment_cash`, `payment_wallet`, `payment_eft`, `current_wallet_balance`, `total_pay_amount`, `remaining_amount`, `driver_amount`, `najez_amount`, `invite_user_id`, `invite_user_amount`, `invite_driver_id`, `invite_driver_amount`, `transfer_amount`, `rounding`, `transaction_detail`, `status`, `created`) VALUES
(1, 1, 8.00, 0.50, 0.00, 8.00, 0.00, 0.00, 0.00, 0.00, 0.00, 6.00, 2.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-05 13:59:32'),
(2, 5, 8.00, 0.50, 0.00, 55.00, 0.00, 0.00, 0.00, 0.00, 0.00, 6.00, 2.00, '', 0.00, '', 0.00, 47.00, 0.00, 'successful', 'complete', '2018-01-05 14:50:23'),
(3, 6, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 0.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-07 06:28:38'),
(4, 10, 12.00, 0.50, 0.00, 20.00, 0.00, 0.00, 0.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 8.00, 0.00, 'successful', 'complete', '2018-01-08 09:38:00'),
(5, 11, 12.00, 0.50, 0.00, 20.00, 0.00, 0.00, 20.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 8.00, 0.00, 'successful', 'complete', '2018-01-08 10:01:47'),
(6, 12, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 40.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-08 10:07:15'),
(7, 15, 8.00, 0.50, 0.00, 20.00, 0.00, 0.00, 8.00, 0.00, 0.00, 6.00, 2.00, '', 0.00, '', 0.00, 12.00, 0.00, 'successful', 'complete', '2018-01-08 13:43:55'),
(8, 17, 12.00, 0.50, 0.00, 55.00, 0.00, 0.00, 0.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 43.00, 0.00, 'successful', 'complete', '2018-01-09 11:22:41'),
(9, 29, 8.00, 0.50, 0.00, 8.00, 0.00, 0.00, 0.00, 0.00, 0.00, 6.00, 2.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-10 12:35:10'),
(10, 31, 8.00, 0.50, 0.00, 98.00, 0.00, 0.00, 8.00, 0.00, 0.00, 6.00, 2.00, '', 0.00, '', 0.00, 90.00, 0.00, 'successful', 'complete', '2018-01-10 12:39:58'),
(11, 32, 8.00, 0.50, 0.00, 999.00, 0.00, 0.00, 106.00, 0.00, 0.00, 6.00, 2.00, '', 0.00, '', 0.00, 991.00, 0.00, 'successful', 'complete', '2018-01-10 12:41:14'),
(12, 38, 12.00, 0.50, 0.00, 12.00, 12.00, 0.00, 20.00, 12.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 12.00, 0.00, 'successful', 'complete', '2018-01-11 11:16:31'),
(13, 43, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 28.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-11 11:27:34'),
(14, 45, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 20.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-11 11:34:51'),
(15, 46, 12.00, 0.50, 0.00, 0.00, 0.00, 0.00, 40.00, 0.00, 12.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'inprocess', '2018-01-11 11:38:11'),
(16, 47, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 32.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-11 11:48:27'),
(17, 49, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 44.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-11 11:55:57'),
(18, 50, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 56.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-11 12:06:22'),
(19, 52, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 68.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-11 13:37:23'),
(20, 53, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 80.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-11 13:41:39'),
(21, 56, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 92.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 08:28:14'),
(22, 57, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 104.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 08:31:06'),
(23, 58, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 116.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 08:34:13'),
(24, 60, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 128.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 08:51:15'),
(25, 61, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 140.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 09:11:07'),
(26, 62, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 152.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 09:17:26'),
(27, 66, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 164.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 10:10:09'),
(28, 67, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 176.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 10:24:17'),
(29, 68, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 188.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 10:25:54'),
(30, 69, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 200.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 10:27:36'),
(31, 71, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 212.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 10:46:00'),
(32, 73, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 224.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 10:58:32'),
(33, 74, 12.00, 0.50, 0.00, 12.00, 0.00, 0.00, 236.00, 0.00, 0.00, 9.00, 3.00, '', 0.00, '', 0.00, 0.00, 0.00, 'successful', 'complete', '2018-01-12 11:23:14');

-- --------------------------------------------------------

--
-- Table structure for table `ride_location`
--

CREATE TABLE `ride_location` (
  `id` int(12) NOT NULL,
  `ride_id` int(12) DEFAULT '0',
  `driver_id` int(12) DEFAULT '0',
  `lattitue` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ride_location`
--

INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(1, 1, 1, '28.443353', '77.055841', '2018-01-05 13:59:13'),
(2, 1, 1, '28.443353', '77.055841', '2018-01-05 13:59:18'),
(3, 1, 1, '28.443353', '77.055841', '2018-01-05 13:59:23'),
(4, 1, 1, '28.443353', '77.055841', '2018-01-05 13:59:28'),
(5, 5, 1, '28.443686', '77.055684', '2018-01-05 14:50:21'),
(6, 6, 4, '28.4092019', '77.0342792', '2018-01-07 06:28:20'),
(7, 6, 4, '28.4092019', '77.0342792', '2018-01-07 06:28:23'),
(8, 6, 4, '28.4092019', '77.0342792', '2018-01-07 06:28:26'),
(9, 6, 4, '28.4092019', '77.0342792', '2018-01-07 06:28:29'),
(10, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:32'),
(11, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:35'),
(12, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:38'),
(13, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:41'),
(14, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:44'),
(15, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:47'),
(16, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:50'),
(17, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:53'),
(18, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:56'),
(19, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:28:59'),
(20, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:02'),
(21, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:05'),
(22, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:08'),
(23, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:11'),
(24, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:14'),
(25, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:17'),
(26, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:20'),
(27, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:23'),
(28, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:26'),
(29, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:29'),
(30, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:32'),
(31, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:35'),
(32, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:38'),
(33, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:41'),
(34, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:44'),
(35, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:47'),
(36, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:50'),
(37, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:53'),
(38, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:56'),
(39, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:29:59'),
(40, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:02'),
(41, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:05'),
(42, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:08'),
(43, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:11'),
(44, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:14'),
(45, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:17'),
(46, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:20'),
(47, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:23'),
(48, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:26'),
(49, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:29'),
(50, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:32'),
(51, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:35'),
(52, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:38'),
(53, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:41'),
(54, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:44'),
(55, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:47'),
(56, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:50'),
(57, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:53'),
(58, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:56'),
(59, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:30:59'),
(60, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:02'),
(61, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:05'),
(62, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:08'),
(63, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:11'),
(64, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:14'),
(65, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:17'),
(66, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:20'),
(67, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:23'),
(68, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:26'),
(69, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:29'),
(70, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:32'),
(71, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:35'),
(72, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:38'),
(73, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:41'),
(74, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:44'),
(75, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:47'),
(76, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:50'),
(77, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:53'),
(78, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:56'),
(79, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:31:59'),
(80, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:02'),
(81, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:05'),
(82, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:08'),
(83, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:11'),
(84, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:14'),
(85, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:17'),
(86, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:20'),
(87, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:23'),
(88, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:26'),
(89, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:29'),
(90, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:32'),
(91, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:35'),
(92, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:38'),
(93, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:41'),
(94, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:44'),
(95, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:47'),
(96, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:50'),
(97, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:53'),
(98, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:56'),
(99, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:32:59'),
(100, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:02'),
(101, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:05'),
(102, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:08'),
(103, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:11'),
(104, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:14'),
(105, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:17'),
(106, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:20'),
(107, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:23'),
(108, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:26'),
(109, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:29'),
(110, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:32'),
(111, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:35'),
(112, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:38'),
(113, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:41'),
(114, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:44'),
(115, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:47'),
(116, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:50'),
(117, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:53'),
(118, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:56'),
(119, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:33:59'),
(120, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:02'),
(121, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:05'),
(122, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:08'),
(123, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:11'),
(124, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:14'),
(125, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:17'),
(126, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:20'),
(127, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:23'),
(128, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:26'),
(129, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:29'),
(130, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:32'),
(131, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:35'),
(132, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:39'),
(133, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:41'),
(134, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:44'),
(135, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:47'),
(136, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:50'),
(137, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:53'),
(138, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:56'),
(139, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:34:59'),
(140, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:02'),
(141, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:05'),
(142, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:08'),
(143, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:11'),
(144, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:14'),
(145, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:17'),
(146, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:20'),
(147, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:23'),
(148, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:26'),
(149, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:29'),
(150, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:32'),
(151, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:35'),
(152, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:38'),
(153, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:41'),
(154, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:44'),
(155, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:47'),
(156, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:50'),
(157, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:53'),
(158, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:35:56'),
(159, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:00'),
(160, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:02'),
(161, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:05'),
(162, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:08'),
(163, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:11'),
(164, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:14'),
(165, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:18'),
(166, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:21'),
(167, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:24'),
(168, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:27'),
(169, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:30'),
(170, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:33'),
(171, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:36'),
(172, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:39'),
(173, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:42'),
(174, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:45'),
(175, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:48'),
(176, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:51'),
(177, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:54'),
(178, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:36:57'),
(179, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:00'),
(180, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:03'),
(181, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:06'),
(182, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:09'),
(183, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:12'),
(184, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:15'),
(185, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:18'),
(186, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:21'),
(187, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:24'),
(188, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:27'),
(189, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:30'),
(190, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:33'),
(191, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:36'),
(192, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:39'),
(193, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:42'),
(194, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:45'),
(195, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:48'),
(196, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:51'),
(197, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:54'),
(198, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:37:57'),
(199, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:00'),
(200, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:03'),
(201, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:06'),
(202, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:09'),
(203, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:12'),
(204, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:15'),
(205, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:18'),
(206, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:21'),
(207, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:24'),
(208, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:27'),
(209, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:30'),
(210, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:33'),
(211, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:36'),
(212, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:39'),
(213, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:42'),
(214, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:45'),
(215, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:48'),
(216, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:51'),
(217, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:54'),
(218, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:38:57'),
(219, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:00'),
(220, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:03'),
(221, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:06'),
(222, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:09'),
(223, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:12'),
(224, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:15'),
(225, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:18'),
(226, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:21'),
(227, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:24'),
(228, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:27'),
(229, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:30'),
(230, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:33'),
(231, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:36'),
(232, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:39'),
(233, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:42'),
(234, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:45'),
(235, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:48'),
(236, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:51'),
(237, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:54'),
(238, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:39:57'),
(239, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:00'),
(240, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:03'),
(241, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:06'),
(242, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:09'),
(243, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:12'),
(244, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:15'),
(245, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:18'),
(246, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:21'),
(247, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:24'),
(248, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:27'),
(249, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:30'),
(250, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:33'),
(251, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:36'),
(252, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:39'),
(253, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:42'),
(254, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:45'),
(255, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:48'),
(256, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:51'),
(257, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:54'),
(258, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:40:57'),
(259, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:00'),
(260, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:03'),
(261, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:06'),
(262, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:09'),
(263, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:12'),
(264, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:15'),
(265, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:18'),
(266, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:21'),
(267, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:24'),
(268, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:27'),
(269, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:30'),
(270, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:33'),
(271, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:36'),
(272, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:39'),
(273, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:42'),
(274, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:45'),
(275, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:48'),
(276, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:51'),
(277, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:54'),
(278, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:41:57'),
(279, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:00'),
(280, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:03'),
(281, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:06'),
(282, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:09'),
(283, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:12'),
(284, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:15'),
(285, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:18'),
(286, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:21'),
(287, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:24'),
(288, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:27'),
(289, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:30'),
(290, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:33'),
(291, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:36'),
(292, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:39'),
(293, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:42'),
(294, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:45'),
(295, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:48'),
(296, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:51'),
(297, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:54'),
(298, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:42:57'),
(299, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:00'),
(300, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:03'),
(301, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:06'),
(302, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:09'),
(303, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:12'),
(304, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:15'),
(305, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:18'),
(306, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:21'),
(307, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:24'),
(308, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:27'),
(309, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:30'),
(310, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:33'),
(311, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:36'),
(312, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:39'),
(313, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:42'),
(314, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:45'),
(315, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:48'),
(316, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:51'),
(317, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:54'),
(318, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:43:57'),
(319, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:00'),
(320, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:03'),
(321, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:06'),
(322, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:09'),
(323, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:12'),
(324, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:15'),
(325, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:18'),
(326, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:21'),
(327, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:24'),
(328, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:27'),
(329, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:30'),
(330, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:33'),
(331, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:36'),
(332, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:39'),
(333, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:42'),
(334, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:45'),
(335, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:48'),
(336, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:51'),
(337, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:54'),
(338, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:44:57'),
(339, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:00'),
(340, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:03'),
(341, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:06'),
(342, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:09'),
(343, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:12'),
(344, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:15'),
(345, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:18'),
(346, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:21'),
(347, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:24'),
(348, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:27'),
(349, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:30'),
(350, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:33'),
(351, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:36'),
(352, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:39'),
(353, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:42'),
(354, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:45'),
(355, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:48'),
(356, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:51'),
(357, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:54'),
(358, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:45:57'),
(359, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:00'),
(360, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:03'),
(361, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:06'),
(362, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:09'),
(363, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:12'),
(364, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:15'),
(365, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:18'),
(366, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:21'),
(367, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:24'),
(368, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:27'),
(369, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:30'),
(370, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:33'),
(371, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:36'),
(372, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:39'),
(373, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:42'),
(374, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:45'),
(375, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:48'),
(376, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:51'),
(377, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:54'),
(378, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:46:57'),
(379, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:00'),
(380, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:03'),
(381, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:06'),
(382, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:09'),
(383, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:12'),
(384, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:15'),
(385, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:18'),
(386, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:21'),
(387, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:24'),
(388, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:27'),
(389, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:30'),
(390, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:33'),
(391, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:36'),
(392, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:39'),
(393, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:42'),
(394, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:45'),
(395, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:48'),
(396, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:52'),
(397, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:54'),
(398, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:47:57'),
(399, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:00'),
(400, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:03'),
(401, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:06'),
(402, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:09'),
(403, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:12'),
(404, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:15'),
(405, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:18'),
(406, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:21'),
(407, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:24'),
(408, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:27'),
(409, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:30'),
(410, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:33'),
(411, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:36'),
(412, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:40'),
(413, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:42'),
(414, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:45'),
(415, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:48'),
(416, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:51'),
(417, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:55'),
(418, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:48:57'),
(419, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:00'),
(420, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:03'),
(421, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:06'),
(422, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:09'),
(423, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:12'),
(424, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:15'),
(425, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:18'),
(426, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:21'),
(427, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:24'),
(428, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:27'),
(429, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:31'),
(430, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:34'),
(431, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:37'),
(432, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:40'),
(433, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:43'),
(434, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:46'),
(435, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:49'),
(436, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:52'),
(437, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:55'),
(438, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:49:58'),
(439, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:01'),
(440, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:04'),
(441, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:07'),
(442, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:10'),
(443, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:13'),
(444, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:16'),
(445, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:19'),
(446, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:22'),
(447, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:25'),
(448, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:28'),
(449, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:31'),
(450, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:34'),
(451, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:37'),
(452, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:40'),
(453, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:43'),
(454, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:46'),
(455, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:49'),
(456, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:52'),
(457, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:55'),
(458, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:50:58'),
(459, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:01'),
(460, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:04'),
(461, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:07'),
(462, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:10'),
(463, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:13'),
(464, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:16'),
(465, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:19'),
(466, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:22'),
(467, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:25'),
(468, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:28'),
(469, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:31'),
(470, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:34'),
(471, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:37'),
(472, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:40'),
(473, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:43'),
(474, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:46'),
(475, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:49'),
(476, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:52'),
(477, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:55'),
(478, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:51:58'),
(479, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:01'),
(480, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:04'),
(481, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:07'),
(482, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:10'),
(483, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:13'),
(484, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:16'),
(485, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:19'),
(486, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:22'),
(487, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:25'),
(488, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:28'),
(489, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:31'),
(490, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:34'),
(491, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:37'),
(492, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:40'),
(493, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:43'),
(494, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:46'),
(495, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:49'),
(496, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:52'),
(497, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:55'),
(498, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:52:58'),
(499, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:01'),
(500, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:04'),
(501, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:07'),
(502, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:10'),
(503, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:13'),
(504, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:16'),
(505, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:19'),
(506, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:22'),
(507, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:25'),
(508, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:28'),
(509, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:31'),
(510, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:34'),
(511, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:37'),
(512, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:40'),
(513, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:43'),
(514, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:46'),
(515, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:49'),
(516, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:52'),
(517, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:55'),
(518, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:53:58'),
(519, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:01'),
(520, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:04'),
(521, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:07'),
(522, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:10'),
(523, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:13'),
(524, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:16'),
(525, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:19'),
(526, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:22'),
(527, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:25'),
(528, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:28'),
(529, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:31'),
(530, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:34'),
(531, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:37'),
(532, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:40'),
(533, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:43'),
(534, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:46'),
(535, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:49'),
(536, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:52'),
(537, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:55'),
(538, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:54:58'),
(539, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:01'),
(540, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:04'),
(541, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:07'),
(542, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:10'),
(543, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:13'),
(544, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:16'),
(545, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:19'),
(546, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:22'),
(547, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:25'),
(548, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:28'),
(549, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:31'),
(550, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:34'),
(551, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:37'),
(552, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:40'),
(553, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:43'),
(554, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:46'),
(555, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:49'),
(556, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:52'),
(557, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:55'),
(558, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:55:58'),
(559, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:01'),
(560, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:04'),
(561, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:07'),
(562, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:10'),
(563, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:13'),
(564, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:16'),
(565, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:19'),
(566, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:22'),
(567, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:25'),
(568, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:28'),
(569, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:31'),
(570, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:34'),
(571, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:37'),
(572, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:40'),
(573, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:43'),
(574, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:46'),
(575, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:49'),
(576, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:52'),
(577, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:55'),
(578, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:56:58'),
(579, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:01'),
(580, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:04'),
(581, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:07'),
(582, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:10'),
(583, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:13'),
(584, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:16'),
(585, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:19'),
(586, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:22'),
(587, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:25'),
(588, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:28'),
(589, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:31'),
(590, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:34'),
(591, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:37'),
(592, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:40'),
(593, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:43'),
(594, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:46'),
(595, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:49'),
(596, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:52'),
(597, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:55'),
(598, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:57:58'),
(599, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:01'),
(600, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:04'),
(601, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:07'),
(602, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:10'),
(603, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:13'),
(604, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:16'),
(605, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:19'),
(606, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:22'),
(607, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:25'),
(608, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:28'),
(609, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:31'),
(610, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:34'),
(611, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:37'),
(612, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:40'),
(613, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:43'),
(614, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:46'),
(615, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:49'),
(616, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:52'),
(617, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:55'),
(618, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:58:58'),
(619, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:01'),
(620, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:04'),
(621, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:07'),
(622, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:10'),
(623, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:13'),
(624, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:16'),
(625, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:19'),
(626, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:22'),
(627, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:25'),
(628, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:28'),
(629, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:31'),
(630, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:34'),
(631, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:37'),
(632, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:40'),
(633, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:43'),
(634, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:46'),
(635, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:49'),
(636, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:52'),
(637, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:55'),
(638, 6, 4, '28.409325', '77.0342542', '2018-01-07 06:59:58'),
(639, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:01'),
(640, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:04'),
(641, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:07'),
(642, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:10'),
(643, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:13'),
(644, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:16'),
(645, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:19'),
(646, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:22'),
(647, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:25'),
(648, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:28'),
(649, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:31'),
(650, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:34'),
(651, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:37'),
(652, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:40'),
(653, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:43'),
(654, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:46'),
(655, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:49'),
(656, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:52'),
(657, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:55'),
(658, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:00:58'),
(659, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:01'),
(660, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:04'),
(661, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:07'),
(662, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:10'),
(663, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:13'),
(664, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:16'),
(665, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:19'),
(666, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:22'),
(667, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:25'),
(668, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:28'),
(669, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:31'),
(670, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:34'),
(671, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:37'),
(672, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:40'),
(673, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:43'),
(674, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:46'),
(675, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:49'),
(676, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:52'),
(677, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:55'),
(678, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:01:59'),
(679, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:01'),
(680, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:04'),
(681, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:07'),
(682, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:10'),
(683, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:14'),
(684, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:16'),
(685, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:19'),
(686, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:22'),
(687, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:25'),
(688, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:28'),
(689, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:31'),
(690, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:34'),
(691, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:38'),
(692, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:41'),
(693, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:44'),
(694, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:47'),
(695, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:50'),
(696, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:53'),
(697, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:56'),
(698, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:02:59'),
(699, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:02'),
(700, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:05'),
(701, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:08'),
(702, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:11'),
(703, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:14'),
(704, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:17'),
(705, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:20'),
(706, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:23'),
(707, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:26'),
(708, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:29'),
(709, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:32'),
(710, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:35'),
(711, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:38'),
(712, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:41'),
(713, 6, 4, '28.409325', '77.0342542', '2018-01-07 07:03:44'),
(714, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:36:22'),
(715, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:36:25'),
(716, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:36:28'),
(717, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:36:31'),
(718, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:36:34'),
(719, 10, 6, '28.4433317', '77.0557823', '2018-01-08 09:36:37'),
(720, 10, 6, '28.4433317', '77.0557823', '2018-01-08 09:36:40'),
(721, 10, 6, '28.4433317', '77.0557823', '2018-01-08 09:36:43'),
(722, 10, 6, '28.4433317', '77.0557823', '2018-01-08 09:36:46'),
(723, 10, 6, '28.4433317', '77.0557823', '2018-01-08 09:36:49'),
(724, 10, 6, '28.4433317', '77.0557823', '2018-01-08 09:36:52'),
(725, 10, 6, '28.4432527', '77.05581', '2018-01-08 09:36:55'),
(726, 10, 6, '28.4432527', '77.05581', '2018-01-08 09:36:58'),
(727, 10, 6, '28.4432527', '77.05581', '2018-01-08 09:37:01'),
(728, 10, 6, '28.4432527', '77.05581', '2018-01-08 09:37:04'),
(729, 10, 6, '28.4432527', '77.05581', '2018-01-08 09:37:10'),
(730, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:37:13'),
(731, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:37:16'),
(732, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:37:19'),
(733, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:37:22'),
(734, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:37:25'),
(735, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:37:28'),
(736, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:37:31'),
(737, 10, 6, '28.4431419', '77.0558376', '2018-01-08 09:37:34'),
(738, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:37:37'),
(739, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:37:40'),
(740, 10, 6, '28.443328', '77.0557805', '2018-01-08 09:37:43'),
(741, 10, 6, '28.443328', '77.0557805', '2018-01-08 09:37:46'),
(742, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:37:49'),
(743, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:37:52'),
(744, 10, 6, '28.4432781', '77.0558435', '2018-01-08 09:37:55'),
(745, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:37:58'),
(746, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:01'),
(747, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:04'),
(748, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:07'),
(749, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:10'),
(750, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:13'),
(751, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:16'),
(752, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:19'),
(753, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:22'),
(754, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:25'),
(755, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:28'),
(756, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:31'),
(757, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:34'),
(758, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:37'),
(759, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:40'),
(760, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:43'),
(761, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:46'),
(762, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:49'),
(763, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:52'),
(764, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:55'),
(765, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:38:58'),
(766, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:01'),
(767, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:04'),
(768, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:07'),
(769, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:10'),
(770, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:13'),
(771, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:16'),
(772, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:19'),
(773, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:22'),
(774, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:25'),
(775, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:28'),
(776, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:31'),
(777, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:34'),
(778, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:37'),
(779, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:40'),
(780, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:43'),
(781, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:46'),
(782, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:49'),
(783, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:52'),
(784, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:55'),
(785, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:39:58'),
(786, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:01'),
(787, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:04'),
(788, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:07'),
(789, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:10'),
(790, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:13'),
(791, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:16'),
(792, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:19'),
(793, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:22'),
(794, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:25'),
(795, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:28'),
(796, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:31'),
(797, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:34'),
(798, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:37'),
(799, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:40'),
(800, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:43'),
(801, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:46'),
(802, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:49'),
(803, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:52'),
(804, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:55'),
(805, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:40:58'),
(806, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:01'),
(807, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:04'),
(808, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:07'),
(809, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:10'),
(810, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:13'),
(811, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:16'),
(812, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:19'),
(813, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:22'),
(814, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:25'),
(815, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:28'),
(816, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:31');
INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(817, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:34'),
(818, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:37'),
(819, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:40'),
(820, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:43'),
(821, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:46'),
(822, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:49'),
(823, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:52'),
(824, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:55'),
(825, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:41:58'),
(826, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:01'),
(827, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:04'),
(828, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:07'),
(829, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:10'),
(830, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:13'),
(831, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:16'),
(832, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:19'),
(833, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:22'),
(834, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:25'),
(835, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:28'),
(836, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:31'),
(837, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:34'),
(838, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:37'),
(839, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:40'),
(840, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:43'),
(841, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:46'),
(842, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:49'),
(843, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:52'),
(844, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:55'),
(845, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:42:58'),
(846, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:01'),
(847, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:04'),
(848, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:07'),
(849, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:10'),
(850, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:13'),
(851, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:16'),
(852, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:19'),
(853, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:22'),
(854, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:25'),
(855, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:28'),
(856, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:31'),
(857, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:34'),
(858, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:37'),
(859, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:41'),
(860, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:43'),
(861, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:46'),
(862, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:49'),
(863, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:52'),
(864, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:55'),
(865, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:43:58'),
(866, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:01'),
(867, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:04'),
(868, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:07'),
(869, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:10'),
(870, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:13'),
(871, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:16'),
(872, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:19'),
(873, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:22'),
(874, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:25'),
(875, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:28'),
(876, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:31'),
(877, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:34'),
(878, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:37'),
(879, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:40'),
(880, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:43'),
(881, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:46'),
(882, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:49'),
(883, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:52'),
(884, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:55'),
(885, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:44:58'),
(886, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:01'),
(887, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:04'),
(888, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:07'),
(889, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:10'),
(890, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:13'),
(891, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:16'),
(892, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:19'),
(893, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:22'),
(894, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:25'),
(895, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:28'),
(896, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:31'),
(897, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:34'),
(898, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:37'),
(899, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:40'),
(900, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:43'),
(901, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:46'),
(902, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:49'),
(903, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:52'),
(904, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:55'),
(905, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:45:58'),
(906, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:01'),
(907, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:04'),
(908, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:07'),
(909, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:10'),
(910, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:13'),
(911, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:16'),
(912, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:19'),
(913, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:22'),
(914, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:25'),
(915, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:28'),
(916, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:31'),
(917, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:34'),
(918, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:37'),
(919, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:40'),
(920, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:43'),
(921, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:46'),
(922, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:49'),
(923, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:52'),
(924, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:55'),
(925, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:46:58'),
(926, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:01'),
(927, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:04'),
(928, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:07'),
(929, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:10'),
(930, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:13'),
(931, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:16'),
(932, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:19'),
(933, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:22'),
(934, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:25'),
(935, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:29'),
(936, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:31'),
(937, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:34'),
(938, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:37'),
(939, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:40'),
(940, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:43'),
(941, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:47'),
(942, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:50'),
(943, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:53'),
(944, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:56'),
(945, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:47:59'),
(946, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:02'),
(947, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:05'),
(948, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:08'),
(949, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:11'),
(950, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:14'),
(951, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:17'),
(952, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:20'),
(953, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:23'),
(954, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:26'),
(955, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:29'),
(956, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:32'),
(957, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:35'),
(958, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:38'),
(959, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:41'),
(960, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:44'),
(961, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:47'),
(962, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:50'),
(963, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:53'),
(964, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:56'),
(965, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:48:59'),
(966, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:02'),
(967, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:05'),
(968, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:08'),
(969, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:11'),
(970, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:14'),
(971, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:17'),
(972, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:20'),
(973, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:23'),
(974, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:26'),
(975, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:29'),
(976, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:32'),
(977, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:35'),
(978, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:38'),
(979, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:41'),
(980, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:44'),
(981, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:47'),
(982, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:50'),
(983, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:53'),
(984, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:56'),
(985, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:49:59'),
(986, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:02'),
(987, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:05'),
(988, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:08'),
(989, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:11'),
(990, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:14'),
(991, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:17'),
(992, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:20'),
(993, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:23'),
(994, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:26'),
(995, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:29'),
(996, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:32'),
(997, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:35'),
(998, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:38'),
(999, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:41'),
(1000, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:44'),
(1001, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:47'),
(1002, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:50'),
(1003, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:53'),
(1004, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:56'),
(1005, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:50:59'),
(1006, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:02'),
(1007, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:05'),
(1008, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:08'),
(1009, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:11'),
(1010, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:14'),
(1011, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:17'),
(1012, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:20'),
(1013, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:23'),
(1014, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:26'),
(1015, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:29'),
(1016, 10, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:32'),
(1017, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:35'),
(1018, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:38'),
(1019, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:41'),
(1020, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:44'),
(1021, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:47'),
(1022, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:50'),
(1023, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:53'),
(1024, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:56'),
(1025, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:51:59'),
(1026, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:02'),
(1027, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:05'),
(1028, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:08'),
(1029, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:11'),
(1030, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:14'),
(1031, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:17'),
(1032, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:20'),
(1033, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:23'),
(1034, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:26'),
(1035, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:29'),
(1036, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:32'),
(1037, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:35'),
(1038, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:38'),
(1039, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:41'),
(1040, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:44'),
(1041, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:47'),
(1042, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:50'),
(1043, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:53'),
(1044, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:56'),
(1045, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:52:59'),
(1046, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:02'),
(1047, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:05'),
(1048, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:08'),
(1049, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:11'),
(1050, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:14'),
(1051, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:17'),
(1052, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:20'),
(1053, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:23'),
(1054, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:26'),
(1055, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:29'),
(1056, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:32'),
(1057, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:35'),
(1058, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:38'),
(1059, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:41'),
(1060, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:44'),
(1061, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:47'),
(1062, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:50'),
(1063, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:53'),
(1064, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:56'),
(1065, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:53:59'),
(1066, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:02'),
(1067, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:05'),
(1068, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:08'),
(1069, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:11'),
(1070, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:14'),
(1071, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:17'),
(1072, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:20'),
(1073, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:23'),
(1074, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:26'),
(1075, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:29'),
(1076, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:32'),
(1077, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:35'),
(1078, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:38'),
(1079, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:41'),
(1080, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:44'),
(1081, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:47'),
(1082, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:50'),
(1083, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:53'),
(1084, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:56'),
(1085, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:54:59'),
(1086, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:02'),
(1087, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:05'),
(1088, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:08'),
(1089, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:11'),
(1090, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:14'),
(1091, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:17'),
(1092, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:20'),
(1093, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:23'),
(1094, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:26'),
(1095, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:29'),
(1096, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:32'),
(1097, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:35'),
(1098, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:38'),
(1099, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:41'),
(1100, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:44'),
(1101, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:47'),
(1102, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:50'),
(1103, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:53'),
(1104, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:56'),
(1105, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:55:59'),
(1106, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:02'),
(1107, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:05'),
(1108, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:08'),
(1109, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:11'),
(1110, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:14'),
(1111, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:17'),
(1112, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:20'),
(1113, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:23'),
(1114, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:26'),
(1115, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:29'),
(1116, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:32'),
(1117, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:35'),
(1118, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:38'),
(1119, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:41'),
(1120, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:44'),
(1121, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:47'),
(1122, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:50'),
(1123, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:53'),
(1124, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:56'),
(1125, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:56:59'),
(1126, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:02'),
(1127, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:05'),
(1128, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:09'),
(1129, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:11'),
(1130, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:14'),
(1131, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:17'),
(1132, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:20'),
(1133, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:24'),
(1134, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:26'),
(1135, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:30'),
(1136, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:33'),
(1137, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:36'),
(1138, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:39'),
(1139, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:42'),
(1140, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:45'),
(1141, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:48'),
(1142, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:51'),
(1143, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:54'),
(1144, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:57:57'),
(1145, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:00'),
(1146, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:03'),
(1147, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:06'),
(1148, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:09'),
(1149, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:12'),
(1150, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:15'),
(1151, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:18'),
(1152, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:21'),
(1153, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:24'),
(1154, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:27'),
(1155, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:30'),
(1156, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:33'),
(1157, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:36'),
(1158, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:39'),
(1159, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:42'),
(1160, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:45'),
(1161, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:48'),
(1162, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:51'),
(1163, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:54'),
(1164, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:58:57'),
(1165, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:00'),
(1166, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:03'),
(1167, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:06'),
(1168, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:09'),
(1169, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:12'),
(1170, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:15'),
(1171, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:18'),
(1172, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:21'),
(1173, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:24'),
(1174, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:27'),
(1175, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:30'),
(1176, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:33'),
(1177, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:36'),
(1178, 11, 6, '28.4431419', '77.0558376', '2018-01-08 09:59:38'),
(1179, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:39'),
(1180, 11, 6, '28.4431419', '77.0558376', '2018-01-08 09:59:41'),
(1181, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:42'),
(1182, 11, 6, '28.4431419', '77.0558376', '2018-01-08 09:59:44'),
(1183, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:45'),
(1184, 11, 6, '28.4431419', '77.0558376', '2018-01-08 09:59:47'),
(1185, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:48'),
(1186, 11, 6, '28.4431419', '77.0558376', '2018-01-08 09:59:50'),
(1187, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:51'),
(1188, 11, 6, '28.4431419', '77.0558376', '2018-01-08 09:59:53'),
(1189, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:54'),
(1190, 11, 6, '28.4431419', '77.0558376', '2018-01-08 09:59:56'),
(1191, 11, 6, '28.4434082', '77.0557561', '2018-01-08 09:59:57'),
(1192, 11, 6, '28.4443312', '77.056986', '2018-01-08 09:59:59'),
(1193, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:00'),
(1194, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:02'),
(1195, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:03'),
(1196, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:05'),
(1197, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:06'),
(1198, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:08'),
(1199, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:09'),
(1200, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:11'),
(1201, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:12'),
(1202, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:14'),
(1203, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:15'),
(1204, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:17'),
(1205, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:18'),
(1206, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:20'),
(1207, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:21'),
(1208, 11, 6, '28.4431909', '77.0558248', '2018-01-08 10:00:23'),
(1209, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:24'),
(1210, 11, 6, '28.4431909', '77.0558248', '2018-01-08 10:00:26'),
(1211, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:27'),
(1212, 11, 6, '28.4433103', '77.0557903', '2018-01-08 10:00:29'),
(1213, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:30'),
(1214, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:32'),
(1215, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:33'),
(1216, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:35'),
(1217, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:36'),
(1218, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:38'),
(1219, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:39'),
(1220, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:41'),
(1221, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:42'),
(1222, 11, 6, '28.4432322', '77.0558326', '2018-01-08 10:00:44'),
(1223, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:45'),
(1224, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:47'),
(1225, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:48'),
(1226, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:50'),
(1227, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:51'),
(1228, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:53'),
(1229, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:54'),
(1230, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:00:56'),
(1231, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:00:57'),
(1232, 11, 6, '28.4432148', '77.055829', '2018-01-08 10:00:59'),
(1233, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:00'),
(1234, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:02'),
(1235, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:03'),
(1236, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:05'),
(1237, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:06'),
(1238, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:08'),
(1239, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:09'),
(1240, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:11'),
(1241, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:12'),
(1242, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:14'),
(1243, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:15'),
(1244, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:17'),
(1245, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:18'),
(1246, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:20'),
(1247, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:21'),
(1248, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:23'),
(1249, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:24'),
(1250, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:26'),
(1251, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:27'),
(1252, 11, 6, '28.4432065', '77.0558275', '2018-01-08 10:01:29'),
(1253, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:30'),
(1254, 11, 6, '28.4432065', '77.0558275', '2018-01-08 10:01:32'),
(1255, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:33'),
(1256, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:35'),
(1257, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:36'),
(1258, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:38'),
(1259, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:39'),
(1260, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:41'),
(1261, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:42'),
(1262, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:44'),
(1263, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:45'),
(1264, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:47'),
(1265, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:48'),
(1266, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:50'),
(1267, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:51'),
(1268, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:53'),
(1269, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:54'),
(1270, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:56'),
(1271, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:01:57'),
(1272, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:01:59'),
(1273, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:00'),
(1274, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:02'),
(1275, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:03'),
(1276, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:05'),
(1277, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:06'),
(1278, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:08'),
(1279, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:09'),
(1280, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:11'),
(1281, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:12'),
(1282, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:14'),
(1283, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:15'),
(1284, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:17'),
(1285, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:18'),
(1286, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:20'),
(1287, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:21'),
(1288, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:23'),
(1289, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:24'),
(1290, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:26'),
(1291, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:27'),
(1292, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:29'),
(1293, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:30'),
(1294, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:32'),
(1295, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:33'),
(1296, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:35'),
(1297, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:36'),
(1298, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:38'),
(1299, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:39'),
(1300, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:41'),
(1301, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:42'),
(1302, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:44'),
(1303, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:45'),
(1304, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:47'),
(1305, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:48'),
(1306, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:50'),
(1307, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:51'),
(1308, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:53'),
(1309, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:54'),
(1310, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:56'),
(1311, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:02:57'),
(1312, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:02:59'),
(1313, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:00'),
(1314, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:02'),
(1315, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:03'),
(1316, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:05'),
(1317, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:06'),
(1318, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:08'),
(1319, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:09'),
(1320, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:12'),
(1321, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:12'),
(1322, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:14'),
(1323, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:15'),
(1324, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:17'),
(1325, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:18'),
(1326, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:20'),
(1327, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:21'),
(1328, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:23'),
(1329, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:24'),
(1330, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:26'),
(1331, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:27'),
(1332, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:29'),
(1333, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:30'),
(1334, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:32'),
(1335, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:33'),
(1336, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:35'),
(1337, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:36'),
(1338, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:38'),
(1339, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:39'),
(1340, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:41'),
(1341, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:42'),
(1342, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:44'),
(1343, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:45'),
(1344, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:47'),
(1345, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:48'),
(1346, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:50'),
(1347, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:51'),
(1348, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:53'),
(1349, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:54'),
(1350, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:56'),
(1351, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:03:57'),
(1352, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:03:59'),
(1353, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:00'),
(1354, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:02'),
(1355, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:03'),
(1356, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:05'),
(1357, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:06'),
(1358, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:08'),
(1359, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:09'),
(1360, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:11'),
(1361, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:12'),
(1362, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:14'),
(1363, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:15'),
(1364, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:17'),
(1365, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:18'),
(1366, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:20'),
(1367, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:21'),
(1368, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:23'),
(1369, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:24'),
(1370, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:26'),
(1371, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:27'),
(1372, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:29'),
(1373, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:30'),
(1374, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:32'),
(1375, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:33'),
(1376, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:35'),
(1377, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:36'),
(1378, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:38'),
(1379, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:39'),
(1380, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:41'),
(1381, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:42'),
(1382, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:44'),
(1383, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:45'),
(1384, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:47'),
(1385, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:48'),
(1386, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:50'),
(1387, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:51'),
(1388, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:53'),
(1389, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:54'),
(1390, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:56'),
(1391, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:04:57'),
(1392, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:04:59'),
(1393, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:00'),
(1394, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:02'),
(1395, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:03'),
(1396, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:05'),
(1397, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:06'),
(1398, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:08'),
(1399, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:09'),
(1400, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:11'),
(1401, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:12'),
(1402, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:14'),
(1403, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:15'),
(1404, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:17'),
(1405, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:18'),
(1406, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:20'),
(1407, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:21'),
(1408, 11, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:23'),
(1409, 11, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:24'),
(1410, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:26'),
(1411, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:27'),
(1412, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:29'),
(1413, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:30'),
(1414, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:32'),
(1415, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:33'),
(1416, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:35'),
(1417, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:36'),
(1418, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:38'),
(1419, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:39'),
(1420, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:41'),
(1421, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:42'),
(1422, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:44'),
(1423, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:45'),
(1424, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:47'),
(1425, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:48'),
(1426, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:50'),
(1427, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:51'),
(1428, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:53'),
(1429, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:54'),
(1430, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:56'),
(1431, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:05:57'),
(1432, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:05:59'),
(1433, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:00'),
(1434, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:02'),
(1435, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:03'),
(1436, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:05'),
(1437, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:06'),
(1438, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:08'),
(1439, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:09'),
(1440, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:10'),
(1441, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:11'),
(1442, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:12'),
(1443, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:13'),
(1444, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:14'),
(1445, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:15'),
(1446, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:16'),
(1447, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:17'),
(1448, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:18'),
(1449, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:19'),
(1450, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:20'),
(1451, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:21'),
(1452, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:22'),
(1453, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:23'),
(1454, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:24'),
(1455, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:25'),
(1456, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:26'),
(1457, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:27'),
(1458, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:28'),
(1459, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:29'),
(1460, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:30'),
(1461, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:31'),
(1462, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:32'),
(1463, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:33'),
(1464, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:34'),
(1465, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:35'),
(1466, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:36'),
(1467, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:37'),
(1468, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:38'),
(1469, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:39'),
(1470, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:40'),
(1471, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:41'),
(1472, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:42'),
(1473, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:43'),
(1474, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:44'),
(1475, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:45'),
(1476, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:46'),
(1477, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:47'),
(1478, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:48'),
(1479, 12, 6, '28.4431837', '77.0558237', '2018-01-08 10:06:49'),
(1480, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:50'),
(1481, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:51'),
(1482, 12, 6, '28.4431837', '77.0558237', '2018-01-08 10:06:52'),
(1483, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:53'),
(1484, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:54'),
(1485, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:55'),
(1486, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:56'),
(1487, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:06:57'),
(1488, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:58'),
(1489, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:06:59'),
(1490, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:00'),
(1491, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:01'),
(1492, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:02'),
(1493, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:03'),
(1494, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:04'),
(1495, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:05'),
(1496, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:06'),
(1497, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:07'),
(1498, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:08'),
(1499, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:09'),
(1500, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:10'),
(1501, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:11'),
(1502, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:12'),
(1503, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:13'),
(1504, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:14'),
(1505, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:15'),
(1506, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:16'),
(1507, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:17'),
(1508, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:18'),
(1509, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:19'),
(1510, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:20'),
(1511, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:21'),
(1512, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:22'),
(1513, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:23'),
(1514, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:24'),
(1515, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:25'),
(1516, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:26'),
(1517, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:27'),
(1518, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:28'),
(1519, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:29'),
(1520, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:30'),
(1521, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:31'),
(1522, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:32'),
(1523, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:33'),
(1524, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:34'),
(1525, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:35'),
(1526, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:36'),
(1527, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:37'),
(1528, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:38'),
(1529, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:39'),
(1530, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:40'),
(1531, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:41'),
(1532, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:42'),
(1533, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:43'),
(1534, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:44'),
(1535, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:45'),
(1536, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:46'),
(1537, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:47'),
(1538, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:48'),
(1539, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:49'),
(1540, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:50'),
(1541, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:51'),
(1542, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:52'),
(1543, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:54'),
(1544, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:54'),
(1545, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:55'),
(1546, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:56'),
(1547, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:07:57'),
(1548, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:07:58'),
(1549, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:07:59'),
(1550, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:00'),
(1551, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:01'),
(1552, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:03'),
(1553, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:03'),
(1554, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:04'),
(1555, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:05'),
(1556, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:06'),
(1557, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:07'),
(1558, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:08'),
(1559, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:09'),
(1560, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:10'),
(1561, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:11'),
(1562, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:12'),
(1563, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:13'),
(1564, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:14'),
(1565, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:15'),
(1566, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:16'),
(1567, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:17'),
(1568, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:18'),
(1569, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:19'),
(1570, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:20'),
(1571, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:22'),
(1572, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:22'),
(1573, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:24'),
(1574, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:24'),
(1575, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:25'),
(1576, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:26'),
(1577, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:27'),
(1578, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:28'),
(1579, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:30'),
(1580, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:30'),
(1581, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:31'),
(1582, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:33'),
(1583, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:33'),
(1584, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:34'),
(1585, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:36'),
(1586, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:36'),
(1587, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:37'),
(1588, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:38'),
(1589, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:39'),
(1590, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:40'),
(1591, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:41'),
(1592, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:42'),
(1593, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:43'),
(1594, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:44'),
(1595, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:45'),
(1596, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:46'),
(1597, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:47'),
(1598, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:48');
INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(1599, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:49'),
(1600, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:50'),
(1601, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:51'),
(1602, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:52'),
(1603, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:53'),
(1604, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:54'),
(1605, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:55'),
(1606, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:56'),
(1607, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:08:57'),
(1608, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:08:58'),
(1609, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:08:59'),
(1610, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:00'),
(1611, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:01'),
(1612, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:02'),
(1613, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:03'),
(1614, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:04'),
(1615, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:05'),
(1616, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:06'),
(1617, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:07'),
(1618, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:08'),
(1619, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:09'),
(1620, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:10'),
(1621, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:11'),
(1622, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:12'),
(1623, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:13'),
(1624, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:14'),
(1625, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:15'),
(1626, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:16'),
(1627, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:17'),
(1628, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:18'),
(1629, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:19'),
(1630, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:20'),
(1631, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:21'),
(1632, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:22'),
(1633, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:23'),
(1634, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:24'),
(1635, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:25'),
(1636, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:26'),
(1637, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:27'),
(1638, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:28'),
(1639, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:29'),
(1640, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:30'),
(1641, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:31'),
(1642, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:32'),
(1643, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:33'),
(1644, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:34'),
(1645, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:35'),
(1646, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:36'),
(1647, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:37'),
(1648, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:38'),
(1649, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:39'),
(1650, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:40'),
(1651, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:41'),
(1652, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:42'),
(1653, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:43'),
(1654, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:44'),
(1655, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:45'),
(1656, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:46'),
(1657, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:47'),
(1658, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:48'),
(1659, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:49'),
(1660, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:50'),
(1661, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:51'),
(1662, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:52'),
(1663, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:53'),
(1664, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:54'),
(1665, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:55'),
(1666, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:56'),
(1667, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:09:57'),
(1668, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:09:58'),
(1669, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:09:59'),
(1670, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:00'),
(1671, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:01'),
(1672, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:02'),
(1673, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:03'),
(1674, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:04'),
(1675, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:05'),
(1676, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:06'),
(1677, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:07'),
(1678, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:08'),
(1679, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:09'),
(1680, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:10'),
(1681, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:11'),
(1682, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:12'),
(1683, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:13'),
(1684, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:14'),
(1685, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:15'),
(1686, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:16'),
(1687, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:17'),
(1688, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:18'),
(1689, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:19'),
(1690, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:20'),
(1691, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:21'),
(1692, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:22'),
(1693, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:23'),
(1694, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:24'),
(1695, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:25'),
(1696, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:26'),
(1697, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:27'),
(1698, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:28'),
(1699, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:29'),
(1700, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:30'),
(1701, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:31'),
(1702, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:33'),
(1703, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:33'),
(1704, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:34'),
(1705, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:36'),
(1706, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:36'),
(1707, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:37'),
(1708, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:39'),
(1709, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:39'),
(1710, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:40'),
(1711, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:42'),
(1712, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:42'),
(1713, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:43'),
(1714, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:45'),
(1715, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:45'),
(1716, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:46'),
(1717, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:48'),
(1718, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:48'),
(1719, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:49'),
(1720, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:51'),
(1721, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:51'),
(1722, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:52'),
(1723, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:54'),
(1724, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:54'),
(1725, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:55'),
(1726, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:10:57'),
(1727, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:10:57'),
(1728, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:10:58'),
(1729, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:11:00'),
(1730, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:11:00'),
(1731, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:11:01'),
(1732, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:11:03'),
(1733, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:11:03'),
(1734, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:11:04'),
(1735, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:11:06'),
(1736, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:11:06'),
(1737, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:11:07'),
(1738, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:11:09'),
(1739, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:11:09'),
(1740, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:11:10'),
(1741, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:11:12'),
(1742, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:11:12'),
(1743, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:11:52'),
(1744, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:11:52'),
(1745, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:11:53'),
(1746, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:11:53'),
(1747, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:11:56'),
(1748, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:11:58'),
(1749, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:11:59'),
(1750, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:11:59'),
(1751, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:11:59'),
(1752, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:00'),
(1753, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:00'),
(1754, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:00'),
(1755, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:04'),
(1756, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:05'),
(1757, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:05'),
(1758, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:06'),
(1759, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:07'),
(1760, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:07'),
(1761, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:07'),
(1762, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:08'),
(1763, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:08'),
(1764, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:08'),
(1765, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:13'),
(1766, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:13'),
(1767, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:13'),
(1768, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:14'),
(1769, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:15'),
(1770, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:16'),
(1771, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:54'),
(1772, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:54'),
(1773, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:54'),
(1774, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:55'),
(1775, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:55'),
(1776, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:55'),
(1777, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:56'),
(1778, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:56'),
(1779, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:56'),
(1780, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:57'),
(1781, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:57'),
(1782, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:57'),
(1783, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:58'),
(1784, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:58'),
(1785, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:12:58'),
(1786, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:12:59'),
(1787, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:12:59'),
(1788, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:00'),
(1789, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:00'),
(1790, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:02'),
(1791, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:02'),
(1792, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:03'),
(1793, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:03'),
(1794, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:03'),
(1795, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:08'),
(1796, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:08'),
(1797, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:08'),
(1798, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:09'),
(1799, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:09'),
(1800, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:09'),
(1801, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:10'),
(1802, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:10'),
(1803, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:11'),
(1804, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:11'),
(1805, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:11'),
(1806, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:12'),
(1807, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:12'),
(1808, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:12'),
(1809, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:13'),
(1810, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:13'),
(1811, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:13'),
(1812, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:14'),
(1813, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:14'),
(1814, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:14'),
(1815, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:16'),
(1816, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:13:17'),
(1817, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:13:17'),
(1818, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:13:17'),
(1819, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:01'),
(1820, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:01'),
(1821, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:02'),
(1822, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:02'),
(1823, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:02'),
(1824, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:03'),
(1825, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:03'),
(1826, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:03'),
(1827, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:04'),
(1828, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:04'),
(1829, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:04'),
(1830, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:04'),
(1831, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:05'),
(1832, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:05'),
(1833, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:05'),
(1834, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:05'),
(1835, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:05'),
(1836, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:05'),
(1837, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:06'),
(1838, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:06'),
(1839, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:06'),
(1840, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:06'),
(1841, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:06'),
(1842, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:06'),
(1843, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:07'),
(1844, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:07'),
(1845, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:07'),
(1846, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:07'),
(1847, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:07'),
(1848, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:08'),
(1849, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:08'),
(1850, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:08'),
(1851, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:08'),
(1852, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:08'),
(1853, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:08'),
(1854, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:09'),
(1855, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:09'),
(1856, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:09'),
(1857, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:09'),
(1858, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:09'),
(1859, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:09'),
(1860, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:10'),
(1861, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:10'),
(1862, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:10'),
(1863, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:10'),
(1864, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:10'),
(1865, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:10'),
(1866, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:10'),
(1867, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:10'),
(1868, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:10'),
(1869, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:11'),
(1870, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:11'),
(1871, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:11'),
(1872, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:11'),
(1873, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:11'),
(1874, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:11'),
(1875, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:11'),
(1876, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:12'),
(1877, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:12'),
(1878, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:12'),
(1879, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:12'),
(1880, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:12'),
(1881, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:12'),
(1882, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:12'),
(1883, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:12'),
(1884, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:13'),
(1885, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:13'),
(1886, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:13'),
(1887, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:13'),
(1888, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:13'),
(1889, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:13'),
(1890, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:13'),
(1891, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:14'),
(1892, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:14'),
(1893, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:14'),
(1894, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:14'),
(1895, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:14'),
(1896, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:14'),
(1897, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:14'),
(1898, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:14'),
(1899, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:15'),
(1900, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:15'),
(1901, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:15'),
(1902, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:15'),
(1903, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:15'),
(1904, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:15'),
(1905, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:15'),
(1906, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:15'),
(1907, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:15'),
(1908, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:15'),
(1909, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:16'),
(1910, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:16'),
(1911, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:16'),
(1912, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:16'),
(1913, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:16'),
(1914, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:16'),
(1915, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:16'),
(1916, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:16'),
(1917, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:16'),
(1918, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:16'),
(1919, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:16'),
(1920, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:17'),
(1921, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:17'),
(1922, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:17'),
(1923, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:17'),
(1924, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:17'),
(1925, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:17'),
(1926, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:17'),
(1927, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:18'),
(1928, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:18'),
(1929, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:19'),
(1930, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:21'),
(1931, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:21'),
(1932, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:22'),
(1933, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:24'),
(1934, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:24'),
(1935, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:25'),
(1936, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:27'),
(1937, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:27'),
(1938, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:28'),
(1939, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:30'),
(1940, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:30'),
(1941, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:31'),
(1942, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:33'),
(1943, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:33'),
(1944, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:34'),
(1945, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:36'),
(1946, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:36'),
(1947, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:37'),
(1948, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:39'),
(1949, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:39'),
(1950, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:40'),
(1951, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:42'),
(1952, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:42'),
(1953, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:43'),
(1954, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:45'),
(1955, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:45'),
(1956, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:46'),
(1957, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:48'),
(1958, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:48'),
(1959, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:49'),
(1960, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:51'),
(1961, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:51'),
(1962, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:52'),
(1963, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:54'),
(1964, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:54'),
(1965, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:55'),
(1966, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:14:57'),
(1967, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:14:57'),
(1968, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:14:58'),
(1969, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:00'),
(1970, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:00'),
(1971, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:01'),
(1972, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:03'),
(1973, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:03'),
(1974, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:04'),
(1975, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:06'),
(1976, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:06'),
(1977, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:07'),
(1978, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:09'),
(1979, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:09'),
(1980, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:10'),
(1981, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:12'),
(1982, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:12'),
(1983, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:13'),
(1984, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:15'),
(1985, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:15'),
(1986, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:16'),
(1987, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:18'),
(1988, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:18'),
(1989, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:19'),
(1990, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:21'),
(1991, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:21'),
(1992, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:22'),
(1993, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:24'),
(1994, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:24'),
(1995, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:25'),
(1996, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:27'),
(1997, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:27'),
(1998, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:28'),
(1999, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:30'),
(2000, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:31'),
(2001, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:31'),
(2002, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:33'),
(2003, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:33'),
(2004, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:34'),
(2005, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:36'),
(2006, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:36'),
(2007, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:37'),
(2008, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:39'),
(2009, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:39'),
(2010, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:40'),
(2011, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:42'),
(2012, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:42'),
(2013, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:43'),
(2014, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:45'),
(2015, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:45'),
(2016, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:46'),
(2017, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:48'),
(2018, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:48'),
(2019, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:49'),
(2020, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:51'),
(2021, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:51'),
(2022, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:52'),
(2023, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:54'),
(2024, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:54'),
(2025, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:55'),
(2026, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:15:57'),
(2027, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:15:57'),
(2028, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:15:58'),
(2029, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:00'),
(2030, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:00'),
(2031, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:01'),
(2032, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:03'),
(2033, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:03'),
(2034, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:04'),
(2035, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:06'),
(2036, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:06'),
(2037, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:07'),
(2038, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:09'),
(2039, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:09'),
(2040, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:10'),
(2041, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:12'),
(2042, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:12'),
(2043, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:13'),
(2044, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:15'),
(2045, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:15'),
(2046, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:16'),
(2047, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:18'),
(2048, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:18'),
(2049, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:19'),
(2050, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:21'),
(2051, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:21'),
(2052, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:22'),
(2053, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:24'),
(2054, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:24'),
(2055, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:25'),
(2056, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:27'),
(2057, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:27'),
(2058, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:28'),
(2059, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:30'),
(2060, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:30'),
(2061, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:31'),
(2062, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:33'),
(2063, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:33'),
(2064, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:34'),
(2065, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:36'),
(2066, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:36'),
(2067, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:37'),
(2068, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:39'),
(2069, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:39'),
(2070, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:40'),
(2071, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:42'),
(2072, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:42'),
(2073, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:43'),
(2074, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:45'),
(2075, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:45'),
(2076, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:46'),
(2077, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:48'),
(2078, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:49'),
(2079, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:49'),
(2080, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:51'),
(2081, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:52'),
(2082, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:52'),
(2083, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:54'),
(2084, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:55'),
(2085, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:55'),
(2086, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:16:57'),
(2087, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:16:58'),
(2088, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:16:58'),
(2089, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:00'),
(2090, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:01'),
(2091, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:01'),
(2092, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:03'),
(2093, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:04'),
(2094, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:04'),
(2095, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:06'),
(2096, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:07'),
(2097, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:07'),
(2098, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:09'),
(2099, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:10'),
(2100, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:10'),
(2101, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:12'),
(2102, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:13'),
(2103, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:13'),
(2104, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:15'),
(2105, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:16'),
(2106, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:16'),
(2107, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:18'),
(2108, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:19'),
(2109, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:19'),
(2110, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:21'),
(2111, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:22'),
(2112, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:22'),
(2113, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:24'),
(2114, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:25'),
(2115, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:25'),
(2116, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:27'),
(2117, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:28'),
(2118, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:28'),
(2119, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:30'),
(2120, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:31'),
(2121, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:31'),
(2122, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:33'),
(2123, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:34'),
(2124, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:34'),
(2125, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:36'),
(2126, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:37'),
(2127, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:37'),
(2128, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:39'),
(2129, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:40'),
(2130, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:40'),
(2131, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:42'),
(2132, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:43'),
(2133, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:43'),
(2134, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:45'),
(2135, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:46'),
(2136, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:46'),
(2137, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:48'),
(2138, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:49'),
(2139, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:49'),
(2140, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:51'),
(2141, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:52'),
(2142, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:52'),
(2143, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:54'),
(2144, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:55'),
(2145, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:55'),
(2146, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:17:57'),
(2147, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:17:58'),
(2148, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:17:58'),
(2149, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:00'),
(2150, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:01'),
(2151, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:01'),
(2152, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:03'),
(2153, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:04'),
(2154, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:04'),
(2155, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:06'),
(2156, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:07'),
(2157, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:07'),
(2158, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:09'),
(2159, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:10'),
(2160, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:10'),
(2161, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:12'),
(2162, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:13'),
(2163, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:13'),
(2164, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:15'),
(2165, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:16'),
(2166, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:16'),
(2167, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:18'),
(2168, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:19'),
(2169, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:19'),
(2170, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:21'),
(2171, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:22'),
(2172, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:22'),
(2173, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:24'),
(2174, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:25'),
(2175, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:25'),
(2176, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:27'),
(2177, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:28'),
(2178, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:28'),
(2179, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:30'),
(2180, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:31'),
(2181, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:31'),
(2182, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:33'),
(2183, 12, 6, '28.4434082', '77.0557561', '2018-01-08 10:18:34'),
(2184, 12, 6, '28.4434013', '77.0557769', '2018-01-08 10:18:34'),
(2185, 12, 6, '28.4431419', '77.0558376', '2018-01-08 10:18:36'),
(2186, 13, 1, '28.443469', '77.056013', '2018-01-08 13:36:46'),
(2187, 14, 1, '28.443472', '77.056010', '2018-01-08 13:36:48'),
(2188, 14, 1, '28.443463', '77.055994', '2018-01-08 13:37:12'),
(2189, 14, 1, '28.443465', '77.055993', '2018-01-08 13:37:13'),
(2190, 14, 1, '28.443470', '77.056006', '2018-01-08 13:37:14'),
(2191, 15, 1, '28.443449', '77.056086', '2018-01-08 13:37:59'),
(2192, 15, 1, '28.443451', '77.056081', '2018-01-08 13:37:59'),
(2193, 15, 1, '28.443454', '77.056077', '2018-01-08 13:38:00'),
(2194, 15, 1, '28.443454', '77.056074', '2018-01-08 13:38:14'),
(2195, 15, 1, '28.443457', '77.056067', '2018-01-08 13:38:30'),
(2196, 15, 1, '28.443460', '77.056061', '2018-01-08 13:38:32'),
(2197, 15, 1, '28.443464', '77.056054', '2018-01-08 13:38:42'),
(2198, 15, 1, '28.443467', '77.056048', '2018-01-08 13:38:43'),
(2199, 15, 1, '28.443470', '77.056042', '2018-01-08 13:39:15'),
(2200, 15, 1, '28.443473', '77.056037', '2018-01-08 13:39:30'),
(2201, 15, 1, '28.443475', '77.056038', '2018-01-08 13:39:40'),
(2202, 15, 1, '28.443476', '77.056038', '2018-01-08 13:39:42'),
(2203, 15, 1, '28.443478', '77.056039', '2018-01-08 13:39:57'),
(2204, 15, 1, '28.443479', '77.056040', '2018-01-08 13:40:12'),
(2205, 15, 1, '28.443480', '77.056040', '2018-01-08 13:40:27'),
(2206, 15, 1, '28.443481', '77.056041', '2018-01-08 13:40:42'),
(2207, 15, 1, '28.443451', '77.056085', '2018-01-08 13:40:49'),
(2208, 15, 1, '28.443451', '77.056065', '2018-01-08 13:40:50'),
(2209, 15, 1, '28.443451', '77.056060', '2018-01-08 13:41:06'),
(2210, 15, 1, '28.443451', '77.056058', '2018-01-08 13:41:20'),
(2211, 15, 1, '28.443455', '77.056052', '2018-01-08 13:41:36'),
(2212, 15, 1, '28.443469', '77.056014', '2018-01-08 13:41:38'),
(2213, 15, 1, '28.443471', '77.056010', '2018-01-08 13:41:53'),
(2214, 15, 1, '28.443471', '77.056009', '2018-01-08 13:42:08'),
(2215, 15, 1, '28.443471', '77.056009', '2018-01-08 13:42:23'),
(2216, 15, 1, '28.443471', '77.056009', '2018-01-08 13:42:38'),
(2217, 15, 1, '28.443471', '77.056009', '2018-01-08 13:42:53'),
(2218, 15, 1, '28.443468', '77.056013', '2018-01-08 13:43:14'),
(2219, 15, 1, '28.443466', '77.056014', '2018-01-08 13:43:24'),
(2220, 15, 1, '28.443465', '77.056016', '2018-01-08 13:43:39'),
(2221, 15, 1, '28.443473', '77.056007', '2018-01-08 13:43:47'),
(2222, 15, 1, '28.443470', '77.056011', '2018-01-08 13:43:48'),
(2223, 15, 1, '28.443470', '77.056011', '2018-01-08 13:43:48'),
(2224, 15, 1, '28.443471', '77.056009', '2018-01-08 13:43:50'),
(2225, 15, 1, '28.443471', '77.056009', '2018-01-08 13:43:53'),
(2226, 15, 1, '28.443470', '77.056010', '2018-01-08 13:44:10'),
(2227, 15, 1, '28.443470', '77.056010', '2018-01-08 13:44:22'),
(2228, 15, 1, '28.443470', '77.056010', '2018-01-08 13:44:37'),
(2229, 15, 1, '28.443470', '77.056010', '2018-01-08 13:44:52'),
(2230, 15, 1, '28.443484', '77.056029', '2018-01-08 13:45:07'),
(2231, 15, 1, '28.443488', '77.056032', '2018-01-08 13:45:09'),
(2232, 17, 12, '28.443456', '77.056022', '2018-01-09 11:22:19'),
(2233, 17, 12, '28.443471', '77.056009', '2018-01-09 11:22:24'),
(2234, 17, 12, '28.443487', '77.055990', '2018-01-09 11:22:29'),
(2235, 17, 12, '28.443487', '77.055990', '2018-01-09 11:22:34'),
(2236, 17, 12, '28.443444', '77.055990', '2018-01-09 11:22:39'),
(2237, 20, 1, '28.443494', '77.056020', '2018-01-10 06:55:23'),
(2238, 20, 1, '28.443462', '77.056026', '2018-01-10 06:55:24'),
(2239, 21, 1, '28.443426', '77.056118', '2018-01-10 06:56:12'),
(2240, 21, 1, '28.443439', '77.056255', '2018-01-10 06:56:12'),
(2241, 21, 1, '28.443484', '77.056008', '2018-01-10 06:56:19'),
(2242, 21, 1, '28.443432', '77.055786', '2018-01-10 06:56:37'),
(2243, 22, 1, '28.443397', '77.055832', '2018-01-10 06:56:43'),
(2244, 22, 1, '28.443494', '77.056010', '2018-01-10 06:56:49'),
(2245, 22, 1, '28.443450', '77.055927', '2018-01-10 06:56:55'),
(2246, 22, 1, '28.443433', '77.055880', '2018-01-10 06:57:02'),
(2247, 22, 1, '28.443453', '77.055902', '2018-01-10 06:57:03'),
(2248, 22, 1, '28.443478', '77.055928', '2018-01-10 06:57:10'),
(2249, 22, 1, '28.443493', '77.055954', '2018-01-10 06:57:11'),
(2250, 22, 1, '28.443506', '77.055972', '2018-01-10 06:57:18'),
(2251, 22, 1, '28.443507', '77.055976', '2018-01-10 06:57:20'),
(2252, 23, 1, '28.443509', '77.055980', '2018-01-10 06:57:25'),
(2253, 23, 1, '28.443412', '77.055889', '2018-01-10 06:57:41'),
(2254, 23, 1, '28.443433', '77.055971', '2018-01-10 06:57:43'),
(2255, 23, 1, '28.443433', '77.055972', '2018-01-10 06:57:50'),
(2256, 24, 1, '28.443436', '77.055978', '2018-01-10 06:58:07'),
(2257, 24, 1, '28.443437', '77.055983', '2018-01-10 06:58:22'),
(2258, 24, 1, '28.443439', '77.055987', '2018-01-10 06:58:37'),
(2259, 24, 1, '28.443441', '77.055991', '2018-01-10 06:58:52'),
(2260, 24, 1, '28.443442', '77.055995', '2018-01-10 06:59:07'),
(2261, 24, 1, '28.443442', '77.055995', '2018-01-10 06:59:16'),
(2262, 24, 1, '28.443442', '77.055995', '2018-01-10 06:59:16'),
(2263, 24, 1, '28.443442', '77.055995', '2018-01-10 06:59:19'),
(2264, 24, 1, '28.443443', '77.055998', '2018-01-10 06:59:22'),
(2265, 24, 1, '28.443444', '77.056001', '2018-01-10 06:59:37'),
(2266, 24, 1, '28.443445', '77.056003', '2018-01-10 06:59:52'),
(2267, 24, 1, '28.443446', '77.056006', '2018-01-10 07:00:07'),
(2268, 24, 1, '28.443447', '77.056008', '2018-01-10 07:00:22'),
(2269, 24, 1, '28.443447', '77.056008', '2018-01-10 07:00:33'),
(2270, 24, 1, '28.443447', '77.056008', '2018-01-10 07:00:33'),
(2271, 24, 1, '28.443447', '77.056008', '2018-01-10 07:00:36'),
(2272, 24, 1, '28.443448', '77.056010', '2018-01-10 07:00:37'),
(2273, 24, 1, '28.443448', '77.056011', '2018-01-10 07:00:52'),
(2274, 24, 1, '28.443449', '77.056013', '2018-01-10 07:01:07'),
(2275, 24, 1, '28.443449', '77.056014', '2018-01-10 07:01:22'),
(2276, 24, 1, '28.443450', '77.056015', '2018-01-10 07:01:37'),
(2277, 24, 1, '28.443450', '77.056016', '2018-01-10 07:01:52'),
(2278, 24, 1, '28.443450', '77.056017', '2018-01-10 07:02:01'),
(2279, 24, 1, '28.443444', '77.056000', '2018-01-10 07:02:20'),
(2280, 24, 1, '28.443438', '77.055985', '2018-01-10 07:02:22'),
(2281, 24, 1, '28.443433', '77.055972', '2018-01-10 07:02:36'),
(2282, 24, 1, '28.443429', '77.055960', '2018-01-10 07:02:51'),
(2283, 24, 1, '28.443425', '77.055950', '2018-01-10 07:03:06'),
(2284, 24, 1, '28.443422', '77.055941', '2018-01-10 07:03:21'),
(2285, 24, 1, '28.443419', '77.055933', '2018-01-10 07:03:36'),
(2286, 24, 1, '28.443416', '77.055926', '2018-01-10 07:03:51'),
(2287, 24, 1, '28.443413', '77.055920', '2018-01-10 07:04:07'),
(2288, 24, 1, '28.443411', '77.055915', '2018-01-10 07:04:21'),
(2289, 24, 1, '28.443410', '77.055910', '2018-01-10 07:04:36'),
(2290, 24, 1, '28.443408', '77.055905', '2018-01-10 07:04:51'),
(2291, 24, 1, '28.443406', '77.055902', '2018-01-10 07:05:07'),
(2292, 24, 1, '28.443405', '77.055898', '2018-01-10 07:05:21'),
(2293, 24, 1, '28.443404', '77.055895', '2018-01-10 07:05:36'),
(2294, 24, 1, '28.443410', '77.055911', '2018-01-10 07:05:52'),
(2295, 24, 1, '28.443416', '77.055926', '2018-01-10 07:06:00'),
(2296, 24, 1, '28.443420', '77.055938', '2018-01-10 07:06:15'),
(2297, 24, 1, '28.443425', '77.055950', '2018-01-10 07:06:30'),
(2298, 24, 1, '28.443425', '77.055950', '2018-01-10 07:06:43'),
(2299, 24, 1, '28.443425', '77.055950', '2018-01-10 07:06:44'),
(2300, 24, 1, '28.443425', '77.055950', '2018-01-10 07:06:44'),
(2301, 24, 1, '28.443428', '77.055959', '2018-01-10 07:06:45'),
(2302, 24, 1, '28.443445', '77.055925', '2018-01-10 07:07:01'),
(2303, 24, 1, '28.443447', '77.055923', '2018-01-10 07:07:02'),
(2304, 24, 1, '28.443453', '77.055925', '2018-01-10 07:07:09'),
(2305, 24, 1, '28.443468', '77.055995', '2018-01-10 07:07:11'),
(2306, 24, 1, '28.443468', '77.055995', '2018-01-10 07:07:43'),
(2307, 24, 1, '28.443468', '77.055995', '2018-01-10 07:07:43'),
(2308, 24, 1, '28.443471', '77.056006', '2018-01-10 07:07:43'),
(2309, 24, 1, '28.443468', '77.055995', '2018-01-10 07:07:43'),
(2310, 24, 1, '28.443471', '77.056006', '2018-01-10 07:07:57'),
(2311, 24, 1, '28.443471', '77.056006', '2018-01-10 07:07:57'),
(2312, 24, 1, '28.443471', '77.056006', '2018-01-10 07:07:57'),
(2313, 24, 1, '28.443471', '77.056008', '2018-01-10 07:07:58'),
(2314, 24, 1, '28.443471', '77.056009', '2018-01-10 07:08:13'),
(2315, 24, 1, '28.443471', '77.056009', '2018-01-10 07:08:28'),
(2316, 24, 1, '28.443471', '77.056009', '2018-01-10 07:08:43'),
(2317, 24, 1, '28.443471', '77.056009', '2018-01-10 07:08:58'),
(2318, 24, 1, '28.443471', '77.056009', '2018-01-10 07:09:13'),
(2319, 24, 1, '28.443471', '77.056009', '2018-01-10 07:09:28'),
(2320, 24, 1, '28.443471', '77.056009', '2018-01-10 07:09:43'),
(2321, 24, 1, '28.443471', '77.056009', '2018-01-10 07:10:29'),
(2322, 24, 1, '28.443826', '77.055997', '2018-01-10 07:11:06'),
(2323, 24, 1, '28.443854', '77.055996', '2018-01-10 07:11:07'),
(2324, 24, 1, '28.443693', '77.056008', '2018-01-10 07:11:07'),
(2325, 24, 1, '28.443646', '77.056012', '2018-01-10 07:11:09'),
(2326, 24, 1, '28.443592', '77.055938', '2018-01-10 07:11:16'),
(2327, 24, 1, '28.443513', '77.055980', '2018-01-10 07:11:17'),
(2328, 24, 1, '28.443384', '77.056036', '2018-01-10 07:11:24'),
(2329, 24, 1, '28.443375', '77.056033', '2018-01-10 07:11:25'),
(2330, 24, 1, '28.443433', '77.055958', '2018-01-10 07:11:34'),
(2331, 24, 1, '28.443447', '77.055976', '2018-01-10 07:11:36'),
(2332, 24, 1, '28.443437', '77.055976', '2018-01-10 07:11:44'),
(2333, 24, 1, '28.443433', '77.056009', '2018-01-10 07:11:53'),
(2334, 24, 1, '28.443451', '77.056033', '2018-01-10 07:11:55'),
(2335, 24, 1, '28.443440', '77.055996', '2018-01-10 07:12:01'),
(2336, 24, 1, '28.443423', '77.055949', '2018-01-10 07:12:03'),
(2337, 24, 1, '28.443437', '77.055912', '2018-01-10 07:12:10'),
(2338, 24, 1, '28.443434', '77.055907', '2018-01-10 07:12:12'),
(2339, 24, 1, '28.443433', '77.055908', '2018-01-10 07:12:24'),
(2340, 24, 1, '28.443404', '77.056095', '2018-01-10 07:12:34'),
(2341, 24, 1, '28.443387', '77.056128', '2018-01-10 07:12:40'),
(2342, 24, 1, '28.443445', '77.055932', '2018-01-10 07:12:46'),
(2343, 24, 1, '28.443444', '77.055989', '2018-01-10 07:12:54'),
(2344, 24, 1, '28.443442', '77.056056', '2018-01-10 07:12:55'),
(2345, 24, 1, '28.443442', '77.056056', '2018-01-10 07:12:58'),
(2346, 24, 1, '28.443438', '77.056195', '2018-01-10 07:13:01'),
(2347, 24, 1, '28.443432', '77.056417', '2018-01-10 07:13:13'),
(2348, 24, 1, '28.443432', '77.056418', '2018-01-10 07:13:14'),
(2349, 24, 1, '28.443432', '77.056418', '2018-01-10 07:13:14'),
(2350, 24, 1, '28.443403', '77.055997', '2018-01-10 07:13:16'),
(2351, 24, 1, '28.443398', '77.055937', '2018-01-10 07:13:18'),
(2352, 24, 1, '28.443438', '77.056009', '2018-01-10 07:13:26'),
(2353, 24, 1, '28.443455', '77.056033', '2018-01-10 07:13:32'),
(2354, 24, 1, '28.443419', '77.055932', '2018-01-10 07:13:40'),
(2355, 24, 1, '28.443433', '77.055971', '2018-01-10 07:13:42'),
(2356, 24, 1, '28.443411', '77.055909', '2018-01-10 07:13:48'),
(2357, 24, 1, '28.443401', '77.055884', '2018-01-10 07:13:50'),
(2358, 24, 1, '28.444157', '77.055598', '2018-01-10 07:14:04'),
(2359, 24, 1, '28.444267', '77.055569', '2018-01-10 07:14:06'),
(2360, 24, 1, '28.443787', '77.055958', '2018-01-10 07:14:13'),
(2361, 24, 1, '28.443575', '77.056095', '2018-01-10 07:14:14'),
(2362, 24, 1, '28.443469', '77.056164', '2018-01-10 07:14:29'),
(2363, 24, 1, '28.443469', '77.056164', '2018-01-10 07:14:39'),
(2364, 24, 1, '28.443469', '77.056164', '2018-01-10 07:14:39'),
(2365, 24, 1, '28.443469', '77.056164', '2018-01-10 07:14:39'),
(2366, 24, 1, '28.443487', '77.056069', '2018-01-10 07:15:02'),
(2367, 24, 1, '28.443496', '77.056023', '2018-01-10 07:15:03'),
(2368, 24, 1, '28.443500', '77.055998', '2018-01-10 07:15:13'),
(2369, 24, 1, '28.444054', '77.055699', '2018-01-10 07:15:23'),
(2370, 24, 1, '28.443525', '77.055988', '2018-01-10 07:15:24'),
(2371, 24, 1, '28.443517', '77.055992', '2018-01-10 07:15:30'),
(2372, 24, 1, '28.443502', '77.055981', '2018-01-10 07:15:51'),
(2373, 24, 1, '28.443452', '77.056023', '2018-01-10 07:15:52'),
(2374, 24, 1, '28.443451', '77.056023', '2018-01-10 07:15:58'),
(2375, 24, 1, '28.443485', '77.055993', '2018-01-10 07:16:11'),
(2376, 24, 1, '28.443498', '77.055982', '2018-01-10 07:16:13'),
(2377, 24, 1, '28.443509', '77.055972', '2018-01-10 07:16:19'),
(2378, 24, 1, '28.443511', '77.055970', '2018-01-10 07:16:31'),
(2379, 24, 1, '28.443508', '77.055973', '2018-01-10 07:16:33'),
(2380, 24, 1, '28.443505', '77.055975', '2018-01-10 07:16:47'),
(2381, 24, 1, '28.443505', '77.055976', '2018-01-10 07:16:49'),
(2382, 24, 1, '28.443504', '77.055976', '2018-01-10 07:16:55'),
(2383, 24, 1, '28.443504', '77.055976', '2018-01-10 07:17:10');
INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(2384, 24, 1, '28.443504', '77.055976', '2018-01-10 07:17:23'),
(2385, 24, 1, '28.443504', '77.055976', '2018-01-10 07:17:31'),
(2386, 24, 1, '28.443472', '77.056009', '2018-01-10 07:17:33'),
(2387, 24, 1, '28.443420', '77.056120', '2018-01-10 07:17:39'),
(2388, 24, 1, '28.443388', '77.056192', '2018-01-10 07:17:41'),
(2389, 24, 1, '28.444652', '77.056061', '2018-01-10 07:17:47'),
(2390, 24, 1, '28.444751', '77.056052', '2018-01-10 07:17:49'),
(2391, 24, 1, '28.444165', '77.055992', '2018-01-10 07:17:59'),
(2392, 24, 1, '28.443757', '77.056172', '2018-01-10 07:18:06'),
(2393, 24, 1, '28.443588', '77.056105', '2018-01-10 07:18:07'),
(2394, 24, 1, '28.443414', '77.056132', '2018-01-10 07:18:14'),
(2395, 24, 1, '28.443415', '77.056127', '2018-01-10 07:18:16'),
(2396, 24, 1, '28.444413', '77.056074', '2018-01-10 07:18:33'),
(2397, 24, 1, '28.444635', '77.056059', '2018-01-10 07:18:34'),
(2398, 24, 1, '28.443465', '77.055923', '2018-01-10 07:18:41'),
(2399, 24, 1, '28.443439', '77.055920', '2018-01-10 07:18:43'),
(2400, 24, 1, '28.443443', '77.055922', '2018-01-10 07:18:49'),
(2401, 24, 1, '28.443447', '77.055922', '2018-01-10 07:18:55'),
(2402, 24, 1, '28.443498', '77.055969', '2018-01-10 07:19:18'),
(2403, 24, 1, '28.443504', '77.055975', '2018-01-10 07:19:19'),
(2404, 24, 1, '28.443922', '77.055950', '2018-01-10 07:19:26'),
(2405, 24, 1, '28.443766', '77.055978', '2018-01-10 07:19:28'),
(2406, 24, 1, '28.443642', '77.055992', '2018-01-10 07:19:34'),
(2407, 24, 1, '28.443750', '77.056026', '2018-01-10 07:19:36'),
(2408, 24, 1, '28.443571', '77.056128', '2018-01-10 07:19:43'),
(2409, 24, 1, '28.443548', '77.056142', '2018-01-10 07:19:49'),
(2410, 24, 1, '28.443460', '77.056144', '2018-01-10 07:19:49'),
(2411, 24, 1, '28.443445', '77.056100', '2018-01-10 07:19:56'),
(2412, 24, 1, '28.443453', '77.056024', '2018-01-10 07:19:57'),
(2413, 24, 1, '28.443669', '77.056044', '2018-01-10 07:20:04'),
(2414, 24, 1, '28.443757', '77.056038', '2018-01-10 07:20:10'),
(2415, 24, 1, '28.443467', '77.056022', '2018-01-10 07:20:12'),
(2416, 24, 1, '28.443442', '77.056096', '2018-01-10 07:20:19'),
(2417, 24, 1, '28.443444', '77.056096', '2018-01-10 07:20:20'),
(2418, 24, 1, '28.443447', '77.056091', '2018-01-10 07:20:26'),
(2419, 24, 1, '28.443372', '77.056234', '2018-01-10 07:20:35'),
(2420, 24, 1, '28.443401', '77.056225', '2018-01-10 07:20:37'),
(2421, 24, 1, '28.443388', '77.056273', '2018-01-10 07:20:43'),
(2422, 24, 1, '28.443371', '77.056239', '2018-01-10 07:20:45'),
(2423, 24, 1, '28.443386', '77.056209', '2018-01-10 07:21:09'),
(2424, 24, 1, '28.443501', '77.055931', '2018-01-10 07:21:17'),
(2425, 24, 1, '28.443493', '77.055954', '2018-01-10 07:21:18'),
(2426, 24, 1, '28.443558', '77.055935', '2018-01-10 07:21:30'),
(2427, 24, 1, '28.443606', '77.055922', '2018-01-10 07:21:36'),
(2428, 24, 1, '28.443616', '77.055925', '2018-01-10 07:21:42'),
(2429, 24, 1, '28.443571', '77.055950', '2018-01-10 07:21:48'),
(2430, 24, 1, '28.443438', '77.056038', '2018-01-10 07:22:05'),
(2431, 24, 1, '28.443601', '77.055929', '2018-01-10 07:22:12'),
(2432, 24, 1, '28.443626', '77.055912', '2018-01-10 07:22:14'),
(2433, 24, 1, '28.443457', '77.055923', '2018-01-10 07:22:29'),
(2434, 24, 1, '28.443442', '77.055925', '2018-01-10 07:22:30'),
(2435, 24, 1, '28.443375', '77.055953', '2018-01-10 07:22:37'),
(2436, 24, 1, '28.443407', '77.055965', '2018-01-10 07:22:39'),
(2437, 24, 1, '28.443448', '77.055925', '2018-01-10 07:22:45'),
(2438, 24, 1, '28.443450', '77.055924', '2018-01-10 07:22:47'),
(2439, 24, 1, '28.443602', '77.056087', '2018-01-10 07:22:53'),
(2440, 24, 1, '28.443452', '77.056134', '2018-01-10 07:23:01'),
(2441, 24, 1, '28.443433', '77.056108', '2018-01-10 07:23:03'),
(2442, 24, 1, '28.443438', '77.056101', '2018-01-10 07:23:12'),
(2443, 24, 1, '28.443465', '77.056026', '2018-01-10 07:23:14'),
(2444, 24, 1, '28.443468', '77.056023', '2018-01-10 07:23:20'),
(2445, 24, 1, '28.443480', '77.056008', '2018-01-10 07:23:26'),
(2446, 24, 1, '28.443489', '77.055996', '2018-01-10 07:23:28'),
(2447, 24, 1, '28.443503', '77.055978', '2018-01-10 07:23:35'),
(2448, 24, 1, '28.443505', '77.055975', '2018-01-10 07:23:37'),
(2449, 24, 1, '28.443476', '77.056004', '2018-01-10 07:23:52'),
(2450, 24, 1, '28.443497', '77.055983', '2018-01-10 07:24:33'),
(2451, 24, 1, '28.443472', '77.056009', '2018-01-10 07:24:34'),
(2452, 24, 1, '28.443471', '77.056009', '2018-01-10 07:24:40'),
(2453, 24, 1, '28.443471', '77.056009', '2018-01-10 07:24:55'),
(2454, 24, 1, '28.443470', '77.056011', '2018-01-10 07:34:53'),
(2455, 24, 1, '28.443469', '77.056011', '2018-01-10 07:35:04'),
(2456, 24, 1, '28.443450', '77.055932', '2018-01-10 07:35:20'),
(2457, 24, 1, '28.443449', '77.055926', '2018-01-10 07:35:21'),
(2458, 24, 1, '28.443449', '77.055924', '2018-01-10 07:35:36'),
(2459, 24, 1, '28.443449', '77.055924', '2018-01-10 07:35:51'),
(2460, 24, 1, '28.443449', '77.055923', '2018-01-10 07:36:03'),
(2461, 24, 1, '28.443449', '77.055923', '2018-01-10 07:36:13'),
(2462, 24, 1, '28.443449', '77.055923', '2018-01-10 07:36:23'),
(2463, 24, 1, '28.443449', '77.055923', '2018-01-10 07:36:23'),
(2464, 24, 1, '28.443449', '77.055923', '2018-01-10 07:36:23'),
(2465, 24, 1, '28.443439', '77.055914', '2018-01-10 07:36:26'),
(2466, 24, 1, '28.443410', '77.055969', '2018-01-10 07:36:28'),
(2467, 24, 1, '28.443408', '77.055973', '2018-01-10 07:36:43'),
(2468, 24, 1, '28.443408', '77.055973', '2018-01-10 07:38:17'),
(2469, 24, 1, '28.443408', '77.055973', '2018-01-10 07:38:17'),
(2470, 24, 1, '28.443408', '77.055973', '2018-01-10 07:38:17'),
(2471, 24, 1, '28.443406', '77.055959', '2018-01-10 07:38:56'),
(2472, 24, 1, '28.443404', '77.055946', '2018-01-10 07:38:57'),
(2473, 24, 1, '28.443420', '77.055951', '2018-01-10 07:39:10'),
(2474, 24, 1, '28.443433', '77.055955', '2018-01-10 07:39:12'),
(2475, 24, 1, '28.443426', '77.055940', '2018-01-10 07:39:40'),
(2476, 24, 1, '28.443421', '77.055928', '2018-01-10 07:39:41'),
(2477, 24, 1, '28.443416', '77.055919', '2018-01-10 07:39:56'),
(2478, 24, 1, '28.443413', '77.055910', '2018-01-10 07:40:11'),
(2479, 24, 1, '28.443410', '77.055904', '2018-01-10 07:40:26'),
(2480, 24, 1, '28.443407', '77.055898', '2018-01-10 07:40:41'),
(2481, 24, 1, '28.443405', '77.055894', '2018-01-10 07:40:56'),
(2482, 24, 1, '28.443425', '77.055910', '2018-01-10 07:41:12'),
(2483, 24, 1, '28.443444', '77.055926', '2018-01-10 07:41:13'),
(2484, 24, 1, '28.443458', '77.055937', '2018-01-10 07:41:28'),
(2485, 24, 1, '28.443468', '77.055946', '2018-01-10 07:41:43'),
(2486, 24, 1, '28.443476', '77.055953', '2018-01-10 07:41:58'),
(2487, 24, 1, '28.443482', '77.055958', '2018-01-10 07:42:13'),
(2488, 24, 1, '28.443487', '77.055962', '2018-01-10 07:42:28'),
(2489, 24, 1, '28.443491', '77.055965', '2018-01-10 07:42:43'),
(2490, 24, 1, '28.443494', '77.055967', '2018-01-10 07:42:58'),
(2491, 24, 1, '28.443496', '77.055969', '2018-01-10 07:43:13'),
(2492, 24, 1, '28.443498', '77.055971', '2018-01-10 07:43:28'),
(2493, 24, 1, '28.443499', '77.055972', '2018-01-10 07:43:43'),
(2494, 24, 1, '28.443500', '77.055973', '2018-01-10 07:43:58'),
(2495, 24, 1, '28.443501', '77.055973', '2018-01-10 07:44:13'),
(2496, 24, 1, '28.443502', '77.055974', '2018-01-10 07:44:28'),
(2497, 24, 1, '28.443503', '77.055975', '2018-01-10 07:44:36'),
(2498, 24, 1, '28.443504', '77.055975', '2018-01-10 07:44:45'),
(2499, 24, 1, '28.443454', '77.056021', '2018-01-10 07:44:47'),
(2500, 24, 1, '28.443453', '77.056022', '2018-01-10 07:44:53'),
(2501, 24, 1, '28.443453', '77.056022', '2018-01-10 07:45:06'),
(2502, 24, 1, '28.443453', '77.056022', '2018-01-10 07:45:15'),
(2503, 24, 1, '28.443453', '77.056022', '2018-01-10 07:45:24'),
(2504, 24, 1, '28.443453', '77.056022', '2018-01-10 07:45:24'),
(2505, 24, 1, '28.443453', '77.056022', '2018-01-10 07:45:25'),
(2506, 24, 1, '28.443449', '77.055925', '2018-01-10 07:45:26'),
(2507, 24, 1, '28.443449', '77.055924', '2018-01-10 07:45:32'),
(2508, 24, 1, '28.443391', '77.055949', '2018-01-10 07:45:45'),
(2509, 24, 1, '28.443389', '77.055950', '2018-01-10 07:45:46'),
(2510, 24, 1, '28.443445', '77.055924', '2018-01-10 07:45:53'),
(2511, 24, 1, '28.443452', '77.055922', '2018-01-10 07:45:55'),
(2512, 24, 1, '28.444199', '77.055989', '2018-01-10 07:46:02'),
(2513, 24, 1, '28.444038', '77.056010', '2018-01-10 07:46:04'),
(2514, 24, 1, '28.443795', '77.055980', '2018-01-10 07:46:10'),
(2515, 24, 1, '28.443718', '77.055968', '2018-01-10 07:46:12'),
(2516, 24, 1, '28.443686', '77.055962', '2018-01-10 07:46:18'),
(2517, 24, 1, '28.443761', '77.055965', '2018-01-10 07:46:18'),
(2518, 24, 1, '28.443645', '77.056037', '2018-01-10 07:46:24'),
(2519, 24, 1, '28.443636', '77.056043', '2018-01-10 07:46:24'),
(2520, 24, 1, '28.443523', '77.056074', '2018-01-10 07:46:30'),
(2521, 24, 1, '28.443821', '77.055976', '2018-01-10 07:46:36'),
(2522, 24, 1, '28.443772', '77.055964', '2018-01-10 07:46:38'),
(2523, 24, 1, '28.443450', '77.056001', '2018-01-10 07:46:50'),
(2524, 24, 1, '28.443727', '77.056031', '2018-01-10 07:46:57'),
(2525, 24, 1, '28.443661', '77.056035', '2018-01-10 07:46:59'),
(2526, 24, 1, '28.443488', '77.056009', '2018-01-10 07:47:09'),
(2527, 24, 1, '28.443877', '77.056017', '2018-01-10 07:47:20'),
(2528, 24, 1, '28.444000', '77.056019', '2018-01-10 07:47:22'),
(2529, 24, 1, '28.443912', '77.056000', '2018-01-10 07:47:28'),
(2530, 24, 1, '28.443831', '77.055994', '2018-01-10 07:47:30'),
(2531, 24, 1, '28.443629', '77.055975', '2018-01-10 07:47:36'),
(2532, 24, 1, '28.443869', '77.055953', '2018-01-10 07:47:38'),
(2533, 24, 1, '28.443632', '77.055953', '2018-01-10 07:47:46'),
(2534, 24, 1, '28.443456', '77.056020', '2018-01-10 07:47:48'),
(2535, 24, 1, '28.443445', '77.055935', '2018-01-10 07:47:54'),
(2536, 24, 1, '28.443451', '77.055930', '2018-01-10 07:47:56'),
(2537, 24, 1, '28.443454', '77.055932', '2018-01-10 07:48:05'),
(2538, 24, 1, '28.443461', '77.055938', '2018-01-10 07:48:20'),
(2539, 24, 1, '28.443480', '77.055955', '2018-01-10 07:48:37'),
(2540, 24, 1, '28.443480', '77.055955', '2018-01-10 07:48:37'),
(2541, 24, 1, '28.443480', '77.055955', '2018-01-10 07:48:37'),
(2542, 24, 1, '28.443403', '77.055880', '2018-01-10 07:48:52'),
(2543, 24, 1, '28.443407', '77.055976', '2018-01-10 07:48:54'),
(2544, 24, 1, '28.443407', '77.055978', '2018-01-10 07:49:00'),
(2545, 24, 1, '28.443407', '77.055978', '2018-01-10 07:49:15'),
(2546, 24, 1, '28.443407', '77.055978', '2018-01-10 07:49:22'),
(2547, 24, 1, '28.443407', '77.055978', '2018-01-10 07:49:34'),
(2548, 24, 1, '28.443407', '77.055978', '2018-01-10 07:49:41'),
(2549, 24, 1, '28.443407', '77.055978', '2018-01-10 07:49:56'),
(2550, 24, 1, '28.443407', '77.055978', '2018-01-10 07:50:04'),
(2551, 24, 1, '28.443407', '77.055978', '2018-01-10 07:50:19'),
(2552, 24, 1, '28.443482', '77.055976', '2018-01-10 07:50:35'),
(2553, 24, 1, '28.443472', '77.056009', '2018-01-10 07:50:36'),
(2554, 24, 1, '28.443480', '77.056021', '2018-01-10 07:50:44'),
(2555, 24, 1, '28.443465', '77.056027', '2018-01-10 07:50:50'),
(2556, 24, 1, '28.443392', '77.056186', '2018-01-10 07:51:00'),
(2557, 24, 1, '28.443376', '77.056222', '2018-01-10 07:51:02'),
(2558, 24, 1, '28.443319', '77.056345', '2018-01-10 07:51:13'),
(2559, 24, 1, '28.443319', '77.056345', '2018-01-10 07:51:13'),
(2560, 24, 1, '28.443319', '77.056346', '2018-01-10 07:51:13'),
(2561, 24, 1, '28.443423', '77.056011', '2018-01-10 07:51:17'),
(2562, 24, 1, '28.443435', '77.055985', '2018-01-10 07:51:18'),
(2563, 24, 1, '28.443472', '77.055943', '2018-01-10 07:51:25'),
(2564, 24, 1, '28.443474', '77.055941', '2018-01-10 07:51:26'),
(2565, 24, 1, '28.443401', '77.056045', '2018-01-10 09:35:54'),
(2566, 24, 1, '28.443369', '77.056235', '2018-01-10 09:36:03'),
(2567, 24, 1, '28.443389', '77.056161', '2018-01-10 09:36:04'),
(2568, 24, 1, '28.443404', '77.056116', '2018-01-10 09:36:13'),
(2569, 24, 1, '28.443415', '77.056100', '2018-01-10 09:36:31'),
(2570, 24, 1, '28.443425', '77.056086', '2018-01-10 09:36:43'),
(2571, 24, 1, '28.443434', '77.056074', '2018-01-10 09:37:32'),
(2572, 24, 1, '28.443448', '77.056055', '2018-01-10 09:37:45'),
(2573, 24, 1, '28.443478', '77.056013', '2018-01-10 09:37:52'),
(2574, 24, 1, '28.443508', '77.055971', '2018-01-10 09:38:07'),
(2575, 24, 1, '28.443372', '77.056236', '2018-01-10 09:38:20'),
(2576, 24, 1, '28.443383', '77.056181', '2018-01-10 09:38:22'),
(2577, 24, 1, '28.443431', '77.055973', '2018-01-10 09:38:36'),
(2578, 24, 1, '28.443440', '77.056092', '2018-01-10 09:38:51'),
(2579, 24, 1, '28.443441', '77.056146', '2018-01-10 09:38:53'),
(2580, 24, 1, '28.443387', '77.056055', '2018-01-10 09:38:59'),
(2581, 24, 1, '28.443380', '77.056036', '2018-01-10 09:39:05'),
(2582, 24, 1, '28.443413', '77.056009', '2018-01-10 09:39:12'),
(2583, 24, 1, '28.443447', '77.055994', '2018-01-10 09:39:14'),
(2584, 24, 1, '28.443440', '77.055915', '2018-01-10 09:39:21'),
(2585, 24, 1, '28.443443', '77.055963', '2018-01-10 09:39:22'),
(2586, 24, 1, '28.443444', '77.055975', '2018-01-10 09:39:28'),
(2587, 24, 1, '28.443458', '77.055983', '2018-01-10 09:39:34'),
(2588, 29, 9, '28.4432634', '77.0559086', '2018-01-10 12:33:51'),
(2589, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:33:54'),
(2590, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:33:57'),
(2591, 29, 9, '28.4433224', '77.0559413', '2018-01-10 12:34:01'),
(2592, 29, 9, '28.4432894', '77.0559328', '2018-01-10 12:34:04'),
(2593, 29, 9, '28.4432894', '77.0559328', '2018-01-10 12:34:07'),
(2594, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:10'),
(2595, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:13'),
(2596, 29, 9, '28.4432951', '77.055925', '2018-01-10 12:34:16'),
(2597, 29, 9, '28.4432951', '77.055925', '2018-01-10 12:34:19'),
(2598, 29, 9, '28.443374', '77.055992', '2018-01-10 12:34:22'),
(2599, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:25'),
(2600, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:28'),
(2601, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:31'),
(2602, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:34'),
(2603, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:37'),
(2604, 29, 9, '28.4432246', '77.0558953', '2018-01-10 12:34:40'),
(2605, 29, 9, '28.4432246', '77.0558953', '2018-01-10 12:34:43'),
(2606, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:46'),
(2607, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:49'),
(2608, 29, 9, '28.4433149', '77.0559522', '2018-01-10 12:34:52'),
(2609, 29, 9, '28.4433149', '77.0559522', '2018-01-10 12:34:55'),
(2610, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:34:58'),
(2611, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:01'),
(2612, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:04'),
(2613, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:07'),
(2614, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:10'),
(2615, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:13'),
(2616, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:16'),
(2617, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:19'),
(2618, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:22'),
(2619, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:25'),
(2620, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:28'),
(2621, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:31'),
(2622, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:34'),
(2623, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:37'),
(2624, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:40'),
(2625, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:43'),
(2626, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:46'),
(2627, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:49'),
(2628, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:52'),
(2629, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:55'),
(2630, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:35:58'),
(2631, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:01'),
(2632, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:04'),
(2633, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:07'),
(2634, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:10'),
(2635, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:13'),
(2636, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:16'),
(2637, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:19'),
(2638, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:22'),
(2639, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:25'),
(2640, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:28'),
(2641, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:32'),
(2642, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:35'),
(2643, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:42'),
(2644, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:52'),
(2645, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:36:56'),
(2646, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:02'),
(2647, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:06'),
(2648, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:09'),
(2649, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:12'),
(2650, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:15'),
(2651, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:18'),
(2652, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:21'),
(2653, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:24'),
(2654, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:27'),
(2655, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:30'),
(2656, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:34'),
(2657, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:39'),
(2658, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:37:42'),
(2659, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:01'),
(2660, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:04'),
(2661, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:07'),
(2662, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:10'),
(2663, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:13'),
(2664, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:16'),
(2665, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:19'),
(2666, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:22'),
(2667, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:25'),
(2668, 29, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:28'),
(2669, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:31'),
(2670, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:34'),
(2671, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:37'),
(2672, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:40'),
(2673, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:43'),
(2674, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:46'),
(2675, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:49'),
(2676, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:52'),
(2677, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:55'),
(2678, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:38:58'),
(2679, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:01'),
(2680, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:04'),
(2681, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:07'),
(2682, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:10'),
(2683, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:13'),
(2684, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:16'),
(2685, 30, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:19'),
(2686, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:22'),
(2687, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:25'),
(2688, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:28'),
(2689, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:31'),
(2690, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:34'),
(2691, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:37'),
(2692, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:40'),
(2693, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:40'),
(2694, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:43'),
(2695, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:43'),
(2696, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:46'),
(2697, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:46'),
(2698, 31, 9, '28.4432711', '77.0559104', '2018-01-10 12:39:49'),
(2699, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:49'),
(2700, 31, 9, '28.4432711', '77.0559104', '2018-01-10 12:39:52'),
(2701, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:52'),
(2702, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:39:55'),
(2703, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:55'),
(2704, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:39:58'),
(2705, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:39:58'),
(2706, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:01'),
(2707, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:01'),
(2708, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:04'),
(2709, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:04'),
(2710, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:07'),
(2711, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:07'),
(2712, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:10'),
(2713, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:10'),
(2714, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:13'),
(2715, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:13'),
(2716, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:16'),
(2717, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:16'),
(2718, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:19'),
(2719, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:19'),
(2720, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:22'),
(2721, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:22'),
(2722, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:25'),
(2723, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:25'),
(2724, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:28'),
(2725, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:28'),
(2726, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:31'),
(2727, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:31'),
(2728, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:34'),
(2729, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:34'),
(2730, 31, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:37'),
(2731, 31, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:37'),
(2732, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:40'),
(2733, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:40'),
(2734, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:43'),
(2735, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:43'),
(2736, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:46'),
(2737, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:46'),
(2738, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:49'),
(2739, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:49'),
(2740, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:52'),
(2741, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:52'),
(2742, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:55'),
(2743, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:55'),
(2744, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:40:58'),
(2745, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:40:58'),
(2746, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:01'),
(2747, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:01'),
(2748, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:04'),
(2749, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:04'),
(2750, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:06'),
(2751, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:07'),
(2752, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:07'),
(2753, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:09'),
(2754, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:10'),
(2755, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:10'),
(2756, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:12'),
(2757, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:13'),
(2758, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:13'),
(2759, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:15'),
(2760, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:16'),
(2761, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:16'),
(2762, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:18'),
(2763, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:19'),
(2764, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:19'),
(2765, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:21'),
(2766, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:22'),
(2767, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:22'),
(2768, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:24'),
(2769, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:25'),
(2770, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:25'),
(2771, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:27'),
(2772, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:28'),
(2773, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:28'),
(2774, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:30'),
(2775, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:31'),
(2776, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:31'),
(2777, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:33'),
(2778, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:34'),
(2779, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:34'),
(2780, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:36'),
(2781, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:37'),
(2782, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:37'),
(2783, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:39'),
(2784, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:40'),
(2785, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:40'),
(2786, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:42'),
(2787, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:43'),
(2788, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:43'),
(2789, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:45'),
(2790, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:46'),
(2791, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:46'),
(2792, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:48'),
(2793, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:49'),
(2794, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:49'),
(2795, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:51'),
(2796, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:52'),
(2797, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:52'),
(2798, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:54'),
(2799, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:55'),
(2800, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:55'),
(2801, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:41:57'),
(2802, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:41:58'),
(2803, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:41:58'),
(2804, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:00'),
(2805, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:01'),
(2806, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:01'),
(2807, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:03'),
(2808, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:04'),
(2809, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:04'),
(2810, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:06'),
(2811, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:07'),
(2812, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:07'),
(2813, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:10'),
(2814, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:11'),
(2815, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:11'),
(2816, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:13'),
(2817, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:20'),
(2818, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:20'),
(2819, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:22'),
(2820, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:23'),
(2821, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:23'),
(2822, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:30'),
(2823, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:33'),
(2824, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:33'),
(2825, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:37'),
(2826, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:38'),
(2827, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:38'),
(2828, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:40'),
(2829, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:41'),
(2830, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:41'),
(2831, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:44'),
(2832, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:47'),
(2833, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:47'),
(2834, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:52'),
(2835, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:42:55'),
(2836, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:42:56'),
(2837, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:42:59'),
(2838, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:00'),
(2839, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:00'),
(2840, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:04'),
(2841, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:05'),
(2842, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:05'),
(2843, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:12'),
(2844, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:13'),
(2845, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:13'),
(2846, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:15'),
(2847, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:16'),
(2848, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:16'),
(2849, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:18'),
(2850, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:19'),
(2851, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:19'),
(2852, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:21'),
(2853, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:22'),
(2854, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:22'),
(2855, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:24'),
(2856, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:25'),
(2857, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:25'),
(2858, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:27'),
(2859, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:28'),
(2860, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:28'),
(2861, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:30'),
(2862, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:31'),
(2863, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:31'),
(2864, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:33'),
(2865, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:34'),
(2866, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:34'),
(2867, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:36'),
(2868, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:37'),
(2869, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:37'),
(2870, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:39'),
(2871, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:40'),
(2872, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:40'),
(2873, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:42'),
(2874, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:43'),
(2875, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:43'),
(2876, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:45'),
(2877, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:46'),
(2878, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:46'),
(2879, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:48'),
(2880, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:49'),
(2881, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:49'),
(2882, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:51'),
(2883, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:52'),
(2884, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:52'),
(2885, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:54'),
(2886, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:55'),
(2887, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:55'),
(2888, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:43:57'),
(2889, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:43:58'),
(2890, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:43:58'),
(2891, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:44:00'),
(2892, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:44:01'),
(2893, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:44:01'),
(2894, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:44:03'),
(2895, 32, 9, '28.4433251', '77.0559643', '2018-01-10 12:44:04'),
(2896, 32, 9, '28.4432106', '77.0558788', '2018-01-10 12:44:04'),
(2897, 32, 9, '28.4432796', '77.0559201', '2018-01-10 12:44:06'),
(2898, 33, 1, '28.443379', '77.056112', '2018-01-10 12:48:13'),
(2899, 33, 1, '28.443382', '77.056073', '2018-01-10 12:48:26'),
(2900, 33, 1, '28.443428', '77.055979', '2018-01-10 12:48:27'),
(2901, 33, 1, '28.443397', '77.056054', '2018-01-10 12:48:33'),
(2902, 33, 1, '28.443378', '77.056185', '2018-01-10 12:48:39'),
(2903, 33, 1, '28.443398', '77.056113', '2018-01-10 12:48:45'),
(2904, 33, 1, '28.443402', '77.056107', '2018-01-10 12:48:51'),
(2905, 33, 1, '28.443402', '77.056097', '2018-01-10 12:49:03'),
(2906, 33, 1, '28.443407', '77.056091', '2018-01-10 12:49:14'),
(2907, 33, 1, '28.443412', '77.056085', '2018-01-10 12:49:14'),
(2908, 33, 1, '28.443377', '77.056204', '2018-01-10 12:49:20'),
(2909, 33, 1, '28.443368', '77.056234', '2018-01-10 12:49:28'),
(2910, 33, 1, '28.443369', '77.056235', '2018-01-10 12:49:30'),
(2911, 33, 1, '28.443369', '77.056235', '2018-01-10 12:49:45'),
(2912, 33, 1, '28.443369', '77.056235', '2018-01-10 12:50:00'),
(2913, 33, 1, '28.443369', '77.056236', '2018-01-10 12:50:15'),
(2914, 33, 1, '28.443369', '77.056236', '2018-01-10 12:50:30'),
(2915, 33, 1, '28.443397', '77.056182', '2018-01-10 12:50:40'),
(2916, 33, 1, '28.443466', '77.056017', '2018-01-10 12:50:42'),
(2917, 34, 1, '28.443416', '77.056047', '2018-01-10 12:52:17'),
(2918, 34, 1, '28.443413', '77.056052', '2018-01-10 12:52:18'),
(2919, 34, 1, '28.443413', '77.056044', '2018-01-10 12:52:18'),
(2920, 34, 1, '28.443411', '77.056065', '2018-01-10 12:52:43'),
(2921, 34, 1, '28.443440', '77.055915', '2018-01-10 12:52:45'),
(2922, 34, 1, '28.443436', '77.055908', '2018-01-10 12:52:47'),
(2923, 34, 1, '28.443454', '77.055902', '2018-01-10 12:52:53'),
(2924, 34, 1, '28.443476', '77.055929', '2018-01-10 12:52:55'),
(2925, 34, 1, '28.443439', '77.055899', '2018-01-10 12:53:01'),
(2926, 34, 1, '28.443415', '77.055883', '2018-01-10 12:53:03'),
(2927, 34, 1, '28.443397', '77.055871', '2018-01-10 12:53:09'),
(2928, 34, 1, '28.443426', '77.055966', '2018-01-10 12:53:15'),
(2929, 34, 1, '28.443443', '77.056017', '2018-01-10 12:53:21'),
(2930, 34, 1, '28.443498', '77.055995', '2018-01-10 12:53:35'),
(2931, 34, 1, '28.443470', '77.056009', '2018-01-10 12:53:37'),
(2932, 34, 1, '28.443371', '77.056231', '2018-01-10 12:53:43'),
(2933, 34, 1, '28.443374', '77.056302', '2018-01-10 12:53:45'),
(2934, 34, 1, '28.443443', '77.055947', '2018-01-10 12:53:51'),
(2935, 34, 1, '28.443617', '77.055886', '2018-01-10 12:53:53'),
(2936, 34, 1, '28.443800', '77.055903', '2018-01-10 12:53:59'),
(2937, 34, 1, '28.443828', '77.055933', '2018-01-10 12:54:05'),
(2938, 34, 1, '28.443623', '77.055916', '2018-01-10 12:54:12'),
(2939, 34, 1, '28.443430', '77.056096', '2018-01-10 12:54:13'),
(2940, 34, 1, '28.443437', '77.056076', '2018-01-10 12:54:24'),
(2941, 34, 1, '28.443452', '77.056026', '2018-01-10 12:54:26'),
(2942, 34, 1, '28.443437', '77.056038', '2018-01-10 12:54:34'),
(2943, 34, 1, '28.443446', '77.056030', '2018-01-10 12:54:41'),
(2944, 34, 1, '28.443466', '77.056012', '2018-01-10 12:54:43'),
(2945, 34, 1, '28.443494', '77.055986', '2018-01-10 12:54:49'),
(2946, 34, 1, '28.443470', '77.056009', '2018-01-10 12:54:51'),
(2947, 34, 1, '28.443469', '77.056009', '2018-01-10 12:54:57'),
(2948, 34, 1, '28.443469', '77.056010', '2018-01-10 12:55:12'),
(2949, 34, 1, '28.443486', '77.055993', '2018-01-10 12:55:21'),
(2950, 34, 1, '28.443455', '77.056020', '2018-01-10 12:55:22'),
(2951, 34, 1, '28.443422', '77.056109', '2018-01-10 12:55:29'),
(2952, 35, 1, '28.443425', '77.056093', '2018-01-10 12:55:30'),
(2953, 35, 1, '28.443405', '77.056113', '2018-01-10 12:55:39'),
(2954, 35, 1, '28.443447', '77.055923', '2018-01-10 12:55:54'),
(2955, 35, 1, '28.443439', '77.055961', '2018-01-10 12:55:56'),
(2956, 36, 1, '28.443442', '77.056069', '2018-01-10 12:56:07'),
(2957, 36, 1, '28.443442', '77.056088', '2018-01-10 12:56:13'),
(2958, 36, 1, '28.443429', '77.056042', '2018-01-10 12:56:19'),
(2959, 36, 1, '28.443433', '77.056040', '2018-01-10 12:56:25'),
(2960, 36, 1, '28.443436', '77.056037', '2018-01-10 12:56:37'),
(2961, 36, 1, '28.443439', '77.056034', '2018-01-10 12:56:48'),
(2962, 36, 1, '28.443441', '77.056032', '2018-01-10 12:56:54'),
(2963, 36, 1, '28.443444', '77.056029', '2018-01-10 12:57:09'),
(2964, 36, 1, '28.443447', '77.056027', '2018-01-10 12:57:24'),
(2965, 36, 1, '28.443449', '77.056025', '2018-01-10 12:57:39'),
(2966, 36, 1, '28.443452', '77.056023', '2018-01-10 12:57:54'),
(2967, 36, 1, '28.443454', '77.056021', '2018-01-10 12:58:09'),
(2968, 36, 1, '28.443456', '77.056019', '2018-01-10 12:58:24'),
(2969, 36, 1, '28.443458', '77.056017', '2018-01-10 12:58:39'),
(2970, 36, 1, '28.443460', '77.056015', '2018-01-10 12:58:54'),
(2971, 36, 1, '28.443462', '77.056013', '2018-01-10 12:59:09'),
(2972, 36, 1, '28.443464', '77.056012', '2018-01-10 12:59:24'),
(2973, 36, 1, '28.443466', '77.056010', '2018-01-10 12:59:39'),
(2974, 36, 1, '28.443467', '77.056009', '2018-01-10 12:59:54'),
(2975, 36, 1, '28.443469', '77.056007', '2018-01-10 13:00:09'),
(2976, 36, 1, '28.443470', '77.056006', '2018-01-10 13:00:24'),
(2977, 36, 1, '28.443472', '77.056005', '2018-01-10 13:00:39'),
(2978, 36, 1, '28.443473', '77.056004', '2018-01-10 13:00:54'),
(2979, 36, 1, '28.443475', '77.056002', '2018-01-10 13:01:09'),
(2980, 36, 1, '28.443476', '77.056001', '2018-01-10 13:01:24'),
(2981, 36, 1, '28.443477', '77.056000', '2018-01-10 13:01:39'),
(2982, 36, 1, '28.443478', '77.055999', '2018-01-10 13:01:54'),
(2983, 36, 1, '28.443479', '77.055998', '2018-01-10 13:02:09'),
(2984, 36, 1, '28.443480', '77.055997', '2018-01-10 13:02:24'),
(2985, 36, 1, '28.443481', '77.055996', '2018-01-10 13:02:39'),
(2986, 36, 1, '28.443482', '77.055995', '2018-01-10 13:02:54'),
(2987, 36, 1, '28.443483', '77.055994', '2018-01-10 13:03:09'),
(2988, 36, 1, '28.443484', '77.055994', '2018-01-10 13:03:24'),
(2989, 36, 1, '28.443485', '77.055993', '2018-01-10 13:03:39'),
(2990, 36, 1, '28.443486', '77.055992', '2018-01-10 13:03:54'),
(2991, 36, 1, '28.443487', '77.055991', '2018-01-10 13:04:09'),
(2992, 36, 1, '28.443488', '77.055991', '2018-01-10 13:04:24'),
(2993, 36, 1, '28.443488', '77.055990', '2018-01-10 13:04:39'),
(2994, 36, 1, '28.443489', '77.055990', '2018-01-10 13:04:54'),
(2995, 36, 1, '28.443490', '77.055989', '2018-01-10 13:05:09'),
(2996, 36, 1, '28.443490', '77.055988', '2018-01-10 13:05:24'),
(2997, 36, 1, '28.443491', '77.055988', '2018-01-10 13:05:39'),
(2998, 36, 1, '28.443491', '77.055987', '2018-01-10 13:05:54'),
(2999, 36, 1, '28.443491', '77.055987', '2018-01-10 13:06:06'),
(3000, 36, 1, '28.443491', '77.055987', '2018-01-10 13:06:06'),
(3001, 36, 1, '28.443491', '77.055987', '2018-01-10 13:06:06'),
(3002, 36, 1, '28.443491', '77.055987', '2018-01-10 13:06:06'),
(3003, 36, 1, '28.443491', '77.055987', '2018-01-10 13:06:06'),
(3004, 36, 1, '28.443491', '77.055987', '2018-01-10 13:06:06'),
(3005, 36, 1, '28.443491', '77.055987', '2018-01-10 13:06:06'),
(3006, 36, 1, '28.443492', '77.055987', '2018-01-10 13:06:09'),
(3007, 36, 1, '28.443493', '77.055986', '2018-01-10 13:06:24'),
(3008, 36, 1, '28.443493', '77.055986', '2018-01-10 13:06:39'),
(3009, 36, 1, '28.443494', '77.055986', '2018-01-10 13:06:54'),
(3010, 36, 1, '28.443494', '77.055985', '2018-01-10 13:07:09'),
(3011, 36, 1, '28.443494', '77.055985', '2018-01-10 13:07:24'),
(3012, 36, 1, '28.443495', '77.055984', '2018-01-10 13:07:40'),
(3013, 36, 1, '28.443458', '77.055936', '2018-01-10 13:07:41'),
(3014, 36, 1, '28.443451', '77.055926', '2018-01-10 13:07:42'),
(3015, 36, 1, '28.443325', '77.055887', '2018-01-10 13:07:50'),
(3016, 36, 1, '28.443239', '77.055870', '2018-01-10 13:07:50'),
(3017, 36, 1, '28.443177', '77.055858', '2018-01-10 13:07:50'),
(3018, 36, 1, '28.443635', '77.055960', '2018-01-10 13:08:15'),
(3019, 36, 1, '28.443622', '77.055952', '2018-01-10 13:08:17'),
(3020, 36, 1, '28.443633', '77.056000', '2018-01-10 13:08:24'),
(3021, 36, 1, '28.443474', '77.055960', '2018-01-10 13:08:30'),
(3022, 36, 1, '28.443456', '77.055998', '2018-01-10 13:08:32'),
(3023, 36, 1, '28.443380', '77.056126', '2018-01-10 13:08:38'),
(3024, 36, 1, '28.443362', '77.056194', '2018-01-10 13:08:40'),
(3025, 36, 1, '28.443257', '77.056189', '2018-01-10 13:08:47'),
(3026, 36, 1, '28.443231', '77.056147', '2018-01-10 13:08:49'),
(3027, 36, 1, '28.443222', '77.056150', '2018-01-10 13:09:07'),
(3028, 36, 1, '28.443222', '77.056150', '2018-01-10 13:09:07'),
(3029, 36, 1, '28.443502', '77.055977', '2018-01-10 13:09:38'),
(3030, 36, 1, '28.443503', '77.055977', '2018-01-10 13:09:40'),
(3031, 38, 2, '28.4432671', '77.0559072', '2018-01-11 11:16:15'),
(3032, 38, 2, '28.4432671', '77.0559072', '2018-01-11 11:16:19'),
(3033, 38, 2, '28.443266', '77.0559053', '2018-01-11 11:16:21'),
(3034, 38, 2, '28.4432852', '77.0559313', '2018-01-11 11:16:24'),
(3035, 38, 2, '28.4432852', '77.0559313', '2018-01-11 11:16:27'),
(3036, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:30'),
(3037, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:33'),
(3038, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:36'),
(3039, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:39'),
(3040, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:42'),
(3041, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:45'),
(3042, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:48'),
(3043, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:51'),
(3044, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:54'),
(3045, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:16:57'),
(3046, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:00'),
(3047, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:03'),
(3048, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:06'),
(3049, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:09'),
(3050, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:12'),
(3051, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:15'),
(3052, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:18'),
(3053, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:21'),
(3054, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:26'),
(3055, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:27'),
(3056, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:30'),
(3057, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:33'),
(3058, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:36'),
(3059, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:39'),
(3060, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:42'),
(3061, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:45'),
(3062, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:48'),
(3063, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:51'),
(3064, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:54'),
(3065, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:17:57'),
(3066, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:06'),
(3067, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:08'),
(3068, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:08'),
(3069, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:09'),
(3070, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:12'),
(3071, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:20'),
(3072, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:42'),
(3073, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:43'),
(3074, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:43'),
(3075, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:43'),
(3076, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:43'),
(3077, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:43'),
(3078, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:43'),
(3079, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:43'),
(3080, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:43'),
(3081, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:45'),
(3082, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:48'),
(3083, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:51'),
(3084, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:54'),
(3085, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:18:57'),
(3086, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:00'),
(3087, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:03'),
(3088, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:06'),
(3089, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:09'),
(3090, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:12'),
(3091, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:15'),
(3092, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:18'),
(3093, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:21'),
(3094, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:24'),
(3095, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:27'),
(3096, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:30'),
(3097, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:33'),
(3098, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:36'),
(3099, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:39'),
(3100, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:42'),
(3101, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:45'),
(3102, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:48'),
(3103, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:51'),
(3104, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:54'),
(3105, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:19:58'),
(3106, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:00'),
(3107, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:03'),
(3108, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:06'),
(3109, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:09'),
(3110, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:13'),
(3111, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:15'),
(3112, 38, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:19'),
(3113, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:23'),
(3114, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:24'),
(3115, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:28'),
(3116, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:30'),
(3117, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:33'),
(3118, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:36'),
(3119, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:39'),
(3120, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:43'),
(3121, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:45'),
(3122, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:51'),
(3123, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:51'),
(3124, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:55'),
(3125, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:20:57'),
(3126, 39, 2, '28.4432493', '77.0559021', '2018-01-11 11:20:57'),
(3127, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:00'),
(3128, 39, 2, '28.4432109', '77.0558783', '2018-01-11 11:21:03'),
(3129, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:05'),
(3130, 39, 2, '28.4432109', '77.0558783', '2018-01-11 11:21:10'),
(3131, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:10'),
(3132, 39, 2, '28.4432757', '77.05593', '2018-01-11 11:21:10'),
(3133, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:10'),
(3134, 39, 2, '28.4432757', '77.05593', '2018-01-11 11:21:10'),
(3135, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:13'),
(3136, 39, 2, '28.4432315', '77.0558891', '2018-01-11 11:21:13'),
(3137, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:15'),
(3138, 39, 2, '28.4432315', '77.0558891', '2018-01-11 11:21:15'),
(3139, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:19'),
(3140, 39, 2, '28.443234', '77.055892', '2018-01-11 11:21:19'),
(3141, 39, 2, '28.443234', '77.055892', '2018-01-11 11:21:23'),
(3142, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:23'),
(3143, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:28'),
(3144, 39, 2, '28.4432358', '77.0558946', '2018-01-11 11:21:28'),
(3145, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:28'),
(3146, 39, 2, '28.4432344', '77.0558967', '2018-01-11 11:21:28'),
(3147, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:30'),
(3148, 39, 2, '28.4432344', '77.0558967', '2018-01-11 11:21:30'),
(3149, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:33'),
(3150, 39, 2, '28.4434019', '77.0560144', '2018-01-11 11:21:33'),
(3151, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:38'),
(3152, 39, 2, '28.4434019', '77.0560144', '2018-01-11 11:21:39'),
(3153, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:39'),
(3154, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:21:39'),
(3155, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:21:42'),
(3156, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:42'),
(3157, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:47'),
(3158, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:48'),
(3159, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:21:48'),
(3160, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:21:57'),
(3161, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:57'),
(3162, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:21:57'),
(3163, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:57'),
(3164, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:21:57'),
(3165, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:21:57'),
(3166, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:21:57'),
(3167, 36, 1, '28.443365', '77.055923', '2018-01-11 11:22:01'),
(3168, 36, 1, '28.443365', '77.055923', '2018-01-11 11:22:01'),
(3169, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:02'),
(3170, 36, 1, '28.443365', '77.055923', '2018-01-11 11:22:02'),
(3171, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:02'),
(3172, 36, 1, '28.443365', '77.055923', '2018-01-11 11:22:03'),
(3173, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:03');
INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(3174, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:03'),
(3175, 36, 1, '28.443365', '77.055923', '2018-01-11 11:22:04'),
(3176, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:06'),
(3177, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:06'),
(3178, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:09'),
(3179, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:09'),
(3180, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:12'),
(3181, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:12'),
(3182, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:16'),
(3183, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:17'),
(3184, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:19'),
(3185, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:19'),
(3186, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:21'),
(3187, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:21'),
(3188, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:24'),
(3189, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:24'),
(3190, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:27'),
(3191, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:27'),
(3192, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:31'),
(3193, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:31'),
(3194, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:33'),
(3195, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:33'),
(3196, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:36'),
(3197, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:36'),
(3198, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:39'),
(3199, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:39'),
(3200, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:42'),
(3201, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:42'),
(3202, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:46'),
(3203, 40, 1, '28.443464', '77.056005', '2018-01-11 11:22:47'),
(3204, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:48'),
(3205, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:48'),
(3206, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:51'),
(3207, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:51'),
(3208, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:53'),
(3209, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:54'),
(3210, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:54'),
(3211, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:22:57'),
(3212, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:22:57'),
(3213, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:01'),
(3214, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:01'),
(3215, 40, 1, '28.443467', '77.056009', '2018-01-11 11:23:02'),
(3216, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:03'),
(3217, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:03'),
(3218, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:06'),
(3219, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:06'),
(3220, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:09'),
(3221, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:09'),
(3222, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:12'),
(3223, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:12'),
(3224, 40, 1, '28.443463', '77.056013', '2018-01-11 11:23:15'),
(3225, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:15'),
(3226, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:15'),
(3227, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:18'),
(3228, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:18'),
(3229, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:22'),
(3230, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:24'),
(3231, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:24'),
(3232, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:25'),
(3233, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:27'),
(3234, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:27'),
(3235, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:31'),
(3236, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:33'),
(3237, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:33'),
(3238, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:36'),
(3239, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:36'),
(3240, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:39'),
(3241, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:39'),
(3242, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:41'),
(3243, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:42'),
(3244, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:42'),
(3245, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:46'),
(3246, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:48'),
(3247, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:48'),
(3248, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:49'),
(3249, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:51'),
(3250, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:51'),
(3251, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:23:54'),
(3252, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:23:54'),
(3253, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:01'),
(3254, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:01'),
(3255, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:04'),
(3256, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:04'),
(3257, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:07'),
(3258, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:07'),
(3259, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:10'),
(3260, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:10'),
(3261, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:13'),
(3262, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:13'),
(3263, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:16'),
(3264, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:17'),
(3265, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:19'),
(3266, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:19'),
(3267, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:22'),
(3268, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:23'),
(3269, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:25'),
(3270, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:25'),
(3271, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:28'),
(3272, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:28'),
(3273, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:31'),
(3274, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:31'),
(3275, 39, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:34'),
(3276, 39, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:34'),
(3277, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:37'),
(3278, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:37'),
(3279, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:40'),
(3280, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:40'),
(3281, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:43'),
(3282, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:43'),
(3283, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:46'),
(3284, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:47'),
(3285, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:49'),
(3286, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:49'),
(3287, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:52'),
(3288, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:52'),
(3289, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:55'),
(3290, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:55'),
(3291, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:24:58'),
(3292, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:24:58'),
(3293, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:01'),
(3294, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:01'),
(3295, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:05'),
(3296, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:05'),
(3297, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:07'),
(3298, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:07'),
(3299, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:10'),
(3300, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:10'),
(3301, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:13'),
(3302, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:13'),
(3303, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:16'),
(3304, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:16'),
(3305, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:19'),
(3306, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:19'),
(3307, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:22'),
(3308, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:22'),
(3309, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:25'),
(3310, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:25'),
(3311, 42, 2, '28.4432587', '77.0559135', '2018-01-11 11:25:28'),
(3312, 42, 2, '28.4432025', '77.0558668', '2018-01-11 11:25:28'),
(3313, 43, 11, '28.443469', '77.056009', '2018-01-11 11:26:59'),
(3314, 43, 11, '28.443469', '77.056009', '2018-01-11 11:27:04'),
(3315, 43, 11, '28.443469', '77.056010', '2018-01-11 11:27:09'),
(3316, 43, 11, '28.443469', '77.056010', '2018-01-11 11:27:14'),
(3317, 43, 11, '28.443469', '77.056010', '2018-01-11 11:27:19'),
(3318, 43, 11, '28.443469', '77.056010', '2018-01-11 11:27:24'),
(3319, 43, 11, '28.443469', '77.056010', '2018-01-11 11:27:29'),
(3320, 45, 2, '28.4434019', '77.0560144', '2018-01-11 11:31:24'),
(3321, 45, 2, '28.4434019', '77.0560144', '2018-01-11 11:31:27'),
(3322, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:31:30'),
(3323, 45, 2, '28.4433697', '77.055947', '2018-01-11 11:31:33'),
(3324, 45, 2, '28.4433697', '77.055947', '2018-01-11 11:31:36'),
(3325, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:31:39'),
(3326, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:31:42'),
(3327, 45, 2, '28.4432968', '77.0559211', '2018-01-11 11:31:45'),
(3328, 45, 2, '28.4433929', '77.0560031', '2018-01-11 11:31:48'),
(3329, 45, 2, '28.4433929', '77.0560031', '2018-01-11 11:31:51'),
(3330, 45, 2, '28.443512', '77.0559596', '2018-01-11 11:31:54'),
(3331, 45, 2, '28.443512', '77.0559596', '2018-01-11 11:31:57'),
(3332, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:00'),
(3333, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:03'),
(3334, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:06'),
(3335, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:09'),
(3336, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:12'),
(3337, 45, 2, '28.4432474', '77.0559013', '2018-01-11 11:32:15'),
(3338, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:18'),
(3339, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:21'),
(3340, 45, 2, '28.4433786', '77.0559107', '2018-01-11 11:32:24'),
(3341, 45, 2, '28.4433786', '77.0559107', '2018-01-11 11:32:27'),
(3342, 45, 2, '28.4432618', '77.0559073', '2018-01-11 11:32:30'),
(3343, 45, 2, '28.4432785', '77.0559139', '2018-01-11 11:32:33'),
(3344, 45, 2, '28.4432785', '77.0559139', '2018-01-11 11:32:36'),
(3345, 45, 2, '28.4432785', '77.0559139', '2018-01-11 11:32:39'),
(3346, 45, 2, '28.4432785', '77.0559139', '2018-01-11 11:32:42'),
(3347, 45, 2, '28.4433317', '77.0558981', '2018-01-11 11:32:45'),
(3348, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:48'),
(3349, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:32:51'),
(3350, 45, 2, '28.4433153', '77.0559281', '2018-01-11 11:32:54'),
(3351, 45, 2, '28.4433153', '77.0559281', '2018-01-11 11:32:57'),
(3352, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:33:00'),
(3353, 45, 2, '28.4433778', '77.0559493', '2018-01-11 11:33:03'),
(3354, 45, 2, '28.4433778', '77.0559493', '2018-01-11 11:33:06'),
(3355, 45, 2, '28.4432771', '77.0559127', '2018-01-11 11:33:09'),
(3356, 45, 2, '28.4432771', '77.0559127', '2018-01-11 11:33:12'),
(3357, 45, 2, '28.4432832', '77.055915', '2018-01-11 11:33:15'),
(3358, 45, 2, '28.4432875', '77.055898', '2018-01-11 11:33:18'),
(3359, 45, 2, '28.4432875', '77.055898', '2018-01-11 11:33:21'),
(3360, 45, 2, '28.4432317', '77.0558865', '2018-01-11 11:33:24'),
(3361, 45, 2, '28.4432317', '77.0558865', '2018-01-11 11:33:27'),
(3362, 45, 2, '28.4433813', '77.0558966', '2018-01-11 11:33:30'),
(3363, 45, 2, '28.4432699', '77.0559106', '2018-01-11 11:33:33'),
(3364, 45, 2, '28.4432699', '77.0559106', '2018-01-11 11:33:36'),
(3365, 45, 2, '28.4434019', '77.0560144', '2018-01-11 11:33:39'),
(3366, 45, 2, '28.4434019', '77.0560144', '2018-01-11 11:33:42'),
(3367, 45, 2, '28.4434695', '77.0559448', '2018-01-11 11:33:45'),
(3368, 45, 2, '28.4432543', '77.0559042', '2018-01-11 11:33:48'),
(3369, 45, 2, '28.4432543', '77.0559042', '2018-01-11 11:33:51'),
(3370, 45, 2, '28.4432785', '77.0559139', '2018-01-11 11:33:54'),
(3371, 45, 2, '28.4432785', '77.0559139', '2018-01-11 11:33:57'),
(3372, 45, 2, '28.4433317', '77.0558981', '2018-01-11 11:34:00'),
(3373, 45, 2, '28.4433321', '77.0559344', '2018-01-11 11:34:03'),
(3374, 45, 2, '28.4433321', '77.0559344', '2018-01-11 11:34:06'),
(3375, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:09'),
(3376, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:12'),
(3377, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:15'),
(3378, 45, 2, '28.4432875', '77.055898', '2018-01-11 11:34:18'),
(3379, 45, 2, '28.4432875', '77.055898', '2018-01-11 11:34:21'),
(3380, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:24'),
(3381, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:27'),
(3382, 45, 2, '28.4432563', '77.0558971', '2018-01-11 11:34:30'),
(3383, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:33'),
(3384, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:36'),
(3385, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:39'),
(3386, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:42'),
(3387, 45, 2, '28.4432106', '77.0558788', '2018-01-11 11:34:45'),
(3388, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:34:48'),
(3389, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:34:51'),
(3390, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:34:54'),
(3391, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:34:57'),
(3392, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:00'),
(3393, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:03'),
(3394, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:06'),
(3395, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:09'),
(3396, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:12'),
(3397, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:15'),
(3398, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:18'),
(3399, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:21'),
(3400, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:24'),
(3401, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:27'),
(3402, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:30'),
(3403, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:33'),
(3404, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:36'),
(3405, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:39'),
(3406, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:42'),
(3407, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:45'),
(3408, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:48'),
(3409, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:51'),
(3410, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:54'),
(3411, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:35:57'),
(3412, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:00'),
(3413, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:03'),
(3414, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:06'),
(3415, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:09'),
(3416, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:12'),
(3417, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:15'),
(3418, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:18'),
(3419, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:21'),
(3420, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:24'),
(3421, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:27'),
(3422, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:30'),
(3423, 46, 11, '28.443428', '77.056071', '2018-01-11 11:36:31'),
(3424, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:33'),
(3425, 46, 11, '28.443428', '77.056071', '2018-01-11 11:36:36'),
(3426, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:36'),
(3427, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:39'),
(3428, 46, 11, '28.443428', '77.056071', '2018-01-11 11:36:41'),
(3429, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:42'),
(3430, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:45'),
(3431, 46, 11, '28.443428', '77.056071', '2018-01-11 11:36:46'),
(3432, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:48'),
(3433, 46, 11, '28.443428', '77.056071', '2018-01-11 11:36:51'),
(3434, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:51'),
(3435, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:54'),
(3436, 46, 11, '28.443429', '77.056070', '2018-01-11 11:36:56'),
(3437, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:36:57'),
(3438, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:00'),
(3439, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:01'),
(3440, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:03'),
(3441, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:06'),
(3442, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:06'),
(3443, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:09'),
(3444, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:11'),
(3445, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:12'),
(3446, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:15'),
(3447, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:16'),
(3448, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:18'),
(3449, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:21'),
(3450, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:21'),
(3451, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:24'),
(3452, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:26'),
(3453, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:27'),
(3454, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:30'),
(3455, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:31'),
(3456, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:33'),
(3457, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:36'),
(3458, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:36'),
(3459, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:39'),
(3460, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:41'),
(3461, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:42'),
(3462, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:45'),
(3463, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:46'),
(3464, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:48'),
(3465, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:51'),
(3466, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:51'),
(3467, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:54'),
(3468, 46, 11, '28.443430', '77.056070', '2018-01-11 11:37:56'),
(3469, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:37:57'),
(3470, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:00'),
(3471, 46, 11, '28.443430', '77.056070', '2018-01-11 11:38:01'),
(3472, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:03'),
(3473, 46, 11, '28.443430', '77.056070', '2018-01-11 11:38:06'),
(3474, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:06'),
(3475, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:09'),
(3476, 46, 11, '28.443430', '77.056070', '2018-01-11 11:38:11'),
(3477, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:12'),
(3478, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:15'),
(3479, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:18'),
(3480, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:21'),
(3481, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:24'),
(3482, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:27'),
(3483, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:30'),
(3484, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:33'),
(3485, 46, 11, '28.443430', '77.056070', '2018-01-11 11:38:35'),
(3486, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:36'),
(3487, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:39'),
(3488, 46, 11, '28.443430', '77.056070', '2018-01-11 11:38:40'),
(3489, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:42'),
(3490, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:45'),
(3491, 46, 11, '28.443430', '77.056070', '2018-01-11 11:38:46'),
(3492, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:48'),
(3493, 46, 11, '28.443430', '77.056070', '2018-01-11 11:38:50'),
(3494, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:51'),
(3495, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:54'),
(3496, 46, 11, '28.443430', '77.056070', '2018-01-11 11:38:55'),
(3497, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:38:57'),
(3498, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:00'),
(3499, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:00'),
(3500, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:03'),
(3501, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:05'),
(3502, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:06'),
(3503, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:09'),
(3504, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:10'),
(3505, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:12'),
(3506, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:15'),
(3507, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:15'),
(3508, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:18'),
(3509, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:20'),
(3510, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:21'),
(3511, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:24'),
(3512, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:25'),
(3513, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:28'),
(3514, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:30'),
(3515, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:30'),
(3516, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:33'),
(3517, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:35'),
(3518, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:36'),
(3519, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:39'),
(3520, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:40'),
(3521, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:42'),
(3522, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:45'),
(3523, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:45'),
(3524, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:48'),
(3525, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:50'),
(3526, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:51'),
(3527, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:54'),
(3528, 46, 11, '28.443430', '77.056070', '2018-01-11 11:39:55'),
(3529, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:39:57'),
(3530, 46, 11, '28.443430', '77.056070', '2018-01-11 11:40:00'),
(3531, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:00'),
(3532, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:03'),
(3533, 46, 11, '28.443430', '77.056070', '2018-01-11 11:40:05'),
(3534, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:06'),
(3535, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:09'),
(3536, 46, 11, '28.443430', '77.056070', '2018-01-11 11:40:10'),
(3537, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:12'),
(3538, 46, 11, '28.443430', '77.056070', '2018-01-11 11:40:15'),
(3539, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:15'),
(3540, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:18'),
(3541, 46, 11, '28.443430', '77.056070', '2018-01-11 11:40:20'),
(3542, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:21'),
(3543, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:24'),
(3544, 46, 11, '28.443430', '77.056066', '2018-01-11 11:40:25'),
(3545, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:27'),
(3546, 46, 11, '28.443430', '77.056066', '2018-01-11 11:40:30'),
(3547, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:30'),
(3548, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:33'),
(3549, 46, 11, '28.443430', '77.056066', '2018-01-11 11:40:35'),
(3550, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:36'),
(3551, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:39'),
(3552, 46, 11, '28.443430', '77.056066', '2018-01-11 11:40:40'),
(3553, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:42'),
(3554, 46, 11, '28.443430', '77.056066', '2018-01-11 11:40:45'),
(3555, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:45'),
(3556, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:48'),
(3557, 46, 11, '28.443430', '77.056066', '2018-01-11 11:40:50'),
(3558, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:51'),
(3559, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:54'),
(3560, 46, 11, '28.443430', '77.056066', '2018-01-11 11:40:55'),
(3561, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:40:57'),
(3562, 46, 11, '28.443430', '77.056066', '2018-01-11 11:41:00'),
(3563, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:01'),
(3564, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:03'),
(3565, 46, 11, '28.443430', '77.056066', '2018-01-11 11:41:05'),
(3566, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:06'),
(3567, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:09'),
(3568, 46, 11, '28.443430', '77.056066', '2018-01-11 11:41:10'),
(3569, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:12'),
(3570, 46, 11, '28.443430', '77.056066', '2018-01-11 11:41:15'),
(3571, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:15'),
(3572, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:18'),
(3573, 46, 11, '28.443430', '77.056066', '2018-01-11 11:41:20'),
(3574, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:21'),
(3575, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:24'),
(3576, 46, 11, '28.443430', '77.056066', '2018-01-11 11:41:25'),
(3577, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:27'),
(3578, 46, 11, '28.443430', '77.056066', '2018-01-11 11:41:30'),
(3579, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:30'),
(3580, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:33'),
(3581, 46, 11, '28.443430', '77.056066', '2018-01-11 11:41:35'),
(3582, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:36'),
(3583, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:39'),
(3584, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:42'),
(3585, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:45'),
(3586, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:48'),
(3587, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:51'),
(3588, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:54'),
(3589, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:41:57'),
(3590, 45, 2, '28.4432548', '77.055904', '2018-01-11 11:42:00'),
(3591, 47, 2, '28.4432566', '77.0559152', '2018-01-11 11:47:29'),
(3592, 47, 2, '28.4432566', '77.0559152', '2018-01-11 11:47:32'),
(3593, 47, 2, '28.4433267', '77.0559654', '2018-01-11 11:47:35'),
(3594, 47, 2, '28.4432947', '77.055935', '2018-01-11 11:47:38'),
(3595, 47, 2, '28.4432947', '77.055935', '2018-01-11 11:47:41'),
(3596, 47, 2, '28.4432873', '77.0559259', '2018-01-11 11:47:44'),
(3597, 47, 2, '28.4432873', '77.0559259', '2018-01-11 11:47:47'),
(3598, 47, 2, '28.4432915', '77.0559276', '2018-01-11 11:47:50'),
(3599, 47, 2, '28.4432906', '77.0559245', '2018-01-11 11:47:53'),
(3600, 47, 2, '28.4432906', '77.0559245', '2018-01-11 11:47:56'),
(3601, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:47:59'),
(3602, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:02'),
(3603, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:05'),
(3604, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:08'),
(3605, 47, 2, '28.4432887', '77.0559303', '2018-01-11 11:48:11'),
(3606, 47, 2, '28.4433022', '77.0559452', '2018-01-11 11:48:14'),
(3607, 47, 2, '28.4433022', '77.0559452', '2018-01-11 11:48:17'),
(3608, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:20'),
(3609, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:23'),
(3610, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:26'),
(3611, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:29'),
(3612, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:32'),
(3613, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:35'),
(3614, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:38'),
(3615, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:41'),
(3616, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:44'),
(3617, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:47'),
(3618, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:50'),
(3619, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:53'),
(3620, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:56'),
(3621, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:48:59'),
(3622, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:02'),
(3623, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:05'),
(3624, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:08'),
(3625, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:11'),
(3626, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:14'),
(3627, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:17'),
(3628, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:20'),
(3629, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:23'),
(3630, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:26'),
(3631, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:29'),
(3632, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:32'),
(3633, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:35'),
(3634, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:38'),
(3635, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:41'),
(3636, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:44'),
(3637, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:47'),
(3638, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:50'),
(3639, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:53'),
(3640, 47, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:56'),
(3641, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:49:59'),
(3642, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:02'),
(3643, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:05'),
(3644, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:08'),
(3645, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:11'),
(3646, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:14'),
(3647, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:17'),
(3648, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:20'),
(3649, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:24'),
(3650, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:27'),
(3651, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:31'),
(3652, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:34'),
(3653, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:37'),
(3654, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:40'),
(3655, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:44'),
(3656, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:47'),
(3657, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:50'),
(3658, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:53'),
(3659, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:50:57'),
(3660, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:00'),
(3661, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:03'),
(3662, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:06'),
(3663, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:09'),
(3664, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:12'),
(3665, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:15'),
(3666, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:18'),
(3667, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:25'),
(3668, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:25'),
(3669, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:28'),
(3670, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:31'),
(3671, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:34'),
(3672, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:37'),
(3673, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:40'),
(3674, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:44'),
(3675, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:47'),
(3676, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:50'),
(3677, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:53'),
(3678, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:56'),
(3679, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:51:59'),
(3680, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:02'),
(3681, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:05'),
(3682, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:09'),
(3683, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:12'),
(3684, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:15'),
(3685, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:18'),
(3686, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:21'),
(3687, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:24'),
(3688, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:28'),
(3689, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:30'),
(3690, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:33'),
(3691, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:37'),
(3692, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:40'),
(3693, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:43'),
(3694, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:46'),
(3695, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:50'),
(3696, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:53'),
(3697, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:52:57'),
(3698, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:00'),
(3699, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:03'),
(3700, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:07'),
(3701, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:10'),
(3702, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:13'),
(3703, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:16'),
(3704, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:19'),
(3705, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:22'),
(3706, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:25'),
(3707, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:28'),
(3708, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:31'),
(3709, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:34'),
(3710, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:37'),
(3711, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:41'),
(3712, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:44'),
(3713, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:47'),
(3714, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:50'),
(3715, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:53:53'),
(3716, 48, 2, '28.4432895', '77.0559174', '2018-01-11 11:54:05'),
(3717, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:54:07'),
(3718, 48, 2, '28.4433035', '77.0559459', '2018-01-11 11:54:10'),
(3719, 48, 2, '28.4433035', '77.0559459', '2018-01-11 11:54:13'),
(3720, 48, 2, '28.4432106', '77.0558788', '2018-01-11 11:54:16'),
(3721, 48, 2, '28.4433253', '77.0559552', '2018-01-11 11:54:49'),
(3722, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:54:52'),
(3723, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:54:55'),
(3724, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:54:58'),
(3725, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:01'),
(3726, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:04'),
(3727, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:07'),
(3728, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:10'),
(3729, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:13'),
(3730, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:16'),
(3731, 48, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:19'),
(3732, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:22'),
(3733, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:25'),
(3734, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:28'),
(3735, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:31'),
(3736, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:34'),
(3737, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:38'),
(3738, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:40'),
(3739, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:43'),
(3740, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:46'),
(3741, 49, 2, '28.4433012', '77.055897', '2018-01-11 11:55:46'),
(3742, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:49'),
(3743, 49, 2, '28.4432481', '77.0559052', '2018-01-11 11:55:49'),
(3744, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:53'),
(3745, 49, 2, '28.4432481', '77.0559052', '2018-01-11 11:55:53'),
(3746, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:55'),
(3747, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:55:55'),
(3748, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:55:58'),
(3749, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:55:58'),
(3750, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:01'),
(3751, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:01'),
(3752, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:04'),
(3753, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:04'),
(3754, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:07'),
(3755, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:07'),
(3756, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:10'),
(3757, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:10'),
(3758, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:13'),
(3759, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:14'),
(3760, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:16'),
(3761, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:16'),
(3762, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:19'),
(3763, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:19'),
(3764, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:22'),
(3765, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:22'),
(3766, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:25'),
(3767, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:25'),
(3768, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:28'),
(3769, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:28'),
(3770, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:31'),
(3771, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:31'),
(3772, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:34'),
(3773, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:34'),
(3774, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:37'),
(3775, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:37'),
(3776, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:40'),
(3777, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:40'),
(3778, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:43'),
(3779, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:43'),
(3780, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:46'),
(3781, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:46'),
(3782, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:49'),
(3783, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:50'),
(3784, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:52'),
(3785, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:52'),
(3786, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:55'),
(3787, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:55'),
(3788, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:56:58'),
(3789, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:56:58'),
(3790, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:01'),
(3791, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:01'),
(3792, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:04'),
(3793, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:04'),
(3794, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:07'),
(3795, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:08'),
(3796, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:10'),
(3797, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:10'),
(3798, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:13'),
(3799, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:13'),
(3800, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:16'),
(3801, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:16'),
(3802, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:19'),
(3803, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:19'),
(3804, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:22'),
(3805, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:22'),
(3806, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:25'),
(3807, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:25'),
(3808, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:28'),
(3809, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:28'),
(3810, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:31'),
(3811, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:31'),
(3812, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:34'),
(3813, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:34'),
(3814, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:37'),
(3815, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:37'),
(3816, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:40'),
(3817, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:40'),
(3818, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:43'),
(3819, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:43'),
(3820, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:46'),
(3821, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:46'),
(3822, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:49'),
(3823, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:49'),
(3824, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:52'),
(3825, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:52'),
(3826, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:55'),
(3827, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:55'),
(3828, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:57:58'),
(3829, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:57:58'),
(3830, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:01'),
(3831, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:01'),
(3832, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:04'),
(3833, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:04'),
(3834, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:07'),
(3835, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:07'),
(3836, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:10'),
(3837, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:10'),
(3838, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:13'),
(3839, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:13'),
(3840, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:16'),
(3841, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:16'),
(3842, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:19'),
(3843, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:19'),
(3844, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:22'),
(3845, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:22'),
(3846, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:25'),
(3847, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:25'),
(3848, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:28'),
(3849, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:28'),
(3850, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:31'),
(3851, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:31'),
(3852, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:34'),
(3853, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:34'),
(3854, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:37'),
(3855, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:37'),
(3856, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:40'),
(3857, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:40'),
(3858, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:43'),
(3859, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:43'),
(3860, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:46'),
(3861, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:46'),
(3862, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:49'),
(3863, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:49'),
(3864, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:52'),
(3865, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:52'),
(3866, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:55'),
(3867, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:55'),
(3868, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:58:58'),
(3869, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:58:58'),
(3870, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:01'),
(3871, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:01'),
(3872, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:04'),
(3873, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:04'),
(3874, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:07'),
(3875, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:07'),
(3876, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:10'),
(3877, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:10'),
(3878, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:13'),
(3879, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:13'),
(3880, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:16'),
(3881, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:17'),
(3882, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:19'),
(3883, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:19'),
(3884, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:22'),
(3885, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:22'),
(3886, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:25'),
(3887, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:25'),
(3888, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:28'),
(3889, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:29'),
(3890, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:31'),
(3891, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:31'),
(3892, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:34'),
(3893, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:35'),
(3894, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:37'),
(3895, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:38'),
(3896, 49, 2, '28.4433835', '77.0559973', '2018-01-11 11:59:40'),
(3897, 49, 2, '28.4432106', '77.0558788', '2018-01-11 11:59:41'),
(3898, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:15'),
(3899, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:18'),
(3900, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:21'),
(3901, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:24'),
(3902, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:27'),
(3903, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:30'),
(3904, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:33'),
(3905, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:36'),
(3906, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:39'),
(3907, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:42'),
(3908, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:45'),
(3909, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:51'),
(3910, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:52'),
(3911, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:55'),
(3912, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:06:57'),
(3913, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:01'),
(3914, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:03'),
(3915, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:06'),
(3916, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:10'),
(3917, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:12'),
(3918, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:15'),
(3919, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:18'),
(3920, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:21'),
(3921, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:24'),
(3922, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:27'),
(3923, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:30'),
(3924, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:33'),
(3925, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:36'),
(3926, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:39'),
(3927, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:42'),
(3928, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:45'),
(3929, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:48'),
(3930, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:51'),
(3931, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:54'),
(3932, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:07:57'),
(3933, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:00'),
(3934, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:03'),
(3935, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:06'),
(3936, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:09'),
(3937, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:12'),
(3938, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:15'),
(3939, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:18'),
(3940, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:21'),
(3941, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:24'),
(3942, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:27'),
(3943, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:30'),
(3944, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:33'),
(3945, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:36'),
(3946, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:39'),
(3947, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:42'),
(3948, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:45'),
(3949, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:48'),
(3950, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:52'),
(3951, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:54'),
(3952, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:08:57'),
(3953, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:00'),
(3954, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:03'),
(3955, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:06'),
(3956, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:09');
INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(3957, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:12'),
(3958, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:15'),
(3959, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:18'),
(3960, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:21'),
(3961, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:24'),
(3962, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:27'),
(3963, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:30'),
(3964, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:33'),
(3965, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:36'),
(3966, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:39'),
(3967, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:42'),
(3968, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:45'),
(3969, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:48'),
(3970, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:51'),
(3971, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:54'),
(3972, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:09:57'),
(3973, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:00'),
(3974, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:03'),
(3975, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:06'),
(3976, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:09'),
(3977, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:12'),
(3978, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:15'),
(3979, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:18'),
(3980, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:21'),
(3981, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:24'),
(3982, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:28'),
(3983, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:30'),
(3984, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:33'),
(3985, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:36'),
(3986, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:39'),
(3987, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:42'),
(3988, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:45'),
(3989, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:48'),
(3990, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:51'),
(3991, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:54'),
(3992, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:10:57'),
(3993, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:00'),
(3994, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:03'),
(3995, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:06'),
(3996, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:09'),
(3997, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:12'),
(3998, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:15'),
(3999, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:18'),
(4000, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:21'),
(4001, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:24'),
(4002, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:27'),
(4003, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:30'),
(4004, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:33'),
(4005, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:36'),
(4006, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:39'),
(4007, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:42'),
(4008, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:45'),
(4009, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:48'),
(4010, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:51'),
(4011, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:54'),
(4012, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:11:57'),
(4013, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:12:00'),
(4014, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:12:03'),
(4015, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:12:06'),
(4016, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:12:09'),
(4017, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:12:12'),
(4018, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:12:15'),
(4019, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:12:19'),
(4020, 50, 2, '28.4432106', '77.0558788', '2018-01-11 12:12:22'),
(4021, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:24'),
(4022, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:27'),
(4023, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:30'),
(4024, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:33'),
(4025, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:36'),
(4026, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:39'),
(4027, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:42'),
(4028, 51, 2, '28.4433032', '77.0559478', '2018-01-11 12:46:45'),
(4029, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:48'),
(4030, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:51'),
(4031, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:54'),
(4032, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:46:57'),
(4033, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:00'),
(4034, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:03'),
(4035, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:06'),
(4036, 51, 2, '28.4432759', '77.055913', '2018-01-11 12:47:09'),
(4037, 51, 2, '28.4432759', '77.055913', '2018-01-11 12:47:12'),
(4038, 51, 2, '28.4434019', '77.0560144', '2018-01-11 12:47:15'),
(4039, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:18'),
(4040, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:21'),
(4041, 51, 2, '28.4432654', '77.0559082', '2018-01-11 12:47:24'),
(4042, 51, 2, '28.4432654', '77.0559082', '2018-01-11 12:47:27'),
(4043, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:30'),
(4044, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:33'),
(4045, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:36'),
(4046, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:39'),
(4047, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:42'),
(4048, 51, 2, '28.4432383', '77.0559014', '2018-01-11 12:47:45'),
(4049, 51, 2, '28.4432895', '77.0559174', '2018-01-11 12:47:48'),
(4050, 51, 2, '28.4432895', '77.0559174', '2018-01-11 12:47:51'),
(4051, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:54'),
(4052, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:47:57'),
(4053, 51, 2, '28.4432911', '77.0559189', '2018-01-11 12:48:00'),
(4054, 51, 2, '28.4432911', '77.0559189', '2018-01-11 12:48:03'),
(4055, 51, 2, '28.4432911', '77.0559189', '2018-01-11 12:48:06'),
(4056, 51, 2, '28.4432895', '77.0559174', '2018-01-11 12:48:09'),
(4057, 51, 2, '28.4432895', '77.0559174', '2018-01-11 12:48:12'),
(4058, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:48:15'),
(4059, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:48:18'),
(4060, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:48:21'),
(4061, 51, 2, '28.4432654', '77.0559082', '2018-01-11 12:48:24'),
(4062, 51, 2, '28.4432654', '77.0559082', '2018-01-11 12:48:27'),
(4063, 51, 2, '28.44325', '77.0559021', '2018-01-11 12:48:30'),
(4064, 51, 2, '28.4432832', '77.055915', '2018-01-11 12:48:33'),
(4065, 51, 2, '28.4432832', '77.055915', '2018-01-11 12:48:36'),
(4066, 51, 2, '28.4433369', '77.0559352', '2018-01-11 12:48:39'),
(4067, 51, 2, '28.4433369', '77.0559352', '2018-01-11 12:48:42'),
(4068, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:48:45'),
(4069, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:48:48'),
(4070, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:48:51'),
(4071, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:48:54'),
(4072, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:48:57'),
(4073, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:00'),
(4074, 51, 2, '28.4432618', '77.0559073', '2018-01-11 12:49:03'),
(4075, 51, 2, '28.4432618', '77.0559073', '2018-01-11 12:49:06'),
(4076, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:09'),
(4077, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:12'),
(4078, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:15'),
(4079, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:18'),
(4080, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:21'),
(4081, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:24'),
(4082, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:27'),
(4083, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:30'),
(4084, 51, 2, '28.4434019', '77.0560144', '2018-01-11 12:49:33'),
(4085, 51, 2, '28.4434019', '77.0560144', '2018-01-11 12:49:36'),
(4086, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:39'),
(4087, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:42'),
(4088, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:45'),
(4089, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:48'),
(4090, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:51'),
(4091, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:54'),
(4092, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:49:57'),
(4093, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:00'),
(4094, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:03'),
(4095, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:06'),
(4096, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:09'),
(4097, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:12'),
(4098, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:15'),
(4099, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:18'),
(4100, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:21'),
(4101, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:24'),
(4102, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:27'),
(4103, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:30'),
(4104, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:33'),
(4105, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:36'),
(4106, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:39'),
(4107, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:42'),
(4108, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:45'),
(4109, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:48'),
(4110, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:51'),
(4111, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:54'),
(4112, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:50:57'),
(4113, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:00'),
(4114, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:03'),
(4115, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:06'),
(4116, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:09'),
(4117, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:12'),
(4118, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:15'),
(4119, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:18'),
(4120, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:21'),
(4121, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:24'),
(4122, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:27'),
(4123, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:30'),
(4124, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:33'),
(4125, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:36'),
(4126, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:39'),
(4127, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:42'),
(4128, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:45'),
(4129, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:48'),
(4130, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:51'),
(4131, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:54'),
(4132, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:51:57'),
(4133, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:00'),
(4134, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:03'),
(4135, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:06'),
(4136, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:09'),
(4137, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:12'),
(4138, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:15'),
(4139, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:18'),
(4140, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:21'),
(4141, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:24'),
(4142, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:27'),
(4143, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:30'),
(4144, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:33'),
(4145, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:36'),
(4146, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:39'),
(4147, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:42'),
(4148, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:45'),
(4149, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:48'),
(4150, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:51'),
(4151, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:54'),
(4152, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:52:57'),
(4153, 51, 2, '28.4432106', '77.0558788', '2018-01-11 12:53:00'),
(4154, 52, 2, '28.4432029', '77.0558711', '2018-01-11 13:35:47'),
(4155, 52, 2, '28.4432029', '77.0558711', '2018-01-11 13:35:50'),
(4156, 52, 2, '28.4432719', '77.0559214', '2018-01-11 13:35:53'),
(4157, 52, 2, '28.4432198', '77.0558766', '2018-01-11 13:35:56'),
(4158, 52, 2, '28.4432198', '77.0558766', '2018-01-11 13:35:59'),
(4159, 52, 2, '28.4432265', '77.0558875', '2018-01-11 13:36:02'),
(4160, 52, 2, '28.4432265', '77.0558875', '2018-01-11 13:36:05'),
(4161, 52, 2, '28.4432169', '77.055885', '2018-01-11 13:36:08'),
(4162, 52, 2, '28.4432199', '77.0558872', '2018-01-11 13:36:11'),
(4163, 52, 2, '28.4432199', '77.0558872', '2018-01-11 13:36:14'),
(4164, 52, 2, '28.4431913', '77.0558737', '2018-01-11 13:36:17'),
(4165, 52, 2, '28.4431913', '77.0558737', '2018-01-11 13:36:20'),
(4166, 52, 2, '28.4432724', '77.0559211', '2018-01-11 13:36:23'),
(4167, 52, 2, '28.4432149', '77.0558855', '2018-01-11 13:36:26'),
(4168, 52, 2, '28.4432149', '77.0558855', '2018-01-11 13:36:29'),
(4169, 52, 2, '28.4432584', '77.055918', '2018-01-11 13:36:32'),
(4170, 52, 2, '28.4432584', '77.055918', '2018-01-11 13:36:35'),
(4171, 52, 2, '28.4432998', '77.0559441', '2018-01-11 13:36:38'),
(4172, 52, 2, '28.4432604', '77.0559194', '2018-01-11 13:36:41'),
(4173, 52, 2, '28.4432604', '77.0559194', '2018-01-11 13:36:44'),
(4174, 52, 2, '28.4432613', '77.0559192', '2018-01-11 13:36:47'),
(4175, 52, 2, '28.4432613', '77.0559192', '2018-01-11 13:36:50'),
(4176, 52, 2, '28.4432651', '77.0559191', '2018-01-11 13:36:53'),
(4177, 52, 2, '28.4433028', '77.055936', '2018-01-11 13:36:56'),
(4178, 52, 2, '28.4433028', '77.055936', '2018-01-11 13:36:59'),
(4179, 52, 2, '28.4432776', '77.0559236', '2018-01-11 13:37:02'),
(4180, 52, 2, '28.4432776', '77.0559236', '2018-01-11 13:37:05'),
(4181, 52, 2, '28.4432649', '77.0559195', '2018-01-11 13:37:08'),
(4182, 52, 2, '28.4432649', '77.0559195', '2018-01-11 13:37:11'),
(4183, 52, 2, '28.4432358', '77.0558976', '2018-01-11 13:37:14'),
(4184, 52, 2, '28.4432372', '77.0558803', '2018-01-11 13:37:17'),
(4185, 52, 2, '28.4432372', '77.0558803', '2018-01-11 13:37:20'),
(4186, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:23'),
(4187, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:26'),
(4188, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:29'),
(4189, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:32'),
(4190, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:35'),
(4191, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:38'),
(4192, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:41'),
(4193, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:44'),
(4194, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:47'),
(4195, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:50'),
(4196, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:53'),
(4197, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:56'),
(4198, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:37:59'),
(4199, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:02'),
(4200, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:05'),
(4201, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:08'),
(4202, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:11'),
(4203, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:15'),
(4204, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:18'),
(4205, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:21'),
(4206, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:24'),
(4207, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:27'),
(4208, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:30'),
(4209, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:33'),
(4210, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:36'),
(4211, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:39'),
(4212, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:42'),
(4213, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:45'),
(4214, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:48'),
(4215, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:51'),
(4216, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:54'),
(4217, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:38:57'),
(4218, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:00'),
(4219, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:03'),
(4220, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:06'),
(4221, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:09'),
(4222, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:12'),
(4223, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:15'),
(4224, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:18'),
(4225, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:21'),
(4226, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:24'),
(4227, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:27'),
(4228, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:30'),
(4229, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:33'),
(4230, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:36'),
(4231, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:39'),
(4232, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:42'),
(4233, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:45'),
(4234, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:48'),
(4235, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:51'),
(4236, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:54'),
(4237, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:39:57'),
(4238, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:00'),
(4239, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:03'),
(4240, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:06'),
(4241, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:09'),
(4242, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:12'),
(4243, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:15'),
(4244, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:18'),
(4245, 52, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:21'),
(4246, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:24'),
(4247, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:27'),
(4248, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:30'),
(4249, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:33'),
(4250, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:36'),
(4251, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:39'),
(4252, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:42'),
(4253, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:45'),
(4254, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:40:48'),
(4255, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:48'),
(4256, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:40:51'),
(4257, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:51'),
(4258, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:40:54'),
(4259, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:54'),
(4260, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:40:57'),
(4261, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:40:57'),
(4262, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:00'),
(4263, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:00'),
(4264, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:03'),
(4265, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:03'),
(4266, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:06'),
(4267, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:06'),
(4268, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:09'),
(4269, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:09'),
(4270, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:12'),
(4271, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:12'),
(4272, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:15'),
(4273, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:15'),
(4274, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:18'),
(4275, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:18'),
(4276, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:21'),
(4277, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:21'),
(4278, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:24'),
(4279, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:24'),
(4280, 53, 2, '28.4432816', '77.0558757', '2018-01-11 13:41:27'),
(4281, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:27'),
(4282, 53, 2, '28.4432816', '77.0558757', '2018-01-11 13:41:30'),
(4283, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:30'),
(4284, 53, 2, '28.4431582', '77.0558272', '2018-01-11 13:41:33'),
(4285, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:33'),
(4286, 53, 2, '28.4431582', '77.0558272', '2018-01-11 13:41:36'),
(4287, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:36'),
(4288, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:39'),
(4289, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:39'),
(4290, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:42'),
(4291, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:42'),
(4292, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:45'),
(4293, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:45'),
(4294, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:48'),
(4295, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:48'),
(4296, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:51'),
(4297, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:51'),
(4298, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:54'),
(4299, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:54'),
(4300, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:41:57'),
(4301, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:41:57'),
(4302, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:00'),
(4303, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:00'),
(4304, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:03'),
(4305, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:03'),
(4306, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:06'),
(4307, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:06'),
(4308, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:09'),
(4309, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:09'),
(4310, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:12'),
(4311, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:12'),
(4312, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:15'),
(4313, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:15'),
(4314, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:18'),
(4315, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:18'),
(4316, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:21'),
(4317, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:21'),
(4318, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:24'),
(4319, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:24'),
(4320, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:27'),
(4321, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:27'),
(4322, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:30'),
(4323, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:30'),
(4324, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:33'),
(4325, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:33'),
(4326, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:36'),
(4327, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:36'),
(4328, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:39'),
(4329, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:39'),
(4330, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:42'),
(4331, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:42'),
(4332, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:45'),
(4333, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:45'),
(4334, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:48'),
(4335, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:48'),
(4336, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:51'),
(4337, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:51'),
(4338, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:54'),
(4339, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:54'),
(4340, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:42:57'),
(4341, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:42:57'),
(4342, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:00'),
(4343, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:00'),
(4344, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:03'),
(4345, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:03'),
(4346, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:06'),
(4347, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:06'),
(4348, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:09'),
(4349, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:09'),
(4350, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:12'),
(4351, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:12'),
(4352, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:15'),
(4353, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:15'),
(4354, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:18'),
(4355, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:18'),
(4356, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:21'),
(4357, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:21'),
(4358, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:24'),
(4359, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:24'),
(4360, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:27'),
(4361, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:27'),
(4362, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:30'),
(4363, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:30'),
(4364, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:33'),
(4365, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:33'),
(4366, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:36'),
(4367, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:36'),
(4368, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:39'),
(4369, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:39'),
(4370, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:42'),
(4371, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:42'),
(4372, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:45'),
(4373, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:45'),
(4374, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:48'),
(4375, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:48'),
(4376, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:51'),
(4377, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:51'),
(4378, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:54'),
(4379, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:54'),
(4380, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:43:57'),
(4381, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:43:57'),
(4382, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:00'),
(4383, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:00'),
(4384, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:03'),
(4385, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:03'),
(4386, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:06'),
(4387, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:06'),
(4388, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:09'),
(4389, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:09'),
(4390, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:12'),
(4391, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:12'),
(4392, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:15'),
(4393, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:15'),
(4394, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:18'),
(4395, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:18'),
(4396, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:21'),
(4397, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:21'),
(4398, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:24'),
(4399, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:24'),
(4400, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:27'),
(4401, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:27'),
(4402, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:30'),
(4403, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:30'),
(4404, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:33'),
(4405, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:33'),
(4406, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:36'),
(4407, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:36'),
(4408, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:39'),
(4409, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:39'),
(4410, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:42'),
(4411, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:42'),
(4412, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:45'),
(4413, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:45'),
(4414, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:48'),
(4415, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:48'),
(4416, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:51'),
(4417, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:51'),
(4418, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:54'),
(4419, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:54'),
(4420, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:44:57'),
(4421, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:44:57'),
(4422, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:00'),
(4423, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:00'),
(4424, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:03'),
(4425, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:03'),
(4426, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:06'),
(4427, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:06'),
(4428, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:09'),
(4429, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:09'),
(4430, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:12'),
(4431, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:12'),
(4432, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:15'),
(4433, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:15'),
(4434, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:18'),
(4435, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:18'),
(4436, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:21'),
(4437, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:21'),
(4438, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:24'),
(4439, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:24'),
(4440, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:27'),
(4441, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:27'),
(4442, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:30'),
(4443, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:30'),
(4444, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:33'),
(4445, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:33'),
(4446, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:36'),
(4447, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:36'),
(4448, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:39'),
(4449, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:39'),
(4450, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:42'),
(4451, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:42'),
(4452, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:45'),
(4453, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:45'),
(4454, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:48'),
(4455, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:48'),
(4456, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:51'),
(4457, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:51'),
(4458, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:54'),
(4459, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:54'),
(4460, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:45:57'),
(4461, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:45:57'),
(4462, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:00'),
(4463, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:00'),
(4464, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:03'),
(4465, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:03'),
(4466, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:06'),
(4467, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:06'),
(4468, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:09'),
(4469, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:09'),
(4470, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:12'),
(4471, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:12'),
(4472, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:15'),
(4473, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:15'),
(4474, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:18'),
(4475, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:18'),
(4476, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:21'),
(4477, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:21'),
(4478, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:24'),
(4479, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:24'),
(4480, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:27'),
(4481, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:27'),
(4482, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:30'),
(4483, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:30'),
(4484, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:33'),
(4485, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:33'),
(4486, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:36'),
(4487, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:36'),
(4488, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:39'),
(4489, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:39'),
(4490, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:42'),
(4491, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:42'),
(4492, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:45'),
(4493, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:45'),
(4494, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:48'),
(4495, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:48'),
(4496, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:51'),
(4497, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:51'),
(4498, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:54'),
(4499, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:54'),
(4500, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:46:57'),
(4501, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:46:57'),
(4502, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:00'),
(4503, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:00'),
(4504, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:03'),
(4505, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:03'),
(4506, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:06'),
(4507, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:06'),
(4508, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:09'),
(4509, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:09'),
(4510, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:12'),
(4511, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:12'),
(4512, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:15'),
(4513, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:15'),
(4514, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:18'),
(4515, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:18'),
(4516, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:21'),
(4517, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:21'),
(4518, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:24'),
(4519, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:24'),
(4520, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:27'),
(4521, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:27'),
(4522, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:30'),
(4523, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:30'),
(4524, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:33'),
(4525, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:33'),
(4526, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:36'),
(4527, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:36'),
(4528, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:39'),
(4529, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:39'),
(4530, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:42'),
(4531, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:42'),
(4532, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:45'),
(4533, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:45'),
(4534, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:48'),
(4535, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:48'),
(4536, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:51'),
(4537, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:51'),
(4538, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:54'),
(4539, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:54'),
(4540, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:47:57'),
(4541, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:47:57'),
(4542, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:00'),
(4543, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:00'),
(4544, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:03'),
(4545, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:03'),
(4546, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:06'),
(4547, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:06'),
(4548, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:09'),
(4549, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:09'),
(4550, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:12'),
(4551, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:12'),
(4552, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:15'),
(4553, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:15'),
(4554, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:18'),
(4555, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:18'),
(4556, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:21'),
(4557, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:21'),
(4558, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:24'),
(4559, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:24'),
(4560, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:27'),
(4561, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:27'),
(4562, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:30'),
(4563, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:30'),
(4564, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:33'),
(4565, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:33'),
(4566, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:36'),
(4567, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:36'),
(4568, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:39'),
(4569, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:39'),
(4570, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:42'),
(4571, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:42'),
(4572, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:45'),
(4573, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:45'),
(4574, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:48'),
(4575, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:48'),
(4576, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:51'),
(4577, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:51'),
(4578, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:54'),
(4579, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:54'),
(4580, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:48:57'),
(4581, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:48:57'),
(4582, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:00'),
(4583, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:00'),
(4584, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:03'),
(4585, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:03'),
(4586, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:06'),
(4587, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:06'),
(4588, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:09'),
(4589, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:09'),
(4590, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:12'),
(4591, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:12'),
(4592, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:15'),
(4593, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:15'),
(4594, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:18'),
(4595, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:18'),
(4596, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:21'),
(4597, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:21'),
(4598, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:24'),
(4599, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:24'),
(4600, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:27'),
(4601, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:27'),
(4602, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:30'),
(4603, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:30'),
(4604, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:33'),
(4605, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:33'),
(4606, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:36'),
(4607, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:36'),
(4608, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:39'),
(4609, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:39'),
(4610, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:42'),
(4611, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:42'),
(4612, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:45'),
(4613, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:45'),
(4614, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:48'),
(4615, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:48'),
(4616, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:51'),
(4617, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:51'),
(4618, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:54'),
(4619, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:54'),
(4620, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:49:57'),
(4621, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:49:57'),
(4622, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:00'),
(4623, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:00'),
(4624, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:03'),
(4625, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:03'),
(4626, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:06'),
(4627, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:06'),
(4628, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:09'),
(4629, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:09'),
(4630, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:12'),
(4631, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:12'),
(4632, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:15'),
(4633, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:15'),
(4634, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:18'),
(4635, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:18'),
(4636, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:21'),
(4637, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:21'),
(4638, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:24'),
(4639, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:24'),
(4640, 53, 2, '28.4432829', '77.0559319', '2018-01-11 13:50:27'),
(4641, 53, 2, '28.4431437', '77.0558317', '2018-01-11 13:50:27'),
(4642, 56, 2, '28.4432446', '77.0559051', '2018-01-12 08:27:00'),
(4643, 56, 2, '28.4432446', '77.0559051', '2018-01-12 08:27:03'),
(4644, 56, 2, '28.4432408', '77.0559039', '2018-01-12 08:27:06'),
(4645, 56, 2, '28.4432524', '77.0559076', '2018-01-12 08:27:09'),
(4646, 56, 2, '28.4432524', '77.0559076', '2018-01-12 08:27:12'),
(4647, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:27:15'),
(4648, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:27:18'),
(4649, 56, 2, '28.4432707', '77.0559122', '2018-01-12 08:27:21'),
(4650, 56, 2, '28.4432524', '77.0559076', '2018-01-12 08:27:24'),
(4651, 56, 2, '28.4432524', '77.0559076', '2018-01-12 08:27:27'),
(4652, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:27:30'),
(4653, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:27:33'),
(4654, 56, 2, '28.4432564', '77.0559089', '2018-01-12 08:27:36'),
(4655, 56, 2, '28.4432524', '77.0559076', '2018-01-12 08:27:39'),
(4656, 56, 2, '28.4432524', '77.0559076', '2018-01-12 08:27:42'),
(4657, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:27:45'),
(4658, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:27:48'),
(4659, 56, 2, '28.4432707', '77.0559122', '2018-01-12 08:27:51'),
(4660, 56, 2, '28.4432408', '77.0559039', '2018-01-12 08:27:54'),
(4661, 56, 2, '28.4432408', '77.0559039', '2018-01-12 08:27:57'),
(4662, 56, 2, '28.4432372', '77.0559026', '2018-01-12 08:28:00'),
(4663, 56, 2, '28.4432372', '77.0559026', '2018-01-12 08:28:03'),
(4664, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:06'),
(4665, 56, 2, '28.4432672', '77.0559109', '2018-01-12 08:28:09'),
(4666, 56, 2, '28.4432672', '77.0559109', '2018-01-12 08:28:12'),
(4667, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:15'),
(4668, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:18'),
(4669, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:21'),
(4670, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:24'),
(4671, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:27'),
(4672, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:30'),
(4673, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:33'),
(4674, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:36'),
(4675, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:39'),
(4676, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:42'),
(4677, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:45'),
(4678, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:48'),
(4679, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:51'),
(4680, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:54'),
(4681, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:28:57'),
(4682, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:00'),
(4683, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:03'),
(4684, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:06'),
(4685, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:09'),
(4686, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:12'),
(4687, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:15'),
(4688, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:18'),
(4689, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:21'),
(4690, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:24'),
(4691, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:27'),
(4692, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:30'),
(4693, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:33'),
(4694, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:36'),
(4695, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:39'),
(4696, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:42'),
(4697, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:45'),
(4698, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:48'),
(4699, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:51'),
(4700, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:54'),
(4701, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:29:57'),
(4702, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:00'),
(4703, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:03'),
(4704, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:06'),
(4705, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:09'),
(4706, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:12'),
(4707, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:15'),
(4708, 56, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:18'),
(4709, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:21'),
(4710, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:24'),
(4711, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:27'),
(4712, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:30'),
(4713, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:33'),
(4714, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:36'),
(4715, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:39'),
(4716, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:30:41'),
(4717, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:42'),
(4718, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:30:44'),
(4719, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:45'),
(4720, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:30:47'),
(4721, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:48'),
(4722, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:30:50'),
(4723, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:51'),
(4724, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:30:53'),
(4725, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:54'),
(4726, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:30:56'),
(4727, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:30:57'),
(4728, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:30:59'),
(4729, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:00'),
(4730, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:02'),
(4731, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:03'),
(4732, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:05'),
(4733, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:06'),
(4734, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:08'),
(4735, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:09');
INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(4736, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:11'),
(4737, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:12'),
(4738, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:14'),
(4739, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:15'),
(4740, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:17'),
(4741, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:19'),
(4742, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:20'),
(4743, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:21'),
(4744, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:23'),
(4745, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:24'),
(4746, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:26'),
(4747, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:27'),
(4748, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:29'),
(4749, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:30'),
(4750, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:32'),
(4751, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:33'),
(4752, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:35'),
(4753, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:36'),
(4754, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:38'),
(4755, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:39'),
(4756, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:41'),
(4757, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:42'),
(4758, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:44'),
(4759, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:45'),
(4760, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:47'),
(4761, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:48'),
(4762, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:50'),
(4763, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:51'),
(4764, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:53'),
(4765, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:54'),
(4766, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:56'),
(4767, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:31:57'),
(4768, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:31:59'),
(4769, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:00'),
(4770, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:02'),
(4771, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:03'),
(4772, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:05'),
(4773, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:06'),
(4774, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:08'),
(4775, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:09'),
(4776, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:11'),
(4777, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:12'),
(4778, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:14'),
(4779, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:15'),
(4780, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:17'),
(4781, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:18'),
(4782, 57, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:20'),
(4783, 57, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:21'),
(4784, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:23'),
(4785, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:24'),
(4786, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:26'),
(4787, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:27'),
(4788, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:29'),
(4789, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:30'),
(4790, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:32'),
(4791, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:33'),
(4792, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:35'),
(4793, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:36'),
(4794, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:38'),
(4795, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:39'),
(4796, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:41'),
(4797, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:42'),
(4798, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:44'),
(4799, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:45'),
(4800, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:47'),
(4801, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:48'),
(4802, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:50'),
(4803, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:51'),
(4804, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:53'),
(4805, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:54'),
(4806, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:56'),
(4807, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:32:57'),
(4808, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:32:59'),
(4809, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:00'),
(4810, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:02'),
(4811, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:03'),
(4812, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:05'),
(4813, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:06'),
(4814, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:08'),
(4815, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:09'),
(4816, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:11'),
(4817, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:12'),
(4818, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:14'),
(4819, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:15'),
(4820, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:17'),
(4821, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:18'),
(4822, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:20'),
(4823, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:21'),
(4824, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:23'),
(4825, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:24'),
(4826, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:26'),
(4827, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:27'),
(4828, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:29'),
(4829, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:30'),
(4830, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:32'),
(4831, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:33'),
(4832, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:35'),
(4833, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:36'),
(4834, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:38'),
(4835, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:39'),
(4836, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:41'),
(4837, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:42'),
(4838, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:44'),
(4839, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:45'),
(4840, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:47'),
(4841, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:48'),
(4842, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:50'),
(4843, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:51'),
(4844, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:53'),
(4845, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:54'),
(4846, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:55'),
(4847, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:56'),
(4848, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:33:57'),
(4849, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:58'),
(4850, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:33:59'),
(4851, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:00'),
(4852, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:01'),
(4853, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:02'),
(4854, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:03'),
(4855, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:04'),
(4856, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:05'),
(4857, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:06'),
(4858, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:07'),
(4859, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:08'),
(4860, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:09'),
(4861, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:10'),
(4862, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:11'),
(4863, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:12'),
(4864, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:13'),
(4865, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:14'),
(4866, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:15'),
(4867, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:16'),
(4868, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:17'),
(4869, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:18'),
(4870, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:19'),
(4871, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:20'),
(4872, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:21'),
(4873, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:22'),
(4874, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:23'),
(4875, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:24'),
(4876, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:25'),
(4877, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:26'),
(4878, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:27'),
(4879, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:28'),
(4880, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:29'),
(4881, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:30'),
(4882, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:31'),
(4883, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:32'),
(4884, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:33'),
(4885, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:34'),
(4886, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:35'),
(4887, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:36'),
(4888, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:37'),
(4889, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:38'),
(4890, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:39'),
(4891, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:40'),
(4892, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:41'),
(4893, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:42'),
(4894, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:43'),
(4895, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:44'),
(4896, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:45'),
(4897, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:46'),
(4898, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:47'),
(4899, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:48'),
(4900, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:51'),
(4901, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:51'),
(4902, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:51'),
(4903, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:54'),
(4904, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:54'),
(4905, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:54'),
(4906, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:57'),
(4907, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:34:57'),
(4908, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:34:57'),
(4909, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:00'),
(4910, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:00'),
(4911, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:00'),
(4912, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:03'),
(4913, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:03'),
(4914, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:04'),
(4915, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:06'),
(4916, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:07'),
(4917, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:07'),
(4918, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:09'),
(4919, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:10'),
(4920, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:10'),
(4921, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:12'),
(4922, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:13'),
(4923, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:13'),
(4924, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:15'),
(4925, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:16'),
(4926, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:16'),
(4927, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:18'),
(4928, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:19'),
(4929, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:19'),
(4930, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:21'),
(4931, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:22'),
(4932, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:22'),
(4933, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:24'),
(4934, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:25'),
(4935, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:25'),
(4936, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:27'),
(4937, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:28'),
(4938, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:28'),
(4939, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:30'),
(4940, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:31'),
(4941, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:31'),
(4942, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:33'),
(4943, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:34'),
(4944, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:34'),
(4945, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:37'),
(4946, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:37'),
(4947, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:37'),
(4948, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:39'),
(4949, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:40'),
(4950, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:40'),
(4951, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:42'),
(4952, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:43'),
(4953, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:43'),
(4954, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:45'),
(4955, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:46'),
(4956, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:46'),
(4957, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:48'),
(4958, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:49'),
(4959, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:49'),
(4960, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:51'),
(4961, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:52'),
(4962, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:52'),
(4963, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:54'),
(4964, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:55'),
(4965, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:55'),
(4966, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:57'),
(4967, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:35:58'),
(4968, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:35:58'),
(4969, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:00'),
(4970, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:01'),
(4971, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:01'),
(4972, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:03'),
(4973, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:04'),
(4974, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:04'),
(4975, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:06'),
(4976, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:07'),
(4977, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:07'),
(4978, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:09'),
(4979, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:10'),
(4980, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:10'),
(4981, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:12'),
(4982, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:13'),
(4983, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:13'),
(4984, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:16'),
(4985, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:16'),
(4986, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:16'),
(4987, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:19'),
(4988, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:19'),
(4989, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:19'),
(4990, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:22'),
(4991, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:22'),
(4992, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:22'),
(4993, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:24'),
(4994, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:25'),
(4995, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:25'),
(4996, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:28'),
(4997, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:28'),
(4998, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:28'),
(4999, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:30'),
(5000, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:31'),
(5001, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:31'),
(5002, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:33'),
(5003, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:34'),
(5004, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:34'),
(5005, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:37'),
(5006, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:37'),
(5007, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:37'),
(5008, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:39'),
(5009, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:40'),
(5010, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:40'),
(5011, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:43'),
(5012, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:43'),
(5013, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:43'),
(5014, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:45'),
(5015, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:46'),
(5016, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:46'),
(5017, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:49'),
(5018, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:49'),
(5019, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:49'),
(5020, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:51'),
(5021, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:52'),
(5022, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:52'),
(5023, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:55'),
(5024, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:55'),
(5025, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:55'),
(5026, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:57'),
(5027, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:36:58'),
(5028, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:36:58'),
(5029, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:01'),
(5030, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:01'),
(5031, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:01'),
(5032, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:04'),
(5033, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:04'),
(5034, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:04'),
(5035, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:07'),
(5036, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:07'),
(5037, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:07'),
(5038, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:10'),
(5039, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:10'),
(5040, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:10'),
(5041, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:13'),
(5042, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:13'),
(5043, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:13'),
(5044, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:16'),
(5045, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:16'),
(5046, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:16'),
(5047, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:19'),
(5048, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:19'),
(5049, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:19'),
(5050, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:22'),
(5051, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:22'),
(5052, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:22'),
(5053, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:25'),
(5054, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:25'),
(5055, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:25'),
(5056, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:28'),
(5057, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:28'),
(5058, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:28'),
(5059, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:31'),
(5060, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:31'),
(5061, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:31'),
(5062, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:34'),
(5063, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:34'),
(5064, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:34'),
(5065, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:37'),
(5066, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:37'),
(5067, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:37'),
(5068, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:40'),
(5069, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:40'),
(5070, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:40'),
(5071, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:43'),
(5072, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:43'),
(5073, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:43'),
(5074, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:46'),
(5075, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:46'),
(5076, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:46'),
(5077, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:49'),
(5078, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:49'),
(5079, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:49'),
(5080, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:52'),
(5081, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:52'),
(5082, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:52'),
(5083, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:55'),
(5084, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:55'),
(5085, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:55'),
(5086, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:58'),
(5087, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:37:58'),
(5088, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:37:58'),
(5089, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:01'),
(5090, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:01'),
(5091, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:01'),
(5092, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:04'),
(5093, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:04'),
(5094, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:04'),
(5095, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:07'),
(5096, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:07'),
(5097, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:07'),
(5098, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:10'),
(5099, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:10'),
(5100, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:10'),
(5101, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:13'),
(5102, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:13'),
(5103, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:13'),
(5104, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:16'),
(5105, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:16'),
(5106, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:16'),
(5107, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:19'),
(5108, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:19'),
(5109, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:19'),
(5110, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:22'),
(5111, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:22'),
(5112, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:22'),
(5113, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:25'),
(5114, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:25'),
(5115, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:25'),
(5116, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:28'),
(5117, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:28'),
(5118, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:28'),
(5119, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:31'),
(5120, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:31'),
(5121, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:31'),
(5122, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:34'),
(5123, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:34'),
(5124, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:34'),
(5125, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:37'),
(5126, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:37'),
(5127, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:37'),
(5128, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:40'),
(5129, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:40'),
(5130, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:40'),
(5131, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:43'),
(5132, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:43'),
(5133, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:43'),
(5134, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:46'),
(5135, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:46'),
(5136, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:46'),
(5137, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:49'),
(5138, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:49'),
(5139, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:49'),
(5140, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:52'),
(5141, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:52'),
(5142, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:52'),
(5143, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:55'),
(5144, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:55'),
(5145, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:55'),
(5146, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:58'),
(5147, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:38:58'),
(5148, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:38:58'),
(5149, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:01'),
(5150, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:01'),
(5151, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:01'),
(5152, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:04'),
(5153, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:04'),
(5154, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:04'),
(5155, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:07'),
(5156, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:07'),
(5157, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:07'),
(5158, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:10'),
(5159, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:10'),
(5160, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:10'),
(5161, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:13'),
(5162, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:13'),
(5163, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:13'),
(5164, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:16'),
(5165, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:16'),
(5166, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:16'),
(5167, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:19'),
(5168, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:19'),
(5169, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:19'),
(5170, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:22'),
(5171, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:22'),
(5172, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:22'),
(5173, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:25'),
(5174, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:25'),
(5175, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:25'),
(5176, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:28'),
(5177, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:28'),
(5178, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:28'),
(5179, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:31'),
(5180, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:31'),
(5181, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:31'),
(5182, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:34'),
(5183, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:34'),
(5184, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:34'),
(5185, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:37'),
(5186, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:37'),
(5187, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:37'),
(5188, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:40'),
(5189, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:40'),
(5190, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:40'),
(5191, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:43'),
(5192, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:43'),
(5193, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:43'),
(5194, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:46'),
(5195, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:46'),
(5196, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:46'),
(5197, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:49'),
(5198, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:49'),
(5199, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:49'),
(5200, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:52'),
(5201, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:52'),
(5202, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:52'),
(5203, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:55'),
(5204, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:55'),
(5205, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:55'),
(5206, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:58'),
(5207, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:39:58'),
(5208, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:39:58'),
(5209, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:01'),
(5210, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:01'),
(5211, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:01'),
(5212, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:04'),
(5213, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:04'),
(5214, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:04'),
(5215, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:07'),
(5216, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:07'),
(5217, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:07'),
(5218, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:10'),
(5219, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:10'),
(5220, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:10'),
(5221, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:13'),
(5222, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:13'),
(5223, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:13'),
(5224, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:16'),
(5225, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:16'),
(5226, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:16'),
(5227, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:19'),
(5228, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:19'),
(5229, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:19'),
(5230, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:22'),
(5231, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:22'),
(5232, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:22'),
(5233, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:25'),
(5234, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:25'),
(5235, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:25'),
(5236, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:28'),
(5237, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:28'),
(5238, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:28'),
(5239, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:31'),
(5240, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:31'),
(5241, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:31'),
(5242, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:34'),
(5243, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:34'),
(5244, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:34'),
(5245, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:37'),
(5246, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:37'),
(5247, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:37'),
(5248, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:40'),
(5249, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:40'),
(5250, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:40'),
(5251, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:43'),
(5252, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:43'),
(5253, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:43'),
(5254, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:46'),
(5255, 58, 2, '28.4432484', '77.0559064', '2018-01-12 08:40:46'),
(5256, 58, 2, '28.4432106', '77.0558788', '2018-01-12 08:40:46'),
(5257, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:48:24'),
(5258, 60, 2, '28.4432759', '77.055913', '2018-01-12 08:48:27'),
(5259, 60, 2, '28.4432759', '77.055913', '2018-01-12 08:48:30'),
(5260, 60, 2, '28.4432484', '77.0559063', '2018-01-12 08:48:33'),
(5261, 60, 2, '28.4432484', '77.0559063', '2018-01-12 08:48:36'),
(5262, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:48:39'),
(5263, 60, 2, '28.443262', '77.0559074', '2018-01-12 08:48:42'),
(5264, 60, 2, '28.443262', '77.0559074', '2018-01-12 08:48:45'),
(5265, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:48:48'),
(5266, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:48:51'),
(5267, 60, 2, '28.4432687', '77.0559101', '2018-01-12 08:48:54'),
(5268, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:48:57'),
(5269, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:00'),
(5270, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:03'),
(5271, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:06'),
(5272, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:09'),
(5273, 60, 2, '28.4432497', '77.0559022', '2018-01-12 08:49:12'),
(5274, 60, 2, '28.4432497', '77.0559022', '2018-01-12 08:49:15'),
(5275, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:18'),
(5276, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:21'),
(5277, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:24'),
(5278, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:27'),
(5279, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:30'),
(5280, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:33'),
(5281, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:37'),
(5282, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:39'),
(5283, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:43'),
(5284, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:45'),
(5285, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:49'),
(5286, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:51'),
(5287, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:55'),
(5288, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:49:57'),
(5289, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:50:01'),
(5290, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:50:04'),
(5291, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:50:07'),
(5292, 60, 2, '28.4432106', '77.0558788', '2018-01-12 08:50:10'),
(5293, 60, 2, '28.4432412', '77.0558984', '2018-01-12 08:50:13'),
(5294, 60, 2, '28.4432412', '77.0558984', '2018-01-12 08:50:16'),
(5295, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:50:19'),
(5296, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:50:22'),
(5297, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:50:25'),
(5298, 60, 2, '28.4432306', '77.0558938', '2018-01-12 08:50:28'),
(5299, 60, 2, '28.4432306', '77.0558938', '2018-01-12 08:50:31'),
(5300, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:50:34'),
(5301, 60, 2, '28.4432373', '77.0558968', '2018-01-12 08:50:37'),
(5302, 60, 2, '28.4432373', '77.0558968', '2018-01-12 08:50:40'),
(5303, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:50:43'),
(5304, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:50:46'),
(5305, 60, 2, '28.4432306', '77.0558938', '2018-01-12 08:50:49'),
(5306, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:50:52'),
(5307, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:50:55'),
(5308, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:50:58'),
(5309, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:51:01'),
(5310, 60, 2, '28.4432373', '77.0558968', '2018-01-12 08:51:04'),
(5311, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:51:07'),
(5312, 60, 2, '28.4432299', '77.0558934', '2018-01-12 08:51:10'),
(5313, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:13'),
(5314, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:16'),
(5315, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:19'),
(5316, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:22'),
(5317, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:25'),
(5318, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:28'),
(5319, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:31'),
(5320, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:34'),
(5321, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:37'),
(5322, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:40'),
(5323, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:43'),
(5324, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:46'),
(5325, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:49'),
(5326, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:52'),
(5327, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:55'),
(5328, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:51:58'),
(5329, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:01'),
(5330, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:04'),
(5331, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:07'),
(5332, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:10'),
(5333, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:13'),
(5334, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:16'),
(5335, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:19'),
(5336, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:22'),
(5337, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:25'),
(5338, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:28'),
(5339, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:31'),
(5340, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:34'),
(5341, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:37'),
(5342, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:40'),
(5343, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:43'),
(5344, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:46'),
(5345, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:49'),
(5346, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:52'),
(5347, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:55'),
(5348, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:52:58'),
(5349, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:01'),
(5350, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:04'),
(5351, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:07'),
(5352, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:10'),
(5353, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:13'),
(5354, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:16'),
(5355, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:19'),
(5356, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:22'),
(5357, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:25'),
(5358, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:28'),
(5359, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:31'),
(5360, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:34'),
(5361, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:37'),
(5362, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:40'),
(5363, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:43'),
(5364, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:46'),
(5365, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:49'),
(5366, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:52'),
(5367, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:55'),
(5368, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:53:58'),
(5369, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:01'),
(5370, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:04'),
(5371, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:07'),
(5372, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:10'),
(5373, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:13'),
(5374, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:16'),
(5375, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:20'),
(5376, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:23'),
(5377, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:26'),
(5378, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:29'),
(5379, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:32'),
(5380, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:35'),
(5381, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:38'),
(5382, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:41'),
(5383, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:44'),
(5384, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:47'),
(5385, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:50'),
(5386, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:53'),
(5387, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:56'),
(5388, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:54:59'),
(5389, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:02'),
(5390, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:05'),
(5391, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:08'),
(5392, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:11'),
(5393, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:14'),
(5394, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:17'),
(5395, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:20'),
(5396, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:23'),
(5397, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:26'),
(5398, 60, 2, '28.4432338', '77.0558952', '2018-01-12 08:55:29'),
(5399, 61, 2, '28.4432106', '77.0558788', '2018-01-12 08:57:00'),
(5400, 61, 2, '28.44325', '77.0559021', '2018-01-12 08:57:03'),
(5401, 61, 2, '28.44325', '77.0559021', '2018-01-12 08:57:06'),
(5402, 61, 2, '28.4432654', '77.0559082', '2018-01-12 08:57:09'),
(5403, 61, 2, '28.4432548', '77.055904', '2018-01-12 08:57:12'),
(5404, 61, 2, '28.4432548', '77.055904', '2018-01-12 08:57:15'),
(5405, 61, 2, '28.4432454', '77.0559002', '2018-01-12 08:57:18'),
(5406, 61, 2, '28.4432454', '77.0559002', '2018-01-12 08:57:21'),
(5407, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:57:24'),
(5408, 61, 2, '28.4432548', '77.055904', '2018-01-12 08:57:28'),
(5409, 61, 2, '28.4432548', '77.055904', '2018-01-12 08:57:31'),
(5410, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:57:34'),
(5411, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:57:37'),
(5412, 61, 2, '28.4432548', '77.055904', '2018-01-12 08:57:40'),
(5413, 61, 2, '28.4432454', '77.0559002', '2018-01-12 08:57:43'),
(5414, 61, 2, '28.4432454', '77.0559002', '2018-01-12 08:57:46'),
(5415, 61, 2, '28.44325', '77.0559021', '2018-01-12 08:57:49'),
(5416, 61, 2, '28.44325', '77.0559021', '2018-01-12 08:57:51'),
(5417, 61, 2, '28.4432654', '77.0559082', '2018-01-12 08:57:55'),
(5418, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:57:58'),
(5419, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:58:18'),
(5420, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:58:20'),
(5421, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:58:20'),
(5422, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:58:20'),
(5423, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:58:21'),
(5424, 61, 2, '28.4432711', '77.0559104', '2018-01-12 08:58:21'),
(5425, 61, 2, '28.4432654', '77.0559082', '2018-01-12 08:58:22'),
(5426, 61, 2, '28.4432654', '77.0559082', '2018-01-12 08:58:22'),
(5427, 61, 2, '28.4432654', '77.0559082', '2018-01-12 08:58:25'),
(5428, 61, 2, '28.4432654', '77.0559082', '2018-01-12 08:58:28'),
(5429, 61, 2, '28.4432654', '77.0559082', '2018-01-12 08:58:38'),
(5430, 61, 2, '28.4419163', '77.054936', '2018-01-12 08:58:38'),
(5431, 61, 2, '28.4419163', '77.054936', '2018-01-12 08:58:38'),
(5432, 61, 2, '28.4419163', '77.054936', '2018-01-12 08:58:51'),
(5433, 61, 2, '28.4419163', '77.054936', '2018-01-12 08:58:51'),
(5434, 61, 2, '28.4419163', '77.054936', '2018-01-12 08:58:51'),
(5435, 61, 2, '28.4419163', '77.054936', '2018-01-12 08:58:51'),
(5436, 61, 2, '28.4419163', '77.054936', '2018-01-12 08:58:52'),
(5437, 61, 2, '28.4419163', '77.054936', '2018-01-12 08:58:55'),
(5438, 61, 2, '28.4432811', '77.0559134', '2018-01-12 09:10:55'),
(5439, 61, 2, '28.4432811', '77.0559134', '2018-01-12 09:10:55'),
(5440, 61, 2, '28.4432811', '77.0559134', '2018-01-12 09:10:55'),
(5441, 61, 2, '28.4432759', '77.055913', '2018-01-12 09:10:56'),
(5442, 61, 2, '28.4432106', '77.0558788', '2018-01-12 09:10:59'),
(5443, 61, 2, '28.4432106', '77.0558788', '2018-01-12 09:11:02'),
(5444, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:05'),
(5445, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:08'),
(5446, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:11'),
(5447, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:14'),
(5448, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:17'),
(5449, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:20'),
(5450, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:24'),
(5451, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:26'),
(5452, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:29'),
(5453, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:32'),
(5454, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:35'),
(5455, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:38'),
(5456, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:41'),
(5457, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:44'),
(5458, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:47'),
(5459, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:50'),
(5460, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:53'),
(5461, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:56'),
(5462, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:11:59'),
(5463, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:02'),
(5464, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:05'),
(5465, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:08'),
(5466, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:11'),
(5467, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:14'),
(5468, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:17'),
(5469, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:20'),
(5470, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:23'),
(5471, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:26'),
(5472, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:29'),
(5473, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:32'),
(5474, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:35'),
(5475, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:38'),
(5476, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:41'),
(5477, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:44'),
(5478, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:47'),
(5479, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:50'),
(5480, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:53'),
(5481, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:56'),
(5482, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:12:59'),
(5483, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:02'),
(5484, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:05'),
(5485, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:08'),
(5486, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:11'),
(5487, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:14'),
(5488, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:17'),
(5489, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:20'),
(5490, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:23'),
(5491, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:26'),
(5492, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:29'),
(5493, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:32'),
(5494, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:35'),
(5495, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:38'),
(5496, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:41'),
(5497, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:44'),
(5498, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:47'),
(5499, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:50'),
(5500, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:53'),
(5501, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:13:56'),
(5502, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:00'),
(5503, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:02'),
(5504, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:05'),
(5505, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:08'),
(5506, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:11'),
(5507, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:14'),
(5508, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:17'),
(5509, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:20'),
(5510, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:23'),
(5511, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:26'),
(5512, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:29'),
(5513, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:32'),
(5514, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:35'),
(5515, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:38'),
(5516, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:41');
INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(5517, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:44'),
(5518, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:47'),
(5519, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:50'),
(5520, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:53'),
(5521, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:56'),
(5522, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:14:59'),
(5523, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:02'),
(5524, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:05'),
(5525, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:08'),
(5526, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:11'),
(5527, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:14'),
(5528, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:17'),
(5529, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:20'),
(5530, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:23'),
(5531, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:26'),
(5532, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:29'),
(5533, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:32'),
(5534, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:35'),
(5535, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:38'),
(5536, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:41'),
(5537, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:44'),
(5538, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:47'),
(5539, 61, 2, '28.4433071', '77.055925', '2018-01-12 09:15:51'),
(5540, 62, 2, '28.4432442', '77.0558999', '2018-01-12 09:17:23'),
(5541, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:26'),
(5542, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:29'),
(5543, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:32'),
(5544, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:35'),
(5545, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:38'),
(5546, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:41'),
(5547, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:44'),
(5548, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:47'),
(5549, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:50'),
(5550, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:53'),
(5551, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:56'),
(5552, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:17:59'),
(5553, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:02'),
(5554, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:06'),
(5555, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:08'),
(5556, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:11'),
(5557, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:14'),
(5558, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:17'),
(5559, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:20'),
(5560, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:23'),
(5561, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:26'),
(5562, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:29'),
(5563, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:32'),
(5564, 62, 2, '28.4432497', '77.0559022', '2018-01-12 09:18:35'),
(5565, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:14'),
(5566, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:17'),
(5567, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:20'),
(5568, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:23'),
(5569, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:26'),
(5570, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:29'),
(5571, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:32'),
(5572, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:35'),
(5573, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:38'),
(5574, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:41'),
(5575, 67, 2, '28.4433047', '77.0559395', '2018-01-12 10:24:44'),
(5576, 68, 2, '28.443324', '77.0559527', '2018-01-12 10:25:49'),
(5577, 68, 2, '28.443324', '77.0559527', '2018-01-12 10:25:52'),
(5578, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:25:55'),
(5579, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:25:58'),
(5580, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:01'),
(5581, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:04'),
(5582, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:07'),
(5583, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:10'),
(5584, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:13'),
(5585, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:16'),
(5586, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:19'),
(5587, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:22'),
(5588, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:25'),
(5589, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:28'),
(5590, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:31'),
(5591, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:34'),
(5592, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:37'),
(5593, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:40'),
(5594, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:43'),
(5595, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:46'),
(5596, 68, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:49'),
(5597, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:52'),
(5598, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:55'),
(5599, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:26:59'),
(5600, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:01'),
(5601, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:04'),
(5602, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:07'),
(5603, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:11'),
(5604, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:13'),
(5605, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:16'),
(5606, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:19'),
(5607, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:22'),
(5608, 69, 2, '28.4432497', '77.0559022', '2018-01-12 10:27:23'),
(5609, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:25'),
(5610, 69, 2, '28.4432497', '77.0559022', '2018-01-12 10:27:26'),
(5611, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:28'),
(5612, 69, 2, '28.4432497', '77.0559022', '2018-01-12 10:27:29'),
(5613, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:31'),
(5614, 69, 2, '28.4432497', '77.0559022', '2018-01-12 10:27:32'),
(5615, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:35'),
(5616, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:35'),
(5617, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:38'),
(5618, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:38'),
(5619, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:41'),
(5620, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:41'),
(5621, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:44'),
(5622, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:44'),
(5623, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:47'),
(5624, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:47'),
(5625, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:50'),
(5626, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:50'),
(5627, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:53'),
(5628, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:53'),
(5629, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:56'),
(5630, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:56'),
(5631, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:27:59'),
(5632, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:27:59'),
(5633, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:02'),
(5634, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:02'),
(5635, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:05'),
(5636, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:05'),
(5637, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:08'),
(5638, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:08'),
(5639, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:11'),
(5640, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:11'),
(5641, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:14'),
(5642, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:14'),
(5643, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:17'),
(5644, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:17'),
(5645, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:20'),
(5646, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:20'),
(5647, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:23'),
(5648, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:23'),
(5649, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:26'),
(5650, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:26'),
(5651, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:29'),
(5652, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:29'),
(5653, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:32'),
(5654, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:32'),
(5655, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:35'),
(5656, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:35'),
(5657, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:38'),
(5658, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:38'),
(5659, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:41'),
(5660, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:41'),
(5661, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:44'),
(5662, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:44'),
(5663, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:47'),
(5664, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:47'),
(5665, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:50'),
(5666, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:50'),
(5667, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:53'),
(5668, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:53'),
(5669, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:56'),
(5670, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:56'),
(5671, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:28:59'),
(5672, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:28:59'),
(5673, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:02'),
(5674, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:02'),
(5675, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:05'),
(5676, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:05'),
(5677, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:08'),
(5678, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:08'),
(5679, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:11'),
(5680, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:11'),
(5681, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:14'),
(5682, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:14'),
(5683, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:17'),
(5684, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:17'),
(5685, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:20'),
(5686, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:20'),
(5687, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:23'),
(5688, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:23'),
(5689, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:26'),
(5690, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:26'),
(5691, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:29'),
(5692, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:29'),
(5693, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:32'),
(5694, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:32'),
(5695, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:35'),
(5696, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:35'),
(5697, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:38'),
(5698, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:38'),
(5699, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:41'),
(5700, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:41'),
(5701, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:44'),
(5702, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:44'),
(5703, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:47'),
(5704, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:47'),
(5705, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:50'),
(5706, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:50'),
(5707, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:53'),
(5708, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:53'),
(5709, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:56'),
(5710, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:56'),
(5711, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:29:59'),
(5712, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:29:59'),
(5713, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:02'),
(5714, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:02'),
(5715, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:05'),
(5716, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:05'),
(5717, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:08'),
(5718, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:08'),
(5719, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:11'),
(5720, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:11'),
(5721, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:14'),
(5722, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:14'),
(5723, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:17'),
(5724, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:17'),
(5725, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:20'),
(5726, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:20'),
(5727, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:23'),
(5728, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:23'),
(5729, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:26'),
(5730, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:26'),
(5731, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:29'),
(5732, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:29'),
(5733, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:32'),
(5734, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:32'),
(5735, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:35'),
(5736, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:35'),
(5737, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:38'),
(5738, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:38'),
(5739, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:41'),
(5740, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:41'),
(5741, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:44'),
(5742, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:44'),
(5743, 69, 2, '28.4432895', '77.0559174', '2018-01-12 10:30:47'),
(5744, 69, 2, '28.4432833', '77.0559159', '2018-01-12 10:30:47'),
(5745, 71, 2, '28.4432106', '77.0558788', '2018-01-12 10:45:37'),
(5746, 71, 2, '28.4432106', '77.0558788', '2018-01-12 10:45:40'),
(5747, 71, 2, '28.4434019', '77.0560144', '2018-01-12 10:45:43'),
(5748, 71, 2, '28.4434019', '77.0560144', '2018-01-12 10:45:46'),
(5749, 71, 2, '28.4432106', '77.0558788', '2018-01-12 10:45:49'),
(5750, 71, 2, '28.4432106', '77.0558788', '2018-01-12 10:45:52'),
(5751, 71, 2, '28.4432106', '77.0558788', '2018-01-12 10:45:55'),
(5752, 71, 2, '28.4432106', '77.0558788', '2018-01-12 10:46:00'),
(5753, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:03'),
(5754, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:07'),
(5755, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:11'),
(5756, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:14'),
(5757, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:17'),
(5758, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:21'),
(5759, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:24'),
(5760, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:27'),
(5761, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:30'),
(5762, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:33'),
(5763, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:36'),
(5764, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:39'),
(5765, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:42'),
(5766, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:45'),
(5767, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:48'),
(5768, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:51'),
(5769, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:54'),
(5770, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:46:58'),
(5771, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:01'),
(5772, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:04'),
(5773, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:07'),
(5774, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:10'),
(5775, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:13'),
(5776, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:16'),
(5777, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:19'),
(5778, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:22'),
(5779, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:25'),
(5780, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:28'),
(5781, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:31'),
(5782, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:34'),
(5783, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:37'),
(5784, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:40'),
(5785, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:43'),
(5786, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:46'),
(5787, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:49'),
(5788, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:52'),
(5789, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:55'),
(5790, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:47:59'),
(5791, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:02'),
(5792, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:05'),
(5793, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:08'),
(5794, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:11'),
(5795, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:14'),
(5796, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:17'),
(5797, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:20'),
(5798, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:23'),
(5799, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:26'),
(5800, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:29'),
(5801, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:32'),
(5802, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:35'),
(5803, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:38'),
(5804, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:41'),
(5805, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:44'),
(5806, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:47'),
(5807, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:50'),
(5808, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:53'),
(5809, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:56'),
(5810, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:48:59'),
(5811, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:02'),
(5812, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:05'),
(5813, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:08'),
(5814, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:11'),
(5815, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:14'),
(5816, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:17'),
(5817, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:20'),
(5818, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:23'),
(5819, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:26'),
(5820, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:29'),
(5821, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:32'),
(5822, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:35'),
(5823, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:38'),
(5824, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:41'),
(5825, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:44'),
(5826, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:47'),
(5827, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:50'),
(5828, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:53'),
(5829, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:56'),
(5830, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:49:59'),
(5831, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:02'),
(5832, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:05'),
(5833, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:08'),
(5834, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:11'),
(5835, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:14'),
(5836, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:17'),
(5837, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:20'),
(5838, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:23'),
(5839, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:26'),
(5840, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:31'),
(5841, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:32'),
(5842, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:35'),
(5843, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:38'),
(5844, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:41'),
(5845, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:44'),
(5846, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:47'),
(5847, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:50'),
(5848, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:53'),
(5849, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:56'),
(5850, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:50:59'),
(5851, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:02'),
(5852, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:05'),
(5853, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:08'),
(5854, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:11'),
(5855, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:14'),
(5856, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:17'),
(5857, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:20'),
(5858, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:23'),
(5859, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:26'),
(5860, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:29'),
(5861, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:32'),
(5862, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:35'),
(5863, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:38'),
(5864, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:41'),
(5865, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:44'),
(5866, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:47'),
(5867, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:50'),
(5868, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:53'),
(5869, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:56'),
(5870, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:51:59'),
(5871, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:02'),
(5872, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:05'),
(5873, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:08'),
(5874, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:11'),
(5875, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:14'),
(5876, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:17'),
(5877, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:20'),
(5878, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:23'),
(5879, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:26'),
(5880, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:29'),
(5881, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:32'),
(5882, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:35'),
(5883, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:38'),
(5884, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:41'),
(5885, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:44'),
(5886, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:47'),
(5887, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:50'),
(5888, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:53'),
(5889, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:56'),
(5890, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:52:59'),
(5891, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:02'),
(5892, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:05'),
(5893, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:08'),
(5894, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:11'),
(5895, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:14'),
(5896, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:17'),
(5897, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:20'),
(5898, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:23'),
(5899, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:26'),
(5900, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:29'),
(5901, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:32'),
(5902, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:35'),
(5903, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:38'),
(5904, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:41'),
(5905, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:44'),
(5906, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:47'),
(5907, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:50'),
(5908, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:53'),
(5909, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:56'),
(5910, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:53:59'),
(5911, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:02'),
(5912, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:05'),
(5913, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:08'),
(5914, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:11'),
(5915, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:14'),
(5916, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:17'),
(5917, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:20'),
(5918, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:23'),
(5919, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:26'),
(5920, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:29'),
(5921, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:32'),
(5922, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:35'),
(5923, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:38'),
(5924, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:41'),
(5925, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:44'),
(5926, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:47'),
(5927, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:50'),
(5928, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:53'),
(5929, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:56'),
(5930, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:54:59'),
(5931, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:02'),
(5932, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:05'),
(5933, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:08'),
(5934, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:11'),
(5935, 71, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:14'),
(5936, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:17'),
(5937, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:20'),
(5938, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:23'),
(5939, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:26'),
(5940, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:29'),
(5941, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:32'),
(5942, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:35'),
(5943, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:38'),
(5944, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:41'),
(5945, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:44'),
(5946, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:47'),
(5947, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:50'),
(5948, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:53'),
(5949, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:56'),
(5950, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:55:59'),
(5951, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:02'),
(5952, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:05'),
(5953, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:08'),
(5954, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:11'),
(5955, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:14'),
(5956, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:17'),
(5957, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:20'),
(5958, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:23'),
(5959, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:26'),
(5960, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:29'),
(5961, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:32'),
(5962, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:35'),
(5963, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:38'),
(5964, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:41'),
(5965, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:44'),
(5966, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:47'),
(5967, 72, 2, '28.4435706', '77.0562396', '2018-01-12 10:56:50'),
(5968, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:30'),
(5969, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:33'),
(5970, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:36'),
(5971, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:39'),
(5972, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:42'),
(5973, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:45'),
(5974, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:48'),
(5975, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:51'),
(5976, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:54'),
(5977, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:58:57'),
(5978, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:00'),
(5979, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:03'),
(5980, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:06'),
(5981, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:11'),
(5982, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:12'),
(5983, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:15'),
(5984, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:18'),
(5985, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:21'),
(5986, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:24'),
(5987, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:27'),
(5988, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:30'),
(5989, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:33'),
(5990, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:36'),
(5991, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:39'),
(5992, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:42'),
(5993, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:45'),
(5994, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:48'),
(5995, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:51'),
(5996, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:54'),
(5997, 73, 2, '28.4432106', '77.0558788', '2018-01-12 10:59:58'),
(5998, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:01'),
(5999, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:03'),
(6000, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:06'),
(6001, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:09'),
(6002, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:12'),
(6003, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:15'),
(6004, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:18'),
(6005, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:21'),
(6006, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:24'),
(6007, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:27'),
(6008, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:30'),
(6009, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:33'),
(6010, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:36'),
(6011, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:39'),
(6012, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:42'),
(6013, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:45'),
(6014, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:48'),
(6015, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:51'),
(6016, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:54'),
(6017, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:00:57'),
(6018, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:00'),
(6019, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:03'),
(6020, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:06'),
(6021, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:09'),
(6022, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:12'),
(6023, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:15'),
(6024, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:18'),
(6025, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:21'),
(6026, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:24'),
(6027, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:27'),
(6028, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:30'),
(6029, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:33'),
(6030, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:36'),
(6031, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:39'),
(6032, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:42'),
(6033, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:45'),
(6034, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:48'),
(6035, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:51'),
(6036, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:54'),
(6037, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:01:57'),
(6038, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:00'),
(6039, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:03'),
(6040, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:06'),
(6041, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:09'),
(6042, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:12'),
(6043, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:15'),
(6044, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:18'),
(6045, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:21'),
(6046, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:24'),
(6047, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:27'),
(6048, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:30'),
(6049, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:33'),
(6050, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:36'),
(6051, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:39'),
(6052, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:42'),
(6053, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:45'),
(6054, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:48'),
(6055, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:51'),
(6056, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:54'),
(6057, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:02:57'),
(6058, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:00'),
(6059, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:03'),
(6060, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:06'),
(6061, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:09'),
(6062, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:12'),
(6063, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:15'),
(6064, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:18'),
(6065, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:21'),
(6066, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:24'),
(6067, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:27'),
(6068, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:30'),
(6069, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:33'),
(6070, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:36'),
(6071, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:39'),
(6072, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:42'),
(6073, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:45'),
(6074, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:48'),
(6075, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:51'),
(6076, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:54'),
(6077, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:03:57'),
(6078, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:00'),
(6079, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:03'),
(6080, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:06'),
(6081, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:09'),
(6082, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:12'),
(6083, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:15'),
(6084, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:19'),
(6085, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:21'),
(6086, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:24'),
(6087, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:27'),
(6088, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:30'),
(6089, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:33'),
(6090, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:36'),
(6091, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:39'),
(6092, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:42'),
(6093, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:45'),
(6094, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:48'),
(6095, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:51'),
(6096, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:54'),
(6097, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:04:57'),
(6098, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:00'),
(6099, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:03'),
(6100, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:06'),
(6101, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:09'),
(6102, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:12'),
(6103, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:15'),
(6104, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:18'),
(6105, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:21'),
(6106, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:24'),
(6107, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:27'),
(6108, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:30'),
(6109, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:33'),
(6110, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:36'),
(6111, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:39'),
(6112, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:42'),
(6113, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:45'),
(6114, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:48'),
(6115, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:51'),
(6116, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:54'),
(6117, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:05:57'),
(6118, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:00'),
(6119, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:03'),
(6120, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:06'),
(6121, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:09'),
(6122, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:12'),
(6123, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:15'),
(6124, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:18'),
(6125, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:21'),
(6126, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:24'),
(6127, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:27'),
(6128, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:30'),
(6129, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:33'),
(6130, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:36'),
(6131, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:39'),
(6132, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:42'),
(6133, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:45'),
(6134, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:51'),
(6135, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:51'),
(6136, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:54'),
(6137, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:06:57'),
(6138, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:00'),
(6139, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:03'),
(6140, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:06'),
(6141, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:09'),
(6142, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:12'),
(6143, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:16'),
(6144, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:18'),
(6145, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:21'),
(6146, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:24'),
(6147, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:27'),
(6148, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:30'),
(6149, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:07:48'),
(6150, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:10'),
(6151, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:11'),
(6152, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:16'),
(6153, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:17'),
(6154, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:33'),
(6155, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:34'),
(6156, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:49'),
(6157, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:50'),
(6158, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:08:50'),
(6159, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:11'),
(6160, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:12'),
(6161, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:13'),
(6162, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:17'),
(6163, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:18'),
(6164, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:20'),
(6165, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:38'),
(6166, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:38'),
(6167, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:39'),
(6168, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:39'),
(6169, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:40'),
(6170, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:41'),
(6171, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:41'),
(6172, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:42'),
(6173, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:52'),
(6174, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:53'),
(6175, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:53'),
(6176, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:54'),
(6177, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:54'),
(6178, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:55'),
(6179, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:56'),
(6180, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:56'),
(6181, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:58'),
(6182, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:09:59'),
(6183, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:00'),
(6184, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:02'),
(6185, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:03'),
(6186, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:05'),
(6187, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:06'),
(6188, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:08'),
(6189, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:16'),
(6190, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:16'),
(6191, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:17'),
(6192, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:17'),
(6193, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:18'),
(6194, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:18'),
(6195, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:18'),
(6196, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:19'),
(6197, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:19'),
(6198, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:20'),
(6199, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:20'),
(6200, 73, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:20'),
(6201, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:21'),
(6202, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:21'),
(6203, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:21'),
(6204, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:22'),
(6205, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:22'),
(6206, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:24'),
(6207, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:27'),
(6208, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:30'),
(6209, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:33'),
(6210, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:36'),
(6211, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:39'),
(6212, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:42'),
(6213, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:45'),
(6214, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:48'),
(6215, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:51'),
(6216, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:54'),
(6217, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:10:57'),
(6218, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:00'),
(6219, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:03'),
(6220, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:06'),
(6221, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:09'),
(6222, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:12'),
(6223, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:15'),
(6224, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:18'),
(6225, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:21'),
(6226, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:24'),
(6227, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:27'),
(6228, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:30'),
(6229, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:33'),
(6230, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:36'),
(6231, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:39'),
(6232, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:42'),
(6233, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:45'),
(6234, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:48'),
(6235, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:51'),
(6236, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:54'),
(6237, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:11:57'),
(6238, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:00'),
(6239, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:03'),
(6240, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:06'),
(6241, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:09'),
(6242, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:12'),
(6243, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:15'),
(6244, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:18'),
(6245, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:21'),
(6246, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:24'),
(6247, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:27'),
(6248, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:31'),
(6249, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:33'),
(6250, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:36'),
(6251, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:40'),
(6252, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:43'),
(6253, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:46'),
(6254, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:48'),
(6255, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:52'),
(6256, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:55'),
(6257, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:12:58'),
(6258, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:01'),
(6259, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:04'),
(6260, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:07'),
(6261, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:10'),
(6262, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:13'),
(6263, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:16'),
(6264, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:19'),
(6265, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:22'),
(6266, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:25'),
(6267, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:28'),
(6268, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:31'),
(6269, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:34'),
(6270, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:37'),
(6271, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:40'),
(6272, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:43'),
(6273, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:46'),
(6274, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:49'),
(6275, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:52'),
(6276, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:55'),
(6277, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:13:58'),
(6278, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:01'),
(6279, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:04'),
(6280, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:07'),
(6281, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:10'),
(6282, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:13'),
(6283, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:16'),
(6284, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:19'),
(6285, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:22'),
(6286, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:25'),
(6287, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:28'),
(6288, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:31'),
(6289, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:34'),
(6290, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:37'),
(6291, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:40'),
(6292, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:43'),
(6293, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:46'),
(6294, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:49'),
(6295, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:52'),
(6296, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:55');
INSERT INTO `ride_location` (`id`, `ride_id`, `driver_id`, `lattitue`, `longitude`, `created`) VALUES
(6297, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:14:58'),
(6298, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:01'),
(6299, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:04'),
(6300, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:07'),
(6301, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:10'),
(6302, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:13'),
(6303, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:16'),
(6304, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:19'),
(6305, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:22'),
(6306, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:25'),
(6307, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:28'),
(6308, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:31'),
(6309, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:15:34'),
(6310, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:23:13'),
(6311, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:23:16'),
(6312, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:23:19'),
(6313, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:23:22'),
(6314, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:23:25'),
(6315, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:23:28'),
(6316, 74, 2, '28.4432106', '77.0558788', '2018-01-12 11:23:31');

-- --------------------------------------------------------

--
-- Table structure for table `ride_status`
--

CREATE TABLE `ride_status` (
  `ID` int(11) NOT NULL,
  `Ride_ID` int(11) DEFAULT NULL,
  `Status_ID` int(11) DEFAULT NULL,
  `Status_Time` datetime(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sent_notification_detail`
--

CREATE TABLE `sent_notification_detail` (
  `id` int(11) NOT NULL,
  `sent_id` int(11) DEFAULT NULL,
  `user_type` varchar(50) DEFAULT NULL,
  `notification` text,
  `sent_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `meta_value` longtext,
  `modified_by` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `meta_key`, `meta_value`, `modified_by`, `created`, `modified`) VALUES
(1, 'welcome_mail_en', NULL, NULL, '2017-07-31 03:47:17', '2017-07-31 03:47:17'),
(2, 'welcome_mail_ar', NULL, NULL, '2017-07-31 03:47:31', '2017-07-31 03:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `vehicle_id` int(12) NOT NULL,
  `vehicle_type` int(11) NOT NULL DEFAULT '0',
  `time` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id`, `driver_id`, `vehicle_id`, `vehicle_type`, `time`, `status`) VALUES
(1, 1, 1, 1, '2018-01-05 13:57:55', 1),
(2, 2, 2, 2, '2018-01-05 14:20:53', 1),
(3, 1, 1, 1, '2018-01-05 14:28:59', 0),
(4, 2, 2, 2, '2018-01-05 14:33:18', 0),
(5, 2, 2, 2, '2018-01-05 14:34:38', 1),
(6, 1, 1, 1, '2018-01-05 14:35:29', 1),
(7, 1, 1, 1, '2018-01-05 14:35:31', 0),
(8, 1, 1, 1, '2018-01-05 14:35:33', 1),
(9, 1, 1, 1, '2018-01-05 14:35:35', 0),
(10, 2, 2, 2, '2018-01-05 14:36:18', 0),
(11, 1, 1, 1, '2018-01-05 14:38:48', 1),
(12, 1, 1, 1, '2018-01-05 14:39:10', 0),
(13, 1, 1, 1, '2018-01-05 14:39:11', 1),
(14, 1, 1, 1, '2018-01-05 14:39:13', 0),
(15, 1, 1, 1, '2018-01-05 14:39:14', 1),
(16, 1, 1, 1, '2018-01-05 14:40:04', 0),
(17, 1, 1, 1, '2018-01-05 14:40:05', 1),
(18, 1, 1, 1, '2018-01-05 14:43:31', 0),
(19, 2, 2, 2, '2018-01-05 14:47:37', 1),
(20, 3, 3, 3, '2018-01-05 14:47:58', 1),
(21, 1, 1, 1, '2018-01-05 14:49:33', 1),
(22, 2, 2, 2, '2018-01-05 14:50:33', 0),
(23, 1, 1, 1, '2018-01-05 14:52:21', 0),
(24, 1, 1, 1, '2018-01-05 14:52:23', 1),
(25, 2, 2, 2, '2018-01-05 14:53:13', 1),
(26, 2, 2, 2, '2018-01-05 15:14:23', 0),
(27, 2, 2, 2, '2018-01-05 15:16:01', 1),
(28, 2, 2, 2, '2018-01-05 15:17:46', 0),
(29, 2, 2, 2, '2018-01-05 15:22:16', 1),
(30, 2, 2, 2, '2018-01-05 15:24:03', 0),
(31, 2, 2, 2, '2018-01-05 15:26:37', 1),
(32, 2, 2, 2, '2018-01-05 15:32:15', 0),
(33, 2, 2, 2, '2018-01-05 15:32:18', 1),
(34, 2, 2, 2, '2018-01-05 15:32:22', 0),
(35, 2, 2, 2, '2018-01-05 15:32:24', 1),
(36, 4, 4, 3, '2018-01-07 06:26:46', 1),
(37, 5, 5, 1, '2018-01-07 08:49:35', 1),
(38, 5, 5, 1, '2018-01-07 08:51:09', 0),
(39, 5, 5, 1, '2018-01-07 08:51:13', 1),
(40, 5, 5, 1, '2018-01-07 08:57:55', 0),
(41, 5, 5, 1, '2018-01-07 08:58:00', 1),
(42, 5, 5, 1, '2018-01-07 15:37:31', 1),
(43, 1, 1, 1, '2018-01-08 06:10:06', 0),
(44, 2, 2, 2, '2018-01-08 06:13:14', 1),
(45, 2, 2, 2, '2018-01-08 06:13:27', 0),
(46, 6, 6, 2, '2018-01-08 07:30:17', 1),
(47, 2, 2, 2, '2018-01-08 07:48:59', 0),
(48, 2, 2, 2, '2018-01-08 07:49:11', 1),
(49, 2, 2, 2, '2018-01-08 07:49:35', 0),
(50, 2, 2, 2, '2018-01-08 07:49:37', 1),
(51, 6, 6, 2, '2018-01-08 07:51:15', 1),
(52, 6, 6, 2, '2018-01-08 07:51:25', 0),
(53, 6, 6, 2, '2018-01-08 07:51:26', 1),
(54, 6, 6, 2, '2018-01-08 07:51:40', 0),
(55, 6, 6, 2, '2018-01-08 07:51:52', 1),
(56, 6, 6, 2, '2018-01-08 07:54:24', 0),
(57, 6, 6, 2, '2018-01-08 07:54:27', 1),
(58, 6, 6, 2, '2018-01-08 09:29:59', 0),
(59, 6, 6, 2, '2018-01-08 09:31:05', 1),
(60, 6, 6, 2, '2018-01-08 09:31:29', 0),
(61, 6, 6, 2, '2018-01-08 09:31:35', 1),
(62, 6, 6, 2, '2018-01-08 10:10:50', 0),
(63, 6, 6, 2, '2018-01-08 10:11:04', 1),
(64, 6, 6, 2, '2018-01-08 10:11:12', 0),
(65, 6, 6, 2, '2018-01-08 10:11:24', 1),
(66, 6, 6, 2, '2018-01-08 10:11:35', 0),
(67, 6, 6, 2, '2018-01-08 10:11:59', 1),
(68, 6, 6, 2, '2018-01-08 10:12:04', 0),
(69, 7, 10, 3, '2018-01-08 11:13:05', 1),
(70, 7, 10, 3, '2018-01-08 11:13:17', 0),
(71, 7, 10, 3, '2018-01-08 11:14:26', 1),
(72, 7, 10, 3, '2018-01-08 11:19:33', 0),
(73, 2, 2, 2, '2018-01-08 12:54:37', 0),
(74, 2, 2, 2, '2018-01-08 13:10:37', 1),
(75, 2, 2, 2, '2018-01-08 13:11:09', 0),
(76, 1, 1, 1, '2018-01-08 13:13:12', 1),
(77, 2, 2, 2, '2018-01-08 13:22:04', 1),
(78, 1, 1, 1, '2018-01-09 05:48:04', 1),
(79, 2, 2, 2, '2018-01-09 05:55:52', 1),
(80, 9, 11, 3, '2018-01-09 09:59:15', 1),
(81, 9, 12, 1, '2018-01-09 10:11:00', 1),
(82, 1, 1, 1, '2018-01-09 10:43:49', 0),
(83, 12, 15, 3, '2018-01-09 11:18:01', 1),
(84, 12, 15, 3, '2018-01-09 11:25:22', 0),
(85, 12, 15, 3, '2018-01-09 11:25:30', 1),
(86, 12, 15, 3, '2018-01-09 11:30:23', 0),
(87, 12, 15, 3, '2018-01-09 11:30:25', 1),
(88, 12, 15, 3, '2018-01-09 11:31:34', 0),
(89, 9, 12, 1, '2018-01-10 06:35:02', 1),
(90, 1, 1, 1, '2018-01-10 06:53:07', 1),
(91, 1, 1, 1, '2018-01-10 06:55:35', 0),
(92, 1, 7, 3, '2018-01-10 06:55:41', 1),
(93, 9, 12, 1, '2018-01-10 07:12:17', 1),
(94, 9, 12, 1, '2018-01-10 07:23:37', 1),
(95, 1, 7, 3, '2018-01-10 09:35:13', 0),
(96, 1, 7, 3, '2018-01-10 09:35:14', 1),
(97, 9, 12, 1, '2018-01-10 09:43:35', 0),
(98, 9, 12, 1, '2018-01-10 09:43:39', 1),
(99, 9, 12, 1, '2018-01-10 11:31:50', 1),
(100, 9, 12, 1, '2018-01-10 11:31:52', 0),
(101, 9, 12, 1, '2018-01-10 11:31:53', 1),
(102, 9, 11, 3, '2018-01-10 11:34:55', 1),
(103, 9, 12, 1, '2018-01-10 12:31:07', 1),
(104, 1, 1, 1, '2018-01-10 12:47:12', 1),
(105, 2, 2, 2, '2018-01-11 08:42:04', 0),
(106, 2, 2, 2, '2018-01-11 11:11:00', 1),
(107, 1, 1, 1, '2018-01-11 11:22:11', 0),
(108, 1, 7, 3, '2018-01-11 11:22:16', 1),
(109, 1, 7, 3, '2018-01-11 11:23:19', 0),
(110, 11, 14, 3, '2018-01-11 11:25:13', 1),
(111, 2, 2, 2, '2018-01-11 11:25:42', 0),
(112, 11, 14, 3, '2018-01-11 11:28:50', 0),
(113, 2, 2, 2, '2018-01-11 11:29:49', 1),
(114, 2, 2, 2, '2018-01-11 11:35:08', 0),
(115, 11, 14, 3, '2018-01-11 11:35:48', 1),
(116, 11, 14, 3, '2018-01-11 11:38:34', 0),
(117, 11, 14, 3, '2018-01-11 11:38:39', 1),
(118, 11, 14, 3, '2018-01-11 11:38:41', 0),
(119, 2, 2, 2, '2018-01-11 11:46:50', 1),
(120, 2, 2, 2, '2018-01-11 11:55:03', 1),
(121, 2, 2, 2, '2018-01-11 12:12:11', 0),
(122, 2, 2, 2, '2018-01-11 12:36:36', 1),
(123, 2, 2, 2, '2018-01-11 12:59:35', 1),
(124, 2, 2, 2, '2018-01-11 13:00:08', 0),
(125, 2, 2, 2, '2018-01-11 13:00:14', 1),
(126, 2, 2, 2, '2018-01-11 13:32:07', 0),
(127, 2, 2, 2, '2018-01-11 13:32:09', 1),
(128, 2, 2, 2, '2018-01-11 13:32:11', 0),
(129, 2, 2, 2, '2018-01-11 13:33:38', 1),
(130, 2, 2, 2, '2018-01-12 06:19:34', 1),
(131, 2, 2, 2, '2018-01-12 08:25:39', 1),
(132, 2, 2, 2, '2018-01-12 08:47:45', 1),
(133, 2, 2, 2, '2018-01-12 09:16:35', 0),
(134, 2, 2, 2, '2018-01-12 09:16:37', 1),
(135, 2, 2, 2, '2018-01-12 10:05:31', 0),
(136, 2, 2, 2, '2018-01-12 10:05:32', 1),
(137, 2, 2, 2, '2018-01-12 10:07:16', 0),
(138, 2, 2, 2, '2018-01-12 10:08:48', 1),
(139, 2, 2, 2, '2018-01-12 10:43:58', 1),
(140, 2, 2, 2, '2018-01-12 11:07:32', 0),
(141, 2, 2, 2, '2018-01-12 11:07:34', 1),
(142, 2, 2, 2, '2018-01-12 11:07:46', 0),
(143, 2, 2, 2, '2018-01-12 11:07:48', 1),
(144, 2, 2, 2, '2018-01-12 11:24:05', 0),
(145, 2, 2, 2, '2018-01-12 11:24:06', 1),
(146, 2, 2, 2, '2018-01-12 12:22:53', 1),
(147, 2, 2, 2, '2018-01-12 12:23:25', 0),
(148, 2, 2, 2, '2018-01-12 12:28:51', 1),
(149, 2, 2, 2, '2018-01-12 12:28:53', 0),
(150, 2, 2, 2, '2018-01-12 12:29:05', 1),
(151, 2, 2, 2, '2018-01-12 12:29:28', 0),
(152, 2, 2, 2, '2018-01-12 12:30:19', 1),
(153, 2, 2, 2, '2018-01-12 12:30:37', 0),
(154, 2, 2, 2, '2018-01-12 12:30:40', 1),
(155, 2, 2, 2, '2018-01-12 12:30:42', 0),
(156, 2, 2, 2, '2018-01-12 12:30:58', 1),
(157, 2, 2, 2, '2018-01-12 12:31:10', 0),
(158, 2, 2, 2, '2018-01-12 12:31:35', 1),
(159, 2, 2, 2, '2018-01-12 12:31:42', 0),
(160, 2, 2, 2, '2018-01-12 12:31:52', 1),
(161, 2, 2, 2, '2018-01-12 12:31:57', 0),
(162, 2, 2, 2, '2018-01-12 12:34:21', 1),
(163, 2, 2, 2, '2018-01-12 12:34:24', 0),
(164, 2, 2, 2, '2018-01-12 12:40:58', 1),
(165, 11, 14, 3, '2018-01-12 12:46:59', 1),
(166, 2, 2, 2, '2018-01-12 12:48:05', 0),
(167, 11, 14, 3, '2018-01-12 12:48:50', 0),
(168, 2, 2, 2, '2018-01-12 13:09:31', 1),
(169, 2, 2, 2, '2018-01-12 13:09:44', 0),
(170, 2, 2, 2, '2018-01-12 13:13:40', 1),
(171, 2, 2, 2, '2018-01-12 13:13:41', 0),
(172, 2, 2, 2, '2018-01-12 13:18:24', 1),
(173, 2, 2, 2, '2018-01-12 13:30:41', 0),
(174, 2, 2, 2, '2018-01-12 13:30:43', 1),
(175, 2, 2, 2, '2018-01-12 19:31:12', 0),
(176, 2, 2, 2, '2018-01-12 19:36:34', 1),
(177, 2, 2, 2, '2018-01-12 20:13:51', 0),
(178, 2, 16, 1, '2018-01-12 20:14:24', 1),
(179, 2, 16, 1, '2018-01-12 20:15:22', 0),
(180, 2, 16, 1, '2018-01-12 20:18:45', 1),
(181, 2, 2, 2, '2018-01-12 20:19:56', 1),
(182, 2, 2, 2, '2018-01-12 20:34:13', 1),
(183, 2, 16, 1, '2018-01-12 20:39:12', 1),
(184, 2, 16, 1, '2018-01-12 20:43:56', 0),
(185, 2, 16, 1, '2018-01-12 20:43:59', 1),
(186, 2, 16, 1, '2018-01-12 20:48:33', 0),
(187, 2, 16, 1, '2018-01-12 20:48:38', 1),
(188, 2, 2, 2, '2018-01-12 20:49:11', 1),
(189, 2, 16, 1, '2018-01-12 20:51:51', 1),
(190, 2, 16, 1, '2018-01-12 20:58:06', 0),
(191, 2, 16, 1, '2018-01-12 20:58:14', 1),
(192, 2, 2, 2, '2018-01-12 20:58:40', 1),
(193, 2, 2, 2, '2018-01-12 21:00:20', 0),
(194, 2, 2, 2, '2018-01-12 21:02:45', 1),
(195, 2, 2, 2, '2018-01-12 21:10:25', 0),
(196, 2, 2, 2, '2018-01-12 21:10:27', 1),
(197, 2, 2, 2, '2018-01-12 21:16:43', 0),
(198, 2, 2, 2, '2018-01-12 21:19:41', 1),
(199, 2, 16, 1, '2018-01-12 21:20:49', 1),
(200, 2, 2, 2, '2018-01-12 21:23:07', 1),
(201, 2, 2, 2, '2018-01-12 21:26:20', 0),
(202, 2, 16, 1, '2018-01-12 21:26:37', 1),
(203, 2, 16, 1, '2018-01-12 21:26:49', 0),
(204, 2, 2, 2, '2018-01-12 21:27:18', 1),
(205, 2, 2, 2, '2018-01-12 21:27:26', 0),
(206, 2, 2, 2, '2018-01-12 21:31:42', 1),
(207, 2, 2, 2, '2018-01-12 21:35:37', 0),
(208, 2, 2, 2, '2018-01-12 21:38:18', 1),
(209, 2, 2, 2, '2018-01-12 21:40:09', 0),
(210, 2, 2, 2, '2018-01-12 21:41:39', 1),
(211, 2, 2, 2, '2018-01-12 21:42:12', 0),
(212, 2, 2, 2, '2018-01-12 21:43:20', 1),
(213, 2, 2, 2, '2018-01-12 21:46:34', 0),
(214, 2, 2, 2, '2018-01-12 21:46:38', 1),
(215, 2, 2, 2, '2018-01-12 21:51:27', 0),
(216, 2, 16, 1, '2018-01-12 21:52:16', 1),
(217, 2, 2, 2, '2018-01-12 22:00:38', 1),
(218, 2, 16, 1, '2018-01-12 22:00:59', 1),
(219, 2, 2, 2, '2018-01-12 22:02:04', 1),
(220, 2, 16, 1, '2018-01-12 22:02:21', 1),
(221, 2, 2, 2, '2018-01-12 22:09:43', 1),
(222, 2, 16, 1, '2018-01-12 22:11:09', 1),
(223, 2, 2, 2, '2018-01-12 22:12:31', 1),
(224, 2, 16, 1, '2018-01-12 22:16:56', 1),
(225, 2, 16, 1, '2018-01-12 22:16:59', 0),
(226, 2, 2, 2, '2018-01-12 22:22:49', 1),
(227, 2, 16, 1, '2018-01-12 22:22:58', 1),
(228, 2, 16, 1, '2018-01-12 22:23:01', 0),
(229, 2, 16, 1, '2018-01-12 22:24:46', 1),
(230, 2, 2, 2, '2018-01-12 22:27:19', 1),
(231, 2, 2, 2, '2018-01-12 22:27:35', 0),
(232, 2, 16, 1, '2018-01-12 22:27:57', 1),
(233, 2, 16, 1, '2018-01-12 22:30:23', 0),
(234, 2, 16, 1, '2018-01-12 22:30:34', 1),
(235, 2, 16, 1, '2018-01-12 22:30:43', 0),
(236, 2, 16, 1, '2018-01-12 22:33:17', 1),
(237, 2, 16, 1, '2018-01-12 22:33:27', 0),
(238, 2, 16, 1, '2018-01-12 22:33:38', 1),
(239, 2, 16, 1, '2018-01-12 22:36:54', 0),
(240, 2, 16, 1, '2018-01-12 22:38:38', 1),
(241, 2, 16, 1, '2018-01-12 22:40:55', 0),
(242, 2, 16, 1, '2018-01-12 22:43:29', 1),
(243, 2, 16, 1, '2018-01-12 22:44:11', 0),
(244, 2, 16, 1, '2018-01-12 22:46:10', 1),
(245, 2, 16, 1, '2018-01-12 22:46:17', 0),
(246, 2, 16, 1, '2018-01-12 22:46:20', 1),
(247, 2, 16, 1, '2018-01-12 22:46:33', 0),
(248, 2, 16, 1, '2018-01-14 14:05:42', 1),
(249, 9, 12, 1, '2018-01-16 11:05:35', 1),
(250, 2, 2, 2, '2018-01-18 12:53:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms_otp_temp`
--

CREATE TABLE `sms_otp_temp` (
  `id` int(11) NOT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `otp` varchar(50) DEFAULT NULL,
  `valid` int(1) DEFAULT '0',
  `time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_otp_temp`
--

INSERT INTO `sms_otp_temp` (`id`, `mobile`, `otp`, `valid`, `time`) VALUES
(1, '919654407464', '1234', 0, '2018-01-05 13:41:10'),
(2, '919030090300', '1234', 0, '2018-01-05 13:52:55'),
(3, '9669230092300', '1234', 0, '2018-01-05 14:18:08'),
(4, '966499448746', '1234', 0, '2018-01-05 14:37:41'),
(5, '919999999999', '1234', 0, '2018-01-05 14:47:17'),
(6, '9661111111111', '1234', 0, '2018-01-07 05:51:19'),
(7, '912222222222', '1234', 0, '2018-01-07 06:27:40'),
(8, '966554804848', '1234', 0, '2018-01-07 08:34:47'),
(9, '966554804848', '1234', 0, '2018-01-07 08:41:25'),
(10, '9669630096300', '1234', 0, '2018-01-08 07:23:41'),
(11, '918560085600', '1234', 0, '2018-01-08 08:36:31'),
(12, '918960089600', '1234', 0, '2018-01-08 09:32:30'),
(13, '9669988556633', '1234', 0, '2018-01-08 11:04:17'),
(14, '9663663663663', '1234', 0, '2018-01-08 11:40:49'),
(15, '966502888235', '1234', 0, '2018-01-08 12:20:03'),
(16, '9660502888235', '1234', 0, '2018-01-08 12:48:20'),
(17, '91946446464', '1234', 0, '2018-01-09 05:19:25'),
(18, '918560085600', '1234', 0, '2018-01-09 05:26:19'),
(19, '91946446464', '1234', 0, '2018-01-09 05:35:03'),
(20, '918560085600', '1234', 0, '2018-01-09 05:44:41'),
(21, '9664664664664', '1234', 0, '2018-01-09 09:43:07'),
(22, '9665665665665', '1234', 0, '2018-01-09 09:44:13'),
(23, '9666336336336', '1234', 0, '2018-01-09 09:49:05'),
(24, '9669229229229', '1234', 0, '2018-01-09 09:53:40'),
(25, '9668228228228', '1234', 0, '2018-01-09 10:14:17'),
(26, '9664484484484', '1234', 0, '2018-01-09 10:54:51'),
(27, '9669219219219', '1234', 0, '2018-01-09 10:55:30'),
(28, '919530095300', '1234', 0, '2018-01-09 10:55:47'),
(29, '9667337337337', '1234', 0, '2018-01-09 10:56:03'),
(30, '9669999', '1234', 0, '2018-01-09 10:57:36'),
(31, '918025680256', '1234', 0, '2018-01-09 10:59:55'),
(32, '919725697256', '1234', 0, '2018-01-09 11:06:10'),
(33, '919725697256', '1234', 0, '2018-01-09 11:08:50'),
(34, '9662292292299', '1234', 0, '2018-01-09 11:39:40'),
(35, '91961285865088880880', '1234', 0, '2018-01-10 10:34:16'),
(36, '919112091120', '1234', 0, '2018-01-10 11:19:47'),
(37, '919416094160', '1234', 0, '2018-01-10 11:25:40'),
(38, '9666226226226', '1234', 0, '2018-01-10 12:54:41'),
(39, '919330093300', '1234', 0, '2018-01-10 13:43:48'),
(40, '919330093300', '1234', 0, '2018-01-10 13:45:08'),
(41, '919330093300', '1234', 0, '2018-01-10 13:45:31'),
(42, '919330093300', '1234', 0, '2018-01-10 13:46:23'),
(43, '919830098300', '1234', 0, '2018-01-11 06:38:00'),
(44, '919830098300', '1234', 0, '2018-01-11 06:38:42'),
(45, '98112106107', '1234', 0, '2018-01-11 07:16:47'),
(46, '+91981121061007', '1234', 0, '2018-01-11 07:19:04'),
(47, '9664484484484', '1234', 0, '2018-01-11 11:07:07');

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `id` int(12) NOT NULL,
  `userid` int(12) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `emailid` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `comment` longtext,
  `user_type` varchar(255) DEFAULT NULL,
  `option_type` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `support`
--

INSERT INTO `support` (`id`, `userid`, `name`, `emailid`, `photo`, `comment`, `user_type`, `option_type`, `created`) VALUES
(1, 7, 'Raman ', 'ramancu@mailinator.com', 'assets/documents/user-icon.png', '', 'customer', 0, '2018-01-09 08:43:57');

-- --------------------------------------------------------

--
-- Table structure for table `transactions_details`
--

CREATE TABLE `transactions_details` (
  `id` int(11) NOT NULL,
  `transaction_detail` longtext NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE `transaction_type` (
  `id` int(11) NOT NULL,
  `transaction_name` varchar(50) DEFAULT NULL,
  `transaction_name_ar` varchar(50) DEFAULT NULL,
  `transaction_mode` varchar(50) DEFAULT NULL,
  `transaction_mode_ar` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`id`, `transaction_name`, `transaction_name_ar`, `transaction_mode`, `transaction_mode_ar`, `status`) VALUES
(1, 'Money Transfer to Friend', 'تحویل النقود إلی Friend', 'debit', 'مدين', 1),
(2, 'Add Money', 'أضف النقود', 'credit', 'ائتمان', 1),
(3, 'Ride Transaction', 'معاملة الرحلة', 'debit', 'مدين', 1),
(4, 'Referral Amount', 'المبالغ الإحالة', 'credit', 'ائتمان', 1),
(5, 'Refund', 'المبالغ المردودۃ', 'credit', 'ائتمان', 1),
(6, 'Money Receive from Friend', 'حصول النقود من Friend', 'credit', 'ائتمان', 1),
(7, 'Money transfer to Bank', 'تحویل النقود إلي البنك', 'debit', 'مدين', 1),
(8, 'Recharge Coupon', 'کوبون المحفظة', 'credit', 'ائتمان', 1),
(9, 'Money transfer to Customer', 'تحويل الأموال إلى العميل', 'debit', 'مدين', 1),
(10, 'Money transfer from Driver', 'تحويل الأموال من سائق', 'credit', 'ائتمان', 1),
(11, 'Najez share for ride ride_id', 'حصة ناجيز للركو ride_id', 'debit', 'مدين', 1),
(12, 'Driver ride share', 'حصة ركوب سائق', 'credit', 'ائتمان', 1),
(13, 'Halal', 'حلال', 'debit', 'مدين', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trip_car_charges_master`
--

CREATE TABLE `trip_car_charges_master` (
  `id` int(11) NOT NULL,
  `surcharge_id` int(11) DEFAULT NULL,
  `figure` double DEFAULT NULL,
  `is_percent` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `created` datetime(3) DEFAULT NULL,
  `created_id` int(11) DEFAULT NULL,
  `trip_type_vehicle_family_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trip_type_master`
--

CREATE TABLE `trip_type_master` (
  `id` int(11) NOT NULL,
  `trip_type_name` varchar(255) DEFAULT NULL,
  `trip_type_name_ar` varchar(255) DEFAULT NULL,
  `city_master_id` int(11) DEFAULT NULL,
  `parent_trip_type_master_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trip_type_master`
--

INSERT INTO `trip_type_master` (`id`, `trip_type_name`, `trip_type_name_ar`, `city_master_id`, `parent_trip_type_master_id`, `created`) VALUES
(1, 'City', 'مدينة', NULL, NULL, '2017-08-03 03:47:31'),
(2, 'Long Trip', 'رحلة طويلة', NULL, NULL, '2017-08-03 03:47:31'),
(3, 'To Bahrain', 'إلى البحرين', NULL, NULL, '2017-08-03 03:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `trip_type_vehicle_family`
--

CREATE TABLE `trip_type_vehicle_family` (
  `id` int(11) NOT NULL,
  `trip_type_id` int(11) DEFAULT NULL,
  `vehicle_family_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trip_type_vehicle_family`
--

INSERT INTO `trip_type_vehicle_family` (`id`, `trip_type_id`, `vehicle_family_id`, `created`) VALUES
(1, 1, 1, '2017-10-10 04:50:42'),
(2, 1, 2, '2017-10-10 04:51:15'),
(3, 1, 3, '2017-10-10 04:51:23'),
(4, 1, 4, '2017-10-10 04:51:30'),
(5, 2, 1, '2017-10-10 04:51:37'),
(6, 2, 2, '2017-10-10 04:51:45'),
(7, 2, 3, '2017-10-10 10:21:49'),
(8, 2, 4, '2017-10-10 04:52:13'),
(9, 3, 1, '2017-10-10 04:52:21'),
(10, 3, 2, '2017-10-10 04:52:28'),
(11, 3, 3, '2017-10-10 10:22:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `unique_id` varchar(255) DEFAULT NULL,
  `emailid` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT 'assets/documents/user-icon.png',
  `mobile` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `latlng` longtext,
  `user_type` int(11) DEFAULT NULL,
  `total_ride` int(11) DEFAULT NULL,
  `referralcode` varchar(255) DEFAULT NULL,
  `is_referred` varchar(255) DEFAULT NULL,
  `total_amount` double(11,2) DEFAULT '0.00',
  `cancel_charge` double(11,2) DEFAULT '0.00',
  `verified` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '1',
  `resetcode` varchar(255) DEFAULT NULL,
  `webblock` int(12) DEFAULT '1',
  `login_type` varchar(50) DEFAULT '1',
  `latitude` longtext,
  `longitude` longtext,
  `chatid` varchar(255) DEFAULT NULL,
  `chatemail` varchar(255) DEFAULT NULL,
  `show_phone` int(12) DEFAULT '1',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `unique_id`, `emailid`, `password`, `fname`, `mname`, `lname`, `gender`, `photo`, `mobile`, `city`, `latlng`, `user_type`, `total_ride`, `referralcode`, `is_referred`, `total_amount`, `cancel_charge`, `verified`, `status`, `resetcode`, `webblock`, `login_type`, `latitude`, `longitude`, `chatid`, `chatemail`, `show_phone`, `created`, `modified`) VALUES
(1, 'Raj', 'USR0342', 'isqdr@gmail.com', '789789789', 'Raj', NULL, NULL, 'male', 'assets/documents/user-icon.png', '919654407464', NULL, '[\"\",\"\"]', 3, NULL, 'RAJ4460', '', 40.00, 0.00, 1, 1, NULL, 1, '1', '28.598889127050192', '77.3152656108141', NULL, NULL, 1, '2018-01-05 13:41:11', '2018-02-06 12:49:11'),
(2, 'k', 'USR3397', 'k@k.com', '9999999999', 'k', NULL, NULL, 'male', 'assets/documents/user-icon.png', '919999999999', NULL, '[\"\",\"\"]', 3, NULL, 'K2334', '', 55.00, 0.00, 1, 1, NULL, 1, '1', '28.44314569770213', '77.05583900213242', NULL, NULL, 1, '2018-01-05 14:47:19', '2018-01-05 14:52:59'),
(3, 'k', 'USR3817', 'k@kkk.com', '2222222222', 'k', NULL, NULL, 'male', 'assets/documents/user-icon.png', '912222222222', NULL, '[\"\",\"\"]', 3, NULL, 'K5596', '', 12.00, 0.00, 1, 1, NULL, 1, '1', '28.40950486956285', '77.03383784741163', NULL, NULL, 1, '2018-01-07 06:27:42', '2018-01-07 06:29:00'),
(4, 'Muhammed ', 'USR8138', 'najez.2017@gmail.com', 'M769200m', 'Muhammed ', NULL, NULL, 'male', 'assets/documents/user-icon.png', '966554804848', NULL, '[\"\",\"\"]', 3, NULL, 'MUH5859', '', 0.00, 0.00, 1, 1, NULL, 1, '1', '26.31555507201684', '50.22889204323292', NULL, NULL, 1, '2018-01-07 08:41:26', '2018-01-07 15:41:25'),
(5, 'iOS new Customer ', 'USR4692', 'customernew@mail.com', '56785678', 'iOS new Customer ', NULL, NULL, 'Male', 'assets/documents/user-icon.png', '918560085600', NULL, '[\"\",\"\"]', 3, NULL, 'IOS5516', '', 0.00, 0.00, 1, 1, NULL, 1, '1', '28.4434963424873', '77.0560101360991', '40509722', '5', 1, '2018-01-08 08:36:32', '2018-01-09 11:04:27'),
(6, 'iOS new Customer3 would', 'USR8299', 'customernewtwo@mail.com', '56785678', 'iOS new Customer3 would', NULL, NULL, 'Male', 'assets/documents/user-icon.png', '918960089600', NULL, '[\"\",\"\"]', 3, NULL, 'IOS3577', '', 52.00, 0.00, 1, 1, NULL, 1, '1', '28.4433824184025', '77.0560982221497', '40462747', '6', 1, '2018-01-08 09:32:31', '2018-01-08 11:34:27'),
(7, 'Raman', 'USR0615', 'ramancu@mailinator.com', '900900900', 'Ramanbobkbb:&***?**', '', '', '', 'assets/documents/user-icon.png', '9663663663663', NULL, '[\"\",\"\"]', 3, NULL, 'RAM3355', '', 55.00, 0.00, 1, 1, NULL, 1, '1', '28.44330577669596', '77.05580580979586', '40467735', '7', 1, '2018-01-08 11:40:50', '2018-01-17 06:37:27'),
(8, 'محمد', 'USR3340', 'boohal@hotmail.com', 'Mo123123', 'محمد', NULL, NULL, 'male', 'assets/documents/user-icon.png', '966502888235', NULL, '[\"\",\"\"]', 3, NULL, 'م', '', 0.00, 0.00, 0, 1, NULL, 1, '1', NULL, NULL, NULL, NULL, 1, '2018-01-08 12:20:04', '2018-01-08 12:20:04'),
(9, NULL, 'USR3090', 'boohal458@gmail.com', NULL, 'mod j', NULL, NULL, '', 'assets/documents/user-icon.png', '9660502888235', NULL, NULL, NULL, NULL, 'MOD6723', NULL, 0.00, 0.00, 1, 1, NULL, 1, 'google', NULL, NULL, NULL, NULL, 1, '2018-01-08 12:48:03', '2018-01-08 12:48:35'),
(10, 'nitin', 'USR5767', 'nitin@gmail.com', '900900900', 'nitin', NULL, NULL, 'female', 'assets/documents/user-icon.png', '9664664664664', NULL, '[\"\",\"\"]', 3, NULL, 'NIT9029', '', 0.00, 0.00, 1, 1, NULL, 1, '1', '28.4277693', '77.0381947', NULL, NULL, 1, '2018-01-09 09:43:08', '2018-01-12 16:42:28'),
(11, 'shiwanshu', 'USR1164', 'shiwanshu@gmail.com', '900900900', 'shiwanshu', NULL, NULL, 'male', 'assets/documents/user-icon.png', '9665665665665', NULL, '[\"\",\"\"]', 3, NULL, 'SHI1414', '', 1105.00, 0.00, 1, 1, NULL, 1, '1', '28.4434755749457', '77.0559290851377', NULL, NULL, 1, '2018-01-09 09:44:13', '2018-01-10 12:52:57'),
(12, 'raman', 'USR5115', 'raman@gmail.com', '900900900', 'raman', NULL, NULL, 'male', 'assets/documents/user-icon.png', '9666336336336', NULL, '[\"\",\"\"]', 3, NULL, 'RAM4566', '', 0.00, 0.00, 1, 1, NULL, 1, '1', '28.4438039952185', '77.05605659633875', NULL, NULL, 1, '2018-01-09 09:49:06', '2018-01-09 09:49:14'),
(13, 'ankita', 'USR0778', 'ankita@gmal.com', '900900900', 'ankita', NULL, NULL, 'male', 'assets/documents/user-icon.png', '9664484484484', NULL, '[\"\",\"\"]', 3, NULL, 'ANK2177', 'RAM4566', 248.00, 0.00, 1, 1, NULL, 1, '1', '28.4434475775913', '77.0558792352676', NULL, NULL, 1, '2018-01-09 10:54:51', '2018-01-19 09:01:24'),
(14, 'ankita', 'USR0147', 'ankita@gmail.com', '900900900', 'ankita', NULL, NULL, 'male', 'assets/documents/user-icon.png', '9669219219219', NULL, '[\"\",\"\"]', 3, NULL, 'ANK4102', 'RAM4566', 20.00, 0.00, 0, 1, NULL, 1, '1', NULL, NULL, NULL, NULL, 1, '2018-01-09 10:55:31', '2018-01-09 10:55:31'),
(15, 'ankita', 'USR2320', 'ankita373@gmail.com', '900900900', 'ankita', NULL, NULL, 'male', 'assets/documents/user-icon.png', '9667337337337', NULL, '[\"\",\"\"]', 3, NULL, 'ANK8584', 'RAM4566', 20.00, 0.00, 0, 1, NULL, 1, '1', '26.3031203378251', '50.2070328029637', NULL, NULL, 1, '2018-01-09 10:56:05', '2018-01-28 10:39:35'),
(16, 'jj', 'USR1676', 'jjj@gna.com', 'mkkj99999999', 'jj', NULL, NULL, 'female', 'assets/documents/user-icon.png', '9669999', NULL, '[\"\",\"\"]', 3, NULL, 'JJ4418', '', 0.00, 0.00, 1, 0, NULL, 1, '1', NULL, NULL, NULL, NULL, 1, '2018-01-09 10:57:37', '2018-03-19 15:38:58'),
(17, 'Testcustmr', 'USR7582', 'customer@mail.com', '323452345', 'Testcustmr', NULL, NULL, 'Male', 'assets/documents/user-icon.png', '919725697256', NULL, '[\"\",\"\"]', 3, NULL, 'TES6821', 'SHI1414', 20.00, 0.00, 1, 0, NULL, 1, '1', '28.4434484620038', '77.0559231564403', NULL, NULL, 1, '2018-01-09 11:06:11', '2018-03-19 15:38:58'),
(18, 'ravish kumar', 'USR5890', 'ravizh@mail.com', '67}96789', 'ravish kumar', NULL, NULL, 'male', 'assets/documents/user-icon.png', '91961285865088880880', NULL, '[\"\",\"\"]', 3, NULL, 'RAV1711', '', 0.00, 0.00, 1, 0, NULL, 1, '1', '26.3027146299443', '50.2182638987303', NULL, NULL, 1, '2018-01-10 10:34:17', '2018-03-19 15:38:58'),
(19, 'brijesh', 'USR8327', 'brijesh@mail.com', '78900987', 'brijesh', NULL, NULL, 'male', 'assets/documents/user-icon.png', '919112091120', NULL, '[\"\",\"\"]', 3, NULL, 'BRI9127', '', 0.00, 0.00, 1, 0, NULL, 1, '1', '39.724185794722764', '47.984747104346745', NULL, NULL, 1, '2018-01-10 11:19:48', '2018-03-19 15:38:58'),
(20, 'Facebook ', 'USR3872', 'fb@mail.com', '12344321', 'Facebook ', NULL, NULL, 'male', 'assets/documents/user-icon.png', '919416094160', NULL, '[\"\",\"\"]', 3, NULL, 'FAC2481', '', 0.00, 0.00, 1, 0, NULL, 1, '1', '28.443231485922336', '77.05588426440954', NULL, NULL, 1, '2018-01-10 11:25:41', '2018-03-19 15:38:58'),
(21, 'Mahesh', 'USR3471', 'm@gmail.com', '900900900', 'Mahesh', NULL, NULL, 'Male', 'assets/documents/user-icon.png', '9666226226226', NULL, '[\"\",\"\"]', 3, NULL, 'MAH8061', '', 0.00, 0.00, 1, 0, NULL, 1, '1', '28.4433691809241', '77.0560052338629', '40588601', '21', 1, '2018-01-10 12:54:42', '2018-03-19 15:38:58'),
(22, 'Testcust', 'USR5500', 'testcust@mail.com', '45674567', 'Testcust', NULL, NULL, 'Male', 'assets/documents/user-icon.png', '919330093300', NULL, '[\"\",\"\"]', 3, NULL, 'TES4090', '', 0.00, 0.00, 1, 0, NULL, 1, '1', '28.4434734903028', '77.0560933202814', '40690349', '23', 1, '2018-01-10 13:43:49', '2018-03-19 15:38:58'),
(23, 'Najez Customer', 'USR2319', 'najzcustomer@mail.com', '23455432', 'Najez Customer', NULL, NULL, 'Male', 'assets/documents/user-icon.png', '919830098300', NULL, '[\"\",\"\"]', 3, NULL, 'NAJ8485', '', 0.00, 0.00, 1, 0, NULL, 1, '1', '28.4434763040328', '77.0560026674866', NULL, NULL, 1, '2018-01-11 06:38:02', '2018-03-19 17:20:20'),
(24, 'test', 'USR4417', 'nitin.yadav@mail.com', 'nitin.test', 'test', NULL, NULL, 'male', 'assets/documents/user-icon.png', '98112106107', 'test', '[\"34536565\",\"456564564\"]', 3, NULL, 'TES8893', '', 0.00, 0.00, 1, 1, NULL, 1, '1', NULL, NULL, NULL, NULL, 1, '2018-01-11 07:16:49', '2018-03-19 17:12:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_feedback`
--

CREATE TABLE `user_feedback` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `decription` varchar(255) DEFAULT NULL,
  `feedback_master_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_locations`
--

CREATE TABLE `user_locations` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `latitude` longtext NOT NULL,
  `longitude` longtext NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `notify_action` varchar(255) NOT NULL,
  `notify_val` longtext NOT NULL,
  `notify_status` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_payment_transfer`
--

CREATE TABLE `user_payment_transfer` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `transfer_userid` int(11) NOT NULL,
  `transaction_amount` double NOT NULL DEFAULT '0',
  `description` varchar(50) DEFAULT NULL,
  `transaction_mode` varchar(50) DEFAULT NULL COMMENT 'debit,credit',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_preferred_locations`
--

CREATE TABLE `user_preferred_locations` (
  `id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `GPS_location` varchar(255) DEFAULT NULL,
  `location_address` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_referal_code`
--

CREATE TABLE `user_referal_code` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `referal_user_id` int(11) NOT NULL,
  `referal_code` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_reviews`
--

CREATE TABLE `user_reviews` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `rideid` int(11) NOT NULL,
  `option_type` int(11) NOT NULL DEFAULT '0',
  `star` int(11) DEFAULT '0',
  `review` longtext,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_reviews`
--

INSERT INTO `user_reviews` (`id`, `userid`, `rideid`, `option_type`, `star`, `review`, `created`) VALUES
(1, 1, 1, 0, 4, '', '2018-01-05 14:00:18'),
(2, 2, 5, 0, 4, '', '2018-01-05 14:51:23'),
(3, 3, 6, 0, 0, '6', '2018-01-07 06:28:49'),
(4, 6, 10, 0, 0, '10', '2018-01-08 09:39:29'),
(5, 6, 11, 0, 0, '11', '2018-01-08 10:02:59'),
(6, 6, 12, 0, 0, '12', '2018-01-08 10:10:05'),
(7, 1, 15, 0, 4, '', '2018-01-08 13:45:25'),
(8, 7, 17, 0, 1, '', '2018-01-09 11:24:11'),
(9, 11, 29, 0, 0, '29', '2018-01-10 12:35:29'),
(10, 11, 31, 0, 0, '31', '2018-01-10 12:40:22'),
(11, 11, 32, 0, 0, '32', '2018-01-10 12:44:21'),
(12, 13, 38, 0, 0, '38', '2018-01-11 11:18:59'),
(13, 1, 43, 0, 3, '', '2018-01-11 11:27:57'),
(14, 13, 45, 0, 0, '45', '2018-01-11 11:35:01'),
(15, 13, 47, 0, 0, '47', '2018-01-11 11:48:36'),
(16, 13, 49, 0, 0, '49', '2018-01-11 11:56:07'),
(17, 13, 50, 0, 0, '50', '2018-01-11 12:06:32'),
(18, 13, 52, 0, 0, '52', '2018-01-11 13:38:20'),
(19, 13, 53, 0, 0, '53', '2018-01-11 13:41:48'),
(20, 13, 56, 0, 0, '56', '2018-01-12 08:28:25'),
(21, 13, 57, 0, 0, '57', '2018-01-12 08:31:31'),
(22, 13, 58, 0, 0, '58', '2018-01-12 08:34:21'),
(23, 13, 60, 0, 0, '60', '2018-01-12 08:52:22'),
(24, 13, 61, 0, 0, '61', '2018-01-12 09:11:18'),
(25, 13, 62, 0, 0, '62', '2018-01-12 09:17:38'),
(26, 13, 66, 0, 0, '66', '2018-01-12 10:11:39'),
(27, 13, 67, 0, 0, '67', '2018-01-12 10:24:32'),
(28, 13, 68, 0, 0, '68', '2018-01-12 10:26:23'),
(29, 13, 69, 0, 0, '69', '2018-01-12 10:30:14'),
(30, 13, 71, 0, 0, '71', '2018-01-12 10:49:11'),
(31, 13, 73, 0, 0, '73', '2018-01-12 10:59:30'),
(32, 13, 74, 0, 0, '74', '2018-01-12 11:23:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_support_option`
--

CREATE TABLE `user_support_option` (
  `id` int(12) NOT NULL,
  `en_option` longtext,
  `ar_option` longtext,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_support_option`
--

INSERT INTO `user_support_option` (`id`, `en_option`, `ar_option`, `created`) VALUES
(1, 'This is test', 'This is test', '2017-09-01 02:24:53'),
(2, 'This is test2', 'This is test2', '2017-09-01 02:24:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_transactions`
--

CREATE TABLE `user_transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `transfer_userid` int(11) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `transaction_type_id` int(11) DEFAULT NULL,
  `transaction_amount` double(11,2) NOT NULL DEFAULT '0.00',
  `transaction_detail` longtext,
  `transaction_id` varchar(50) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_transactions`
--

INSERT INTO `user_transactions` (`id`, `user_id`, `ride_id`, `transfer_userid`, `voucher_id`, `transaction_type_id`, `transaction_amount`, `transaction_detail`, `transaction_id`, `status`, `created`) VALUES
(1, 2, 5, 1, NULL, 10, 47.00, 'Money transfer from Driver', NULL, 1, NULL),
(2, 6, 10, 6, NULL, 10, 8.00, 'Money transfer from Driver', NULL, 1, NULL),
(3, 6, 11, 6, NULL, 10, 8.00, 'Money transfer from Driver', NULL, 1, NULL),
(4, 1, 15, 1, NULL, 10, 12.00, 'Money transfer from Driver', NULL, 1, NULL),
(5, 7, 17, 12, NULL, 10, 43.00, 'Money transfer from Driver', NULL, 1, NULL),
(6, 11, 31, 9, NULL, 10, 90.00, 'Money transfer from Driver', NULL, 1, NULL),
(7, 11, 32, 9, NULL, 10, 991.00, 'Money transfer from Driver', NULL, 1, NULL),
(8, 13, 38, NULL, NULL, 3, 12.00, 'Ride transaction', NULL, 1, NULL),
(9, 13, 38, 2, NULL, 10, 12.00, 'Money transfer from Driver', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_wallet_details`
--

CREATE TABLE `user_wallet_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ride_id` int(11) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `transfer_name` varchar(200) DEFAULT NULL,
  `transaction_type_id` int(11) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL,
  `amount_num` double DEFAULT NULL,
  `transaction_mode` varchar(50) DEFAULT NULL,
  `transaction_datetime` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_wallet_details`
--

INSERT INTO `user_wallet_details` (`id`, `user_id`, `ride_id`, `description`, `transfer_name`, `transaction_type_id`, `amount`, `amount_num`, `transaction_mode`, `transaction_datetime`, `status`) VALUES
(1, 2, 5, 'Money transfer from Driver', NULL, 10, '+ SAR 47', 47, 'credit', '2018-01-05 14:50:32', 1),
(2, 6, 10, 'Money transfer from Driver', NULL, 10, '+ SAR 8', 8, 'credit', '2018-01-08 09:39:00', 1),
(3, 6, 11, 'Money transfer from Driver', NULL, 10, '+ SAR 8', 8, 'credit', '2018-01-08 10:02:20', 1),
(4, 1, 15, 'Money transfer from Driver', NULL, 10, '+ SAR 12', 12, 'credit', '2018-01-08 13:44:06', 1),
(5, 7, 17, 'Money transfer from Driver', NULL, 10, '+ SAR 43', 43, 'credit', '2018-01-09 11:22:49', 1),
(6, 11, 31, 'Money transfer from Driver', NULL, 10, '+ SAR 90', 90, 'credit', '2018-01-10 12:40:09', 1),
(7, 11, 32, 'Money transfer from Driver', NULL, 10, '+ SAR 991', 991, 'credit', '2018-01-10 12:41:20', 1),
(8, 13, 38, 'Ride transaction', NULL, 3, '- SAR 12', 12, 'debit', '2018-01-11 11:16:31', 1),
(9, 13, 38, 'Money transfer from Driver', NULL, 10, '+ SAR 12', 12, 'credit', '2018-01-11 11:18:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `vehicle_sno` varchar(255) DEFAULT NULL,
  `vehicle_number` varchar(255) DEFAULT NULL,
  `vehicle_manufacture_year` varchar(255) DEFAULT NULL,
  `vehicle_type` varchar(255) DEFAULT NULL,
  `vehicle_model` varchar(255) DEFAULT NULL,
  `vehicle_color` varchar(255) DEFAULT NULL,
  `vehicle_year` text,
  `photograph` text,
  `identity` text,
  `vehicle_regisration` text,
  `vehicle_insurance` text,
  `authorization_image` text,
  `bank_card` longtext,
  `iban_bank` varchar(255) DEFAULT NULL,
  `vehicle_family_id` int(11) DEFAULT '0',
  `driver_id` int(12) DEFAULT '0',
  `driver_license` text,
  `active` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `vehicle_sno`, `vehicle_number`, `vehicle_manufacture_year`, `vehicle_type`, `vehicle_model`, `vehicle_color`, `vehicle_year`, `photograph`, `identity`, `vehicle_regisration`, `vehicle_insurance`, `authorization_image`, `bank_card`, `iban_bank`, `vehicle_family_id`, `driver_id`, `driver_license`, `active`, `created`) VALUES
(1, 'hhdhhbshh683764963', 'HHV-5405', '2016', 'Medium', '18', 'Orange', NULL, NULL, NULL, NULL, NULL, NULL, '', '', 1, 1, NULL, 1, '2018-01-05 13:53:59'),
(2, '76463777366727', 'GHJ-2845', '2015', 'Family', '53', 'Black', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 2, 2, NULL, 1, '2018-01-05 14:19:48'),
(3, '99876', '788-5549', '2017', 'Luxury', '5', 'Brown', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 3, 3, NULL, 1, '2018-01-05 14:39:21'),
(4, '11111111', 'AAA-1111', '2010', 'Luxury', '6', 'Red', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 3, 4, NULL, 1, '2018-01-07 05:52:22'),
(5, '123545668778688', 'LOU-1234', '2017', 'Medium', '1', 'Black', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 5, NULL, 1, '2018-01-07 08:36:51'),
(7, '64774783766373', 'BVH-5483', '2016', 'Luxury', '216', '???????', NULL, NULL, NULL, NULL, NULL, NULL, '', '', 3, 1, NULL, 1, '2018-01-08 10:14:03'),
(9, '67ey3764764', 'CVF-8485', '2016', 'Luxury', '210', 'Black', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 3, 6, NULL, 1, '2018-01-08 10:52:49'),
(10, 'hh789i8i', 'GGB-8888', '2014', 'Luxury', '209', 'White', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 3, 7, NULL, 1, '2018-01-08 11:07:26'),
(11, 'uu779', 'UP-3366', '2016', 'Luxury', '6', 'Black', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 3, 9, NULL, 1, '2018-01-09 09:55:57'),
(12, 'hg68887', 'GG-6589', '2016', 'Medium', '19', 'Brown', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 9, NULL, 1, '2018-01-09 10:10:39'),
(14, '636674747647477636', 'BJO-5843', '2018', 'Luxury', '210', 'Black', NULL, NULL, NULL, NULL, NULL, NULL, '', '', 3, 11, NULL, 1, '2018-01-09 10:56:59'),
(15, '63777367377378', 'BBH-5483', '2015', 'Luxury', '210', 'Black', NULL, NULL, NULL, NULL, NULL, NULL, '', '', 3, 12, NULL, 1, '2018-01-09 11:01:08'),
(16, '63633733733447337', 'ghu-5483', '2016', 'Medium', '192', 'White', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 1, 2, NULL, 1, '2018-01-12 12:34:04'),
(17, '11111', 'XYZ-1234', '2012', 'Medium', '2', 'Black', NULL, NULL, NULL, NULL, NULL, NULL, '1234545', 'National Bank of Kuwait (NBK)', 1, 3, NULL, 0, '2018-03-16 15:43:44'),
(18, '11221', 'XYZ-1234', '2012', 'Medium', '195', 'Red', NULL, NULL, NULL, NULL, NULL, NULL, '111111', 'National Bank of Bahrain (NBB)', 1, 8, NULL, 0, '2018-03-16 16:01:06'),
(19, 'Bu278YY', 'AAA-4455', '2012', 'Luxury', 'A6', 'Silver', '2015', '', '', '', '', '', 'Silver', 'The National Commercial Bank', 1, NULL, '', 0, '2018-03-16 16:06:02'),
(20, 'Bu27899445', 'DL6-0568', '2014', 'Luxury', '6', 'Green', NULL, NULL, NULL, NULL, NULL, NULL, '78987', 'Muscat Bank', 3, 10, NULL, 0, '2018-03-28 11:30:25'),
(21, 'Bu2789944', 'ssd-2323', '2010', 'Luxury', '5', 'Green', NULL, NULL, NULL, NULL, NULL, NULL, '45345', '', 3, 3, NULL, 0, '2018-03-28 12:54:03'),
(22, 'BBBB2277', 'SAS-9998', '2011', 'Medium', '17', 'White', NULL, NULL, NULL, NULL, NULL, NULL, 'CXDVCZD12', 'Qatar National Bank (QNB)', 1, 5, NULL, 0, '2018-03-28 13:00:47');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_documents`
--

CREATE TABLE `vehicle_documents` (
  `id` int(12) NOT NULL,
  `driver_id` int(12) DEFAULT NULL,
  `vehicle_id` int(12) DEFAULT NULL,
  `doc_key` varchar(255) DEFAULT NULL,
  `doc_val` longtext,
  `doc_expire` varchar(255) DEFAULT NULL,
  `isexpire` int(12) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modify` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_documents`
--

INSERT INTO `vehicle_documents` (`id`, `driver_id`, `vehicle_id`, `doc_key`, `doc_val`, `doc_expire`, `isexpire`, `created`, `modify`) VALUES
(1, 1, 1, 'photograph', 'assets/cars/BMW/BMW_2-series_F45_Active_Tourer_Electrical_2016_360_720_50-1.png', '', 0, '2018-01-05 13:53:59', '2018-01-05 13:53:59'),
(2, 1, 1, 'vehicle-reg', 'assets/documents/5a4f8377976a9.jpg', NULL, 0, '2018-01-05 13:53:59', '2018-01-05 13:53:59'),
(3, 1, 1, 'vehicle-insur', 'assets/documents/5a4f83779786e.jpg', NULL, 0, '2018-01-05 13:53:59', '2018-01-05 13:53:59'),
(4, 1, 1, 'auth-img', 'assets/documents/5a4f837797a14.jpg', '', 0, '2018-01-05 13:53:59', '2018-01-05 13:53:59'),
(5, 1, 1, 'tafweeth-img', '', NULL, 0, '2018-01-05 13:53:59', '2018-01-05 13:53:59'),
(6, 2, 2, 'photograph', 'assets/cars/GMC/GMC_Yukon_Mk4_GMTK2UG_XL_2014_360_720_50-1.png', '', 0, '2018-01-05 14:19:48', '2018-01-05 14:19:48'),
(7, 2, 2, 'vehicle-reg', 'assets/documents/5a4f898489d3f.jpg', NULL, 0, '2018-01-05 14:19:48', '2018-01-05 14:19:48'),
(8, 2, 2, 'vehicle-insur', 'assets/documents/5a4f898489eb1.jpg', NULL, 0, '2018-01-05 14:19:48', '2018-01-05 14:19:48'),
(9, 2, 2, 'auth-img', 'assets/documents/5a4f89848a00a.jpg', '', 0, '2018-01-05 14:19:48', '2018-01-05 14:19:48'),
(10, 2, 2, 'tafweeth-img', '', '', 0, '2018-01-05 14:19:48', '2018-01-05 14:19:48'),
(11, 3, 3, 'photograph', 'assets/cars/Audi/Audi_A8_Mk3f_D4_2014_360_720_50-1.png', '', 0, '2018-01-05 14:39:21', '2018-01-05 14:39:21'),
(12, 3, 3, 'vehicle-reg', 'assets/documents/5a4f8e19502cf.jpg', NULL, 0, '2018-01-05 14:39:21', '2018-01-05 14:39:21'),
(13, 3, 3, 'vehicle-insur', 'assets/documents/5a4f8e1950482.jpg', NULL, 0, '2018-01-05 14:39:21', '2018-01-05 14:39:21'),
(14, 3, 3, 'auth-img', 'assets/documents/5a4f8e195060b.jpg', '', 0, '2018-01-05 14:39:21', '2018-01-05 14:39:21'),
(15, 3, 3, 'tafweeth-img', 'assets/documents/5a4f8e195078a.jpg', '', 0, '2018-01-05 14:39:21', '2018-01-05 14:39:21'),
(16, 4, 4, 'photograph', 'assets/cars/Audi/Audi_SQ5_2013_360_720_50-1.png', '', 0, '2018-01-07 05:52:22', '2018-01-07 05:52:22'),
(17, 4, 4, 'vehicle-reg', 'assets/documents/5a51b59687476.jpg', NULL, 0, '2018-01-07 05:52:22', '2018-01-07 05:52:22'),
(18, 4, 4, 'vehicle-insur', 'assets/documents/5a51b59687694.jpg', NULL, 0, '2018-01-07 05:52:22', '2018-01-07 05:52:22'),
(19, 4, 4, 'auth-img', 'assets/documents/5a51b5968789d.jpg', '', 0, '2018-01-07 05:52:22', '2018-01-07 05:52:22'),
(20, 4, 4, 'tafweeth-img', '', '', 0, '2018-01-07 05:52:22', '2018-01-07 05:52:22'),
(21, 5, 5, 'photograph', 'assets/cars/Audi/Audi_A4_Sedan_2013_360_720_50-1.png', '', 0, '2018-01-07 08:36:51', '2018-01-07 08:36:51'),
(22, 5, 5, 'vehicle-reg', 'assets/documents/5a51dc239c0bb.jpg', NULL, 0, '2018-01-07 08:36:51', '2018-01-07 08:36:51'),
(23, 5, 5, 'vehicle-insur', 'assets/documents/5a51dc239c2aa.jpg', NULL, 0, '2018-01-07 08:36:51', '2018-01-07 08:36:51'),
(24, 5, 5, 'auth-img', 'assets/documents/5a51dc239c46a.jpg', '', 0, '2018-01-07 08:36:51', '2018-01-07 08:36:51'),
(25, 5, 5, 'tafweeth-img', 'assets/documents/5a51dc239c61e.jpg', '', 0, '2018-01-07 08:36:51', '2018-01-07 08:36:51'),
(26, 6, 6, 'photograph', 'assets/cars/GMC/GMC_Yukon_Mk4_GMTK2UG_XL_2014_360_720_50-1.png', '', 0, '2018-01-08 07:26:56', '2018-01-08 07:26:56'),
(27, 6, 6, 'vehicle-reg', 'assets/documents/5a531d402ed35.jpg', NULL, 0, '2018-01-08 07:26:56', '2018-01-08 07:26:56'),
(28, 6, 6, 'vehicle-insur', 'assets/documents/5a531d402eeae.jpg', NULL, 0, '2018-01-08 07:26:56', '2018-01-08 07:26:56'),
(29, 6, 6, 'auth-img', 'assets/documents/5a531d402f030.jpg', '', 0, '2018-01-08 07:26:56', '2018-01-08 07:26:56'),
(30, 6, 6, 'tafweeth-img', '', '', 0, '2018-01-08 07:26:56', '2018-01-08 07:26:56'),
(31, 1, 7, 'photograph', 'assets/cars/Lincoln/Lincoln_Navigator_Mk4_U554_Black_Label_2017_360_720_50-1.png', '', 0, '2018-01-08 10:14:03', '2018-01-08 10:14:03'),
(32, 1, 7, 'vehicle-reg', 'assets/documents/5a53446bf3845.jpg', NULL, 0, '2018-01-08 10:14:03', '2018-01-08 10:14:03'),
(33, 1, 7, 'vehicle-insur', 'assets/documents/5a53446bf3a04.jpg', NULL, 0, '2018-01-08 10:14:03', '2018-01-08 10:14:03'),
(34, 1, 7, 'auth-img', 'assets/documents/5a53446bf3b81.jpg', '', 0, '2018-01-08 10:14:03', '2018-01-08 10:14:03'),
(35, 1, 7, 'tafweeth-img', '', NULL, 0, '2018-01-08 10:14:03', '2018-01-08 10:14:03'),
(36, 6, 8, 'photograph', 'assets/cars/Audi/Audi_A4_Sedan_2013_360_720_50-1.png', '', 0, '2018-01-08 10:25:53', '2018-01-08 10:25:53'),
(37, 6, 8, 'vehicle-reg', 'assets/documents/5a53473153cc9.jpg', NULL, 0, '2018-01-08 10:25:53', '2018-01-08 10:25:53'),
(38, 6, 8, 'vehicle-insur', 'assets/documents/5a53473153e6f.jpg', NULL, 0, '2018-01-08 10:25:53', '2018-01-08 10:25:53'),
(39, 6, 8, 'auth-img', 'assets/documents/5a53473154002.jpg', '', 0, '2018-01-08 10:25:53', '2018-01-08 10:25:53'),
(40, 6, 8, 'tafweeth-img', '', '', 0, '2018-01-08 10:25:53', '2018-01-08 10:25:53'),
(41, 6, 9, 'photograph', 'assets/cars/Lincoln/Lincoln_Continental_concept_2015_360_720_50-1.png', '', 0, '2018-01-08 10:52:49', '2018-01-08 10:52:49'),
(42, 6, 9, 'vehicle-reg', 'assets/documents/5a534d8168928.jpg', NULL, 0, '2018-01-08 10:52:49', '2018-01-08 10:52:49'),
(43, 6, 9, 'vehicle-insur', 'assets/documents/5a534d8168aa2.jpg', NULL, 0, '2018-01-08 10:52:49', '2018-01-08 10:52:49'),
(44, 6, 9, 'auth-img', 'assets/documents/5a534d8168bfa.jpg', '', 0, '2018-01-08 10:52:49', '2018-01-08 10:52:49'),
(45, 6, 9, 'tafweeth-img', '', '', 0, '2018-01-08 10:52:49', '2018-01-08 10:52:49'),
(46, 7, 10, 'photograph', 'assets/cars/Lincoln/Lincoln_Continental_concept_2015_360_720_50-1.png', '', 0, '2018-01-08 11:07:26', '2018-01-08 11:07:26'),
(47, 7, 10, 'vehicle-reg', 'assets/documents/5a5350eeacb0e.jpg', NULL, 0, '2018-01-08 11:07:26', '2018-01-08 11:07:26'),
(48, 7, 10, 'vehicle-insur', 'assets/documents/5a5350eeaccc5.jpg', NULL, 0, '2018-01-08 11:07:26', '2018-01-08 11:07:26'),
(49, 7, 10, 'auth-img', 'assets/documents/5a5350eeacf25.jpg', '', 0, '2018-01-08 11:07:26', '2018-01-08 11:07:26'),
(50, 7, 10, 'tafweeth-img', '', '', 0, '2018-01-08 11:07:26', '2018-01-08 11:07:26'),
(51, 9, 11, 'photograph', 'assets/cars/Audi/Audi_SQ5_2013_360_720_50-1.png', '', 0, '2018-01-09 09:55:57', '2018-01-09 09:55:57'),
(52, 9, 11, 'vehicle-reg', 'assets/documents/5a5491adab911.jpg', NULL, 0, '2018-01-09 09:55:57', '2018-01-09 09:55:57'),
(53, 9, 11, 'vehicle-insur', 'assets/documents/5a5491adababf.jpg', NULL, 0, '2018-01-09 09:55:57', '2018-01-09 09:55:57'),
(54, 9, 11, 'auth-img', 'assets/documents/5a5491adabc58.jpg', '', 0, '2018-01-09 09:55:57', '2018-01-09 09:55:57'),
(55, 9, 11, 'tafweeth-img', 'assets/documents/5a5491adabdd2.jpg', '', 0, '2018-01-09 09:55:57', '2018-01-09 09:55:57'),
(56, 9, 12, 'photograph', 'assets/cars/BMW/BMW_3-series_Mk6_F30_sedan_E_2016_360_720_50-1.png', '', 0, '2018-01-09 10:10:39', '2018-01-09 10:10:39'),
(57, 9, 12, 'vehicle-reg', 'assets/documents/5a54952001195.jpg', NULL, 0, '2018-01-09 10:10:39', '2018-01-09 10:10:39'),
(58, 9, 12, 'vehicle-insur', 'assets/documents/5a54952001339.jpg', NULL, 0, '2018-01-09 10:10:39', '2018-01-09 10:10:39'),
(59, 9, 12, 'auth-img', 'assets/documents/5a549520014bd.jpg', '', 0, '2018-01-09 10:10:39', '2018-01-09 10:10:39'),
(60, 9, 12, 'tafweeth-img', 'assets/documents/5a54952001650.jpg', '', 0, '2018-01-09 10:10:39', '2018-01-09 10:10:39'),
(61, 10, 13, 'photograph', 'assets/cars/Audi/Audi_Q7_Mk2_e-tron_2017_360_720_50-1.png', '', 0, '2018-01-09 10:14:51', '2018-01-09 10:14:51'),
(62, 10, 13, 'vehicle-reg', '', NULL, 0, '2018-01-09 10:14:51', '2018-01-09 10:14:51'),
(63, 10, 13, 'vehicle-insur', '', NULL, 0, '2018-01-09 10:14:51', '2018-01-09 10:14:51'),
(64, 10, 13, 'auth-img', '', '', 0, '2018-01-09 10:14:51', '2018-01-09 10:14:51'),
(65, 10, 13, 'tafweeth-img', 'assets/documents/5a54961b1899f.jpg', '', 0, '2018-01-09 10:14:51', '2018-01-09 10:14:51'),
(66, 11, 14, 'photograph', 'assets/cars/Lincoln/Lincoln_Continental_concept_2015_360_720_50-1.png', '', 0, '2018-01-09 10:56:59', '2018-01-09 10:56:59'),
(67, 11, 14, 'vehicle-reg', 'assets/documents/5a549ffbbc3bf.jpg', NULL, 0, '2018-01-09 10:56:59', '2018-01-09 10:56:59'),
(68, 11, 14, 'vehicle-insur', 'assets/documents/5a549ffbbc581.jpg', NULL, 0, '2018-01-09 10:56:59', '2018-01-09 10:56:59'),
(69, 11, 14, 'auth-img', 'assets/documents/5a549ffbbc72e.jpg', '', 0, '2018-01-09 10:56:59', '2018-01-09 10:56:59'),
(70, 11, 14, 'tafweeth-img', '', NULL, 0, '2018-01-09 10:56:59', '2018-01-09 10:56:59'),
(71, 12, 15, 'photograph', 'assets/cars/Lincoln/Lincoln_Continental_concept_2015_360_720_50-1.png', '', 0, '2018-01-09 11:01:08', '2018-01-09 11:01:08'),
(72, 12, 15, 'vehicle-reg', 'assets/documents/5a54a0f428cbb.jpg', NULL, 0, '2018-01-09 11:01:08', '2018-01-09 11:01:08'),
(73, 12, 15, 'vehicle-insur', 'assets/documents/5a54a0f428e8e.jpg', NULL, 0, '2018-01-09 11:01:08', '2018-01-09 11:01:08'),
(74, 12, 15, 'auth-img', 'assets/documents/5a54a0f429036.jpg', '', 0, '2018-01-09 11:01:08', '2018-01-09 11:01:08'),
(75, 12, 15, 'tafweeth-img', '', NULL, 0, '2018-01-09 11:01:08', '2018-01-09 11:01:08'),
(76, 2, 16, 'photograph', 'assets/cars/caar.png', '', 0, '2018-01-12 12:34:04', '2018-01-12 12:34:04'),
(77, 2, 16, 'vehicle-reg', 'assets/documents/5a58ab3c17fe3.jpg', NULL, 0, '2018-01-12 12:34:04', '2018-01-12 12:34:04'),
(78, 2, 16, 'vehicle-insur', 'assets/documents/5a58ab3c18126.jpg', NULL, 0, '2018-01-12 12:34:04', '2018-01-12 12:34:04'),
(79, 2, 16, 'auth-img', 'assets/documents/5a58ab3c18253.jpg', '', 0, '2018-01-12 12:34:04', '2018-01-12 12:34:04'),
(80, 2, 16, 'tafweeth-img', '', '', 0, '2018-01-12 12:34:04', '2018-01-12 12:34:04');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_family`
--

CREATE TABLE `vehicle_family` (
  `id` int(11) NOT NULL,
  `vehicle_family_name` varchar(50) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `vehicle_family_description` varchar(255) DEFAULT NULL,
  `display_name_ar` varchar(255) DEFAULT NULL,
  `passengers` int(10) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle_family`
--

INSERT INTO `vehicle_family` (`id`, `vehicle_family_name`, `display_name`, `vehicle_family_description`, `display_name_ar`, `passengers`, `created`) VALUES
(1, 'medium', 'Medium', 'Medium', 'سیارۃ متوسطة', 4, '2017-08-02 23:15:14'),
(2, 'family', 'Family', 'Family', 'سیّارۃ عائلیة', 6, '2017-08-02 23:15:14'),
(3, 'luxury', 'Luxury', 'Luxury', 'سیّارۃ فخمة', 4, '2017-08-02 23:15:50'),
(4, 'delivery', 'Delivery', 'Delivery', 'توصيل', 0, '2017-09-12 14:58:14');

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int(11) NOT NULL,
  `card_code` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext NOT NULL,
  `price` varchar(255) DEFAULT NULL,
  `total_rides` int(11) DEFAULT NULL,
  `user_no` int(12) DEFAULT NULL,
  `start_date` varchar(50) DEFAULT NULL,
  `end_date` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `use_flag` int(12) NOT NULL DEFAULT '0',
  `card_type` int(12) DEFAULT NULL,
  `rate_card_pricelist_id` int(12) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `card_code`, `title`, `description`, `price`, `total_rides`, `user_no`, `start_date`, `end_date`, `status`, `use_flag`, `card_type`, `rate_card_pricelist_id`, `created`) VALUES
(1, 'user100', NULL, '', '100', NULL, NULL, '01/05/2018', '01/06/2018', 1, 0, NULL, NULL, '2018-01-05 14:02:53'),
(2, 'voucher50c', NULL, '', '50', NULL, NULL, '01/09/2018', '11/27/2018', 1, 0, NULL, NULL, '2018-01-09 09:32:18'),
(3, 'voucher100c', NULL, '', '100', NULL, NULL, '01/09/2018', '11/26/2018', 1, 0, NULL, NULL, '2018-01-09 09:32:39'),
(4, 'voucher200c', NULL, '', '200', NULL, NULL, '01/09/2018', '11/12/2018', 1, 0, NULL, NULL, '2018-01-09 09:33:03'),
(5, 'voucher150c', NULL, '', '150', NULL, NULL, '01/09/2018', '11/13/2018', 1, 0, NULL, NULL, '2018-01-09 09:33:25'),
(6, 'voucher40d', NULL, '', '40', NULL, NULL, '01/09/2018', '11/19/2018', 1, 0, NULL, NULL, '2018-01-09 09:34:20'),
(7, 'voucher80d', NULL, '', '80', NULL, NULL, '01/09/2018', '11/22/2018', 1, 0, NULL, NULL, '2018-01-09 09:34:37'),
(8, 'voucher120d', NULL, '', '120', NULL, NULL, '01/09/2018', '11/14/2018', 1, 0, NULL, NULL, '2018-01-09 09:34:57'),
(9, 'voucher160d', NULL, '', '800', NULL, NULL, '03/21/2018', '07/10/2018', 1, 0, NULL, NULL, '2018-01-09 09:35:17');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_pricelist`
--

CREATE TABLE `voucher_pricelist` (
  `id` int(12) NOT NULL,
  `price` int(12) NOT NULL,
  `rides` int(12) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher_pricelist`
--

INSERT INTO `voucher_pricelist` (`id`, `price`, `rides`, `created`) VALUES
(1, 100, 10, '2017-07-18 02:02:57'),
(2, 200, 20, '2017-07-18 02:02:57'),
(7, 50, 1, '2017-08-11 11:47:14'),
(8, 0, 1, '2017-08-11 11:51:36'),
(9, 0, 3, '2017-08-11 11:51:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_setting`
--
ALTER TABLE `app_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_logs`
--
ALTER TABLE `backend_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_list`
--
ALTER TABLE `bank_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `card_detail`
--
ALTER TABLE `card_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_master`
--
ALTER TABLE `city_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_name` (`city_name_en`);

--
-- Indexes for table `cms_log`
--
ALTER TABLE `cms_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emailid` (`emailid`);

--
-- Indexes for table `driver_clear_balance`
--
ALTER TABLE `driver_clear_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_feedback`
--
ALTER TABLE `driver_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_locations`
--
ALTER TABLE `driver_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_payment_transfer`
--
ALTER TABLE `driver_payment_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_pending_balance`
--
ALTER TABLE `driver_pending_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_rate_card`
--
ALTER TABLE `driver_rate_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_referal_code`
--
ALTER TABLE `driver_referal_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_reviews`
--
ALTER TABLE `driver_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_ride_action`
--
ALTER TABLE `driver_ride_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_support_option`
--
ALTER TABLE `driver_support_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_transactions`
--
ALTER TABLE `driver_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_vehicles`
--
ALTER TABLE `driver_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_vehicles_segment`
--
ALTER TABLE `driver_vehicles_segment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uix_vehiclesegment` (`driver_id`,`vehicle_type`);

--
-- Indexes for table `driver_wallet_details`
--
ALTER TABLE `driver_wallet_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_template`
--
ALTER TABLE `email_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fare_ratecard`
--
ALTER TABLE `fare_ratecard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_carlist`
--
ALTER TABLE `front_carlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halal_details`
--
ALTER TABLE `halal_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `last_location`
--
ALTER TABLE `last_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_airport_zone`
--
ALTER TABLE `master_airport_zone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_cancel_options`
--
ALTER TABLE `master_cancel_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_car_brand_logo`
--
ALTER TABLE `master_car_brand_logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_charge_type`
--
ALTER TABLE `master_charge_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_country_code`
--
ALTER TABLE `master_country_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_driver_cancel_options`
--
ALTER TABLE `master_driver_cancel_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_feedback`
--
ALTER TABLE `master_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_ride_status`
--
ALTER TABLE `master_ride_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_user_cancel_options`
--
ALTER TABLE `master_user_cancel_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_user_roles`
--
ALTER TABLE `master_user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `najez`
--
ALTER TABLE `najez`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `najez_transactions`
--
ALTER TABLE `najez_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_data`
--
ALTER TABLE `notification_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `event_type` (`event`,`type`);

--
-- Indexes for table `opt_language`
--
ALTER TABLE `opt_language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opt_longtrip`
--
ALTER TABLE `opt_longtrip`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cityid_userid` (`cityid`,`userid`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion_order`
--
ALTER TABLE `promotion_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push_notification_driver`
--
ALTER TABLE `push_notification_driver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push_notification_user`
--
ALTER TABLE `push_notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referal_driver_detail`
--
ALTER TABLE `referal_driver_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refer_amount`
--
ALTER TABLE `refer_amount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `response_message`
--
ALTER TABLE `response_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rides`
--
ALTER TABLE `rides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ride_break_down`
--
ALTER TABLE `ride_break_down`
  ADD PRIMARY KEY (`ride_break_down_id`);

--
-- Indexes for table `ride_complete`
--
ALTER TABLE `ride_complete`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ride_location`
--
ALTER TABLE `ride_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ride_status`
--
ALTER TABLE `ride_status`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sent_notification_detail`
--
ALTER TABLE `sent_notification_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_otp_temp`
--
ALTER TABLE `sms_otp_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions_details`
--
ALTER TABLE `transactions_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trip_car_charges_master`
--
ALTER TABLE `trip_car_charges_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trip_type_master`
--
ALTER TABLE `trip_type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trip_type_vehicle_family`
--
ALTER TABLE `trip_type_vehicle_family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`emailid`);

--
-- Indexes for table `user_feedback`
--
ALTER TABLE `user_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_locations`
--
ALTER TABLE `user_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_payment_transfer`
--
ALTER TABLE `user_payment_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_preferred_locations`
--
ALTER TABLE `user_preferred_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_referal_code`
--
ALTER TABLE `user_referal_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reviews`
--
ALTER TABLE `user_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_support_option`
--
ALTER TABLE `user_support_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_transactions`
--
ALTER TABLE `user_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_wallet_details`
--
ALTER TABLE `user_wallet_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_documents`
--
ALTER TABLE `vehicle_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_family`
--
ALTER TABLE `vehicle_family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_code` (`card_code`);

--
-- Indexes for table `voucher_pricelist`
--
ALTER TABLE `voucher_pricelist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_setting`
--
ALTER TABLE `app_setting`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_logs`
--
ALTER TABLE `backend_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `bank_list`
--
ALTER TABLE `bank_list`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `card_detail`
--
ALTER TABLE `card_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `city_master`
--
ALTER TABLE `city_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `cms_log`
--
ALTER TABLE `cms_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `driver_clear_balance`
--
ALTER TABLE `driver_clear_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `driver_locations`
--
ALTER TABLE `driver_locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_payment_transfer`
--
ALTER TABLE `driver_payment_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_pending_balance`
--
ALTER TABLE `driver_pending_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `driver_rate_card`
--
ALTER TABLE `driver_rate_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_referal_code`
--
ALTER TABLE `driver_referal_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_reviews`
--
ALTER TABLE `driver_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `driver_ride_action`
--
ALTER TABLE `driver_ride_action`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `driver_support_option`
--
ALTER TABLE `driver_support_option`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `driver_transactions`
--
ALTER TABLE `driver_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `driver_vehicles`
--
ALTER TABLE `driver_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `driver_vehicles_segment`
--
ALTER TABLE `driver_vehicles_segment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `driver_wallet_details`
--
ALTER TABLE `driver_wallet_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `email_template`
--
ALTER TABLE `email_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `fare_ratecard`
--
ALTER TABLE `fare_ratecard`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `front_carlist`
--
ALTER TABLE `front_carlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=293;

--
-- AUTO_INCREMENT for table `halal_details`
--
ALTER TABLE `halal_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `last_location`
--
ALTER TABLE `last_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_airport_zone`
--
ALTER TABLE `master_airport_zone`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `master_cancel_options`
--
ALTER TABLE `master_cancel_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_car_brand_logo`
--
ALTER TABLE `master_car_brand_logo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `master_charge_type`
--
ALTER TABLE `master_charge_type`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `master_country_code`
--
ALTER TABLE `master_country_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `master_driver_cancel_options`
--
ALTER TABLE `master_driver_cancel_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `master_ride_status`
--
ALTER TABLE `master_ride_status`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `master_user_cancel_options`
--
ALTER TABLE `master_user_cancel_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `master_user_roles`
--
ALTER TABLE `master_user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `najez`
--
ALTER TABLE `najez`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `najez_transactions`
--
ALTER TABLE `najez_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `notification_data`
--
ALTER TABLE `notification_data`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `opt_language`
--
ALTER TABLE `opt_language`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `opt_longtrip`
--
ALTER TABLE `opt_longtrip`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `promotion_order`
--
ALTER TABLE `promotion_order`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `push_notification_driver`
--
ALTER TABLE `push_notification_driver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `push_notification_user`
--
ALTER TABLE `push_notification_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `referal_driver_detail`
--
ALTER TABLE `referal_driver_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `refer_amount`
--
ALTER TABLE `refer_amount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `response_message`
--
ALTER TABLE `response_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `rides`
--
ALTER TABLE `rides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `ride_break_down`
--
ALTER TABLE `ride_break_down`
  MODIFY `ride_break_down_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `ride_complete`
--
ALTER TABLE `ride_complete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `ride_location`
--
ALTER TABLE `ride_location`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6317;

--
-- AUTO_INCREMENT for table `sent_notification_detail`
--
ALTER TABLE `sent_notification_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT for table `sms_otp_temp`
--
ALTER TABLE `sms_otp_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transactions_details`
--
ALTER TABLE `transactions_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `trip_type_master`
--
ALTER TABLE `trip_type_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trip_type_vehicle_family`
--
ALTER TABLE `trip_type_vehicle_family`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user_locations`
--
ALTER TABLE `user_locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_payment_transfer`
--
ALTER TABLE `user_payment_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_referal_code`
--
ALTER TABLE `user_referal_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reviews`
--
ALTER TABLE `user_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `user_support_option`
--
ALTER TABLE `user_support_option`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_transactions`
--
ALTER TABLE `user_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_wallet_details`
--
ALTER TABLE `user_wallet_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `vehicle_documents`
--
ALTER TABLE `vehicle_documents`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `vehicle_family`
--
ALTER TABLE `vehicle_family`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `voucher_pricelist`
--
ALTER TABLE `voucher_pricelist`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
